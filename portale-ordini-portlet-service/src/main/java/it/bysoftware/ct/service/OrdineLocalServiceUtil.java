package it.bysoftware.ct.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Ordine. This utility wraps
 * {@link it.bysoftware.ct.service.impl.OrdineLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Mario Torrisi
 * @see OrdineLocalService
 * @see it.bysoftware.ct.service.base.OrdineLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.impl.OrdineLocalServiceImpl
 * @generated
 */
public class OrdineLocalServiceUtil {
    private static OrdineLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link it.bysoftware.ct.service.impl.OrdineLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the ordine to the database. Also notifies the appropriate model listeners.
    *
    * @param ordine the ordine
    * @return the ordine that was added
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine addOrdine(
        it.bysoftware.ct.model.Ordine ordine)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addOrdine(ordine);
    }

    /**
    * Creates a new ordine with the primary key. Does not add the ordine to the database.
    *
    * @param id the primary key for the new ordine
    * @return the new ordine
    */
    public static it.bysoftware.ct.model.Ordine createOrdine(long id) {
        return getService().createOrdine(id);
    }

    /**
    * Deletes the ordine with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the ordine
    * @return the ordine that was removed
    * @throws PortalException if a ordine with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine deleteOrdine(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteOrdine(id);
    }

    /**
    * Deletes the ordine from the database. Also notifies the appropriate model listeners.
    *
    * @param ordine the ordine
    * @return the ordine that was removed
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine deleteOrdine(
        it.bysoftware.ct.model.Ordine ordine)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteOrdine(ordine);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static it.bysoftware.ct.model.Ordine fetchOrdine(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchOrdine(id);
    }

    /**
    * Returns the ordine with the primary key.
    *
    * @param id the primary key of the ordine
    * @return the ordine
    * @throws PortalException if a ordine with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine getOrdine(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getOrdine(id);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the ordines.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @return the range of ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> getOrdines(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getOrdines(start, end);
    }

    /**
    * Returns the number of ordines.
    *
    * @return the number of ordines
    * @throws SystemException if a system exception occurred
    */
    public static int getOrdinesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getOrdinesCount();
    }

    /**
    * Updates the ordine in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param ordine the ordine
    * @return the ordine that was updated
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine updateOrdine(
        it.bysoftware.ct.model.Ordine ordine)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateOrdine(ordine);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static int getNumber(int year, java.lang.String center) {
        return getService().getNumber(year, center);
    }

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param partnerCode
    the partner code
    * @param state
    the state
    * @param passport
    the passport number
    * @return the array of order IDs
    */
    public static java.util.List<java.lang.Object[]> getOrderByCodeStatePassport(
        java.lang.String partnerCode, int state, java.lang.String passport) {
        return getService()
                   .getOrderByCodeStatePassport(partnerCode, state, passport);
    }

    public static java.util.List<it.bysoftware.ct.model.Ordine> getOrderByCustomer(
        java.lang.String code) {
        return getService().getOrderByCustomer(code);
    }

    public static java.util.List<it.bysoftware.ct.model.Ordine> getOrderByState(
        int state) {
        return getService().getOrderByState(state);
    }

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param state
    the state
    * @param passport
    the passport number
    * @return the array of order IDs
    */
    public static java.util.List<java.lang.Object[]> getOrderByStatePassport(
        int state, java.lang.String passport) {
        return getService().getOrderByStatePassport(state, passport);
    }

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param state
    the state
    * @param passport
    the passport number
    * @return the array of order IDs
    */
    public static java.util.List<java.lang.Object[]> getOrderByStatePassportPartner(
        int state, java.lang.String passport, java.lang.String partnerCode) {
        return getService()
                   .getOrderByStatePassportPartner(state, passport, partnerCode);
    }

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param partnerCode
    the partner code
    * @param from
    from date
    * @param to
    to date
    * @return the array of order IDs
    */
    public static java.util.List<java.lang.Object[]> getOrderBetweenDatePerPartner(
        java.lang.String partnerCode, java.util.Date from, java.util.Date to) {
        return getService().getOrderBetweenDatePerPartner(partnerCode, from, to);
    }

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param from
    from date
    * @param to
    to date
    * @return the array of order IDs
    */
    public static java.util.List<java.lang.Object[]> getOrderBetweenDate(
        java.util.Date from, java.util.Date to) {
        return getService().getOrderBetweenDate(from, to);
    }

    public static java.util.List<java.lang.Object[]> getOrderBetweenDatePerItem(
        java.lang.String cod, java.util.Date from, java.util.Date to) {
        return getService().getOrderBetweenDatePerItem(cod, from, to);
    }

    public static java.util.List<java.lang.Object[]> getItemsInOrderBetweenDate(
        java.util.Date from, java.util.Date to) {
        return getService().getItemsInOrderBetweenDate(from, to);
    }

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param partnerCode
    the partner code
    * @param from
    from date
    * @param to
    to date
    * @return the array of order IDs
    */
    public static java.util.List<java.lang.Object[]> getOrderWithPassportByPartnerAndDate(
        java.lang.String partnerCode, java.util.Date from, java.util.Date to) {
        return getService()
                   .getOrderWithPassportByPartnerAndDate(partnerCode, from, to);
    }

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param from
    from date
    * @param to
    to date
    * @return the array of order IDs
    */
    public static java.util.List<java.lang.Object[]> getOrderWithPassportByDate(
        java.util.Date from, java.util.Date to) {
        return getService().getOrderWithPassportByDate(from, to);
    }

    public static java.util.List<it.bysoftware.ct.model.Ordine> getCustomerConfirmedOrders(
        java.lang.String code) {
        return getService().getCustomerConfirmedOrders(code);
    }

    public static java.util.List<it.bysoftware.ct.model.Ordine> getCustomerPendingOrders(
        java.lang.String code) {
        return getService().getCustomerPendingOrders(code);
    }

    public static java.util.List<it.bysoftware.ct.model.Ordine> getSavedCustomerOrders(
        java.lang.String code) {
        return getService().getSavedCustomerOrders(code);
    }

    public static java.util.List<it.bysoftware.ct.model.Ordine> getScheduledCustomerOrders(
        java.lang.String code) {
        return getService().getScheduledCustomerOrders(code);
    }

    public static java.util.List<it.bysoftware.ct.model.Ordine> getProcessedCustomerOrders(
        java.lang.String code) {
        return getService().getProcessedCustomerOrders(code);
    }

    public static java.util.List<it.bysoftware.ct.model.Ordine> getUnScheduledOrderByCustomer(
        java.lang.String code) {
        return getService().getUnScheduledOrderByCustomer(code);
    }

    public static java.util.List<it.bysoftware.ct.model.Ordine> getTodayReadyOrders() {
        return getService().getTodayReadyOrders();
    }

    public static java.util.List<it.bysoftware.ct.model.Ordine> getTodayConfirmedOrders() {
        return getService().getTodayConfirmedOrders();
    }

    public static java.util.List<it.bysoftware.ct.model.Ordine> getTodayProcessedOrders() {
        return getService().getTodayProcessedOrders();
    }

    public static java.util.List<it.bysoftware.ct.model.Ordine> findOrders(
        java.lang.String customerCode, java.util.Date orderDate,
        java.util.Date shipDate, boolean showsInactive, boolean showsDraft,
        boolean showsPending, boolean showsApproved, boolean showsProcessed,
        boolean andSearch, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .findOrders(customerCode, orderDate, shipDate,
            showsInactive, showsDraft, showsPending, showsApproved,
            showsProcessed, andSearch, start, end, orderByComparator);
    }

    public static int getFindOrdersCount(java.lang.String customerCode,
        java.util.Date orderDate, java.util.Date shipDate,
        boolean showsInactive, boolean showsDraft, boolean showsPending,
        boolean showsApproved, boolean showsProcessed, boolean andSearch)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .getFindOrdersCount(customerCode, orderDate, shipDate,
            showsInactive, showsDraft, showsPending, showsApproved,
            showsProcessed, andSearch);
    }

    public static it.bysoftware.ct.model.Ordine getByYearNumberCenter(int n,
        java.lang.String c) {
        return getService().getByYearNumberCenter(n, c);
    }

    public static void clearService() {
        _service = null;
    }

    public static OrdineLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    OrdineLocalService.class.getName());

            if (invokableLocalService instanceof OrdineLocalService) {
                _service = (OrdineLocalService) invokableLocalService;
            } else {
                _service = new OrdineLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(OrdineLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(OrdineLocalService service) {
    }
}
