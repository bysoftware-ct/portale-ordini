package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CausaliContabiliService}.
 *
 * @author Mario Torrisi
 * @see CausaliContabiliService
 * @generated
 */
public class CausaliContabiliServiceWrapper implements CausaliContabiliService,
    ServiceWrapper<CausaliContabiliService> {
    private CausaliContabiliService _causaliContabiliService;

    public CausaliContabiliServiceWrapper(
        CausaliContabiliService causaliContabiliService) {
        _causaliContabiliService = causaliContabiliService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _causaliContabiliService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _causaliContabiliService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _causaliContabiliService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public CausaliContabiliService getWrappedCausaliContabiliService() {
        return _causaliContabiliService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedCausaliContabiliService(
        CausaliContabiliService causaliContabiliService) {
        _causaliContabiliService = causaliContabiliService;
    }

    @Override
    public CausaliContabiliService getWrappedService() {
        return _causaliContabiliService;
    }

    @Override
    public void setWrappedService(
        CausaliContabiliService causaliContabiliService) {
        _causaliContabiliService = causaliContabiliService;
    }
}
