package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CategorieService}.
 *
 * @author Mario Torrisi
 * @see CategorieService
 * @generated
 */
public class CategorieServiceWrapper implements CategorieService,
    ServiceWrapper<CategorieService> {
    private CategorieService _categorieService;

    public CategorieServiceWrapper(CategorieService categorieService) {
        _categorieService = categorieService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _categorieService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _categorieService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _categorieService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public CategorieService getWrappedCategorieService() {
        return _categorieService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedCategorieService(CategorieService categorieService) {
        _categorieService = categorieService;
    }

    @Override
    public CategorieService getWrappedService() {
        return _categorieService;
    }

    @Override
    public void setWrappedService(CategorieService categorieService) {
        _categorieService = categorieService;
    }
}
