package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VuotoLocalService}.
 *
 * @author Mario Torrisi
 * @see VuotoLocalService
 * @generated
 */
public class VuotoLocalServiceWrapper implements VuotoLocalService,
    ServiceWrapper<VuotoLocalService> {
    private VuotoLocalService _vuotoLocalService;

    public VuotoLocalServiceWrapper(VuotoLocalService vuotoLocalService) {
        _vuotoLocalService = vuotoLocalService;
    }

    /**
    * Adds the vuoto to the database. Also notifies the appropriate model listeners.
    *
    * @param vuoto the vuoto
    * @return the vuoto that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Vuoto addVuoto(
        it.bysoftware.ct.model.Vuoto vuoto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vuotoLocalService.addVuoto(vuoto);
    }

    /**
    * Creates a new vuoto with the primary key. Does not add the vuoto to the database.
    *
    * @param codiceVuoto the primary key for the new vuoto
    * @return the new vuoto
    */
    @Override
    public it.bysoftware.ct.model.Vuoto createVuoto(
        java.lang.String codiceVuoto) {
        return _vuotoLocalService.createVuoto(codiceVuoto);
    }

    /**
    * Deletes the vuoto with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceVuoto the primary key of the vuoto
    * @return the vuoto that was removed
    * @throws PortalException if a vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Vuoto deleteVuoto(
        java.lang.String codiceVuoto)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vuotoLocalService.deleteVuoto(codiceVuoto);
    }

    /**
    * Deletes the vuoto from the database. Also notifies the appropriate model listeners.
    *
    * @param vuoto the vuoto
    * @return the vuoto that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Vuoto deleteVuoto(
        it.bysoftware.ct.model.Vuoto vuoto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vuotoLocalService.deleteVuoto(vuoto);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _vuotoLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vuotoLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _vuotoLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vuotoLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vuotoLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vuotoLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public it.bysoftware.ct.model.Vuoto fetchVuoto(java.lang.String codiceVuoto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vuotoLocalService.fetchVuoto(codiceVuoto);
    }

    /**
    * Returns the vuoto with the primary key.
    *
    * @param codiceVuoto the primary key of the vuoto
    * @return the vuoto
    * @throws PortalException if a vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Vuoto getVuoto(java.lang.String codiceVuoto)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vuotoLocalService.getVuoto(codiceVuoto);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vuotoLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the vuotos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vuotos
    * @param end the upper bound of the range of vuotos (not inclusive)
    * @return the range of vuotos
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.Vuoto> getVuotos(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _vuotoLocalService.getVuotos(start, end);
    }

    /**
    * Returns the number of vuotos.
    *
    * @return the number of vuotos
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getVuotosCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vuotoLocalService.getVuotosCount();
    }

    /**
    * Updates the vuoto in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param vuoto the vuoto
    * @return the vuoto that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Vuoto updateVuoto(
        it.bysoftware.ct.model.Vuoto vuoto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vuotoLocalService.updateVuoto(vuoto);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _vuotoLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _vuotoLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _vuotoLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public VuotoLocalService getWrappedVuotoLocalService() {
        return _vuotoLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedVuotoLocalService(VuotoLocalService vuotoLocalService) {
        _vuotoLocalService = vuotoLocalService;
    }

    @Override
    public VuotoLocalService getWrappedService() {
        return _vuotoLocalService;
    }

    @Override
    public void setWrappedService(VuotoLocalService vuotoLocalService) {
        _vuotoLocalService = vuotoLocalService;
    }
}
