package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ScadenzePartiteLocalService}.
 *
 * @author Mario Torrisi
 * @see ScadenzePartiteLocalService
 * @generated
 */
public class ScadenzePartiteLocalServiceWrapper
    implements ScadenzePartiteLocalService,
        ServiceWrapper<ScadenzePartiteLocalService> {
    private ScadenzePartiteLocalService _scadenzePartiteLocalService;

    public ScadenzePartiteLocalServiceWrapper(
        ScadenzePartiteLocalService scadenzePartiteLocalService) {
        _scadenzePartiteLocalService = scadenzePartiteLocalService;
    }

    /**
    * Adds the scadenze partite to the database. Also notifies the appropriate model listeners.
    *
    * @param scadenzePartite the scadenze partite
    * @return the scadenze partite that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ScadenzePartite addScadenzePartite(
        it.bysoftware.ct.model.ScadenzePartite scadenzePartite)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.addScadenzePartite(scadenzePartite);
    }

    /**
    * Creates a new scadenze partite with the primary key. Does not add the scadenze partite to the database.
    *
    * @param scadenzePartitePK the primary key for the new scadenze partite
    * @return the new scadenze partite
    */
    @Override
    public it.bysoftware.ct.model.ScadenzePartite createScadenzePartite(
        it.bysoftware.ct.service.persistence.ScadenzePartitePK scadenzePartitePK) {
        return _scadenzePartiteLocalService.createScadenzePartite(scadenzePartitePK);
    }

    /**
    * Deletes the scadenze partite with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param scadenzePartitePK the primary key of the scadenze partite
    * @return the scadenze partite that was removed
    * @throws PortalException if a scadenze partite with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ScadenzePartite deleteScadenzePartite(
        it.bysoftware.ct.service.persistence.ScadenzePartitePK scadenzePartitePK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.deleteScadenzePartite(scadenzePartitePK);
    }

    /**
    * Deletes the scadenze partite from the database. Also notifies the appropriate model listeners.
    *
    * @param scadenzePartite the scadenze partite
    * @return the scadenze partite that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ScadenzePartite deleteScadenzePartite(
        it.bysoftware.ct.model.ScadenzePartite scadenzePartite)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.deleteScadenzePartite(scadenzePartite);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _scadenzePartiteLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ScadenzePartiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.dynamicQuery(dynamicQuery, start,
            end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ScadenzePartiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public it.bysoftware.ct.model.ScadenzePartite fetchScadenzePartite(
        it.bysoftware.ct.service.persistence.ScadenzePartitePK scadenzePartitePK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.fetchScadenzePartite(scadenzePartitePK);
    }

    /**
    * Returns the scadenze partite with the primary key.
    *
    * @param scadenzePartitePK the primary key of the scadenze partite
    * @return the scadenze partite
    * @throws PortalException if a scadenze partite with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ScadenzePartite getScadenzePartite(
        it.bysoftware.ct.service.persistence.ScadenzePartitePK scadenzePartitePK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.getScadenzePartite(scadenzePartitePK);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the scadenze partites.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ScadenzePartiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of scadenze partites
    * @param end the upper bound of the range of scadenze partites (not inclusive)
    * @return the range of scadenze partites
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.ScadenzePartite> getScadenzePartites(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.getScadenzePartites(start, end);
    }

    /**
    * Returns the number of scadenze partites.
    *
    * @return the number of scadenze partites
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getScadenzePartitesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.getScadenzePartitesCount();
    }

    /**
    * Updates the scadenze partite in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param scadenzePartite the scadenze partite
    * @return the scadenze partite that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ScadenzePartite updateScadenzePartite(
        it.bysoftware.ct.model.ScadenzePartite scadenzePartite)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.updateScadenzePartite(scadenzePartite);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _scadenzePartiteLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _scadenzePartiteLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _scadenzePartiteLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.ScadenzePartite> getCustomersClosedEntries(
        java.util.Date from, java.util.Date to, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.getCustomersClosedEntries(from, to,
            start, end, orderByComparator);
    }

    @Override
    public int getCustomersClosedEntriesCount(java.util.Date from,
        java.util.Date to)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.getCustomersClosedEntriesCount(from,
            to);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.ScadenzePartite> getCustomerClosedEntries(
        java.lang.String customer, java.util.Date from, java.util.Date to,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.getCustomerClosedEntries(customer,
            from, to, start, end, orderByComparator);
    }

    @Override
    public int getCustomerClosedEntriesCount(java.lang.String customer,
        java.util.Date from, java.util.Date to)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.getCustomerClosedEntriesCount(customer,
            from, to);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.ScadenzePartite> getPartnerOpenedEntries(
        java.lang.String partnerCode, java.util.Date from, java.util.Date to,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.getPartnerOpenedEntries(partnerCode,
            from, to, start, end, orderByComparator);
    }

    @Override
    public int getPartnerOpenedEntriesCount(java.lang.String partnerCode,
        java.util.Date from, java.util.Date to)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _scadenzePartiteLocalService.getPartnerOpenedEntriesCount(partnerCode,
            from, to);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public ScadenzePartiteLocalService getWrappedScadenzePartiteLocalService() {
        return _scadenzePartiteLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedScadenzePartiteLocalService(
        ScadenzePartiteLocalService scadenzePartiteLocalService) {
        _scadenzePartiteLocalService = scadenzePartiteLocalService;
    }

    @Override
    public ScadenzePartiteLocalService getWrappedService() {
        return _scadenzePartiteLocalService;
    }

    @Override
    public void setWrappedService(
        ScadenzePartiteLocalService scadenzePartiteLocalService) {
        _scadenzePartiteLocalService = scadenzePartiteLocalService;
    }
}
