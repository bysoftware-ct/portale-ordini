package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link DescrizioniVariantiService}.
 *
 * @author Mario Torrisi
 * @see DescrizioniVariantiService
 * @generated
 */
public class DescrizioniVariantiServiceWrapper
    implements DescrizioniVariantiService,
        ServiceWrapper<DescrizioniVariantiService> {
    private DescrizioniVariantiService _descrizioniVariantiService;

    public DescrizioniVariantiServiceWrapper(
        DescrizioniVariantiService descrizioniVariantiService) {
        _descrizioniVariantiService = descrizioniVariantiService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _descrizioniVariantiService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _descrizioniVariantiService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _descrizioniVariantiService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public DescrizioniVariantiService getWrappedDescrizioniVariantiService() {
        return _descrizioniVariantiService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedDescrizioniVariantiService(
        DescrizioniVariantiService descrizioniVariantiService) {
        _descrizioniVariantiService = descrizioniVariantiService;
    }

    @Override
    public DescrizioniVariantiService getWrappedService() {
        return _descrizioniVariantiService;
    }

    @Override
    public void setWrappedService(
        DescrizioniVariantiService descrizioniVariantiService) {
        _descrizioniVariantiService = descrizioniVariantiService;
    }
}
