package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.DettagliOperazioniContabili;

/**
 * The persistence interface for the dettagli operazioni contabili service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see DettagliOperazioniContabiliPersistenceImpl
 * @see DettagliOperazioniContabiliUtil
 * @generated
 */
public interface DettagliOperazioniContabiliPersistence extends BasePersistence<DettagliOperazioniContabili> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link DettagliOperazioniContabiliUtil} to access the dettagli operazioni contabili persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the dettagli operazioni contabilis where codiceOperazione = &#63;.
    *
    * @param codiceOperazione the codice operazione
    * @return the matching dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.DettagliOperazioniContabili> findByCodiceOperazione(
        java.lang.String codiceOperazione)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the dettagli operazioni contabilis where codiceOperazione = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceOperazione the codice operazione
    * @param start the lower bound of the range of dettagli operazioni contabilis
    * @param end the upper bound of the range of dettagli operazioni contabilis (not inclusive)
    * @return the range of matching dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.DettagliOperazioniContabili> findByCodiceOperazione(
        java.lang.String codiceOperazione, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the dettagli operazioni contabilis where codiceOperazione = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceOperazione the codice operazione
    * @param start the lower bound of the range of dettagli operazioni contabilis
    * @param end the upper bound of the range of dettagli operazioni contabilis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.DettagliOperazioniContabili> findByCodiceOperazione(
        java.lang.String codiceOperazione, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first dettagli operazioni contabili in the ordered set where codiceOperazione = &#63;.
    *
    * @param codiceOperazione the codice operazione
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching dettagli operazioni contabili
    * @throws it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException if a matching dettagli operazioni contabili could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.DettagliOperazioniContabili findByCodiceOperazione_First(
        java.lang.String codiceOperazione,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException;

    /**
    * Returns the first dettagli operazioni contabili in the ordered set where codiceOperazione = &#63;.
    *
    * @param codiceOperazione the codice operazione
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching dettagli operazioni contabili, or <code>null</code> if a matching dettagli operazioni contabili could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.DettagliOperazioniContabili fetchByCodiceOperazione_First(
        java.lang.String codiceOperazione,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last dettagli operazioni contabili in the ordered set where codiceOperazione = &#63;.
    *
    * @param codiceOperazione the codice operazione
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching dettagli operazioni contabili
    * @throws it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException if a matching dettagli operazioni contabili could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.DettagliOperazioniContabili findByCodiceOperazione_Last(
        java.lang.String codiceOperazione,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException;

    /**
    * Returns the last dettagli operazioni contabili in the ordered set where codiceOperazione = &#63;.
    *
    * @param codiceOperazione the codice operazione
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching dettagli operazioni contabili, or <code>null</code> if a matching dettagli operazioni contabili could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.DettagliOperazioniContabili fetchByCodiceOperazione_Last(
        java.lang.String codiceOperazione,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the dettagli operazioni contabilis before and after the current dettagli operazioni contabili in the ordered set where codiceOperazione = &#63;.
    *
    * @param dettagliOperazioniContabiliPK the primary key of the current dettagli operazioni contabili
    * @param codiceOperazione the codice operazione
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next dettagli operazioni contabili
    * @throws it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException if a dettagli operazioni contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.DettagliOperazioniContabili[] findByCodiceOperazione_PrevAndNext(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK,
        java.lang.String codiceOperazione,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException;

    /**
    * Removes all the dettagli operazioni contabilis where codiceOperazione = &#63; from the database.
    *
    * @param codiceOperazione the codice operazione
    * @throws SystemException if a system exception occurred
    */
    public void removeByCodiceOperazione(java.lang.String codiceOperazione)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of dettagli operazioni contabilis where codiceOperazione = &#63;.
    *
    * @param codiceOperazione the codice operazione
    * @return the number of matching dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    public int countByCodiceOperazione(java.lang.String codiceOperazione)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the dettagli operazioni contabili in the entity cache if it is enabled.
    *
    * @param dettagliOperazioniContabili the dettagli operazioni contabili
    */
    public void cacheResult(
        it.bysoftware.ct.model.DettagliOperazioniContabili dettagliOperazioniContabili);

    /**
    * Caches the dettagli operazioni contabilis in the entity cache if it is enabled.
    *
    * @param dettagliOperazioniContabilis the dettagli operazioni contabilis
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.DettagliOperazioniContabili> dettagliOperazioniContabilis);

    /**
    * Creates a new dettagli operazioni contabili with the primary key. Does not add the dettagli operazioni contabili to the database.
    *
    * @param dettagliOperazioniContabiliPK the primary key for the new dettagli operazioni contabili
    * @return the new dettagli operazioni contabili
    */
    public it.bysoftware.ct.model.DettagliOperazioniContabili create(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK);

    /**
    * Removes the dettagli operazioni contabili with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param dettagliOperazioniContabiliPK the primary key of the dettagli operazioni contabili
    * @return the dettagli operazioni contabili that was removed
    * @throws it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException if a dettagli operazioni contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.DettagliOperazioniContabili remove(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException;

    public it.bysoftware.ct.model.DettagliOperazioniContabili updateImpl(
        it.bysoftware.ct.model.DettagliOperazioniContabili dettagliOperazioniContabili)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the dettagli operazioni contabili with the primary key or throws a {@link it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException} if it could not be found.
    *
    * @param dettagliOperazioniContabiliPK the primary key of the dettagli operazioni contabili
    * @return the dettagli operazioni contabili
    * @throws it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException if a dettagli operazioni contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.DettagliOperazioniContabili findByPrimaryKey(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException;

    /**
    * Returns the dettagli operazioni contabili with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param dettagliOperazioniContabiliPK the primary key of the dettagli operazioni contabili
    * @return the dettagli operazioni contabili, or <code>null</code> if a dettagli operazioni contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.DettagliOperazioniContabili fetchByPrimaryKey(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the dettagli operazioni contabilis.
    *
    * @return the dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.DettagliOperazioniContabili> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the dettagli operazioni contabilis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of dettagli operazioni contabilis
    * @param end the upper bound of the range of dettagli operazioni contabilis (not inclusive)
    * @return the range of dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.DettagliOperazioniContabili> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the dettagli operazioni contabilis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of dettagli operazioni contabilis
    * @param end the upper bound of the range of dettagli operazioni contabilis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.DettagliOperazioniContabili> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the dettagli operazioni contabilis from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of dettagli operazioni contabilis.
    *
    * @return the number of dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
