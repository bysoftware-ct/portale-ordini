package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.Carrello;

/**
 * The persistence interface for the carrello service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CarrelloPersistenceImpl
 * @see CarrelloUtil
 * @generated
 */
public interface CarrelloPersistence extends BasePersistence<Carrello> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link CarrelloUtil} to access the carrello persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the carrellos where idOrdine = &#63;.
    *
    * @param idOrdine the id ordine
    * @return the matching carrellos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Carrello> findByIdOrdine(
        long idOrdine)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the carrellos where idOrdine = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CarrelloModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param idOrdine the id ordine
    * @param start the lower bound of the range of carrellos
    * @param end the upper bound of the range of carrellos (not inclusive)
    * @return the range of matching carrellos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Carrello> findByIdOrdine(
        long idOrdine, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the carrellos where idOrdine = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CarrelloModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param idOrdine the id ordine
    * @param start the lower bound of the range of carrellos
    * @param end the upper bound of the range of carrellos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching carrellos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Carrello> findByIdOrdine(
        long idOrdine, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first carrello in the ordered set where idOrdine = &#63;.
    *
    * @param idOrdine the id ordine
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching carrello
    * @throws it.bysoftware.ct.NoSuchCarrelloException if a matching carrello could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Carrello findByIdOrdine_First(long idOrdine,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCarrelloException;

    /**
    * Returns the first carrello in the ordered set where idOrdine = &#63;.
    *
    * @param idOrdine the id ordine
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching carrello, or <code>null</code> if a matching carrello could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Carrello fetchByIdOrdine_First(
        long idOrdine,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last carrello in the ordered set where idOrdine = &#63;.
    *
    * @param idOrdine the id ordine
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching carrello
    * @throws it.bysoftware.ct.NoSuchCarrelloException if a matching carrello could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Carrello findByIdOrdine_Last(long idOrdine,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCarrelloException;

    /**
    * Returns the last carrello in the ordered set where idOrdine = &#63;.
    *
    * @param idOrdine the id ordine
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching carrello, or <code>null</code> if a matching carrello could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Carrello fetchByIdOrdine_Last(long idOrdine,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the carrellos before and after the current carrello in the ordered set where idOrdine = &#63;.
    *
    * @param id the primary key of the current carrello
    * @param idOrdine the id ordine
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next carrello
    * @throws it.bysoftware.ct.NoSuchCarrelloException if a carrello with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Carrello[] findByIdOrdine_PrevAndNext(
        long id, long idOrdine,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCarrelloException;

    /**
    * Removes all the carrellos where idOrdine = &#63; from the database.
    *
    * @param idOrdine the id ordine
    * @throws SystemException if a system exception occurred
    */
    public void removeByIdOrdine(long idOrdine)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of carrellos where idOrdine = &#63;.
    *
    * @param idOrdine the id ordine
    * @return the number of matching carrellos
    * @throws SystemException if a system exception occurred
    */
    public int countByIdOrdine(long idOrdine)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the carrello in the entity cache if it is enabled.
    *
    * @param carrello the carrello
    */
    public void cacheResult(it.bysoftware.ct.model.Carrello carrello);

    /**
    * Caches the carrellos in the entity cache if it is enabled.
    *
    * @param carrellos the carrellos
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.Carrello> carrellos);

    /**
    * Creates a new carrello with the primary key. Does not add the carrello to the database.
    *
    * @param id the primary key for the new carrello
    * @return the new carrello
    */
    public it.bysoftware.ct.model.Carrello create(long id);

    /**
    * Removes the carrello with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the carrello
    * @return the carrello that was removed
    * @throws it.bysoftware.ct.NoSuchCarrelloException if a carrello with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Carrello remove(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCarrelloException;

    public it.bysoftware.ct.model.Carrello updateImpl(
        it.bysoftware.ct.model.Carrello carrello)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the carrello with the primary key or throws a {@link it.bysoftware.ct.NoSuchCarrelloException} if it could not be found.
    *
    * @param id the primary key of the carrello
    * @return the carrello
    * @throws it.bysoftware.ct.NoSuchCarrelloException if a carrello with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Carrello findByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCarrelloException;

    /**
    * Returns the carrello with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the carrello
    * @return the carrello, or <code>null</code> if a carrello with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Carrello fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the carrellos.
    *
    * @return the carrellos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Carrello> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the carrellos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CarrelloModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of carrellos
    * @param end the upper bound of the range of carrellos (not inclusive)
    * @return the range of carrellos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Carrello> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the carrellos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CarrelloModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of carrellos
    * @param end the upper bound of the range of carrellos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of carrellos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Carrello> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the carrellos from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of carrellos.
    *
    * @return the number of carrellos
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
