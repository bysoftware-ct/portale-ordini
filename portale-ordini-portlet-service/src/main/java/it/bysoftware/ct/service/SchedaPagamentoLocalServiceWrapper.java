package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SchedaPagamentoLocalService}.
 *
 * @author Mario Torrisi
 * @see SchedaPagamentoLocalService
 * @generated
 */
public class SchedaPagamentoLocalServiceWrapper
    implements SchedaPagamentoLocalService,
        ServiceWrapper<SchedaPagamentoLocalService> {
    private SchedaPagamentoLocalService _schedaPagamentoLocalService;

    public SchedaPagamentoLocalServiceWrapper(
        SchedaPagamentoLocalService schedaPagamentoLocalService) {
        _schedaPagamentoLocalService = schedaPagamentoLocalService;
    }

    /**
    * Adds the scheda pagamento to the database. Also notifies the appropriate model listeners.
    *
    * @param schedaPagamento the scheda pagamento
    * @return the scheda pagamento that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.SchedaPagamento addSchedaPagamento(
        it.bysoftware.ct.model.SchedaPagamento schedaPagamento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _schedaPagamentoLocalService.addSchedaPagamento(schedaPagamento);
    }

    /**
    * Creates a new scheda pagamento with the primary key. Does not add the scheda pagamento to the database.
    *
    * @param schedaPagamentoPK the primary key for the new scheda pagamento
    * @return the new scheda pagamento
    */
    @Override
    public it.bysoftware.ct.model.SchedaPagamento createSchedaPagamento(
        it.bysoftware.ct.service.persistence.SchedaPagamentoPK schedaPagamentoPK) {
        return _schedaPagamentoLocalService.createSchedaPagamento(schedaPagamentoPK);
    }

    /**
    * Deletes the scheda pagamento with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param schedaPagamentoPK the primary key of the scheda pagamento
    * @return the scheda pagamento that was removed
    * @throws PortalException if a scheda pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.SchedaPagamento deleteSchedaPagamento(
        it.bysoftware.ct.service.persistence.SchedaPagamentoPK schedaPagamentoPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _schedaPagamentoLocalService.deleteSchedaPagamento(schedaPagamentoPK);
    }

    /**
    * Deletes the scheda pagamento from the database. Also notifies the appropriate model listeners.
    *
    * @param schedaPagamento the scheda pagamento
    * @return the scheda pagamento that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.SchedaPagamento deleteSchedaPagamento(
        it.bysoftware.ct.model.SchedaPagamento schedaPagamento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _schedaPagamentoLocalService.deleteSchedaPagamento(schedaPagamento);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _schedaPagamentoLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _schedaPagamentoLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _schedaPagamentoLocalService.dynamicQuery(dynamicQuery, start,
            end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _schedaPagamentoLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _schedaPagamentoLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _schedaPagamentoLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public it.bysoftware.ct.model.SchedaPagamento fetchSchedaPagamento(
        it.bysoftware.ct.service.persistence.SchedaPagamentoPK schedaPagamentoPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _schedaPagamentoLocalService.fetchSchedaPagamento(schedaPagamentoPK);
    }

    /**
    * Returns the scheda pagamento with the primary key.
    *
    * @param schedaPagamentoPK the primary key of the scheda pagamento
    * @return the scheda pagamento
    * @throws PortalException if a scheda pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.SchedaPagamento getSchedaPagamento(
        it.bysoftware.ct.service.persistence.SchedaPagamentoPK schedaPagamentoPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _schedaPagamentoLocalService.getSchedaPagamento(schedaPagamentoPK);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _schedaPagamentoLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the scheda pagamentos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of scheda pagamentos
    * @param end the upper bound of the range of scheda pagamentos (not inclusive)
    * @return the range of scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.SchedaPagamento> getSchedaPagamentos(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _schedaPagamentoLocalService.getSchedaPagamentos(start, end);
    }

    /**
    * Returns the number of scheda pagamentos.
    *
    * @return the number of scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getSchedaPagamentosCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _schedaPagamentoLocalService.getSchedaPagamentosCount();
    }

    /**
    * Updates the scheda pagamento in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param schedaPagamento the scheda pagamento
    * @return the scheda pagamento that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.SchedaPagamento updateSchedaPagamento(
        it.bysoftware.ct.model.SchedaPagamento schedaPagamento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _schedaPagamentoLocalService.updateSchedaPagamento(schedaPagamento);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _schedaPagamentoLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _schedaPagamentoLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _schedaPagamentoLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.SchedaPagamento> findByCustomerYearAndIdPayment(
        java.lang.String partnerCode, int year, int idPagamento) {
        return _schedaPagamentoLocalService.findByCustomerYearAndIdPayment(partnerCode,
            year, idPagamento);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.SchedaPagamento> findByCustomerYearIdPaymentAndState(
        java.lang.String partnerCode, int year, int idPagamento,
        java.lang.Integer state) {
        return _schedaPagamentoLocalService.findByCustomerYearIdPaymentAndState(partnerCode,
            year, idPagamento, state);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public SchedaPagamentoLocalService getWrappedSchedaPagamentoLocalService() {
        return _schedaPagamentoLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedSchedaPagamentoLocalService(
        SchedaPagamentoLocalService schedaPagamentoLocalService) {
        _schedaPagamentoLocalService = schedaPagamentoLocalService;
    }

    @Override
    public SchedaPagamentoLocalService getWrappedService() {
        return _schedaPagamentoLocalService;
    }

    @Override
    public void setWrappedService(
        SchedaPagamentoLocalService schedaPagamentoLocalService) {
        _schedaPagamentoLocalService = schedaPagamentoLocalService;
    }
}
