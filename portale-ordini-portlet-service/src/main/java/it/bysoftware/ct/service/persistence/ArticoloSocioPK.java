package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class ArticoloSocioPK implements Comparable<ArticoloSocioPK>,
    Serializable {
    public String associato;
    public String codiceArticoloSocio;
    public String codiceVarianteSocio;

    public ArticoloSocioPK() {
    }

    public ArticoloSocioPK(String associato, String codiceArticoloSocio,
        String codiceVarianteSocio) {
        this.associato = associato;
        this.codiceArticoloSocio = codiceArticoloSocio;
        this.codiceVarianteSocio = codiceVarianteSocio;
    }

    public String getAssociato() {
        return associato;
    }

    public void setAssociato(String associato) {
        this.associato = associato;
    }

    public String getCodiceArticoloSocio() {
        return codiceArticoloSocio;
    }

    public void setCodiceArticoloSocio(String codiceArticoloSocio) {
        this.codiceArticoloSocio = codiceArticoloSocio;
    }

    public String getCodiceVarianteSocio() {
        return codiceVarianteSocio;
    }

    public void setCodiceVarianteSocio(String codiceVarianteSocio) {
        this.codiceVarianteSocio = codiceVarianteSocio;
    }

    @Override
    public int compareTo(ArticoloSocioPK pk) {
        if (pk == null) {
            return -1;
        }

        int value = 0;

        value = associato.compareTo(pk.associato);

        if (value != 0) {
            return value;
        }

        value = codiceArticoloSocio.compareTo(pk.codiceArticoloSocio);

        if (value != 0) {
            return value;
        }

        value = codiceVarianteSocio.compareTo(pk.codiceVarianteSocio);

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ArticoloSocioPK)) {
            return false;
        }

        ArticoloSocioPK pk = (ArticoloSocioPK) obj;

        if ((associato.equals(pk.associato)) &&
                (codiceArticoloSocio.equals(pk.codiceArticoloSocio)) &&
                (codiceVarianteSocio.equals(pk.codiceVarianteSocio))) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (String.valueOf(associato) +
        String.valueOf(codiceArticoloSocio) +
        String.valueOf(codiceVarianteSocio)).hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(15);

        sb.append(StringPool.OPEN_CURLY_BRACE);

        sb.append("associato");
        sb.append(StringPool.EQUAL);
        sb.append(associato);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("codiceArticoloSocio");
        sb.append(StringPool.EQUAL);
        sb.append(codiceArticoloSocio);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("codiceVarianteSocio");
        sb.append(StringPool.EQUAL);
        sb.append(codiceVarianteSocio);

        sb.append(StringPool.CLOSE_CURLY_BRACE);

        return sb.toString();
    }
}
