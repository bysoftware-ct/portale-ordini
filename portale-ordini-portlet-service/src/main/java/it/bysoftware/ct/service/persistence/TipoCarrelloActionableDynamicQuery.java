package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.TipoCarrello;
import it.bysoftware.ct.service.TipoCarrelloLocalServiceUtil;

/**
 * @author Mario Torrisi
 * @generated
 */
public abstract class TipoCarrelloActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public TipoCarrelloActionableDynamicQuery() throws SystemException {
        setBaseLocalService(TipoCarrelloLocalServiceUtil.getService());
        setClass(TipoCarrello.class);

        setClassLoader(it.bysoftware.ct.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
