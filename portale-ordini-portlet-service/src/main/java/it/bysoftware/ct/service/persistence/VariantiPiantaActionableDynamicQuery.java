package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.VariantiPianta;
import it.bysoftware.ct.service.VariantiPiantaLocalServiceUtil;

/**
 * @author Mario Torrisi
 * @generated
 */
public abstract class VariantiPiantaActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public VariantiPiantaActionableDynamicQuery() throws SystemException {
        setBaseLocalService(VariantiPiantaLocalServiceUtil.getService());
        setClass(VariantiPianta.class);

        setClassLoader(it.bysoftware.ct.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
