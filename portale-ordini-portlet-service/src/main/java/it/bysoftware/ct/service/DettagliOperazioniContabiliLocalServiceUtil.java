package it.bysoftware.ct.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for DettagliOperazioniContabili. This utility wraps
 * {@link it.bysoftware.ct.service.impl.DettagliOperazioniContabiliLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Mario Torrisi
 * @see DettagliOperazioniContabiliLocalService
 * @see it.bysoftware.ct.service.base.DettagliOperazioniContabiliLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.impl.DettagliOperazioniContabiliLocalServiceImpl
 * @generated
 */
public class DettagliOperazioniContabiliLocalServiceUtil {
    private static DettagliOperazioniContabiliLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link it.bysoftware.ct.service.impl.DettagliOperazioniContabiliLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the dettagli operazioni contabili to the database. Also notifies the appropriate model listeners.
    *
    * @param dettagliOperazioniContabili the dettagli operazioni contabili
    * @return the dettagli operazioni contabili that was added
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DettagliOperazioniContabili addDettagliOperazioniContabili(
        it.bysoftware.ct.model.DettagliOperazioniContabili dettagliOperazioniContabili)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addDettagliOperazioniContabili(dettagliOperazioniContabili);
    }

    /**
    * Creates a new dettagli operazioni contabili with the primary key. Does not add the dettagli operazioni contabili to the database.
    *
    * @param dettagliOperazioniContabiliPK the primary key for the new dettagli operazioni contabili
    * @return the new dettagli operazioni contabili
    */
    public static it.bysoftware.ct.model.DettagliOperazioniContabili createDettagliOperazioniContabili(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK) {
        return getService()
                   .createDettagliOperazioniContabili(dettagliOperazioniContabiliPK);
    }

    /**
    * Deletes the dettagli operazioni contabili with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param dettagliOperazioniContabiliPK the primary key of the dettagli operazioni contabili
    * @return the dettagli operazioni contabili that was removed
    * @throws PortalException if a dettagli operazioni contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DettagliOperazioniContabili deleteDettagliOperazioniContabili(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .deleteDettagliOperazioniContabili(dettagliOperazioniContabiliPK);
    }

    /**
    * Deletes the dettagli operazioni contabili from the database. Also notifies the appropriate model listeners.
    *
    * @param dettagliOperazioniContabili the dettagli operazioni contabili
    * @return the dettagli operazioni contabili that was removed
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DettagliOperazioniContabili deleteDettagliOperazioniContabili(
        it.bysoftware.ct.model.DettagliOperazioniContabili dettagliOperazioniContabili)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .deleteDettagliOperazioniContabili(dettagliOperazioniContabili);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static it.bysoftware.ct.model.DettagliOperazioniContabili fetchDettagliOperazioniContabili(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .fetchDettagliOperazioniContabili(dettagliOperazioniContabiliPK);
    }

    /**
    * Returns the dettagli operazioni contabili with the primary key.
    *
    * @param dettagliOperazioniContabiliPK the primary key of the dettagli operazioni contabili
    * @return the dettagli operazioni contabili
    * @throws PortalException if a dettagli operazioni contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DettagliOperazioniContabili getDettagliOperazioniContabili(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .getDettagliOperazioniContabili(dettagliOperazioniContabiliPK);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the dettagli operazioni contabilis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of dettagli operazioni contabilis
    * @param end the upper bound of the range of dettagli operazioni contabilis (not inclusive)
    * @return the range of dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.DettagliOperazioniContabili> getDettagliOperazioniContabilis(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getDettagliOperazioniContabilis(start, end);
    }

    /**
    * Returns the number of dettagli operazioni contabilis.
    *
    * @return the number of dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    public static int getDettagliOperazioniContabilisCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getDettagliOperazioniContabilisCount();
    }

    /**
    * Updates the dettagli operazioni contabili in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param dettagliOperazioniContabili the dettagli operazioni contabili
    * @return the dettagli operazioni contabili that was updated
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DettagliOperazioniContabili updateDettagliOperazioniContabili(
        it.bysoftware.ct.model.DettagliOperazioniContabili dettagliOperazioniContabili)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateDettagliOperazioniContabili(dettagliOperazioniContabili);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.util.List<it.bysoftware.ct.model.DettagliOperazioniContabili> findByAccountingOperation(
        java.lang.String code) {
        return getService().findByAccountingOperation(code);
    }

    public static void clearService() {
        _service = null;
    }

    public static DettagliOperazioniContabiliLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    DettagliOperazioniContabiliLocalService.class.getName());

            if (invokableLocalService instanceof DettagliOperazioniContabiliLocalService) {
                _service = (DettagliOperazioniContabiliLocalService) invokableLocalService;
            } else {
                _service = new DettagliOperazioniContabiliLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(DettagliOperazioniContabiliLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(DettagliOperazioniContabiliLocalService service) {
    }
}
