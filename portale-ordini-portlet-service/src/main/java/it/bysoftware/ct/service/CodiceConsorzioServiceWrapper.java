package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CodiceConsorzioService}.
 *
 * @author Mario Torrisi
 * @see CodiceConsorzioService
 * @generated
 */
public class CodiceConsorzioServiceWrapper implements CodiceConsorzioService,
    ServiceWrapper<CodiceConsorzioService> {
    private CodiceConsorzioService _codiceConsorzioService;

    public CodiceConsorzioServiceWrapper(
        CodiceConsorzioService codiceConsorzioService) {
        _codiceConsorzioService = codiceConsorzioService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _codiceConsorzioService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _codiceConsorzioService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _codiceConsorzioService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public CodiceConsorzioService getWrappedCodiceConsorzioService() {
        return _codiceConsorzioService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedCodiceConsorzioService(
        CodiceConsorzioService codiceConsorzioService) {
        _codiceConsorzioService = codiceConsorzioService;
    }

    @Override
    public CodiceConsorzioService getWrappedService() {
        return _codiceConsorzioService;
    }

    @Override
    public void setWrappedService(CodiceConsorzioService codiceConsorzioService) {
        _codiceConsorzioService = codiceConsorzioService;
    }
}
