package it.bysoftware.ct.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import it.bysoftware.ct.model.AnagraficheClientiFornitoriClp;
import it.bysoftware.ct.model.ArticoliClp;
import it.bysoftware.ct.model.ArticoloSocioClp;
import it.bysoftware.ct.model.CarrelloClp;
import it.bysoftware.ct.model.CategorieClp;
import it.bysoftware.ct.model.CategorieMerceologicheClp;
import it.bysoftware.ct.model.CausaliContabiliClp;
import it.bysoftware.ct.model.CausaliEstrattoContoClp;
import it.bysoftware.ct.model.CentroClp;
import it.bysoftware.ct.model.ClientiFornitoriDatiAggClp;
import it.bysoftware.ct.model.CodiceConsorzioClp;
import it.bysoftware.ct.model.ContatoreSocioClp;
import it.bysoftware.ct.model.DescrizioniVariantiClp;
import it.bysoftware.ct.model.DettagliOperazioniContabiliClp;
import it.bysoftware.ct.model.MovimentoVuotoClp;
import it.bysoftware.ct.model.OrdineClp;
import it.bysoftware.ct.model.PagamentoClp;
import it.bysoftware.ct.model.PianoPagamentiClp;
import it.bysoftware.ct.model.PianteClp;
import it.bysoftware.ct.model.RigaClp;
import it.bysoftware.ct.model.RigheFattureVeditaClp;
import it.bysoftware.ct.model.RigoDocumentoClp;
import it.bysoftware.ct.model.RubricaClp;
import it.bysoftware.ct.model.ScadenzePartiteClp;
import it.bysoftware.ct.model.SchedaPagamentoClp;
import it.bysoftware.ct.model.SottocontiClp;
import it.bysoftware.ct.model.SpettanzeSociClp;
import it.bysoftware.ct.model.StatiClp;
import it.bysoftware.ct.model.TestataDocumentoClp;
import it.bysoftware.ct.model.TestataFattureClientiClp;
import it.bysoftware.ct.model.TipoCarrelloClp;
import it.bysoftware.ct.model.VariantiClp;
import it.bysoftware.ct.model.VettoriClp;
import it.bysoftware.ct.model.VociIvaClp;
import it.bysoftware.ct.model.VuotoClp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;


public class ClpSerializer {
    private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
    private static String _servletContextName;
    private static boolean _useReflectionToTranslateThrowable = true;

    public static String getServletContextName() {
        if (Validator.isNotNull(_servletContextName)) {
            return _servletContextName;
        }

        synchronized (ClpSerializer.class) {
            if (Validator.isNotNull(_servletContextName)) {
                return _servletContextName;
            }

            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Class<?> portletPropsClass = classLoader.loadClass(
                        "com.liferay.util.portlet.PortletProps");

                Method getMethod = portletPropsClass.getMethod("get",
                        new Class<?>[] { String.class });

                String portletPropsServletContextName = (String) getMethod.invoke(null,
                        "portale-ordini-portlet-deployment-context");

                if (Validator.isNotNull(portletPropsServletContextName)) {
                    _servletContextName = portletPropsServletContextName;
                }
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info(
                        "Unable to locate deployment context from portlet properties");
                }
            }

            if (Validator.isNull(_servletContextName)) {
                try {
                    String propsUtilServletContextName = PropsUtil.get(
                            "portale-ordini-portlet-deployment-context");

                    if (Validator.isNotNull(propsUtilServletContextName)) {
                        _servletContextName = propsUtilServletContextName;
                    }
                } catch (Throwable t) {
                    if (_log.isInfoEnabled()) {
                        _log.info(
                            "Unable to locate deployment context from portal properties");
                    }
                }
            }

            if (Validator.isNull(_servletContextName)) {
                _servletContextName = "portale-ordini-portlet";
            }

            return _servletContextName;
        }
    }

    public static Object translateInput(BaseModel<?> oldModel) {
        Class<?> oldModelClass = oldModel.getClass();

        String oldModelClassName = oldModelClass.getName();

        if (oldModelClassName.equals(
                    AnagraficheClientiFornitoriClp.class.getName())) {
            return translateInputAnagraficheClientiFornitori(oldModel);
        }

        if (oldModelClassName.equals(ArticoliClp.class.getName())) {
            return translateInputArticoli(oldModel);
        }

        if (oldModelClassName.equals(ArticoloSocioClp.class.getName())) {
            return translateInputArticoloSocio(oldModel);
        }

        if (oldModelClassName.equals(CarrelloClp.class.getName())) {
            return translateInputCarrello(oldModel);
        }

        if (oldModelClassName.equals(CategorieClp.class.getName())) {
            return translateInputCategorie(oldModel);
        }

        if (oldModelClassName.equals(CategorieMerceologicheClp.class.getName())) {
            return translateInputCategorieMerceologiche(oldModel);
        }

        if (oldModelClassName.equals(CausaliContabiliClp.class.getName())) {
            return translateInputCausaliContabili(oldModel);
        }

        if (oldModelClassName.equals(CausaliEstrattoContoClp.class.getName())) {
            return translateInputCausaliEstrattoConto(oldModel);
        }

        if (oldModelClassName.equals(CentroClp.class.getName())) {
            return translateInputCentro(oldModel);
        }

        if (oldModelClassName.equals(ClientiFornitoriDatiAggClp.class.getName())) {
            return translateInputClientiFornitoriDatiAgg(oldModel);
        }

        if (oldModelClassName.equals(CodiceConsorzioClp.class.getName())) {
            return translateInputCodiceConsorzio(oldModel);
        }

        if (oldModelClassName.equals(ContatoreSocioClp.class.getName())) {
            return translateInputContatoreSocio(oldModel);
        }

        if (oldModelClassName.equals(DescrizioniVariantiClp.class.getName())) {
            return translateInputDescrizioniVarianti(oldModel);
        }

        if (oldModelClassName.equals(
                    DettagliOperazioniContabiliClp.class.getName())) {
            return translateInputDettagliOperazioniContabili(oldModel);
        }

        if (oldModelClassName.equals(MovimentoVuotoClp.class.getName())) {
            return translateInputMovimentoVuoto(oldModel);
        }

        if (oldModelClassName.equals(OrdineClp.class.getName())) {
            return translateInputOrdine(oldModel);
        }

        if (oldModelClassName.equals(PagamentoClp.class.getName())) {
            return translateInputPagamento(oldModel);
        }

        if (oldModelClassName.equals(PianoPagamentiClp.class.getName())) {
            return translateInputPianoPagamenti(oldModel);
        }

        if (oldModelClassName.equals(PianteClp.class.getName())) {
            return translateInputPiante(oldModel);
        }

        if (oldModelClassName.equals(RigaClp.class.getName())) {
            return translateInputRiga(oldModel);
        }

        if (oldModelClassName.equals(RigheFattureVeditaClp.class.getName())) {
            return translateInputRigheFattureVedita(oldModel);
        }

        if (oldModelClassName.equals(RigoDocumentoClp.class.getName())) {
            return translateInputRigoDocumento(oldModel);
        }

        if (oldModelClassName.equals(RubricaClp.class.getName())) {
            return translateInputRubrica(oldModel);
        }

        if (oldModelClassName.equals(ScadenzePartiteClp.class.getName())) {
            return translateInputScadenzePartite(oldModel);
        }

        if (oldModelClassName.equals(SchedaPagamentoClp.class.getName())) {
            return translateInputSchedaPagamento(oldModel);
        }

        if (oldModelClassName.equals(SottocontiClp.class.getName())) {
            return translateInputSottoconti(oldModel);
        }

        if (oldModelClassName.equals(SpettanzeSociClp.class.getName())) {
            return translateInputSpettanzeSoci(oldModel);
        }

        if (oldModelClassName.equals(StatiClp.class.getName())) {
            return translateInputStati(oldModel);
        }

        if (oldModelClassName.equals(TestataDocumentoClp.class.getName())) {
            return translateInputTestataDocumento(oldModel);
        }

        if (oldModelClassName.equals(TestataFattureClientiClp.class.getName())) {
            return translateInputTestataFattureClienti(oldModel);
        }

        if (oldModelClassName.equals(TipoCarrelloClp.class.getName())) {
            return translateInputTipoCarrello(oldModel);
        }

        if (oldModelClassName.equals(VariantiClp.class.getName())) {
            return translateInputVarianti(oldModel);
        }

        if (oldModelClassName.equals(VettoriClp.class.getName())) {
            return translateInputVettori(oldModel);
        }

        if (oldModelClassName.equals(VociIvaClp.class.getName())) {
            return translateInputVociIva(oldModel);
        }

        if (oldModelClassName.equals(VuotoClp.class.getName())) {
            return translateInputVuoto(oldModel);
        }

        return oldModel;
    }

    public static Object translateInput(List<Object> oldList) {
        List<Object> newList = new ArrayList<Object>(oldList.size());

        for (int i = 0; i < oldList.size(); i++) {
            Object curObj = oldList.get(i);

            newList.add(translateInput(curObj));
        }

        return newList;
    }

    public static Object translateInputAnagraficheClientiFornitori(
        BaseModel<?> oldModel) {
        AnagraficheClientiFornitoriClp oldClpModel = (AnagraficheClientiFornitoriClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getAnagraficheClientiFornitoriRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputArticoli(BaseModel<?> oldModel) {
        ArticoliClp oldClpModel = (ArticoliClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getArticoliRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputArticoloSocio(BaseModel<?> oldModel) {
        ArticoloSocioClp oldClpModel = (ArticoloSocioClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getArticoloSocioRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputCarrello(BaseModel<?> oldModel) {
        CarrelloClp oldClpModel = (CarrelloClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getCarrelloRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputCategorie(BaseModel<?> oldModel) {
        CategorieClp oldClpModel = (CategorieClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getCategorieRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputCategorieMerceologiche(
        BaseModel<?> oldModel) {
        CategorieMerceologicheClp oldClpModel = (CategorieMerceologicheClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getCategorieMerceologicheRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputCausaliContabili(BaseModel<?> oldModel) {
        CausaliContabiliClp oldClpModel = (CausaliContabiliClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getCausaliContabiliRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputCausaliEstrattoConto(
        BaseModel<?> oldModel) {
        CausaliEstrattoContoClp oldClpModel = (CausaliEstrattoContoClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getCausaliEstrattoContoRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputCentro(BaseModel<?> oldModel) {
        CentroClp oldClpModel = (CentroClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getCentroRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputClientiFornitoriDatiAgg(
        BaseModel<?> oldModel) {
        ClientiFornitoriDatiAggClp oldClpModel = (ClientiFornitoriDatiAggClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getClientiFornitoriDatiAggRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputCodiceConsorzio(BaseModel<?> oldModel) {
        CodiceConsorzioClp oldClpModel = (CodiceConsorzioClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getCodiceConsorzioRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputContatoreSocio(BaseModel<?> oldModel) {
        ContatoreSocioClp oldClpModel = (ContatoreSocioClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getContatoreSocioRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputDescrizioniVarianti(
        BaseModel<?> oldModel) {
        DescrizioniVariantiClp oldClpModel = (DescrizioniVariantiClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getDescrizioniVariantiRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputDettagliOperazioniContabili(
        BaseModel<?> oldModel) {
        DettagliOperazioniContabiliClp oldClpModel = (DettagliOperazioniContabiliClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getDettagliOperazioniContabiliRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputMovimentoVuoto(BaseModel<?> oldModel) {
        MovimentoVuotoClp oldClpModel = (MovimentoVuotoClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getMovimentoVuotoRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputOrdine(BaseModel<?> oldModel) {
        OrdineClp oldClpModel = (OrdineClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getOrdineRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputPagamento(BaseModel<?> oldModel) {
        PagamentoClp oldClpModel = (PagamentoClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getPagamentoRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputPianoPagamenti(BaseModel<?> oldModel) {
        PianoPagamentiClp oldClpModel = (PianoPagamentiClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getPianoPagamentiRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputPiante(BaseModel<?> oldModel) {
        PianteClp oldClpModel = (PianteClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getPianteRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputRiga(BaseModel<?> oldModel) {
        RigaClp oldClpModel = (RigaClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getRigaRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputRigheFattureVedita(BaseModel<?> oldModel) {
        RigheFattureVeditaClp oldClpModel = (RigheFattureVeditaClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getRigheFattureVeditaRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputRigoDocumento(BaseModel<?> oldModel) {
        RigoDocumentoClp oldClpModel = (RigoDocumentoClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getRigoDocumentoRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputRubrica(BaseModel<?> oldModel) {
        RubricaClp oldClpModel = (RubricaClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getRubricaRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputScadenzePartite(BaseModel<?> oldModel) {
        ScadenzePartiteClp oldClpModel = (ScadenzePartiteClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getScadenzePartiteRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputSchedaPagamento(BaseModel<?> oldModel) {
        SchedaPagamentoClp oldClpModel = (SchedaPagamentoClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getSchedaPagamentoRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputSottoconti(BaseModel<?> oldModel) {
        SottocontiClp oldClpModel = (SottocontiClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getSottocontiRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputSpettanzeSoci(BaseModel<?> oldModel) {
        SpettanzeSociClp oldClpModel = (SpettanzeSociClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getSpettanzeSociRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputStati(BaseModel<?> oldModel) {
        StatiClp oldClpModel = (StatiClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getStatiRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputTestataDocumento(BaseModel<?> oldModel) {
        TestataDocumentoClp oldClpModel = (TestataDocumentoClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getTestataDocumentoRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputTestataFattureClienti(
        BaseModel<?> oldModel) {
        TestataFattureClientiClp oldClpModel = (TestataFattureClientiClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getTestataFattureClientiRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputTipoCarrello(BaseModel<?> oldModel) {
        TipoCarrelloClp oldClpModel = (TipoCarrelloClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getTipoCarrelloRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputVarianti(BaseModel<?> oldModel) {
        VariantiClp oldClpModel = (VariantiClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getVariantiRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputVettori(BaseModel<?> oldModel) {
        VettoriClp oldClpModel = (VettoriClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getVettoriRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputVociIva(BaseModel<?> oldModel) {
        VociIvaClp oldClpModel = (VociIvaClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getVociIvaRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputVuoto(BaseModel<?> oldModel) {
        VuotoClp oldClpModel = (VuotoClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getVuotoRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInput(Object obj) {
        if (obj instanceof BaseModel<?>) {
            return translateInput((BaseModel<?>) obj);
        } else if (obj instanceof List<?>) {
            return translateInput((List<Object>) obj);
        } else {
            return obj;
        }
    }

    public static Object translateOutput(BaseModel<?> oldModel) {
        Class<?> oldModelClass = oldModel.getClass();

        String oldModelClassName = oldModelClass.getName();

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.AnagraficheClientiFornitoriImpl")) {
            return translateOutputAnagraficheClientiFornitori(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals("it.bysoftware.ct.model.impl.ArticoliImpl")) {
            return translateOutputArticoli(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.ArticoloSocioImpl")) {
            return translateOutputArticoloSocio(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals("it.bysoftware.ct.model.impl.CarrelloImpl")) {
            return translateOutputCarrello(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.CategorieImpl")) {
            return translateOutputCategorie(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.CategorieMerceologicheImpl")) {
            return translateOutputCategorieMerceologiche(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.CausaliContabiliImpl")) {
            return translateOutputCausaliContabili(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.CausaliEstrattoContoImpl")) {
            return translateOutputCausaliEstrattoConto(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals("it.bysoftware.ct.model.impl.CentroImpl")) {
            return translateOutputCentro(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggImpl")) {
            return translateOutputClientiFornitoriDatiAgg(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.CodiceConsorzioImpl")) {
            return translateOutputCodiceConsorzio(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.ContatoreSocioImpl")) {
            return translateOutputContatoreSocio(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.DescrizioniVariantiImpl")) {
            return translateOutputDescrizioniVarianti(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.DettagliOperazioniContabiliImpl")) {
            return translateOutputDettagliOperazioniContabili(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.MovimentoVuotoImpl")) {
            return translateOutputMovimentoVuoto(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals("it.bysoftware.ct.model.impl.OrdineImpl")) {
            return translateOutputOrdine(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.PagamentoImpl")) {
            return translateOutputPagamento(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.PianoPagamentiImpl")) {
            return translateOutputPianoPagamenti(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals("it.bysoftware.ct.model.impl.PianteImpl")) {
            return translateOutputPiante(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals("it.bysoftware.ct.model.impl.RigaImpl")) {
            return translateOutputRiga(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.RigheFattureVeditaImpl")) {
            return translateOutputRigheFattureVedita(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.RigoDocumentoImpl")) {
            return translateOutputRigoDocumento(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals("it.bysoftware.ct.model.impl.RubricaImpl")) {
            return translateOutputRubrica(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.ScadenzePartiteImpl")) {
            return translateOutputScadenzePartite(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.SchedaPagamentoImpl")) {
            return translateOutputSchedaPagamento(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.SottocontiImpl")) {
            return translateOutputSottoconti(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.SpettanzeSociImpl")) {
            return translateOutputSpettanzeSoci(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals("it.bysoftware.ct.model.impl.StatiImpl")) {
            return translateOutputStati(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.TestataDocumentoImpl")) {
            return translateOutputTestataDocumento(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.TestataFattureClientiImpl")) {
            return translateOutputTestataFattureClienti(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "it.bysoftware.ct.model.impl.TipoCarrelloImpl")) {
            return translateOutputTipoCarrello(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals("it.bysoftware.ct.model.impl.VariantiImpl")) {
            return translateOutputVarianti(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals("it.bysoftware.ct.model.impl.VettoriImpl")) {
            return translateOutputVettori(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals("it.bysoftware.ct.model.impl.VociIvaImpl")) {
            return translateOutputVociIva(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals("it.bysoftware.ct.model.impl.VuotoImpl")) {
            return translateOutputVuoto(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        return oldModel;
    }

    public static Object translateOutput(List<Object> oldList) {
        List<Object> newList = new ArrayList<Object>(oldList.size());

        for (int i = 0; i < oldList.size(); i++) {
            Object curObj = oldList.get(i);

            newList.add(translateOutput(curObj));
        }

        return newList;
    }

    public static Object translateOutput(Object obj) {
        if (obj instanceof BaseModel<?>) {
            return translateOutput((BaseModel<?>) obj);
        } else if (obj instanceof List<?>) {
            return translateOutput((List<Object>) obj);
        } else {
            return obj;
        }
    }

    public static Throwable translateThrowable(Throwable throwable) {
        if (_useReflectionToTranslateThrowable) {
            try {
                UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

                objectOutputStream.writeObject(throwable);

                objectOutputStream.flush();
                objectOutputStream.close();

                UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
                        0, unsyncByteArrayOutputStream.size());

                Thread currentThread = Thread.currentThread();

                ClassLoader contextClassLoader = currentThread.getContextClassLoader();

                ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
                        contextClassLoader);

                throwable = (Throwable) objectInputStream.readObject();

                objectInputStream.close();

                return throwable;
            } catch (SecurityException se) {
                if (_log.isInfoEnabled()) {
                    _log.info("Do not use reflection to translate throwable");
                }

                _useReflectionToTranslateThrowable = false;
            } catch (Throwable throwable2) {
                _log.error(throwable2, throwable2);

                return throwable2;
            }
        }

        Class<?> clazz = throwable.getClass();

        String className = clazz.getName();

        if (className.equals(PortalException.class.getName())) {
            return new PortalException();
        }

        if (className.equals(SystemException.class.getName())) {
            return new SystemException();
        }

        if (className.equals(
                    "it.bysoftware.ct.NoSuchAnagraficheClientiFornitoriException")) {
            return new it.bysoftware.ct.NoSuchAnagraficheClientiFornitoriException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchArticoliException")) {
            return new it.bysoftware.ct.NoSuchArticoliException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchArticoloSocioException")) {
            return new it.bysoftware.ct.NoSuchArticoloSocioException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchCarrelloException")) {
            return new it.bysoftware.ct.NoSuchCarrelloException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchCategorieException")) {
            return new it.bysoftware.ct.NoSuchCategorieException();
        }

        if (className.equals(
                    "it.bysoftware.ct.NoSuchCategorieMerceologicheException")) {
            return new it.bysoftware.ct.NoSuchCategorieMerceologicheException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchCausaliContabiliException")) {
            return new it.bysoftware.ct.NoSuchCausaliContabiliException();
        }

        if (className.equals(
                    "it.bysoftware.ct.NoSuchCausaliEstrattoContoException")) {
            return new it.bysoftware.ct.NoSuchCausaliEstrattoContoException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchCentroException")) {
            return new it.bysoftware.ct.NoSuchCentroException();
        }

        if (className.equals(
                    "it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException")) {
            return new it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchCodiceConsorzioException")) {
            return new it.bysoftware.ct.NoSuchCodiceConsorzioException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchContatoreSocioException")) {
            return new it.bysoftware.ct.NoSuchContatoreSocioException();
        }

        if (className.equals(
                    "it.bysoftware.ct.NoSuchDescrizioniVariantiException")) {
            return new it.bysoftware.ct.NoSuchDescrizioniVariantiException();
        }

        if (className.equals(
                    "it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException")) {
            return new it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchMovimentoVuotoException")) {
            return new it.bysoftware.ct.NoSuchMovimentoVuotoException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchOrdineException")) {
            return new it.bysoftware.ct.NoSuchOrdineException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchPagamentoException")) {
            return new it.bysoftware.ct.NoSuchPagamentoException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchPianoPagamentiException")) {
            return new it.bysoftware.ct.NoSuchPianoPagamentiException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchPianteException")) {
            return new it.bysoftware.ct.NoSuchPianteException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchRigaException")) {
            return new it.bysoftware.ct.NoSuchRigaException();
        }

        if (className.equals(
                    "it.bysoftware.ct.NoSuchRigheFattureVeditaException")) {
            return new it.bysoftware.ct.NoSuchRigheFattureVeditaException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchRigoDocumentoException")) {
            return new it.bysoftware.ct.NoSuchRigoDocumentoException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchRubricaException")) {
            return new it.bysoftware.ct.NoSuchRubricaException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchScadenzePartiteException")) {
            return new it.bysoftware.ct.NoSuchScadenzePartiteException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchSchedaPagamentoException")) {
            return new it.bysoftware.ct.NoSuchSchedaPagamentoException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchSottocontiException")) {
            return new it.bysoftware.ct.NoSuchSottocontiException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchSpettanzeSociException")) {
            return new it.bysoftware.ct.NoSuchSpettanzeSociException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchStatiException")) {
            return new it.bysoftware.ct.NoSuchStatiException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchTestataDocumentoException")) {
            return new it.bysoftware.ct.NoSuchTestataDocumentoException();
        }

        if (className.equals(
                    "it.bysoftware.ct.NoSuchTestataFattureClientiException")) {
            return new it.bysoftware.ct.NoSuchTestataFattureClientiException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchTipoCarrelloException")) {
            return new it.bysoftware.ct.NoSuchTipoCarrelloException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchVariantiException")) {
            return new it.bysoftware.ct.NoSuchVariantiException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchVettoriException")) {
            return new it.bysoftware.ct.NoSuchVettoriException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchVociIvaException")) {
            return new it.bysoftware.ct.NoSuchVociIvaException();
        }

        if (className.equals("it.bysoftware.ct.NoSuchVuotoException")) {
            return new it.bysoftware.ct.NoSuchVuotoException();
        }

        return throwable;
    }

    public static Object translateOutputAnagraficheClientiFornitori(
        BaseModel<?> oldModel) {
        AnagraficheClientiFornitoriClp newModel = new AnagraficheClientiFornitoriClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setAnagraficheClientiFornitoriRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputArticoli(BaseModel<?> oldModel) {
        ArticoliClp newModel = new ArticoliClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setArticoliRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputArticoloSocio(BaseModel<?> oldModel) {
        ArticoloSocioClp newModel = new ArticoloSocioClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setArticoloSocioRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputCarrello(BaseModel<?> oldModel) {
        CarrelloClp newModel = new CarrelloClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setCarrelloRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputCategorie(BaseModel<?> oldModel) {
        CategorieClp newModel = new CategorieClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setCategorieRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputCategorieMerceologiche(
        BaseModel<?> oldModel) {
        CategorieMerceologicheClp newModel = new CategorieMerceologicheClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setCategorieMerceologicheRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputCausaliContabili(BaseModel<?> oldModel) {
        CausaliContabiliClp newModel = new CausaliContabiliClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setCausaliContabiliRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputCausaliEstrattoConto(
        BaseModel<?> oldModel) {
        CausaliEstrattoContoClp newModel = new CausaliEstrattoContoClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setCausaliEstrattoContoRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputCentro(BaseModel<?> oldModel) {
        CentroClp newModel = new CentroClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setCentroRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputClientiFornitoriDatiAgg(
        BaseModel<?> oldModel) {
        ClientiFornitoriDatiAggClp newModel = new ClientiFornitoriDatiAggClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setClientiFornitoriDatiAggRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputCodiceConsorzio(BaseModel<?> oldModel) {
        CodiceConsorzioClp newModel = new CodiceConsorzioClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setCodiceConsorzioRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputContatoreSocio(BaseModel<?> oldModel) {
        ContatoreSocioClp newModel = new ContatoreSocioClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setContatoreSocioRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputDescrizioniVarianti(
        BaseModel<?> oldModel) {
        DescrizioniVariantiClp newModel = new DescrizioniVariantiClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setDescrizioniVariantiRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputDettagliOperazioniContabili(
        BaseModel<?> oldModel) {
        DettagliOperazioniContabiliClp newModel = new DettagliOperazioniContabiliClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setDettagliOperazioniContabiliRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputMovimentoVuoto(BaseModel<?> oldModel) {
        MovimentoVuotoClp newModel = new MovimentoVuotoClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setMovimentoVuotoRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputOrdine(BaseModel<?> oldModel) {
        OrdineClp newModel = new OrdineClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setOrdineRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputPagamento(BaseModel<?> oldModel) {
        PagamentoClp newModel = new PagamentoClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setPagamentoRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputPianoPagamenti(BaseModel<?> oldModel) {
        PianoPagamentiClp newModel = new PianoPagamentiClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setPianoPagamentiRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputPiante(BaseModel<?> oldModel) {
        PianteClp newModel = new PianteClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setPianteRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputRiga(BaseModel<?> oldModel) {
        RigaClp newModel = new RigaClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setRigaRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputRigheFattureVedita(
        BaseModel<?> oldModel) {
        RigheFattureVeditaClp newModel = new RigheFattureVeditaClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setRigheFattureVeditaRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputRigoDocumento(BaseModel<?> oldModel) {
        RigoDocumentoClp newModel = new RigoDocumentoClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setRigoDocumentoRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputRubrica(BaseModel<?> oldModel) {
        RubricaClp newModel = new RubricaClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setRubricaRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputScadenzePartite(BaseModel<?> oldModel) {
        ScadenzePartiteClp newModel = new ScadenzePartiteClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setScadenzePartiteRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputSchedaPagamento(BaseModel<?> oldModel) {
        SchedaPagamentoClp newModel = new SchedaPagamentoClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setSchedaPagamentoRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputSottoconti(BaseModel<?> oldModel) {
        SottocontiClp newModel = new SottocontiClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setSottocontiRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputSpettanzeSoci(BaseModel<?> oldModel) {
        SpettanzeSociClp newModel = new SpettanzeSociClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setSpettanzeSociRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputStati(BaseModel<?> oldModel) {
        StatiClp newModel = new StatiClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setStatiRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputTestataDocumento(BaseModel<?> oldModel) {
        TestataDocumentoClp newModel = new TestataDocumentoClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setTestataDocumentoRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputTestataFattureClienti(
        BaseModel<?> oldModel) {
        TestataFattureClientiClp newModel = new TestataFattureClientiClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setTestataFattureClientiRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputTipoCarrello(BaseModel<?> oldModel) {
        TipoCarrelloClp newModel = new TipoCarrelloClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setTipoCarrelloRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputVarianti(BaseModel<?> oldModel) {
        VariantiClp newModel = new VariantiClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setVariantiRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputVettori(BaseModel<?> oldModel) {
        VettoriClp newModel = new VettoriClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setVettoriRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputVociIva(BaseModel<?> oldModel) {
        VociIvaClp newModel = new VociIvaClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setVociIvaRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputVuoto(BaseModel<?> oldModel) {
        VuotoClp newModel = new VuotoClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setVuotoRemoteModel(oldModel);

        return newModel;
    }
}
