package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class SchedaPagamentoPK implements Comparable<SchedaPagamentoPK>,
    Serializable {
    public int esercizioRegistrazione;
    public String codiceSoggetto;
    public int numeroPartita;
    public int numeroScadenza;
    public boolean tipoSoggetto;

    public SchedaPagamentoPK() {
    }

    public SchedaPagamentoPK(int esercizioRegistrazione, String codiceSoggetto,
        int numeroPartita, int numeroScadenza, boolean tipoSoggetto) {
        this.esercizioRegistrazione = esercizioRegistrazione;
        this.codiceSoggetto = codiceSoggetto;
        this.numeroPartita = numeroPartita;
        this.numeroScadenza = numeroScadenza;
        this.tipoSoggetto = tipoSoggetto;
    }

    public int getEsercizioRegistrazione() {
        return esercizioRegistrazione;
    }

    public void setEsercizioRegistrazione(int esercizioRegistrazione) {
        this.esercizioRegistrazione = esercizioRegistrazione;
    }

    public String getCodiceSoggetto() {
        return codiceSoggetto;
    }

    public void setCodiceSoggetto(String codiceSoggetto) {
        this.codiceSoggetto = codiceSoggetto;
    }

    public int getNumeroPartita() {
        return numeroPartita;
    }

    public void setNumeroPartita(int numeroPartita) {
        this.numeroPartita = numeroPartita;
    }

    public int getNumeroScadenza() {
        return numeroScadenza;
    }

    public void setNumeroScadenza(int numeroScadenza) {
        this.numeroScadenza = numeroScadenza;
    }

    public boolean getTipoSoggetto() {
        return tipoSoggetto;
    }

    public boolean isTipoSoggetto() {
        return tipoSoggetto;
    }

    public void setTipoSoggetto(boolean tipoSoggetto) {
        this.tipoSoggetto = tipoSoggetto;
    }

    @Override
    public int compareTo(SchedaPagamentoPK pk) {
        if (pk == null) {
            return -1;
        }

        int value = 0;

        if (esercizioRegistrazione < pk.esercizioRegistrazione) {
            value = -1;
        } else if (esercizioRegistrazione > pk.esercizioRegistrazione) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        value = codiceSoggetto.compareTo(pk.codiceSoggetto);

        if (value != 0) {
            return value;
        }

        if (numeroPartita < pk.numeroPartita) {
            value = -1;
        } else if (numeroPartita > pk.numeroPartita) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        if (numeroScadenza < pk.numeroScadenza) {
            value = -1;
        } else if (numeroScadenza > pk.numeroScadenza) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        if (!tipoSoggetto && pk.tipoSoggetto) {
            value = -1;
        } else if (tipoSoggetto && !pk.tipoSoggetto) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof SchedaPagamentoPK)) {
            return false;
        }

        SchedaPagamentoPK pk = (SchedaPagamentoPK) obj;

        if ((esercizioRegistrazione == pk.esercizioRegistrazione) &&
                (codiceSoggetto.equals(pk.codiceSoggetto)) &&
                (numeroPartita == pk.numeroPartita) &&
                (numeroScadenza == pk.numeroScadenza) &&
                (tipoSoggetto == pk.tipoSoggetto)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (String.valueOf(esercizioRegistrazione) +
        String.valueOf(codiceSoggetto) + String.valueOf(numeroPartita) +
        String.valueOf(numeroScadenza) + String.valueOf(tipoSoggetto)).hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(25);

        sb.append(StringPool.OPEN_CURLY_BRACE);

        sb.append("esercizioRegistrazione");
        sb.append(StringPool.EQUAL);
        sb.append(esercizioRegistrazione);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("codiceSoggetto");
        sb.append(StringPool.EQUAL);
        sb.append(codiceSoggetto);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("numeroPartita");
        sb.append(StringPool.EQUAL);
        sb.append(numeroPartita);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("numeroScadenza");
        sb.append(StringPool.EQUAL);
        sb.append(numeroScadenza);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("tipoSoggetto");
        sb.append(StringPool.EQUAL);
        sb.append(tipoSoggetto);

        sb.append(StringPool.CLOSE_CURLY_BRACE);

        return sb.toString();
    }
}
