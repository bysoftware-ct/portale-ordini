package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.Categorie;

import java.util.List;

/**
 * The persistence utility for the categorie service. This utility wraps {@link CategoriePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CategoriePersistence
 * @see CategoriePersistenceImpl
 * @generated
 */
public class CategorieUtil {
    private static CategoriePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Categorie categorie) {
        getPersistence().clearCache(categorie);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Categorie> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Categorie> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Categorie> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Categorie update(Categorie categorie)
        throws SystemException {
        return getPersistence().update(categorie);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Categorie update(Categorie categorie,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(categorie, serviceContext);
    }

    /**
    * Caches the categorie in the entity cache if it is enabled.
    *
    * @param categorie the categorie
    */
    public static void cacheResult(it.bysoftware.ct.model.Categorie categorie) {
        getPersistence().cacheResult(categorie);
    }

    /**
    * Caches the categories in the entity cache if it is enabled.
    *
    * @param categories the categories
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.Categorie> categories) {
        getPersistence().cacheResult(categories);
    }

    /**
    * Creates a new categorie with the primary key. Does not add the categorie to the database.
    *
    * @param id the primary key for the new categorie
    * @return the new categorie
    */
    public static it.bysoftware.ct.model.Categorie create(long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the categorie with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the categorie
    * @return the categorie that was removed
    * @throws it.bysoftware.ct.NoSuchCategorieException if a categorie with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Categorie remove(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCategorieException {
        return getPersistence().remove(id);
    }

    public static it.bysoftware.ct.model.Categorie updateImpl(
        it.bysoftware.ct.model.Categorie categorie)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(categorie);
    }

    /**
    * Returns the categorie with the primary key or throws a {@link it.bysoftware.ct.NoSuchCategorieException} if it could not be found.
    *
    * @param id the primary key of the categorie
    * @return the categorie
    * @throws it.bysoftware.ct.NoSuchCategorieException if a categorie with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Categorie findByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCategorieException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the categorie with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the categorie
    * @return the categorie, or <code>null</code> if a categorie with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Categorie fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the categories.
    *
    * @return the categories
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Categorie> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the categories.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CategorieModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of categories
    * @param end the upper bound of the range of categories (not inclusive)
    * @return the range of categories
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Categorie> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the categories.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CategorieModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of categories
    * @param end the upper bound of the range of categories (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of categories
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Categorie> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the categories from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of categories.
    *
    * @return the number of categories
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static CategoriePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (CategoriePersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    CategoriePersistence.class.getName());

            ReferenceRegistry.registerReference(CategorieUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(CategoriePersistence persistence) {
    }
}
