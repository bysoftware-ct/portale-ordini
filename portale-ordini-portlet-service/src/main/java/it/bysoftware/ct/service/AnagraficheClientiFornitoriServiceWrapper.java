package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AnagraficheClientiFornitoriService}.
 *
 * @author Mario Torrisi
 * @see AnagraficheClientiFornitoriService
 * @generated
 */
public class AnagraficheClientiFornitoriServiceWrapper
    implements AnagraficheClientiFornitoriService,
        ServiceWrapper<AnagraficheClientiFornitoriService> {
    private AnagraficheClientiFornitoriService _anagraficheClientiFornitoriService;

    public AnagraficheClientiFornitoriServiceWrapper(
        AnagraficheClientiFornitoriService anagraficheClientiFornitoriService) {
        _anagraficheClientiFornitoriService = anagraficheClientiFornitoriService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _anagraficheClientiFornitoriService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _anagraficheClientiFornitoriService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _anagraficheClientiFornitoriService.invokeMethod(name,
            parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public AnagraficheClientiFornitoriService getWrappedAnagraficheClientiFornitoriService() {
        return _anagraficheClientiFornitoriService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedAnagraficheClientiFornitoriService(
        AnagraficheClientiFornitoriService anagraficheClientiFornitoriService) {
        _anagraficheClientiFornitoriService = anagraficheClientiFornitoriService;
    }

    @Override
    public AnagraficheClientiFornitoriService getWrappedService() {
        return _anagraficheClientiFornitoriService;
    }

    @Override
    public void setWrappedService(
        AnagraficheClientiFornitoriService anagraficheClientiFornitoriService) {
        _anagraficheClientiFornitoriService = anagraficheClientiFornitoriService;
    }
}
