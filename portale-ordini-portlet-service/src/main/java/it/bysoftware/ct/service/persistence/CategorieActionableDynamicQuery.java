package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.Categorie;
import it.bysoftware.ct.service.CategorieLocalServiceUtil;

/**
 * @author Mario Torrisi
 * @generated
 */
public abstract class CategorieActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public CategorieActionableDynamicQuery() throws SystemException {
        setBaseLocalService(CategorieLocalServiceUtil.getService());
        setClass(Categorie.class);

        setClassLoader(it.bysoftware.ct.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
