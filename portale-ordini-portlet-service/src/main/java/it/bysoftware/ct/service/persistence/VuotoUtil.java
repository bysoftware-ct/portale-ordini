package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.Vuoto;

import java.util.List;

/**
 * The persistence utility for the vuoto service. This utility wraps {@link VuotoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see VuotoPersistence
 * @see VuotoPersistenceImpl
 * @generated
 */
public class VuotoUtil {
    private static VuotoPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Vuoto vuoto) {
        getPersistence().clearCache(vuoto);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Vuoto> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Vuoto> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Vuoto> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Vuoto update(Vuoto vuoto) throws SystemException {
        return getPersistence().update(vuoto);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Vuoto update(Vuoto vuoto, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(vuoto, serviceContext);
    }

    /**
    * Caches the vuoto in the entity cache if it is enabled.
    *
    * @param vuoto the vuoto
    */
    public static void cacheResult(it.bysoftware.ct.model.Vuoto vuoto) {
        getPersistence().cacheResult(vuoto);
    }

    /**
    * Caches the vuotos in the entity cache if it is enabled.
    *
    * @param vuotos the vuotos
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.Vuoto> vuotos) {
        getPersistence().cacheResult(vuotos);
    }

    /**
    * Creates a new vuoto with the primary key. Does not add the vuoto to the database.
    *
    * @param codiceVuoto the primary key for the new vuoto
    * @return the new vuoto
    */
    public static it.bysoftware.ct.model.Vuoto create(
        java.lang.String codiceVuoto) {
        return getPersistence().create(codiceVuoto);
    }

    /**
    * Removes the vuoto with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceVuoto the primary key of the vuoto
    * @return the vuoto that was removed
    * @throws it.bysoftware.ct.NoSuchVuotoException if a vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Vuoto remove(
        java.lang.String codiceVuoto)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVuotoException {
        return getPersistence().remove(codiceVuoto);
    }

    public static it.bysoftware.ct.model.Vuoto updateImpl(
        it.bysoftware.ct.model.Vuoto vuoto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(vuoto);
    }

    /**
    * Returns the vuoto with the primary key or throws a {@link it.bysoftware.ct.NoSuchVuotoException} if it could not be found.
    *
    * @param codiceVuoto the primary key of the vuoto
    * @return the vuoto
    * @throws it.bysoftware.ct.NoSuchVuotoException if a vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Vuoto findByPrimaryKey(
        java.lang.String codiceVuoto)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVuotoException {
        return getPersistence().findByPrimaryKey(codiceVuoto);
    }

    /**
    * Returns the vuoto with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param codiceVuoto the primary key of the vuoto
    * @return the vuoto, or <code>null</code> if a vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Vuoto fetchByPrimaryKey(
        java.lang.String codiceVuoto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(codiceVuoto);
    }

    /**
    * Returns all the vuotos.
    *
    * @return the vuotos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Vuoto> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the vuotos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vuotos
    * @param end the upper bound of the range of vuotos (not inclusive)
    * @return the range of vuotos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Vuoto> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the vuotos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vuotos
    * @param end the upper bound of the range of vuotos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of vuotos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Vuoto> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the vuotos from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of vuotos.
    *
    * @return the number of vuotos
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static VuotoPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (VuotoPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    VuotoPersistence.class.getName());

            ReferenceRegistry.registerReference(VuotoUtil.class, "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(VuotoPersistence persistence) {
    }
}
