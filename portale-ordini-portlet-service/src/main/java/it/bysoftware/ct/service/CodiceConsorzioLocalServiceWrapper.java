package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CodiceConsorzioLocalService}.
 *
 * @author Mario Torrisi
 * @see CodiceConsorzioLocalService
 * @generated
 */
public class CodiceConsorzioLocalServiceWrapper
    implements CodiceConsorzioLocalService,
        ServiceWrapper<CodiceConsorzioLocalService> {
    private CodiceConsorzioLocalService _codiceConsorzioLocalService;

    public CodiceConsorzioLocalServiceWrapper(
        CodiceConsorzioLocalService codiceConsorzioLocalService) {
        _codiceConsorzioLocalService = codiceConsorzioLocalService;
    }

    /**
    * Adds the codice consorzio to the database. Also notifies the appropriate model listeners.
    *
    * @param codiceConsorzio the codice consorzio
    * @return the codice consorzio that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.CodiceConsorzio addCodiceConsorzio(
        it.bysoftware.ct.model.CodiceConsorzio codiceConsorzio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _codiceConsorzioLocalService.addCodiceConsorzio(codiceConsorzio);
    }

    /**
    * Creates a new codice consorzio with the primary key. Does not add the codice consorzio to the database.
    *
    * @param codiceSocio the primary key for the new codice consorzio
    * @return the new codice consorzio
    */
    @Override
    public it.bysoftware.ct.model.CodiceConsorzio createCodiceConsorzio(
        java.lang.String codiceSocio) {
        return _codiceConsorzioLocalService.createCodiceConsorzio(codiceSocio);
    }

    /**
    * Deletes the codice consorzio with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceSocio the primary key of the codice consorzio
    * @return the codice consorzio that was removed
    * @throws PortalException if a codice consorzio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.CodiceConsorzio deleteCodiceConsorzio(
        java.lang.String codiceSocio)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _codiceConsorzioLocalService.deleteCodiceConsorzio(codiceSocio);
    }

    /**
    * Deletes the codice consorzio from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceConsorzio the codice consorzio
    * @return the codice consorzio that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.CodiceConsorzio deleteCodiceConsorzio(
        it.bysoftware.ct.model.CodiceConsorzio codiceConsorzio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _codiceConsorzioLocalService.deleteCodiceConsorzio(codiceConsorzio);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _codiceConsorzioLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _codiceConsorzioLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CodiceConsorzioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _codiceConsorzioLocalService.dynamicQuery(dynamicQuery, start,
            end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CodiceConsorzioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _codiceConsorzioLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _codiceConsorzioLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _codiceConsorzioLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public it.bysoftware.ct.model.CodiceConsorzio fetchCodiceConsorzio(
        java.lang.String codiceSocio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _codiceConsorzioLocalService.fetchCodiceConsorzio(codiceSocio);
    }

    /**
    * Returns the codice consorzio with the primary key.
    *
    * @param codiceSocio the primary key of the codice consorzio
    * @return the codice consorzio
    * @throws PortalException if a codice consorzio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.CodiceConsorzio getCodiceConsorzio(
        java.lang.String codiceSocio)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _codiceConsorzioLocalService.getCodiceConsorzio(codiceSocio);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _codiceConsorzioLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the codice consorzios.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CodiceConsorzioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of codice consorzios
    * @param end the upper bound of the range of codice consorzios (not inclusive)
    * @return the range of codice consorzios
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.CodiceConsorzio> getCodiceConsorzios(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _codiceConsorzioLocalService.getCodiceConsorzios(start, end);
    }

    /**
    * Returns the number of codice consorzios.
    *
    * @return the number of codice consorzios
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getCodiceConsorziosCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _codiceConsorzioLocalService.getCodiceConsorziosCount();
    }

    /**
    * Updates the codice consorzio in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param codiceConsorzio the codice consorzio
    * @return the codice consorzio that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.CodiceConsorzio updateCodiceConsorzio(
        it.bysoftware.ct.model.CodiceConsorzio codiceConsorzio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _codiceConsorzioLocalService.updateCodiceConsorzio(codiceConsorzio);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _codiceConsorzioLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _codiceConsorzioLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _codiceConsorzioLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public CodiceConsorzioLocalService getWrappedCodiceConsorzioLocalService() {
        return _codiceConsorzioLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedCodiceConsorzioLocalService(
        CodiceConsorzioLocalService codiceConsorzioLocalService) {
        _codiceConsorzioLocalService = codiceConsorzioLocalService;
    }

    @Override
    public CodiceConsorzioLocalService getWrappedService() {
        return _codiceConsorzioLocalService;
    }

    @Override
    public void setWrappedService(
        CodiceConsorzioLocalService codiceConsorzioLocalService) {
        _codiceConsorzioLocalService = codiceConsorzioLocalService;
    }
}
