package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class ClientiFornitoriDatiAggPK implements Comparable<ClientiFornitoriDatiAggPK>,
    Serializable {
    public String codiceAnagrafica;
    public boolean fornitore;

    public ClientiFornitoriDatiAggPK() {
    }

    public ClientiFornitoriDatiAggPK(String codiceAnagrafica, boolean fornitore) {
        this.codiceAnagrafica = codiceAnagrafica;
        this.fornitore = fornitore;
    }

    public String getCodiceAnagrafica() {
        return codiceAnagrafica;
    }

    public void setCodiceAnagrafica(String codiceAnagrafica) {
        this.codiceAnagrafica = codiceAnagrafica;
    }

    public boolean getFornitore() {
        return fornitore;
    }

    public boolean isFornitore() {
        return fornitore;
    }

    public void setFornitore(boolean fornitore) {
        this.fornitore = fornitore;
    }

    @Override
    public int compareTo(ClientiFornitoriDatiAggPK pk) {
        if (pk == null) {
            return -1;
        }

        int value = 0;

        value = codiceAnagrafica.compareTo(pk.codiceAnagrafica);

        if (value != 0) {
            return value;
        }

        if (!fornitore && pk.fornitore) {
            value = -1;
        } else if (fornitore && !pk.fornitore) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ClientiFornitoriDatiAggPK)) {
            return false;
        }

        ClientiFornitoriDatiAggPK pk = (ClientiFornitoriDatiAggPK) obj;

        if ((codiceAnagrafica.equals(pk.codiceAnagrafica)) &&
                (fornitore == pk.fornitore)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (String.valueOf(codiceAnagrafica) + String.valueOf(fornitore)).hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(10);

        sb.append(StringPool.OPEN_CURLY_BRACE);

        sb.append("codiceAnagrafica");
        sb.append(StringPool.EQUAL);
        sb.append(codiceAnagrafica);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("fornitore");
        sb.append(StringPool.EQUAL);
        sb.append(fornitore);

        sb.append(StringPool.CLOSE_CURLY_BRACE);

        return sb.toString();
    }
}
