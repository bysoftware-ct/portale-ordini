package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.Piante;

import java.util.List;

/**
 * The persistence utility for the piante service. This utility wraps {@link PiantePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see PiantePersistence
 * @see PiantePersistenceImpl
 * @generated
 */
public class PianteUtil {
    private static PiantePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Piante piante) {
        getPersistence().clearCache(piante);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Piante> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Piante> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Piante> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Piante update(Piante piante) throws SystemException {
        return getPersistence().update(piante);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Piante update(Piante piante, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(piante, serviceContext);
    }

    /**
    * Returns the piante where codice = &#63; or throws a {@link it.bysoftware.ct.NoSuchPianteException} if it could not be found.
    *
    * @param codice the codice
    * @return the matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante findByCodice(
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence().findByCodice(codice);
    }

    /**
    * Returns the piante where codice = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param codice the codice
    * @return the matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByCodice(
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByCodice(codice);
    }

    /**
    * Returns the piante where codice = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param codice the codice
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByCodice(
        java.lang.String codice, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByCodice(codice, retrieveFromCache);
    }

    /**
    * Removes the piante where codice = &#63; from the database.
    *
    * @param codice the codice
    * @return the piante that was removed
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante removeByCodice(
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence().removeByCodice(codice);
    }

    /**
    * Returns the number of piantes where codice = &#63;.
    *
    * @param codice the codice
    * @return the number of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static int countByCodice(java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByCodice(codice);
    }

    /**
    * Returns all the piantes where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @return the matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByNomeCodiceLike(
        boolean obsoleto, java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByNomeCodiceLike(obsoleto, nome, codice);
    }

    /**
    * Returns a range of all the piantes where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByNomeCodiceLike(
        boolean obsoleto, java.lang.String nome, java.lang.String codice,
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByNomeCodiceLike(obsoleto, nome, codice, start, end);
    }

    /**
    * Returns an ordered range of all the piantes where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByNomeCodiceLike(
        boolean obsoleto, java.lang.String nome, java.lang.String codice,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByNomeCodiceLike(obsoleto, nome, codice, start, end,
            orderByComparator);
    }

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante findByNomeCodiceLike_First(
        boolean obsoleto, java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByNomeCodiceLike_First(obsoleto, nome, codice,
            orderByComparator);
    }

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByNomeCodiceLike_First(
        boolean obsoleto, java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByNomeCodiceLike_First(obsoleto, nome, codice,
            orderByComparator);
    }

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante findByNomeCodiceLike_Last(
        boolean obsoleto, java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByNomeCodiceLike_Last(obsoleto, nome, codice,
            orderByComparator);
    }

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByNomeCodiceLike_Last(
        boolean obsoleto, java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByNomeCodiceLike_Last(obsoleto, nome, codice,
            orderByComparator);
    }

    /**
    * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param id the primary key of the current piante
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante[] findByNomeCodiceLike_PrevAndNext(
        long id, boolean obsoleto, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByNomeCodiceLike_PrevAndNext(id, obsoleto, nome,
            codice, orderByComparator);
    }

    /**
    * Removes all the piantes where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
    *
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @throws SystemException if a system exception occurred
    */
    public static void removeByNomeCodiceLike(boolean obsoleto,
        java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByNomeCodiceLike(obsoleto, nome, codice);
    }

    /**
    * Returns the number of piantes where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @return the number of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static int countByNomeCodiceLike(boolean obsoleto,
        java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByNomeCodiceLike(obsoleto, nome, codice);
    }

    /**
    * Returns all the piantes where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @return the matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByCategoria(
        boolean obsoleto, long idCategoria, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCategoria(obsoleto, idCategoria, nome, codice);
    }

    /**
    * Returns a range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByCategoria(
        boolean obsoleto, long idCategoria, java.lang.String nome,
        java.lang.String codice, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCategoria(obsoleto, idCategoria, nome, codice, start,
            end);
    }

    /**
    * Returns an ordered range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByCategoria(
        boolean obsoleto, long idCategoria, java.lang.String nome,
        java.lang.String codice, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCategoria(obsoleto, idCategoria, nome, codice, start,
            end, orderByComparator);
    }

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante findByCategoria_First(
        boolean obsoleto, long idCategoria, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByCategoria_First(obsoleto, idCategoria, nome, codice,
            orderByComparator);
    }

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByCategoria_First(
        boolean obsoleto, long idCategoria, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCategoria_First(obsoleto, idCategoria, nome, codice,
            orderByComparator);
    }

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante findByCategoria_Last(
        boolean obsoleto, long idCategoria, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByCategoria_Last(obsoleto, idCategoria, nome, codice,
            orderByComparator);
    }

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByCategoria_Last(
        boolean obsoleto, long idCategoria, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCategoria_Last(obsoleto, idCategoria, nome, codice,
            orderByComparator);
    }

    /**
    * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param id the primary key of the current piante
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante[] findByCategoria_PrevAndNext(
        long id, boolean obsoleto, long idCategoria, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByCategoria_PrevAndNext(id, obsoleto, idCategoria,
            nome, codice, orderByComparator);
    }

    /**
    * Removes all the piantes where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @throws SystemException if a system exception occurred
    */
    public static void removeByCategoria(boolean obsoleto, long idCategoria,
        java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByCategoria(obsoleto, idCategoria, nome, codice);
    }

    /**
    * Returns the number of piantes where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @return the number of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static int countByCategoria(boolean obsoleto, long idCategoria,
        java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByCategoria(obsoleto, idCategoria, nome, codice);
    }

    /**
    * Returns all the piantes where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @return the matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByAttivaNomeCodiceLike(
        boolean obsoleto, boolean attiva, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByAttivaNomeCodiceLike(obsoleto, attiva, nome, codice);
    }

    /**
    * Returns a range of all the piantes where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByAttivaNomeCodiceLike(
        boolean obsoleto, boolean attiva, java.lang.String nome,
        java.lang.String codice, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByAttivaNomeCodiceLike(obsoleto, attiva, nome, codice,
            start, end);
    }

    /**
    * Returns an ordered range of all the piantes where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByAttivaNomeCodiceLike(
        boolean obsoleto, boolean attiva, java.lang.String nome,
        java.lang.String codice, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByAttivaNomeCodiceLike(obsoleto, attiva, nome, codice,
            start, end, orderByComparator);
    }

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante findByAttivaNomeCodiceLike_First(
        boolean obsoleto, boolean attiva, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByAttivaNomeCodiceLike_First(obsoleto, attiva, nome,
            codice, orderByComparator);
    }

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByAttivaNomeCodiceLike_First(
        boolean obsoleto, boolean attiva, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByAttivaNomeCodiceLike_First(obsoleto, attiva, nome,
            codice, orderByComparator);
    }

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante findByAttivaNomeCodiceLike_Last(
        boolean obsoleto, boolean attiva, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByAttivaNomeCodiceLike_Last(obsoleto, attiva, nome,
            codice, orderByComparator);
    }

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByAttivaNomeCodiceLike_Last(
        boolean obsoleto, boolean attiva, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByAttivaNomeCodiceLike_Last(obsoleto, attiva, nome,
            codice, orderByComparator);
    }

    /**
    * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param id the primary key of the current piante
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante[] findByAttivaNomeCodiceLike_PrevAndNext(
        long id, boolean obsoleto, boolean attiva, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByAttivaNomeCodiceLike_PrevAndNext(id, obsoleto,
            attiva, nome, codice, orderByComparator);
    }

    /**
    * Removes all the piantes where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
    *
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @throws SystemException if a system exception occurred
    */
    public static void removeByAttivaNomeCodiceLike(boolean obsoleto,
        boolean attiva, java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByAttivaNomeCodiceLike(obsoleto, attiva, nome, codice);
    }

    /**
    * Returns the number of piantes where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @return the number of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static int countByAttivaNomeCodiceLike(boolean obsoleto,
        boolean attiva, java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByAttivaNomeCodiceLike(obsoleto, attiva, nome, codice);
    }

    /**
    * Returns all the piantes where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @return the matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByCategoriaAttiva(
        boolean obsoleto, long idCategoria, boolean attiva,
        java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCategoriaAttiva(obsoleto, idCategoria, attiva, nome,
            codice);
    }

    /**
    * Returns a range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByCategoriaAttiva(
        boolean obsoleto, long idCategoria, boolean attiva,
        java.lang.String nome, java.lang.String codice, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCategoriaAttiva(obsoleto, idCategoria, attiva, nome,
            codice, start, end);
    }

    /**
    * Returns an ordered range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByCategoriaAttiva(
        boolean obsoleto, long idCategoria, boolean attiva,
        java.lang.String nome, java.lang.String codice, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCategoriaAttiva(obsoleto, idCategoria, attiva, nome,
            codice, start, end, orderByComparator);
    }

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante findByCategoriaAttiva_First(
        boolean obsoleto, long idCategoria, boolean attiva,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByCategoriaAttiva_First(obsoleto, idCategoria, attiva,
            nome, codice, orderByComparator);
    }

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByCategoriaAttiva_First(
        boolean obsoleto, long idCategoria, boolean attiva,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCategoriaAttiva_First(obsoleto, idCategoria, attiva,
            nome, codice, orderByComparator);
    }

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante findByCategoriaAttiva_Last(
        boolean obsoleto, long idCategoria, boolean attiva,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByCategoriaAttiva_Last(obsoleto, idCategoria, attiva,
            nome, codice, orderByComparator);
    }

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByCategoriaAttiva_Last(
        boolean obsoleto, long idCategoria, boolean attiva,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCategoriaAttiva_Last(obsoleto, idCategoria, attiva,
            nome, codice, orderByComparator);
    }

    /**
    * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param id the primary key of the current piante
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante[] findByCategoriaAttiva_PrevAndNext(
        long id, boolean obsoleto, long idCategoria, boolean attiva,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByCategoriaAttiva_PrevAndNext(id, obsoleto,
            idCategoria, attiva, nome, codice, orderByComparator);
    }

    /**
    * Removes all the piantes where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @throws SystemException if a system exception occurred
    */
    public static void removeByCategoriaAttiva(boolean obsoleto,
        long idCategoria, boolean attiva, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByCategoriaAttiva(obsoleto, idCategoria, attiva, nome, codice);
    }

    /**
    * Returns the number of piantes where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @return the number of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static int countByCategoriaAttiva(boolean obsoleto,
        long idCategoria, boolean attiva, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByCategoriaAttiva(obsoleto, idCategoria, attiva, nome,
            codice);
    }

    /**
    * Returns all the piantes where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @return the matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByFornitoreNomeCodiceLike(
        boolean obsoleto, java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByFornitoreNomeCodiceLike(obsoleto, idFornitore, nome,
            codice);
    }

    /**
    * Returns a range of all the piantes where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByFornitoreNomeCodiceLike(
        boolean obsoleto, java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByFornitoreNomeCodiceLike(obsoleto, idFornitore, nome,
            codice, start, end);
    }

    /**
    * Returns an ordered range of all the piantes where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByFornitoreNomeCodiceLike(
        boolean obsoleto, java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByFornitoreNomeCodiceLike(obsoleto, idFornitore, nome,
            codice, start, end, orderByComparator);
    }

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante findByFornitoreNomeCodiceLike_First(
        boolean obsoleto, java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByFornitoreNomeCodiceLike_First(obsoleto, idFornitore,
            nome, codice, orderByComparator);
    }

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByFornitoreNomeCodiceLike_First(
        boolean obsoleto, java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByFornitoreNomeCodiceLike_First(obsoleto, idFornitore,
            nome, codice, orderByComparator);
    }

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante findByFornitoreNomeCodiceLike_Last(
        boolean obsoleto, java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByFornitoreNomeCodiceLike_Last(obsoleto, idFornitore,
            nome, codice, orderByComparator);
    }

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByFornitoreNomeCodiceLike_Last(
        boolean obsoleto, java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByFornitoreNomeCodiceLike_Last(obsoleto, idFornitore,
            nome, codice, orderByComparator);
    }

    /**
    * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param id the primary key of the current piante
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante[] findByFornitoreNomeCodiceLike_PrevAndNext(
        long id, boolean obsoleto, java.lang.String idFornitore,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByFornitoreNomeCodiceLike_PrevAndNext(id, obsoleto,
            idFornitore, nome, codice, orderByComparator);
    }

    /**
    * Removes all the piantes where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @throws SystemException if a system exception occurred
    */
    public static void removeByFornitoreNomeCodiceLike(boolean obsoleto,
        java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByFornitoreNomeCodiceLike(obsoleto, idFornitore, nome, codice);
    }

    /**
    * Returns the number of piantes where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @return the number of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static int countByFornitoreNomeCodiceLike(boolean obsoleto,
        java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByFornitoreNomeCodiceLike(obsoleto, idFornitore, nome,
            codice);
    }

    /**
    * Returns all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @return the matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByCategoriaFornitore(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCategoriaFornitore(obsoleto, idCategoria,
            idFornitore, nome, codice);
    }

    /**
    * Returns a range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByCategoriaFornitore(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        java.lang.String nome, java.lang.String codice, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCategoriaFornitore(obsoleto, idCategoria,
            idFornitore, nome, codice, start, end);
    }

    /**
    * Returns an ordered range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByCategoriaFornitore(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        java.lang.String nome, java.lang.String codice, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCategoriaFornitore(obsoleto, idCategoria,
            idFornitore, nome, codice, start, end, orderByComparator);
    }

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante findByCategoriaFornitore_First(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByCategoriaFornitore_First(obsoleto, idCategoria,
            idFornitore, nome, codice, orderByComparator);
    }

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByCategoriaFornitore_First(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCategoriaFornitore_First(obsoleto, idCategoria,
            idFornitore, nome, codice, orderByComparator);
    }

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante findByCategoriaFornitore_Last(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByCategoriaFornitore_Last(obsoleto, idCategoria,
            idFornitore, nome, codice, orderByComparator);
    }

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByCategoriaFornitore_Last(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCategoriaFornitore_Last(obsoleto, idCategoria,
            idFornitore, nome, codice, orderByComparator);
    }

    /**
    * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param id the primary key of the current piante
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante[] findByCategoriaFornitore_PrevAndNext(
        long id, boolean obsoleto, long idCategoria,
        java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByCategoriaFornitore_PrevAndNext(id, obsoleto,
            idCategoria, idFornitore, nome, codice, orderByComparator);
    }

    /**
    * Removes all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @throws SystemException if a system exception occurred
    */
    public static void removeByCategoriaFornitore(boolean obsoleto,
        long idCategoria, java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByCategoriaFornitore(obsoleto, idCategoria, idFornitore,
            nome, codice);
    }

    /**
    * Returns the number of piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @return the number of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static int countByCategoriaFornitore(boolean obsoleto,
        long idCategoria, java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByCategoriaFornitore(obsoleto, idCategoria,
            idFornitore, nome, codice);
    }

    /**
    * Returns all the piantes where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @return the matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByFornitoreAttivaNomeCodiceLike(
        boolean obsoleto, java.lang.String idFornitore, boolean attiva,
        java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByFornitoreAttivaNomeCodiceLike(obsoleto, idFornitore,
            attiva, nome, codice);
    }

    /**
    * Returns a range of all the piantes where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByFornitoreAttivaNomeCodiceLike(
        boolean obsoleto, java.lang.String idFornitore, boolean attiva,
        java.lang.String nome, java.lang.String codice, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByFornitoreAttivaNomeCodiceLike(obsoleto, idFornitore,
            attiva, nome, codice, start, end);
    }

    /**
    * Returns an ordered range of all the piantes where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByFornitoreAttivaNomeCodiceLike(
        boolean obsoleto, java.lang.String idFornitore, boolean attiva,
        java.lang.String nome, java.lang.String codice, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByFornitoreAttivaNomeCodiceLike(obsoleto, idFornitore,
            attiva, nome, codice, start, end, orderByComparator);
    }

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante findByFornitoreAttivaNomeCodiceLike_First(
        boolean obsoleto, java.lang.String idFornitore, boolean attiva,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByFornitoreAttivaNomeCodiceLike_First(obsoleto,
            idFornitore, attiva, nome, codice, orderByComparator);
    }

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByFornitoreAttivaNomeCodiceLike_First(
        boolean obsoleto, java.lang.String idFornitore, boolean attiva,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByFornitoreAttivaNomeCodiceLike_First(obsoleto,
            idFornitore, attiva, nome, codice, orderByComparator);
    }

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante findByFornitoreAttivaNomeCodiceLike_Last(
        boolean obsoleto, java.lang.String idFornitore, boolean attiva,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByFornitoreAttivaNomeCodiceLike_Last(obsoleto,
            idFornitore, attiva, nome, codice, orderByComparator);
    }

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByFornitoreAttivaNomeCodiceLike_Last(
        boolean obsoleto, java.lang.String idFornitore, boolean attiva,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByFornitoreAttivaNomeCodiceLike_Last(obsoleto,
            idFornitore, attiva, nome, codice, orderByComparator);
    }

    /**
    * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param id the primary key of the current piante
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante[] findByFornitoreAttivaNomeCodiceLike_PrevAndNext(
        long id, boolean obsoleto, java.lang.String idFornitore,
        boolean attiva, java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByFornitoreAttivaNomeCodiceLike_PrevAndNext(id,
            obsoleto, idFornitore, attiva, nome, codice, orderByComparator);
    }

    /**
    * Removes all the piantes where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @throws SystemException if a system exception occurred
    */
    public static void removeByFornitoreAttivaNomeCodiceLike(boolean obsoleto,
        java.lang.String idFornitore, boolean attiva, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByFornitoreAttivaNomeCodiceLike(obsoleto, idFornitore,
            attiva, nome, codice);
    }

    /**
    * Returns the number of piantes where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @return the number of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static int countByFornitoreAttivaNomeCodiceLike(boolean obsoleto,
        java.lang.String idFornitore, boolean attiva, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByFornitoreAttivaNomeCodiceLike(obsoleto, idFornitore,
            attiva, nome, codice);
    }

    /**
    * Returns all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @return the matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByCategoriaFornitoreAttiva(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        boolean attiva, java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCategoriaFornitoreAttiva(obsoleto, idCategoria,
            idFornitore, attiva, nome, codice);
    }

    /**
    * Returns a range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByCategoriaFornitoreAttiva(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        boolean attiva, java.lang.String nome, java.lang.String codice,
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCategoriaFornitoreAttiva(obsoleto, idCategoria,
            idFornitore, attiva, nome, codice, start, end);
    }

    /**
    * Returns an ordered range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findByCategoriaFornitoreAttiva(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        boolean attiva, java.lang.String nome, java.lang.String codice,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCategoriaFornitoreAttiva(obsoleto, idCategoria,
            idFornitore, attiva, nome, codice, start, end, orderByComparator);
    }

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante findByCategoriaFornitoreAttiva_First(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        boolean attiva, java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByCategoriaFornitoreAttiva_First(obsoleto, idCategoria,
            idFornitore, attiva, nome, codice, orderByComparator);
    }

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByCategoriaFornitoreAttiva_First(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        boolean attiva, java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCategoriaFornitoreAttiva_First(obsoleto,
            idCategoria, idFornitore, attiva, nome, codice, orderByComparator);
    }

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante findByCategoriaFornitoreAttiva_Last(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        boolean attiva, java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByCategoriaFornitoreAttiva_Last(obsoleto, idCategoria,
            idFornitore, attiva, nome, codice, orderByComparator);
    }

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByCategoriaFornitoreAttiva_Last(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        boolean attiva, java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCategoriaFornitoreAttiva_Last(obsoleto, idCategoria,
            idFornitore, attiva, nome, codice, orderByComparator);
    }

    /**
    * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param id the primary key of the current piante
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante[] findByCategoriaFornitoreAttiva_PrevAndNext(
        long id, boolean obsoleto, long idCategoria,
        java.lang.String idFornitore, boolean attiva, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence()
                   .findByCategoriaFornitoreAttiva_PrevAndNext(id, obsoleto,
            idCategoria, idFornitore, attiva, nome, codice, orderByComparator);
    }

    /**
    * Removes all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @throws SystemException if a system exception occurred
    */
    public static void removeByCategoriaFornitoreAttiva(boolean obsoleto,
        long idCategoria, java.lang.String idFornitore, boolean attiva,
        java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByCategoriaFornitoreAttiva(obsoleto, idCategoria,
            idFornitore, attiva, nome, codice);
    }

    /**
    * Returns the number of piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @return the number of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public static int countByCategoriaFornitoreAttiva(boolean obsoleto,
        long idCategoria, java.lang.String idFornitore, boolean attiva,
        java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByCategoriaFornitoreAttiva(obsoleto, idCategoria,
            idFornitore, attiva, nome, codice);
    }

    /**
    * Caches the piante in the entity cache if it is enabled.
    *
    * @param piante the piante
    */
    public static void cacheResult(it.bysoftware.ct.model.Piante piante) {
        getPersistence().cacheResult(piante);
    }

    /**
    * Caches the piantes in the entity cache if it is enabled.
    *
    * @param piantes the piantes
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.Piante> piantes) {
        getPersistence().cacheResult(piantes);
    }

    /**
    * Creates a new piante with the primary key. Does not add the piante to the database.
    *
    * @param id the primary key for the new piante
    * @return the new piante
    */
    public static it.bysoftware.ct.model.Piante create(long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the piante with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the piante
    * @return the piante that was removed
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante remove(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence().remove(id);
    }

    public static it.bysoftware.ct.model.Piante updateImpl(
        it.bysoftware.ct.model.Piante piante)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(piante);
    }

    /**
    * Returns the piante with the primary key or throws a {@link it.bysoftware.ct.NoSuchPianteException} if it could not be found.
    *
    * @param id the primary key of the piante
    * @return the piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante findByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the piante with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the piante
    * @return the piante, or <code>null</code> if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Piante fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the piantes.
    *
    * @return the piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the piantes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the piantes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of piantes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Piante> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the piantes from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of piantes.
    *
    * @return the number of piantes
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static PiantePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (PiantePersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    PiantePersistence.class.getName());

            ReferenceRegistry.registerReference(PianteUtil.class, "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(PiantePersistence persistence) {
    }
}
