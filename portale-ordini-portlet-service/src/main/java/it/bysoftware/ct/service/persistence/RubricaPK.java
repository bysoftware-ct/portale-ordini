package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class RubricaPK implements Comparable<RubricaPK>, Serializable {
    public String codiceSoggetto;
    public int numeroVoce;

    public RubricaPK() {
    }

    public RubricaPK(String codiceSoggetto, int numeroVoce) {
        this.codiceSoggetto = codiceSoggetto;
        this.numeroVoce = numeroVoce;
    }

    public String getCodiceSoggetto() {
        return codiceSoggetto;
    }

    public void setCodiceSoggetto(String codiceSoggetto) {
        this.codiceSoggetto = codiceSoggetto;
    }

    public int getNumeroVoce() {
        return numeroVoce;
    }

    public void setNumeroVoce(int numeroVoce) {
        this.numeroVoce = numeroVoce;
    }

    @Override
    public int compareTo(RubricaPK pk) {
        if (pk == null) {
            return -1;
        }

        int value = 0;

        value = codiceSoggetto.compareTo(pk.codiceSoggetto);

        if (value != 0) {
            return value;
        }

        if (numeroVoce < pk.numeroVoce) {
            value = -1;
        } else if (numeroVoce > pk.numeroVoce) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof RubricaPK)) {
            return false;
        }

        RubricaPK pk = (RubricaPK) obj;

        if ((codiceSoggetto.equals(pk.codiceSoggetto)) &&
                (numeroVoce == pk.numeroVoce)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (String.valueOf(codiceSoggetto) + String.valueOf(numeroVoce)).hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(10);

        sb.append(StringPool.OPEN_CURLY_BRACE);

        sb.append("codiceSoggetto");
        sb.append(StringPool.EQUAL);
        sb.append(codiceSoggetto);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("numeroVoce");
        sb.append(StringPool.EQUAL);
        sb.append(numeroVoce);

        sb.append(StringPool.CLOSE_CURLY_BRACE);

        return sb.toString();
    }
}
