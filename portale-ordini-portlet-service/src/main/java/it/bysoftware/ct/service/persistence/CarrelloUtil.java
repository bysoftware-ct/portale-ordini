package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.Carrello;

import java.util.List;

/**
 * The persistence utility for the carrello service. This utility wraps {@link CarrelloPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CarrelloPersistence
 * @see CarrelloPersistenceImpl
 * @generated
 */
public class CarrelloUtil {
    private static CarrelloPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Carrello carrello) {
        getPersistence().clearCache(carrello);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Carrello> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Carrello> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Carrello> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Carrello update(Carrello carrello) throws SystemException {
        return getPersistence().update(carrello);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Carrello update(Carrello carrello,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(carrello, serviceContext);
    }

    /**
    * Returns all the carrellos where idOrdine = &#63;.
    *
    * @param idOrdine the id ordine
    * @return the matching carrellos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Carrello> findByIdOrdine(
        long idOrdine)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByIdOrdine(idOrdine);
    }

    /**
    * Returns a range of all the carrellos where idOrdine = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CarrelloModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param idOrdine the id ordine
    * @param start the lower bound of the range of carrellos
    * @param end the upper bound of the range of carrellos (not inclusive)
    * @return the range of matching carrellos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Carrello> findByIdOrdine(
        long idOrdine, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByIdOrdine(idOrdine, start, end);
    }

    /**
    * Returns an ordered range of all the carrellos where idOrdine = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CarrelloModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param idOrdine the id ordine
    * @param start the lower bound of the range of carrellos
    * @param end the upper bound of the range of carrellos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching carrellos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Carrello> findByIdOrdine(
        long idOrdine, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByIdOrdine(idOrdine, start, end, orderByComparator);
    }

    /**
    * Returns the first carrello in the ordered set where idOrdine = &#63;.
    *
    * @param idOrdine the id ordine
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching carrello
    * @throws it.bysoftware.ct.NoSuchCarrelloException if a matching carrello could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Carrello findByIdOrdine_First(
        long idOrdine,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCarrelloException {
        return getPersistence().findByIdOrdine_First(idOrdine, orderByComparator);
    }

    /**
    * Returns the first carrello in the ordered set where idOrdine = &#63;.
    *
    * @param idOrdine the id ordine
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching carrello, or <code>null</code> if a matching carrello could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Carrello fetchByIdOrdine_First(
        long idOrdine,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByIdOrdine_First(idOrdine, orderByComparator);
    }

    /**
    * Returns the last carrello in the ordered set where idOrdine = &#63;.
    *
    * @param idOrdine the id ordine
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching carrello
    * @throws it.bysoftware.ct.NoSuchCarrelloException if a matching carrello could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Carrello findByIdOrdine_Last(
        long idOrdine,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCarrelloException {
        return getPersistence().findByIdOrdine_Last(idOrdine, orderByComparator);
    }

    /**
    * Returns the last carrello in the ordered set where idOrdine = &#63;.
    *
    * @param idOrdine the id ordine
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching carrello, or <code>null</code> if a matching carrello could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Carrello fetchByIdOrdine_Last(
        long idOrdine,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByIdOrdine_Last(idOrdine, orderByComparator);
    }

    /**
    * Returns the carrellos before and after the current carrello in the ordered set where idOrdine = &#63;.
    *
    * @param id the primary key of the current carrello
    * @param idOrdine the id ordine
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next carrello
    * @throws it.bysoftware.ct.NoSuchCarrelloException if a carrello with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Carrello[] findByIdOrdine_PrevAndNext(
        long id, long idOrdine,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCarrelloException {
        return getPersistence()
                   .findByIdOrdine_PrevAndNext(id, idOrdine, orderByComparator);
    }

    /**
    * Removes all the carrellos where idOrdine = &#63; from the database.
    *
    * @param idOrdine the id ordine
    * @throws SystemException if a system exception occurred
    */
    public static void removeByIdOrdine(long idOrdine)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByIdOrdine(idOrdine);
    }

    /**
    * Returns the number of carrellos where idOrdine = &#63;.
    *
    * @param idOrdine the id ordine
    * @return the number of matching carrellos
    * @throws SystemException if a system exception occurred
    */
    public static int countByIdOrdine(long idOrdine)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByIdOrdine(idOrdine);
    }

    /**
    * Caches the carrello in the entity cache if it is enabled.
    *
    * @param carrello the carrello
    */
    public static void cacheResult(it.bysoftware.ct.model.Carrello carrello) {
        getPersistence().cacheResult(carrello);
    }

    /**
    * Caches the carrellos in the entity cache if it is enabled.
    *
    * @param carrellos the carrellos
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.Carrello> carrellos) {
        getPersistence().cacheResult(carrellos);
    }

    /**
    * Creates a new carrello with the primary key. Does not add the carrello to the database.
    *
    * @param id the primary key for the new carrello
    * @return the new carrello
    */
    public static it.bysoftware.ct.model.Carrello create(long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the carrello with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the carrello
    * @return the carrello that was removed
    * @throws it.bysoftware.ct.NoSuchCarrelloException if a carrello with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Carrello remove(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCarrelloException {
        return getPersistence().remove(id);
    }

    public static it.bysoftware.ct.model.Carrello updateImpl(
        it.bysoftware.ct.model.Carrello carrello)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(carrello);
    }

    /**
    * Returns the carrello with the primary key or throws a {@link it.bysoftware.ct.NoSuchCarrelloException} if it could not be found.
    *
    * @param id the primary key of the carrello
    * @return the carrello
    * @throws it.bysoftware.ct.NoSuchCarrelloException if a carrello with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Carrello findByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCarrelloException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the carrello with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the carrello
    * @return the carrello, or <code>null</code> if a carrello with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Carrello fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the carrellos.
    *
    * @return the carrellos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Carrello> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the carrellos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CarrelloModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of carrellos
    * @param end the upper bound of the range of carrellos (not inclusive)
    * @return the range of carrellos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Carrello> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the carrellos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CarrelloModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of carrellos
    * @param end the upper bound of the range of carrellos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of carrellos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Carrello> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the carrellos from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of carrellos.
    *
    * @return the number of carrellos
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static CarrelloPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (CarrelloPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    CarrelloPersistence.class.getName());

            ReferenceRegistry.registerReference(CarrelloUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(CarrelloPersistence persistence) {
    }
}
