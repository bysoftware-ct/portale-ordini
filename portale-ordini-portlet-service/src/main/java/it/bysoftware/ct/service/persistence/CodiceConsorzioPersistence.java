package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.CodiceConsorzio;

/**
 * The persistence interface for the codice consorzio service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CodiceConsorzioPersistenceImpl
 * @see CodiceConsorzioUtil
 * @generated
 */
public interface CodiceConsorzioPersistence extends BasePersistence<CodiceConsorzio> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link CodiceConsorzioUtil} to access the codice consorzio persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the codice consorzio in the entity cache if it is enabled.
    *
    * @param codiceConsorzio the codice consorzio
    */
    public void cacheResult(
        it.bysoftware.ct.model.CodiceConsorzio codiceConsorzio);

    /**
    * Caches the codice consorzios in the entity cache if it is enabled.
    *
    * @param codiceConsorzios the codice consorzios
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.CodiceConsorzio> codiceConsorzios);

    /**
    * Creates a new codice consorzio with the primary key. Does not add the codice consorzio to the database.
    *
    * @param codiceSocio the primary key for the new codice consorzio
    * @return the new codice consorzio
    */
    public it.bysoftware.ct.model.CodiceConsorzio create(
        java.lang.String codiceSocio);

    /**
    * Removes the codice consorzio with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceSocio the primary key of the codice consorzio
    * @return the codice consorzio that was removed
    * @throws it.bysoftware.ct.NoSuchCodiceConsorzioException if a codice consorzio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.CodiceConsorzio remove(
        java.lang.String codiceSocio)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCodiceConsorzioException;

    public it.bysoftware.ct.model.CodiceConsorzio updateImpl(
        it.bysoftware.ct.model.CodiceConsorzio codiceConsorzio)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the codice consorzio with the primary key or throws a {@link it.bysoftware.ct.NoSuchCodiceConsorzioException} if it could not be found.
    *
    * @param codiceSocio the primary key of the codice consorzio
    * @return the codice consorzio
    * @throws it.bysoftware.ct.NoSuchCodiceConsorzioException if a codice consorzio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.CodiceConsorzio findByPrimaryKey(
        java.lang.String codiceSocio)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCodiceConsorzioException;

    /**
    * Returns the codice consorzio with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param codiceSocio the primary key of the codice consorzio
    * @return the codice consorzio, or <code>null</code> if a codice consorzio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.CodiceConsorzio fetchByPrimaryKey(
        java.lang.String codiceSocio)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the codice consorzios.
    *
    * @return the codice consorzios
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.CodiceConsorzio> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the codice consorzios.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CodiceConsorzioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of codice consorzios
    * @param end the upper bound of the range of codice consorzios (not inclusive)
    * @return the range of codice consorzios
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.CodiceConsorzio> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the codice consorzios.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CodiceConsorzioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of codice consorzios
    * @param end the upper bound of the range of codice consorzios (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of codice consorzios
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.CodiceConsorzio> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the codice consorzios from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of codice consorzios.
    *
    * @return the number of codice consorzios
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
