package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link OrdineLocalService}.
 *
 * @author Mario Torrisi
 * @see OrdineLocalService
 * @generated
 */
public class OrdineLocalServiceWrapper implements OrdineLocalService,
    ServiceWrapper<OrdineLocalService> {
    private OrdineLocalService _ordineLocalService;

    public OrdineLocalServiceWrapper(OrdineLocalService ordineLocalService) {
        _ordineLocalService = ordineLocalService;
    }

    /**
    * Adds the ordine to the database. Also notifies the appropriate model listeners.
    *
    * @param ordine the ordine
    * @return the ordine that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Ordine addOrdine(
        it.bysoftware.ct.model.Ordine ordine)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _ordineLocalService.addOrdine(ordine);
    }

    /**
    * Creates a new ordine with the primary key. Does not add the ordine to the database.
    *
    * @param id the primary key for the new ordine
    * @return the new ordine
    */
    @Override
    public it.bysoftware.ct.model.Ordine createOrdine(long id) {
        return _ordineLocalService.createOrdine(id);
    }

    /**
    * Deletes the ordine with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the ordine
    * @return the ordine that was removed
    * @throws PortalException if a ordine with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Ordine deleteOrdine(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _ordineLocalService.deleteOrdine(id);
    }

    /**
    * Deletes the ordine from the database. Also notifies the appropriate model listeners.
    *
    * @param ordine the ordine
    * @return the ordine that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Ordine deleteOrdine(
        it.bysoftware.ct.model.Ordine ordine)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _ordineLocalService.deleteOrdine(ordine);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _ordineLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _ordineLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _ordineLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _ordineLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _ordineLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _ordineLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public it.bysoftware.ct.model.Ordine fetchOrdine(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _ordineLocalService.fetchOrdine(id);
    }

    /**
    * Returns the ordine with the primary key.
    *
    * @param id the primary key of the ordine
    * @return the ordine
    * @throws PortalException if a ordine with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Ordine getOrdine(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _ordineLocalService.getOrdine(id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _ordineLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the ordines.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @return the range of ordines
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.Ordine> getOrdines(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _ordineLocalService.getOrdines(start, end);
    }

    /**
    * Returns the number of ordines.
    *
    * @return the number of ordines
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getOrdinesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _ordineLocalService.getOrdinesCount();
    }

    /**
    * Updates the ordine in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param ordine the ordine
    * @return the ordine that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Ordine updateOrdine(
        it.bysoftware.ct.model.Ordine ordine)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _ordineLocalService.updateOrdine(ordine);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _ordineLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _ordineLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _ordineLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public int getNumber(int year, java.lang.String center) {
        return _ordineLocalService.getNumber(year, center);
    }

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param partnerCode
    the partner code
    * @param state
    the state
    * @param passport
    the passport number
    * @return the array of order IDs
    */
    @Override
    public java.util.List<java.lang.Object[]> getOrderByCodeStatePassport(
        java.lang.String partnerCode, int state, java.lang.String passport) {
        return _ordineLocalService.getOrderByCodeStatePassport(partnerCode,
            state, passport);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Ordine> getOrderByCustomer(
        java.lang.String code) {
        return _ordineLocalService.getOrderByCustomer(code);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Ordine> getOrderByState(
        int state) {
        return _ordineLocalService.getOrderByState(state);
    }

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param state
    the state
    * @param passport
    the passport number
    * @return the array of order IDs
    */
    @Override
    public java.util.List<java.lang.Object[]> getOrderByStatePassport(
        int state, java.lang.String passport) {
        return _ordineLocalService.getOrderByStatePassport(state, passport);
    }

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param state
    the state
    * @param passport
    the passport number
    * @return the array of order IDs
    */
    @Override
    public java.util.List<java.lang.Object[]> getOrderByStatePassportPartner(
        int state, java.lang.String passport, java.lang.String partnerCode) {
        return _ordineLocalService.getOrderByStatePassportPartner(state,
            passport, partnerCode);
    }

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param partnerCode
    the partner code
    * @param from
    from date
    * @param to
    to date
    * @return the array of order IDs
    */
    @Override
    public java.util.List<java.lang.Object[]> getOrderBetweenDatePerPartner(
        java.lang.String partnerCode, java.util.Date from, java.util.Date to) {
        return _ordineLocalService.getOrderBetweenDatePerPartner(partnerCode,
            from, to);
    }

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param from
    from date
    * @param to
    to date
    * @return the array of order IDs
    */
    @Override
    public java.util.List<java.lang.Object[]> getOrderBetweenDate(
        java.util.Date from, java.util.Date to) {
        return _ordineLocalService.getOrderBetweenDate(from, to);
    }

    @Override
    public java.util.List<java.lang.Object[]> getOrderBetweenDatePerItem(
        java.lang.String cod, java.util.Date from, java.util.Date to) {
        return _ordineLocalService.getOrderBetweenDatePerItem(cod, from, to);
    }

    @Override
    public java.util.List<java.lang.Object[]> getItemsInOrderBetweenDate(
        java.util.Date from, java.util.Date to) {
        return _ordineLocalService.getItemsInOrderBetweenDate(from, to);
    }

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param partnerCode
    the partner code
    * @param from
    from date
    * @param to
    to date
    * @return the array of order IDs
    */
    @Override
    public java.util.List<java.lang.Object[]> getOrderWithPassportByPartnerAndDate(
        java.lang.String partnerCode, java.util.Date from, java.util.Date to) {
        return _ordineLocalService.getOrderWithPassportByPartnerAndDate(partnerCode,
            from, to);
    }

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param from
    from date
    * @param to
    to date
    * @return the array of order IDs
    */
    @Override
    public java.util.List<java.lang.Object[]> getOrderWithPassportByDate(
        java.util.Date from, java.util.Date to) {
        return _ordineLocalService.getOrderWithPassportByDate(from, to);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Ordine> getCustomerConfirmedOrders(
        java.lang.String code) {
        return _ordineLocalService.getCustomerConfirmedOrders(code);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Ordine> getCustomerPendingOrders(
        java.lang.String code) {
        return _ordineLocalService.getCustomerPendingOrders(code);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Ordine> getSavedCustomerOrders(
        java.lang.String code) {
        return _ordineLocalService.getSavedCustomerOrders(code);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Ordine> getScheduledCustomerOrders(
        java.lang.String code) {
        return _ordineLocalService.getScheduledCustomerOrders(code);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Ordine> getProcessedCustomerOrders(
        java.lang.String code) {
        return _ordineLocalService.getProcessedCustomerOrders(code);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Ordine> getUnScheduledOrderByCustomer(
        java.lang.String code) {
        return _ordineLocalService.getUnScheduledOrderByCustomer(code);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Ordine> getTodayReadyOrders() {
        return _ordineLocalService.getTodayReadyOrders();
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Ordine> getTodayConfirmedOrders() {
        return _ordineLocalService.getTodayConfirmedOrders();
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Ordine> getTodayProcessedOrders() {
        return _ordineLocalService.getTodayProcessedOrders();
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Ordine> findOrders(
        java.lang.String customerCode, java.util.Date orderDate,
        java.util.Date shipDate, boolean showsInactive, boolean showsDraft,
        boolean showsPending, boolean showsApproved, boolean showsProcessed,
        boolean andSearch, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _ordineLocalService.findOrders(customerCode, orderDate,
            shipDate, showsInactive, showsDraft, showsPending, showsApproved,
            showsProcessed, andSearch, start, end, orderByComparator);
    }

    @Override
    public int getFindOrdersCount(java.lang.String customerCode,
        java.util.Date orderDate, java.util.Date shipDate,
        boolean showsInactive, boolean showsDraft, boolean showsPending,
        boolean showsApproved, boolean showsProcessed, boolean andSearch)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _ordineLocalService.getFindOrdersCount(customerCode, orderDate,
            shipDate, showsInactive, showsDraft, showsPending, showsApproved,
            showsProcessed, andSearch);
    }

    @Override
    public it.bysoftware.ct.model.Ordine getByYearNumberCenter(int n,
        java.lang.String c) {
        return _ordineLocalService.getByYearNumberCenter(n, c);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public OrdineLocalService getWrappedOrdineLocalService() {
        return _ordineLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedOrdineLocalService(
        OrdineLocalService ordineLocalService) {
        _ordineLocalService = ordineLocalService;
    }

    @Override
    public OrdineLocalService getWrappedService() {
        return _ordineLocalService;
    }

    @Override
    public void setWrappedService(OrdineLocalService ordineLocalService) {
        _ordineLocalService = ordineLocalService;
    }
}
