package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.DettagliOperazioniContabili;

import java.util.List;

/**
 * The persistence utility for the dettagli operazioni contabili service. This utility wraps {@link DettagliOperazioniContabiliPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see DettagliOperazioniContabiliPersistence
 * @see DettagliOperazioniContabiliPersistenceImpl
 * @generated
 */
public class DettagliOperazioniContabiliUtil {
    private static DettagliOperazioniContabiliPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(
        DettagliOperazioniContabili dettagliOperazioniContabili) {
        getPersistence().clearCache(dettagliOperazioniContabili);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<DettagliOperazioniContabili> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<DettagliOperazioniContabili> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<DettagliOperazioniContabili> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static DettagliOperazioniContabili update(
        DettagliOperazioniContabili dettagliOperazioniContabili)
        throws SystemException {
        return getPersistence().update(dettagliOperazioniContabili);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static DettagliOperazioniContabili update(
        DettagliOperazioniContabili dettagliOperazioniContabili,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence()
                   .update(dettagliOperazioniContabili, serviceContext);
    }

    /**
    * Returns all the dettagli operazioni contabilis where codiceOperazione = &#63;.
    *
    * @param codiceOperazione the codice operazione
    * @return the matching dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.DettagliOperazioniContabili> findByCodiceOperazione(
        java.lang.String codiceOperazione)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByCodiceOperazione(codiceOperazione);
    }

    /**
    * Returns a range of all the dettagli operazioni contabilis where codiceOperazione = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceOperazione the codice operazione
    * @param start the lower bound of the range of dettagli operazioni contabilis
    * @param end the upper bound of the range of dettagli operazioni contabilis (not inclusive)
    * @return the range of matching dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.DettagliOperazioniContabili> findByCodiceOperazione(
        java.lang.String codiceOperazione, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCodiceOperazione(codiceOperazione, start, end);
    }

    /**
    * Returns an ordered range of all the dettagli operazioni contabilis where codiceOperazione = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceOperazione the codice operazione
    * @param start the lower bound of the range of dettagli operazioni contabilis
    * @param end the upper bound of the range of dettagli operazioni contabilis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.DettagliOperazioniContabili> findByCodiceOperazione(
        java.lang.String codiceOperazione, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCodiceOperazione(codiceOperazione, start, end,
            orderByComparator);
    }

    /**
    * Returns the first dettagli operazioni contabili in the ordered set where codiceOperazione = &#63;.
    *
    * @param codiceOperazione the codice operazione
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching dettagli operazioni contabili
    * @throws it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException if a matching dettagli operazioni contabili could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DettagliOperazioniContabili findByCodiceOperazione_First(
        java.lang.String codiceOperazione,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException {
        return getPersistence()
                   .findByCodiceOperazione_First(codiceOperazione,
            orderByComparator);
    }

    /**
    * Returns the first dettagli operazioni contabili in the ordered set where codiceOperazione = &#63;.
    *
    * @param codiceOperazione the codice operazione
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching dettagli operazioni contabili, or <code>null</code> if a matching dettagli operazioni contabili could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DettagliOperazioniContabili fetchByCodiceOperazione_First(
        java.lang.String codiceOperazione,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCodiceOperazione_First(codiceOperazione,
            orderByComparator);
    }

    /**
    * Returns the last dettagli operazioni contabili in the ordered set where codiceOperazione = &#63;.
    *
    * @param codiceOperazione the codice operazione
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching dettagli operazioni contabili
    * @throws it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException if a matching dettagli operazioni contabili could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DettagliOperazioniContabili findByCodiceOperazione_Last(
        java.lang.String codiceOperazione,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException {
        return getPersistence()
                   .findByCodiceOperazione_Last(codiceOperazione,
            orderByComparator);
    }

    /**
    * Returns the last dettagli operazioni contabili in the ordered set where codiceOperazione = &#63;.
    *
    * @param codiceOperazione the codice operazione
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching dettagli operazioni contabili, or <code>null</code> if a matching dettagli operazioni contabili could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DettagliOperazioniContabili fetchByCodiceOperazione_Last(
        java.lang.String codiceOperazione,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCodiceOperazione_Last(codiceOperazione,
            orderByComparator);
    }

    /**
    * Returns the dettagli operazioni contabilis before and after the current dettagli operazioni contabili in the ordered set where codiceOperazione = &#63;.
    *
    * @param dettagliOperazioniContabiliPK the primary key of the current dettagli operazioni contabili
    * @param codiceOperazione the codice operazione
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next dettagli operazioni contabili
    * @throws it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException if a dettagli operazioni contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DettagliOperazioniContabili[] findByCodiceOperazione_PrevAndNext(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK,
        java.lang.String codiceOperazione,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException {
        return getPersistence()
                   .findByCodiceOperazione_PrevAndNext(dettagliOperazioniContabiliPK,
            codiceOperazione, orderByComparator);
    }

    /**
    * Removes all the dettagli operazioni contabilis where codiceOperazione = &#63; from the database.
    *
    * @param codiceOperazione the codice operazione
    * @throws SystemException if a system exception occurred
    */
    public static void removeByCodiceOperazione(
        java.lang.String codiceOperazione)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByCodiceOperazione(codiceOperazione);
    }

    /**
    * Returns the number of dettagli operazioni contabilis where codiceOperazione = &#63;.
    *
    * @param codiceOperazione the codice operazione
    * @return the number of matching dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    public static int countByCodiceOperazione(java.lang.String codiceOperazione)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByCodiceOperazione(codiceOperazione);
    }

    /**
    * Caches the dettagli operazioni contabili in the entity cache if it is enabled.
    *
    * @param dettagliOperazioniContabili the dettagli operazioni contabili
    */
    public static void cacheResult(
        it.bysoftware.ct.model.DettagliOperazioniContabili dettagliOperazioniContabili) {
        getPersistence().cacheResult(dettagliOperazioniContabili);
    }

    /**
    * Caches the dettagli operazioni contabilis in the entity cache if it is enabled.
    *
    * @param dettagliOperazioniContabilis the dettagli operazioni contabilis
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.DettagliOperazioniContabili> dettagliOperazioniContabilis) {
        getPersistence().cacheResult(dettagliOperazioniContabilis);
    }

    /**
    * Creates a new dettagli operazioni contabili with the primary key. Does not add the dettagli operazioni contabili to the database.
    *
    * @param dettagliOperazioniContabiliPK the primary key for the new dettagli operazioni contabili
    * @return the new dettagli operazioni contabili
    */
    public static it.bysoftware.ct.model.DettagliOperazioniContabili create(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK) {
        return getPersistence().create(dettagliOperazioniContabiliPK);
    }

    /**
    * Removes the dettagli operazioni contabili with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param dettagliOperazioniContabiliPK the primary key of the dettagli operazioni contabili
    * @return the dettagli operazioni contabili that was removed
    * @throws it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException if a dettagli operazioni contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DettagliOperazioniContabili remove(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException {
        return getPersistence().remove(dettagliOperazioniContabiliPK);
    }

    public static it.bysoftware.ct.model.DettagliOperazioniContabili updateImpl(
        it.bysoftware.ct.model.DettagliOperazioniContabili dettagliOperazioniContabili)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(dettagliOperazioniContabili);
    }

    /**
    * Returns the dettagli operazioni contabili with the primary key or throws a {@link it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException} if it could not be found.
    *
    * @param dettagliOperazioniContabiliPK the primary key of the dettagli operazioni contabili
    * @return the dettagli operazioni contabili
    * @throws it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException if a dettagli operazioni contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DettagliOperazioniContabili findByPrimaryKey(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException {
        return getPersistence().findByPrimaryKey(dettagliOperazioniContabiliPK);
    }

    /**
    * Returns the dettagli operazioni contabili with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param dettagliOperazioniContabiliPK the primary key of the dettagli operazioni contabili
    * @return the dettagli operazioni contabili, or <code>null</code> if a dettagli operazioni contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DettagliOperazioniContabili fetchByPrimaryKey(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(dettagliOperazioniContabiliPK);
    }

    /**
    * Returns all the dettagli operazioni contabilis.
    *
    * @return the dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.DettagliOperazioniContabili> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the dettagli operazioni contabilis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of dettagli operazioni contabilis
    * @param end the upper bound of the range of dettagli operazioni contabilis (not inclusive)
    * @return the range of dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.DettagliOperazioniContabili> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the dettagli operazioni contabilis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of dettagli operazioni contabilis
    * @param end the upper bound of the range of dettagli operazioni contabilis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.DettagliOperazioniContabili> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the dettagli operazioni contabilis from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of dettagli operazioni contabilis.
    *
    * @return the number of dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static DettagliOperazioniContabiliPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (DettagliOperazioniContabiliPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    DettagliOperazioniContabiliPersistence.class.getName());

            ReferenceRegistry.registerReference(DettagliOperazioniContabiliUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(
        DettagliOperazioniContabiliPersistence persistence) {
    }
}
