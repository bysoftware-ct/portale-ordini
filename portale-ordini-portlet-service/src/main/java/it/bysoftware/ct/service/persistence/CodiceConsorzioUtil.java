package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.CodiceConsorzio;

import java.util.List;

/**
 * The persistence utility for the codice consorzio service. This utility wraps {@link CodiceConsorzioPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CodiceConsorzioPersistence
 * @see CodiceConsorzioPersistenceImpl
 * @generated
 */
public class CodiceConsorzioUtil {
    private static CodiceConsorzioPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(CodiceConsorzio codiceConsorzio) {
        getPersistence().clearCache(codiceConsorzio);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<CodiceConsorzio> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<CodiceConsorzio> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<CodiceConsorzio> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static CodiceConsorzio update(CodiceConsorzio codiceConsorzio)
        throws SystemException {
        return getPersistence().update(codiceConsorzio);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static CodiceConsorzio update(CodiceConsorzio codiceConsorzio,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(codiceConsorzio, serviceContext);
    }

    /**
    * Caches the codice consorzio in the entity cache if it is enabled.
    *
    * @param codiceConsorzio the codice consorzio
    */
    public static void cacheResult(
        it.bysoftware.ct.model.CodiceConsorzio codiceConsorzio) {
        getPersistence().cacheResult(codiceConsorzio);
    }

    /**
    * Caches the codice consorzios in the entity cache if it is enabled.
    *
    * @param codiceConsorzios the codice consorzios
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.CodiceConsorzio> codiceConsorzios) {
        getPersistence().cacheResult(codiceConsorzios);
    }

    /**
    * Creates a new codice consorzio with the primary key. Does not add the codice consorzio to the database.
    *
    * @param codiceSocio the primary key for the new codice consorzio
    * @return the new codice consorzio
    */
    public static it.bysoftware.ct.model.CodiceConsorzio create(
        java.lang.String codiceSocio) {
        return getPersistence().create(codiceSocio);
    }

    /**
    * Removes the codice consorzio with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceSocio the primary key of the codice consorzio
    * @return the codice consorzio that was removed
    * @throws it.bysoftware.ct.NoSuchCodiceConsorzioException if a codice consorzio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.CodiceConsorzio remove(
        java.lang.String codiceSocio)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCodiceConsorzioException {
        return getPersistence().remove(codiceSocio);
    }

    public static it.bysoftware.ct.model.CodiceConsorzio updateImpl(
        it.bysoftware.ct.model.CodiceConsorzio codiceConsorzio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(codiceConsorzio);
    }

    /**
    * Returns the codice consorzio with the primary key or throws a {@link it.bysoftware.ct.NoSuchCodiceConsorzioException} if it could not be found.
    *
    * @param codiceSocio the primary key of the codice consorzio
    * @return the codice consorzio
    * @throws it.bysoftware.ct.NoSuchCodiceConsorzioException if a codice consorzio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.CodiceConsorzio findByPrimaryKey(
        java.lang.String codiceSocio)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCodiceConsorzioException {
        return getPersistence().findByPrimaryKey(codiceSocio);
    }

    /**
    * Returns the codice consorzio with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param codiceSocio the primary key of the codice consorzio
    * @return the codice consorzio, or <code>null</code> if a codice consorzio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.CodiceConsorzio fetchByPrimaryKey(
        java.lang.String codiceSocio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(codiceSocio);
    }

    /**
    * Returns all the codice consorzios.
    *
    * @return the codice consorzios
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.CodiceConsorzio> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the codice consorzios.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CodiceConsorzioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of codice consorzios
    * @param end the upper bound of the range of codice consorzios (not inclusive)
    * @return the range of codice consorzios
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.CodiceConsorzio> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the codice consorzios.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CodiceConsorzioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of codice consorzios
    * @param end the upper bound of the range of codice consorzios (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of codice consorzios
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.CodiceConsorzio> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the codice consorzios from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of codice consorzios.
    *
    * @return the number of codice consorzios
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static CodiceConsorzioPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (CodiceConsorzioPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    CodiceConsorzioPersistence.class.getName());

            ReferenceRegistry.registerReference(CodiceConsorzioUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(CodiceConsorzioPersistence persistence) {
    }
}
