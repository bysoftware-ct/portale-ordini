package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.Riga;

import java.util.List;

/**
 * The persistence utility for the riga service. This utility wraps {@link RigaPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see RigaPersistence
 * @see RigaPersistenceImpl
 * @generated
 */
public class RigaUtil {
    private static RigaPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Riga riga) {
        getPersistence().clearCache(riga);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Riga> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Riga> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Riga> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Riga update(Riga riga) throws SystemException {
        return getPersistence().update(riga);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Riga update(Riga riga, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(riga, serviceContext);
    }

    /**
    * Returns all the rigas where idCarrello = &#63;.
    *
    * @param idCarrello the id carrello
    * @return the matching rigas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Riga> findByIdCarello(
        long idCarrello)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByIdCarello(idCarrello);
    }

    /**
    * Returns a range of all the rigas where idCarrello = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param idCarrello the id carrello
    * @param start the lower bound of the range of rigas
    * @param end the upper bound of the range of rigas (not inclusive)
    * @return the range of matching rigas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Riga> findByIdCarello(
        long idCarrello, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByIdCarello(idCarrello, start, end);
    }

    /**
    * Returns an ordered range of all the rigas where idCarrello = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param idCarrello the id carrello
    * @param start the lower bound of the range of rigas
    * @param end the upper bound of the range of rigas (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rigas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Riga> findByIdCarello(
        long idCarrello, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByIdCarello(idCarrello, start, end, orderByComparator);
    }

    /**
    * Returns the first riga in the ordered set where idCarrello = &#63;.
    *
    * @param idCarrello the id carrello
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching riga
    * @throws it.bysoftware.ct.NoSuchRigaException if a matching riga could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Riga findByIdCarello_First(
        long idCarrello,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchRigaException {
        return getPersistence()
                   .findByIdCarello_First(idCarrello, orderByComparator);
    }

    /**
    * Returns the first riga in the ordered set where idCarrello = &#63;.
    *
    * @param idCarrello the id carrello
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching riga, or <code>null</code> if a matching riga could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Riga fetchByIdCarello_First(
        long idCarrello,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByIdCarello_First(idCarrello, orderByComparator);
    }

    /**
    * Returns the last riga in the ordered set where idCarrello = &#63;.
    *
    * @param idCarrello the id carrello
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching riga
    * @throws it.bysoftware.ct.NoSuchRigaException if a matching riga could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Riga findByIdCarello_Last(
        long idCarrello,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchRigaException {
        return getPersistence()
                   .findByIdCarello_Last(idCarrello, orderByComparator);
    }

    /**
    * Returns the last riga in the ordered set where idCarrello = &#63;.
    *
    * @param idCarrello the id carrello
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching riga, or <code>null</code> if a matching riga could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Riga fetchByIdCarello_Last(
        long idCarrello,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByIdCarello_Last(idCarrello, orderByComparator);
    }

    /**
    * Returns the rigas before and after the current riga in the ordered set where idCarrello = &#63;.
    *
    * @param id the primary key of the current riga
    * @param idCarrello the id carrello
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next riga
    * @throws it.bysoftware.ct.NoSuchRigaException if a riga with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Riga[] findByIdCarello_PrevAndNext(
        long id, long idCarrello,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchRigaException {
        return getPersistence()
                   .findByIdCarello_PrevAndNext(id, idCarrello,
            orderByComparator);
    }

    /**
    * Removes all the rigas where idCarrello = &#63; from the database.
    *
    * @param idCarrello the id carrello
    * @throws SystemException if a system exception occurred
    */
    public static void removeByIdCarello(long idCarrello)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByIdCarello(idCarrello);
    }

    /**
    * Returns the number of rigas where idCarrello = &#63;.
    *
    * @param idCarrello the id carrello
    * @return the number of matching rigas
    * @throws SystemException if a system exception occurred
    */
    public static int countByIdCarello(long idCarrello)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByIdCarello(idCarrello);
    }

    /**
    * Returns all the rigas where passaporto = &#63;.
    *
    * @param passaporto the passaporto
    * @return the matching rigas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Riga> findByfornitorePassaporto(
        java.lang.String passaporto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByfornitorePassaporto(passaporto);
    }

    /**
    * Returns a range of all the rigas where passaporto = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param passaporto the passaporto
    * @param start the lower bound of the range of rigas
    * @param end the upper bound of the range of rigas (not inclusive)
    * @return the range of matching rigas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Riga> findByfornitorePassaporto(
        java.lang.String passaporto, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByfornitorePassaporto(passaporto, start, end);
    }

    /**
    * Returns an ordered range of all the rigas where passaporto = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param passaporto the passaporto
    * @param start the lower bound of the range of rigas
    * @param end the upper bound of the range of rigas (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rigas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Riga> findByfornitorePassaporto(
        java.lang.String passaporto, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByfornitorePassaporto(passaporto, start, end,
            orderByComparator);
    }

    /**
    * Returns the first riga in the ordered set where passaporto = &#63;.
    *
    * @param passaporto the passaporto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching riga
    * @throws it.bysoftware.ct.NoSuchRigaException if a matching riga could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Riga findByfornitorePassaporto_First(
        java.lang.String passaporto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchRigaException {
        return getPersistence()
                   .findByfornitorePassaporto_First(passaporto,
            orderByComparator);
    }

    /**
    * Returns the first riga in the ordered set where passaporto = &#63;.
    *
    * @param passaporto the passaporto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching riga, or <code>null</code> if a matching riga could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Riga fetchByfornitorePassaporto_First(
        java.lang.String passaporto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByfornitorePassaporto_First(passaporto,
            orderByComparator);
    }

    /**
    * Returns the last riga in the ordered set where passaporto = &#63;.
    *
    * @param passaporto the passaporto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching riga
    * @throws it.bysoftware.ct.NoSuchRigaException if a matching riga could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Riga findByfornitorePassaporto_Last(
        java.lang.String passaporto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchRigaException {
        return getPersistence()
                   .findByfornitorePassaporto_Last(passaporto, orderByComparator);
    }

    /**
    * Returns the last riga in the ordered set where passaporto = &#63;.
    *
    * @param passaporto the passaporto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching riga, or <code>null</code> if a matching riga could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Riga fetchByfornitorePassaporto_Last(
        java.lang.String passaporto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByfornitorePassaporto_Last(passaporto,
            orderByComparator);
    }

    /**
    * Returns the rigas before and after the current riga in the ordered set where passaporto = &#63;.
    *
    * @param id the primary key of the current riga
    * @param passaporto the passaporto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next riga
    * @throws it.bysoftware.ct.NoSuchRigaException if a riga with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Riga[] findByfornitorePassaporto_PrevAndNext(
        long id, java.lang.String passaporto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchRigaException {
        return getPersistence()
                   .findByfornitorePassaporto_PrevAndNext(id, passaporto,
            orderByComparator);
    }

    /**
    * Removes all the rigas where passaporto = &#63; from the database.
    *
    * @param passaporto the passaporto
    * @throws SystemException if a system exception occurred
    */
    public static void removeByfornitorePassaporto(java.lang.String passaporto)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByfornitorePassaporto(passaporto);
    }

    /**
    * Returns the number of rigas where passaporto = &#63;.
    *
    * @param passaporto the passaporto
    * @return the number of matching rigas
    * @throws SystemException if a system exception occurred
    */
    public static int countByfornitorePassaporto(java.lang.String passaporto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByfornitorePassaporto(passaporto);
    }

    /**
    * Caches the riga in the entity cache if it is enabled.
    *
    * @param riga the riga
    */
    public static void cacheResult(it.bysoftware.ct.model.Riga riga) {
        getPersistence().cacheResult(riga);
    }

    /**
    * Caches the rigas in the entity cache if it is enabled.
    *
    * @param rigas the rigas
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.Riga> rigas) {
        getPersistence().cacheResult(rigas);
    }

    /**
    * Creates a new riga with the primary key. Does not add the riga to the database.
    *
    * @param id the primary key for the new riga
    * @return the new riga
    */
    public static it.bysoftware.ct.model.Riga create(long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the riga with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the riga
    * @return the riga that was removed
    * @throws it.bysoftware.ct.NoSuchRigaException if a riga with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Riga remove(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchRigaException {
        return getPersistence().remove(id);
    }

    public static it.bysoftware.ct.model.Riga updateImpl(
        it.bysoftware.ct.model.Riga riga)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(riga);
    }

    /**
    * Returns the riga with the primary key or throws a {@link it.bysoftware.ct.NoSuchRigaException} if it could not be found.
    *
    * @param id the primary key of the riga
    * @return the riga
    * @throws it.bysoftware.ct.NoSuchRigaException if a riga with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Riga findByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchRigaException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the riga with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the riga
    * @return the riga, or <code>null</code> if a riga with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Riga fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the rigas.
    *
    * @return the rigas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Riga> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the rigas.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of rigas
    * @param end the upper bound of the range of rigas (not inclusive)
    * @return the range of rigas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Riga> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the rigas.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of rigas
    * @param end the upper bound of the range of rigas (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of rigas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Riga> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the rigas from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of rigas.
    *
    * @return the number of rigas
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static RigaPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (RigaPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    RigaPersistence.class.getName());

            ReferenceRegistry.registerReference(RigaUtil.class, "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(RigaPersistence persistence) {
    }
}
