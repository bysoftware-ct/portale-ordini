package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CausaliEstrattoContoLocalService}.
 *
 * @author Mario Torrisi
 * @see CausaliEstrattoContoLocalService
 * @generated
 */
public class CausaliEstrattoContoLocalServiceWrapper
    implements CausaliEstrattoContoLocalService,
        ServiceWrapper<CausaliEstrattoContoLocalService> {
    private CausaliEstrattoContoLocalService _causaliEstrattoContoLocalService;

    public CausaliEstrattoContoLocalServiceWrapper(
        CausaliEstrattoContoLocalService causaliEstrattoContoLocalService) {
        _causaliEstrattoContoLocalService = causaliEstrattoContoLocalService;
    }

    /**
    * Adds the causali estratto conto to the database. Also notifies the appropriate model listeners.
    *
    * @param causaliEstrattoConto the causali estratto conto
    * @return the causali estratto conto that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.CausaliEstrattoConto addCausaliEstrattoConto(
        it.bysoftware.ct.model.CausaliEstrattoConto causaliEstrattoConto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliEstrattoContoLocalService.addCausaliEstrattoConto(causaliEstrattoConto);
    }

    /**
    * Creates a new causali estratto conto with the primary key. Does not add the causali estratto conto to the database.
    *
    * @param codiceCausaleEC the primary key for the new causali estratto conto
    * @return the new causali estratto conto
    */
    @Override
    public it.bysoftware.ct.model.CausaliEstrattoConto createCausaliEstrattoConto(
        java.lang.String codiceCausaleEC) {
        return _causaliEstrattoContoLocalService.createCausaliEstrattoConto(codiceCausaleEC);
    }

    /**
    * Deletes the causali estratto conto with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceCausaleEC the primary key of the causali estratto conto
    * @return the causali estratto conto that was removed
    * @throws PortalException if a causali estratto conto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.CausaliEstrattoConto deleteCausaliEstrattoConto(
        java.lang.String codiceCausaleEC)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _causaliEstrattoContoLocalService.deleteCausaliEstrattoConto(codiceCausaleEC);
    }

    /**
    * Deletes the causali estratto conto from the database. Also notifies the appropriate model listeners.
    *
    * @param causaliEstrattoConto the causali estratto conto
    * @return the causali estratto conto that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.CausaliEstrattoConto deleteCausaliEstrattoConto(
        it.bysoftware.ct.model.CausaliEstrattoConto causaliEstrattoConto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliEstrattoContoLocalService.deleteCausaliEstrattoConto(causaliEstrattoConto);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _causaliEstrattoContoLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliEstrattoContoLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CausaliEstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliEstrattoContoLocalService.dynamicQuery(dynamicQuery,
            start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CausaliEstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliEstrattoContoLocalService.dynamicQuery(dynamicQuery,
            start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliEstrattoContoLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliEstrattoContoLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public it.bysoftware.ct.model.CausaliEstrattoConto fetchCausaliEstrattoConto(
        java.lang.String codiceCausaleEC)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliEstrattoContoLocalService.fetchCausaliEstrattoConto(codiceCausaleEC);
    }

    /**
    * Returns the causali estratto conto with the primary key.
    *
    * @param codiceCausaleEC the primary key of the causali estratto conto
    * @return the causali estratto conto
    * @throws PortalException if a causali estratto conto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.CausaliEstrattoConto getCausaliEstrattoConto(
        java.lang.String codiceCausaleEC)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _causaliEstrattoContoLocalService.getCausaliEstrattoConto(codiceCausaleEC);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _causaliEstrattoContoLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the causali estratto contos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CausaliEstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of causali estratto contos
    * @param end the upper bound of the range of causali estratto contos (not inclusive)
    * @return the range of causali estratto contos
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.CausaliEstrattoConto> getCausaliEstrattoContos(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliEstrattoContoLocalService.getCausaliEstrattoContos(start,
            end);
    }

    /**
    * Returns the number of causali estratto contos.
    *
    * @return the number of causali estratto contos
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getCausaliEstrattoContosCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliEstrattoContoLocalService.getCausaliEstrattoContosCount();
    }

    /**
    * Updates the causali estratto conto in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param causaliEstrattoConto the causali estratto conto
    * @return the causali estratto conto that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.CausaliEstrattoConto updateCausaliEstrattoConto(
        it.bysoftware.ct.model.CausaliEstrattoConto causaliEstrattoConto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliEstrattoContoLocalService.updateCausaliEstrattoConto(causaliEstrattoConto);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _causaliEstrattoContoLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _causaliEstrattoContoLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _causaliEstrattoContoLocalService.invokeMethod(name,
            parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public CausaliEstrattoContoLocalService getWrappedCausaliEstrattoContoLocalService() {
        return _causaliEstrattoContoLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedCausaliEstrattoContoLocalService(
        CausaliEstrattoContoLocalService causaliEstrattoContoLocalService) {
        _causaliEstrattoContoLocalService = causaliEstrattoContoLocalService;
    }

    @Override
    public CausaliEstrattoContoLocalService getWrappedService() {
        return _causaliEstrattoContoLocalService;
    }

    @Override
    public void setWrappedService(
        CausaliEstrattoContoLocalService causaliEstrattoContoLocalService) {
        _causaliEstrattoContoLocalService = causaliEstrattoContoLocalService;
    }
}
