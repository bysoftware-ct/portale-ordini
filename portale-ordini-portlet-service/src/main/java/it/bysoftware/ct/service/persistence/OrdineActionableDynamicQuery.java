package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.Ordine;
import it.bysoftware.ct.service.OrdineLocalServiceUtil;

/**
 * @author Mario Torrisi
 * @generated
 */
public abstract class OrdineActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public OrdineActionableDynamicQuery() throws SystemException {
        setBaseLocalService(OrdineLocalServiceUtil.getService());
        setClass(Ordine.class);

        setClassLoader(it.bysoftware.ct.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
