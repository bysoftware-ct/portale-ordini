package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.Rubrica;

import java.util.List;

/**
 * The persistence utility for the rubrica service. This utility wraps {@link RubricaPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see RubricaPersistence
 * @see RubricaPersistenceImpl
 * @generated
 */
public class RubricaUtil {
    private static RubricaPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Rubrica rubrica) {
        getPersistence().clearCache(rubrica);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Rubrica> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Rubrica> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Rubrica> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Rubrica update(Rubrica rubrica) throws SystemException {
        return getPersistence().update(rubrica);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Rubrica update(Rubrica rubrica, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(rubrica, serviceContext);
    }

    /**
    * Returns all the rubricas where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @return the matching rubricas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Rubrica> findByCodiceSoggetto(
        java.lang.String codiceSoggetto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByCodiceSoggetto(codiceSoggetto);
    }

    /**
    * Returns a range of all the rubricas where codiceSoggetto = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RubricaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceSoggetto the codice soggetto
    * @param start the lower bound of the range of rubricas
    * @param end the upper bound of the range of rubricas (not inclusive)
    * @return the range of matching rubricas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Rubrica> findByCodiceSoggetto(
        java.lang.String codiceSoggetto, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByCodiceSoggetto(codiceSoggetto, start, end);
    }

    /**
    * Returns an ordered range of all the rubricas where codiceSoggetto = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RubricaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceSoggetto the codice soggetto
    * @param start the lower bound of the range of rubricas
    * @param end the upper bound of the range of rubricas (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rubricas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Rubrica> findByCodiceSoggetto(
        java.lang.String codiceSoggetto, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCodiceSoggetto(codiceSoggetto, start, end,
            orderByComparator);
    }

    /**
    * Returns the first rubrica in the ordered set where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching rubrica
    * @throws it.bysoftware.ct.NoSuchRubricaException if a matching rubrica could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Rubrica findByCodiceSoggetto_First(
        java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchRubricaException {
        return getPersistence()
                   .findByCodiceSoggetto_First(codiceSoggetto, orderByComparator);
    }

    /**
    * Returns the first rubrica in the ordered set where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching rubrica, or <code>null</code> if a matching rubrica could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Rubrica fetchByCodiceSoggetto_First(
        java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCodiceSoggetto_First(codiceSoggetto,
            orderByComparator);
    }

    /**
    * Returns the last rubrica in the ordered set where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching rubrica
    * @throws it.bysoftware.ct.NoSuchRubricaException if a matching rubrica could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Rubrica findByCodiceSoggetto_Last(
        java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchRubricaException {
        return getPersistence()
                   .findByCodiceSoggetto_Last(codiceSoggetto, orderByComparator);
    }

    /**
    * Returns the last rubrica in the ordered set where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching rubrica, or <code>null</code> if a matching rubrica could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Rubrica fetchByCodiceSoggetto_Last(
        java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCodiceSoggetto_Last(codiceSoggetto, orderByComparator);
    }

    /**
    * Returns the rubricas before and after the current rubrica in the ordered set where codiceSoggetto = &#63;.
    *
    * @param rubricaPK the primary key of the current rubrica
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next rubrica
    * @throws it.bysoftware.ct.NoSuchRubricaException if a rubrica with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Rubrica[] findByCodiceSoggetto_PrevAndNext(
        it.bysoftware.ct.service.persistence.RubricaPK rubricaPK,
        java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchRubricaException {
        return getPersistence()
                   .findByCodiceSoggetto_PrevAndNext(rubricaPK, codiceSoggetto,
            orderByComparator);
    }

    /**
    * Removes all the rubricas where codiceSoggetto = &#63; from the database.
    *
    * @param codiceSoggetto the codice soggetto
    * @throws SystemException if a system exception occurred
    */
    public static void removeByCodiceSoggetto(java.lang.String codiceSoggetto)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByCodiceSoggetto(codiceSoggetto);
    }

    /**
    * Returns the number of rubricas where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @return the number of matching rubricas
    * @throws SystemException if a system exception occurred
    */
    public static int countByCodiceSoggetto(java.lang.String codiceSoggetto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByCodiceSoggetto(codiceSoggetto);
    }

    /**
    * Caches the rubrica in the entity cache if it is enabled.
    *
    * @param rubrica the rubrica
    */
    public static void cacheResult(it.bysoftware.ct.model.Rubrica rubrica) {
        getPersistence().cacheResult(rubrica);
    }

    /**
    * Caches the rubricas in the entity cache if it is enabled.
    *
    * @param rubricas the rubricas
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.Rubrica> rubricas) {
        getPersistence().cacheResult(rubricas);
    }

    /**
    * Creates a new rubrica with the primary key. Does not add the rubrica to the database.
    *
    * @param rubricaPK the primary key for the new rubrica
    * @return the new rubrica
    */
    public static it.bysoftware.ct.model.Rubrica create(
        it.bysoftware.ct.service.persistence.RubricaPK rubricaPK) {
        return getPersistence().create(rubricaPK);
    }

    /**
    * Removes the rubrica with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param rubricaPK the primary key of the rubrica
    * @return the rubrica that was removed
    * @throws it.bysoftware.ct.NoSuchRubricaException if a rubrica with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Rubrica remove(
        it.bysoftware.ct.service.persistence.RubricaPK rubricaPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchRubricaException {
        return getPersistence().remove(rubricaPK);
    }

    public static it.bysoftware.ct.model.Rubrica updateImpl(
        it.bysoftware.ct.model.Rubrica rubrica)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(rubrica);
    }

    /**
    * Returns the rubrica with the primary key or throws a {@link it.bysoftware.ct.NoSuchRubricaException} if it could not be found.
    *
    * @param rubricaPK the primary key of the rubrica
    * @return the rubrica
    * @throws it.bysoftware.ct.NoSuchRubricaException if a rubrica with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Rubrica findByPrimaryKey(
        it.bysoftware.ct.service.persistence.RubricaPK rubricaPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchRubricaException {
        return getPersistence().findByPrimaryKey(rubricaPK);
    }

    /**
    * Returns the rubrica with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param rubricaPK the primary key of the rubrica
    * @return the rubrica, or <code>null</code> if a rubrica with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Rubrica fetchByPrimaryKey(
        it.bysoftware.ct.service.persistence.RubricaPK rubricaPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(rubricaPK);
    }

    /**
    * Returns all the rubricas.
    *
    * @return the rubricas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Rubrica> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the rubricas.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RubricaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of rubricas
    * @param end the upper bound of the range of rubricas (not inclusive)
    * @return the range of rubricas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Rubrica> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the rubricas.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RubricaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of rubricas
    * @param end the upper bound of the range of rubricas (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of rubricas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Rubrica> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the rubricas from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of rubricas.
    *
    * @return the number of rubricas
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static RubricaPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (RubricaPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    RubricaPersistence.class.getName());

            ReferenceRegistry.registerReference(RubricaUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(RubricaPersistence persistence) {
    }
}
