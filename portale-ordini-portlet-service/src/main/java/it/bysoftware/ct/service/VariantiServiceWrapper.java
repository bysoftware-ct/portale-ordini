package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VariantiService}.
 *
 * @author Mario Torrisi
 * @see VariantiService
 * @generated
 */
public class VariantiServiceWrapper implements VariantiService,
    ServiceWrapper<VariantiService> {
    private VariantiService _variantiService;

    public VariantiServiceWrapper(VariantiService variantiService) {
        _variantiService = variantiService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _variantiService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _variantiService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _variantiService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public VariantiService getWrappedVariantiService() {
        return _variantiService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedVariantiService(VariantiService variantiService) {
        _variantiService = variantiService;
    }

    @Override
    public VariantiService getWrappedService() {
        return _variantiService;
    }

    @Override
    public void setWrappedService(VariantiService variantiService) {
        _variantiService = variantiService;
    }
}
