package it.bysoftware.ct.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Pagamento. This utility wraps
 * {@link it.bysoftware.ct.service.impl.PagamentoLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Mario Torrisi
 * @see PagamentoLocalService
 * @see it.bysoftware.ct.service.base.PagamentoLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.impl.PagamentoLocalServiceImpl
 * @generated
 */
public class PagamentoLocalServiceUtil {
    private static PagamentoLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link it.bysoftware.ct.service.impl.PagamentoLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the pagamento to the database. Also notifies the appropriate model listeners.
    *
    * @param pagamento the pagamento
    * @return the pagamento that was added
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Pagamento addPagamento(
        it.bysoftware.ct.model.Pagamento pagamento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addPagamento(pagamento);
    }

    /**
    * Creates a new pagamento with the primary key. Does not add the pagamento to the database.
    *
    * @param pagamentoPK the primary key for the new pagamento
    * @return the new pagamento
    */
    public static it.bysoftware.ct.model.Pagamento createPagamento(
        it.bysoftware.ct.service.persistence.PagamentoPK pagamentoPK) {
        return getService().createPagamento(pagamentoPK);
    }

    /**
    * Deletes the pagamento with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param pagamentoPK the primary key of the pagamento
    * @return the pagamento that was removed
    * @throws PortalException if a pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Pagamento deletePagamento(
        it.bysoftware.ct.service.persistence.PagamentoPK pagamentoPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deletePagamento(pagamentoPK);
    }

    /**
    * Deletes the pagamento from the database. Also notifies the appropriate model listeners.
    *
    * @param pagamento the pagamento
    * @return the pagamento that was removed
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Pagamento deletePagamento(
        it.bysoftware.ct.model.Pagamento pagamento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deletePagamento(pagamento);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static it.bysoftware.ct.model.Pagamento fetchPagamento(
        it.bysoftware.ct.service.persistence.PagamentoPK pagamentoPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchPagamento(pagamentoPK);
    }

    /**
    * Returns the pagamento with the primary key.
    *
    * @param pagamentoPK the primary key of the pagamento
    * @return the pagamento
    * @throws PortalException if a pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Pagamento getPagamento(
        it.bysoftware.ct.service.persistence.PagamentoPK pagamentoPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPagamento(pagamentoPK);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the pagamentos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of pagamentos
    * @param end the upper bound of the range of pagamentos (not inclusive)
    * @return the range of pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Pagamento> getPagamentos(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPagamentos(start, end);
    }

    /**
    * Returns the number of pagamentos.
    *
    * @return the number of pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static int getPagamentosCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPagamentosCount();
    }

    /**
    * Updates the pagamento in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param pagamento the pagamento
    * @return the pagamento that was updated
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Pagamento updatePagamento(
        it.bysoftware.ct.model.Pagamento pagamento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updatePagamento(pagamento);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static int getNumber(int year, java.lang.String partner) {
        return getService().getNumber(year, partner);
    }

    public static java.util.List<it.bysoftware.ct.model.Pagamento> findByYear(
        int year) {
        return getService().findByYear(year);
    }

    public static java.util.List<it.bysoftware.ct.model.Pagamento> findByYearAndState(
        int year, java.lang.Integer state) {
        return getService().findByYearAndState(year, state);
    }

    public static void clearService() {
        _service = null;
    }

    public static PagamentoLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    PagamentoLocalService.class.getName());

            if (invokableLocalService instanceof PagamentoLocalService) {
                _service = (PagamentoLocalService) invokableLocalService;
            } else {
                _service = new PagamentoLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(PagamentoLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(PagamentoLocalService service) {
    }
}
