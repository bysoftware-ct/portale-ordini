package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PianteLocalService}.
 *
 * @author Mario Torrisi
 * @see PianteLocalService
 * @generated
 */
public class PianteLocalServiceWrapper implements PianteLocalService,
    ServiceWrapper<PianteLocalService> {
    private PianteLocalService _pianteLocalService;

    public PianteLocalServiceWrapper(PianteLocalService pianteLocalService) {
        _pianteLocalService = pianteLocalService;
    }

    /**
    * Adds the piante to the database. Also notifies the appropriate model listeners.
    *
    * @param piante the piante
    * @return the piante that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Piante addPiante(
        it.bysoftware.ct.model.Piante piante)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pianteLocalService.addPiante(piante);
    }

    /**
    * Creates a new piante with the primary key. Does not add the piante to the database.
    *
    * @param id the primary key for the new piante
    * @return the new piante
    */
    @Override
    public it.bysoftware.ct.model.Piante createPiante(long id) {
        return _pianteLocalService.createPiante(id);
    }

    /**
    * Deletes the piante with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the piante
    * @return the piante that was removed
    * @throws PortalException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Piante deletePiante(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _pianteLocalService.deletePiante(id);
    }

    /**
    * Deletes the piante from the database. Also notifies the appropriate model listeners.
    *
    * @param piante the piante
    * @return the piante that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Piante deletePiante(
        it.bysoftware.ct.model.Piante piante)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pianteLocalService.deletePiante(piante);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _pianteLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pianteLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _pianteLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pianteLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pianteLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pianteLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public it.bysoftware.ct.model.Piante fetchPiante(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pianteLocalService.fetchPiante(id);
    }

    /**
    * Returns the piante with the primary key.
    *
    * @param id the primary key of the piante
    * @return the piante
    * @throws PortalException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Piante getPiante(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _pianteLocalService.getPiante(id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _pianteLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the piantes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of piantes
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.Piante> getPiantes(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _pianteLocalService.getPiantes(start, end);
    }

    /**
    * Returns the number of piantes.
    *
    * @return the number of piantes
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getPiantesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pianteLocalService.getPiantesCount();
    }

    /**
    * Updates the piante in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param piante the piante
    * @return the piante that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Piante updatePiante(
        it.bysoftware.ct.model.Piante piante)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pianteLocalService.updatePiante(piante);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _pianteLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _pianteLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _pianteLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Piante> findPlants(
        long idCategory, boolean active, boolean inactive,
        java.lang.String idSupplier, java.lang.String name,
        java.lang.String code, boolean obsolete) {
        return _pianteLocalService.findPlants(idCategory, active, inactive,
            idSupplier, name, code, obsolete);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Piante> findPlants(
        boolean active, boolean inactive, java.lang.String idSupplier,
        java.lang.String name, java.lang.String code, boolean obsolete) {
        return _pianteLocalService.findPlants(active, inactive, idSupplier,
            name, code, obsolete);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Piante> findPlants(
        boolean active, boolean inactive, java.lang.String idSupplier,
        java.lang.String name, java.lang.String code, boolean obsolete,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator) {
        return _pianteLocalService.findPlants(active, inactive, idSupplier,
            name, code, obsolete, orderByComparator);
    }

    @Override
    public it.bysoftware.ct.model.Piante findPlants(java.lang.String code) {
        return _pianteLocalService.findPlants(code);
    }

    /**
    * Returns a {@link List} of {@link Object} array representing the items
    * found.
    *
    * @param category
    the select category
    * @param code
    item code
    * @param name
    item name
    * @param vase
    vase
    * @param shape
    item shape
    * @param height
    item height
    * @param supplier
    supplier
    * @param active
    true for only active items, false otherwise
    * @param available
    true for only available items, false otherwise
    * @param obsolete
    * @return the item list
    */
    @Override
    public java.util.List<java.lang.Object[]> getItemsByFilters(int category,
        java.lang.String code, java.lang.String name, java.lang.String vase,
        java.lang.String shape, int height, java.lang.String supplier,
        boolean active, boolean inactive, boolean available, boolean obsolete) {
        return _pianteLocalService.getItemsByFilters(category, code, name,
            vase, shape, height, supplier, active, inactive, available, obsolete);
    }

    /**
    * Returns the list of not associated partner items.
    *
    * @param partner
    the partner code
    * @return list of not associated partner items
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.Piante> getUnassociatedItems(
        java.lang.String partner) {
        return _pianteLocalService.getUnassociatedItems(partner);
    }

    /**
    * Returns the list of associated partner items.
    *
    * @param partner
    the partner code
    * @return list of associated partner items
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.Piante> getAssociatedItems(
        java.lang.String partner) {
        return _pianteLocalService.getAssociatedItems(partner);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public PianteLocalService getWrappedPianteLocalService() {
        return _pianteLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedPianteLocalService(
        PianteLocalService pianteLocalService) {
        _pianteLocalService = pianteLocalService;
    }

    @Override
    public PianteLocalService getWrappedService() {
        return _pianteLocalService;
    }

    @Override
    public void setWrappedService(PianteLocalService pianteLocalService) {
        _pianteLocalService = pianteLocalService;
    }
}
