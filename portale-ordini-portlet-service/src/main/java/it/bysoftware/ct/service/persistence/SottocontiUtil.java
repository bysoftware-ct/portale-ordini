package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.Sottoconti;

import java.util.List;

/**
 * The persistence utility for the sottoconti service. This utility wraps {@link SottocontiPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see SottocontiPersistence
 * @see SottocontiPersistenceImpl
 * @generated
 */
public class SottocontiUtil {
    private static SottocontiPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Sottoconti sottoconti) {
        getPersistence().clearCache(sottoconti);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Sottoconti> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Sottoconti> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Sottoconti> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Sottoconti update(Sottoconti sottoconti)
        throws SystemException {
        return getPersistence().update(sottoconti);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Sottoconti update(Sottoconti sottoconti,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(sottoconti, serviceContext);
    }

    /**
    * Caches the sottoconti in the entity cache if it is enabled.
    *
    * @param sottoconti the sottoconti
    */
    public static void cacheResult(it.bysoftware.ct.model.Sottoconti sottoconti) {
        getPersistence().cacheResult(sottoconti);
    }

    /**
    * Caches the sottocontis in the entity cache if it is enabled.
    *
    * @param sottocontis the sottocontis
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.Sottoconti> sottocontis) {
        getPersistence().cacheResult(sottocontis);
    }

    /**
    * Creates a new sottoconti with the primary key. Does not add the sottoconti to the database.
    *
    * @param codice the primary key for the new sottoconti
    * @return the new sottoconti
    */
    public static it.bysoftware.ct.model.Sottoconti create(
        java.lang.String codice) {
        return getPersistence().create(codice);
    }

    /**
    * Removes the sottoconti with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codice the primary key of the sottoconti
    * @return the sottoconti that was removed
    * @throws it.bysoftware.ct.NoSuchSottocontiException if a sottoconti with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Sottoconti remove(
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSottocontiException {
        return getPersistence().remove(codice);
    }

    public static it.bysoftware.ct.model.Sottoconti updateImpl(
        it.bysoftware.ct.model.Sottoconti sottoconti)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(sottoconti);
    }

    /**
    * Returns the sottoconti with the primary key or throws a {@link it.bysoftware.ct.NoSuchSottocontiException} if it could not be found.
    *
    * @param codice the primary key of the sottoconti
    * @return the sottoconti
    * @throws it.bysoftware.ct.NoSuchSottocontiException if a sottoconti with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Sottoconti findByPrimaryKey(
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSottocontiException {
        return getPersistence().findByPrimaryKey(codice);
    }

    /**
    * Returns the sottoconti with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param codice the primary key of the sottoconti
    * @return the sottoconti, or <code>null</code> if a sottoconti with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Sottoconti fetchByPrimaryKey(
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(codice);
    }

    /**
    * Returns all the sottocontis.
    *
    * @return the sottocontis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Sottoconti> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the sottocontis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SottocontiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of sottocontis
    * @param end the upper bound of the range of sottocontis (not inclusive)
    * @return the range of sottocontis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Sottoconti> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the sottocontis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SottocontiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of sottocontis
    * @param end the upper bound of the range of sottocontis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of sottocontis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Sottoconti> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the sottocontis from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of sottocontis.
    *
    * @return the number of sottocontis
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static SottocontiPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (SottocontiPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    SottocontiPersistence.class.getName());

            ReferenceRegistry.registerReference(SottocontiUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(SottocontiPersistence persistence) {
    }
}
