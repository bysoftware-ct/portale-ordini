package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VuotoService}.
 *
 * @author Mario Torrisi
 * @see VuotoService
 * @generated
 */
public class VuotoServiceWrapper implements VuotoService,
    ServiceWrapper<VuotoService> {
    private VuotoService _vuotoService;

    public VuotoServiceWrapper(VuotoService vuotoService) {
        _vuotoService = vuotoService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _vuotoService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _vuotoService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _vuotoService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public VuotoService getWrappedVuotoService() {
        return _vuotoService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedVuotoService(VuotoService vuotoService) {
        _vuotoService = vuotoService;
    }

    @Override
    public VuotoService getWrappedService() {
        return _vuotoService;
    }

    @Override
    public void setWrappedService(VuotoService vuotoService) {
        _vuotoService = vuotoService;
    }
}
