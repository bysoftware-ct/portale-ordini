package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.CausaliEstrattoConto;

import java.util.List;

/**
 * The persistence utility for the causali estratto conto service. This utility wraps {@link CausaliEstrattoContoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CausaliEstrattoContoPersistence
 * @see CausaliEstrattoContoPersistenceImpl
 * @generated
 */
public class CausaliEstrattoContoUtil {
    private static CausaliEstrattoContoPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(CausaliEstrattoConto causaliEstrattoConto) {
        getPersistence().clearCache(causaliEstrattoConto);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<CausaliEstrattoConto> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<CausaliEstrattoConto> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<CausaliEstrattoConto> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static CausaliEstrattoConto update(
        CausaliEstrattoConto causaliEstrattoConto) throws SystemException {
        return getPersistence().update(causaliEstrattoConto);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static CausaliEstrattoConto update(
        CausaliEstrattoConto causaliEstrattoConto, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(causaliEstrattoConto, serviceContext);
    }

    /**
    * Caches the causali estratto conto in the entity cache if it is enabled.
    *
    * @param causaliEstrattoConto the causali estratto conto
    */
    public static void cacheResult(
        it.bysoftware.ct.model.CausaliEstrattoConto causaliEstrattoConto) {
        getPersistence().cacheResult(causaliEstrattoConto);
    }

    /**
    * Caches the causali estratto contos in the entity cache if it is enabled.
    *
    * @param causaliEstrattoContos the causali estratto contos
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.CausaliEstrattoConto> causaliEstrattoContos) {
        getPersistence().cacheResult(causaliEstrattoContos);
    }

    /**
    * Creates a new causali estratto conto with the primary key. Does not add the causali estratto conto to the database.
    *
    * @param codiceCausaleEC the primary key for the new causali estratto conto
    * @return the new causali estratto conto
    */
    public static it.bysoftware.ct.model.CausaliEstrattoConto create(
        java.lang.String codiceCausaleEC) {
        return getPersistence().create(codiceCausaleEC);
    }

    /**
    * Removes the causali estratto conto with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceCausaleEC the primary key of the causali estratto conto
    * @return the causali estratto conto that was removed
    * @throws it.bysoftware.ct.NoSuchCausaliEstrattoContoException if a causali estratto conto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.CausaliEstrattoConto remove(
        java.lang.String codiceCausaleEC)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCausaliEstrattoContoException {
        return getPersistence().remove(codiceCausaleEC);
    }

    public static it.bysoftware.ct.model.CausaliEstrattoConto updateImpl(
        it.bysoftware.ct.model.CausaliEstrattoConto causaliEstrattoConto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(causaliEstrattoConto);
    }

    /**
    * Returns the causali estratto conto with the primary key or throws a {@link it.bysoftware.ct.NoSuchCausaliEstrattoContoException} if it could not be found.
    *
    * @param codiceCausaleEC the primary key of the causali estratto conto
    * @return the causali estratto conto
    * @throws it.bysoftware.ct.NoSuchCausaliEstrattoContoException if a causali estratto conto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.CausaliEstrattoConto findByPrimaryKey(
        java.lang.String codiceCausaleEC)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCausaliEstrattoContoException {
        return getPersistence().findByPrimaryKey(codiceCausaleEC);
    }

    /**
    * Returns the causali estratto conto with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param codiceCausaleEC the primary key of the causali estratto conto
    * @return the causali estratto conto, or <code>null</code> if a causali estratto conto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.CausaliEstrattoConto fetchByPrimaryKey(
        java.lang.String codiceCausaleEC)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(codiceCausaleEC);
    }

    /**
    * Returns all the causali estratto contos.
    *
    * @return the causali estratto contos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.CausaliEstrattoConto> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the causali estratto contos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CausaliEstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of causali estratto contos
    * @param end the upper bound of the range of causali estratto contos (not inclusive)
    * @return the range of causali estratto contos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.CausaliEstrattoConto> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the causali estratto contos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CausaliEstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of causali estratto contos
    * @param end the upper bound of the range of causali estratto contos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of causali estratto contos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.CausaliEstrattoConto> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the causali estratto contos from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of causali estratto contos.
    *
    * @return the number of causali estratto contos
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static CausaliEstrattoContoPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (CausaliEstrattoContoPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    CausaliEstrattoContoPersistence.class.getName());

            ReferenceRegistry.registerReference(CausaliEstrattoContoUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(CausaliEstrattoContoPersistence persistence) {
    }
}
