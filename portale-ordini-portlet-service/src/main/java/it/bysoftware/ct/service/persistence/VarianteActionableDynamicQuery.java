package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.Variante;
import it.bysoftware.ct.service.VarianteLocalServiceUtil;

/**
 * @author Mario Torrisi
 * @generated
 */
public abstract class VarianteActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public VarianteActionableDynamicQuery() throws SystemException {
        setBaseLocalService(VarianteLocalServiceUtil.getService());
        setClass(Variante.class);

        setClassLoader(it.bysoftware.ct.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
