package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link MovimentoVuotoService}.
 *
 * @author Mario Torrisi
 * @see MovimentoVuotoService
 * @generated
 */
public class MovimentoVuotoServiceWrapper implements MovimentoVuotoService,
    ServiceWrapper<MovimentoVuotoService> {
    private MovimentoVuotoService _movimentoVuotoService;

    public MovimentoVuotoServiceWrapper(
        MovimentoVuotoService movimentoVuotoService) {
        _movimentoVuotoService = movimentoVuotoService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _movimentoVuotoService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _movimentoVuotoService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _movimentoVuotoService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public MovimentoVuotoService getWrappedMovimentoVuotoService() {
        return _movimentoVuotoService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedMovimentoVuotoService(
        MovimentoVuotoService movimentoVuotoService) {
        _movimentoVuotoService = movimentoVuotoService;
    }

    @Override
    public MovimentoVuotoService getWrappedService() {
        return _movimentoVuotoService;
    }

    @Override
    public void setWrappedService(MovimentoVuotoService movimentoVuotoService) {
        _movimentoVuotoService = movimentoVuotoService;
    }
}
