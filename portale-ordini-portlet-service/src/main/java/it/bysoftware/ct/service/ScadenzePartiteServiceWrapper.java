package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ScadenzePartiteService}.
 *
 * @author Mario Torrisi
 * @see ScadenzePartiteService
 * @generated
 */
public class ScadenzePartiteServiceWrapper implements ScadenzePartiteService,
    ServiceWrapper<ScadenzePartiteService> {
    private ScadenzePartiteService _scadenzePartiteService;

    public ScadenzePartiteServiceWrapper(
        ScadenzePartiteService scadenzePartiteService) {
        _scadenzePartiteService = scadenzePartiteService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _scadenzePartiteService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _scadenzePartiteService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _scadenzePartiteService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public ScadenzePartiteService getWrappedScadenzePartiteService() {
        return _scadenzePartiteService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedScadenzePartiteService(
        ScadenzePartiteService scadenzePartiteService) {
        _scadenzePartiteService = scadenzePartiteService;
    }

    @Override
    public ScadenzePartiteService getWrappedService() {
        return _scadenzePartiteService;
    }

    @Override
    public void setWrappedService(ScadenzePartiteService scadenzePartiteService) {
        _scadenzePartiteService = scadenzePartiteService;
    }
}
