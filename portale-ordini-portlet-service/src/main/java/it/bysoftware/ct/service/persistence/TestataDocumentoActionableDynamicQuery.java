package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.TestataDocumento;
import it.bysoftware.ct.service.TestataDocumentoLocalServiceUtil;

/**
 * @author Mario Torrisi
 * @generated
 */
public abstract class TestataDocumentoActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public TestataDocumentoActionableDynamicQuery() throws SystemException {
        setBaseLocalService(TestataDocumentoLocalServiceUtil.getService());
        setClass(TestataDocumento.class);

        setClassLoader(it.bysoftware.ct.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("primaryKey.anno");
    }
}
