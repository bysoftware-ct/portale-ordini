package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CodiceConsorsioService}.
 *
 * @author Mario Torrisi
 * @see CodiceConsorsioService
 * @generated
 */
public class CodiceConsorsioServiceWrapper implements CodiceConsorsioService,
    ServiceWrapper<CodiceConsorsioService> {
    private CodiceConsorsioService _codiceConsorsioService;

    public CodiceConsorsioServiceWrapper(
        CodiceConsorsioService codiceConsorsioService) {
        _codiceConsorsioService = codiceConsorsioService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _codiceConsorsioService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _codiceConsorsioService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _codiceConsorsioService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public CodiceConsorsioService getWrappedCodiceConsorsioService() {
        return _codiceConsorsioService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedCodiceConsorsioService(
        CodiceConsorsioService codiceConsorsioService) {
        _codiceConsorsioService = codiceConsorsioService;
    }

    @Override
    public CodiceConsorsioService getWrappedService() {
        return _codiceConsorsioService;
    }

    @Override
    public void setWrappedService(CodiceConsorsioService codiceConsorsioService) {
        _codiceConsorsioService = codiceConsorsioService;
    }
}
