package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.MovimentoVuoto;

/**
 * The persistence interface for the movimento vuoto service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see MovimentoVuotoPersistenceImpl
 * @see MovimentoVuotoUtil
 * @generated
 */
public interface MovimentoVuotoPersistence extends BasePersistence<MovimentoVuoto> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link MovimentoVuotoUtil} to access the movimento vuoto persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the movimento vuotos where codiceVuoto = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @return the matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findByCodiceVuoto(
        java.lang.String codiceVuoto)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the movimento vuotos where codiceVuoto = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceVuoto the codice vuoto
    * @param start the lower bound of the range of movimento vuotos
    * @param end the upper bound of the range of movimento vuotos (not inclusive)
    * @return the range of matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findByCodiceVuoto(
        java.lang.String codiceVuoto, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the movimento vuotos where codiceVuoto = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceVuoto the codice vuoto
    * @param start the lower bound of the range of movimento vuotos
    * @param end the upper bound of the range of movimento vuotos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findByCodiceVuoto(
        java.lang.String codiceVuoto, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first movimento vuoto in the ordered set where codiceVuoto = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.MovimentoVuoto findByCodiceVuoto_First(
        java.lang.String codiceVuoto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException;

    /**
    * Returns the first movimento vuoto in the ordered set where codiceVuoto = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching movimento vuoto, or <code>null</code> if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.MovimentoVuoto fetchByCodiceVuoto_First(
        java.lang.String codiceVuoto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last movimento vuoto in the ordered set where codiceVuoto = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.MovimentoVuoto findByCodiceVuoto_Last(
        java.lang.String codiceVuoto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException;

    /**
    * Returns the last movimento vuoto in the ordered set where codiceVuoto = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching movimento vuoto, or <code>null</code> if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.MovimentoVuoto fetchByCodiceVuoto_Last(
        java.lang.String codiceVuoto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the movimento vuotos before and after the current movimento vuoto in the ordered set where codiceVuoto = &#63;.
    *
    * @param idMovimento the primary key of the current movimento vuoto
    * @param codiceVuoto the codice vuoto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a movimento vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.MovimentoVuoto[] findByCodiceVuoto_PrevAndNext(
        long idMovimento, java.lang.String codiceVuoto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException;

    /**
    * Removes all the movimento vuotos where codiceVuoto = &#63; from the database.
    *
    * @param codiceVuoto the codice vuoto
    * @throws SystemException if a system exception occurred
    */
    public void removeByCodiceVuoto(java.lang.String codiceVuoto)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of movimento vuotos where codiceVuoto = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @return the number of matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public int countByCodiceVuoto(java.lang.String codiceVuoto)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the movimento vuotos where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @return the matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findByfindByVuotoSoggettoData(
        java.lang.String codiceVuoto, java.lang.String codiceSoggetto,
        java.util.Date dataMovimento, int tipoMovimento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the movimento vuotos where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @param start the lower bound of the range of movimento vuotos
    * @param end the upper bound of the range of movimento vuotos (not inclusive)
    * @return the range of matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findByfindByVuotoSoggettoData(
        java.lang.String codiceVuoto, java.lang.String codiceSoggetto,
        java.util.Date dataMovimento, int tipoMovimento, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the movimento vuotos where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @param start the lower bound of the range of movimento vuotos
    * @param end the upper bound of the range of movimento vuotos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findByfindByVuotoSoggettoData(
        java.lang.String codiceVuoto, java.lang.String codiceSoggetto,
        java.util.Date dataMovimento, int tipoMovimento, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first movimento vuoto in the ordered set where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.MovimentoVuoto findByfindByVuotoSoggettoData_First(
        java.lang.String codiceVuoto, java.lang.String codiceSoggetto,
        java.util.Date dataMovimento, int tipoMovimento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException;

    /**
    * Returns the first movimento vuoto in the ordered set where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching movimento vuoto, or <code>null</code> if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.MovimentoVuoto fetchByfindByVuotoSoggettoData_First(
        java.lang.String codiceVuoto, java.lang.String codiceSoggetto,
        java.util.Date dataMovimento, int tipoMovimento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last movimento vuoto in the ordered set where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.MovimentoVuoto findByfindByVuotoSoggettoData_Last(
        java.lang.String codiceVuoto, java.lang.String codiceSoggetto,
        java.util.Date dataMovimento, int tipoMovimento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException;

    /**
    * Returns the last movimento vuoto in the ordered set where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching movimento vuoto, or <code>null</code> if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.MovimentoVuoto fetchByfindByVuotoSoggettoData_Last(
        java.lang.String codiceVuoto, java.lang.String codiceSoggetto,
        java.util.Date dataMovimento, int tipoMovimento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the movimento vuotos before and after the current movimento vuoto in the ordered set where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
    *
    * @param idMovimento the primary key of the current movimento vuoto
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a movimento vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.MovimentoVuoto[] findByfindByVuotoSoggettoData_PrevAndNext(
        long idMovimento, java.lang.String codiceVuoto,
        java.lang.String codiceSoggetto, java.util.Date dataMovimento,
        int tipoMovimento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException;

    /**
    * Removes all the movimento vuotos where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63; from the database.
    *
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @throws SystemException if a system exception occurred
    */
    public void removeByfindByVuotoSoggettoData(java.lang.String codiceVuoto,
        java.lang.String codiceSoggetto, java.util.Date dataMovimento,
        int tipoMovimento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of movimento vuotos where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @return the number of matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public int countByfindByVuotoSoggettoData(java.lang.String codiceVuoto,
        java.lang.String codiceSoggetto, java.util.Date dataMovimento,
        int tipoMovimento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the movimento vuotos where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @return the matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findByCodiceSoggetto(
        java.lang.String codiceSoggetto)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the movimento vuotos where codiceSoggetto = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceSoggetto the codice soggetto
    * @param start the lower bound of the range of movimento vuotos
    * @param end the upper bound of the range of movimento vuotos (not inclusive)
    * @return the range of matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findByCodiceSoggetto(
        java.lang.String codiceSoggetto, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the movimento vuotos where codiceSoggetto = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceSoggetto the codice soggetto
    * @param start the lower bound of the range of movimento vuotos
    * @param end the upper bound of the range of movimento vuotos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findByCodiceSoggetto(
        java.lang.String codiceSoggetto, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first movimento vuoto in the ordered set where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.MovimentoVuoto findByCodiceSoggetto_First(
        java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException;

    /**
    * Returns the first movimento vuoto in the ordered set where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching movimento vuoto, or <code>null</code> if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.MovimentoVuoto fetchByCodiceSoggetto_First(
        java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last movimento vuoto in the ordered set where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.MovimentoVuoto findByCodiceSoggetto_Last(
        java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException;

    /**
    * Returns the last movimento vuoto in the ordered set where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching movimento vuoto, or <code>null</code> if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.MovimentoVuoto fetchByCodiceSoggetto_Last(
        java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the movimento vuotos before and after the current movimento vuoto in the ordered set where codiceSoggetto = &#63;.
    *
    * @param idMovimento the primary key of the current movimento vuoto
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a movimento vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.MovimentoVuoto[] findByCodiceSoggetto_PrevAndNext(
        long idMovimento, java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException;

    /**
    * Removes all the movimento vuotos where codiceSoggetto = &#63; from the database.
    *
    * @param codiceSoggetto the codice soggetto
    * @throws SystemException if a system exception occurred
    */
    public void removeByCodiceSoggetto(java.lang.String codiceSoggetto)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of movimento vuotos where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @return the number of matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public int countByCodiceSoggetto(java.lang.String codiceSoggetto)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the movimento vuoto in the entity cache if it is enabled.
    *
    * @param movimentoVuoto the movimento vuoto
    */
    public void cacheResult(
        it.bysoftware.ct.model.MovimentoVuoto movimentoVuoto);

    /**
    * Caches the movimento vuotos in the entity cache if it is enabled.
    *
    * @param movimentoVuotos the movimento vuotos
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.MovimentoVuoto> movimentoVuotos);

    /**
    * Creates a new movimento vuoto with the primary key. Does not add the movimento vuoto to the database.
    *
    * @param idMovimento the primary key for the new movimento vuoto
    * @return the new movimento vuoto
    */
    public it.bysoftware.ct.model.MovimentoVuoto create(long idMovimento);

    /**
    * Removes the movimento vuoto with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param idMovimento the primary key of the movimento vuoto
    * @return the movimento vuoto that was removed
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a movimento vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.MovimentoVuoto remove(long idMovimento)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException;

    public it.bysoftware.ct.model.MovimentoVuoto updateImpl(
        it.bysoftware.ct.model.MovimentoVuoto movimentoVuoto)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the movimento vuoto with the primary key or throws a {@link it.bysoftware.ct.NoSuchMovimentoVuotoException} if it could not be found.
    *
    * @param idMovimento the primary key of the movimento vuoto
    * @return the movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a movimento vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.MovimentoVuoto findByPrimaryKey(
        long idMovimento)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException;

    /**
    * Returns the movimento vuoto with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param idMovimento the primary key of the movimento vuoto
    * @return the movimento vuoto, or <code>null</code> if a movimento vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.MovimentoVuoto fetchByPrimaryKey(
        long idMovimento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the movimento vuotos.
    *
    * @return the movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the movimento vuotos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of movimento vuotos
    * @param end the upper bound of the range of movimento vuotos (not inclusive)
    * @return the range of movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the movimento vuotos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of movimento vuotos
    * @param end the upper bound of the range of movimento vuotos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the movimento vuotos from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of movimento vuotos.
    *
    * @return the number of movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
