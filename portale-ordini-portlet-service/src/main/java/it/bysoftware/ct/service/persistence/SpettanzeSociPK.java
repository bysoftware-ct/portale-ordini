package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class SpettanzeSociPK implements Comparable<SpettanzeSociPK>,
    Serializable {
    public long id;
    public int anno;

    public SpettanzeSociPK() {
    }

    public SpettanzeSociPK(long id, int anno) {
        this.id = id;
        this.anno = anno;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    @Override
    public int compareTo(SpettanzeSociPK pk) {
        if (pk == null) {
            return -1;
        }

        int value = 0;

        if (id < pk.id) {
            value = -1;
        } else if (id > pk.id) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        if (anno < pk.anno) {
            value = -1;
        } else if (anno > pk.anno) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof SpettanzeSociPK)) {
            return false;
        }

        SpettanzeSociPK pk = (SpettanzeSociPK) obj;

        if ((id == pk.id) && (anno == pk.anno)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (String.valueOf(id) + String.valueOf(anno)).hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(10);

        sb.append(StringPool.OPEN_CURLY_BRACE);

        sb.append("id");
        sb.append(StringPool.EQUAL);
        sb.append(id);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("anno");
        sb.append(StringPool.EQUAL);
        sb.append(anno);

        sb.append(StringPool.CLOSE_CURLY_BRACE);

        return sb.toString();
    }
}
