package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.TestataDocumento;

/**
 * The persistence interface for the testata documento service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see TestataDocumentoPersistenceImpl
 * @see TestataDocumentoUtil
 * @generated
 */
public interface TestataDocumentoPersistence extends BasePersistence<TestataDocumento> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link TestataDocumentoUtil} to access the testata documento persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the testata documentos where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @return the matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findByAnnoAttivitaCentroDeposito(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, java.lang.String codiceDeposito,
        java.lang.String codiceFornitore, java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the testata documentos where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @return the range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findByAnnoAttivitaCentroDeposito(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, java.lang.String codiceDeposito,
        java.lang.String codiceFornitore, java.lang.String tipoDocumento,
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the testata documentos where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findByAnnoAttivitaCentroDeposito(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, java.lang.String codiceDeposito,
        java.lang.String codiceFornitore, java.lang.String tipoDocumento,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first testata documento in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento findByAnnoAttivitaCentroDeposito_First(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, java.lang.String codiceDeposito,
        java.lang.String codiceFornitore, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Returns the first testata documento in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento fetchByAnnoAttivitaCentroDeposito_First(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, java.lang.String codiceDeposito,
        java.lang.String codiceFornitore, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last testata documento in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento findByAnnoAttivitaCentroDeposito_Last(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, java.lang.String codiceDeposito,
        java.lang.String codiceFornitore, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Returns the last testata documento in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento fetchByAnnoAttivitaCentroDeposito_Last(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, java.lang.String codiceDeposito,
        java.lang.String codiceFornitore, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the testata documentos before and after the current testata documento in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param testataDocumentoPK the primary key of the current testata documento
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento[] findByAnnoAttivitaCentroDeposito_PrevAndNext(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK,
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, java.lang.String codiceDeposito,
        java.lang.String codiceFornitore, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Removes all the testata documentos where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63; from the database.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @throws SystemException if a system exception occurred
    */
    public void removeByAnnoAttivitaCentroDeposito(int anno,
        java.lang.String codiceAttivita, java.lang.String codiceCentro,
        java.lang.String codiceDeposito, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of testata documentos where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @return the number of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public int countByAnnoAttivitaCentroDeposito(int anno,
        java.lang.String codiceAttivita, java.lang.String codiceCentro,
        java.lang.String codiceDeposito, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the testata documentos where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @return the matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findByDataStatoTipoDocumento(
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the testata documentos where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @return the range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findByDataStatoTipoDocumento(
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the testata documentos where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findByDataStatoTipoDocumento(
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first testata documento in the ordered set where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento findByDataStatoTipoDocumento_First(
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Returns the first testata documento in the ordered set where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento fetchByDataStatoTipoDocumento_First(
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last testata documento in the ordered set where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento findByDataStatoTipoDocumento_Last(
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Returns the last testata documento in the ordered set where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento fetchByDataStatoTipoDocumento_Last(
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the testata documentos before and after the current testata documento in the ordered set where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param testataDocumentoPK the primary key of the current testata documento
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento[] findByDataStatoTipoDocumento_PrevAndNext(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK,
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Removes all the testata documentos where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63; from the database.
    *
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @throws SystemException if a system exception occurred
    */
    public void removeByDataStatoTipoDocumento(
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of testata documentos where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @return the number of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public int countByDataStatoTipoDocumento(java.util.Date dataRegistrazione,
        boolean stato, java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the testata documentos where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @return the matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findByDataFornitoreTipoDocumento(
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the testata documentos where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @return the range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findByDataFornitoreTipoDocumento(
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the testata documentos where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findByDataFornitoreTipoDocumento(
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first testata documento in the ordered set where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento findByDataFornitoreTipoDocumento_First(
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Returns the first testata documento in the ordered set where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento fetchByDataFornitoreTipoDocumento_First(
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last testata documento in the ordered set where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento findByDataFornitoreTipoDocumento_Last(
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Returns the last testata documento in the ordered set where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento fetchByDataFornitoreTipoDocumento_Last(
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the testata documentos before and after the current testata documento in the ordered set where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param testataDocumentoPK the primary key of the current testata documento
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento[] findByDataFornitoreTipoDocumento_PrevAndNext(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK,
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Removes all the testata documentos where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63; from the database.
    *
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @throws SystemException if a system exception occurred
    */
    public void removeByDataFornitoreTipoDocumento(
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of testata documentos where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @return the number of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public int countByDataFornitoreTipoDocumento(
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the testata documentos where stato = &#63; and tipoDocumento = &#63;.
    *
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @return the matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findByStatoTipoDocumento(
        boolean stato, java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the testata documentos where stato = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @return the range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findByStatoTipoDocumento(
        boolean stato, java.lang.String tipoDocumento, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the testata documentos where stato = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findByStatoTipoDocumento(
        boolean stato, java.lang.String tipoDocumento, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first testata documento in the ordered set where stato = &#63; and tipoDocumento = &#63;.
    *
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento findByStatoTipoDocumento_First(
        boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Returns the first testata documento in the ordered set where stato = &#63; and tipoDocumento = &#63;.
    *
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento fetchByStatoTipoDocumento_First(
        boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last testata documento in the ordered set where stato = &#63; and tipoDocumento = &#63;.
    *
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento findByStatoTipoDocumento_Last(
        boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Returns the last testata documento in the ordered set where stato = &#63; and tipoDocumento = &#63;.
    *
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento fetchByStatoTipoDocumento_Last(
        boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the testata documentos before and after the current testata documento in the ordered set where stato = &#63; and tipoDocumento = &#63;.
    *
    * @param testataDocumentoPK the primary key of the current testata documento
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento[] findByStatoTipoDocumento_PrevAndNext(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK,
        boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Removes all the testata documentos where stato = &#63; and tipoDocumento = &#63; from the database.
    *
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @throws SystemException if a system exception occurred
    */
    public void removeByStatoTipoDocumento(boolean stato,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of testata documentos where stato = &#63; and tipoDocumento = &#63;.
    *
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @return the number of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public int countByStatoTipoDocumento(boolean stato,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the testata documentos where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @return the matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findByIdOrdineTipoDocumentoStato(
        long libLng1, boolean stato, java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the testata documentos where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @return the range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findByIdOrdineTipoDocumentoStato(
        long libLng1, boolean stato, java.lang.String tipoDocumento, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the testata documentos where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findByIdOrdineTipoDocumentoStato(
        long libLng1, boolean stato, java.lang.String tipoDocumento, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first testata documento in the ordered set where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento findByIdOrdineTipoDocumentoStato_First(
        long libLng1, boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Returns the first testata documento in the ordered set where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento fetchByIdOrdineTipoDocumentoStato_First(
        long libLng1, boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last testata documento in the ordered set where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento findByIdOrdineTipoDocumentoStato_Last(
        long libLng1, boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Returns the last testata documento in the ordered set where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento fetchByIdOrdineTipoDocumentoStato_Last(
        long libLng1, boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the testata documentos before and after the current testata documento in the ordered set where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param testataDocumentoPK the primary key of the current testata documento
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento[] findByIdOrdineTipoDocumentoStato_PrevAndNext(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK,
        long libLng1, boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Removes all the testata documentos where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63; from the database.
    *
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @throws SystemException if a system exception occurred
    */
    public void removeByIdOrdineTipoDocumentoStato(long libLng1, boolean stato,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of testata documentos where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @return the number of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public int countByIdOrdineTipoDocumentoStato(long libLng1, boolean stato,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the testata documentos where libLng1 = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @return the matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findByIdOrdineTipo(
        long libLng1, java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the testata documentos where libLng1 = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @return the range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findByIdOrdineTipo(
        long libLng1, java.lang.String tipoDocumento, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the testata documentos where libLng1 = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findByIdOrdineTipo(
        long libLng1, java.lang.String tipoDocumento, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first testata documento in the ordered set where libLng1 = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento findByIdOrdineTipo_First(
        long libLng1, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Returns the first testata documento in the ordered set where libLng1 = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento fetchByIdOrdineTipo_First(
        long libLng1, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last testata documento in the ordered set where libLng1 = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento findByIdOrdineTipo_Last(
        long libLng1, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Returns the last testata documento in the ordered set where libLng1 = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento fetchByIdOrdineTipo_Last(
        long libLng1, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the testata documentos before and after the current testata documento in the ordered set where libLng1 = &#63; and tipoDocumento = &#63;.
    *
    * @param testataDocumentoPK the primary key of the current testata documento
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento[] findByIdOrdineTipo_PrevAndNext(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK,
        long libLng1, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Removes all the testata documentos where libLng1 = &#63; and tipoDocumento = &#63; from the database.
    *
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @throws SystemException if a system exception occurred
    */
    public void removeByIdOrdineTipo(long libLng1,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of testata documentos where libLng1 = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @return the number of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public int countByIdOrdineTipo(long libLng1, java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the testata documento where libLng1 = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63; or throws a {@link it.bysoftware.ct.NoSuchTestataDocumentoException} if it could not be found.
    *
    * @param libLng1 the lib lng1
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @return the matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento findByIdOrdineFornitoreTipo(
        long libLng1, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Returns the testata documento where libLng1 = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param libLng1 the lib lng1
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @return the matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento fetchByIdOrdineFornitoreTipo(
        long libLng1, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the testata documento where libLng1 = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param libLng1 the lib lng1
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento fetchByIdOrdineFornitoreTipo(
        long libLng1, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the testata documento where libLng1 = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63; from the database.
    *
    * @param libLng1 the lib lng1
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @return the testata documento that was removed
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento removeByIdOrdineFornitoreTipo(
        long libLng1, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Returns the number of testata documentos where libLng1 = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @return the number of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public int countByIdOrdineFornitoreTipo(long libLng1,
        java.lang.String codiceFornitore, java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the testata documento in the entity cache if it is enabled.
    *
    * @param testataDocumento the testata documento
    */
    public void cacheResult(
        it.bysoftware.ct.model.TestataDocumento testataDocumento);

    /**
    * Caches the testata documentos in the entity cache if it is enabled.
    *
    * @param testataDocumentos the testata documentos
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.TestataDocumento> testataDocumentos);

    /**
    * Creates a new testata documento with the primary key. Does not add the testata documento to the database.
    *
    * @param testataDocumentoPK the primary key for the new testata documento
    * @return the new testata documento
    */
    public it.bysoftware.ct.model.TestataDocumento create(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK);

    /**
    * Removes the testata documento with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param testataDocumentoPK the primary key of the testata documento
    * @return the testata documento that was removed
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento remove(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    public it.bysoftware.ct.model.TestataDocumento updateImpl(
        it.bysoftware.ct.model.TestataDocumento testataDocumento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the testata documento with the primary key or throws a {@link it.bysoftware.ct.NoSuchTestataDocumentoException} if it could not be found.
    *
    * @param testataDocumentoPK the primary key of the testata documento
    * @return the testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento findByPrimaryKey(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException;

    /**
    * Returns the testata documento with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param testataDocumentoPK the primary key of the testata documento
    * @return the testata documento, or <code>null</code> if a testata documento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TestataDocumento fetchByPrimaryKey(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the testata documentos.
    *
    * @return the testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the testata documentos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @return the range of testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the testata documentos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of testata documentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TestataDocumento> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the testata documentos from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of testata documentos.
    *
    * @return the number of testata documentos
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
