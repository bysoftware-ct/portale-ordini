package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link RigaLocalService}.
 *
 * @author Mario Torrisi
 * @see RigaLocalService
 * @generated
 */
public class RigaLocalServiceWrapper implements RigaLocalService,
    ServiceWrapper<RigaLocalService> {
    private RigaLocalService _rigaLocalService;

    public RigaLocalServiceWrapper(RigaLocalService rigaLocalService) {
        _rigaLocalService = rigaLocalService;
    }

    /**
    * Adds the riga to the database. Also notifies the appropriate model listeners.
    *
    * @param riga the riga
    * @return the riga that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Riga addRiga(it.bysoftware.ct.model.Riga riga)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rigaLocalService.addRiga(riga);
    }

    /**
    * Creates a new riga with the primary key. Does not add the riga to the database.
    *
    * @param id the primary key for the new riga
    * @return the new riga
    */
    @Override
    public it.bysoftware.ct.model.Riga createRiga(long id) {
        return _rigaLocalService.createRiga(id);
    }

    /**
    * Deletes the riga with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the riga
    * @return the riga that was removed
    * @throws PortalException if a riga with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Riga deleteRiga(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _rigaLocalService.deleteRiga(id);
    }

    /**
    * Deletes the riga from the database. Also notifies the appropriate model listeners.
    *
    * @param riga the riga
    * @return the riga that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Riga deleteRiga(
        it.bysoftware.ct.model.Riga riga)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rigaLocalService.deleteRiga(riga);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _rigaLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rigaLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _rigaLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rigaLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rigaLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rigaLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public it.bysoftware.ct.model.Riga fetchRiga(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rigaLocalService.fetchRiga(id);
    }

    /**
    * Returns the riga with the primary key.
    *
    * @param id the primary key of the riga
    * @return the riga
    * @throws PortalException if a riga with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Riga getRiga(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _rigaLocalService.getRiga(id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _rigaLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the rigas.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of rigas
    * @param end the upper bound of the range of rigas (not inclusive)
    * @return the range of rigas
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.Riga> getRigas(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _rigaLocalService.getRigas(start, end);
    }

    /**
    * Returns the number of rigas.
    *
    * @return the number of rigas
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getRigasCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rigaLocalService.getRigasCount();
    }

    /**
    * Updates the riga in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param riga the riga
    * @return the riga that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Riga updateRiga(
        it.bysoftware.ct.model.Riga riga)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rigaLocalService.updateRiga(riga);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _rigaLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _rigaLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _rigaLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Riga> findRowsInCart(
        long cartId) {
        return _rigaLocalService.findRowsInCart(cartId);
    }

    @Override
    public java.util.List<java.lang.Object[]> getReceivedPerPartnerAndOrder(
        long orderId, java.lang.String partnerCode) {
        return _rigaLocalService.getReceivedPerPartnerAndOrder(orderId,
            partnerCode);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public RigaLocalService getWrappedRigaLocalService() {
        return _rigaLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedRigaLocalService(RigaLocalService rigaLocalService) {
        _rigaLocalService = rigaLocalService;
    }

    @Override
    public RigaLocalService getWrappedService() {
        return _rigaLocalService;
    }

    @Override
    public void setWrappedService(RigaLocalService rigaLocalService) {
        _rigaLocalService = rigaLocalService;
    }
}
