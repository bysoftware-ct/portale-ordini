package it.bysoftware.ct.service.messaging;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;

import it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.AnagraficheClientiFornitoriServiceUtil;
import it.bysoftware.ct.service.ArticoliLocalServiceUtil;
import it.bysoftware.ct.service.ArticoliServiceUtil;
import it.bysoftware.ct.service.ArticoloSocioLocalServiceUtil;
import it.bysoftware.ct.service.ArticoloSocioServiceUtil;
import it.bysoftware.ct.service.CarrelloLocalServiceUtil;
import it.bysoftware.ct.service.CarrelloServiceUtil;
import it.bysoftware.ct.service.CategorieLocalServiceUtil;
import it.bysoftware.ct.service.CategorieMerceologicheLocalServiceUtil;
import it.bysoftware.ct.service.CategorieMerceologicheServiceUtil;
import it.bysoftware.ct.service.CategorieServiceUtil;
import it.bysoftware.ct.service.CausaliContabiliLocalServiceUtil;
import it.bysoftware.ct.service.CausaliContabiliServiceUtil;
import it.bysoftware.ct.service.CausaliEstrattoContoLocalServiceUtil;
import it.bysoftware.ct.service.CausaliEstrattoContoServiceUtil;
import it.bysoftware.ct.service.CentroLocalServiceUtil;
import it.bysoftware.ct.service.CentroServiceUtil;
import it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil;
import it.bysoftware.ct.service.ClientiFornitoriDatiAggServiceUtil;
import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.CodiceConsorzioLocalServiceUtil;
import it.bysoftware.ct.service.CodiceConsorzioServiceUtil;
import it.bysoftware.ct.service.ContatoreSocioLocalServiceUtil;
import it.bysoftware.ct.service.ContatoreSocioServiceUtil;
import it.bysoftware.ct.service.DescrizioniVariantiLocalServiceUtil;
import it.bysoftware.ct.service.DescrizioniVariantiServiceUtil;
import it.bysoftware.ct.service.DettagliOperazioniContabiliLocalServiceUtil;
import it.bysoftware.ct.service.DettagliOperazioniContabiliServiceUtil;
import it.bysoftware.ct.service.MovimentoVuotoLocalServiceUtil;
import it.bysoftware.ct.service.MovimentoVuotoServiceUtil;
import it.bysoftware.ct.service.OrdineLocalServiceUtil;
import it.bysoftware.ct.service.OrdineServiceUtil;
import it.bysoftware.ct.service.PagamentoLocalServiceUtil;
import it.bysoftware.ct.service.PagamentoServiceUtil;
import it.bysoftware.ct.service.PianoPagamentiLocalServiceUtil;
import it.bysoftware.ct.service.PianoPagamentiServiceUtil;
import it.bysoftware.ct.service.PianteLocalServiceUtil;
import it.bysoftware.ct.service.PianteServiceUtil;
import it.bysoftware.ct.service.RigaLocalServiceUtil;
import it.bysoftware.ct.service.RigaServiceUtil;
import it.bysoftware.ct.service.RigheFattureVeditaLocalServiceUtil;
import it.bysoftware.ct.service.RigheFattureVeditaServiceUtil;
import it.bysoftware.ct.service.RigoDocumentoLocalServiceUtil;
import it.bysoftware.ct.service.RigoDocumentoServiceUtil;
import it.bysoftware.ct.service.RubricaLocalServiceUtil;
import it.bysoftware.ct.service.RubricaServiceUtil;
import it.bysoftware.ct.service.ScadenzePartiteLocalServiceUtil;
import it.bysoftware.ct.service.ScadenzePartiteServiceUtil;
import it.bysoftware.ct.service.SchedaPagamentoLocalServiceUtil;
import it.bysoftware.ct.service.SchedaPagamentoServiceUtil;
import it.bysoftware.ct.service.SottocontiLocalServiceUtil;
import it.bysoftware.ct.service.SottocontiServiceUtil;
import it.bysoftware.ct.service.SpettanzeSociLocalServiceUtil;
import it.bysoftware.ct.service.SpettanzeSociServiceUtil;
import it.bysoftware.ct.service.StatiLocalServiceUtil;
import it.bysoftware.ct.service.StatiServiceUtil;
import it.bysoftware.ct.service.TestataDocumentoLocalServiceUtil;
import it.bysoftware.ct.service.TestataDocumentoServiceUtil;
import it.bysoftware.ct.service.TestataFattureClientiLocalServiceUtil;
import it.bysoftware.ct.service.TestataFattureClientiServiceUtil;
import it.bysoftware.ct.service.TipoCarrelloLocalServiceUtil;
import it.bysoftware.ct.service.TipoCarrelloServiceUtil;
import it.bysoftware.ct.service.VariantiLocalServiceUtil;
import it.bysoftware.ct.service.VariantiServiceUtil;
import it.bysoftware.ct.service.VettoriLocalServiceUtil;
import it.bysoftware.ct.service.VettoriServiceUtil;
import it.bysoftware.ct.service.VociIvaLocalServiceUtil;
import it.bysoftware.ct.service.VociIvaServiceUtil;
import it.bysoftware.ct.service.VuotoLocalServiceUtil;
import it.bysoftware.ct.service.VuotoServiceUtil;


public class ClpMessageListener extends BaseMessageListener {
    public static String getServletContextName() {
        return ClpSerializer.getServletContextName();
    }

    @Override
    protected void doReceive(Message message) throws Exception {
        String command = message.getString("command");
        String servletContextName = message.getString("servletContextName");

        if (command.equals("undeploy") &&
                servletContextName.equals(getServletContextName())) {
            AnagraficheClientiFornitoriLocalServiceUtil.clearService();

            AnagraficheClientiFornitoriServiceUtil.clearService();
            ArticoliLocalServiceUtil.clearService();

            ArticoliServiceUtil.clearService();
            ArticoloSocioLocalServiceUtil.clearService();

            ArticoloSocioServiceUtil.clearService();
            CarrelloLocalServiceUtil.clearService();

            CarrelloServiceUtil.clearService();
            CategorieLocalServiceUtil.clearService();

            CategorieServiceUtil.clearService();
            CategorieMerceologicheLocalServiceUtil.clearService();

            CategorieMerceologicheServiceUtil.clearService();
            CausaliContabiliLocalServiceUtil.clearService();

            CausaliContabiliServiceUtil.clearService();
            CausaliEstrattoContoLocalServiceUtil.clearService();

            CausaliEstrattoContoServiceUtil.clearService();
            CentroLocalServiceUtil.clearService();

            CentroServiceUtil.clearService();
            ClientiFornitoriDatiAggLocalServiceUtil.clearService();

            ClientiFornitoriDatiAggServiceUtil.clearService();
            CodiceConsorzioLocalServiceUtil.clearService();

            CodiceConsorzioServiceUtil.clearService();
            ContatoreSocioLocalServiceUtil.clearService();

            ContatoreSocioServiceUtil.clearService();
            DescrizioniVariantiLocalServiceUtil.clearService();

            DescrizioniVariantiServiceUtil.clearService();
            DettagliOperazioniContabiliLocalServiceUtil.clearService();

            DettagliOperazioniContabiliServiceUtil.clearService();
            MovimentoVuotoLocalServiceUtil.clearService();

            MovimentoVuotoServiceUtil.clearService();
            OrdineLocalServiceUtil.clearService();

            OrdineServiceUtil.clearService();
            PagamentoLocalServiceUtil.clearService();

            PagamentoServiceUtil.clearService();
            PianoPagamentiLocalServiceUtil.clearService();

            PianoPagamentiServiceUtil.clearService();
            PianteLocalServiceUtil.clearService();

            PianteServiceUtil.clearService();
            RigaLocalServiceUtil.clearService();

            RigaServiceUtil.clearService();
            RigheFattureVeditaLocalServiceUtil.clearService();

            RigheFattureVeditaServiceUtil.clearService();
            RigoDocumentoLocalServiceUtil.clearService();

            RigoDocumentoServiceUtil.clearService();
            RubricaLocalServiceUtil.clearService();

            RubricaServiceUtil.clearService();
            ScadenzePartiteLocalServiceUtil.clearService();

            ScadenzePartiteServiceUtil.clearService();
            SchedaPagamentoLocalServiceUtil.clearService();

            SchedaPagamentoServiceUtil.clearService();
            SottocontiLocalServiceUtil.clearService();

            SottocontiServiceUtil.clearService();
            SpettanzeSociLocalServiceUtil.clearService();

            SpettanzeSociServiceUtil.clearService();
            StatiLocalServiceUtil.clearService();

            StatiServiceUtil.clearService();
            TestataDocumentoLocalServiceUtil.clearService();

            TestataDocumentoServiceUtil.clearService();
            TestataFattureClientiLocalServiceUtil.clearService();

            TestataFattureClientiServiceUtil.clearService();
            TipoCarrelloLocalServiceUtil.clearService();

            TipoCarrelloServiceUtil.clearService();
            VariantiLocalServiceUtil.clearService();

            VariantiServiceUtil.clearService();
            VettoriLocalServiceUtil.clearService();

            VettoriServiceUtil.clearService();
            VociIvaLocalServiceUtil.clearService();

            VociIvaServiceUtil.clearService();
            VuotoLocalServiceUtil.clearService();

            VuotoServiceUtil.clearService();
        }
    }
}
