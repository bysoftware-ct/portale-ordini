package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.ClientiFornitoriDatiAgg;

import java.util.List;

/**
 * The persistence utility for the clienti fornitori dati agg service. This utility wraps {@link ClientiFornitoriDatiAggPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see ClientiFornitoriDatiAggPersistence
 * @see ClientiFornitoriDatiAggPersistenceImpl
 * @generated
 */
public class ClientiFornitoriDatiAggUtil {
    private static ClientiFornitoriDatiAggPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg) {
        getPersistence().clearCache(clientiFornitoriDatiAgg);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<ClientiFornitoriDatiAgg> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<ClientiFornitoriDatiAgg> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<ClientiFornitoriDatiAgg> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static ClientiFornitoriDatiAgg update(
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg)
        throws SystemException {
        return getPersistence().update(clientiFornitoriDatiAgg);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static ClientiFornitoriDatiAgg update(
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(clientiFornitoriDatiAgg, serviceContext);
    }

    /**
    * Returns all the clienti fornitori dati aggs where fornitore = &#63;.
    *
    * @param fornitore the fornitore
    * @return the matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findByClienti(
        boolean fornitore)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByClienti(fornitore);
    }

    /**
    * Returns a range of all the clienti fornitori dati aggs where fornitore = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param fornitore the fornitore
    * @param start the lower bound of the range of clienti fornitori dati aggs
    * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
    * @return the range of matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findByClienti(
        boolean fornitore, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByClienti(fornitore, start, end);
    }

    /**
    * Returns an ordered range of all the clienti fornitori dati aggs where fornitore = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param fornitore the fornitore
    * @param start the lower bound of the range of clienti fornitori dati aggs
    * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findByClienti(
        boolean fornitore, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByClienti(fornitore, start, end, orderByComparator);
    }

    /**
    * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63;.
    *
    * @param fornitore the fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching clienti fornitori dati agg
    * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg findByClienti_First(
        boolean fornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException {
        return getPersistence().findByClienti_First(fornitore, orderByComparator);
    }

    /**
    * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63;.
    *
    * @param fornitore the fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg fetchByClienti_First(
        boolean fornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByClienti_First(fornitore, orderByComparator);
    }

    /**
    * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63;.
    *
    * @param fornitore the fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching clienti fornitori dati agg
    * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg findByClienti_Last(
        boolean fornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException {
        return getPersistence().findByClienti_Last(fornitore, orderByComparator);
    }

    /**
    * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63;.
    *
    * @param fornitore the fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg fetchByClienti_Last(
        boolean fornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByClienti_Last(fornitore, orderByComparator);
    }

    /**
    * Returns the clienti fornitori dati aggs before and after the current clienti fornitori dati agg in the ordered set where fornitore = &#63;.
    *
    * @param clientiFornitoriDatiAggPK the primary key of the current clienti fornitori dati agg
    * @param fornitore the fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next clienti fornitori dati agg
    * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a clienti fornitori dati agg with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg[] findByClienti_PrevAndNext(
        it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK,
        boolean fornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException {
        return getPersistence()
                   .findByClienti_PrevAndNext(clientiFornitoriDatiAggPK,
            fornitore, orderByComparator);
    }

    /**
    * Removes all the clienti fornitori dati aggs where fornitore = &#63; from the database.
    *
    * @param fornitore the fornitore
    * @throws SystemException if a system exception occurred
    */
    public static void removeByClienti(boolean fornitore)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByClienti(fornitore);
    }

    /**
    * Returns the number of clienti fornitori dati aggs where fornitore = &#63;.
    *
    * @param fornitore the fornitore
    * @return the number of matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static int countByClienti(boolean fornitore)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByClienti(fornitore);
    }

    /**
    * Returns all the clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63;.
    *
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @return the matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findByClientiCategoria(
        boolean fornitore, java.lang.String categoriaEconomica)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByClientiCategoria(fornitore, categoriaEconomica);
    }

    /**
    * Returns a range of all the clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @param start the lower bound of the range of clienti fornitori dati aggs
    * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
    * @return the range of matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findByClientiCategoria(
        boolean fornitore, java.lang.String categoriaEconomica, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByClientiCategoria(fornitore, categoriaEconomica,
            start, end);
    }

    /**
    * Returns an ordered range of all the clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @param start the lower bound of the range of clienti fornitori dati aggs
    * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findByClientiCategoria(
        boolean fornitore, java.lang.String categoriaEconomica, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByClientiCategoria(fornitore, categoriaEconomica,
            start, end, orderByComparator);
    }

    /**
    * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
    *
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching clienti fornitori dati agg
    * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg findByClientiCategoria_First(
        boolean fornitore, java.lang.String categoriaEconomica,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException {
        return getPersistence()
                   .findByClientiCategoria_First(fornitore, categoriaEconomica,
            orderByComparator);
    }

    /**
    * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
    *
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg fetchByClientiCategoria_First(
        boolean fornitore, java.lang.String categoriaEconomica,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByClientiCategoria_First(fornitore,
            categoriaEconomica, orderByComparator);
    }

    /**
    * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
    *
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching clienti fornitori dati agg
    * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg findByClientiCategoria_Last(
        boolean fornitore, java.lang.String categoriaEconomica,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException {
        return getPersistence()
                   .findByClientiCategoria_Last(fornitore, categoriaEconomica,
            orderByComparator);
    }

    /**
    * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
    *
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg fetchByClientiCategoria_Last(
        boolean fornitore, java.lang.String categoriaEconomica,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByClientiCategoria_Last(fornitore, categoriaEconomica,
            orderByComparator);
    }

    /**
    * Returns the clienti fornitori dati aggs before and after the current clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
    *
    * @param clientiFornitoriDatiAggPK the primary key of the current clienti fornitori dati agg
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next clienti fornitori dati agg
    * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a clienti fornitori dati agg with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg[] findByClientiCategoria_PrevAndNext(
        it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK,
        boolean fornitore, java.lang.String categoriaEconomica,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException {
        return getPersistence()
                   .findByClientiCategoria_PrevAndNext(clientiFornitoriDatiAggPK,
            fornitore, categoriaEconomica, orderByComparator);
    }

    /**
    * Removes all the clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63; from the database.
    *
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @throws SystemException if a system exception occurred
    */
    public static void removeByClientiCategoria(boolean fornitore,
        java.lang.String categoriaEconomica)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByClientiCategoria(fornitore, categoriaEconomica);
    }

    /**
    * Returns the number of clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63;.
    *
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @return the number of matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static int countByClientiCategoria(boolean fornitore,
        java.lang.String categoriaEconomica)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByClientiCategoria(fornitore, categoriaEconomica);
    }

    /**
    * Returns all the clienti fornitori dati aggs where fornitore = &#63; and associato = &#63;.
    *
    * @param fornitore the fornitore
    * @param associato the associato
    * @return the matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findByAssociato(
        boolean fornitore, boolean associato)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByAssociato(fornitore, associato);
    }

    /**
    * Returns a range of all the clienti fornitori dati aggs where fornitore = &#63; and associato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param fornitore the fornitore
    * @param associato the associato
    * @param start the lower bound of the range of clienti fornitori dati aggs
    * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
    * @return the range of matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findByAssociato(
        boolean fornitore, boolean associato, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByAssociato(fornitore, associato, start, end);
    }

    /**
    * Returns an ordered range of all the clienti fornitori dati aggs where fornitore = &#63; and associato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param fornitore the fornitore
    * @param associato the associato
    * @param start the lower bound of the range of clienti fornitori dati aggs
    * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findByAssociato(
        boolean fornitore, boolean associato, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByAssociato(fornitore, associato, start, end,
            orderByComparator);
    }

    /**
    * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63; and associato = &#63;.
    *
    * @param fornitore the fornitore
    * @param associato the associato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching clienti fornitori dati agg
    * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg findByAssociato_First(
        boolean fornitore, boolean associato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException {
        return getPersistence()
                   .findByAssociato_First(fornitore, associato,
            orderByComparator);
    }

    /**
    * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63; and associato = &#63;.
    *
    * @param fornitore the fornitore
    * @param associato the associato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg fetchByAssociato_First(
        boolean fornitore, boolean associato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByAssociato_First(fornitore, associato,
            orderByComparator);
    }

    /**
    * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63; and associato = &#63;.
    *
    * @param fornitore the fornitore
    * @param associato the associato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching clienti fornitori dati agg
    * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg findByAssociato_Last(
        boolean fornitore, boolean associato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException {
        return getPersistence()
                   .findByAssociato_Last(fornitore, associato, orderByComparator);
    }

    /**
    * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63; and associato = &#63;.
    *
    * @param fornitore the fornitore
    * @param associato the associato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg fetchByAssociato_Last(
        boolean fornitore, boolean associato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByAssociato_Last(fornitore, associato,
            orderByComparator);
    }

    /**
    * Returns the clienti fornitori dati aggs before and after the current clienti fornitori dati agg in the ordered set where fornitore = &#63; and associato = &#63;.
    *
    * @param clientiFornitoriDatiAggPK the primary key of the current clienti fornitori dati agg
    * @param fornitore the fornitore
    * @param associato the associato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next clienti fornitori dati agg
    * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a clienti fornitori dati agg with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg[] findByAssociato_PrevAndNext(
        it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK,
        boolean fornitore, boolean associato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException {
        return getPersistence()
                   .findByAssociato_PrevAndNext(clientiFornitoriDatiAggPK,
            fornitore, associato, orderByComparator);
    }

    /**
    * Removes all the clienti fornitori dati aggs where fornitore = &#63; and associato = &#63; from the database.
    *
    * @param fornitore the fornitore
    * @param associato the associato
    * @throws SystemException if a system exception occurred
    */
    public static void removeByAssociato(boolean fornitore, boolean associato)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByAssociato(fornitore, associato);
    }

    /**
    * Returns the number of clienti fornitori dati aggs where fornitore = &#63; and associato = &#63;.
    *
    * @param fornitore the fornitore
    * @param associato the associato
    * @return the number of matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static int countByAssociato(boolean fornitore, boolean associato)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByAssociato(fornitore, associato);
    }

    /**
    * Returns all the clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63;.
    *
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @return the matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findByFornitori(
        boolean fornitore, java.lang.String categoriaEconomica)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByFornitori(fornitore, categoriaEconomica);
    }

    /**
    * Returns a range of all the clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @param start the lower bound of the range of clienti fornitori dati aggs
    * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
    * @return the range of matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findByFornitori(
        boolean fornitore, java.lang.String categoriaEconomica, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByFornitori(fornitore, categoriaEconomica, start, end);
    }

    /**
    * Returns an ordered range of all the clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @param start the lower bound of the range of clienti fornitori dati aggs
    * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findByFornitori(
        boolean fornitore, java.lang.String categoriaEconomica, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByFornitori(fornitore, categoriaEconomica, start, end,
            orderByComparator);
    }

    /**
    * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
    *
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching clienti fornitori dati agg
    * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg findByFornitori_First(
        boolean fornitore, java.lang.String categoriaEconomica,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException {
        return getPersistence()
                   .findByFornitori_First(fornitore, categoriaEconomica,
            orderByComparator);
    }

    /**
    * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
    *
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg fetchByFornitori_First(
        boolean fornitore, java.lang.String categoriaEconomica,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByFornitori_First(fornitore, categoriaEconomica,
            orderByComparator);
    }

    /**
    * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
    *
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching clienti fornitori dati agg
    * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg findByFornitori_Last(
        boolean fornitore, java.lang.String categoriaEconomica,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException {
        return getPersistence()
                   .findByFornitori_Last(fornitore, categoriaEconomica,
            orderByComparator);
    }

    /**
    * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
    *
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg fetchByFornitori_Last(
        boolean fornitore, java.lang.String categoriaEconomica,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByFornitori_Last(fornitore, categoriaEconomica,
            orderByComparator);
    }

    /**
    * Returns the clienti fornitori dati aggs before and after the current clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
    *
    * @param clientiFornitoriDatiAggPK the primary key of the current clienti fornitori dati agg
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next clienti fornitori dati agg
    * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a clienti fornitori dati agg with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg[] findByFornitori_PrevAndNext(
        it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK,
        boolean fornitore, java.lang.String categoriaEconomica,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException {
        return getPersistence()
                   .findByFornitori_PrevAndNext(clientiFornitoriDatiAggPK,
            fornitore, categoriaEconomica, orderByComparator);
    }

    /**
    * Removes all the clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63; from the database.
    *
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @throws SystemException if a system exception occurred
    */
    public static void removeByFornitori(boolean fornitore,
        java.lang.String categoriaEconomica)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByFornitori(fornitore, categoriaEconomica);
    }

    /**
    * Returns the number of clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63;.
    *
    * @param fornitore the fornitore
    * @param categoriaEconomica the categoria economica
    * @return the number of matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static int countByFornitori(boolean fornitore,
        java.lang.String categoriaEconomica)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByFornitori(fornitore, categoriaEconomica);
    }

    /**
    * Returns all the clienti fornitori dati aggs where fornitore = &#63;.
    *
    * @param fornitore the fornitore
    * @return the matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findByTuttiFornitori(
        boolean fornitore)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByTuttiFornitori(fornitore);
    }

    /**
    * Returns a range of all the clienti fornitori dati aggs where fornitore = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param fornitore the fornitore
    * @param start the lower bound of the range of clienti fornitori dati aggs
    * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
    * @return the range of matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findByTuttiFornitori(
        boolean fornitore, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByTuttiFornitori(fornitore, start, end);
    }

    /**
    * Returns an ordered range of all the clienti fornitori dati aggs where fornitore = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param fornitore the fornitore
    * @param start the lower bound of the range of clienti fornitori dati aggs
    * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findByTuttiFornitori(
        boolean fornitore, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByTuttiFornitori(fornitore, start, end,
            orderByComparator);
    }

    /**
    * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63;.
    *
    * @param fornitore the fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching clienti fornitori dati agg
    * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg findByTuttiFornitori_First(
        boolean fornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException {
        return getPersistence()
                   .findByTuttiFornitori_First(fornitore, orderByComparator);
    }

    /**
    * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63;.
    *
    * @param fornitore the fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg fetchByTuttiFornitori_First(
        boolean fornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByTuttiFornitori_First(fornitore, orderByComparator);
    }

    /**
    * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63;.
    *
    * @param fornitore the fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching clienti fornitori dati agg
    * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg findByTuttiFornitori_Last(
        boolean fornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException {
        return getPersistence()
                   .findByTuttiFornitori_Last(fornitore, orderByComparator);
    }

    /**
    * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63;.
    *
    * @param fornitore the fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg fetchByTuttiFornitori_Last(
        boolean fornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByTuttiFornitori_Last(fornitore, orderByComparator);
    }

    /**
    * Returns the clienti fornitori dati aggs before and after the current clienti fornitori dati agg in the ordered set where fornitore = &#63;.
    *
    * @param clientiFornitoriDatiAggPK the primary key of the current clienti fornitori dati agg
    * @param fornitore the fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next clienti fornitori dati agg
    * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a clienti fornitori dati agg with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg[] findByTuttiFornitori_PrevAndNext(
        it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK,
        boolean fornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException {
        return getPersistence()
                   .findByTuttiFornitori_PrevAndNext(clientiFornitoriDatiAggPK,
            fornitore, orderByComparator);
    }

    /**
    * Removes all the clienti fornitori dati aggs where fornitore = &#63; from the database.
    *
    * @param fornitore the fornitore
    * @throws SystemException if a system exception occurred
    */
    public static void removeByTuttiFornitori(boolean fornitore)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByTuttiFornitori(fornitore);
    }

    /**
    * Returns the number of clienti fornitori dati aggs where fornitore = &#63;.
    *
    * @param fornitore the fornitore
    * @return the number of matching clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static int countByTuttiFornitori(boolean fornitore)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByTuttiFornitori(fornitore);
    }

    /**
    * Caches the clienti fornitori dati agg in the entity cache if it is enabled.
    *
    * @param clientiFornitoriDatiAgg the clienti fornitori dati agg
    */
    public static void cacheResult(
        it.bysoftware.ct.model.ClientiFornitoriDatiAgg clientiFornitoriDatiAgg) {
        getPersistence().cacheResult(clientiFornitoriDatiAgg);
    }

    /**
    * Caches the clienti fornitori dati aggs in the entity cache if it is enabled.
    *
    * @param clientiFornitoriDatiAggs the clienti fornitori dati aggs
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> clientiFornitoriDatiAggs) {
        getPersistence().cacheResult(clientiFornitoriDatiAggs);
    }

    /**
    * Creates a new clienti fornitori dati agg with the primary key. Does not add the clienti fornitori dati agg to the database.
    *
    * @param clientiFornitoriDatiAggPK the primary key for the new clienti fornitori dati agg
    * @return the new clienti fornitori dati agg
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg create(
        it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK) {
        return getPersistence().create(clientiFornitoriDatiAggPK);
    }

    /**
    * Removes the clienti fornitori dati agg with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param clientiFornitoriDatiAggPK the primary key of the clienti fornitori dati agg
    * @return the clienti fornitori dati agg that was removed
    * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a clienti fornitori dati agg with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg remove(
        it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException {
        return getPersistence().remove(clientiFornitoriDatiAggPK);
    }

    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg updateImpl(
        it.bysoftware.ct.model.ClientiFornitoriDatiAgg clientiFornitoriDatiAgg)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(clientiFornitoriDatiAgg);
    }

    /**
    * Returns the clienti fornitori dati agg with the primary key or throws a {@link it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException} if it could not be found.
    *
    * @param clientiFornitoriDatiAggPK the primary key of the clienti fornitori dati agg
    * @return the clienti fornitori dati agg
    * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a clienti fornitori dati agg with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg findByPrimaryKey(
        it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException {
        return getPersistence().findByPrimaryKey(clientiFornitoriDatiAggPK);
    }

    /**
    * Returns the clienti fornitori dati agg with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param clientiFornitoriDatiAggPK the primary key of the clienti fornitori dati agg
    * @return the clienti fornitori dati agg, or <code>null</code> if a clienti fornitori dati agg with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ClientiFornitoriDatiAgg fetchByPrimaryKey(
        it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(clientiFornitoriDatiAggPK);
    }

    /**
    * Returns all the clienti fornitori dati aggs.
    *
    * @return the clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the clienti fornitori dati aggs.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of clienti fornitori dati aggs
    * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
    * @return the range of clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the clienti fornitori dati aggs.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of clienti fornitori dati aggs
    * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the clienti fornitori dati aggs from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of clienti fornitori dati aggs.
    *
    * @return the number of clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static ClientiFornitoriDatiAggPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (ClientiFornitoriDatiAggPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    ClientiFornitoriDatiAggPersistence.class.getName());

            ReferenceRegistry.registerReference(ClientiFornitoriDatiAggUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(ClientiFornitoriDatiAggPersistence persistence) {
    }
}
