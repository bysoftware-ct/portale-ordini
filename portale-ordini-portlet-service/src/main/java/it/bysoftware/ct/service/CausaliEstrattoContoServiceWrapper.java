package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CausaliEstrattoContoService}.
 *
 * @author Mario Torrisi
 * @see CausaliEstrattoContoService
 * @generated
 */
public class CausaliEstrattoContoServiceWrapper
    implements CausaliEstrattoContoService,
        ServiceWrapper<CausaliEstrattoContoService> {
    private CausaliEstrattoContoService _causaliEstrattoContoService;

    public CausaliEstrattoContoServiceWrapper(
        CausaliEstrattoContoService causaliEstrattoContoService) {
        _causaliEstrattoContoService = causaliEstrattoContoService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _causaliEstrattoContoService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _causaliEstrattoContoService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _causaliEstrattoContoService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public CausaliEstrattoContoService getWrappedCausaliEstrattoContoService() {
        return _causaliEstrattoContoService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedCausaliEstrattoContoService(
        CausaliEstrattoContoService causaliEstrattoContoService) {
        _causaliEstrattoContoService = causaliEstrattoContoService;
    }

    @Override
    public CausaliEstrattoContoService getWrappedService() {
        return _causaliEstrattoContoService;
    }

    @Override
    public void setWrappedService(
        CausaliEstrattoContoService causaliEstrattoContoService) {
        _causaliEstrattoContoService = causaliEstrattoContoService;
    }
}
