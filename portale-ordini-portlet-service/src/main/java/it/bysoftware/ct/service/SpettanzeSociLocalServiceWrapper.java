package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SpettanzeSociLocalService}.
 *
 * @author Mario Torrisi
 * @see SpettanzeSociLocalService
 * @generated
 */
public class SpettanzeSociLocalServiceWrapper
    implements SpettanzeSociLocalService,
        ServiceWrapper<SpettanzeSociLocalService> {
    private SpettanzeSociLocalService _spettanzeSociLocalService;

    public SpettanzeSociLocalServiceWrapper(
        SpettanzeSociLocalService spettanzeSociLocalService) {
        _spettanzeSociLocalService = spettanzeSociLocalService;
    }

    /**
    * Adds the spettanze soci to the database. Also notifies the appropriate model listeners.
    *
    * @param spettanzeSoci the spettanze soci
    * @return the spettanze soci that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.SpettanzeSoci addSpettanzeSoci(
        it.bysoftware.ct.model.SpettanzeSoci spettanzeSoci)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _spettanzeSociLocalService.addSpettanzeSoci(spettanzeSoci);
    }

    /**
    * Creates a new spettanze soci with the primary key. Does not add the spettanze soci to the database.
    *
    * @param id the primary key for the new spettanze soci
    * @return the new spettanze soci
    */
    @Override
    public it.bysoftware.ct.model.SpettanzeSoci createSpettanzeSoci(long id) {
        return _spettanzeSociLocalService.createSpettanzeSoci(id);
    }

    /**
    * Deletes the spettanze soci with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the spettanze soci
    * @return the spettanze soci that was removed
    * @throws PortalException if a spettanze soci with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.SpettanzeSoci deleteSpettanzeSoci(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _spettanzeSociLocalService.deleteSpettanzeSoci(id);
    }

    /**
    * Deletes the spettanze soci from the database. Also notifies the appropriate model listeners.
    *
    * @param spettanzeSoci the spettanze soci
    * @return the spettanze soci that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.SpettanzeSoci deleteSpettanzeSoci(
        it.bysoftware.ct.model.SpettanzeSoci spettanzeSoci)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _spettanzeSociLocalService.deleteSpettanzeSoci(spettanzeSoci);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _spettanzeSociLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _spettanzeSociLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _spettanzeSociLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _spettanzeSociLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _spettanzeSociLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _spettanzeSociLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public it.bysoftware.ct.model.SpettanzeSoci fetchSpettanzeSoci(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _spettanzeSociLocalService.fetchSpettanzeSoci(id);
    }

    /**
    * Returns the spettanze soci with the primary key.
    *
    * @param id the primary key of the spettanze soci
    * @return the spettanze soci
    * @throws PortalException if a spettanze soci with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.SpettanzeSoci getSpettanzeSoci(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _spettanzeSociLocalService.getSpettanzeSoci(id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _spettanzeSociLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the spettanze socis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of spettanze socis
    * @param end the upper bound of the range of spettanze socis (not inclusive)
    * @return the range of spettanze socis
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.SpettanzeSoci> getSpettanzeSocis(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _spettanzeSociLocalService.getSpettanzeSocis(start, end);
    }

    /**
    * Returns the number of spettanze socis.
    *
    * @return the number of spettanze socis
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getSpettanzeSocisCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _spettanzeSociLocalService.getSpettanzeSocisCount();
    }

    /**
    * Updates the spettanze soci in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param spettanzeSoci the spettanze soci
    * @return the spettanze soci that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.SpettanzeSoci updateSpettanzeSoci(
        it.bysoftware.ct.model.SpettanzeSoci spettanzeSoci)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _spettanzeSociLocalService.updateSpettanzeSoci(spettanzeSoci);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _spettanzeSociLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _spettanzeSociLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _spettanzeSociLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByPartner(
        java.lang.String subjectCode) {
        return _spettanzeSociLocalService.findByPartner(subjectCode);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByPartnerState(
        java.lang.String subjectCode, java.lang.Integer state) {
        return _spettanzeSociLocalService.findByPartnerState(subjectCode, state);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByPartnerPaymentYearPaymentId(
        java.lang.String subjectCode, int year, int paymentId) {
        return _spettanzeSociLocalService.findByPartnerPaymentYearPaymentId(subjectCode,
            year, paymentId);
    }

    @Override
    public it.bysoftware.ct.model.SpettanzeSoci findByInvoice(
        it.bysoftware.ct.service.persistence.TestataFattureClientiPK invoicePK,
        java.lang.String subjectCode) {
        return _spettanzeSociLocalService.findByInvoice(invoicePK, subjectCode);
    }

    @Override
    public it.bysoftware.ct.model.SpettanzeSoci findByInvoiceState(
        it.bysoftware.ct.service.persistence.TestataFattureClientiPK invoicePK,
        java.lang.String subjectCode, java.lang.Integer state) {
        return _spettanzeSociLocalService.findByInvoiceState(invoicePK,
            subjectCode, state);
    }

    @Override
    public java.util.List<java.lang.Object[]> getCreditAmountPerPartner(
        java.lang.Integer state, java.util.Date from, java.util.Date to) {
        return _spettanzeSociLocalService.getCreditAmountPerPartner(state,
            from, to);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByPartnerAndComputeDate(
        java.lang.String partnerCode, java.util.Date from, java.util.Date to,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator comp) {
        return _spettanzeSociLocalService.findByPartnerAndComputeDate(partnerCode,
            from, to, start, end, comp);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public SpettanzeSociLocalService getWrappedSpettanzeSociLocalService() {
        return _spettanzeSociLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedSpettanzeSociLocalService(
        SpettanzeSociLocalService spettanzeSociLocalService) {
        _spettanzeSociLocalService = spettanzeSociLocalService;
    }

    @Override
    public SpettanzeSociLocalService getWrappedService() {
        return _spettanzeSociLocalService;
    }

    @Override
    public void setWrappedService(
        SpettanzeSociLocalService spettanzeSociLocalService) {
        _spettanzeSociLocalService = spettanzeSociLocalService;
    }
}
