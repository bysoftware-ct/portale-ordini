package it.bysoftware.ct.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.BaseLocalService;
import com.liferay.portal.service.InvokableLocalService;
import com.liferay.portal.service.PersistedModelLocalService;

/**
 * Provides the local service interface for Ordine. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Mario Torrisi
 * @see OrdineLocalServiceUtil
 * @see it.bysoftware.ct.service.base.OrdineLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.impl.OrdineLocalServiceImpl
 * @generated
 */
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
    PortalException.class, SystemException.class}
)
public interface OrdineLocalService extends BaseLocalService,
    InvokableLocalService, PersistedModelLocalService {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link OrdineLocalServiceUtil} to access the ordine local service. Add custom service methods to {@link it.bysoftware.ct.service.impl.OrdineLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */

    /**
    * Adds the ordine to the database. Also notifies the appropriate model listeners.
    *
    * @param ordine the ordine
    * @return the ordine that was added
    * @throws SystemException if a system exception occurred
    */
    @com.liferay.portal.kernel.search.Indexable(type = IndexableType.REINDEX)
    public it.bysoftware.ct.model.Ordine addOrdine(
        it.bysoftware.ct.model.Ordine ordine)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Creates a new ordine with the primary key. Does not add the ordine to the database.
    *
    * @param id the primary key for the new ordine
    * @return the new ordine
    */
    public it.bysoftware.ct.model.Ordine createOrdine(long id);

    /**
    * Deletes the ordine with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the ordine
    * @return the ordine that was removed
    * @throws PortalException if a ordine with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @com.liferay.portal.kernel.search.Indexable(type = IndexableType.DELETE)
    public it.bysoftware.ct.model.Ordine deleteOrdine(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Deletes the ordine from the database. Also notifies the appropriate model listeners.
    *
    * @param ordine the ordine
    * @return the ordine that was removed
    * @throws SystemException if a system exception occurred
    */
    @com.liferay.portal.kernel.search.Indexable(type = IndexableType.DELETE)
    public it.bysoftware.ct.model.Ordine deleteOrdine(
        it.bysoftware.ct.model.Ordine ordine)
        throws com.liferay.portal.kernel.exception.SystemException;

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery();

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public it.bysoftware.ct.model.Ordine fetchOrdine(long id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the ordine with the primary key.
    *
    * @param id the primary key of the ordine
    * @return the ordine
    * @throws PortalException if a ordine with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public it.bysoftware.ct.model.Ordine getOrdine(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the ordines.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @return the range of ordines
    * @throws SystemException if a system exception occurred
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<it.bysoftware.ct.model.Ordine> getOrdines(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of ordines.
    *
    * @return the number of ordines
    * @throws SystemException if a system exception occurred
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getOrdinesCount()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Updates the ordine in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param ordine the ordine
    * @return the ordine that was updated
    * @throws SystemException if a system exception occurred
    */
    @com.liferay.portal.kernel.search.Indexable(type = IndexableType.REINDEX)
    public it.bysoftware.ct.model.Ordine updateOrdine(
        it.bysoftware.ct.model.Ordine ordine)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier();

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier);

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getNumber(int year, java.lang.String center);

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param partnerCode
    the partner code
    * @param state
    the state
    * @param passport
    the passport number
    * @return the array of order IDs
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<java.lang.Object[]> getOrderByCodeStatePassport(
        java.lang.String partnerCode, int state, java.lang.String passport);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<it.bysoftware.ct.model.Ordine> getOrderByCustomer(
        java.lang.String code);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<it.bysoftware.ct.model.Ordine> getOrderByState(
        int state);

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param state
    the state
    * @param passport
    the passport number
    * @return the array of order IDs
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<java.lang.Object[]> getOrderByStatePassport(
        int state, java.lang.String passport);

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param state
    the state
    * @param passport
    the passport number
    * @return the array of order IDs
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<java.lang.Object[]> getOrderByStatePassportPartner(
        int state, java.lang.String passport, java.lang.String partnerCode);

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param partnerCode
    the partner code
    * @param from
    from date
    * @param to
    to date
    * @return the array of order IDs
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<java.lang.Object[]> getOrderBetweenDatePerPartner(
        java.lang.String partnerCode, java.util.Date from, java.util.Date to);

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param from
    from date
    * @param to
    to date
    * @return the array of order IDs
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<java.lang.Object[]> getOrderBetweenDate(
        java.util.Date from, java.util.Date to);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<java.lang.Object[]> getOrderBetweenDatePerItem(
        java.lang.String cod, java.util.Date from, java.util.Date to);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<java.lang.Object[]> getItemsInOrderBetweenDate(
        java.util.Date from, java.util.Date to);

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param partnerCode
    the partner code
    * @param from
    from date
    * @param to
    to date
    * @return the array of order IDs
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<java.lang.Object[]> getOrderWithPassportByPartnerAndDate(
        java.lang.String partnerCode, java.util.Date from, java.util.Date to);

    /**
    * Returns an array of object that contains the order IDs in which the
    * provided partner is involved having the specified state.
    *
    * @param from
    from date
    * @param to
    to date
    * @return the array of order IDs
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<java.lang.Object[]> getOrderWithPassportByDate(
        java.util.Date from, java.util.Date to);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<it.bysoftware.ct.model.Ordine> getCustomerConfirmedOrders(
        java.lang.String code);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<it.bysoftware.ct.model.Ordine> getCustomerPendingOrders(
        java.lang.String code);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<it.bysoftware.ct.model.Ordine> getSavedCustomerOrders(
        java.lang.String code);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<it.bysoftware.ct.model.Ordine> getScheduledCustomerOrders(
        java.lang.String code);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<it.bysoftware.ct.model.Ordine> getProcessedCustomerOrders(
        java.lang.String code);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<it.bysoftware.ct.model.Ordine> getUnScheduledOrderByCustomer(
        java.lang.String code);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<it.bysoftware.ct.model.Ordine> getTodayReadyOrders();

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<it.bysoftware.ct.model.Ordine> getTodayConfirmedOrders();

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<it.bysoftware.ct.model.Ordine> getTodayProcessedOrders();

    public java.util.List<it.bysoftware.ct.model.Ordine> findOrders(
        java.lang.String customerCode, java.util.Date orderDate,
        java.util.Date shipDate, boolean showsInactive, boolean showsDraft,
        boolean showsPending, boolean showsApproved, boolean showsProcessed,
        boolean andSearch, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getFindOrdersCount(java.lang.String customerCode,
        java.util.Date orderDate, java.util.Date shipDate,
        boolean showsInactive, boolean showsDraft, boolean showsPending,
        boolean showsApproved, boolean showsProcessed, boolean andSearch)
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public it.bysoftware.ct.model.Ordine getByYearNumberCenter(int n,
        java.lang.String c);
}
