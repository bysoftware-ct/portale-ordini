package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link RigoDocumentoService}.
 *
 * @author Mario Torrisi
 * @see RigoDocumentoService
 * @generated
 */
public class RigoDocumentoServiceWrapper implements RigoDocumentoService,
    ServiceWrapper<RigoDocumentoService> {
    private RigoDocumentoService _rigoDocumentoService;

    public RigoDocumentoServiceWrapper(
        RigoDocumentoService rigoDocumentoService) {
        _rigoDocumentoService = rigoDocumentoService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _rigoDocumentoService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _rigoDocumentoService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _rigoDocumentoService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public RigoDocumentoService getWrappedRigoDocumentoService() {
        return _rigoDocumentoService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedRigoDocumentoService(
        RigoDocumentoService rigoDocumentoService) {
        _rigoDocumentoService = rigoDocumentoService;
    }

    @Override
    public RigoDocumentoService getWrappedService() {
        return _rigoDocumentoService;
    }

    @Override
    public void setWrappedService(RigoDocumentoService rigoDocumentoService) {
        _rigoDocumentoService = rigoDocumentoService;
    }
}
