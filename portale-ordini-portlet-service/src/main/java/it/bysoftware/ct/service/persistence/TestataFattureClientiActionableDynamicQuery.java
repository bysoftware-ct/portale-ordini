package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.TestataFattureClienti;
import it.bysoftware.ct.service.TestataFattureClientiLocalServiceUtil;

/**
 * @author Mario Torrisi
 * @generated
 */
public abstract class TestataFattureClientiActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public TestataFattureClientiActionableDynamicQuery()
        throws SystemException {
        setBaseLocalService(TestataFattureClientiLocalServiceUtil.getService());
        setClass(TestataFattureClienti.class);

        setClassLoader(it.bysoftware.ct.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("primaryKey.anno");
    }
}
