package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CarrelloService}.
 *
 * @author Mario Torrisi
 * @see CarrelloService
 * @generated
 */
public class CarrelloServiceWrapper implements CarrelloService,
    ServiceWrapper<CarrelloService> {
    private CarrelloService _carrelloService;

    public CarrelloServiceWrapper(CarrelloService carrelloService) {
        _carrelloService = carrelloService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _carrelloService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _carrelloService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _carrelloService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public CarrelloService getWrappedCarrelloService() {
        return _carrelloService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedCarrelloService(CarrelloService carrelloService) {
        _carrelloService = carrelloService;
    }

    @Override
    public CarrelloService getWrappedService() {
        return _carrelloService;
    }

    @Override
    public void setWrappedService(CarrelloService carrelloService) {
        _carrelloService = carrelloService;
    }
}
