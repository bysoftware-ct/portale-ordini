package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class DescrizioniVariantiPK implements Comparable<DescrizioniVariantiPK>,
    Serializable {
    public String codiceArticolo;
    public String codiceVariante;

    public DescrizioniVariantiPK() {
    }

    public DescrizioniVariantiPK(String codiceArticolo, String codiceVariante) {
        this.codiceArticolo = codiceArticolo;
        this.codiceVariante = codiceVariante;
    }

    public String getCodiceArticolo() {
        return codiceArticolo;
    }

    public void setCodiceArticolo(String codiceArticolo) {
        this.codiceArticolo = codiceArticolo;
    }

    public String getCodiceVariante() {
        return codiceVariante;
    }

    public void setCodiceVariante(String codiceVariante) {
        this.codiceVariante = codiceVariante;
    }

    @Override
    public int compareTo(DescrizioniVariantiPK pk) {
        if (pk == null) {
            return -1;
        }

        int value = 0;

        value = codiceArticolo.compareTo(pk.codiceArticolo);

        if (value != 0) {
            return value;
        }

        value = codiceVariante.compareTo(pk.codiceVariante);

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof DescrizioniVariantiPK)) {
            return false;
        }

        DescrizioniVariantiPK pk = (DescrizioniVariantiPK) obj;

        if ((codiceArticolo.equals(pk.codiceArticolo)) &&
                (codiceVariante.equals(pk.codiceVariante))) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (String.valueOf(codiceArticolo) +
        String.valueOf(codiceVariante)).hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(10);

        sb.append(StringPool.OPEN_CURLY_BRACE);

        sb.append("codiceArticolo");
        sb.append(StringPool.EQUAL);
        sb.append(codiceArticolo);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("codiceVariante");
        sb.append(StringPool.EQUAL);
        sb.append(codiceVariante);

        sb.append(StringPool.CLOSE_CURLY_BRACE);

        return sb.toString();
    }
}
