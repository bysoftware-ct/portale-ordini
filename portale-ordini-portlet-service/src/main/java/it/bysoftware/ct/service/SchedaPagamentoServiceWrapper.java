package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SchedaPagamentoService}.
 *
 * @author Mario Torrisi
 * @see SchedaPagamentoService
 * @generated
 */
public class SchedaPagamentoServiceWrapper implements SchedaPagamentoService,
    ServiceWrapper<SchedaPagamentoService> {
    private SchedaPagamentoService _schedaPagamentoService;

    public SchedaPagamentoServiceWrapper(
        SchedaPagamentoService schedaPagamentoService) {
        _schedaPagamentoService = schedaPagamentoService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _schedaPagamentoService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _schedaPagamentoService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _schedaPagamentoService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public SchedaPagamentoService getWrappedSchedaPagamentoService() {
        return _schedaPagamentoService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedSchedaPagamentoService(
        SchedaPagamentoService schedaPagamentoService) {
        _schedaPagamentoService = schedaPagamentoService;
    }

    @Override
    public SchedaPagamentoService getWrappedService() {
        return _schedaPagamentoService;
    }

    @Override
    public void setWrappedService(SchedaPagamentoService schedaPagamentoService) {
        _schedaPagamentoService = schedaPagamentoService;
    }
}
