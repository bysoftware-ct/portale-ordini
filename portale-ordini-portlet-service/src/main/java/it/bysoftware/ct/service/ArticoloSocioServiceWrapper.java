package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ArticoloSocioService}.
 *
 * @author Mario Torrisi
 * @see ArticoloSocioService
 * @generated
 */
public class ArticoloSocioServiceWrapper implements ArticoloSocioService,
    ServiceWrapper<ArticoloSocioService> {
    private ArticoloSocioService _articoloSocioService;

    public ArticoloSocioServiceWrapper(
        ArticoloSocioService articoloSocioService) {
        _articoloSocioService = articoloSocioService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _articoloSocioService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _articoloSocioService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _articoloSocioService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public ArticoloSocioService getWrappedArticoloSocioService() {
        return _articoloSocioService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedArticoloSocioService(
        ArticoloSocioService articoloSocioService) {
        _articoloSocioService = articoloSocioService;
    }

    @Override
    public ArticoloSocioService getWrappedService() {
        return _articoloSocioService;
    }

    @Override
    public void setWrappedService(ArticoloSocioService articoloSocioService) {
        _articoloSocioService = articoloSocioService;
    }
}
