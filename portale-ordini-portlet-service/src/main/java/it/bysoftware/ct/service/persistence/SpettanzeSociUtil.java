package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.SpettanzeSoci;

import java.util.List;

/**
 * The persistence utility for the spettanze soci service. This utility wraps {@link SpettanzeSociPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see SpettanzeSociPersistence
 * @see SpettanzeSociPersistenceImpl
 * @generated
 */
public class SpettanzeSociUtil {
    private static SpettanzeSociPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(SpettanzeSoci spettanzeSoci) {
        getPersistence().clearCache(spettanzeSoci);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<SpettanzeSoci> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<SpettanzeSoci> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<SpettanzeSoci> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static SpettanzeSoci update(SpettanzeSoci spettanzeSoci)
        throws SystemException {
        return getPersistence().update(spettanzeSoci);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static SpettanzeSoci update(SpettanzeSoci spettanzeSoci,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(spettanzeSoci, serviceContext);
    }

    /**
    * Returns the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; and stato = &#63; or throws a {@link it.bysoftware.ct.NoSuchSpettanzeSociException} if it could not be found.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @return the matching spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci findByFatturaVenditaStato(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, int numeroProtocollo,
        java.lang.String codiceFornitore, int stato)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException {
        return getPersistence()
                   .findByFatturaVenditaStato(anno, codiceAttivita,
            codiceCentro, numeroProtocollo, codiceFornitore, stato);
    }

    /**
    * Returns the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; and stato = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @return the matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci fetchByFatturaVenditaStato(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, int numeroProtocollo,
        java.lang.String codiceFornitore, int stato)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByFatturaVenditaStato(anno, codiceAttivita,
            codiceCentro, numeroProtocollo, codiceFornitore, stato);
    }

    /**
    * Returns the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; and stato = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci fetchByFatturaVenditaStato(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, int numeroProtocollo,
        java.lang.String codiceFornitore, int stato, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByFatturaVenditaStato(anno, codiceAttivita,
            codiceCentro, numeroProtocollo, codiceFornitore, stato,
            retrieveFromCache);
    }

    /**
    * Removes the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; and stato = &#63; from the database.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @return the spettanze soci that was removed
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci removeByFatturaVenditaStato(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, int numeroProtocollo,
        java.lang.String codiceFornitore, int stato)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException {
        return getPersistence()
                   .removeByFatturaVenditaStato(anno, codiceAttivita,
            codiceCentro, numeroProtocollo, codiceFornitore, stato);
    }

    /**
    * Returns the number of spettanze socis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; and stato = &#63;.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @return the number of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static int countByFatturaVenditaStato(int anno,
        java.lang.String codiceAttivita, java.lang.String codiceCentro,
        int numeroProtocollo, java.lang.String codiceFornitore, int stato)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByFatturaVenditaStato(anno, codiceAttivita,
            codiceCentro, numeroProtocollo, codiceFornitore, stato);
    }

    /**
    * Returns the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; or throws a {@link it.bysoftware.ct.NoSuchSpettanzeSociException} if it could not be found.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @return the matching spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci findByFatturaVendita(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, int numeroProtocollo,
        java.lang.String codiceFornitore)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException {
        return getPersistence()
                   .findByFatturaVendita(anno, codiceAttivita, codiceCentro,
            numeroProtocollo, codiceFornitore);
    }

    /**
    * Returns the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @return the matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci fetchByFatturaVendita(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, int numeroProtocollo,
        java.lang.String codiceFornitore)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByFatturaVendita(anno, codiceAttivita, codiceCentro,
            numeroProtocollo, codiceFornitore);
    }

    /**
    * Returns the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci fetchByFatturaVendita(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, int numeroProtocollo,
        java.lang.String codiceFornitore, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByFatturaVendita(anno, codiceAttivita, codiceCentro,
            numeroProtocollo, codiceFornitore, retrieveFromCache);
    }

    /**
    * Removes the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; from the database.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @return the spettanze soci that was removed
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci removeByFatturaVendita(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, int numeroProtocollo,
        java.lang.String codiceFornitore)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException {
        return getPersistence()
                   .removeByFatturaVendita(anno, codiceAttivita, codiceCentro,
            numeroProtocollo, codiceFornitore);
    }

    /**
    * Returns the number of spettanze socis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63;.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @return the number of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static int countByFatturaVendita(int anno,
        java.lang.String codiceAttivita, java.lang.String codiceCentro,
        int numeroProtocollo, java.lang.String codiceFornitore)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByFatturaVendita(anno, codiceAttivita, codiceCentro,
            numeroProtocollo, codiceFornitore);
    }

    /**
    * Returns all the spettanze socis where codiceFornitore = &#63; and stato = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @return the matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByFornitoreStato(
        java.lang.String codiceFornitore, int stato)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByFornitoreStato(codiceFornitore, stato);
    }

    /**
    * Returns a range of all the spettanze socis where codiceFornitore = &#63; and stato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @param start the lower bound of the range of spettanze socis
    * @param end the upper bound of the range of spettanze socis (not inclusive)
    * @return the range of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByFornitoreStato(
        java.lang.String codiceFornitore, int stato, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByFornitoreStato(codiceFornitore, stato, start, end);
    }

    /**
    * Returns an ordered range of all the spettanze socis where codiceFornitore = &#63; and stato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @param start the lower bound of the range of spettanze socis
    * @param end the upper bound of the range of spettanze socis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByFornitoreStato(
        java.lang.String codiceFornitore, int stato, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByFornitoreStato(codiceFornitore, stato, start, end,
            orderByComparator);
    }

    /**
    * Returns the first spettanze soci in the ordered set where codiceFornitore = &#63; and stato = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci findByFornitoreStato_First(
        java.lang.String codiceFornitore, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException {
        return getPersistence()
                   .findByFornitoreStato_First(codiceFornitore, stato,
            orderByComparator);
    }

    /**
    * Returns the first spettanze soci in the ordered set where codiceFornitore = &#63; and stato = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci fetchByFornitoreStato_First(
        java.lang.String codiceFornitore, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByFornitoreStato_First(codiceFornitore, stato,
            orderByComparator);
    }

    /**
    * Returns the last spettanze soci in the ordered set where codiceFornitore = &#63; and stato = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci findByFornitoreStato_Last(
        java.lang.String codiceFornitore, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException {
        return getPersistence()
                   .findByFornitoreStato_Last(codiceFornitore, stato,
            orderByComparator);
    }

    /**
    * Returns the last spettanze soci in the ordered set where codiceFornitore = &#63; and stato = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci fetchByFornitoreStato_Last(
        java.lang.String codiceFornitore, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByFornitoreStato_Last(codiceFornitore, stato,
            orderByComparator);
    }

    /**
    * Returns the spettanze socis before and after the current spettanze soci in the ordered set where codiceFornitore = &#63; and stato = &#63;.
    *
    * @param id the primary key of the current spettanze soci
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a spettanze soci with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci[] findByFornitoreStato_PrevAndNext(
        long id, java.lang.String codiceFornitore, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException {
        return getPersistence()
                   .findByFornitoreStato_PrevAndNext(id, codiceFornitore,
            stato, orderByComparator);
    }

    /**
    * Removes all the spettanze socis where codiceFornitore = &#63; and stato = &#63; from the database.
    *
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @throws SystemException if a system exception occurred
    */
    public static void removeByFornitoreStato(
        java.lang.String codiceFornitore, int stato)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByFornitoreStato(codiceFornitore, stato);
    }

    /**
    * Returns the number of spettanze socis where codiceFornitore = &#63; and stato = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @return the number of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static int countByFornitoreStato(java.lang.String codiceFornitore,
        int stato) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByFornitoreStato(codiceFornitore, stato);
    }

    /**
    * Returns all the spettanze socis where codiceFornitore = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @return the matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByFornitore(
        java.lang.String codiceFornitore)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByFornitore(codiceFornitore);
    }

    /**
    * Returns a range of all the spettanze socis where codiceFornitore = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceFornitore the codice fornitore
    * @param start the lower bound of the range of spettanze socis
    * @param end the upper bound of the range of spettanze socis (not inclusive)
    * @return the range of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByFornitore(
        java.lang.String codiceFornitore, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByFornitore(codiceFornitore, start, end);
    }

    /**
    * Returns an ordered range of all the spettanze socis where codiceFornitore = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceFornitore the codice fornitore
    * @param start the lower bound of the range of spettanze socis
    * @param end the upper bound of the range of spettanze socis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByFornitore(
        java.lang.String codiceFornitore, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByFornitore(codiceFornitore, start, end,
            orderByComparator);
    }

    /**
    * Returns the first spettanze soci in the ordered set where codiceFornitore = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci findByFornitore_First(
        java.lang.String codiceFornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException {
        return getPersistence()
                   .findByFornitore_First(codiceFornitore, orderByComparator);
    }

    /**
    * Returns the first spettanze soci in the ordered set where codiceFornitore = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci fetchByFornitore_First(
        java.lang.String codiceFornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByFornitore_First(codiceFornitore, orderByComparator);
    }

    /**
    * Returns the last spettanze soci in the ordered set where codiceFornitore = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci findByFornitore_Last(
        java.lang.String codiceFornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException {
        return getPersistence()
                   .findByFornitore_Last(codiceFornitore, orderByComparator);
    }

    /**
    * Returns the last spettanze soci in the ordered set where codiceFornitore = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci fetchByFornitore_Last(
        java.lang.String codiceFornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByFornitore_Last(codiceFornitore, orderByComparator);
    }

    /**
    * Returns the spettanze socis before and after the current spettanze soci in the ordered set where codiceFornitore = &#63;.
    *
    * @param id the primary key of the current spettanze soci
    * @param codiceFornitore the codice fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a spettanze soci with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci[] findByFornitore_PrevAndNext(
        long id, java.lang.String codiceFornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException {
        return getPersistence()
                   .findByFornitore_PrevAndNext(id, codiceFornitore,
            orderByComparator);
    }

    /**
    * Removes all the spettanze socis where codiceFornitore = &#63; from the database.
    *
    * @param codiceFornitore the codice fornitore
    * @throws SystemException if a system exception occurred
    */
    public static void removeByFornitore(java.lang.String codiceFornitore)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByFornitore(codiceFornitore);
    }

    /**
    * Returns the number of spettanze socis where codiceFornitore = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @return the number of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static int countByFornitore(java.lang.String codiceFornitore)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByFornitore(codiceFornitore);
    }

    /**
    * Returns all the spettanze socis where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @return the matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByFornitoreAnnoPagamentoIdPagamento(
        java.lang.String codiceFornitore, int annoPagamento, int idPagamento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByFornitoreAnnoPagamentoIdPagamento(codiceFornitore,
            annoPagamento, idPagamento);
    }

    /**
    * Returns a range of all the spettanze socis where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param start the lower bound of the range of spettanze socis
    * @param end the upper bound of the range of spettanze socis (not inclusive)
    * @return the range of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByFornitoreAnnoPagamentoIdPagamento(
        java.lang.String codiceFornitore, int annoPagamento, int idPagamento,
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByFornitoreAnnoPagamentoIdPagamento(codiceFornitore,
            annoPagamento, idPagamento, start, end);
    }

    /**
    * Returns an ordered range of all the spettanze socis where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param start the lower bound of the range of spettanze socis
    * @param end the upper bound of the range of spettanze socis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByFornitoreAnnoPagamentoIdPagamento(
        java.lang.String codiceFornitore, int annoPagamento, int idPagamento,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByFornitoreAnnoPagamentoIdPagamento(codiceFornitore,
            annoPagamento, idPagamento, start, end, orderByComparator);
    }

    /**
    * Returns the first spettanze soci in the ordered set where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci findByFornitoreAnnoPagamentoIdPagamento_First(
        java.lang.String codiceFornitore, int annoPagamento, int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException {
        return getPersistence()
                   .findByFornitoreAnnoPagamentoIdPagamento_First(codiceFornitore,
            annoPagamento, idPagamento, orderByComparator);
    }

    /**
    * Returns the first spettanze soci in the ordered set where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci fetchByFornitoreAnnoPagamentoIdPagamento_First(
        java.lang.String codiceFornitore, int annoPagamento, int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByFornitoreAnnoPagamentoIdPagamento_First(codiceFornitore,
            annoPagamento, idPagamento, orderByComparator);
    }

    /**
    * Returns the last spettanze soci in the ordered set where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci findByFornitoreAnnoPagamentoIdPagamento_Last(
        java.lang.String codiceFornitore, int annoPagamento, int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException {
        return getPersistence()
                   .findByFornitoreAnnoPagamentoIdPagamento_Last(codiceFornitore,
            annoPagamento, idPagamento, orderByComparator);
    }

    /**
    * Returns the last spettanze soci in the ordered set where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci fetchByFornitoreAnnoPagamentoIdPagamento_Last(
        java.lang.String codiceFornitore, int annoPagamento, int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByFornitoreAnnoPagamentoIdPagamento_Last(codiceFornitore,
            annoPagamento, idPagamento, orderByComparator);
    }

    /**
    * Returns the spettanze socis before and after the current spettanze soci in the ordered set where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param id the primary key of the current spettanze soci
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a spettanze soci with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci[] findByFornitoreAnnoPagamentoIdPagamento_PrevAndNext(
        long id, java.lang.String codiceFornitore, int annoPagamento,
        int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException {
        return getPersistence()
                   .findByFornitoreAnnoPagamentoIdPagamento_PrevAndNext(id,
            codiceFornitore, annoPagamento, idPagamento, orderByComparator);
    }

    /**
    * Removes all the spettanze socis where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63; from the database.
    *
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @throws SystemException if a system exception occurred
    */
    public static void removeByFornitoreAnnoPagamentoIdPagamento(
        java.lang.String codiceFornitore, int annoPagamento, int idPagamento)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByFornitoreAnnoPagamentoIdPagamento(codiceFornitore,
            annoPagamento, idPagamento);
    }

    /**
    * Returns the number of spettanze socis where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @return the number of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static int countByFornitoreAnnoPagamentoIdPagamento(
        java.lang.String codiceFornitore, int annoPagamento, int idPagamento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByFornitoreAnnoPagamentoIdPagamento(codiceFornitore,
            annoPagamento, idPagamento);
    }

    /**
    * Caches the spettanze soci in the entity cache if it is enabled.
    *
    * @param spettanzeSoci the spettanze soci
    */
    public static void cacheResult(
        it.bysoftware.ct.model.SpettanzeSoci spettanzeSoci) {
        getPersistence().cacheResult(spettanzeSoci);
    }

    /**
    * Caches the spettanze socis in the entity cache if it is enabled.
    *
    * @param spettanzeSocis the spettanze socis
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.SpettanzeSoci> spettanzeSocis) {
        getPersistence().cacheResult(spettanzeSocis);
    }

    /**
    * Creates a new spettanze soci with the primary key. Does not add the spettanze soci to the database.
    *
    * @param id the primary key for the new spettanze soci
    * @return the new spettanze soci
    */
    public static it.bysoftware.ct.model.SpettanzeSoci create(long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the spettanze soci with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the spettanze soci
    * @return the spettanze soci that was removed
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a spettanze soci with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci remove(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException {
        return getPersistence().remove(id);
    }

    public static it.bysoftware.ct.model.SpettanzeSoci updateImpl(
        it.bysoftware.ct.model.SpettanzeSoci spettanzeSoci)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(spettanzeSoci);
    }

    /**
    * Returns the spettanze soci with the primary key or throws a {@link it.bysoftware.ct.NoSuchSpettanzeSociException} if it could not be found.
    *
    * @param id the primary key of the spettanze soci
    * @return the spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a spettanze soci with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci findByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the spettanze soci with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the spettanze soci
    * @return the spettanze soci, or <code>null</code> if a spettanze soci with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the spettanze socis.
    *
    * @return the spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the spettanze socis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of spettanze socis
    * @param end the upper bound of the range of spettanze socis (not inclusive)
    * @return the range of spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the spettanze socis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of spettanze socis
    * @param end the upper bound of the range of spettanze socis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the spettanze socis from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of spettanze socis.
    *
    * @return the number of spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static SpettanzeSociPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (SpettanzeSociPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    SpettanzeSociPersistence.class.getName());

            ReferenceRegistry.registerReference(SpettanzeSociUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(SpettanzeSociPersistence persistence) {
    }
}
