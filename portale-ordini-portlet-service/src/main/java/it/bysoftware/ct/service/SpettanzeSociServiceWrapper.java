package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SpettanzeSociService}.
 *
 * @author Mario Torrisi
 * @see SpettanzeSociService
 * @generated
 */
public class SpettanzeSociServiceWrapper implements SpettanzeSociService,
    ServiceWrapper<SpettanzeSociService> {
    private SpettanzeSociService _spettanzeSociService;

    public SpettanzeSociServiceWrapper(
        SpettanzeSociService spettanzeSociService) {
        _spettanzeSociService = spettanzeSociService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _spettanzeSociService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _spettanzeSociService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _spettanzeSociService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public SpettanzeSociService getWrappedSpettanzeSociService() {
        return _spettanzeSociService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedSpettanzeSociService(
        SpettanzeSociService spettanzeSociService) {
        _spettanzeSociService = spettanzeSociService;
    }

    @Override
    public SpettanzeSociService getWrappedService() {
        return _spettanzeSociService;
    }

    @Override
    public void setWrappedService(SpettanzeSociService spettanzeSociService) {
        _spettanzeSociService = spettanzeSociService;
    }
}
