package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.VariantiPianta;

import java.util.List;

/**
 * The persistence utility for the varianti pianta service. This utility wraps {@link VariantiPiantaPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see VariantiPiantaPersistence
 * @see VariantiPiantaPersistenceImpl
 * @generated
 */
public class VariantiPiantaUtil {
    private static VariantiPiantaPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(VariantiPianta variantiPianta) {
        getPersistence().clearCache(variantiPianta);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<VariantiPianta> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<VariantiPianta> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<VariantiPianta> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static VariantiPianta update(VariantiPianta variantiPianta)
        throws SystemException {
        return getPersistence().update(variantiPianta);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static VariantiPianta update(VariantiPianta variantiPianta,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(variantiPianta, serviceContext);
    }

    /**
    * Returns all the varianti piantas where idPianta = &#63;.
    *
    * @param idPianta the id pianta
    * @return the matching varianti piantas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.VariantiPianta> findByVariantiPianta(
        long idPianta)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByVariantiPianta(idPianta);
    }

    /**
    * Returns a range of all the varianti piantas where idPianta = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VariantiPiantaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param idPianta the id pianta
    * @param start the lower bound of the range of varianti piantas
    * @param end the upper bound of the range of varianti piantas (not inclusive)
    * @return the range of matching varianti piantas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.VariantiPianta> findByVariantiPianta(
        long idPianta, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByVariantiPianta(idPianta, start, end);
    }

    /**
    * Returns an ordered range of all the varianti piantas where idPianta = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VariantiPiantaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param idPianta the id pianta
    * @param start the lower bound of the range of varianti piantas
    * @param end the upper bound of the range of varianti piantas (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching varianti piantas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.VariantiPianta> findByVariantiPianta(
        long idPianta, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVariantiPianta(idPianta, start, end, orderByComparator);
    }

    /**
    * Returns the first varianti pianta in the ordered set where idPianta = &#63;.
    *
    * @param idPianta the id pianta
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching varianti pianta
    * @throws it.bysoftware.ct.NoSuchVariantiPiantaException if a matching varianti pianta could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.VariantiPianta findByVariantiPianta_First(
        long idPianta,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVariantiPiantaException {
        return getPersistence()
                   .findByVariantiPianta_First(idPianta, orderByComparator);
    }

    /**
    * Returns the first varianti pianta in the ordered set where idPianta = &#63;.
    *
    * @param idPianta the id pianta
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching varianti pianta, or <code>null</code> if a matching varianti pianta could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.VariantiPianta fetchByVariantiPianta_First(
        long idPianta,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVariantiPianta_First(idPianta, orderByComparator);
    }

    /**
    * Returns the last varianti pianta in the ordered set where idPianta = &#63;.
    *
    * @param idPianta the id pianta
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching varianti pianta
    * @throws it.bysoftware.ct.NoSuchVariantiPiantaException if a matching varianti pianta could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.VariantiPianta findByVariantiPianta_Last(
        long idPianta,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVariantiPiantaException {
        return getPersistence()
                   .findByVariantiPianta_Last(idPianta, orderByComparator);
    }

    /**
    * Returns the last varianti pianta in the ordered set where idPianta = &#63;.
    *
    * @param idPianta the id pianta
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching varianti pianta, or <code>null</code> if a matching varianti pianta could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.VariantiPianta fetchByVariantiPianta_Last(
        long idPianta,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVariantiPianta_Last(idPianta, orderByComparator);
    }

    /**
    * Returns the varianti piantas before and after the current varianti pianta in the ordered set where idPianta = &#63;.
    *
    * @param id the primary key of the current varianti pianta
    * @param idPianta the id pianta
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next varianti pianta
    * @throws it.bysoftware.ct.NoSuchVariantiPiantaException if a varianti pianta with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.VariantiPianta[] findByVariantiPianta_PrevAndNext(
        long id, long idPianta,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVariantiPiantaException {
        return getPersistence()
                   .findByVariantiPianta_PrevAndNext(id, idPianta,
            orderByComparator);
    }

    /**
    * Removes all the varianti piantas where idPianta = &#63; from the database.
    *
    * @param idPianta the id pianta
    * @throws SystemException if a system exception occurred
    */
    public static void removeByVariantiPianta(long idPianta)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByVariantiPianta(idPianta);
    }

    /**
    * Returns the number of varianti piantas where idPianta = &#63;.
    *
    * @param idPianta the id pianta
    * @return the number of matching varianti piantas
    * @throws SystemException if a system exception occurred
    */
    public static int countByVariantiPianta(long idPianta)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByVariantiPianta(idPianta);
    }

    /**
    * Caches the varianti pianta in the entity cache if it is enabled.
    *
    * @param variantiPianta the varianti pianta
    */
    public static void cacheResult(
        it.bysoftware.ct.model.VariantiPianta variantiPianta) {
        getPersistence().cacheResult(variantiPianta);
    }

    /**
    * Caches the varianti piantas in the entity cache if it is enabled.
    *
    * @param variantiPiantas the varianti piantas
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.VariantiPianta> variantiPiantas) {
        getPersistence().cacheResult(variantiPiantas);
    }

    /**
    * Creates a new varianti pianta with the primary key. Does not add the varianti pianta to the database.
    *
    * @param id the primary key for the new varianti pianta
    * @return the new varianti pianta
    */
    public static it.bysoftware.ct.model.VariantiPianta create(long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the varianti pianta with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the varianti pianta
    * @return the varianti pianta that was removed
    * @throws it.bysoftware.ct.NoSuchVariantiPiantaException if a varianti pianta with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.VariantiPianta remove(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVariantiPiantaException {
        return getPersistence().remove(id);
    }

    public static it.bysoftware.ct.model.VariantiPianta updateImpl(
        it.bysoftware.ct.model.VariantiPianta variantiPianta)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(variantiPianta);
    }

    /**
    * Returns the varianti pianta with the primary key or throws a {@link it.bysoftware.ct.NoSuchVariantiPiantaException} if it could not be found.
    *
    * @param id the primary key of the varianti pianta
    * @return the varianti pianta
    * @throws it.bysoftware.ct.NoSuchVariantiPiantaException if a varianti pianta with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.VariantiPianta findByPrimaryKey(
        long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVariantiPiantaException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the varianti pianta with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the varianti pianta
    * @return the varianti pianta, or <code>null</code> if a varianti pianta with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.VariantiPianta fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the varianti piantas.
    *
    * @return the varianti piantas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.VariantiPianta> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the varianti piantas.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VariantiPiantaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of varianti piantas
    * @param end the upper bound of the range of varianti piantas (not inclusive)
    * @return the range of varianti piantas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.VariantiPianta> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the varianti piantas.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VariantiPiantaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of varianti piantas
    * @param end the upper bound of the range of varianti piantas (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of varianti piantas
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.VariantiPianta> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the varianti piantas from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of varianti piantas.
    *
    * @return the number of varianti piantas
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static VariantiPiantaPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (VariantiPiantaPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    VariantiPiantaPersistence.class.getName());

            ReferenceRegistry.registerReference(VariantiPiantaUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(VariantiPiantaPersistence persistence) {
    }
}
