package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class ContatoreSocioPK implements Comparable<ContatoreSocioPK>,
    Serializable {
    public int anno;
    public String codiceSocio;
    public String tipoDocumento;

    public ContatoreSocioPK() {
    }

    public ContatoreSocioPK(int anno, String codiceSocio, String tipoDocumento) {
        this.anno = anno;
        this.codiceSocio = codiceSocio;
        this.tipoDocumento = tipoDocumento;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    public String getCodiceSocio() {
        return codiceSocio;
    }

    public void setCodiceSocio(String codiceSocio) {
        this.codiceSocio = codiceSocio;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    @Override
    public int compareTo(ContatoreSocioPK pk) {
        if (pk == null) {
            return -1;
        }

        int value = 0;

        if (anno < pk.anno) {
            value = -1;
        } else if (anno > pk.anno) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        value = codiceSocio.compareTo(pk.codiceSocio);

        if (value != 0) {
            return value;
        }

        value = tipoDocumento.compareTo(pk.tipoDocumento);

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ContatoreSocioPK)) {
            return false;
        }

        ContatoreSocioPK pk = (ContatoreSocioPK) obj;

        if ((anno == pk.anno) && (codiceSocio.equals(pk.codiceSocio)) &&
                (tipoDocumento.equals(pk.tipoDocumento))) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (String.valueOf(anno) + String.valueOf(codiceSocio) +
        String.valueOf(tipoDocumento)).hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(15);

        sb.append(StringPool.OPEN_CURLY_BRACE);

        sb.append("anno");
        sb.append(StringPool.EQUAL);
        sb.append(anno);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("codiceSocio");
        sb.append(StringPool.EQUAL);
        sb.append(codiceSocio);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("tipoDocumento");
        sb.append(StringPool.EQUAL);
        sb.append(tipoDocumento);

        sb.append(StringPool.CLOSE_CURLY_BRACE);

        return sb.toString();
    }
}
