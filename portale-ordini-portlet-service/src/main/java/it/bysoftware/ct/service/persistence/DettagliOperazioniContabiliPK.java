package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class DettagliOperazioniContabiliPK implements Comparable<DettagliOperazioniContabiliPK>,
    Serializable {
    public String codiceOperazione;
    public int progressivoLegame;

    public DettagliOperazioniContabiliPK() {
    }

    public DettagliOperazioniContabiliPK(String codiceOperazione,
        int progressivoLegame) {
        this.codiceOperazione = codiceOperazione;
        this.progressivoLegame = progressivoLegame;
    }

    public String getCodiceOperazione() {
        return codiceOperazione;
    }

    public void setCodiceOperazione(String codiceOperazione) {
        this.codiceOperazione = codiceOperazione;
    }

    public int getProgressivoLegame() {
        return progressivoLegame;
    }

    public void setProgressivoLegame(int progressivoLegame) {
        this.progressivoLegame = progressivoLegame;
    }

    @Override
    public int compareTo(DettagliOperazioniContabiliPK pk) {
        if (pk == null) {
            return -1;
        }

        int value = 0;

        value = codiceOperazione.compareTo(pk.codiceOperazione);

        if (value != 0) {
            return value;
        }

        if (progressivoLegame < pk.progressivoLegame) {
            value = -1;
        } else if (progressivoLegame > pk.progressivoLegame) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof DettagliOperazioniContabiliPK)) {
            return false;
        }

        DettagliOperazioniContabiliPK pk = (DettagliOperazioniContabiliPK) obj;

        if ((codiceOperazione.equals(pk.codiceOperazione)) &&
                (progressivoLegame == pk.progressivoLegame)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (String.valueOf(codiceOperazione) +
        String.valueOf(progressivoLegame)).hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(10);

        sb.append(StringPool.OPEN_CURLY_BRACE);

        sb.append("codiceOperazione");
        sb.append(StringPool.EQUAL);
        sb.append(codiceOperazione);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("progressivoLegame");
        sb.append(StringPool.EQUAL);
        sb.append(progressivoLegame);

        sb.append(StringPool.CLOSE_CURLY_BRACE);

        return sb.toString();
    }
}
