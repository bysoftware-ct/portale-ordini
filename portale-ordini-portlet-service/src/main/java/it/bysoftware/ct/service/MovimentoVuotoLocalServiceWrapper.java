package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link MovimentoVuotoLocalService}.
 *
 * @author Mario Torrisi
 * @see MovimentoVuotoLocalService
 * @generated
 */
public class MovimentoVuotoLocalServiceWrapper
    implements MovimentoVuotoLocalService,
        ServiceWrapper<MovimentoVuotoLocalService> {
    private MovimentoVuotoLocalService _movimentoVuotoLocalService;

    public MovimentoVuotoLocalServiceWrapper(
        MovimentoVuotoLocalService movimentoVuotoLocalService) {
        _movimentoVuotoLocalService = movimentoVuotoLocalService;
    }

    /**
    * Adds the movimento vuoto to the database. Also notifies the appropriate model listeners.
    *
    * @param movimentoVuoto the movimento vuoto
    * @return the movimento vuoto that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.MovimentoVuoto addMovimentoVuoto(
        it.bysoftware.ct.model.MovimentoVuoto movimentoVuoto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _movimentoVuotoLocalService.addMovimentoVuoto(movimentoVuoto);
    }

    /**
    * Creates a new movimento vuoto with the primary key. Does not add the movimento vuoto to the database.
    *
    * @param idMovimento the primary key for the new movimento vuoto
    * @return the new movimento vuoto
    */
    @Override
    public it.bysoftware.ct.model.MovimentoVuoto createMovimentoVuoto(
        long idMovimento) {
        return _movimentoVuotoLocalService.createMovimentoVuoto(idMovimento);
    }

    /**
    * Deletes the movimento vuoto with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param idMovimento the primary key of the movimento vuoto
    * @return the movimento vuoto that was removed
    * @throws PortalException if a movimento vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.MovimentoVuoto deleteMovimentoVuoto(
        long idMovimento)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _movimentoVuotoLocalService.deleteMovimentoVuoto(idMovimento);
    }

    /**
    * Deletes the movimento vuoto from the database. Also notifies the appropriate model listeners.
    *
    * @param movimentoVuoto the movimento vuoto
    * @return the movimento vuoto that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.MovimentoVuoto deleteMovimentoVuoto(
        it.bysoftware.ct.model.MovimentoVuoto movimentoVuoto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _movimentoVuotoLocalService.deleteMovimentoVuoto(movimentoVuoto);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _movimentoVuotoLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _movimentoVuotoLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _movimentoVuotoLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _movimentoVuotoLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _movimentoVuotoLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _movimentoVuotoLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public it.bysoftware.ct.model.MovimentoVuoto fetchMovimentoVuoto(
        long idMovimento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _movimentoVuotoLocalService.fetchMovimentoVuoto(idMovimento);
    }

    /**
    * Returns the movimento vuoto with the primary key.
    *
    * @param idMovimento the primary key of the movimento vuoto
    * @return the movimento vuoto
    * @throws PortalException if a movimento vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.MovimentoVuoto getMovimentoVuoto(
        long idMovimento)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _movimentoVuotoLocalService.getMovimentoVuoto(idMovimento);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _movimentoVuotoLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the movimento vuotos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of movimento vuotos
    * @param end the upper bound of the range of movimento vuotos (not inclusive)
    * @return the range of movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> getMovimentoVuotos(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _movimentoVuotoLocalService.getMovimentoVuotos(start, end);
    }

    /**
    * Returns the number of movimento vuotos.
    *
    * @return the number of movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getMovimentoVuotosCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _movimentoVuotoLocalService.getMovimentoVuotosCount();
    }

    /**
    * Updates the movimento vuoto in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param movimentoVuoto the movimento vuoto
    * @return the movimento vuoto that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.MovimentoVuoto updateMovimentoVuoto(
        it.bysoftware.ct.model.MovimentoVuoto movimentoVuoto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _movimentoVuotoLocalService.updateMovimentoVuoto(movimentoVuoto);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _movimentoVuotoLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _movimentoVuotoLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _movimentoVuotoLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findMovementiByCodiceVuoto(
        java.lang.String c) {
        return _movimentoVuotoLocalService.findMovementiByCodiceVuoto(c);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findMovementiByCodiceSoggetto(
        java.lang.String c) {
        return _movimentoVuotoLocalService.findMovementiByCodiceSoggetto(c);
    }

    @Override
    public java.util.List<java.lang.Object[]> getPartnerReturnablesStock(
        java.lang.String partner, java.util.Date date, java.lang.String code) {
        return _movimentoVuotoLocalService.getPartnerReturnablesStock(partner,
            date, code);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findMovementiByVuotoSoggettoDataTipo(
        java.lang.String itemCode, java.lang.String parternCode,
        java.util.Date date, int type) {
        return _movimentoVuotoLocalService.findMovementiByVuotoSoggettoDataTipo(itemCode,
            parternCode, date, type);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findMovementiByCodiceSoggetto(
        java.lang.String c, java.util.Date from, java.util.Date to,
        java.lang.String code, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator comp) {
        return _movimentoVuotoLocalService.findMovementiByCodiceSoggetto(c,
            from, to, code, start, end, comp);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findMovementiByCodiceSoggetto(
        java.lang.String c, int type, java.util.Date from, java.util.Date to,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator comp) {
        return _movimentoVuotoLocalService.findMovementiByCodiceSoggetto(c,
            type, from, to, start, end, comp);
    }

    @Override
    public int getNumber(java.lang.String code, java.util.Date date) {
        return _movimentoVuotoLocalService.getNumber(code, date);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public MovimentoVuotoLocalService getWrappedMovimentoVuotoLocalService() {
        return _movimentoVuotoLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedMovimentoVuotoLocalService(
        MovimentoVuotoLocalService movimentoVuotoLocalService) {
        _movimentoVuotoLocalService = movimentoVuotoLocalService;
    }

    @Override
    public MovimentoVuotoLocalService getWrappedService() {
        return _movimentoVuotoLocalService;
    }

    @Override
    public void setWrappedService(
        MovimentoVuotoLocalService movimentoVuotoLocalService) {
        _movimentoVuotoLocalService = movimentoVuotoLocalService;
    }
}
