package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.ScadenzePartite;

/**
 * The persistence interface for the scadenze partite service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see ScadenzePartitePersistenceImpl
 * @see ScadenzePartiteUtil
 * @generated
 */
public interface ScadenzePartitePersistence extends BasePersistence<ScadenzePartite> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link ScadenzePartiteUtil} to access the scadenze partite persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the scadenze partite in the entity cache if it is enabled.
    *
    * @param scadenzePartite the scadenze partite
    */
    public void cacheResult(
        it.bysoftware.ct.model.ScadenzePartite scadenzePartite);

    /**
    * Caches the scadenze partites in the entity cache if it is enabled.
    *
    * @param scadenzePartites the scadenze partites
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.ScadenzePartite> scadenzePartites);

    /**
    * Creates a new scadenze partite with the primary key. Does not add the scadenze partite to the database.
    *
    * @param scadenzePartitePK the primary key for the new scadenze partite
    * @return the new scadenze partite
    */
    public it.bysoftware.ct.model.ScadenzePartite create(
        it.bysoftware.ct.service.persistence.ScadenzePartitePK scadenzePartitePK);

    /**
    * Removes the scadenze partite with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param scadenzePartitePK the primary key of the scadenze partite
    * @return the scadenze partite that was removed
    * @throws it.bysoftware.ct.NoSuchScadenzePartiteException if a scadenze partite with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ScadenzePartite remove(
        it.bysoftware.ct.service.persistence.ScadenzePartitePK scadenzePartitePK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchScadenzePartiteException;

    public it.bysoftware.ct.model.ScadenzePartite updateImpl(
        it.bysoftware.ct.model.ScadenzePartite scadenzePartite)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the scadenze partite with the primary key or throws a {@link it.bysoftware.ct.NoSuchScadenzePartiteException} if it could not be found.
    *
    * @param scadenzePartitePK the primary key of the scadenze partite
    * @return the scadenze partite
    * @throws it.bysoftware.ct.NoSuchScadenzePartiteException if a scadenze partite with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ScadenzePartite findByPrimaryKey(
        it.bysoftware.ct.service.persistence.ScadenzePartitePK scadenzePartitePK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchScadenzePartiteException;

    /**
    * Returns the scadenze partite with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param scadenzePartitePK the primary key of the scadenze partite
    * @return the scadenze partite, or <code>null</code> if a scadenze partite with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ScadenzePartite fetchByPrimaryKey(
        it.bysoftware.ct.service.persistence.ScadenzePartitePK scadenzePartitePK)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the scadenze partites.
    *
    * @return the scadenze partites
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.ScadenzePartite> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the scadenze partites.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ScadenzePartiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of scadenze partites
    * @param end the upper bound of the range of scadenze partites (not inclusive)
    * @return the range of scadenze partites
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.ScadenzePartite> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the scadenze partites.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ScadenzePartiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of scadenze partites
    * @param end the upper bound of the range of scadenze partites (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of scadenze partites
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.ScadenzePartite> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the scadenze partites from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of scadenze partites.
    *
    * @return the number of scadenze partites
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
