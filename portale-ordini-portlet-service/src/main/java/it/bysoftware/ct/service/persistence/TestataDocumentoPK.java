package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class TestataDocumentoPK implements Comparable<TestataDocumentoPK>,
    Serializable {
    public int anno;
    public String codiceAttivita;
    public String codiceCentro;
    public String codiceDeposito;
    public int protocollo;
    public String codiceFornitore;
    public String tipoDocumento;

    public TestataDocumentoPK() {
    }

    public TestataDocumentoPK(int anno, String codiceAttivita,
        String codiceCentro, String codiceDeposito, int protocollo,
        String codiceFornitore, String tipoDocumento) {
        this.anno = anno;
        this.codiceAttivita = codiceAttivita;
        this.codiceCentro = codiceCentro;
        this.codiceDeposito = codiceDeposito;
        this.protocollo = protocollo;
        this.codiceFornitore = codiceFornitore;
        this.tipoDocumento = tipoDocumento;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    public String getCodiceAttivita() {
        return codiceAttivita;
    }

    public void setCodiceAttivita(String codiceAttivita) {
        this.codiceAttivita = codiceAttivita;
    }

    public String getCodiceCentro() {
        return codiceCentro;
    }

    public void setCodiceCentro(String codiceCentro) {
        this.codiceCentro = codiceCentro;
    }

    public String getCodiceDeposito() {
        return codiceDeposito;
    }

    public void setCodiceDeposito(String codiceDeposito) {
        this.codiceDeposito = codiceDeposito;
    }

    public int getProtocollo() {
        return protocollo;
    }

    public void setProtocollo(int protocollo) {
        this.protocollo = protocollo;
    }

    public String getCodiceFornitore() {
        return codiceFornitore;
    }

    public void setCodiceFornitore(String codiceFornitore) {
        this.codiceFornitore = codiceFornitore;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    @Override
    public int compareTo(TestataDocumentoPK pk) {
        if (pk == null) {
            return -1;
        }

        int value = 0;

        if (anno < pk.anno) {
            value = -1;
        } else if (anno > pk.anno) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        value = codiceAttivita.compareTo(pk.codiceAttivita);

        if (value != 0) {
            return value;
        }

        value = codiceCentro.compareTo(pk.codiceCentro);

        if (value != 0) {
            return value;
        }

        value = codiceDeposito.compareTo(pk.codiceDeposito);

        if (value != 0) {
            return value;
        }

        if (protocollo < pk.protocollo) {
            value = -1;
        } else if (protocollo > pk.protocollo) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        value = codiceFornitore.compareTo(pk.codiceFornitore);

        if (value != 0) {
            return value;
        }

        value = tipoDocumento.compareTo(pk.tipoDocumento);

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof TestataDocumentoPK)) {
            return false;
        }

        TestataDocumentoPK pk = (TestataDocumentoPK) obj;

        if ((anno == pk.anno) && (codiceAttivita.equals(pk.codiceAttivita)) &&
                (codiceCentro.equals(pk.codiceCentro)) &&
                (codiceDeposito.equals(pk.codiceDeposito)) &&
                (protocollo == pk.protocollo) &&
                (codiceFornitore.equals(pk.codiceFornitore)) &&
                (tipoDocumento.equals(pk.tipoDocumento))) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (String.valueOf(anno) + String.valueOf(codiceAttivita) +
        String.valueOf(codiceCentro) + String.valueOf(codiceDeposito) +
        String.valueOf(protocollo) + String.valueOf(codiceFornitore) +
        String.valueOf(tipoDocumento)).hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(35);

        sb.append(StringPool.OPEN_CURLY_BRACE);

        sb.append("anno");
        sb.append(StringPool.EQUAL);
        sb.append(anno);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("codiceAttivita");
        sb.append(StringPool.EQUAL);
        sb.append(codiceAttivita);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("codiceCentro");
        sb.append(StringPool.EQUAL);
        sb.append(codiceCentro);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("codiceDeposito");
        sb.append(StringPool.EQUAL);
        sb.append(codiceDeposito);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("protocollo");
        sb.append(StringPool.EQUAL);
        sb.append(protocollo);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("codiceFornitore");
        sb.append(StringPool.EQUAL);
        sb.append(codiceFornitore);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("tipoDocumento");
        sb.append(StringPool.EQUAL);
        sb.append(tipoDocumento);

        sb.append(StringPool.CLOSE_CURLY_BRACE);

        return sb.toString();
    }
}
