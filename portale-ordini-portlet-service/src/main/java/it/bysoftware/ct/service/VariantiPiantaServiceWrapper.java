package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VariantiPiantaService}.
 *
 * @author Mario Torrisi
 * @see VariantiPiantaService
 * @generated
 */
public class VariantiPiantaServiceWrapper implements VariantiPiantaService,
    ServiceWrapper<VariantiPiantaService> {
    private VariantiPiantaService _variantiPiantaService;

    public VariantiPiantaServiceWrapper(
        VariantiPiantaService variantiPiantaService) {
        _variantiPiantaService = variantiPiantaService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _variantiPiantaService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _variantiPiantaService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _variantiPiantaService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public VariantiPiantaService getWrappedVariantiPiantaService() {
        return _variantiPiantaService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedVariantiPiantaService(
        VariantiPiantaService variantiPiantaService) {
        _variantiPiantaService = variantiPiantaService;
    }

    @Override
    public VariantiPiantaService getWrappedService() {
        return _variantiPiantaService;
    }

    @Override
    public void setWrappedService(VariantiPiantaService variantiPiantaService) {
        _variantiPiantaService = variantiPiantaService;
    }
}
