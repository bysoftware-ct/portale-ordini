package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.TipoCarrello;

import java.util.List;

/**
 * The persistence utility for the tipo carrello service. This utility wraps {@link TipoCarrelloPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see TipoCarrelloPersistence
 * @see TipoCarrelloPersistenceImpl
 * @generated
 */
public class TipoCarrelloUtil {
    private static TipoCarrelloPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(TipoCarrello tipoCarrello) {
        getPersistence().clearCache(tipoCarrello);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<TipoCarrello> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<TipoCarrello> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<TipoCarrello> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static TipoCarrello update(TipoCarrello tipoCarrello)
        throws SystemException {
        return getPersistence().update(tipoCarrello);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static TipoCarrello update(TipoCarrello tipoCarrello,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(tipoCarrello, serviceContext);
    }

    /**
    * Caches the tipo carrello in the entity cache if it is enabled.
    *
    * @param tipoCarrello the tipo carrello
    */
    public static void cacheResult(
        it.bysoftware.ct.model.TipoCarrello tipoCarrello) {
        getPersistence().cacheResult(tipoCarrello);
    }

    /**
    * Caches the tipo carrellos in the entity cache if it is enabled.
    *
    * @param tipoCarrellos the tipo carrellos
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.TipoCarrello> tipoCarrellos) {
        getPersistence().cacheResult(tipoCarrellos);
    }

    /**
    * Creates a new tipo carrello with the primary key. Does not add the tipo carrello to the database.
    *
    * @param id the primary key for the new tipo carrello
    * @return the new tipo carrello
    */
    public static it.bysoftware.ct.model.TipoCarrello create(long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the tipo carrello with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the tipo carrello
    * @return the tipo carrello that was removed
    * @throws it.bysoftware.ct.NoSuchTipoCarrelloException if a tipo carrello with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TipoCarrello remove(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTipoCarrelloException {
        return getPersistence().remove(id);
    }

    public static it.bysoftware.ct.model.TipoCarrello updateImpl(
        it.bysoftware.ct.model.TipoCarrello tipoCarrello)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(tipoCarrello);
    }

    /**
    * Returns the tipo carrello with the primary key or throws a {@link it.bysoftware.ct.NoSuchTipoCarrelloException} if it could not be found.
    *
    * @param id the primary key of the tipo carrello
    * @return the tipo carrello
    * @throws it.bysoftware.ct.NoSuchTipoCarrelloException if a tipo carrello with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TipoCarrello findByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTipoCarrelloException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the tipo carrello with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the tipo carrello
    * @return the tipo carrello, or <code>null</code> if a tipo carrello with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TipoCarrello fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the tipo carrellos.
    *
    * @return the tipo carrellos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TipoCarrello> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the tipo carrellos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TipoCarrelloModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of tipo carrellos
    * @param end the upper bound of the range of tipo carrellos (not inclusive)
    * @return the range of tipo carrellos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TipoCarrello> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the tipo carrellos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TipoCarrelloModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of tipo carrellos
    * @param end the upper bound of the range of tipo carrellos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of tipo carrellos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TipoCarrello> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the tipo carrellos from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of tipo carrellos.
    *
    * @return the number of tipo carrellos
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static TipoCarrelloPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (TipoCarrelloPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    TipoCarrelloPersistence.class.getName());

            ReferenceRegistry.registerReference(TipoCarrelloUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(TipoCarrelloPersistence persistence) {
    }
}
