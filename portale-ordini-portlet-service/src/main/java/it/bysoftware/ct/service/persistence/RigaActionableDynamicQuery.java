package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.Riga;
import it.bysoftware.ct.service.RigaLocalServiceUtil;

/**
 * @author Mario Torrisi
 * @generated
 */
public abstract class RigaActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public RigaActionableDynamicQuery() throws SystemException {
        setBaseLocalService(RigaLocalServiceUtil.getService());
        setClass(Riga.class);

        setClassLoader(it.bysoftware.ct.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
