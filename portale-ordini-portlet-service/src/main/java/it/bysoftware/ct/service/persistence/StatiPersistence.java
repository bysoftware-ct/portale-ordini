package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.Stati;

/**
 * The persistence interface for the stati service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see StatiPersistenceImpl
 * @see StatiUtil
 * @generated
 */
public interface StatiPersistence extends BasePersistence<Stati> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link StatiUtil} to access the stati persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the stati in the entity cache if it is enabled.
    *
    * @param stati the stati
    */
    public void cacheResult(it.bysoftware.ct.model.Stati stati);

    /**
    * Caches the statis in the entity cache if it is enabled.
    *
    * @param statis the statis
    */
    public void cacheResult(java.util.List<it.bysoftware.ct.model.Stati> statis);

    /**
    * Creates a new stati with the primary key. Does not add the stati to the database.
    *
    * @param codiceStato the primary key for the new stati
    * @return the new stati
    */
    public it.bysoftware.ct.model.Stati create(java.lang.String codiceStato);

    /**
    * Removes the stati with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceStato the primary key of the stati
    * @return the stati that was removed
    * @throws it.bysoftware.ct.NoSuchStatiException if a stati with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Stati remove(java.lang.String codiceStato)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchStatiException;

    public it.bysoftware.ct.model.Stati updateImpl(
        it.bysoftware.ct.model.Stati stati)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the stati with the primary key or throws a {@link it.bysoftware.ct.NoSuchStatiException} if it could not be found.
    *
    * @param codiceStato the primary key of the stati
    * @return the stati
    * @throws it.bysoftware.ct.NoSuchStatiException if a stati with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Stati findByPrimaryKey(
        java.lang.String codiceStato)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchStatiException;

    /**
    * Returns the stati with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param codiceStato the primary key of the stati
    * @return the stati, or <code>null</code> if a stati with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Stati fetchByPrimaryKey(
        java.lang.String codiceStato)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the statis.
    *
    * @return the statis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Stati> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the statis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.StatiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of statis
    * @param end the upper bound of the range of statis (not inclusive)
    * @return the range of statis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Stati> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the statis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.StatiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of statis
    * @param end the upper bound of the range of statis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of statis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Stati> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the statis from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of statis.
    *
    * @return the number of statis
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
