package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.ScadenzePartite;
import it.bysoftware.ct.service.ScadenzePartiteLocalServiceUtil;

/**
 * @author Mario Torrisi
 * @generated
 */
public abstract class ScadenzePartiteActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public ScadenzePartiteActionableDynamicQuery() throws SystemException {
        setBaseLocalService(ScadenzePartiteLocalServiceUtil.getService());
        setClass(ScadenzePartite.class);

        setClassLoader(it.bysoftware.ct.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("primaryKey.tipoSoggetto");
    }
}
