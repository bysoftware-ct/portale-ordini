package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.ArticoloSocio;

import java.util.List;

/**
 * The persistence utility for the articolo socio service. This utility wraps {@link ArticoloSocioPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see ArticoloSocioPersistence
 * @see ArticoloSocioPersistenceImpl
 * @generated
 */
public class ArticoloSocioUtil {
    private static ArticoloSocioPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(ArticoloSocio articoloSocio) {
        getPersistence().clearCache(articoloSocio);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<ArticoloSocio> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<ArticoloSocio> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<ArticoloSocio> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static ArticoloSocio update(ArticoloSocio articoloSocio)
        throws SystemException {
        return getPersistence().update(articoloSocio);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static ArticoloSocio update(ArticoloSocio articoloSocio,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(articoloSocio, serviceContext);
    }

    /**
    * Returns all the articolo socios where associato = &#63; and codiceArticolo = &#63;.
    *
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @return the matching articolo socios
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ArticoloSocio> findBySocioArticolo(
        java.lang.String associato, java.lang.String codiceArticolo)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBySocioArticolo(associato, codiceArticolo);
    }

    /**
    * Returns a range of all the articolo socios where associato = &#63; and codiceArticolo = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @param start the lower bound of the range of articolo socios
    * @param end the upper bound of the range of articolo socios (not inclusive)
    * @return the range of matching articolo socios
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ArticoloSocio> findBySocioArticolo(
        java.lang.String associato, java.lang.String codiceArticolo, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBySocioArticolo(associato, codiceArticolo, start, end);
    }

    /**
    * Returns an ordered range of all the articolo socios where associato = &#63; and codiceArticolo = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @param start the lower bound of the range of articolo socios
    * @param end the upper bound of the range of articolo socios (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching articolo socios
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ArticoloSocio> findBySocioArticolo(
        java.lang.String associato, java.lang.String codiceArticolo, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBySocioArticolo(associato, codiceArticolo, start, end,
            orderByComparator);
    }

    /**
    * Returns the first articolo socio in the ordered set where associato = &#63; and codiceArticolo = &#63;.
    *
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching articolo socio
    * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio findBySocioArticolo_First(
        java.lang.String associato, java.lang.String codiceArticolo,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException {
        return getPersistence()
                   .findBySocioArticolo_First(associato, codiceArticolo,
            orderByComparator);
    }

    /**
    * Returns the first articolo socio in the ordered set where associato = &#63; and codiceArticolo = &#63;.
    *
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching articolo socio, or <code>null</code> if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio fetchBySocioArticolo_First(
        java.lang.String associato, java.lang.String codiceArticolo,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchBySocioArticolo_First(associato, codiceArticolo,
            orderByComparator);
    }

    /**
    * Returns the last articolo socio in the ordered set where associato = &#63; and codiceArticolo = &#63;.
    *
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching articolo socio
    * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio findBySocioArticolo_Last(
        java.lang.String associato, java.lang.String codiceArticolo,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException {
        return getPersistence()
                   .findBySocioArticolo_Last(associato, codiceArticolo,
            orderByComparator);
    }

    /**
    * Returns the last articolo socio in the ordered set where associato = &#63; and codiceArticolo = &#63;.
    *
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching articolo socio, or <code>null</code> if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio fetchBySocioArticolo_Last(
        java.lang.String associato, java.lang.String codiceArticolo,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchBySocioArticolo_Last(associato, codiceArticolo,
            orderByComparator);
    }

    /**
    * Returns the articolo socios before and after the current articolo socio in the ordered set where associato = &#63; and codiceArticolo = &#63;.
    *
    * @param articoloSocioPK the primary key of the current articolo socio
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next articolo socio
    * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a articolo socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio[] findBySocioArticolo_PrevAndNext(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK,
        java.lang.String associato, java.lang.String codiceArticolo,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException {
        return getPersistence()
                   .findBySocioArticolo_PrevAndNext(articoloSocioPK, associato,
            codiceArticolo, orderByComparator);
    }

    /**
    * Removes all the articolo socios where associato = &#63; and codiceArticolo = &#63; from the database.
    *
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @throws SystemException if a system exception occurred
    */
    public static void removeBySocioArticolo(java.lang.String associato,
        java.lang.String codiceArticolo)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeBySocioArticolo(associato, codiceArticolo);
    }

    /**
    * Returns the number of articolo socios where associato = &#63; and codiceArticolo = &#63;.
    *
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @return the number of matching articolo socios
    * @throws SystemException if a system exception occurred
    */
    public static int countBySocioArticolo(java.lang.String associato,
        java.lang.String codiceArticolo)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countBySocioArticolo(associato, codiceArticolo);
    }

    /**
    * Returns all the articolo socios where associato = &#63;.
    *
    * @param associato the associato
    * @return the matching articolo socios
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ArticoloSocio> findBySocio(
        java.lang.String associato)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBySocio(associato);
    }

    /**
    * Returns a range of all the articolo socios where associato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param associato the associato
    * @param start the lower bound of the range of articolo socios
    * @param end the upper bound of the range of articolo socios (not inclusive)
    * @return the range of matching articolo socios
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ArticoloSocio> findBySocio(
        java.lang.String associato, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBySocio(associato, start, end);
    }

    /**
    * Returns an ordered range of all the articolo socios where associato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param associato the associato
    * @param start the lower bound of the range of articolo socios
    * @param end the upper bound of the range of articolo socios (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching articolo socios
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ArticoloSocio> findBySocio(
        java.lang.String associato, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBySocio(associato, start, end, orderByComparator);
    }

    /**
    * Returns the first articolo socio in the ordered set where associato = &#63;.
    *
    * @param associato the associato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching articolo socio
    * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio findBySocio_First(
        java.lang.String associato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException {
        return getPersistence().findBySocio_First(associato, orderByComparator);
    }

    /**
    * Returns the first articolo socio in the ordered set where associato = &#63;.
    *
    * @param associato the associato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching articolo socio, or <code>null</code> if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio fetchBySocio_First(
        java.lang.String associato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchBySocio_First(associato, orderByComparator);
    }

    /**
    * Returns the last articolo socio in the ordered set where associato = &#63;.
    *
    * @param associato the associato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching articolo socio
    * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio findBySocio_Last(
        java.lang.String associato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException {
        return getPersistence().findBySocio_Last(associato, orderByComparator);
    }

    /**
    * Returns the last articolo socio in the ordered set where associato = &#63;.
    *
    * @param associato the associato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching articolo socio, or <code>null</code> if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio fetchBySocio_Last(
        java.lang.String associato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchBySocio_Last(associato, orderByComparator);
    }

    /**
    * Returns the articolo socios before and after the current articolo socio in the ordered set where associato = &#63;.
    *
    * @param articoloSocioPK the primary key of the current articolo socio
    * @param associato the associato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next articolo socio
    * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a articolo socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio[] findBySocio_PrevAndNext(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK,
        java.lang.String associato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException {
        return getPersistence()
                   .findBySocio_PrevAndNext(articoloSocioPK, associato,
            orderByComparator);
    }

    /**
    * Removes all the articolo socios where associato = &#63; from the database.
    *
    * @param associato the associato
    * @throws SystemException if a system exception occurred
    */
    public static void removeBySocio(java.lang.String associato)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeBySocio(associato);
    }

    /**
    * Returns the number of articolo socios where associato = &#63;.
    *
    * @param associato the associato
    * @return the number of matching articolo socios
    * @throws SystemException if a system exception occurred
    */
    public static int countBySocio(java.lang.String associato)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countBySocio(associato);
    }

    /**
    * Returns the articolo socio where associato = &#63; and codiceArticoloSocio = &#63; and codiceVarianteSocio = &#63; or throws a {@link it.bysoftware.ct.NoSuchArticoloSocioException} if it could not be found.
    *
    * @param associato the associato
    * @param codiceArticoloSocio the codice articolo socio
    * @param codiceVarianteSocio the codice variante socio
    * @return the matching articolo socio
    * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio findByArticoloVarianteSocio(
        java.lang.String associato, java.lang.String codiceArticoloSocio,
        java.lang.String codiceVarianteSocio)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException {
        return getPersistence()
                   .findByArticoloVarianteSocio(associato, codiceArticoloSocio,
            codiceVarianteSocio);
    }

    /**
    * Returns the articolo socio where associato = &#63; and codiceArticoloSocio = &#63; and codiceVarianteSocio = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param associato the associato
    * @param codiceArticoloSocio the codice articolo socio
    * @param codiceVarianteSocio the codice variante socio
    * @return the matching articolo socio, or <code>null</code> if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio fetchByArticoloVarianteSocio(
        java.lang.String associato, java.lang.String codiceArticoloSocio,
        java.lang.String codiceVarianteSocio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByArticoloVarianteSocio(associato,
            codiceArticoloSocio, codiceVarianteSocio);
    }

    /**
    * Returns the articolo socio where associato = &#63; and codiceArticoloSocio = &#63; and codiceVarianteSocio = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param associato the associato
    * @param codiceArticoloSocio the codice articolo socio
    * @param codiceVarianteSocio the codice variante socio
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching articolo socio, or <code>null</code> if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio fetchByArticoloVarianteSocio(
        java.lang.String associato, java.lang.String codiceArticoloSocio,
        java.lang.String codiceVarianteSocio, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByArticoloVarianteSocio(associato,
            codiceArticoloSocio, codiceVarianteSocio, retrieveFromCache);
    }

    /**
    * Removes the articolo socio where associato = &#63; and codiceArticoloSocio = &#63; and codiceVarianteSocio = &#63; from the database.
    *
    * @param associato the associato
    * @param codiceArticoloSocio the codice articolo socio
    * @param codiceVarianteSocio the codice variante socio
    * @return the articolo socio that was removed
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio removeByArticoloVarianteSocio(
        java.lang.String associato, java.lang.String codiceArticoloSocio,
        java.lang.String codiceVarianteSocio)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException {
        return getPersistence()
                   .removeByArticoloVarianteSocio(associato,
            codiceArticoloSocio, codiceVarianteSocio);
    }

    /**
    * Returns the number of articolo socios where associato = &#63; and codiceArticoloSocio = &#63; and codiceVarianteSocio = &#63;.
    *
    * @param associato the associato
    * @param codiceArticoloSocio the codice articolo socio
    * @param codiceVarianteSocio the codice variante socio
    * @return the number of matching articolo socios
    * @throws SystemException if a system exception occurred
    */
    public static int countByArticoloVarianteSocio(java.lang.String associato,
        java.lang.String codiceArticoloSocio,
        java.lang.String codiceVarianteSocio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByArticoloVarianteSocio(associato,
            codiceArticoloSocio, codiceVarianteSocio);
    }

    /**
    * Caches the articolo socio in the entity cache if it is enabled.
    *
    * @param articoloSocio the articolo socio
    */
    public static void cacheResult(
        it.bysoftware.ct.model.ArticoloSocio articoloSocio) {
        getPersistence().cacheResult(articoloSocio);
    }

    /**
    * Caches the articolo socios in the entity cache if it is enabled.
    *
    * @param articoloSocios the articolo socios
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.ArticoloSocio> articoloSocios) {
        getPersistence().cacheResult(articoloSocios);
    }

    /**
    * Creates a new articolo socio with the primary key. Does not add the articolo socio to the database.
    *
    * @param articoloSocioPK the primary key for the new articolo socio
    * @return the new articolo socio
    */
    public static it.bysoftware.ct.model.ArticoloSocio create(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK) {
        return getPersistence().create(articoloSocioPK);
    }

    /**
    * Removes the articolo socio with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param articoloSocioPK the primary key of the articolo socio
    * @return the articolo socio that was removed
    * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a articolo socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio remove(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException {
        return getPersistence().remove(articoloSocioPK);
    }

    public static it.bysoftware.ct.model.ArticoloSocio updateImpl(
        it.bysoftware.ct.model.ArticoloSocio articoloSocio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(articoloSocio);
    }

    /**
    * Returns the articolo socio with the primary key or throws a {@link it.bysoftware.ct.NoSuchArticoloSocioException} if it could not be found.
    *
    * @param articoloSocioPK the primary key of the articolo socio
    * @return the articolo socio
    * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a articolo socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio findByPrimaryKey(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException {
        return getPersistence().findByPrimaryKey(articoloSocioPK);
    }

    /**
    * Returns the articolo socio with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param articoloSocioPK the primary key of the articolo socio
    * @return the articolo socio, or <code>null</code> if a articolo socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio fetchByPrimaryKey(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(articoloSocioPK);
    }

    /**
    * Returns all the articolo socios.
    *
    * @return the articolo socios
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ArticoloSocio> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the articolo socios.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of articolo socios
    * @param end the upper bound of the range of articolo socios (not inclusive)
    * @return the range of articolo socios
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ArticoloSocio> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the articolo socios.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of articolo socios
    * @param end the upper bound of the range of articolo socios (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of articolo socios
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ArticoloSocio> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the articolo socios from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of articolo socios.
    *
    * @return the number of articolo socios
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static ArticoloSocioPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (ArticoloSocioPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    ArticoloSocioPersistence.class.getName());

            ReferenceRegistry.registerReference(ArticoloSocioUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(ArticoloSocioPersistence persistence) {
    }
}
