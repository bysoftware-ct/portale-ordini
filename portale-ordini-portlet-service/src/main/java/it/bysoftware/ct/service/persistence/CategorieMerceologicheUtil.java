package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.CategorieMerceologiche;

import java.util.List;

/**
 * The persistence utility for the categorie merceologiche service. This utility wraps {@link CategorieMerceologichePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CategorieMerceologichePersistence
 * @see CategorieMerceologichePersistenceImpl
 * @generated
 */
public class CategorieMerceologicheUtil {
    private static CategorieMerceologichePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(CategorieMerceologiche categorieMerceologiche) {
        getPersistence().clearCache(categorieMerceologiche);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<CategorieMerceologiche> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<CategorieMerceologiche> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<CategorieMerceologiche> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static CategorieMerceologiche update(
        CategorieMerceologiche categorieMerceologiche)
        throws SystemException {
        return getPersistence().update(categorieMerceologiche);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static CategorieMerceologiche update(
        CategorieMerceologiche categorieMerceologiche,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(categorieMerceologiche, serviceContext);
    }

    /**
    * Caches the categorie merceologiche in the entity cache if it is enabled.
    *
    * @param categorieMerceologiche the categorie merceologiche
    */
    public static void cacheResult(
        it.bysoftware.ct.model.CategorieMerceologiche categorieMerceologiche) {
        getPersistence().cacheResult(categorieMerceologiche);
    }

    /**
    * Caches the categorie merceologiches in the entity cache if it is enabled.
    *
    * @param categorieMerceologiches the categorie merceologiches
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.CategorieMerceologiche> categorieMerceologiches) {
        getPersistence().cacheResult(categorieMerceologiches);
    }

    /**
    * Creates a new categorie merceologiche with the primary key. Does not add the categorie merceologiche to the database.
    *
    * @param codiceCategoria the primary key for the new categorie merceologiche
    * @return the new categorie merceologiche
    */
    public static it.bysoftware.ct.model.CategorieMerceologiche create(
        java.lang.String codiceCategoria) {
        return getPersistence().create(codiceCategoria);
    }

    /**
    * Removes the categorie merceologiche with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceCategoria the primary key of the categorie merceologiche
    * @return the categorie merceologiche that was removed
    * @throws it.bysoftware.ct.NoSuchCategorieMerceologicheException if a categorie merceologiche with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.CategorieMerceologiche remove(
        java.lang.String codiceCategoria)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCategorieMerceologicheException {
        return getPersistence().remove(codiceCategoria);
    }

    public static it.bysoftware.ct.model.CategorieMerceologiche updateImpl(
        it.bysoftware.ct.model.CategorieMerceologiche categorieMerceologiche)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(categorieMerceologiche);
    }

    /**
    * Returns the categorie merceologiche with the primary key or throws a {@link it.bysoftware.ct.NoSuchCategorieMerceologicheException} if it could not be found.
    *
    * @param codiceCategoria the primary key of the categorie merceologiche
    * @return the categorie merceologiche
    * @throws it.bysoftware.ct.NoSuchCategorieMerceologicheException if a categorie merceologiche with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.CategorieMerceologiche findByPrimaryKey(
        java.lang.String codiceCategoria)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCategorieMerceologicheException {
        return getPersistence().findByPrimaryKey(codiceCategoria);
    }

    /**
    * Returns the categorie merceologiche with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param codiceCategoria the primary key of the categorie merceologiche
    * @return the categorie merceologiche, or <code>null</code> if a categorie merceologiche with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.CategorieMerceologiche fetchByPrimaryKey(
        java.lang.String codiceCategoria)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(codiceCategoria);
    }

    /**
    * Returns all the categorie merceologiches.
    *
    * @return the categorie merceologiches
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.CategorieMerceologiche> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the categorie merceologiches.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CategorieMerceologicheModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of categorie merceologiches
    * @param end the upper bound of the range of categorie merceologiches (not inclusive)
    * @return the range of categorie merceologiches
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.CategorieMerceologiche> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the categorie merceologiches.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CategorieMerceologicheModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of categorie merceologiches
    * @param end the upper bound of the range of categorie merceologiches (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of categorie merceologiches
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.CategorieMerceologiche> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the categorie merceologiches from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of categorie merceologiches.
    *
    * @return the number of categorie merceologiches
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static CategorieMerceologichePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (CategorieMerceologichePersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    CategorieMerceologichePersistence.class.getName());

            ReferenceRegistry.registerReference(CategorieMerceologicheUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(CategorieMerceologichePersistence persistence) {
    }
}
