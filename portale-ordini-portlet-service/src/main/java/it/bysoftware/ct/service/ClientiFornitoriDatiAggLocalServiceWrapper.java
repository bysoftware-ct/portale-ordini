package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ClientiFornitoriDatiAggLocalService}.
 *
 * @author Mario Torrisi
 * @see ClientiFornitoriDatiAggLocalService
 * @generated
 */
public class ClientiFornitoriDatiAggLocalServiceWrapper
    implements ClientiFornitoriDatiAggLocalService,
        ServiceWrapper<ClientiFornitoriDatiAggLocalService> {
    private ClientiFornitoriDatiAggLocalService _clientiFornitoriDatiAggLocalService;

    public ClientiFornitoriDatiAggLocalServiceWrapper(
        ClientiFornitoriDatiAggLocalService clientiFornitoriDatiAggLocalService) {
        _clientiFornitoriDatiAggLocalService = clientiFornitoriDatiAggLocalService;
    }

    /**
    * Adds the clienti fornitori dati agg to the database. Also notifies the appropriate model listeners.
    *
    * @param clientiFornitoriDatiAgg the clienti fornitori dati agg
    * @return the clienti fornitori dati agg that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ClientiFornitoriDatiAgg addClientiFornitoriDatiAgg(
        it.bysoftware.ct.model.ClientiFornitoriDatiAgg clientiFornitoriDatiAgg)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _clientiFornitoriDatiAggLocalService.addClientiFornitoriDatiAgg(clientiFornitoriDatiAgg);
    }

    /**
    * Creates a new clienti fornitori dati agg with the primary key. Does not add the clienti fornitori dati agg to the database.
    *
    * @param clientiFornitoriDatiAggPK the primary key for the new clienti fornitori dati agg
    * @return the new clienti fornitori dati agg
    */
    @Override
    public it.bysoftware.ct.model.ClientiFornitoriDatiAgg createClientiFornitoriDatiAgg(
        it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK) {
        return _clientiFornitoriDatiAggLocalService.createClientiFornitoriDatiAgg(clientiFornitoriDatiAggPK);
    }

    /**
    * Deletes the clienti fornitori dati agg with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param clientiFornitoriDatiAggPK the primary key of the clienti fornitori dati agg
    * @return the clienti fornitori dati agg that was removed
    * @throws PortalException if a clienti fornitori dati agg with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ClientiFornitoriDatiAgg deleteClientiFornitoriDatiAgg(
        it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _clientiFornitoriDatiAggLocalService.deleteClientiFornitoriDatiAgg(clientiFornitoriDatiAggPK);
    }

    /**
    * Deletes the clienti fornitori dati agg from the database. Also notifies the appropriate model listeners.
    *
    * @param clientiFornitoriDatiAgg the clienti fornitori dati agg
    * @return the clienti fornitori dati agg that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ClientiFornitoriDatiAgg deleteClientiFornitoriDatiAgg(
        it.bysoftware.ct.model.ClientiFornitoriDatiAgg clientiFornitoriDatiAgg)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _clientiFornitoriDatiAggLocalService.deleteClientiFornitoriDatiAgg(clientiFornitoriDatiAgg);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _clientiFornitoriDatiAggLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _clientiFornitoriDatiAggLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _clientiFornitoriDatiAggLocalService.dynamicQuery(dynamicQuery,
            start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _clientiFornitoriDatiAggLocalService.dynamicQuery(dynamicQuery,
            start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _clientiFornitoriDatiAggLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _clientiFornitoriDatiAggLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public it.bysoftware.ct.model.ClientiFornitoriDatiAgg fetchClientiFornitoriDatiAgg(
        it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _clientiFornitoriDatiAggLocalService.fetchClientiFornitoriDatiAgg(clientiFornitoriDatiAggPK);
    }

    /**
    * Returns the clienti fornitori dati agg with the primary key.
    *
    * @param clientiFornitoriDatiAggPK the primary key of the clienti fornitori dati agg
    * @return the clienti fornitori dati agg
    * @throws PortalException if a clienti fornitori dati agg with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ClientiFornitoriDatiAgg getClientiFornitoriDatiAgg(
        it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _clientiFornitoriDatiAggLocalService.getClientiFornitoriDatiAgg(clientiFornitoriDatiAggPK);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _clientiFornitoriDatiAggLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the clienti fornitori dati aggs.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of clienti fornitori dati aggs
    * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
    * @return the range of clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> getClientiFornitoriDatiAggs(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _clientiFornitoriDatiAggLocalService.getClientiFornitoriDatiAggs(start,
            end);
    }

    /**
    * Returns the number of clienti fornitori dati aggs.
    *
    * @return the number of clienti fornitori dati aggs
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getClientiFornitoriDatiAggsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _clientiFornitoriDatiAggLocalService.getClientiFornitoriDatiAggsCount();
    }

    /**
    * Updates the clienti fornitori dati agg in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param clientiFornitoriDatiAgg the clienti fornitori dati agg
    * @return the clienti fornitori dati agg that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ClientiFornitoriDatiAgg updateClientiFornitoriDatiAgg(
        it.bysoftware.ct.model.ClientiFornitoriDatiAgg clientiFornitoriDatiAgg)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _clientiFornitoriDatiAggLocalService.updateClientiFornitoriDatiAgg(clientiFornitoriDatiAgg);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _clientiFornitoriDatiAggLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _clientiFornitoriDatiAggLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _clientiFornitoriDatiAggLocalService.invokeMethod(name,
            parameterTypes, arguments);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findAllCustomersInCategory(
        java.lang.String category) {
        return _clientiFornitoriDatiAggLocalService.findAllCustomersInCategory(category);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findAllCustomers() {
        return _clientiFornitoriDatiAggLocalService.findAllCustomers();
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findAllSuppliers() {
        return _clientiFornitoriDatiAggLocalService.findAllSuppliers();
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findGenericSuppliers() {
        return _clientiFornitoriDatiAggLocalService.findGenericSuppliers();
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> findAllAssociates() {
        return _clientiFornitoriDatiAggLocalService.findAllAssociates();
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public ClientiFornitoriDatiAggLocalService getWrappedClientiFornitoriDatiAggLocalService() {
        return _clientiFornitoriDatiAggLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedClientiFornitoriDatiAggLocalService(
        ClientiFornitoriDatiAggLocalService clientiFornitoriDatiAggLocalService) {
        _clientiFornitoriDatiAggLocalService = clientiFornitoriDatiAggLocalService;
    }

    @Override
    public ClientiFornitoriDatiAggLocalService getWrappedService() {
        return _clientiFornitoriDatiAggLocalService;
    }

    @Override
    public void setWrappedService(
        ClientiFornitoriDatiAggLocalService clientiFornitoriDatiAggLocalService) {
        _clientiFornitoriDatiAggLocalService = clientiFornitoriDatiAggLocalService;
    }
}
