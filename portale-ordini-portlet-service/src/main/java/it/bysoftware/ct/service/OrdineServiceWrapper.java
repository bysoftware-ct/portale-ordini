package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link OrdineService}.
 *
 * @author Mario Torrisi
 * @see OrdineService
 * @generated
 */
public class OrdineServiceWrapper implements OrdineService,
    ServiceWrapper<OrdineService> {
    private OrdineService _ordineService;

    public OrdineServiceWrapper(OrdineService ordineService) {
        _ordineService = ordineService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _ordineService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _ordineService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _ordineService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public OrdineService getWrappedOrdineService() {
        return _ordineService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedOrdineService(OrdineService ordineService) {
        _ordineService = ordineService;
    }

    @Override
    public OrdineService getWrappedService() {
        return _ordineService;
    }

    @Override
    public void setWrappedService(OrdineService ordineService) {
        _ordineService = ordineService;
    }
}
