package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SottocontiLocalService}.
 *
 * @author Mario Torrisi
 * @see SottocontiLocalService
 * @generated
 */
public class SottocontiLocalServiceWrapper implements SottocontiLocalService,
    ServiceWrapper<SottocontiLocalService> {
    private SottocontiLocalService _sottocontiLocalService;

    public SottocontiLocalServiceWrapper(
        SottocontiLocalService sottocontiLocalService) {
        _sottocontiLocalService = sottocontiLocalService;
    }

    /**
    * Adds the sottoconti to the database. Also notifies the appropriate model listeners.
    *
    * @param sottoconti the sottoconti
    * @return the sottoconti that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Sottoconti addSottoconti(
        it.bysoftware.ct.model.Sottoconti sottoconti)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sottocontiLocalService.addSottoconti(sottoconti);
    }

    /**
    * Creates a new sottoconti with the primary key. Does not add the sottoconti to the database.
    *
    * @param codice the primary key for the new sottoconti
    * @return the new sottoconti
    */
    @Override
    public it.bysoftware.ct.model.Sottoconti createSottoconti(
        java.lang.String codice) {
        return _sottocontiLocalService.createSottoconti(codice);
    }

    /**
    * Deletes the sottoconti with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codice the primary key of the sottoconti
    * @return the sottoconti that was removed
    * @throws PortalException if a sottoconti with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Sottoconti deleteSottoconti(
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _sottocontiLocalService.deleteSottoconti(codice);
    }

    /**
    * Deletes the sottoconti from the database. Also notifies the appropriate model listeners.
    *
    * @param sottoconti the sottoconti
    * @return the sottoconti that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Sottoconti deleteSottoconti(
        it.bysoftware.ct.model.Sottoconti sottoconti)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sottocontiLocalService.deleteSottoconti(sottoconti);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _sottocontiLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sottocontiLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SottocontiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _sottocontiLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SottocontiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sottocontiLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sottocontiLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sottocontiLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public it.bysoftware.ct.model.Sottoconti fetchSottoconti(
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sottocontiLocalService.fetchSottoconti(codice);
    }

    /**
    * Returns the sottoconti with the primary key.
    *
    * @param codice the primary key of the sottoconti
    * @return the sottoconti
    * @throws PortalException if a sottoconti with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Sottoconti getSottoconti(
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _sottocontiLocalService.getSottoconti(codice);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _sottocontiLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the sottocontis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SottocontiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of sottocontis
    * @param end the upper bound of the range of sottocontis (not inclusive)
    * @return the range of sottocontis
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.Sottoconti> getSottocontis(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sottocontiLocalService.getSottocontis(start, end);
    }

    /**
    * Returns the number of sottocontis.
    *
    * @return the number of sottocontis
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getSottocontisCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sottocontiLocalService.getSottocontisCount();
    }

    /**
    * Updates the sottoconti in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param sottoconti the sottoconti
    * @return the sottoconti that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Sottoconti updateSottoconti(
        it.bysoftware.ct.model.Sottoconti sottoconti)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sottocontiLocalService.updateSottoconti(sottoconti);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _sottocontiLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _sottocontiLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _sottocontiLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public SottocontiLocalService getWrappedSottocontiLocalService() {
        return _sottocontiLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedSottocontiLocalService(
        SottocontiLocalService sottocontiLocalService) {
        _sottocontiLocalService = sottocontiLocalService;
    }

    @Override
    public SottocontiLocalService getWrappedService() {
        return _sottocontiLocalService;
    }

    @Override
    public void setWrappedService(SottocontiLocalService sottocontiLocalService) {
        _sottocontiLocalService = sottocontiLocalService;
    }
}
