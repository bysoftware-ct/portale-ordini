package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.Carrello;
import it.bysoftware.ct.service.CarrelloLocalServiceUtil;

/**
 * @author Mario Torrisi
 * @generated
 */
public abstract class CarrelloActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public CarrelloActionableDynamicQuery() throws SystemException {
        setBaseLocalService(CarrelloLocalServiceUtil.getService());
        setClass(Carrello.class);

        setClassLoader(it.bysoftware.ct.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
