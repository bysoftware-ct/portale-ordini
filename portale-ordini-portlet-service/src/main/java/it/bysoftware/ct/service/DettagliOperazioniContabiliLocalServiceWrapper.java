package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link DettagliOperazioniContabiliLocalService}.
 *
 * @author Mario Torrisi
 * @see DettagliOperazioniContabiliLocalService
 * @generated
 */
public class DettagliOperazioniContabiliLocalServiceWrapper
    implements DettagliOperazioniContabiliLocalService,
        ServiceWrapper<DettagliOperazioniContabiliLocalService> {
    private DettagliOperazioniContabiliLocalService _dettagliOperazioniContabiliLocalService;

    public DettagliOperazioniContabiliLocalServiceWrapper(
        DettagliOperazioniContabiliLocalService dettagliOperazioniContabiliLocalService) {
        _dettagliOperazioniContabiliLocalService = dettagliOperazioniContabiliLocalService;
    }

    /**
    * Adds the dettagli operazioni contabili to the database. Also notifies the appropriate model listeners.
    *
    * @param dettagliOperazioniContabili the dettagli operazioni contabili
    * @return the dettagli operazioni contabili that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.DettagliOperazioniContabili addDettagliOperazioniContabili(
        it.bysoftware.ct.model.DettagliOperazioniContabili dettagliOperazioniContabili)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _dettagliOperazioniContabiliLocalService.addDettagliOperazioniContabili(dettagliOperazioniContabili);
    }

    /**
    * Creates a new dettagli operazioni contabili with the primary key. Does not add the dettagli operazioni contabili to the database.
    *
    * @param dettagliOperazioniContabiliPK the primary key for the new dettagli operazioni contabili
    * @return the new dettagli operazioni contabili
    */
    @Override
    public it.bysoftware.ct.model.DettagliOperazioniContabili createDettagliOperazioniContabili(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK) {
        return _dettagliOperazioniContabiliLocalService.createDettagliOperazioniContabili(dettagliOperazioniContabiliPK);
    }

    /**
    * Deletes the dettagli operazioni contabili with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param dettagliOperazioniContabiliPK the primary key of the dettagli operazioni contabili
    * @return the dettagli operazioni contabili that was removed
    * @throws PortalException if a dettagli operazioni contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.DettagliOperazioniContabili deleteDettagliOperazioniContabili(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _dettagliOperazioniContabiliLocalService.deleteDettagliOperazioniContabili(dettagliOperazioniContabiliPK);
    }

    /**
    * Deletes the dettagli operazioni contabili from the database. Also notifies the appropriate model listeners.
    *
    * @param dettagliOperazioniContabili the dettagli operazioni contabili
    * @return the dettagli operazioni contabili that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.DettagliOperazioniContabili deleteDettagliOperazioniContabili(
        it.bysoftware.ct.model.DettagliOperazioniContabili dettagliOperazioniContabili)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _dettagliOperazioniContabiliLocalService.deleteDettagliOperazioniContabili(dettagliOperazioniContabili);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _dettagliOperazioniContabiliLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _dettagliOperazioniContabiliLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _dettagliOperazioniContabiliLocalService.dynamicQuery(dynamicQuery,
            start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _dettagliOperazioniContabiliLocalService.dynamicQuery(dynamicQuery,
            start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _dettagliOperazioniContabiliLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _dettagliOperazioniContabiliLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public it.bysoftware.ct.model.DettagliOperazioniContabili fetchDettagliOperazioniContabili(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _dettagliOperazioniContabiliLocalService.fetchDettagliOperazioniContabili(dettagliOperazioniContabiliPK);
    }

    /**
    * Returns the dettagli operazioni contabili with the primary key.
    *
    * @param dettagliOperazioniContabiliPK the primary key of the dettagli operazioni contabili
    * @return the dettagli operazioni contabili
    * @throws PortalException if a dettagli operazioni contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.DettagliOperazioniContabili getDettagliOperazioniContabili(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _dettagliOperazioniContabiliLocalService.getDettagliOperazioniContabili(dettagliOperazioniContabiliPK);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _dettagliOperazioniContabiliLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the dettagli operazioni contabilis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of dettagli operazioni contabilis
    * @param end the upper bound of the range of dettagli operazioni contabilis (not inclusive)
    * @return the range of dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.DettagliOperazioniContabili> getDettagliOperazioniContabilis(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _dettagliOperazioniContabiliLocalService.getDettagliOperazioniContabilis(start,
            end);
    }

    /**
    * Returns the number of dettagli operazioni contabilis.
    *
    * @return the number of dettagli operazioni contabilis
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getDettagliOperazioniContabilisCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _dettagliOperazioniContabiliLocalService.getDettagliOperazioniContabilisCount();
    }

    /**
    * Updates the dettagli operazioni contabili in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param dettagliOperazioniContabili the dettagli operazioni contabili
    * @return the dettagli operazioni contabili that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.DettagliOperazioniContabili updateDettagliOperazioniContabili(
        it.bysoftware.ct.model.DettagliOperazioniContabili dettagliOperazioniContabili)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _dettagliOperazioniContabiliLocalService.updateDettagliOperazioniContabili(dettagliOperazioniContabili);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _dettagliOperazioniContabiliLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _dettagliOperazioniContabiliLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _dettagliOperazioniContabiliLocalService.invokeMethod(name,
            parameterTypes, arguments);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.DettagliOperazioniContabili> findByAccountingOperation(
        java.lang.String code) {
        return _dettagliOperazioniContabiliLocalService.findByAccountingOperation(code);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public DettagliOperazioniContabiliLocalService getWrappedDettagliOperazioniContabiliLocalService() {
        return _dettagliOperazioniContabiliLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedDettagliOperazioniContabiliLocalService(
        DettagliOperazioniContabiliLocalService dettagliOperazioniContabiliLocalService) {
        _dettagliOperazioniContabiliLocalService = dettagliOperazioniContabiliLocalService;
    }

    @Override
    public DettagliOperazioniContabiliLocalService getWrappedService() {
        return _dettagliOperazioniContabiliLocalService;
    }

    @Override
    public void setWrappedService(
        DettagliOperazioniContabiliLocalService dettagliOperazioniContabiliLocalService) {
        _dettagliOperazioniContabiliLocalService = dettagliOperazioniContabiliLocalService;
    }
}
