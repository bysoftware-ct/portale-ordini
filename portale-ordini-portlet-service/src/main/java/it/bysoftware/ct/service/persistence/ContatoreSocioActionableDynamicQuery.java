package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.ContatoreSocio;
import it.bysoftware.ct.service.ContatoreSocioLocalServiceUtil;

/**
 * @author Mario Torrisi
 * @generated
 */
public abstract class ContatoreSocioActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public ContatoreSocioActionableDynamicQuery() throws SystemException {
        setBaseLocalService(ContatoreSocioLocalServiceUtil.getService());
        setClass(ContatoreSocio.class);

        setClassLoader(it.bysoftware.ct.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("primaryKey.anno");
    }
}
