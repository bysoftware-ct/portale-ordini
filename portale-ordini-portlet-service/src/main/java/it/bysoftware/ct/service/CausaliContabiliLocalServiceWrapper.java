package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CausaliContabiliLocalService}.
 *
 * @author Mario Torrisi
 * @see CausaliContabiliLocalService
 * @generated
 */
public class CausaliContabiliLocalServiceWrapper
    implements CausaliContabiliLocalService,
        ServiceWrapper<CausaliContabiliLocalService> {
    private CausaliContabiliLocalService _causaliContabiliLocalService;

    public CausaliContabiliLocalServiceWrapper(
        CausaliContabiliLocalService causaliContabiliLocalService) {
        _causaliContabiliLocalService = causaliContabiliLocalService;
    }

    /**
    * Adds the causali contabili to the database. Also notifies the appropriate model listeners.
    *
    * @param causaliContabili the causali contabili
    * @return the causali contabili that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.CausaliContabili addCausaliContabili(
        it.bysoftware.ct.model.CausaliContabili causaliContabili)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliContabiliLocalService.addCausaliContabili(causaliContabili);
    }

    /**
    * Creates a new causali contabili with the primary key. Does not add the causali contabili to the database.
    *
    * @param codiceCausaleCont the primary key for the new causali contabili
    * @return the new causali contabili
    */
    @Override
    public it.bysoftware.ct.model.CausaliContabili createCausaliContabili(
        java.lang.String codiceCausaleCont) {
        return _causaliContabiliLocalService.createCausaliContabili(codiceCausaleCont);
    }

    /**
    * Deletes the causali contabili with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceCausaleCont the primary key of the causali contabili
    * @return the causali contabili that was removed
    * @throws PortalException if a causali contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.CausaliContabili deleteCausaliContabili(
        java.lang.String codiceCausaleCont)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _causaliContabiliLocalService.deleteCausaliContabili(codiceCausaleCont);
    }

    /**
    * Deletes the causali contabili from the database. Also notifies the appropriate model listeners.
    *
    * @param causaliContabili the causali contabili
    * @return the causali contabili that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.CausaliContabili deleteCausaliContabili(
        it.bysoftware.ct.model.CausaliContabili causaliContabili)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliContabiliLocalService.deleteCausaliContabili(causaliContabili);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _causaliContabiliLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliContabiliLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CausaliContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliContabiliLocalService.dynamicQuery(dynamicQuery, start,
            end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CausaliContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliContabiliLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliContabiliLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliContabiliLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public it.bysoftware.ct.model.CausaliContabili fetchCausaliContabili(
        java.lang.String codiceCausaleCont)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliContabiliLocalService.fetchCausaliContabili(codiceCausaleCont);
    }

    /**
    * Returns the causali contabili with the primary key.
    *
    * @param codiceCausaleCont the primary key of the causali contabili
    * @return the causali contabili
    * @throws PortalException if a causali contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.CausaliContabili getCausaliContabili(
        java.lang.String codiceCausaleCont)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _causaliContabiliLocalService.getCausaliContabili(codiceCausaleCont);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _causaliContabiliLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the causali contabilis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CausaliContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of causali contabilis
    * @param end the upper bound of the range of causali contabilis (not inclusive)
    * @return the range of causali contabilis
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.CausaliContabili> getCausaliContabilis(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliContabiliLocalService.getCausaliContabilis(start, end);
    }

    /**
    * Returns the number of causali contabilis.
    *
    * @return the number of causali contabilis
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getCausaliContabilisCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliContabiliLocalService.getCausaliContabilisCount();
    }

    /**
    * Updates the causali contabili in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param causaliContabili the causali contabili
    * @return the causali contabili that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.CausaliContabili updateCausaliContabili(
        it.bysoftware.ct.model.CausaliContabili causaliContabili)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _causaliContabiliLocalService.updateCausaliContabili(causaliContabili);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _causaliContabiliLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _causaliContabiliLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _causaliContabiliLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public CausaliContabiliLocalService getWrappedCausaliContabiliLocalService() {
        return _causaliContabiliLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedCausaliContabiliLocalService(
        CausaliContabiliLocalService causaliContabiliLocalService) {
        _causaliContabiliLocalService = causaliContabiliLocalService;
    }

    @Override
    public CausaliContabiliLocalService getWrappedService() {
        return _causaliContabiliLocalService;
    }

    @Override
    public void setWrappedService(
        CausaliContabiliLocalService causaliContabiliLocalService) {
        _causaliContabiliLocalService = causaliContabiliLocalService;
    }
}
