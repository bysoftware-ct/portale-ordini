package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ContatoreSocioService}.
 *
 * @author Mario Torrisi
 * @see ContatoreSocioService
 * @generated
 */
public class ContatoreSocioServiceWrapper implements ContatoreSocioService,
    ServiceWrapper<ContatoreSocioService> {
    private ContatoreSocioService _contatoreSocioService;

    public ContatoreSocioServiceWrapper(
        ContatoreSocioService contatoreSocioService) {
        _contatoreSocioService = contatoreSocioService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _contatoreSocioService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _contatoreSocioService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _contatoreSocioService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public ContatoreSocioService getWrappedContatoreSocioService() {
        return _contatoreSocioService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedContatoreSocioService(
        ContatoreSocioService contatoreSocioService) {
        _contatoreSocioService = contatoreSocioService;
    }

    @Override
    public ContatoreSocioService getWrappedService() {
        return _contatoreSocioService;
    }

    @Override
    public void setWrappedService(ContatoreSocioService contatoreSocioService) {
        _contatoreSocioService = contatoreSocioService;
    }
}
