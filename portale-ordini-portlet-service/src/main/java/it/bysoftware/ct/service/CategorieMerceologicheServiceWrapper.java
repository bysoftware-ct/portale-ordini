package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CategorieMerceologicheService}.
 *
 * @author Mario Torrisi
 * @see CategorieMerceologicheService
 * @generated
 */
public class CategorieMerceologicheServiceWrapper
    implements CategorieMerceologicheService,
        ServiceWrapper<CategorieMerceologicheService> {
    private CategorieMerceologicheService _categorieMerceologicheService;

    public CategorieMerceologicheServiceWrapper(
        CategorieMerceologicheService categorieMerceologicheService) {
        _categorieMerceologicheService = categorieMerceologicheService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _categorieMerceologicheService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _categorieMerceologicheService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _categorieMerceologicheService.invokeMethod(name,
            parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public CategorieMerceologicheService getWrappedCategorieMerceologicheService() {
        return _categorieMerceologicheService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedCategorieMerceologicheService(
        CategorieMerceologicheService categorieMerceologicheService) {
        _categorieMerceologicheService = categorieMerceologicheService;
    }

    @Override
    public CategorieMerceologicheService getWrappedService() {
        return _categorieMerceologicheService;
    }

    @Override
    public void setWrappedService(
        CategorieMerceologicheService categorieMerceologicheService) {
        _categorieMerceologicheService = categorieMerceologicheService;
    }
}
