package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link TipoCarrelloService}.
 *
 * @author Mario Torrisi
 * @see TipoCarrelloService
 * @generated
 */
public class TipoCarrelloServiceWrapper implements TipoCarrelloService,
    ServiceWrapper<TipoCarrelloService> {
    private TipoCarrelloService _tipoCarrelloService;

    public TipoCarrelloServiceWrapper(TipoCarrelloService tipoCarrelloService) {
        _tipoCarrelloService = tipoCarrelloService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _tipoCarrelloService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _tipoCarrelloService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _tipoCarrelloService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public TipoCarrelloService getWrappedTipoCarrelloService() {
        return _tipoCarrelloService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedTipoCarrelloService(
        TipoCarrelloService tipoCarrelloService) {
        _tipoCarrelloService = tipoCarrelloService;
    }

    @Override
    public TipoCarrelloService getWrappedService() {
        return _tipoCarrelloService;
    }

    @Override
    public void setWrappedService(TipoCarrelloService tipoCarrelloService) {
        _tipoCarrelloService = tipoCarrelloService;
    }
}
