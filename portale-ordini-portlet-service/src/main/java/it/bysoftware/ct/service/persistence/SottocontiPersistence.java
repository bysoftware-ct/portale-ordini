package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.Sottoconti;

/**
 * The persistence interface for the sottoconti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see SottocontiPersistenceImpl
 * @see SottocontiUtil
 * @generated
 */
public interface SottocontiPersistence extends BasePersistence<Sottoconti> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link SottocontiUtil} to access the sottoconti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the sottoconti in the entity cache if it is enabled.
    *
    * @param sottoconti the sottoconti
    */
    public void cacheResult(it.bysoftware.ct.model.Sottoconti sottoconti);

    /**
    * Caches the sottocontis in the entity cache if it is enabled.
    *
    * @param sottocontis the sottocontis
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.Sottoconti> sottocontis);

    /**
    * Creates a new sottoconti with the primary key. Does not add the sottoconti to the database.
    *
    * @param codice the primary key for the new sottoconti
    * @return the new sottoconti
    */
    public it.bysoftware.ct.model.Sottoconti create(java.lang.String codice);

    /**
    * Removes the sottoconti with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codice the primary key of the sottoconti
    * @return the sottoconti that was removed
    * @throws it.bysoftware.ct.NoSuchSottocontiException if a sottoconti with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Sottoconti remove(java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSottocontiException;

    public it.bysoftware.ct.model.Sottoconti updateImpl(
        it.bysoftware.ct.model.Sottoconti sottoconti)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the sottoconti with the primary key or throws a {@link it.bysoftware.ct.NoSuchSottocontiException} if it could not be found.
    *
    * @param codice the primary key of the sottoconti
    * @return the sottoconti
    * @throws it.bysoftware.ct.NoSuchSottocontiException if a sottoconti with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Sottoconti findByPrimaryKey(
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSottocontiException;

    /**
    * Returns the sottoconti with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param codice the primary key of the sottoconti
    * @return the sottoconti, or <code>null</code> if a sottoconti with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Sottoconti fetchByPrimaryKey(
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the sottocontis.
    *
    * @return the sottocontis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Sottoconti> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the sottocontis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SottocontiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of sottocontis
    * @param end the upper bound of the range of sottocontis (not inclusive)
    * @return the range of sottocontis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Sottoconti> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the sottocontis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SottocontiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of sottocontis
    * @param end the upper bound of the range of sottocontis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of sottocontis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Sottoconti> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the sottocontis from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of sottocontis.
    *
    * @return the number of sottocontis
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
