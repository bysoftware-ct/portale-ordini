package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.SpettanzeSoci;

/**
 * The persistence interface for the spettanze soci service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see SpettanzeSociPersistenceImpl
 * @see SpettanzeSociUtil
 * @generated
 */
public interface SpettanzeSociPersistence extends BasePersistence<SpettanzeSoci> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link SpettanzeSociUtil} to access the spettanze soci persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; and stato = &#63; or throws a {@link it.bysoftware.ct.NoSuchSpettanzeSociException} if it could not be found.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @return the matching spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci findByFatturaVenditaStato(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, int numeroProtocollo,
        java.lang.String codiceFornitore, int stato)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException;

    /**
    * Returns the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; and stato = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @return the matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci fetchByFatturaVenditaStato(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, int numeroProtocollo,
        java.lang.String codiceFornitore, int stato)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; and stato = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci fetchByFatturaVenditaStato(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, int numeroProtocollo,
        java.lang.String codiceFornitore, int stato, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; and stato = &#63; from the database.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @return the spettanze soci that was removed
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci removeByFatturaVenditaStato(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, int numeroProtocollo,
        java.lang.String codiceFornitore, int stato)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException;

    /**
    * Returns the number of spettanze socis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; and stato = &#63;.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @return the number of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public int countByFatturaVenditaStato(int anno,
        java.lang.String codiceAttivita, java.lang.String codiceCentro,
        int numeroProtocollo, java.lang.String codiceFornitore, int stato)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; or throws a {@link it.bysoftware.ct.NoSuchSpettanzeSociException} if it could not be found.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @return the matching spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci findByFatturaVendita(int anno,
        java.lang.String codiceAttivita, java.lang.String codiceCentro,
        int numeroProtocollo, java.lang.String codiceFornitore)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException;

    /**
    * Returns the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @return the matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci fetchByFatturaVendita(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, int numeroProtocollo,
        java.lang.String codiceFornitore)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci fetchByFatturaVendita(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, int numeroProtocollo,
        java.lang.String codiceFornitore, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; from the database.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @return the spettanze soci that was removed
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci removeByFatturaVendita(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, int numeroProtocollo,
        java.lang.String codiceFornitore)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException;

    /**
    * Returns the number of spettanze socis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63;.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param numeroProtocollo the numero protocollo
    * @param codiceFornitore the codice fornitore
    * @return the number of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public int countByFatturaVendita(int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, int numeroProtocollo,
        java.lang.String codiceFornitore)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the spettanze socis where codiceFornitore = &#63; and stato = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @return the matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByFornitoreStato(
        java.lang.String codiceFornitore, int stato)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the spettanze socis where codiceFornitore = &#63; and stato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @param start the lower bound of the range of spettanze socis
    * @param end the upper bound of the range of spettanze socis (not inclusive)
    * @return the range of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByFornitoreStato(
        java.lang.String codiceFornitore, int stato, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the spettanze socis where codiceFornitore = &#63; and stato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @param start the lower bound of the range of spettanze socis
    * @param end the upper bound of the range of spettanze socis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByFornitoreStato(
        java.lang.String codiceFornitore, int stato, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first spettanze soci in the ordered set where codiceFornitore = &#63; and stato = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci findByFornitoreStato_First(
        java.lang.String codiceFornitore, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException;

    /**
    * Returns the first spettanze soci in the ordered set where codiceFornitore = &#63; and stato = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci fetchByFornitoreStato_First(
        java.lang.String codiceFornitore, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last spettanze soci in the ordered set where codiceFornitore = &#63; and stato = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci findByFornitoreStato_Last(
        java.lang.String codiceFornitore, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException;

    /**
    * Returns the last spettanze soci in the ordered set where codiceFornitore = &#63; and stato = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci fetchByFornitoreStato_Last(
        java.lang.String codiceFornitore, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the spettanze socis before and after the current spettanze soci in the ordered set where codiceFornitore = &#63; and stato = &#63;.
    *
    * @param id the primary key of the current spettanze soci
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a spettanze soci with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci[] findByFornitoreStato_PrevAndNext(
        long id, java.lang.String codiceFornitore, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException;

    /**
    * Removes all the spettanze socis where codiceFornitore = &#63; and stato = &#63; from the database.
    *
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @throws SystemException if a system exception occurred
    */
    public void removeByFornitoreStato(java.lang.String codiceFornitore,
        int stato) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of spettanze socis where codiceFornitore = &#63; and stato = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param stato the stato
    * @return the number of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public int countByFornitoreStato(java.lang.String codiceFornitore, int stato)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the spettanze socis where codiceFornitore = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @return the matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByFornitore(
        java.lang.String codiceFornitore)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the spettanze socis where codiceFornitore = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceFornitore the codice fornitore
    * @param start the lower bound of the range of spettanze socis
    * @param end the upper bound of the range of spettanze socis (not inclusive)
    * @return the range of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByFornitore(
        java.lang.String codiceFornitore, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the spettanze socis where codiceFornitore = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceFornitore the codice fornitore
    * @param start the lower bound of the range of spettanze socis
    * @param end the upper bound of the range of spettanze socis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByFornitore(
        java.lang.String codiceFornitore, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first spettanze soci in the ordered set where codiceFornitore = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci findByFornitore_First(
        java.lang.String codiceFornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException;

    /**
    * Returns the first spettanze soci in the ordered set where codiceFornitore = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci fetchByFornitore_First(
        java.lang.String codiceFornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last spettanze soci in the ordered set where codiceFornitore = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci findByFornitore_Last(
        java.lang.String codiceFornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException;

    /**
    * Returns the last spettanze soci in the ordered set where codiceFornitore = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci fetchByFornitore_Last(
        java.lang.String codiceFornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the spettanze socis before and after the current spettanze soci in the ordered set where codiceFornitore = &#63;.
    *
    * @param id the primary key of the current spettanze soci
    * @param codiceFornitore the codice fornitore
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a spettanze soci with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci[] findByFornitore_PrevAndNext(
        long id, java.lang.String codiceFornitore,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException;

    /**
    * Removes all the spettanze socis where codiceFornitore = &#63; from the database.
    *
    * @param codiceFornitore the codice fornitore
    * @throws SystemException if a system exception occurred
    */
    public void removeByFornitore(java.lang.String codiceFornitore)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of spettanze socis where codiceFornitore = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @return the number of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public int countByFornitore(java.lang.String codiceFornitore)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the spettanze socis where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @return the matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByFornitoreAnnoPagamentoIdPagamento(
        java.lang.String codiceFornitore, int annoPagamento, int idPagamento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the spettanze socis where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param start the lower bound of the range of spettanze socis
    * @param end the upper bound of the range of spettanze socis (not inclusive)
    * @return the range of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByFornitoreAnnoPagamentoIdPagamento(
        java.lang.String codiceFornitore, int annoPagamento, int idPagamento,
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the spettanze socis where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param start the lower bound of the range of spettanze socis
    * @param end the upper bound of the range of spettanze socis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByFornitoreAnnoPagamentoIdPagamento(
        java.lang.String codiceFornitore, int annoPagamento, int idPagamento,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first spettanze soci in the ordered set where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci findByFornitoreAnnoPagamentoIdPagamento_First(
        java.lang.String codiceFornitore, int annoPagamento, int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException;

    /**
    * Returns the first spettanze soci in the ordered set where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci fetchByFornitoreAnnoPagamentoIdPagamento_First(
        java.lang.String codiceFornitore, int annoPagamento, int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last spettanze soci in the ordered set where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci findByFornitoreAnnoPagamentoIdPagamento_Last(
        java.lang.String codiceFornitore, int annoPagamento, int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException;

    /**
    * Returns the last spettanze soci in the ordered set where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci fetchByFornitoreAnnoPagamentoIdPagamento_Last(
        java.lang.String codiceFornitore, int annoPagamento, int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the spettanze socis before and after the current spettanze soci in the ordered set where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param id the primary key of the current spettanze soci
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a spettanze soci with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci[] findByFornitoreAnnoPagamentoIdPagamento_PrevAndNext(
        long id, java.lang.String codiceFornitore, int annoPagamento,
        int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException;

    /**
    * Removes all the spettanze socis where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63; from the database.
    *
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @throws SystemException if a system exception occurred
    */
    public void removeByFornitoreAnnoPagamentoIdPagamento(
        java.lang.String codiceFornitore, int annoPagamento, int idPagamento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of spettanze socis where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceFornitore the codice fornitore
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @return the number of matching spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public int countByFornitoreAnnoPagamentoIdPagamento(
        java.lang.String codiceFornitore, int annoPagamento, int idPagamento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the spettanze soci in the entity cache if it is enabled.
    *
    * @param spettanzeSoci the spettanze soci
    */
    public void cacheResult(it.bysoftware.ct.model.SpettanzeSoci spettanzeSoci);

    /**
    * Caches the spettanze socis in the entity cache if it is enabled.
    *
    * @param spettanzeSocis the spettanze socis
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.SpettanzeSoci> spettanzeSocis);

    /**
    * Creates a new spettanze soci with the primary key. Does not add the spettanze soci to the database.
    *
    * @param id the primary key for the new spettanze soci
    * @return the new spettanze soci
    */
    public it.bysoftware.ct.model.SpettanzeSoci create(long id);

    /**
    * Removes the spettanze soci with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the spettanze soci
    * @return the spettanze soci that was removed
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a spettanze soci with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci remove(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException;

    public it.bysoftware.ct.model.SpettanzeSoci updateImpl(
        it.bysoftware.ct.model.SpettanzeSoci spettanzeSoci)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the spettanze soci with the primary key or throws a {@link it.bysoftware.ct.NoSuchSpettanzeSociException} if it could not be found.
    *
    * @param id the primary key of the spettanze soci
    * @return the spettanze soci
    * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a spettanze soci with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci findByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSpettanzeSociException;

    /**
    * Returns the spettanze soci with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the spettanze soci
    * @return the spettanze soci, or <code>null</code> if a spettanze soci with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SpettanzeSoci fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the spettanze socis.
    *
    * @return the spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the spettanze socis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of spettanze socis
    * @param end the upper bound of the range of spettanze socis (not inclusive)
    * @return the range of spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the spettanze socis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of spettanze socis
    * @param end the upper bound of the range of spettanze socis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the spettanze socis from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of spettanze socis.
    *
    * @return the number of spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
