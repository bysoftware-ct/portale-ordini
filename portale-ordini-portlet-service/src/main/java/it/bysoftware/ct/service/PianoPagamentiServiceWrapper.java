package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PianoPagamentiService}.
 *
 * @author Mario Torrisi
 * @see PianoPagamentiService
 * @generated
 */
public class PianoPagamentiServiceWrapper implements PianoPagamentiService,
    ServiceWrapper<PianoPagamentiService> {
    private PianoPagamentiService _pianoPagamentiService;

    public PianoPagamentiServiceWrapper(
        PianoPagamentiService pianoPagamentiService) {
        _pianoPagamentiService = pianoPagamentiService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _pianoPagamentiService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _pianoPagamentiService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _pianoPagamentiService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public PianoPagamentiService getWrappedPianoPagamentiService() {
        return _pianoPagamentiService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedPianoPagamentiService(
        PianoPagamentiService pianoPagamentiService) {
        _pianoPagamentiService = pianoPagamentiService;
    }

    @Override
    public PianoPagamentiService getWrappedService() {
        return _pianoPagamentiService;
    }

    @Override
    public void setWrappedService(PianoPagamentiService pianoPagamentiService) {
        _pianoPagamentiService = pianoPagamentiService;
    }
}
