package it.bysoftware.ct.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for MovimentoVuoto. This utility wraps
 * {@link it.bysoftware.ct.service.impl.MovimentoVuotoLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Mario Torrisi
 * @see MovimentoVuotoLocalService
 * @see it.bysoftware.ct.service.base.MovimentoVuotoLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.impl.MovimentoVuotoLocalServiceImpl
 * @generated
 */
public class MovimentoVuotoLocalServiceUtil {
    private static MovimentoVuotoLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link it.bysoftware.ct.service.impl.MovimentoVuotoLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the movimento vuoto to the database. Also notifies the appropriate model listeners.
    *
    * @param movimentoVuoto the movimento vuoto
    * @return the movimento vuoto that was added
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto addMovimentoVuoto(
        it.bysoftware.ct.model.MovimentoVuoto movimentoVuoto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addMovimentoVuoto(movimentoVuoto);
    }

    /**
    * Creates a new movimento vuoto with the primary key. Does not add the movimento vuoto to the database.
    *
    * @param idMovimento the primary key for the new movimento vuoto
    * @return the new movimento vuoto
    */
    public static it.bysoftware.ct.model.MovimentoVuoto createMovimentoVuoto(
        long idMovimento) {
        return getService().createMovimentoVuoto(idMovimento);
    }

    /**
    * Deletes the movimento vuoto with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param idMovimento the primary key of the movimento vuoto
    * @return the movimento vuoto that was removed
    * @throws PortalException if a movimento vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto deleteMovimentoVuoto(
        long idMovimento)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteMovimentoVuoto(idMovimento);
    }

    /**
    * Deletes the movimento vuoto from the database. Also notifies the appropriate model listeners.
    *
    * @param movimentoVuoto the movimento vuoto
    * @return the movimento vuoto that was removed
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto deleteMovimentoVuoto(
        it.bysoftware.ct.model.MovimentoVuoto movimentoVuoto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteMovimentoVuoto(movimentoVuoto);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static it.bysoftware.ct.model.MovimentoVuoto fetchMovimentoVuoto(
        long idMovimento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchMovimentoVuoto(idMovimento);
    }

    /**
    * Returns the movimento vuoto with the primary key.
    *
    * @param idMovimento the primary key of the movimento vuoto
    * @return the movimento vuoto
    * @throws PortalException if a movimento vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto getMovimentoVuoto(
        long idMovimento)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getMovimentoVuoto(idMovimento);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the movimento vuotos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of movimento vuotos
    * @param end the upper bound of the range of movimento vuotos (not inclusive)
    * @return the range of movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.MovimentoVuoto> getMovimentoVuotos(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getMovimentoVuotos(start, end);
    }

    /**
    * Returns the number of movimento vuotos.
    *
    * @return the number of movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public static int getMovimentoVuotosCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getMovimentoVuotosCount();
    }

    /**
    * Updates the movimento vuoto in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param movimentoVuoto the movimento vuoto
    * @return the movimento vuoto that was updated
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto updateMovimentoVuoto(
        it.bysoftware.ct.model.MovimentoVuoto movimentoVuoto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateMovimentoVuoto(movimentoVuoto);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findMovementiByCodiceVuoto(
        java.lang.String c) {
        return getService().findMovementiByCodiceVuoto(c);
    }

    public static java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findMovementiByCodiceSoggetto(
        java.lang.String c) {
        return getService().findMovementiByCodiceSoggetto(c);
    }

    public static java.util.List<java.lang.Object[]> getPartnerReturnablesStock(
        java.lang.String partner, java.util.Date date, java.lang.String code) {
        return getService().getPartnerReturnablesStock(partner, date, code);
    }

    public static java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findMovementiByVuotoSoggettoDataTipo(
        java.lang.String itemCode, java.lang.String parternCode,
        java.util.Date date, int type) {
        return getService()
                   .findMovementiByVuotoSoggettoDataTipo(itemCode, parternCode,
            date, type);
    }

    public static java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findMovementiByCodiceSoggetto(
        java.lang.String c, java.util.Date from, java.util.Date to,
        java.lang.String code, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator comp) {
        return getService()
                   .findMovementiByCodiceSoggetto(c, from, to, code, start,
            end, comp);
    }

    public static java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findMovementiByCodiceSoggetto(
        java.lang.String c, int type, java.util.Date from, java.util.Date to,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator comp) {
        return getService()
                   .findMovementiByCodiceSoggetto(c, type, from, to, start,
            end, comp);
    }

    public static int getNumber(java.lang.String code, java.util.Date date) {
        return getService().getNumber(code, date);
    }

    public static void clearService() {
        _service = null;
    }

    public static MovimentoVuotoLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    MovimentoVuotoLocalService.class.getName());

            if (invokableLocalService instanceof MovimentoVuotoLocalService) {
                _service = (MovimentoVuotoLocalService) invokableLocalService;
            } else {
                _service = new MovimentoVuotoLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(MovimentoVuotoLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(MovimentoVuotoLocalService service) {
    }
}
