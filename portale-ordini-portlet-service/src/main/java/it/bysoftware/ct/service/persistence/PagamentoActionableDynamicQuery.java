package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.Pagamento;
import it.bysoftware.ct.service.PagamentoLocalServiceUtil;

/**
 * @author Mario Torrisi
 * @generated
 */
public abstract class PagamentoActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public PagamentoActionableDynamicQuery() throws SystemException {
        setBaseLocalService(PagamentoLocalServiceUtil.getService());
        setClass(Pagamento.class);

        setClassLoader(it.bysoftware.ct.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("primaryKey.id");
    }
}
