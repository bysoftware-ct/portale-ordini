package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ClientiFornitoriDatiAggService}.
 *
 * @author Mario Torrisi
 * @see ClientiFornitoriDatiAggService
 * @generated
 */
public class ClientiFornitoriDatiAggServiceWrapper
    implements ClientiFornitoriDatiAggService,
        ServiceWrapper<ClientiFornitoriDatiAggService> {
    private ClientiFornitoriDatiAggService _clientiFornitoriDatiAggService;

    public ClientiFornitoriDatiAggServiceWrapper(
        ClientiFornitoriDatiAggService clientiFornitoriDatiAggService) {
        _clientiFornitoriDatiAggService = clientiFornitoriDatiAggService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _clientiFornitoriDatiAggService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _clientiFornitoriDatiAggService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _clientiFornitoriDatiAggService.invokeMethod(name,
            parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public ClientiFornitoriDatiAggService getWrappedClientiFornitoriDatiAggService() {
        return _clientiFornitoriDatiAggService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedClientiFornitoriDatiAggService(
        ClientiFornitoriDatiAggService clientiFornitoriDatiAggService) {
        _clientiFornitoriDatiAggService = clientiFornitoriDatiAggService;
    }

    @Override
    public ClientiFornitoriDatiAggService getWrappedService() {
        return _clientiFornitoriDatiAggService;
    }

    @Override
    public void setWrappedService(
        ClientiFornitoriDatiAggService clientiFornitoriDatiAggService) {
        _clientiFornitoriDatiAggService = clientiFornitoriDatiAggService;
    }
}
