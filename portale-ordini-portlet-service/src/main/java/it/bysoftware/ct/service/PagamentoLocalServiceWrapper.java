package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PagamentoLocalService}.
 *
 * @author Mario Torrisi
 * @see PagamentoLocalService
 * @generated
 */
public class PagamentoLocalServiceWrapper implements PagamentoLocalService,
    ServiceWrapper<PagamentoLocalService> {
    private PagamentoLocalService _pagamentoLocalService;

    public PagamentoLocalServiceWrapper(
        PagamentoLocalService pagamentoLocalService) {
        _pagamentoLocalService = pagamentoLocalService;
    }

    /**
    * Adds the pagamento to the database. Also notifies the appropriate model listeners.
    *
    * @param pagamento the pagamento
    * @return the pagamento that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Pagamento addPagamento(
        it.bysoftware.ct.model.Pagamento pagamento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pagamentoLocalService.addPagamento(pagamento);
    }

    /**
    * Creates a new pagamento with the primary key. Does not add the pagamento to the database.
    *
    * @param pagamentoPK the primary key for the new pagamento
    * @return the new pagamento
    */
    @Override
    public it.bysoftware.ct.model.Pagamento createPagamento(
        it.bysoftware.ct.service.persistence.PagamentoPK pagamentoPK) {
        return _pagamentoLocalService.createPagamento(pagamentoPK);
    }

    /**
    * Deletes the pagamento with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param pagamentoPK the primary key of the pagamento
    * @return the pagamento that was removed
    * @throws PortalException if a pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Pagamento deletePagamento(
        it.bysoftware.ct.service.persistence.PagamentoPK pagamentoPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _pagamentoLocalService.deletePagamento(pagamentoPK);
    }

    /**
    * Deletes the pagamento from the database. Also notifies the appropriate model listeners.
    *
    * @param pagamento the pagamento
    * @return the pagamento that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Pagamento deletePagamento(
        it.bysoftware.ct.model.Pagamento pagamento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pagamentoLocalService.deletePagamento(pagamento);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _pagamentoLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pagamentoLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _pagamentoLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pagamentoLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pagamentoLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pagamentoLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public it.bysoftware.ct.model.Pagamento fetchPagamento(
        it.bysoftware.ct.service.persistence.PagamentoPK pagamentoPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pagamentoLocalService.fetchPagamento(pagamentoPK);
    }

    /**
    * Returns the pagamento with the primary key.
    *
    * @param pagamentoPK the primary key of the pagamento
    * @return the pagamento
    * @throws PortalException if a pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Pagamento getPagamento(
        it.bysoftware.ct.service.persistence.PagamentoPK pagamentoPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _pagamentoLocalService.getPagamento(pagamentoPK);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _pagamentoLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the pagamentos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of pagamentos
    * @param end the upper bound of the range of pagamentos (not inclusive)
    * @return the range of pagamentos
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.Pagamento> getPagamentos(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pagamentoLocalService.getPagamentos(start, end);
    }

    /**
    * Returns the number of pagamentos.
    *
    * @return the number of pagamentos
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getPagamentosCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pagamentoLocalService.getPagamentosCount();
    }

    /**
    * Updates the pagamento in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param pagamento the pagamento
    * @return the pagamento that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Pagamento updatePagamento(
        it.bysoftware.ct.model.Pagamento pagamento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _pagamentoLocalService.updatePagamento(pagamento);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _pagamentoLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _pagamentoLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _pagamentoLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    @Override
    public int getNumber(int year, java.lang.String partner) {
        return _pagamentoLocalService.getNumber(year, partner);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Pagamento> findByYear(int year) {
        return _pagamentoLocalService.findByYear(year);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Pagamento> findByYearAndState(
        int year, java.lang.Integer state) {
        return _pagamentoLocalService.findByYearAndState(year, state);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public PagamentoLocalService getWrappedPagamentoLocalService() {
        return _pagamentoLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedPagamentoLocalService(
        PagamentoLocalService pagamentoLocalService) {
        _pagamentoLocalService = pagamentoLocalService;
    }

    @Override
    public PagamentoLocalService getWrappedService() {
        return _pagamentoLocalService;
    }

    @Override
    public void setWrappedService(PagamentoLocalService pagamentoLocalService) {
        _pagamentoLocalService = pagamentoLocalService;
    }
}
