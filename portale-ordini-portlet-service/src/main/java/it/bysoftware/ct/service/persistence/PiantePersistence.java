package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.Piante;

/**
 * The persistence interface for the piante service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see PiantePersistenceImpl
 * @see PianteUtil
 * @generated
 */
public interface PiantePersistence extends BasePersistence<Piante> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link PianteUtil} to access the piante persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns the piante where codice = &#63; or throws a {@link it.bysoftware.ct.NoSuchPianteException} if it could not be found.
    *
    * @param codice the codice
    * @return the matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante findByCodice(java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the piante where codice = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param codice the codice
    * @return the matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByCodice(java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the piante where codice = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param codice the codice
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByCodice(
        java.lang.String codice, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the piante where codice = &#63; from the database.
    *
    * @param codice the codice
    * @return the piante that was removed
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante removeByCodice(java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the number of piantes where codice = &#63;.
    *
    * @param codice the codice
    * @return the number of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public int countByCodice(java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the piantes where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @return the matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByNomeCodiceLike(
        boolean obsoleto, java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the piantes where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByNomeCodiceLike(
        boolean obsoleto, java.lang.String nome, java.lang.String codice,
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the piantes where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByNomeCodiceLike(
        boolean obsoleto, java.lang.String nome, java.lang.String codice,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante findByNomeCodiceLike_First(
        boolean obsoleto, java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByNomeCodiceLike_First(
        boolean obsoleto, java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante findByNomeCodiceLike_Last(
        boolean obsoleto, java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByNomeCodiceLike_Last(
        boolean obsoleto, java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param id the primary key of the current piante
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante[] findByNomeCodiceLike_PrevAndNext(
        long id, boolean obsoleto, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Removes all the piantes where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
    *
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @throws SystemException if a system exception occurred
    */
    public void removeByNomeCodiceLike(boolean obsoleto, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of piantes where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param nome the nome
    * @param codice the codice
    * @return the number of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public int countByNomeCodiceLike(boolean obsoleto, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the piantes where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @return the matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByCategoria(
        boolean obsoleto, long idCategoria, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByCategoria(
        boolean obsoleto, long idCategoria, java.lang.String nome,
        java.lang.String codice, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByCategoria(
        boolean obsoleto, long idCategoria, java.lang.String nome,
        java.lang.String codice, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante findByCategoria_First(
        boolean obsoleto, long idCategoria, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByCategoria_First(
        boolean obsoleto, long idCategoria, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante findByCategoria_Last(
        boolean obsoleto, long idCategoria, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByCategoria_Last(
        boolean obsoleto, long idCategoria, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param id the primary key of the current piante
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante[] findByCategoria_PrevAndNext(
        long id, boolean obsoleto, long idCategoria, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Removes all the piantes where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @throws SystemException if a system exception occurred
    */
    public void removeByCategoria(boolean obsoleto, long idCategoria,
        java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of piantes where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param nome the nome
    * @param codice the codice
    * @return the number of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public int countByCategoria(boolean obsoleto, long idCategoria,
        java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the piantes where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @return the matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByAttivaNomeCodiceLike(
        boolean obsoleto, boolean attiva, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the piantes where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByAttivaNomeCodiceLike(
        boolean obsoleto, boolean attiva, java.lang.String nome,
        java.lang.String codice, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the piantes where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByAttivaNomeCodiceLike(
        boolean obsoleto, boolean attiva, java.lang.String nome,
        java.lang.String codice, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante findByAttivaNomeCodiceLike_First(
        boolean obsoleto, boolean attiva, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByAttivaNomeCodiceLike_First(
        boolean obsoleto, boolean attiva, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante findByAttivaNomeCodiceLike_Last(
        boolean obsoleto, boolean attiva, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByAttivaNomeCodiceLike_Last(
        boolean obsoleto, boolean attiva, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param id the primary key of the current piante
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante[] findByAttivaNomeCodiceLike_PrevAndNext(
        long id, boolean obsoleto, boolean attiva, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Removes all the piantes where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
    *
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @throws SystemException if a system exception occurred
    */
    public void removeByAttivaNomeCodiceLike(boolean obsoleto, boolean attiva,
        java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of piantes where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @return the number of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public int countByAttivaNomeCodiceLike(boolean obsoleto, boolean attiva,
        java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the piantes where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @return the matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByCategoriaAttiva(
        boolean obsoleto, long idCategoria, boolean attiva,
        java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByCategoriaAttiva(
        boolean obsoleto, long idCategoria, boolean attiva,
        java.lang.String nome, java.lang.String codice, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByCategoriaAttiva(
        boolean obsoleto, long idCategoria, boolean attiva,
        java.lang.String nome, java.lang.String codice, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante findByCategoriaAttiva_First(
        boolean obsoleto, long idCategoria, boolean attiva,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByCategoriaAttiva_First(
        boolean obsoleto, long idCategoria, boolean attiva,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante findByCategoriaAttiva_Last(
        boolean obsoleto, long idCategoria, boolean attiva,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByCategoriaAttiva_Last(
        boolean obsoleto, long idCategoria, boolean attiva,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param id the primary key of the current piante
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante[] findByCategoriaAttiva_PrevAndNext(
        long id, boolean obsoleto, long idCategoria, boolean attiva,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Removes all the piantes where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @throws SystemException if a system exception occurred
    */
    public void removeByCategoriaAttiva(boolean obsoleto, long idCategoria,
        boolean attiva, java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of piantes where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @return the number of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public int countByCategoriaAttiva(boolean obsoleto, long idCategoria,
        boolean attiva, java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the piantes where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @return the matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByFornitoreNomeCodiceLike(
        boolean obsoleto, java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the piantes where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByFornitoreNomeCodiceLike(
        boolean obsoleto, java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the piantes where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByFornitoreNomeCodiceLike(
        boolean obsoleto, java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante findByFornitoreNomeCodiceLike_First(
        boolean obsoleto, java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByFornitoreNomeCodiceLike_First(
        boolean obsoleto, java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante findByFornitoreNomeCodiceLike_Last(
        boolean obsoleto, java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByFornitoreNomeCodiceLike_Last(
        boolean obsoleto, java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param id the primary key of the current piante
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante[] findByFornitoreNomeCodiceLike_PrevAndNext(
        long id, boolean obsoleto, java.lang.String idFornitore,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Removes all the piantes where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @throws SystemException if a system exception occurred
    */
    public void removeByFornitoreNomeCodiceLike(boolean obsoleto,
        java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of piantes where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @return the number of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public int countByFornitoreNomeCodiceLike(boolean obsoleto,
        java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @return the matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByCategoriaFornitore(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByCategoriaFornitore(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        java.lang.String nome, java.lang.String codice, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByCategoriaFornitore(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        java.lang.String nome, java.lang.String codice, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante findByCategoriaFornitore_First(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByCategoriaFornitore_First(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante findByCategoriaFornitore_Last(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByCategoriaFornitore_Last(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param id the primary key of the current piante
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante[] findByCategoriaFornitore_PrevAndNext(
        long id, boolean obsoleto, long idCategoria,
        java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Removes all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @throws SystemException if a system exception occurred
    */
    public void removeByCategoriaFornitore(boolean obsoleto, long idCategoria,
        java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param nome the nome
    * @param codice the codice
    * @return the number of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public int countByCategoriaFornitore(boolean obsoleto, long idCategoria,
        java.lang.String idFornitore, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the piantes where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @return the matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByFornitoreAttivaNomeCodiceLike(
        boolean obsoleto, java.lang.String idFornitore, boolean attiva,
        java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the piantes where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByFornitoreAttivaNomeCodiceLike(
        boolean obsoleto, java.lang.String idFornitore, boolean attiva,
        java.lang.String nome, java.lang.String codice, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the piantes where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByFornitoreAttivaNomeCodiceLike(
        boolean obsoleto, java.lang.String idFornitore, boolean attiva,
        java.lang.String nome, java.lang.String codice, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante findByFornitoreAttivaNomeCodiceLike_First(
        boolean obsoleto, java.lang.String idFornitore, boolean attiva,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByFornitoreAttivaNomeCodiceLike_First(
        boolean obsoleto, java.lang.String idFornitore, boolean attiva,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante findByFornitoreAttivaNomeCodiceLike_Last(
        boolean obsoleto, java.lang.String idFornitore, boolean attiva,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByFornitoreAttivaNomeCodiceLike_Last(
        boolean obsoleto, java.lang.String idFornitore, boolean attiva,
        java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param id the primary key of the current piante
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante[] findByFornitoreAttivaNomeCodiceLike_PrevAndNext(
        long id, boolean obsoleto, java.lang.String idFornitore,
        boolean attiva, java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Removes all the piantes where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @throws SystemException if a system exception occurred
    */
    public void removeByFornitoreAttivaNomeCodiceLike(boolean obsoleto,
        java.lang.String idFornitore, boolean attiva, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of piantes where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @return the number of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public int countByFornitoreAttivaNomeCodiceLike(boolean obsoleto,
        java.lang.String idFornitore, boolean attiva, java.lang.String nome,
        java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @return the matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByCategoriaFornitoreAttiva(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        boolean attiva, java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByCategoriaFornitoreAttiva(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        boolean attiva, java.lang.String nome, java.lang.String codice,
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findByCategoriaFornitoreAttiva(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        boolean attiva, java.lang.String nome, java.lang.String codice,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante findByCategoriaFornitoreAttiva_First(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        boolean attiva, java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByCategoriaFornitoreAttiva_First(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        boolean attiva, java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante findByCategoriaFornitoreAttiva_Last(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        boolean attiva, java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching piante, or <code>null</code> if a matching piante could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByCategoriaFornitoreAttiva_Last(
        boolean obsoleto, long idCategoria, java.lang.String idFornitore,
        boolean attiva, java.lang.String nome, java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param id the primary key of the current piante
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante[] findByCategoriaFornitoreAttiva_PrevAndNext(
        long id, boolean obsoleto, long idCategoria,
        java.lang.String idFornitore, boolean attiva, java.lang.String nome,
        java.lang.String codice,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Removes all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @throws SystemException if a system exception occurred
    */
    public void removeByCategoriaFornitoreAttiva(boolean obsoleto,
        long idCategoria, java.lang.String idFornitore, boolean attiva,
        java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
    *
    * @param obsoleto the obsoleto
    * @param idCategoria the id categoria
    * @param idFornitore the id fornitore
    * @param attiva the attiva
    * @param nome the nome
    * @param codice the codice
    * @return the number of matching piantes
    * @throws SystemException if a system exception occurred
    */
    public int countByCategoriaFornitoreAttiva(boolean obsoleto,
        long idCategoria, java.lang.String idFornitore, boolean attiva,
        java.lang.String nome, java.lang.String codice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the piante in the entity cache if it is enabled.
    *
    * @param piante the piante
    */
    public void cacheResult(it.bysoftware.ct.model.Piante piante);

    /**
    * Caches the piantes in the entity cache if it is enabled.
    *
    * @param piantes the piantes
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.Piante> piantes);

    /**
    * Creates a new piante with the primary key. Does not add the piante to the database.
    *
    * @param id the primary key for the new piante
    * @return the new piante
    */
    public it.bysoftware.ct.model.Piante create(long id);

    /**
    * Removes the piante with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the piante
    * @return the piante that was removed
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante remove(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    public it.bysoftware.ct.model.Piante updateImpl(
        it.bysoftware.ct.model.Piante piante)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the piante with the primary key or throws a {@link it.bysoftware.ct.NoSuchPianteException} if it could not be found.
    *
    * @param id the primary key of the piante
    * @return the piante
    * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante findByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPianteException;

    /**
    * Returns the piante with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the piante
    * @return the piante, or <code>null</code> if a piante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Piante fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the piantes.
    *
    * @return the piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the piantes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @return the range of piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the piantes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of piantes
    * @param end the upper bound of the range of piantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of piantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Piante> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the piantes from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of piantes.
    *
    * @return the number of piantes
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
