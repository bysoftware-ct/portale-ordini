package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.RigoDocumento;
import it.bysoftware.ct.service.RigoDocumentoLocalServiceUtil;

/**
 * @author Mario Torrisi
 * @generated
 */
public abstract class RigoDocumentoActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public RigoDocumentoActionableDynamicQuery() throws SystemException {
        setBaseLocalService(RigoDocumentoLocalServiceUtil.getService());
        setClass(RigoDocumento.class);

        setClassLoader(it.bysoftware.ct.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("primaryKey.anno");
    }
}
