package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.Ordine;

import java.util.List;

/**
 * The persistence utility for the ordine service. This utility wraps {@link OrdinePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see OrdinePersistence
 * @see OrdinePersistenceImpl
 * @generated
 */
public class OrdineUtil {
    private static OrdinePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Ordine ordine) {
        getPersistence().clearCache(ordine);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Ordine> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Ordine> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Ordine> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Ordine update(Ordine ordine) throws SystemException {
        return getPersistence().update(ordine);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Ordine update(Ordine ordine, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(ordine, serviceContext);
    }

    /**
    * Returns all the ordines where idCliente = &#63;.
    *
    * @param idCliente the id cliente
    * @return the matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByCodiceCliente(
        java.lang.String idCliente)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByCodiceCliente(idCliente);
    }

    /**
    * Returns a range of all the ordines where idCliente = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param idCliente the id cliente
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @return the range of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByCodiceCliente(
        java.lang.String idCliente, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByCodiceCliente(idCliente, start, end);
    }

    /**
    * Returns an ordered range of all the ordines where idCliente = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param idCliente the id cliente
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByCodiceCliente(
        java.lang.String idCliente, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCodiceCliente(idCliente, start, end, orderByComparator);
    }

    /**
    * Returns the first ordine in the ordered set where idCliente = &#63;.
    *
    * @param idCliente the id cliente
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine findByCodiceCliente_First(
        java.lang.String idCliente,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence()
                   .findByCodiceCliente_First(idCliente, orderByComparator);
    }

    /**
    * Returns the first ordine in the ordered set where idCliente = &#63;.
    *
    * @param idCliente the id cliente
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching ordine, or <code>null</code> if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine fetchByCodiceCliente_First(
        java.lang.String idCliente,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCodiceCliente_First(idCliente, orderByComparator);
    }

    /**
    * Returns the last ordine in the ordered set where idCliente = &#63;.
    *
    * @param idCliente the id cliente
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine findByCodiceCliente_Last(
        java.lang.String idCliente,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence()
                   .findByCodiceCliente_Last(idCliente, orderByComparator);
    }

    /**
    * Returns the last ordine in the ordered set where idCliente = &#63;.
    *
    * @param idCliente the id cliente
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching ordine, or <code>null</code> if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine fetchByCodiceCliente_Last(
        java.lang.String idCliente,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCodiceCliente_Last(idCliente, orderByComparator);
    }

    /**
    * Returns the ordines before and after the current ordine in the ordered set where idCliente = &#63;.
    *
    * @param id the primary key of the current ordine
    * @param idCliente the id cliente
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a ordine with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine[] findByCodiceCliente_PrevAndNext(
        long id, java.lang.String idCliente,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence()
                   .findByCodiceCliente_PrevAndNext(id, idCliente,
            orderByComparator);
    }

    /**
    * Removes all the ordines where idCliente = &#63; from the database.
    *
    * @param idCliente the id cliente
    * @throws SystemException if a system exception occurred
    */
    public static void removeByCodiceCliente(java.lang.String idCliente)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByCodiceCliente(idCliente);
    }

    /**
    * Returns the number of ordines where idCliente = &#63;.
    *
    * @param idCliente the id cliente
    * @return the number of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static int countByCodiceCliente(java.lang.String idCliente)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByCodiceCliente(idCliente);
    }

    /**
    * Returns all the ordines where stato = &#63;.
    *
    * @param stato the stato
    * @return the matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByStato(
        int stato) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByStato(stato);
    }

    /**
    * Returns a range of all the ordines where stato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param stato the stato
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @return the range of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByStato(
        int stato, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByStato(stato, start, end);
    }

    /**
    * Returns an ordered range of all the ordines where stato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param stato the stato
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByStato(
        int stato, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByStato(stato, start, end, orderByComparator);
    }

    /**
    * Returns the first ordine in the ordered set where stato = &#63;.
    *
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine findByStato_First(int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence().findByStato_First(stato, orderByComparator);
    }

    /**
    * Returns the first ordine in the ordered set where stato = &#63;.
    *
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching ordine, or <code>null</code> if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine fetchByStato_First(int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByStato_First(stato, orderByComparator);
    }

    /**
    * Returns the last ordine in the ordered set where stato = &#63;.
    *
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine findByStato_Last(int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence().findByStato_Last(stato, orderByComparator);
    }

    /**
    * Returns the last ordine in the ordered set where stato = &#63;.
    *
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching ordine, or <code>null</code> if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine fetchByStato_Last(int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByStato_Last(stato, orderByComparator);
    }

    /**
    * Returns the ordines before and after the current ordine in the ordered set where stato = &#63;.
    *
    * @param id the primary key of the current ordine
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a ordine with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine[] findByStato_PrevAndNext(
        long id, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence()
                   .findByStato_PrevAndNext(id, stato, orderByComparator);
    }

    /**
    * Removes all the ordines where stato = &#63; from the database.
    *
    * @param stato the stato
    * @throws SystemException if a system exception occurred
    */
    public static void removeByStato(int stato)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByStato(stato);
    }

    /**
    * Returns the number of ordines where stato = &#63;.
    *
    * @param stato the stato
    * @return the number of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static int countByStato(int stato)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByStato(stato);
    }

    /**
    * Returns all the ordines where dataConsegna = &#63; and stato = &#63;.
    *
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @return the matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByConsegnaStato(
        java.util.Date dataConsegna, int stato)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByConsegnaStato(dataConsegna, stato);
    }

    /**
    * Returns a range of all the ordines where dataConsegna = &#63; and stato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @return the range of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByConsegnaStato(
        java.util.Date dataConsegna, int stato, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByConsegnaStato(dataConsegna, stato, start, end);
    }

    /**
    * Returns an ordered range of all the ordines where dataConsegna = &#63; and stato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByConsegnaStato(
        java.util.Date dataConsegna, int stato, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByConsegnaStato(dataConsegna, stato, start, end,
            orderByComparator);
    }

    /**
    * Returns the first ordine in the ordered set where dataConsegna = &#63; and stato = &#63;.
    *
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine findByConsegnaStato_First(
        java.util.Date dataConsegna, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence()
                   .findByConsegnaStato_First(dataConsegna, stato,
            orderByComparator);
    }

    /**
    * Returns the first ordine in the ordered set where dataConsegna = &#63; and stato = &#63;.
    *
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching ordine, or <code>null</code> if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine fetchByConsegnaStato_First(
        java.util.Date dataConsegna, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByConsegnaStato_First(dataConsegna, stato,
            orderByComparator);
    }

    /**
    * Returns the last ordine in the ordered set where dataConsegna = &#63; and stato = &#63;.
    *
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine findByConsegnaStato_Last(
        java.util.Date dataConsegna, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence()
                   .findByConsegnaStato_Last(dataConsegna, stato,
            orderByComparator);
    }

    /**
    * Returns the last ordine in the ordered set where dataConsegna = &#63; and stato = &#63;.
    *
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching ordine, or <code>null</code> if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine fetchByConsegnaStato_Last(
        java.util.Date dataConsegna, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByConsegnaStato_Last(dataConsegna, stato,
            orderByComparator);
    }

    /**
    * Returns the ordines before and after the current ordine in the ordered set where dataConsegna = &#63; and stato = &#63;.
    *
    * @param id the primary key of the current ordine
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a ordine with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine[] findByConsegnaStato_PrevAndNext(
        long id, java.util.Date dataConsegna, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence()
                   .findByConsegnaStato_PrevAndNext(id, dataConsegna, stato,
            orderByComparator);
    }

    /**
    * Returns all the ordines where dataConsegna = &#63; and stato = any &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dataConsegna the data consegna
    * @param statos the statos
    * @return the matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByConsegnaStato(
        java.util.Date dataConsegna, int[] statos)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByConsegnaStato(dataConsegna, statos);
    }

    /**
    * Returns a range of all the ordines where dataConsegna = &#63; and stato = any &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dataConsegna the data consegna
    * @param statos the statos
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @return the range of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByConsegnaStato(
        java.util.Date dataConsegna, int[] statos, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByConsegnaStato(dataConsegna, statos, start, end);
    }

    /**
    * Returns an ordered range of all the ordines where dataConsegna = &#63; and stato = any &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dataConsegna the data consegna
    * @param statos the statos
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByConsegnaStato(
        java.util.Date dataConsegna, int[] statos, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByConsegnaStato(dataConsegna, statos, start, end,
            orderByComparator);
    }

    /**
    * Removes all the ordines where dataConsegna = &#63; and stato = &#63; from the database.
    *
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @throws SystemException if a system exception occurred
    */
    public static void removeByConsegnaStato(java.util.Date dataConsegna,
        int stato) throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByConsegnaStato(dataConsegna, stato);
    }

    /**
    * Returns the number of ordines where dataConsegna = &#63; and stato = &#63;.
    *
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @return the number of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static int countByConsegnaStato(java.util.Date dataConsegna,
        int stato) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByConsegnaStato(dataConsegna, stato);
    }

    /**
    * Returns the number of ordines where dataConsegna = &#63; and stato = any &#63;.
    *
    * @param dataConsegna the data consegna
    * @param statos the statos
    * @return the number of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static int countByConsegnaStato(java.util.Date dataConsegna,
        int[] statos)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByConsegnaStato(dataConsegna, statos);
    }

    /**
    * Returns all the ordines where idCliente = &#63; and stato = &#63;.
    *
    * @param idCliente the id cliente
    * @param stato the stato
    * @return the matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByCodiceClienteStato(
        java.lang.String idCliente, int stato)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByCodiceClienteStato(idCliente, stato);
    }

    /**
    * Returns a range of all the ordines where idCliente = &#63; and stato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param idCliente the id cliente
    * @param stato the stato
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @return the range of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByCodiceClienteStato(
        java.lang.String idCliente, int stato, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCodiceClienteStato(idCliente, stato, start, end);
    }

    /**
    * Returns an ordered range of all the ordines where idCliente = &#63; and stato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param idCliente the id cliente
    * @param stato the stato
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByCodiceClienteStato(
        java.lang.String idCliente, int stato, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCodiceClienteStato(idCliente, stato, start, end,
            orderByComparator);
    }

    /**
    * Returns the first ordine in the ordered set where idCliente = &#63; and stato = &#63;.
    *
    * @param idCliente the id cliente
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine findByCodiceClienteStato_First(
        java.lang.String idCliente, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence()
                   .findByCodiceClienteStato_First(idCliente, stato,
            orderByComparator);
    }

    /**
    * Returns the first ordine in the ordered set where idCliente = &#63; and stato = &#63;.
    *
    * @param idCliente the id cliente
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching ordine, or <code>null</code> if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine fetchByCodiceClienteStato_First(
        java.lang.String idCliente, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCodiceClienteStato_First(idCliente, stato,
            orderByComparator);
    }

    /**
    * Returns the last ordine in the ordered set where idCliente = &#63; and stato = &#63;.
    *
    * @param idCliente the id cliente
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine findByCodiceClienteStato_Last(
        java.lang.String idCliente, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence()
                   .findByCodiceClienteStato_Last(idCliente, stato,
            orderByComparator);
    }

    /**
    * Returns the last ordine in the ordered set where idCliente = &#63; and stato = &#63;.
    *
    * @param idCliente the id cliente
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching ordine, or <code>null</code> if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine fetchByCodiceClienteStato_Last(
        java.lang.String idCliente, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCodiceClienteStato_Last(idCliente, stato,
            orderByComparator);
    }

    /**
    * Returns the ordines before and after the current ordine in the ordered set where idCliente = &#63; and stato = &#63;.
    *
    * @param id the primary key of the current ordine
    * @param idCliente the id cliente
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a ordine with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine[] findByCodiceClienteStato_PrevAndNext(
        long id, java.lang.String idCliente, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence()
                   .findByCodiceClienteStato_PrevAndNext(id, idCliente, stato,
            orderByComparator);
    }

    /**
    * Removes all the ordines where idCliente = &#63; and stato = &#63; from the database.
    *
    * @param idCliente the id cliente
    * @param stato the stato
    * @throws SystemException if a system exception occurred
    */
    public static void removeByCodiceClienteStato(java.lang.String idCliente,
        int stato) throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByCodiceClienteStato(idCliente, stato);
    }

    /**
    * Returns the number of ordines where idCliente = &#63; and stato = &#63;.
    *
    * @param idCliente the id cliente
    * @param stato the stato
    * @return the number of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static int countByCodiceClienteStato(java.lang.String idCliente,
        int stato) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByCodiceClienteStato(idCliente, stato);
    }

    /**
    * Returns all the ordines where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63;.
    *
    * @param dataInserimento the data inserimento
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @return the matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByInserimentoConsegnaStato(
        java.util.Date dataInserimento, java.util.Date dataConsegna, int stato)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByInserimentoConsegnaStato(dataInserimento,
            dataConsegna, stato);
    }

    /**
    * Returns a range of all the ordines where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dataInserimento the data inserimento
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @return the range of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByInserimentoConsegnaStato(
        java.util.Date dataInserimento, java.util.Date dataConsegna, int stato,
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByInserimentoConsegnaStato(dataInserimento,
            dataConsegna, stato, start, end);
    }

    /**
    * Returns an ordered range of all the ordines where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dataInserimento the data inserimento
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByInserimentoConsegnaStato(
        java.util.Date dataInserimento, java.util.Date dataConsegna, int stato,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByInserimentoConsegnaStato(dataInserimento,
            dataConsegna, stato, start, end, orderByComparator);
    }

    /**
    * Returns the first ordine in the ordered set where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63;.
    *
    * @param dataInserimento the data inserimento
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine findByInserimentoConsegnaStato_First(
        java.util.Date dataInserimento, java.util.Date dataConsegna, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence()
                   .findByInserimentoConsegnaStato_First(dataInserimento,
            dataConsegna, stato, orderByComparator);
    }

    /**
    * Returns the first ordine in the ordered set where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63;.
    *
    * @param dataInserimento the data inserimento
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching ordine, or <code>null</code> if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine fetchByInserimentoConsegnaStato_First(
        java.util.Date dataInserimento, java.util.Date dataConsegna, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByInserimentoConsegnaStato_First(dataInserimento,
            dataConsegna, stato, orderByComparator);
    }

    /**
    * Returns the last ordine in the ordered set where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63;.
    *
    * @param dataInserimento the data inserimento
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine findByInserimentoConsegnaStato_Last(
        java.util.Date dataInserimento, java.util.Date dataConsegna, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence()
                   .findByInserimentoConsegnaStato_Last(dataInserimento,
            dataConsegna, stato, orderByComparator);
    }

    /**
    * Returns the last ordine in the ordered set where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63;.
    *
    * @param dataInserimento the data inserimento
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching ordine, or <code>null</code> if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine fetchByInserimentoConsegnaStato_Last(
        java.util.Date dataInserimento, java.util.Date dataConsegna, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByInserimentoConsegnaStato_Last(dataInserimento,
            dataConsegna, stato, orderByComparator);
    }

    /**
    * Returns the ordines before and after the current ordine in the ordered set where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63;.
    *
    * @param id the primary key of the current ordine
    * @param dataInserimento the data inserimento
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a ordine with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine[] findByInserimentoConsegnaStato_PrevAndNext(
        long id, java.util.Date dataInserimento, java.util.Date dataConsegna,
        int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence()
                   .findByInserimentoConsegnaStato_PrevAndNext(id,
            dataInserimento, dataConsegna, stato, orderByComparator);
    }

    /**
    * Removes all the ordines where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63; from the database.
    *
    * @param dataInserimento the data inserimento
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @throws SystemException if a system exception occurred
    */
    public static void removeByInserimentoConsegnaStato(
        java.util.Date dataInserimento, java.util.Date dataConsegna, int stato)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByInserimentoConsegnaStato(dataInserimento, dataConsegna,
            stato);
    }

    /**
    * Returns the number of ordines where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63;.
    *
    * @param dataInserimento the data inserimento
    * @param dataConsegna the data consegna
    * @param stato the stato
    * @return the number of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static int countByInserimentoConsegnaStato(
        java.util.Date dataInserimento, java.util.Date dataConsegna, int stato)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByInserimentoConsegnaStato(dataInserimento,
            dataConsegna, stato);
    }

    /**
    * Returns all the ordines where anno = &#63; and centro = &#63;.
    *
    * @param anno the anno
    * @param centro the centro
    * @return the matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByAnnoCentro(
        int anno, java.lang.String centro)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByAnnoCentro(anno, centro);
    }

    /**
    * Returns a range of all the ordines where anno = &#63; and centro = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param anno the anno
    * @param centro the centro
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @return the range of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByAnnoCentro(
        int anno, java.lang.String centro, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByAnnoCentro(anno, centro, start, end);
    }

    /**
    * Returns an ordered range of all the ordines where anno = &#63; and centro = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param anno the anno
    * @param centro the centro
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findByAnnoCentro(
        int anno, java.lang.String centro, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByAnnoCentro(anno, centro, start, end, orderByComparator);
    }

    /**
    * Returns the first ordine in the ordered set where anno = &#63; and centro = &#63;.
    *
    * @param anno the anno
    * @param centro the centro
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine findByAnnoCentro_First(
        int anno, java.lang.String centro,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence()
                   .findByAnnoCentro_First(anno, centro, orderByComparator);
    }

    /**
    * Returns the first ordine in the ordered set where anno = &#63; and centro = &#63;.
    *
    * @param anno the anno
    * @param centro the centro
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching ordine, or <code>null</code> if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine fetchByAnnoCentro_First(
        int anno, java.lang.String centro,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByAnnoCentro_First(anno, centro, orderByComparator);
    }

    /**
    * Returns the last ordine in the ordered set where anno = &#63; and centro = &#63;.
    *
    * @param anno the anno
    * @param centro the centro
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine findByAnnoCentro_Last(
        int anno, java.lang.String centro,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence()
                   .findByAnnoCentro_Last(anno, centro, orderByComparator);
    }

    /**
    * Returns the last ordine in the ordered set where anno = &#63; and centro = &#63;.
    *
    * @param anno the anno
    * @param centro the centro
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching ordine, or <code>null</code> if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine fetchByAnnoCentro_Last(
        int anno, java.lang.String centro,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByAnnoCentro_Last(anno, centro, orderByComparator);
    }

    /**
    * Returns the ordines before and after the current ordine in the ordered set where anno = &#63; and centro = &#63;.
    *
    * @param id the primary key of the current ordine
    * @param anno the anno
    * @param centro the centro
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a ordine with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine[] findByAnnoCentro_PrevAndNext(
        long id, int anno, java.lang.String centro,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence()
                   .findByAnnoCentro_PrevAndNext(id, anno, centro,
            orderByComparator);
    }

    /**
    * Removes all the ordines where anno = &#63; and centro = &#63; from the database.
    *
    * @param anno the anno
    * @param centro the centro
    * @throws SystemException if a system exception occurred
    */
    public static void removeByAnnoCentro(int anno, java.lang.String centro)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByAnnoCentro(anno, centro);
    }

    /**
    * Returns the number of ordines where anno = &#63; and centro = &#63;.
    *
    * @param anno the anno
    * @param centro the centro
    * @return the number of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static int countByAnnoCentro(int anno, java.lang.String centro)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByAnnoCentro(anno, centro);
    }

    /**
    * Returns the ordine where anno = &#63; and numero = &#63; and centro = &#63; or throws a {@link it.bysoftware.ct.NoSuchOrdineException} if it could not be found.
    *
    * @param anno the anno
    * @param numero the numero
    * @param centro the centro
    * @return the matching ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine findByAnnoNumeroCentro(
        int anno, int numero, java.lang.String centro)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence().findByAnnoNumeroCentro(anno, numero, centro);
    }

    /**
    * Returns the ordine where anno = &#63; and numero = &#63; and centro = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param anno the anno
    * @param numero the numero
    * @param centro the centro
    * @return the matching ordine, or <code>null</code> if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine fetchByAnnoNumeroCentro(
        int anno, int numero, java.lang.String centro)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByAnnoNumeroCentro(anno, numero, centro);
    }

    /**
    * Returns the ordine where anno = &#63; and numero = &#63; and centro = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param anno the anno
    * @param numero the numero
    * @param centro the centro
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching ordine, or <code>null</code> if a matching ordine could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine fetchByAnnoNumeroCentro(
        int anno, int numero, java.lang.String centro, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByAnnoNumeroCentro(anno, numero, centro,
            retrieveFromCache);
    }

    /**
    * Removes the ordine where anno = &#63; and numero = &#63; and centro = &#63; from the database.
    *
    * @param anno the anno
    * @param numero the numero
    * @param centro the centro
    * @return the ordine that was removed
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine removeByAnnoNumeroCentro(
        int anno, int numero, java.lang.String centro)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence().removeByAnnoNumeroCentro(anno, numero, centro);
    }

    /**
    * Returns the number of ordines where anno = &#63; and numero = &#63; and centro = &#63;.
    *
    * @param anno the anno
    * @param numero the numero
    * @param centro the centro
    * @return the number of matching ordines
    * @throws SystemException if a system exception occurred
    */
    public static int countByAnnoNumeroCentro(int anno, int numero,
        java.lang.String centro)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByAnnoNumeroCentro(anno, numero, centro);
    }

    /**
    * Caches the ordine in the entity cache if it is enabled.
    *
    * @param ordine the ordine
    */
    public static void cacheResult(it.bysoftware.ct.model.Ordine ordine) {
        getPersistence().cacheResult(ordine);
    }

    /**
    * Caches the ordines in the entity cache if it is enabled.
    *
    * @param ordines the ordines
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.Ordine> ordines) {
        getPersistence().cacheResult(ordines);
    }

    /**
    * Creates a new ordine with the primary key. Does not add the ordine to the database.
    *
    * @param id the primary key for the new ordine
    * @return the new ordine
    */
    public static it.bysoftware.ct.model.Ordine create(long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the ordine with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the ordine
    * @return the ordine that was removed
    * @throws it.bysoftware.ct.NoSuchOrdineException if a ordine with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine remove(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence().remove(id);
    }

    public static it.bysoftware.ct.model.Ordine updateImpl(
        it.bysoftware.ct.model.Ordine ordine)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(ordine);
    }

    /**
    * Returns the ordine with the primary key or throws a {@link it.bysoftware.ct.NoSuchOrdineException} if it could not be found.
    *
    * @param id the primary key of the ordine
    * @return the ordine
    * @throws it.bysoftware.ct.NoSuchOrdineException if a ordine with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine findByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchOrdineException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the ordine with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the ordine
    * @return the ordine, or <code>null</code> if a ordine with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Ordine fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the ordines.
    *
    * @return the ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the ordines.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @return the range of ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the ordines.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of ordines
    * @param end the upper bound of the range of ordines (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of ordines
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Ordine> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the ordines from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of ordines.
    *
    * @return the number of ordines
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static OrdinePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (OrdinePersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    OrdinePersistence.class.getName());

            ReferenceRegistry.registerReference(OrdineUtil.class, "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(OrdinePersistence persistence) {
    }
}
