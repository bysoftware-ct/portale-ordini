package it.bysoftware.ct.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for AnagraficheClientiFornitori. This utility wraps
 * {@link it.bysoftware.ct.service.impl.AnagraficheClientiFornitoriLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Mario Torrisi
 * @see AnagraficheClientiFornitoriLocalService
 * @see it.bysoftware.ct.service.base.AnagraficheClientiFornitoriLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.impl.AnagraficheClientiFornitoriLocalServiceImpl
 * @generated
 */
public class AnagraficheClientiFornitoriLocalServiceUtil {
    private static AnagraficheClientiFornitoriLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link it.bysoftware.ct.service.impl.AnagraficheClientiFornitoriLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the anagrafiche clienti fornitori to the database. Also notifies the appropriate model listeners.
    *
    * @param anagraficheClientiFornitori the anagrafiche clienti fornitori
    * @return the anagrafiche clienti fornitori that was added
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.AnagraficheClientiFornitori addAnagraficheClientiFornitori(
        it.bysoftware.ct.model.AnagraficheClientiFornitori anagraficheClientiFornitori)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addAnagraficheClientiFornitori(anagraficheClientiFornitori);
    }

    /**
    * Creates a new anagrafiche clienti fornitori with the primary key. Does not add the anagrafiche clienti fornitori to the database.
    *
    * @param codiceAnagrafica the primary key for the new anagrafiche clienti fornitori
    * @return the new anagrafiche clienti fornitori
    */
    public static it.bysoftware.ct.model.AnagraficheClientiFornitori createAnagraficheClientiFornitori(
        java.lang.String codiceAnagrafica) {
        return getService().createAnagraficheClientiFornitori(codiceAnagrafica);
    }

    /**
    * Deletes the anagrafiche clienti fornitori with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceAnagrafica the primary key of the anagrafiche clienti fornitori
    * @return the anagrafiche clienti fornitori that was removed
    * @throws PortalException if a anagrafiche clienti fornitori with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.AnagraficheClientiFornitori deleteAnagraficheClientiFornitori(
        java.lang.String codiceAnagrafica)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteAnagraficheClientiFornitori(codiceAnagrafica);
    }

    /**
    * Deletes the anagrafiche clienti fornitori from the database. Also notifies the appropriate model listeners.
    *
    * @param anagraficheClientiFornitori the anagrafiche clienti fornitori
    * @return the anagrafiche clienti fornitori that was removed
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.AnagraficheClientiFornitori deleteAnagraficheClientiFornitori(
        it.bysoftware.ct.model.AnagraficheClientiFornitori anagraficheClientiFornitori)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .deleteAnagraficheClientiFornitori(anagraficheClientiFornitori);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficheClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficheClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static it.bysoftware.ct.model.AnagraficheClientiFornitori fetchAnagraficheClientiFornitori(
        java.lang.String codiceAnagrafica)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchAnagraficheClientiFornitori(codiceAnagrafica);
    }

    /**
    * Returns the anagrafiche clienti fornitori with the primary key.
    *
    * @param codiceAnagrafica the primary key of the anagrafiche clienti fornitori
    * @return the anagrafiche clienti fornitori
    * @throws PortalException if a anagrafiche clienti fornitori with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.AnagraficheClientiFornitori getAnagraficheClientiFornitori(
        java.lang.String codiceAnagrafica)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getAnagraficheClientiFornitori(codiceAnagrafica);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the anagrafiche clienti fornitoris.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficheClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of anagrafiche clienti fornitoris
    * @param end the upper bound of the range of anagrafiche clienti fornitoris (not inclusive)
    * @return the range of anagrafiche clienti fornitoris
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.AnagraficheClientiFornitori> getAnagraficheClientiFornitoris(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getAnagraficheClientiFornitoris(start, end);
    }

    /**
    * Returns the number of anagrafiche clienti fornitoris.
    *
    * @return the number of anagrafiche clienti fornitoris
    * @throws SystemException if a system exception occurred
    */
    public static int getAnagraficheClientiFornitorisCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getAnagraficheClientiFornitorisCount();
    }

    /**
    * Updates the anagrafiche clienti fornitori in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param anagraficheClientiFornitori the anagrafiche clienti fornitori
    * @return the anagrafiche clienti fornitori that was updated
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.AnagraficheClientiFornitori updateAnagraficheClientiFornitori(
        it.bysoftware.ct.model.AnagraficheClientiFornitori anagraficheClientiFornitori)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateAnagraficheClientiFornitori(anagraficheClientiFornitori);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.util.List<it.bysoftware.ct.model.AnagraficheClientiFornitori> findCustomers() {
        return getService().findCustomers();
    }

    public static java.util.List<it.bysoftware.ct.model.AnagraficheClientiFornitori> findCustomersInCategory(
        java.lang.String category) {
        return getService().findCustomersInCategory(category);
    }

    public static java.util.List<it.bysoftware.ct.model.AnagraficheClientiFornitori> findAllSuppliers() {
        return getService().findAllSuppliers();
    }

    public static java.util.List<it.bysoftware.ct.model.AnagraficheClientiFornitori> findSuppliers() {
        return getService().findSuppliers();
    }

    public static java.util.List<it.bysoftware.ct.model.AnagraficheClientiFornitori> findAssociates() {
        return getService().findAssociates();
    }

    public static java.util.List<it.bysoftware.ct.model.AnagraficheClientiFornitori> getCustomers(
        java.lang.String customerCode, java.lang.String businessName,
        java.lang.String vatCode, java.lang.String country,
        java.lang.String city, boolean andSearch, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .getCustomers(customerCode, businessName, vatCode, country,
            city, andSearch, start, end, orderByComparator);
    }

    public static int getCustomersCount(java.lang.String customerCode,
        java.lang.String businessName, java.lang.String vatCode,
        java.lang.String country, java.lang.String city, boolean andSearch)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .getCustomersCount(customerCode, businessName, vatCode,
            country, city, andSearch);
    }

    public static void clearService() {
        _service = null;
    }

    public static AnagraficheClientiFornitoriLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    AnagraficheClientiFornitoriLocalService.class.getName());

            if (invokableLocalService instanceof AnagraficheClientiFornitoriLocalService) {
                _service = (AnagraficheClientiFornitoriLocalService) invokableLocalService;
            } else {
                _service = new AnagraficheClientiFornitoriLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(AnagraficheClientiFornitoriLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(AnagraficheClientiFornitoriLocalService service) {
    }
}
