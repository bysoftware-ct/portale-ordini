package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link RubricaService}.
 *
 * @author Mario Torrisi
 * @see RubricaService
 * @generated
 */
public class RubricaServiceWrapper implements RubricaService,
    ServiceWrapper<RubricaService> {
    private RubricaService _rubricaService;

    public RubricaServiceWrapper(RubricaService rubricaService) {
        _rubricaService = rubricaService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _rubricaService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _rubricaService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _rubricaService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public RubricaService getWrappedRubricaService() {
        return _rubricaService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedRubricaService(RubricaService rubricaService) {
        _rubricaService = rubricaService;
    }

    @Override
    public RubricaService getWrappedService() {
        return _rubricaService;
    }

    @Override
    public void setWrappedService(RubricaService rubricaService) {
        _rubricaService = rubricaService;
    }
}
