package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.ArticoloSocio;

/**
 * The persistence interface for the articolo socio service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see ArticoloSocioPersistenceImpl
 * @see ArticoloSocioUtil
 * @generated
 */
public interface ArticoloSocioPersistence extends BasePersistence<ArticoloSocio> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link ArticoloSocioUtil} to access the articolo socio persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the articolo socios where associato = &#63; and codiceArticolo = &#63;.
    *
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @return the matching articolo socios
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.ArticoloSocio> findBySocioArticolo(
        java.lang.String associato, java.lang.String codiceArticolo)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the articolo socios where associato = &#63; and codiceArticolo = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @param start the lower bound of the range of articolo socios
    * @param end the upper bound of the range of articolo socios (not inclusive)
    * @return the range of matching articolo socios
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.ArticoloSocio> findBySocioArticolo(
        java.lang.String associato, java.lang.String codiceArticolo, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the articolo socios where associato = &#63; and codiceArticolo = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @param start the lower bound of the range of articolo socios
    * @param end the upper bound of the range of articolo socios (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching articolo socios
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.ArticoloSocio> findBySocioArticolo(
        java.lang.String associato, java.lang.String codiceArticolo, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first articolo socio in the ordered set where associato = &#63; and codiceArticolo = &#63;.
    *
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching articolo socio
    * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ArticoloSocio findBySocioArticolo_First(
        java.lang.String associato, java.lang.String codiceArticolo,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException;

    /**
    * Returns the first articolo socio in the ordered set where associato = &#63; and codiceArticolo = &#63;.
    *
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching articolo socio, or <code>null</code> if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ArticoloSocio fetchBySocioArticolo_First(
        java.lang.String associato, java.lang.String codiceArticolo,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last articolo socio in the ordered set where associato = &#63; and codiceArticolo = &#63;.
    *
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching articolo socio
    * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ArticoloSocio findBySocioArticolo_Last(
        java.lang.String associato, java.lang.String codiceArticolo,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException;

    /**
    * Returns the last articolo socio in the ordered set where associato = &#63; and codiceArticolo = &#63;.
    *
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching articolo socio, or <code>null</code> if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ArticoloSocio fetchBySocioArticolo_Last(
        java.lang.String associato, java.lang.String codiceArticolo,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the articolo socios before and after the current articolo socio in the ordered set where associato = &#63; and codiceArticolo = &#63;.
    *
    * @param articoloSocioPK the primary key of the current articolo socio
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next articolo socio
    * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a articolo socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ArticoloSocio[] findBySocioArticolo_PrevAndNext(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK,
        java.lang.String associato, java.lang.String codiceArticolo,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException;

    /**
    * Removes all the articolo socios where associato = &#63; and codiceArticolo = &#63; from the database.
    *
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @throws SystemException if a system exception occurred
    */
    public void removeBySocioArticolo(java.lang.String associato,
        java.lang.String codiceArticolo)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of articolo socios where associato = &#63; and codiceArticolo = &#63;.
    *
    * @param associato the associato
    * @param codiceArticolo the codice articolo
    * @return the number of matching articolo socios
    * @throws SystemException if a system exception occurred
    */
    public int countBySocioArticolo(java.lang.String associato,
        java.lang.String codiceArticolo)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the articolo socios where associato = &#63;.
    *
    * @param associato the associato
    * @return the matching articolo socios
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.ArticoloSocio> findBySocio(
        java.lang.String associato)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the articolo socios where associato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param associato the associato
    * @param start the lower bound of the range of articolo socios
    * @param end the upper bound of the range of articolo socios (not inclusive)
    * @return the range of matching articolo socios
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.ArticoloSocio> findBySocio(
        java.lang.String associato, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the articolo socios where associato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param associato the associato
    * @param start the lower bound of the range of articolo socios
    * @param end the upper bound of the range of articolo socios (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching articolo socios
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.ArticoloSocio> findBySocio(
        java.lang.String associato, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first articolo socio in the ordered set where associato = &#63;.
    *
    * @param associato the associato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching articolo socio
    * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ArticoloSocio findBySocio_First(
        java.lang.String associato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException;

    /**
    * Returns the first articolo socio in the ordered set where associato = &#63;.
    *
    * @param associato the associato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching articolo socio, or <code>null</code> if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ArticoloSocio fetchBySocio_First(
        java.lang.String associato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last articolo socio in the ordered set where associato = &#63;.
    *
    * @param associato the associato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching articolo socio
    * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ArticoloSocio findBySocio_Last(
        java.lang.String associato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException;

    /**
    * Returns the last articolo socio in the ordered set where associato = &#63;.
    *
    * @param associato the associato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching articolo socio, or <code>null</code> if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ArticoloSocio fetchBySocio_Last(
        java.lang.String associato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the articolo socios before and after the current articolo socio in the ordered set where associato = &#63;.
    *
    * @param articoloSocioPK the primary key of the current articolo socio
    * @param associato the associato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next articolo socio
    * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a articolo socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ArticoloSocio[] findBySocio_PrevAndNext(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK,
        java.lang.String associato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException;

    /**
    * Removes all the articolo socios where associato = &#63; from the database.
    *
    * @param associato the associato
    * @throws SystemException if a system exception occurred
    */
    public void removeBySocio(java.lang.String associato)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of articolo socios where associato = &#63;.
    *
    * @param associato the associato
    * @return the number of matching articolo socios
    * @throws SystemException if a system exception occurred
    */
    public int countBySocio(java.lang.String associato)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the articolo socio where associato = &#63; and codiceArticoloSocio = &#63; and codiceVarianteSocio = &#63; or throws a {@link it.bysoftware.ct.NoSuchArticoloSocioException} if it could not be found.
    *
    * @param associato the associato
    * @param codiceArticoloSocio the codice articolo socio
    * @param codiceVarianteSocio the codice variante socio
    * @return the matching articolo socio
    * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ArticoloSocio findByArticoloVarianteSocio(
        java.lang.String associato, java.lang.String codiceArticoloSocio,
        java.lang.String codiceVarianteSocio)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException;

    /**
    * Returns the articolo socio where associato = &#63; and codiceArticoloSocio = &#63; and codiceVarianteSocio = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param associato the associato
    * @param codiceArticoloSocio the codice articolo socio
    * @param codiceVarianteSocio the codice variante socio
    * @return the matching articolo socio, or <code>null</code> if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ArticoloSocio fetchByArticoloVarianteSocio(
        java.lang.String associato, java.lang.String codiceArticoloSocio,
        java.lang.String codiceVarianteSocio)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the articolo socio where associato = &#63; and codiceArticoloSocio = &#63; and codiceVarianteSocio = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param associato the associato
    * @param codiceArticoloSocio the codice articolo socio
    * @param codiceVarianteSocio the codice variante socio
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching articolo socio, or <code>null</code> if a matching articolo socio could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ArticoloSocio fetchByArticoloVarianteSocio(
        java.lang.String associato, java.lang.String codiceArticoloSocio,
        java.lang.String codiceVarianteSocio, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the articolo socio where associato = &#63; and codiceArticoloSocio = &#63; and codiceVarianteSocio = &#63; from the database.
    *
    * @param associato the associato
    * @param codiceArticoloSocio the codice articolo socio
    * @param codiceVarianteSocio the codice variante socio
    * @return the articolo socio that was removed
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ArticoloSocio removeByArticoloVarianteSocio(
        java.lang.String associato, java.lang.String codiceArticoloSocio,
        java.lang.String codiceVarianteSocio)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException;

    /**
    * Returns the number of articolo socios where associato = &#63; and codiceArticoloSocio = &#63; and codiceVarianteSocio = &#63;.
    *
    * @param associato the associato
    * @param codiceArticoloSocio the codice articolo socio
    * @param codiceVarianteSocio the codice variante socio
    * @return the number of matching articolo socios
    * @throws SystemException if a system exception occurred
    */
    public int countByArticoloVarianteSocio(java.lang.String associato,
        java.lang.String codiceArticoloSocio,
        java.lang.String codiceVarianteSocio)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the articolo socio in the entity cache if it is enabled.
    *
    * @param articoloSocio the articolo socio
    */
    public void cacheResult(it.bysoftware.ct.model.ArticoloSocio articoloSocio);

    /**
    * Caches the articolo socios in the entity cache if it is enabled.
    *
    * @param articoloSocios the articolo socios
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.ArticoloSocio> articoloSocios);

    /**
    * Creates a new articolo socio with the primary key. Does not add the articolo socio to the database.
    *
    * @param articoloSocioPK the primary key for the new articolo socio
    * @return the new articolo socio
    */
    public it.bysoftware.ct.model.ArticoloSocio create(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK);

    /**
    * Removes the articolo socio with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param articoloSocioPK the primary key of the articolo socio
    * @return the articolo socio that was removed
    * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a articolo socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ArticoloSocio remove(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException;

    public it.bysoftware.ct.model.ArticoloSocio updateImpl(
        it.bysoftware.ct.model.ArticoloSocio articoloSocio)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the articolo socio with the primary key or throws a {@link it.bysoftware.ct.NoSuchArticoloSocioException} if it could not be found.
    *
    * @param articoloSocioPK the primary key of the articolo socio
    * @return the articolo socio
    * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a articolo socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ArticoloSocio findByPrimaryKey(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchArticoloSocioException;

    /**
    * Returns the articolo socio with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param articoloSocioPK the primary key of the articolo socio
    * @return the articolo socio, or <code>null</code> if a articolo socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ArticoloSocio fetchByPrimaryKey(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the articolo socios.
    *
    * @return the articolo socios
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.ArticoloSocio> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the articolo socios.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of articolo socios
    * @param end the upper bound of the range of articolo socios (not inclusive)
    * @return the range of articolo socios
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.ArticoloSocio> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the articolo socios.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of articolo socios
    * @param end the upper bound of the range of articolo socios (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of articolo socios
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.ArticoloSocio> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the articolo socios from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of articolo socios.
    *
    * @return the number of articolo socios
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
