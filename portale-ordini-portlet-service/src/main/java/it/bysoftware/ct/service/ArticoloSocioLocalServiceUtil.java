package it.bysoftware.ct.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for ArticoloSocio. This utility wraps
 * {@link it.bysoftware.ct.service.impl.ArticoloSocioLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Mario Torrisi
 * @see ArticoloSocioLocalService
 * @see it.bysoftware.ct.service.base.ArticoloSocioLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.impl.ArticoloSocioLocalServiceImpl
 * @generated
 */
public class ArticoloSocioLocalServiceUtil {
    private static ArticoloSocioLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link it.bysoftware.ct.service.impl.ArticoloSocioLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the articolo socio to the database. Also notifies the appropriate model listeners.
    *
    * @param articoloSocio the articolo socio
    * @return the articolo socio that was added
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio addArticoloSocio(
        it.bysoftware.ct.model.ArticoloSocio articoloSocio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addArticoloSocio(articoloSocio);
    }

    /**
    * Creates a new articolo socio with the primary key. Does not add the articolo socio to the database.
    *
    * @param articoloSocioPK the primary key for the new articolo socio
    * @return the new articolo socio
    */
    public static it.bysoftware.ct.model.ArticoloSocio createArticoloSocio(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK) {
        return getService().createArticoloSocio(articoloSocioPK);
    }

    /**
    * Deletes the articolo socio with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param articoloSocioPK the primary key of the articolo socio
    * @return the articolo socio that was removed
    * @throws PortalException if a articolo socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio deleteArticoloSocio(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteArticoloSocio(articoloSocioPK);
    }

    /**
    * Deletes the articolo socio from the database. Also notifies the appropriate model listeners.
    *
    * @param articoloSocio the articolo socio
    * @return the articolo socio that was removed
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio deleteArticoloSocio(
        it.bysoftware.ct.model.ArticoloSocio articoloSocio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteArticoloSocio(articoloSocio);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static it.bysoftware.ct.model.ArticoloSocio fetchArticoloSocio(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchArticoloSocio(articoloSocioPK);
    }

    /**
    * Returns the articolo socio with the primary key.
    *
    * @param articoloSocioPK the primary key of the articolo socio
    * @return the articolo socio
    * @throws PortalException if a articolo socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio getArticoloSocio(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getArticoloSocio(articoloSocioPK);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the articolo socios.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of articolo socios
    * @param end the upper bound of the range of articolo socios (not inclusive)
    * @return the range of articolo socios
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ArticoloSocio> getArticoloSocios(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getArticoloSocios(start, end);
    }

    /**
    * Returns the number of articolo socios.
    *
    * @return the number of articolo socios
    * @throws SystemException if a system exception occurred
    */
    public static int getArticoloSociosCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getArticoloSociosCount();
    }

    /**
    * Updates the articolo socio in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param articoloSocio the articolo socio
    * @return the articolo socio that was updated
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ArticoloSocio updateArticoloSocio(
        it.bysoftware.ct.model.ArticoloSocio articoloSocio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateArticoloSocio(articoloSocio);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static it.bysoftware.ct.model.ArticoloSocio findPartnerItems(
        java.lang.String partner, java.lang.String item,
        java.lang.String variant) {
        return getService().findPartnerItems(partner, item, variant);
    }

    public static it.bysoftware.ct.model.ArticoloSocio findCompanyItem(
        java.lang.String partner, java.lang.String item,
        java.lang.String variant) {
        return getService().findCompanyItem(partner, item, variant);
    }

    public static java.util.List<it.bysoftware.ct.model.ArticoloSocio> findAllPartnerItems(
        java.lang.String partner) {
        return getService().findAllPartnerItems(partner);
    }

    public static java.util.List<it.bysoftware.ct.model.ArticoloSocio> findPartnerItems(
        java.lang.String partner, java.lang.String itemCode,
        java.lang.String itemVariant, java.lang.String prtnerItemCode,
        java.lang.String partnerItemVariant, boolean andSearch, int start,
        int end, com.liferay.portal.kernel.util.OrderByComparator comparator) {
        return getService()
                   .findPartnerItems(partner, itemCode, itemVariant,
            prtnerItemCode, partnerItemVariant, andSearch, start, end,
            comparator);
    }

    public static int findPartnerItemsCount(java.lang.String partner,
        java.lang.String itemCode, java.lang.String itemVariant,
        java.lang.String partnerItemCode, java.lang.String partnerItemVariant,
        boolean andSearch) {
        return getService()
                   .findPartnerItemsCount(partner, itemCode, itemVariant,
            partnerItemCode, partnerItemVariant, andSearch);
    }

    public static void clearService() {
        _service = null;
    }

    public static ArticoloSocioLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    ArticoloSocioLocalService.class.getName());

            if (invokableLocalService instanceof ArticoloSocioLocalService) {
                _service = (ArticoloSocioLocalService) invokableLocalService;
            } else {
                _service = new ArticoloSocioLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(ArticoloSocioLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(ArticoloSocioLocalService service) {
    }
}
