package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class CentroPK implements Comparable<CentroPK>, Serializable {
    public String attivita;
    public String centro;

    public CentroPK() {
    }

    public CentroPK(String attivita, String centro) {
        this.attivita = attivita;
        this.centro = centro;
    }

    public String getAttivita() {
        return attivita;
    }

    public void setAttivita(String attivita) {
        this.attivita = attivita;
    }

    public String getCentro() {
        return centro;
    }

    public void setCentro(String centro) {
        this.centro = centro;
    }

    @Override
    public int compareTo(CentroPK pk) {
        if (pk == null) {
            return -1;
        }

        int value = 0;

        value = attivita.compareTo(pk.attivita);

        if (value != 0) {
            return value;
        }

        value = centro.compareTo(pk.centro);

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CentroPK)) {
            return false;
        }

        CentroPK pk = (CentroPK) obj;

        if ((attivita.equals(pk.attivita)) && (centro.equals(pk.centro))) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (String.valueOf(attivita) + String.valueOf(centro)).hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(10);

        sb.append(StringPool.OPEN_CURLY_BRACE);

        sb.append("attivita");
        sb.append(StringPool.EQUAL);
        sb.append(attivita);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("centro");
        sb.append(StringPool.EQUAL);
        sb.append(centro);

        sb.append(StringPool.CLOSE_CURLY_BRACE);

        return sb.toString();
    }
}
