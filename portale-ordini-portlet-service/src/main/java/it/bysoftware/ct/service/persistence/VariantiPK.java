package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class VariantiPK implements Comparable<VariantiPK>, Serializable {
    public String codiceVariante;
    public String tipoVariante;

    public VariantiPK() {
    }

    public VariantiPK(String codiceVariante, String tipoVariante) {
        this.codiceVariante = codiceVariante;
        this.tipoVariante = tipoVariante;
    }

    public String getCodiceVariante() {
        return codiceVariante;
    }

    public void setCodiceVariante(String codiceVariante) {
        this.codiceVariante = codiceVariante;
    }

    public String getTipoVariante() {
        return tipoVariante;
    }

    public void setTipoVariante(String tipoVariante) {
        this.tipoVariante = tipoVariante;
    }

    @Override
    public int compareTo(VariantiPK pk) {
        if (pk == null) {
            return -1;
        }

        int value = 0;

        value = codiceVariante.compareTo(pk.codiceVariante);

        if (value != 0) {
            return value;
        }

        value = tipoVariante.compareTo(pk.tipoVariante);

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VariantiPK)) {
            return false;
        }

        VariantiPK pk = (VariantiPK) obj;

        if ((codiceVariante.equals(pk.codiceVariante)) &&
                (tipoVariante.equals(pk.tipoVariante))) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (String.valueOf(codiceVariante) + String.valueOf(tipoVariante)).hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(10);

        sb.append(StringPool.OPEN_CURLY_BRACE);

        sb.append("codiceVariante");
        sb.append(StringPool.EQUAL);
        sb.append(codiceVariante);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("tipoVariante");
        sb.append(StringPool.EQUAL);
        sb.append(tipoVariante);

        sb.append(StringPool.CLOSE_CURLY_BRACE);

        return sb.toString();
    }
}
