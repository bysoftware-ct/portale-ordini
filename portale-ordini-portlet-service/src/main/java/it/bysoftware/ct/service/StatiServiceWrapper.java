package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link StatiService}.
 *
 * @author Mario Torrisi
 * @see StatiService
 * @generated
 */
public class StatiServiceWrapper implements StatiService,
    ServiceWrapper<StatiService> {
    private StatiService _statiService;

    public StatiServiceWrapper(StatiService statiService) {
        _statiService = statiService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _statiService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _statiService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _statiService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public StatiService getWrappedStatiService() {
        return _statiService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedStatiService(StatiService statiService) {
        _statiService = statiService;
    }

    @Override
    public StatiService getWrappedService() {
        return _statiService;
    }

    @Override
    public void setWrappedService(StatiService statiService) {
        _statiService = statiService;
    }
}
