package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class TestataFattureClientiPK implements Comparable<TestataFattureClientiPK>,
    Serializable {
    public int anno;
    public String codiceAttivita;
    public String codiceCentro;
    public int numeroProtocollo;

    public TestataFattureClientiPK() {
    }

    public TestataFattureClientiPK(int anno, String codiceAttivita,
        String codiceCentro, int numeroProtocollo) {
        this.anno = anno;
        this.codiceAttivita = codiceAttivita;
        this.codiceCentro = codiceCentro;
        this.numeroProtocollo = numeroProtocollo;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    public String getCodiceAttivita() {
        return codiceAttivita;
    }

    public void setCodiceAttivita(String codiceAttivita) {
        this.codiceAttivita = codiceAttivita;
    }

    public String getCodiceCentro() {
        return codiceCentro;
    }

    public void setCodiceCentro(String codiceCentro) {
        this.codiceCentro = codiceCentro;
    }

    public int getNumeroProtocollo() {
        return numeroProtocollo;
    }

    public void setNumeroProtocollo(int numeroProtocollo) {
        this.numeroProtocollo = numeroProtocollo;
    }

    @Override
    public int compareTo(TestataFattureClientiPK pk) {
        if (pk == null) {
            return -1;
        }

        int value = 0;

        if (anno < pk.anno) {
            value = -1;
        } else if (anno > pk.anno) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        value = codiceAttivita.compareTo(pk.codiceAttivita);

        if (value != 0) {
            return value;
        }

        value = codiceCentro.compareTo(pk.codiceCentro);

        if (value != 0) {
            return value;
        }

        if (numeroProtocollo < pk.numeroProtocollo) {
            value = -1;
        } else if (numeroProtocollo > pk.numeroProtocollo) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof TestataFattureClientiPK)) {
            return false;
        }

        TestataFattureClientiPK pk = (TestataFattureClientiPK) obj;

        if ((anno == pk.anno) && (codiceAttivita.equals(pk.codiceAttivita)) &&
                (codiceCentro.equals(pk.codiceCentro)) &&
                (numeroProtocollo == pk.numeroProtocollo)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (String.valueOf(anno) + String.valueOf(codiceAttivita) +
        String.valueOf(codiceCentro) + String.valueOf(numeroProtocollo)).hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(20);

        sb.append(StringPool.OPEN_CURLY_BRACE);

        sb.append("anno");
        sb.append(StringPool.EQUAL);
        sb.append(anno);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("codiceAttivita");
        sb.append(StringPool.EQUAL);
        sb.append(codiceAttivita);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("codiceCentro");
        sb.append(StringPool.EQUAL);
        sb.append(codiceCentro);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("numeroProtocollo");
        sb.append(StringPool.EQUAL);
        sb.append(numeroProtocollo);

        sb.append(StringPool.CLOSE_CURLY_BRACE);

        return sb.toString();
    }
}
