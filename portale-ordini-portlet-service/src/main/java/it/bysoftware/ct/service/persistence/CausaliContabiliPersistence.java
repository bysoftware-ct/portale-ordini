package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.CausaliContabili;

/**
 * The persistence interface for the causali contabili service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CausaliContabiliPersistenceImpl
 * @see CausaliContabiliUtil
 * @generated
 */
public interface CausaliContabiliPersistence extends BasePersistence<CausaliContabili> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link CausaliContabiliUtil} to access the causali contabili persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the causali contabili in the entity cache if it is enabled.
    *
    * @param causaliContabili the causali contabili
    */
    public void cacheResult(
        it.bysoftware.ct.model.CausaliContabili causaliContabili);

    /**
    * Caches the causali contabilis in the entity cache if it is enabled.
    *
    * @param causaliContabilis the causali contabilis
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.CausaliContabili> causaliContabilis);

    /**
    * Creates a new causali contabili with the primary key. Does not add the causali contabili to the database.
    *
    * @param codiceCausaleCont the primary key for the new causali contabili
    * @return the new causali contabili
    */
    public it.bysoftware.ct.model.CausaliContabili create(
        java.lang.String codiceCausaleCont);

    /**
    * Removes the causali contabili with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceCausaleCont the primary key of the causali contabili
    * @return the causali contabili that was removed
    * @throws it.bysoftware.ct.NoSuchCausaliContabiliException if a causali contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.CausaliContabili remove(
        java.lang.String codiceCausaleCont)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCausaliContabiliException;

    public it.bysoftware.ct.model.CausaliContabili updateImpl(
        it.bysoftware.ct.model.CausaliContabili causaliContabili)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the causali contabili with the primary key or throws a {@link it.bysoftware.ct.NoSuchCausaliContabiliException} if it could not be found.
    *
    * @param codiceCausaleCont the primary key of the causali contabili
    * @return the causali contabili
    * @throws it.bysoftware.ct.NoSuchCausaliContabiliException if a causali contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.CausaliContabili findByPrimaryKey(
        java.lang.String codiceCausaleCont)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCausaliContabiliException;

    /**
    * Returns the causali contabili with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param codiceCausaleCont the primary key of the causali contabili
    * @return the causali contabili, or <code>null</code> if a causali contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.CausaliContabili fetchByPrimaryKey(
        java.lang.String codiceCausaleCont)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the causali contabilis.
    *
    * @return the causali contabilis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.CausaliContabili> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the causali contabilis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CausaliContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of causali contabilis
    * @param end the upper bound of the range of causali contabilis (not inclusive)
    * @return the range of causali contabilis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.CausaliContabili> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the causali contabilis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CausaliContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of causali contabilis
    * @param end the upper bound of the range of causali contabilis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of causali contabilis
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.CausaliContabili> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the causali contabilis from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of causali contabilis.
    *
    * @return the number of causali contabilis
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
