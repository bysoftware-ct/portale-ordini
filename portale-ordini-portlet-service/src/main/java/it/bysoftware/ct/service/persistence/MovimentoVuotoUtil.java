package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.MovimentoVuoto;

import java.util.List;

/**
 * The persistence utility for the movimento vuoto service. This utility wraps {@link MovimentoVuotoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see MovimentoVuotoPersistence
 * @see MovimentoVuotoPersistenceImpl
 * @generated
 */
public class MovimentoVuotoUtil {
    private static MovimentoVuotoPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(MovimentoVuoto movimentoVuoto) {
        getPersistence().clearCache(movimentoVuoto);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<MovimentoVuoto> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<MovimentoVuoto> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<MovimentoVuoto> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static MovimentoVuoto update(MovimentoVuoto movimentoVuoto)
        throws SystemException {
        return getPersistence().update(movimentoVuoto);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static MovimentoVuoto update(MovimentoVuoto movimentoVuoto,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(movimentoVuoto, serviceContext);
    }

    /**
    * Returns all the movimento vuotos where codiceVuoto = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @return the matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findByCodiceVuoto(
        java.lang.String codiceVuoto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByCodiceVuoto(codiceVuoto);
    }

    /**
    * Returns a range of all the movimento vuotos where codiceVuoto = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceVuoto the codice vuoto
    * @param start the lower bound of the range of movimento vuotos
    * @param end the upper bound of the range of movimento vuotos (not inclusive)
    * @return the range of matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findByCodiceVuoto(
        java.lang.String codiceVuoto, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByCodiceVuoto(codiceVuoto, start, end);
    }

    /**
    * Returns an ordered range of all the movimento vuotos where codiceVuoto = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceVuoto the codice vuoto
    * @param start the lower bound of the range of movimento vuotos
    * @param end the upper bound of the range of movimento vuotos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findByCodiceVuoto(
        java.lang.String codiceVuoto, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCodiceVuoto(codiceVuoto, start, end, orderByComparator);
    }

    /**
    * Returns the first movimento vuoto in the ordered set where codiceVuoto = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto findByCodiceVuoto_First(
        java.lang.String codiceVuoto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException {
        return getPersistence()
                   .findByCodiceVuoto_First(codiceVuoto, orderByComparator);
    }

    /**
    * Returns the first movimento vuoto in the ordered set where codiceVuoto = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching movimento vuoto, or <code>null</code> if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto fetchByCodiceVuoto_First(
        java.lang.String codiceVuoto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCodiceVuoto_First(codiceVuoto, orderByComparator);
    }

    /**
    * Returns the last movimento vuoto in the ordered set where codiceVuoto = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto findByCodiceVuoto_Last(
        java.lang.String codiceVuoto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException {
        return getPersistence()
                   .findByCodiceVuoto_Last(codiceVuoto, orderByComparator);
    }

    /**
    * Returns the last movimento vuoto in the ordered set where codiceVuoto = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching movimento vuoto, or <code>null</code> if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto fetchByCodiceVuoto_Last(
        java.lang.String codiceVuoto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCodiceVuoto_Last(codiceVuoto, orderByComparator);
    }

    /**
    * Returns the movimento vuotos before and after the current movimento vuoto in the ordered set where codiceVuoto = &#63;.
    *
    * @param idMovimento the primary key of the current movimento vuoto
    * @param codiceVuoto the codice vuoto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a movimento vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto[] findByCodiceVuoto_PrevAndNext(
        long idMovimento, java.lang.String codiceVuoto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException {
        return getPersistence()
                   .findByCodiceVuoto_PrevAndNext(idMovimento, codiceVuoto,
            orderByComparator);
    }

    /**
    * Removes all the movimento vuotos where codiceVuoto = &#63; from the database.
    *
    * @param codiceVuoto the codice vuoto
    * @throws SystemException if a system exception occurred
    */
    public static void removeByCodiceVuoto(java.lang.String codiceVuoto)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByCodiceVuoto(codiceVuoto);
    }

    /**
    * Returns the number of movimento vuotos where codiceVuoto = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @return the number of matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public static int countByCodiceVuoto(java.lang.String codiceVuoto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByCodiceVuoto(codiceVuoto);
    }

    /**
    * Returns all the movimento vuotos where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @return the matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findByfindByVuotoSoggettoData(
        java.lang.String codiceVuoto, java.lang.String codiceSoggetto,
        java.util.Date dataMovimento, int tipoMovimento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByfindByVuotoSoggettoData(codiceVuoto, codiceSoggetto,
            dataMovimento, tipoMovimento);
    }

    /**
    * Returns a range of all the movimento vuotos where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @param start the lower bound of the range of movimento vuotos
    * @param end the upper bound of the range of movimento vuotos (not inclusive)
    * @return the range of matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findByfindByVuotoSoggettoData(
        java.lang.String codiceVuoto, java.lang.String codiceSoggetto,
        java.util.Date dataMovimento, int tipoMovimento, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByfindByVuotoSoggettoData(codiceVuoto, codiceSoggetto,
            dataMovimento, tipoMovimento, start, end);
    }

    /**
    * Returns an ordered range of all the movimento vuotos where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @param start the lower bound of the range of movimento vuotos
    * @param end the upper bound of the range of movimento vuotos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findByfindByVuotoSoggettoData(
        java.lang.String codiceVuoto, java.lang.String codiceSoggetto,
        java.util.Date dataMovimento, int tipoMovimento, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByfindByVuotoSoggettoData(codiceVuoto, codiceSoggetto,
            dataMovimento, tipoMovimento, start, end, orderByComparator);
    }

    /**
    * Returns the first movimento vuoto in the ordered set where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto findByfindByVuotoSoggettoData_First(
        java.lang.String codiceVuoto, java.lang.String codiceSoggetto,
        java.util.Date dataMovimento, int tipoMovimento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException {
        return getPersistence()
                   .findByfindByVuotoSoggettoData_First(codiceVuoto,
            codiceSoggetto, dataMovimento, tipoMovimento, orderByComparator);
    }

    /**
    * Returns the first movimento vuoto in the ordered set where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching movimento vuoto, or <code>null</code> if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto fetchByfindByVuotoSoggettoData_First(
        java.lang.String codiceVuoto, java.lang.String codiceSoggetto,
        java.util.Date dataMovimento, int tipoMovimento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByfindByVuotoSoggettoData_First(codiceVuoto,
            codiceSoggetto, dataMovimento, tipoMovimento, orderByComparator);
    }

    /**
    * Returns the last movimento vuoto in the ordered set where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto findByfindByVuotoSoggettoData_Last(
        java.lang.String codiceVuoto, java.lang.String codiceSoggetto,
        java.util.Date dataMovimento, int tipoMovimento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException {
        return getPersistence()
                   .findByfindByVuotoSoggettoData_Last(codiceVuoto,
            codiceSoggetto, dataMovimento, tipoMovimento, orderByComparator);
    }

    /**
    * Returns the last movimento vuoto in the ordered set where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching movimento vuoto, or <code>null</code> if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto fetchByfindByVuotoSoggettoData_Last(
        java.lang.String codiceVuoto, java.lang.String codiceSoggetto,
        java.util.Date dataMovimento, int tipoMovimento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByfindByVuotoSoggettoData_Last(codiceVuoto,
            codiceSoggetto, dataMovimento, tipoMovimento, orderByComparator);
    }

    /**
    * Returns the movimento vuotos before and after the current movimento vuoto in the ordered set where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
    *
    * @param idMovimento the primary key of the current movimento vuoto
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a movimento vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto[] findByfindByVuotoSoggettoData_PrevAndNext(
        long idMovimento, java.lang.String codiceVuoto,
        java.lang.String codiceSoggetto, java.util.Date dataMovimento,
        int tipoMovimento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException {
        return getPersistence()
                   .findByfindByVuotoSoggettoData_PrevAndNext(idMovimento,
            codiceVuoto, codiceSoggetto, dataMovimento, tipoMovimento,
            orderByComparator);
    }

    /**
    * Removes all the movimento vuotos where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63; from the database.
    *
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @throws SystemException if a system exception occurred
    */
    public static void removeByfindByVuotoSoggettoData(
        java.lang.String codiceVuoto, java.lang.String codiceSoggetto,
        java.util.Date dataMovimento, int tipoMovimento)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByfindByVuotoSoggettoData(codiceVuoto, codiceSoggetto,
            dataMovimento, tipoMovimento);
    }

    /**
    * Returns the number of movimento vuotos where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
    *
    * @param codiceVuoto the codice vuoto
    * @param codiceSoggetto the codice soggetto
    * @param dataMovimento the data movimento
    * @param tipoMovimento the tipo movimento
    * @return the number of matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public static int countByfindByVuotoSoggettoData(
        java.lang.String codiceVuoto, java.lang.String codiceSoggetto,
        java.util.Date dataMovimento, int tipoMovimento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByfindByVuotoSoggettoData(codiceVuoto, codiceSoggetto,
            dataMovimento, tipoMovimento);
    }

    /**
    * Returns all the movimento vuotos where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @return the matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findByCodiceSoggetto(
        java.lang.String codiceSoggetto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByCodiceSoggetto(codiceSoggetto);
    }

    /**
    * Returns a range of all the movimento vuotos where codiceSoggetto = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceSoggetto the codice soggetto
    * @param start the lower bound of the range of movimento vuotos
    * @param end the upper bound of the range of movimento vuotos (not inclusive)
    * @return the range of matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findByCodiceSoggetto(
        java.lang.String codiceSoggetto, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByCodiceSoggetto(codiceSoggetto, start, end);
    }

    /**
    * Returns an ordered range of all the movimento vuotos where codiceSoggetto = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceSoggetto the codice soggetto
    * @param start the lower bound of the range of movimento vuotos
    * @param end the upper bound of the range of movimento vuotos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findByCodiceSoggetto(
        java.lang.String codiceSoggetto, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCodiceSoggetto(codiceSoggetto, start, end,
            orderByComparator);
    }

    /**
    * Returns the first movimento vuoto in the ordered set where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto findByCodiceSoggetto_First(
        java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException {
        return getPersistence()
                   .findByCodiceSoggetto_First(codiceSoggetto, orderByComparator);
    }

    /**
    * Returns the first movimento vuoto in the ordered set where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching movimento vuoto, or <code>null</code> if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto fetchByCodiceSoggetto_First(
        java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCodiceSoggetto_First(codiceSoggetto,
            orderByComparator);
    }

    /**
    * Returns the last movimento vuoto in the ordered set where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto findByCodiceSoggetto_Last(
        java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException {
        return getPersistence()
                   .findByCodiceSoggetto_Last(codiceSoggetto, orderByComparator);
    }

    /**
    * Returns the last movimento vuoto in the ordered set where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching movimento vuoto, or <code>null</code> if a matching movimento vuoto could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto fetchByCodiceSoggetto_Last(
        java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCodiceSoggetto_Last(codiceSoggetto, orderByComparator);
    }

    /**
    * Returns the movimento vuotos before and after the current movimento vuoto in the ordered set where codiceSoggetto = &#63;.
    *
    * @param idMovimento the primary key of the current movimento vuoto
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a movimento vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto[] findByCodiceSoggetto_PrevAndNext(
        long idMovimento, java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException {
        return getPersistence()
                   .findByCodiceSoggetto_PrevAndNext(idMovimento,
            codiceSoggetto, orderByComparator);
    }

    /**
    * Removes all the movimento vuotos where codiceSoggetto = &#63; from the database.
    *
    * @param codiceSoggetto the codice soggetto
    * @throws SystemException if a system exception occurred
    */
    public static void removeByCodiceSoggetto(java.lang.String codiceSoggetto)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByCodiceSoggetto(codiceSoggetto);
    }

    /**
    * Returns the number of movimento vuotos where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @return the number of matching movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public static int countByCodiceSoggetto(java.lang.String codiceSoggetto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByCodiceSoggetto(codiceSoggetto);
    }

    /**
    * Caches the movimento vuoto in the entity cache if it is enabled.
    *
    * @param movimentoVuoto the movimento vuoto
    */
    public static void cacheResult(
        it.bysoftware.ct.model.MovimentoVuoto movimentoVuoto) {
        getPersistence().cacheResult(movimentoVuoto);
    }

    /**
    * Caches the movimento vuotos in the entity cache if it is enabled.
    *
    * @param movimentoVuotos the movimento vuotos
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.MovimentoVuoto> movimentoVuotos) {
        getPersistence().cacheResult(movimentoVuotos);
    }

    /**
    * Creates a new movimento vuoto with the primary key. Does not add the movimento vuoto to the database.
    *
    * @param idMovimento the primary key for the new movimento vuoto
    * @return the new movimento vuoto
    */
    public static it.bysoftware.ct.model.MovimentoVuoto create(long idMovimento) {
        return getPersistence().create(idMovimento);
    }

    /**
    * Removes the movimento vuoto with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param idMovimento the primary key of the movimento vuoto
    * @return the movimento vuoto that was removed
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a movimento vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto remove(long idMovimento)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException {
        return getPersistence().remove(idMovimento);
    }

    public static it.bysoftware.ct.model.MovimentoVuoto updateImpl(
        it.bysoftware.ct.model.MovimentoVuoto movimentoVuoto)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(movimentoVuoto);
    }

    /**
    * Returns the movimento vuoto with the primary key or throws a {@link it.bysoftware.ct.NoSuchMovimentoVuotoException} if it could not be found.
    *
    * @param idMovimento the primary key of the movimento vuoto
    * @return the movimento vuoto
    * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a movimento vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto findByPrimaryKey(
        long idMovimento)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchMovimentoVuotoException {
        return getPersistence().findByPrimaryKey(idMovimento);
    }

    /**
    * Returns the movimento vuoto with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param idMovimento the primary key of the movimento vuoto
    * @return the movimento vuoto, or <code>null</code> if a movimento vuoto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.MovimentoVuoto fetchByPrimaryKey(
        long idMovimento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(idMovimento);
    }

    /**
    * Returns all the movimento vuotos.
    *
    * @return the movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the movimento vuotos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of movimento vuotos
    * @param end the upper bound of the range of movimento vuotos (not inclusive)
    * @return the range of movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the movimento vuotos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of movimento vuotos
    * @param end the upper bound of the range of movimento vuotos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.MovimentoVuoto> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the movimento vuotos from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of movimento vuotos.
    *
    * @return the number of movimento vuotos
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static MovimentoVuotoPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (MovimentoVuotoPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    MovimentoVuotoPersistence.class.getName());

            ReferenceRegistry.registerReference(MovimentoVuotoUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(MovimentoVuotoPersistence persistence) {
    }
}
