package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.Rubrica;

/**
 * The persistence interface for the rubrica service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see RubricaPersistenceImpl
 * @see RubricaUtil
 * @generated
 */
public interface RubricaPersistence extends BasePersistence<Rubrica> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link RubricaUtil} to access the rubrica persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the rubricas where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @return the matching rubricas
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Rubrica> findByCodiceSoggetto(
        java.lang.String codiceSoggetto)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the rubricas where codiceSoggetto = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RubricaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceSoggetto the codice soggetto
    * @param start the lower bound of the range of rubricas
    * @param end the upper bound of the range of rubricas (not inclusive)
    * @return the range of matching rubricas
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Rubrica> findByCodiceSoggetto(
        java.lang.String codiceSoggetto, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the rubricas where codiceSoggetto = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RubricaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceSoggetto the codice soggetto
    * @param start the lower bound of the range of rubricas
    * @param end the upper bound of the range of rubricas (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rubricas
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Rubrica> findByCodiceSoggetto(
        java.lang.String codiceSoggetto, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first rubrica in the ordered set where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching rubrica
    * @throws it.bysoftware.ct.NoSuchRubricaException if a matching rubrica could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Rubrica findByCodiceSoggetto_First(
        java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchRubricaException;

    /**
    * Returns the first rubrica in the ordered set where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching rubrica, or <code>null</code> if a matching rubrica could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Rubrica fetchByCodiceSoggetto_First(
        java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last rubrica in the ordered set where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching rubrica
    * @throws it.bysoftware.ct.NoSuchRubricaException if a matching rubrica could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Rubrica findByCodiceSoggetto_Last(
        java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchRubricaException;

    /**
    * Returns the last rubrica in the ordered set where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching rubrica, or <code>null</code> if a matching rubrica could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Rubrica fetchByCodiceSoggetto_Last(
        java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the rubricas before and after the current rubrica in the ordered set where codiceSoggetto = &#63;.
    *
    * @param rubricaPK the primary key of the current rubrica
    * @param codiceSoggetto the codice soggetto
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next rubrica
    * @throws it.bysoftware.ct.NoSuchRubricaException if a rubrica with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Rubrica[] findByCodiceSoggetto_PrevAndNext(
        it.bysoftware.ct.service.persistence.RubricaPK rubricaPK,
        java.lang.String codiceSoggetto,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchRubricaException;

    /**
    * Removes all the rubricas where codiceSoggetto = &#63; from the database.
    *
    * @param codiceSoggetto the codice soggetto
    * @throws SystemException if a system exception occurred
    */
    public void removeByCodiceSoggetto(java.lang.String codiceSoggetto)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of rubricas where codiceSoggetto = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @return the number of matching rubricas
    * @throws SystemException if a system exception occurred
    */
    public int countByCodiceSoggetto(java.lang.String codiceSoggetto)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the rubrica in the entity cache if it is enabled.
    *
    * @param rubrica the rubrica
    */
    public void cacheResult(it.bysoftware.ct.model.Rubrica rubrica);

    /**
    * Caches the rubricas in the entity cache if it is enabled.
    *
    * @param rubricas the rubricas
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.Rubrica> rubricas);

    /**
    * Creates a new rubrica with the primary key. Does not add the rubrica to the database.
    *
    * @param rubricaPK the primary key for the new rubrica
    * @return the new rubrica
    */
    public it.bysoftware.ct.model.Rubrica create(
        it.bysoftware.ct.service.persistence.RubricaPK rubricaPK);

    /**
    * Removes the rubrica with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param rubricaPK the primary key of the rubrica
    * @return the rubrica that was removed
    * @throws it.bysoftware.ct.NoSuchRubricaException if a rubrica with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Rubrica remove(
        it.bysoftware.ct.service.persistence.RubricaPK rubricaPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchRubricaException;

    public it.bysoftware.ct.model.Rubrica updateImpl(
        it.bysoftware.ct.model.Rubrica rubrica)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the rubrica with the primary key or throws a {@link it.bysoftware.ct.NoSuchRubricaException} if it could not be found.
    *
    * @param rubricaPK the primary key of the rubrica
    * @return the rubrica
    * @throws it.bysoftware.ct.NoSuchRubricaException if a rubrica with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Rubrica findByPrimaryKey(
        it.bysoftware.ct.service.persistence.RubricaPK rubricaPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchRubricaException;

    /**
    * Returns the rubrica with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param rubricaPK the primary key of the rubrica
    * @return the rubrica, or <code>null</code> if a rubrica with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Rubrica fetchByPrimaryKey(
        it.bysoftware.ct.service.persistence.RubricaPK rubricaPK)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the rubricas.
    *
    * @return the rubricas
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Rubrica> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the rubricas.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RubricaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of rubricas
    * @param end the upper bound of the range of rubricas (not inclusive)
    * @return the range of rubricas
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Rubrica> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the rubricas.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RubricaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of rubricas
    * @param end the upper bound of the range of rubricas (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of rubricas
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Rubrica> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the rubricas from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of rubricas.
    *
    * @return the number of rubricas
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
