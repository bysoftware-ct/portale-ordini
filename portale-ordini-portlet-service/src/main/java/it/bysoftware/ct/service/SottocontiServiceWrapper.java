package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SottocontiService}.
 *
 * @author Mario Torrisi
 * @see SottocontiService
 * @generated
 */
public class SottocontiServiceWrapper implements SottocontiService,
    ServiceWrapper<SottocontiService> {
    private SottocontiService _sottocontiService;

    public SottocontiServiceWrapper(SottocontiService sottocontiService) {
        _sottocontiService = sottocontiService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _sottocontiService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _sottocontiService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _sottocontiService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public SottocontiService getWrappedSottocontiService() {
        return _sottocontiService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedSottocontiService(SottocontiService sottocontiService) {
        _sottocontiService = sottocontiService;
    }

    @Override
    public SottocontiService getWrappedService() {
        return _sottocontiService;
    }

    @Override
    public void setWrappedService(SottocontiService sottocontiService) {
        _sottocontiService = sottocontiService;
    }
}
