package it.bysoftware.ct.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for SpettanzeSoci. This utility wraps
 * {@link it.bysoftware.ct.service.impl.SpettanzeSociLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Mario Torrisi
 * @see SpettanzeSociLocalService
 * @see it.bysoftware.ct.service.base.SpettanzeSociLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.impl.SpettanzeSociLocalServiceImpl
 * @generated
 */
public class SpettanzeSociLocalServiceUtil {
    private static SpettanzeSociLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link it.bysoftware.ct.service.impl.SpettanzeSociLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the spettanze soci to the database. Also notifies the appropriate model listeners.
    *
    * @param spettanzeSoci the spettanze soci
    * @return the spettanze soci that was added
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci addSpettanzeSoci(
        it.bysoftware.ct.model.SpettanzeSoci spettanzeSoci)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addSpettanzeSoci(spettanzeSoci);
    }

    /**
    * Creates a new spettanze soci with the primary key. Does not add the spettanze soci to the database.
    *
    * @param id the primary key for the new spettanze soci
    * @return the new spettanze soci
    */
    public static it.bysoftware.ct.model.SpettanzeSoci createSpettanzeSoci(
        long id) {
        return getService().createSpettanzeSoci(id);
    }

    /**
    * Deletes the spettanze soci with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the spettanze soci
    * @return the spettanze soci that was removed
    * @throws PortalException if a spettanze soci with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci deleteSpettanzeSoci(
        long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteSpettanzeSoci(id);
    }

    /**
    * Deletes the spettanze soci from the database. Also notifies the appropriate model listeners.
    *
    * @param spettanzeSoci the spettanze soci
    * @return the spettanze soci that was removed
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci deleteSpettanzeSoci(
        it.bysoftware.ct.model.SpettanzeSoci spettanzeSoci)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteSpettanzeSoci(spettanzeSoci);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static it.bysoftware.ct.model.SpettanzeSoci fetchSpettanzeSoci(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchSpettanzeSoci(id);
    }

    /**
    * Returns the spettanze soci with the primary key.
    *
    * @param id the primary key of the spettanze soci
    * @return the spettanze soci
    * @throws PortalException if a spettanze soci with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci getSpettanzeSoci(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getSpettanzeSoci(id);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the spettanze socis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of spettanze socis
    * @param end the upper bound of the range of spettanze socis (not inclusive)
    * @return the range of spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SpettanzeSoci> getSpettanzeSocis(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getSpettanzeSocis(start, end);
    }

    /**
    * Returns the number of spettanze socis.
    *
    * @return the number of spettanze socis
    * @throws SystemException if a system exception occurred
    */
    public static int getSpettanzeSocisCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getSpettanzeSocisCount();
    }

    /**
    * Updates the spettanze soci in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param spettanzeSoci the spettanze soci
    * @return the spettanze soci that was updated
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SpettanzeSoci updateSpettanzeSoci(
        it.bysoftware.ct.model.SpettanzeSoci spettanzeSoci)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateSpettanzeSoci(spettanzeSoci);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByPartner(
        java.lang.String subjectCode) {
        return getService().findByPartner(subjectCode);
    }

    public static java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByPartnerState(
        java.lang.String subjectCode, java.lang.Integer state) {
        return getService().findByPartnerState(subjectCode, state);
    }

    public static java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByPartnerPaymentYearPaymentId(
        java.lang.String subjectCode, int year, int paymentId) {
        return getService()
                   .findByPartnerPaymentYearPaymentId(subjectCode, year,
            paymentId);
    }

    public static it.bysoftware.ct.model.SpettanzeSoci findByInvoice(
        it.bysoftware.ct.service.persistence.TestataFattureClientiPK invoicePK,
        java.lang.String subjectCode) {
        return getService().findByInvoice(invoicePK, subjectCode);
    }

    public static it.bysoftware.ct.model.SpettanzeSoci findByInvoiceState(
        it.bysoftware.ct.service.persistence.TestataFattureClientiPK invoicePK,
        java.lang.String subjectCode, java.lang.Integer state) {
        return getService().findByInvoiceState(invoicePK, subjectCode, state);
    }

    public static java.util.List<java.lang.Object[]> getCreditAmountPerPartner(
        java.lang.Integer state, java.util.Date from, java.util.Date to) {
        return getService().getCreditAmountPerPartner(state, from, to);
    }

    public static java.util.List<it.bysoftware.ct.model.SpettanzeSoci> findByPartnerAndComputeDate(
        java.lang.String partnerCode, java.util.Date from, java.util.Date to,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator comp) {
        return getService()
                   .findByPartnerAndComputeDate(partnerCode, from, to, start,
            end, comp);
    }

    public static void clearService() {
        _service = null;
    }

    public static SpettanzeSociLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    SpettanzeSociLocalService.class.getName());

            if (invokableLocalService instanceof SpettanzeSociLocalService) {
                _service = (SpettanzeSociLocalService) invokableLocalService;
            } else {
                _service = new SpettanzeSociLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(SpettanzeSociLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(SpettanzeSociLocalService service) {
    }
}
