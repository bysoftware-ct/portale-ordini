package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.TestataDocumento;

import java.util.List;

/**
 * The persistence utility for the testata documento service. This utility wraps {@link TestataDocumentoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see TestataDocumentoPersistence
 * @see TestataDocumentoPersistenceImpl
 * @generated
 */
public class TestataDocumentoUtil {
    private static TestataDocumentoPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(TestataDocumento testataDocumento) {
        getPersistence().clearCache(testataDocumento);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<TestataDocumento> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<TestataDocumento> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<TestataDocumento> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static TestataDocumento update(TestataDocumento testataDocumento)
        throws SystemException {
        return getPersistence().update(testataDocumento);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static TestataDocumento update(TestataDocumento testataDocumento,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(testataDocumento, serviceContext);
    }

    /**
    * Returns all the testata documentos where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @return the matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findByAnnoAttivitaCentroDeposito(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, java.lang.String codiceDeposito,
        java.lang.String codiceFornitore, java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByAnnoAttivitaCentroDeposito(anno, codiceAttivita,
            codiceCentro, codiceDeposito, codiceFornitore, tipoDocumento);
    }

    /**
    * Returns a range of all the testata documentos where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @return the range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findByAnnoAttivitaCentroDeposito(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, java.lang.String codiceDeposito,
        java.lang.String codiceFornitore, java.lang.String tipoDocumento,
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByAnnoAttivitaCentroDeposito(anno, codiceAttivita,
            codiceCentro, codiceDeposito, codiceFornitore, tipoDocumento,
            start, end);
    }

    /**
    * Returns an ordered range of all the testata documentos where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findByAnnoAttivitaCentroDeposito(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, java.lang.String codiceDeposito,
        java.lang.String codiceFornitore, java.lang.String tipoDocumento,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByAnnoAttivitaCentroDeposito(anno, codiceAttivita,
            codiceCentro, codiceDeposito, codiceFornitore, tipoDocumento,
            start, end, orderByComparator);
    }

    /**
    * Returns the first testata documento in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento findByAnnoAttivitaCentroDeposito_First(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, java.lang.String codiceDeposito,
        java.lang.String codiceFornitore, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByAnnoAttivitaCentroDeposito_First(anno,
            codiceAttivita, codiceCentro, codiceDeposito, codiceFornitore,
            tipoDocumento, orderByComparator);
    }

    /**
    * Returns the first testata documento in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento fetchByAnnoAttivitaCentroDeposito_First(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, java.lang.String codiceDeposito,
        java.lang.String codiceFornitore, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByAnnoAttivitaCentroDeposito_First(anno,
            codiceAttivita, codiceCentro, codiceDeposito, codiceFornitore,
            tipoDocumento, orderByComparator);
    }

    /**
    * Returns the last testata documento in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento findByAnnoAttivitaCentroDeposito_Last(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, java.lang.String codiceDeposito,
        java.lang.String codiceFornitore, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByAnnoAttivitaCentroDeposito_Last(anno, codiceAttivita,
            codiceCentro, codiceDeposito, codiceFornitore, tipoDocumento,
            orderByComparator);
    }

    /**
    * Returns the last testata documento in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento fetchByAnnoAttivitaCentroDeposito_Last(
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, java.lang.String codiceDeposito,
        java.lang.String codiceFornitore, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByAnnoAttivitaCentroDeposito_Last(anno,
            codiceAttivita, codiceCentro, codiceDeposito, codiceFornitore,
            tipoDocumento, orderByComparator);
    }

    /**
    * Returns the testata documentos before and after the current testata documento in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param testataDocumentoPK the primary key of the current testata documento
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento[] findByAnnoAttivitaCentroDeposito_PrevAndNext(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK,
        int anno, java.lang.String codiceAttivita,
        java.lang.String codiceCentro, java.lang.String codiceDeposito,
        java.lang.String codiceFornitore, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByAnnoAttivitaCentroDeposito_PrevAndNext(testataDocumentoPK,
            anno, codiceAttivita, codiceCentro, codiceDeposito,
            codiceFornitore, tipoDocumento, orderByComparator);
    }

    /**
    * Removes all the testata documentos where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63; from the database.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @throws SystemException if a system exception occurred
    */
    public static void removeByAnnoAttivitaCentroDeposito(int anno,
        java.lang.String codiceAttivita, java.lang.String codiceCentro,
        java.lang.String codiceDeposito, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByAnnoAttivitaCentroDeposito(anno, codiceAttivita,
            codiceCentro, codiceDeposito, codiceFornitore, tipoDocumento);
    }

    /**
    * Returns the number of testata documentos where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param anno the anno
    * @param codiceAttivita the codice attivita
    * @param codiceCentro the codice centro
    * @param codiceDeposito the codice deposito
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @return the number of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static int countByAnnoAttivitaCentroDeposito(int anno,
        java.lang.String codiceAttivita, java.lang.String codiceCentro,
        java.lang.String codiceDeposito, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByAnnoAttivitaCentroDeposito(anno, codiceAttivita,
            codiceCentro, codiceDeposito, codiceFornitore, tipoDocumento);
    }

    /**
    * Returns all the testata documentos where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @return the matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findByDataStatoTipoDocumento(
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDataStatoTipoDocumento(dataRegistrazione, stato,
            tipoDocumento);
    }

    /**
    * Returns a range of all the testata documentos where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @return the range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findByDataStatoTipoDocumento(
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDataStatoTipoDocumento(dataRegistrazione, stato,
            tipoDocumento, start, end);
    }

    /**
    * Returns an ordered range of all the testata documentos where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findByDataStatoTipoDocumento(
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDataStatoTipoDocumento(dataRegistrazione, stato,
            tipoDocumento, start, end, orderByComparator);
    }

    /**
    * Returns the first testata documento in the ordered set where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento findByDataStatoTipoDocumento_First(
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByDataStatoTipoDocumento_First(dataRegistrazione,
            stato, tipoDocumento, orderByComparator);
    }

    /**
    * Returns the first testata documento in the ordered set where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento fetchByDataStatoTipoDocumento_First(
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByDataStatoTipoDocumento_First(dataRegistrazione,
            stato, tipoDocumento, orderByComparator);
    }

    /**
    * Returns the last testata documento in the ordered set where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento findByDataStatoTipoDocumento_Last(
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByDataStatoTipoDocumento_Last(dataRegistrazione, stato,
            tipoDocumento, orderByComparator);
    }

    /**
    * Returns the last testata documento in the ordered set where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento fetchByDataStatoTipoDocumento_Last(
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByDataStatoTipoDocumento_Last(dataRegistrazione,
            stato, tipoDocumento, orderByComparator);
    }

    /**
    * Returns the testata documentos before and after the current testata documento in the ordered set where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param testataDocumentoPK the primary key of the current testata documento
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento[] findByDataStatoTipoDocumento_PrevAndNext(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK,
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByDataStatoTipoDocumento_PrevAndNext(testataDocumentoPK,
            dataRegistrazione, stato, tipoDocumento, orderByComparator);
    }

    /**
    * Removes all the testata documentos where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63; from the database.
    *
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @throws SystemException if a system exception occurred
    */
    public static void removeByDataStatoTipoDocumento(
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByDataStatoTipoDocumento(dataRegistrazione, stato,
            tipoDocumento);
    }

    /**
    * Returns the number of testata documentos where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @return the number of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static int countByDataStatoTipoDocumento(
        java.util.Date dataRegistrazione, boolean stato,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByDataStatoTipoDocumento(dataRegistrazione, stato,
            tipoDocumento);
    }

    /**
    * Returns all the testata documentos where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @return the matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findByDataFornitoreTipoDocumento(
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDataFornitoreTipoDocumento(dataRegistrazione,
            codiceFornitore, tipoDocumento);
    }

    /**
    * Returns a range of all the testata documentos where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @return the range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findByDataFornitoreTipoDocumento(
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDataFornitoreTipoDocumento(dataRegistrazione,
            codiceFornitore, tipoDocumento, start, end);
    }

    /**
    * Returns an ordered range of all the testata documentos where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findByDataFornitoreTipoDocumento(
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDataFornitoreTipoDocumento(dataRegistrazione,
            codiceFornitore, tipoDocumento, start, end, orderByComparator);
    }

    /**
    * Returns the first testata documento in the ordered set where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento findByDataFornitoreTipoDocumento_First(
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByDataFornitoreTipoDocumento_First(dataRegistrazione,
            codiceFornitore, tipoDocumento, orderByComparator);
    }

    /**
    * Returns the first testata documento in the ordered set where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento fetchByDataFornitoreTipoDocumento_First(
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByDataFornitoreTipoDocumento_First(dataRegistrazione,
            codiceFornitore, tipoDocumento, orderByComparator);
    }

    /**
    * Returns the last testata documento in the ordered set where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento findByDataFornitoreTipoDocumento_Last(
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByDataFornitoreTipoDocumento_Last(dataRegistrazione,
            codiceFornitore, tipoDocumento, orderByComparator);
    }

    /**
    * Returns the last testata documento in the ordered set where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento fetchByDataFornitoreTipoDocumento_Last(
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByDataFornitoreTipoDocumento_Last(dataRegistrazione,
            codiceFornitore, tipoDocumento, orderByComparator);
    }

    /**
    * Returns the testata documentos before and after the current testata documento in the ordered set where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param testataDocumentoPK the primary key of the current testata documento
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento[] findByDataFornitoreTipoDocumento_PrevAndNext(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK,
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByDataFornitoreTipoDocumento_PrevAndNext(testataDocumentoPK,
            dataRegistrazione, codiceFornitore, tipoDocumento, orderByComparator);
    }

    /**
    * Removes all the testata documentos where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63; from the database.
    *
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @throws SystemException if a system exception occurred
    */
    public static void removeByDataFornitoreTipoDocumento(
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByDataFornitoreTipoDocumento(dataRegistrazione,
            codiceFornitore, tipoDocumento);
    }

    /**
    * Returns the number of testata documentos where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param dataRegistrazione the data registrazione
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @return the number of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static int countByDataFornitoreTipoDocumento(
        java.util.Date dataRegistrazione, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByDataFornitoreTipoDocumento(dataRegistrazione,
            codiceFornitore, tipoDocumento);
    }

    /**
    * Returns all the testata documentos where stato = &#63; and tipoDocumento = &#63;.
    *
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @return the matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findByStatoTipoDocumento(
        boolean stato, java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByStatoTipoDocumento(stato, tipoDocumento);
    }

    /**
    * Returns a range of all the testata documentos where stato = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @return the range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findByStatoTipoDocumento(
        boolean stato, java.lang.String tipoDocumento, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByStatoTipoDocumento(stato, tipoDocumento, start, end);
    }

    /**
    * Returns an ordered range of all the testata documentos where stato = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findByStatoTipoDocumento(
        boolean stato, java.lang.String tipoDocumento, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByStatoTipoDocumento(stato, tipoDocumento, start, end,
            orderByComparator);
    }

    /**
    * Returns the first testata documento in the ordered set where stato = &#63; and tipoDocumento = &#63;.
    *
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento findByStatoTipoDocumento_First(
        boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByStatoTipoDocumento_First(stato, tipoDocumento,
            orderByComparator);
    }

    /**
    * Returns the first testata documento in the ordered set where stato = &#63; and tipoDocumento = &#63;.
    *
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento fetchByStatoTipoDocumento_First(
        boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByStatoTipoDocumento_First(stato, tipoDocumento,
            orderByComparator);
    }

    /**
    * Returns the last testata documento in the ordered set where stato = &#63; and tipoDocumento = &#63;.
    *
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento findByStatoTipoDocumento_Last(
        boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByStatoTipoDocumento_Last(stato, tipoDocumento,
            orderByComparator);
    }

    /**
    * Returns the last testata documento in the ordered set where stato = &#63; and tipoDocumento = &#63;.
    *
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento fetchByStatoTipoDocumento_Last(
        boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByStatoTipoDocumento_Last(stato, tipoDocumento,
            orderByComparator);
    }

    /**
    * Returns the testata documentos before and after the current testata documento in the ordered set where stato = &#63; and tipoDocumento = &#63;.
    *
    * @param testataDocumentoPK the primary key of the current testata documento
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento[] findByStatoTipoDocumento_PrevAndNext(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK,
        boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByStatoTipoDocumento_PrevAndNext(testataDocumentoPK,
            stato, tipoDocumento, orderByComparator);
    }

    /**
    * Removes all the testata documentos where stato = &#63; and tipoDocumento = &#63; from the database.
    *
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @throws SystemException if a system exception occurred
    */
    public static void removeByStatoTipoDocumento(boolean stato,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByStatoTipoDocumento(stato, tipoDocumento);
    }

    /**
    * Returns the number of testata documentos where stato = &#63; and tipoDocumento = &#63;.
    *
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @return the number of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static int countByStatoTipoDocumento(boolean stato,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByStatoTipoDocumento(stato, tipoDocumento);
    }

    /**
    * Returns all the testata documentos where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @return the matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findByIdOrdineTipoDocumentoStato(
        long libLng1, boolean stato, java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByIdOrdineTipoDocumentoStato(libLng1, stato,
            tipoDocumento);
    }

    /**
    * Returns a range of all the testata documentos where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @return the range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findByIdOrdineTipoDocumentoStato(
        long libLng1, boolean stato, java.lang.String tipoDocumento, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByIdOrdineTipoDocumentoStato(libLng1, stato,
            tipoDocumento, start, end);
    }

    /**
    * Returns an ordered range of all the testata documentos where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findByIdOrdineTipoDocumentoStato(
        long libLng1, boolean stato, java.lang.String tipoDocumento, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByIdOrdineTipoDocumentoStato(libLng1, stato,
            tipoDocumento, start, end, orderByComparator);
    }

    /**
    * Returns the first testata documento in the ordered set where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento findByIdOrdineTipoDocumentoStato_First(
        long libLng1, boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByIdOrdineTipoDocumentoStato_First(libLng1, stato,
            tipoDocumento, orderByComparator);
    }

    /**
    * Returns the first testata documento in the ordered set where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento fetchByIdOrdineTipoDocumentoStato_First(
        long libLng1, boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByIdOrdineTipoDocumentoStato_First(libLng1, stato,
            tipoDocumento, orderByComparator);
    }

    /**
    * Returns the last testata documento in the ordered set where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento findByIdOrdineTipoDocumentoStato_Last(
        long libLng1, boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByIdOrdineTipoDocumentoStato_Last(libLng1, stato,
            tipoDocumento, orderByComparator);
    }

    /**
    * Returns the last testata documento in the ordered set where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento fetchByIdOrdineTipoDocumentoStato_Last(
        long libLng1, boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByIdOrdineTipoDocumentoStato_Last(libLng1, stato,
            tipoDocumento, orderByComparator);
    }

    /**
    * Returns the testata documentos before and after the current testata documento in the ordered set where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param testataDocumentoPK the primary key of the current testata documento
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento[] findByIdOrdineTipoDocumentoStato_PrevAndNext(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK,
        long libLng1, boolean stato, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByIdOrdineTipoDocumentoStato_PrevAndNext(testataDocumentoPK,
            libLng1, stato, tipoDocumento, orderByComparator);
    }

    /**
    * Removes all the testata documentos where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63; from the database.
    *
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @throws SystemException if a system exception occurred
    */
    public static void removeByIdOrdineTipoDocumentoStato(long libLng1,
        boolean stato, java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByIdOrdineTipoDocumentoStato(libLng1, stato, tipoDocumento);
    }

    /**
    * Returns the number of testata documentos where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param stato the stato
    * @param tipoDocumento the tipo documento
    * @return the number of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static int countByIdOrdineTipoDocumentoStato(long libLng1,
        boolean stato, java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByIdOrdineTipoDocumentoStato(libLng1, stato,
            tipoDocumento);
    }

    /**
    * Returns all the testata documentos where libLng1 = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @return the matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findByIdOrdineTipo(
        long libLng1, java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByIdOrdineTipo(libLng1, tipoDocumento);
    }

    /**
    * Returns a range of all the testata documentos where libLng1 = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @return the range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findByIdOrdineTipo(
        long libLng1, java.lang.String tipoDocumento, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByIdOrdineTipo(libLng1, tipoDocumento, start, end);
    }

    /**
    * Returns an ordered range of all the testata documentos where libLng1 = &#63; and tipoDocumento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findByIdOrdineTipo(
        long libLng1, java.lang.String tipoDocumento, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByIdOrdineTipo(libLng1, tipoDocumento, start, end,
            orderByComparator);
    }

    /**
    * Returns the first testata documento in the ordered set where libLng1 = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento findByIdOrdineTipo_First(
        long libLng1, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByIdOrdineTipo_First(libLng1, tipoDocumento,
            orderByComparator);
    }

    /**
    * Returns the first testata documento in the ordered set where libLng1 = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento fetchByIdOrdineTipo_First(
        long libLng1, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByIdOrdineTipo_First(libLng1, tipoDocumento,
            orderByComparator);
    }

    /**
    * Returns the last testata documento in the ordered set where libLng1 = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento findByIdOrdineTipo_Last(
        long libLng1, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByIdOrdineTipo_Last(libLng1, tipoDocumento,
            orderByComparator);
    }

    /**
    * Returns the last testata documento in the ordered set where libLng1 = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento fetchByIdOrdineTipo_Last(
        long libLng1, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByIdOrdineTipo_Last(libLng1, tipoDocumento,
            orderByComparator);
    }

    /**
    * Returns the testata documentos before and after the current testata documento in the ordered set where libLng1 = &#63; and tipoDocumento = &#63;.
    *
    * @param testataDocumentoPK the primary key of the current testata documento
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento[] findByIdOrdineTipo_PrevAndNext(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK,
        long libLng1, java.lang.String tipoDocumento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByIdOrdineTipo_PrevAndNext(testataDocumentoPK, libLng1,
            tipoDocumento, orderByComparator);
    }

    /**
    * Removes all the testata documentos where libLng1 = &#63; and tipoDocumento = &#63; from the database.
    *
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @throws SystemException if a system exception occurred
    */
    public static void removeByIdOrdineTipo(long libLng1,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByIdOrdineTipo(libLng1, tipoDocumento);
    }

    /**
    * Returns the number of testata documentos where libLng1 = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param tipoDocumento the tipo documento
    * @return the number of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static int countByIdOrdineTipo(long libLng1,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByIdOrdineTipo(libLng1, tipoDocumento);
    }

    /**
    * Returns the testata documento where libLng1 = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63; or throws a {@link it.bysoftware.ct.NoSuchTestataDocumentoException} if it could not be found.
    *
    * @param libLng1 the lib lng1
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @return the matching testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento findByIdOrdineFornitoreTipo(
        long libLng1, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .findByIdOrdineFornitoreTipo(libLng1, codiceFornitore,
            tipoDocumento);
    }

    /**
    * Returns the testata documento where libLng1 = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param libLng1 the lib lng1
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @return the matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento fetchByIdOrdineFornitoreTipo(
        long libLng1, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByIdOrdineFornitoreTipo(libLng1, codiceFornitore,
            tipoDocumento);
    }

    /**
    * Returns the testata documento where libLng1 = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param libLng1 the lib lng1
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching testata documento, or <code>null</code> if a matching testata documento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento fetchByIdOrdineFornitoreTipo(
        long libLng1, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByIdOrdineFornitoreTipo(libLng1, codiceFornitore,
            tipoDocumento, retrieveFromCache);
    }

    /**
    * Removes the testata documento where libLng1 = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63; from the database.
    *
    * @param libLng1 the lib lng1
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @return the testata documento that was removed
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento removeByIdOrdineFornitoreTipo(
        long libLng1, java.lang.String codiceFornitore,
        java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence()
                   .removeByIdOrdineFornitoreTipo(libLng1, codiceFornitore,
            tipoDocumento);
    }

    /**
    * Returns the number of testata documentos where libLng1 = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
    *
    * @param libLng1 the lib lng1
    * @param codiceFornitore the codice fornitore
    * @param tipoDocumento the tipo documento
    * @return the number of matching testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static int countByIdOrdineFornitoreTipo(long libLng1,
        java.lang.String codiceFornitore, java.lang.String tipoDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByIdOrdineFornitoreTipo(libLng1, codiceFornitore,
            tipoDocumento);
    }

    /**
    * Caches the testata documento in the entity cache if it is enabled.
    *
    * @param testataDocumento the testata documento
    */
    public static void cacheResult(
        it.bysoftware.ct.model.TestataDocumento testataDocumento) {
        getPersistence().cacheResult(testataDocumento);
    }

    /**
    * Caches the testata documentos in the entity cache if it is enabled.
    *
    * @param testataDocumentos the testata documentos
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.TestataDocumento> testataDocumentos) {
        getPersistence().cacheResult(testataDocumentos);
    }

    /**
    * Creates a new testata documento with the primary key. Does not add the testata documento to the database.
    *
    * @param testataDocumentoPK the primary key for the new testata documento
    * @return the new testata documento
    */
    public static it.bysoftware.ct.model.TestataDocumento create(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK) {
        return getPersistence().create(testataDocumentoPK);
    }

    /**
    * Removes the testata documento with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param testataDocumentoPK the primary key of the testata documento
    * @return the testata documento that was removed
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento remove(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence().remove(testataDocumentoPK);
    }

    public static it.bysoftware.ct.model.TestataDocumento updateImpl(
        it.bysoftware.ct.model.TestataDocumento testataDocumento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(testataDocumento);
    }

    /**
    * Returns the testata documento with the primary key or throws a {@link it.bysoftware.ct.NoSuchTestataDocumentoException} if it could not be found.
    *
    * @param testataDocumentoPK the primary key of the testata documento
    * @return the testata documento
    * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento findByPrimaryKey(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTestataDocumentoException {
        return getPersistence().findByPrimaryKey(testataDocumentoPK);
    }

    /**
    * Returns the testata documento with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param testataDocumentoPK the primary key of the testata documento
    * @return the testata documento, or <code>null</code> if a testata documento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.TestataDocumento fetchByPrimaryKey(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK testataDocumentoPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(testataDocumentoPK);
    }

    /**
    * Returns all the testata documentos.
    *
    * @return the testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the testata documentos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @return the range of testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the testata documentos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of testata documentos
    * @param end the upper bound of the range of testata documentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.TestataDocumento> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the testata documentos from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of testata documentos.
    *
    * @return the number of testata documentos
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static TestataDocumentoPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (TestataDocumentoPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    TestataDocumentoPersistence.class.getName());

            ReferenceRegistry.registerReference(TestataDocumentoUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(TestataDocumentoPersistence persistence) {
    }
}
