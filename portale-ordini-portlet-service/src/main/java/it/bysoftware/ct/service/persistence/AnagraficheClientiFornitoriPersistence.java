package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.AnagraficheClientiFornitori;

/**
 * The persistence interface for the anagrafiche clienti fornitori service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see AnagraficheClientiFornitoriPersistenceImpl
 * @see AnagraficheClientiFornitoriUtil
 * @generated
 */
public interface AnagraficheClientiFornitoriPersistence extends BasePersistence<AnagraficheClientiFornitori> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link AnagraficheClientiFornitoriUtil} to access the anagrafiche clienti fornitori persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the anagrafiche clienti fornitori in the entity cache if it is enabled.
    *
    * @param anagraficheClientiFornitori the anagrafiche clienti fornitori
    */
    public void cacheResult(
        it.bysoftware.ct.model.AnagraficheClientiFornitori anagraficheClientiFornitori);

    /**
    * Caches the anagrafiche clienti fornitoris in the entity cache if it is enabled.
    *
    * @param anagraficheClientiFornitoris the anagrafiche clienti fornitoris
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.AnagraficheClientiFornitori> anagraficheClientiFornitoris);

    /**
    * Creates a new anagrafiche clienti fornitori with the primary key. Does not add the anagrafiche clienti fornitori to the database.
    *
    * @param codiceAnagrafica the primary key for the new anagrafiche clienti fornitori
    * @return the new anagrafiche clienti fornitori
    */
    public it.bysoftware.ct.model.AnagraficheClientiFornitori create(
        java.lang.String codiceAnagrafica);

    /**
    * Removes the anagrafiche clienti fornitori with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceAnagrafica the primary key of the anagrafiche clienti fornitori
    * @return the anagrafiche clienti fornitori that was removed
    * @throws it.bysoftware.ct.NoSuchAnagraficheClientiFornitoriException if a anagrafiche clienti fornitori with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.AnagraficheClientiFornitori remove(
        java.lang.String codiceAnagrafica)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchAnagraficheClientiFornitoriException;

    public it.bysoftware.ct.model.AnagraficheClientiFornitori updateImpl(
        it.bysoftware.ct.model.AnagraficheClientiFornitori anagraficheClientiFornitori)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the anagrafiche clienti fornitori with the primary key or throws a {@link it.bysoftware.ct.NoSuchAnagraficheClientiFornitoriException} if it could not be found.
    *
    * @param codiceAnagrafica the primary key of the anagrafiche clienti fornitori
    * @return the anagrafiche clienti fornitori
    * @throws it.bysoftware.ct.NoSuchAnagraficheClientiFornitoriException if a anagrafiche clienti fornitori with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.AnagraficheClientiFornitori findByPrimaryKey(
        java.lang.String codiceAnagrafica)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchAnagraficheClientiFornitoriException;

    /**
    * Returns the anagrafiche clienti fornitori with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param codiceAnagrafica the primary key of the anagrafiche clienti fornitori
    * @return the anagrafiche clienti fornitori, or <code>null</code> if a anagrafiche clienti fornitori with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.AnagraficheClientiFornitori fetchByPrimaryKey(
        java.lang.String codiceAnagrafica)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the anagrafiche clienti fornitoris.
    *
    * @return the anagrafiche clienti fornitoris
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.AnagraficheClientiFornitori> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the anagrafiche clienti fornitoris.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficheClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of anagrafiche clienti fornitoris
    * @param end the upper bound of the range of anagrafiche clienti fornitoris (not inclusive)
    * @return the range of anagrafiche clienti fornitoris
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.AnagraficheClientiFornitori> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the anagrafiche clienti fornitoris.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficheClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of anagrafiche clienti fornitoris
    * @param end the upper bound of the range of anagrafiche clienti fornitoris (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of anagrafiche clienti fornitoris
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.AnagraficheClientiFornitori> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the anagrafiche clienti fornitoris from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of anagrafiche clienti fornitoris.
    *
    * @return the number of anagrafiche clienti fornitoris
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
