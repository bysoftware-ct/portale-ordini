package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AnagraficheClientiFornitoriLocalService}.
 *
 * @author Mario Torrisi
 * @see AnagraficheClientiFornitoriLocalService
 * @generated
 */
public class AnagraficheClientiFornitoriLocalServiceWrapper
    implements AnagraficheClientiFornitoriLocalService,
        ServiceWrapper<AnagraficheClientiFornitoriLocalService> {
    private AnagraficheClientiFornitoriLocalService _anagraficheClientiFornitoriLocalService;

    public AnagraficheClientiFornitoriLocalServiceWrapper(
        AnagraficheClientiFornitoriLocalService anagraficheClientiFornitoriLocalService) {
        _anagraficheClientiFornitoriLocalService = anagraficheClientiFornitoriLocalService;
    }

    /**
    * Adds the anagrafiche clienti fornitori to the database. Also notifies the appropriate model listeners.
    *
    * @param anagraficheClientiFornitori the anagrafiche clienti fornitori
    * @return the anagrafiche clienti fornitori that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.AnagraficheClientiFornitori addAnagraficheClientiFornitori(
        it.bysoftware.ct.model.AnagraficheClientiFornitori anagraficheClientiFornitori)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _anagraficheClientiFornitoriLocalService.addAnagraficheClientiFornitori(anagraficheClientiFornitori);
    }

    /**
    * Creates a new anagrafiche clienti fornitori with the primary key. Does not add the anagrafiche clienti fornitori to the database.
    *
    * @param codiceAnagrafica the primary key for the new anagrafiche clienti fornitori
    * @return the new anagrafiche clienti fornitori
    */
    @Override
    public it.bysoftware.ct.model.AnagraficheClientiFornitori createAnagraficheClientiFornitori(
        java.lang.String codiceAnagrafica) {
        return _anagraficheClientiFornitoriLocalService.createAnagraficheClientiFornitori(codiceAnagrafica);
    }

    /**
    * Deletes the anagrafiche clienti fornitori with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceAnagrafica the primary key of the anagrafiche clienti fornitori
    * @return the anagrafiche clienti fornitori that was removed
    * @throws PortalException if a anagrafiche clienti fornitori with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.AnagraficheClientiFornitori deleteAnagraficheClientiFornitori(
        java.lang.String codiceAnagrafica)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _anagraficheClientiFornitoriLocalService.deleteAnagraficheClientiFornitori(codiceAnagrafica);
    }

    /**
    * Deletes the anagrafiche clienti fornitori from the database. Also notifies the appropriate model listeners.
    *
    * @param anagraficheClientiFornitori the anagrafiche clienti fornitori
    * @return the anagrafiche clienti fornitori that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.AnagraficheClientiFornitori deleteAnagraficheClientiFornitori(
        it.bysoftware.ct.model.AnagraficheClientiFornitori anagraficheClientiFornitori)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _anagraficheClientiFornitoriLocalService.deleteAnagraficheClientiFornitori(anagraficheClientiFornitori);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _anagraficheClientiFornitoriLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _anagraficheClientiFornitoriLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficheClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _anagraficheClientiFornitoriLocalService.dynamicQuery(dynamicQuery,
            start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficheClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _anagraficheClientiFornitoriLocalService.dynamicQuery(dynamicQuery,
            start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _anagraficheClientiFornitoriLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _anagraficheClientiFornitoriLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public it.bysoftware.ct.model.AnagraficheClientiFornitori fetchAnagraficheClientiFornitori(
        java.lang.String codiceAnagrafica)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _anagraficheClientiFornitoriLocalService.fetchAnagraficheClientiFornitori(codiceAnagrafica);
    }

    /**
    * Returns the anagrafiche clienti fornitori with the primary key.
    *
    * @param codiceAnagrafica the primary key of the anagrafiche clienti fornitori
    * @return the anagrafiche clienti fornitori
    * @throws PortalException if a anagrafiche clienti fornitori with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.AnagraficheClientiFornitori getAnagraficheClientiFornitori(
        java.lang.String codiceAnagrafica)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _anagraficheClientiFornitoriLocalService.getAnagraficheClientiFornitori(codiceAnagrafica);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _anagraficheClientiFornitoriLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the anagrafiche clienti fornitoris.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficheClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of anagrafiche clienti fornitoris
    * @param end the upper bound of the range of anagrafiche clienti fornitoris (not inclusive)
    * @return the range of anagrafiche clienti fornitoris
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.AnagraficheClientiFornitori> getAnagraficheClientiFornitoris(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _anagraficheClientiFornitoriLocalService.getAnagraficheClientiFornitoris(start,
            end);
    }

    /**
    * Returns the number of anagrafiche clienti fornitoris.
    *
    * @return the number of anagrafiche clienti fornitoris
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getAnagraficheClientiFornitorisCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _anagraficheClientiFornitoriLocalService.getAnagraficheClientiFornitorisCount();
    }

    /**
    * Updates the anagrafiche clienti fornitori in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param anagraficheClientiFornitori the anagrafiche clienti fornitori
    * @return the anagrafiche clienti fornitori that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.AnagraficheClientiFornitori updateAnagraficheClientiFornitori(
        it.bysoftware.ct.model.AnagraficheClientiFornitori anagraficheClientiFornitori)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _anagraficheClientiFornitoriLocalService.updateAnagraficheClientiFornitori(anagraficheClientiFornitori);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _anagraficheClientiFornitoriLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _anagraficheClientiFornitoriLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _anagraficheClientiFornitoriLocalService.invokeMethod(name,
            parameterTypes, arguments);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.AnagraficheClientiFornitori> findCustomers() {
        return _anagraficheClientiFornitoriLocalService.findCustomers();
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.AnagraficheClientiFornitori> findCustomersInCategory(
        java.lang.String category) {
        return _anagraficheClientiFornitoriLocalService.findCustomersInCategory(category);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.AnagraficheClientiFornitori> findAllSuppliers() {
        return _anagraficheClientiFornitoriLocalService.findAllSuppliers();
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.AnagraficheClientiFornitori> findSuppliers() {
        return _anagraficheClientiFornitoriLocalService.findSuppliers();
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.AnagraficheClientiFornitori> findAssociates() {
        return _anagraficheClientiFornitoriLocalService.findAssociates();
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.AnagraficheClientiFornitori> getCustomers(
        java.lang.String customerCode, java.lang.String businessName,
        java.lang.String vatCode, java.lang.String country,
        java.lang.String city, boolean andSearch, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _anagraficheClientiFornitoriLocalService.getCustomers(customerCode,
            businessName, vatCode, country, city, andSearch, start, end,
            orderByComparator);
    }

    @Override
    public int getCustomersCount(java.lang.String customerCode,
        java.lang.String businessName, java.lang.String vatCode,
        java.lang.String country, java.lang.String city, boolean andSearch)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _anagraficheClientiFornitoriLocalService.getCustomersCount(customerCode,
            businessName, vatCode, country, city, andSearch);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public AnagraficheClientiFornitoriLocalService getWrappedAnagraficheClientiFornitoriLocalService() {
        return _anagraficheClientiFornitoriLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedAnagraficheClientiFornitoriLocalService(
        AnagraficheClientiFornitoriLocalService anagraficheClientiFornitoriLocalService) {
        _anagraficheClientiFornitoriLocalService = anagraficheClientiFornitoriLocalService;
    }

    @Override
    public AnagraficheClientiFornitoriLocalService getWrappedService() {
        return _anagraficheClientiFornitoriLocalService;
    }

    @Override
    public void setWrappedService(
        AnagraficheClientiFornitoriLocalService anagraficheClientiFornitoriLocalService) {
        _anagraficheClientiFornitoriLocalService = anagraficheClientiFornitoriLocalService;
    }
}
