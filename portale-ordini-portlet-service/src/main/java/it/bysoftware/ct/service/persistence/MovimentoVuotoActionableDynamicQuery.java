package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.MovimentoVuoto;
import it.bysoftware.ct.service.MovimentoVuotoLocalServiceUtil;

/**
 * @author Mario Torrisi
 * @generated
 */
public abstract class MovimentoVuotoActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public MovimentoVuotoActionableDynamicQuery() throws SystemException {
        setBaseLocalService(MovimentoVuotoLocalServiceUtil.getService());
        setClass(MovimentoVuoto.class);

        setClassLoader(it.bysoftware.ct.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("idMovimento");
    }
}
