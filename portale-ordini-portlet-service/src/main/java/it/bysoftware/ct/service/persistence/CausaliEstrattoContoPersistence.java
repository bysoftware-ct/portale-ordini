package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.CausaliEstrattoConto;

/**
 * The persistence interface for the causali estratto conto service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CausaliEstrattoContoPersistenceImpl
 * @see CausaliEstrattoContoUtil
 * @generated
 */
public interface CausaliEstrattoContoPersistence extends BasePersistence<CausaliEstrattoConto> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link CausaliEstrattoContoUtil} to access the causali estratto conto persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the causali estratto conto in the entity cache if it is enabled.
    *
    * @param causaliEstrattoConto the causali estratto conto
    */
    public void cacheResult(
        it.bysoftware.ct.model.CausaliEstrattoConto causaliEstrattoConto);

    /**
    * Caches the causali estratto contos in the entity cache if it is enabled.
    *
    * @param causaliEstrattoContos the causali estratto contos
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.CausaliEstrattoConto> causaliEstrattoContos);

    /**
    * Creates a new causali estratto conto with the primary key. Does not add the causali estratto conto to the database.
    *
    * @param codiceCausaleEC the primary key for the new causali estratto conto
    * @return the new causali estratto conto
    */
    public it.bysoftware.ct.model.CausaliEstrattoConto create(
        java.lang.String codiceCausaleEC);

    /**
    * Removes the causali estratto conto with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceCausaleEC the primary key of the causali estratto conto
    * @return the causali estratto conto that was removed
    * @throws it.bysoftware.ct.NoSuchCausaliEstrattoContoException if a causali estratto conto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.CausaliEstrattoConto remove(
        java.lang.String codiceCausaleEC)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCausaliEstrattoContoException;

    public it.bysoftware.ct.model.CausaliEstrattoConto updateImpl(
        it.bysoftware.ct.model.CausaliEstrattoConto causaliEstrattoConto)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the causali estratto conto with the primary key or throws a {@link it.bysoftware.ct.NoSuchCausaliEstrattoContoException} if it could not be found.
    *
    * @param codiceCausaleEC the primary key of the causali estratto conto
    * @return the causali estratto conto
    * @throws it.bysoftware.ct.NoSuchCausaliEstrattoContoException if a causali estratto conto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.CausaliEstrattoConto findByPrimaryKey(
        java.lang.String codiceCausaleEC)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCausaliEstrattoContoException;

    /**
    * Returns the causali estratto conto with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param codiceCausaleEC the primary key of the causali estratto conto
    * @return the causali estratto conto, or <code>null</code> if a causali estratto conto with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.CausaliEstrattoConto fetchByPrimaryKey(
        java.lang.String codiceCausaleEC)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the causali estratto contos.
    *
    * @return the causali estratto contos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.CausaliEstrattoConto> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the causali estratto contos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CausaliEstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of causali estratto contos
    * @param end the upper bound of the range of causali estratto contos (not inclusive)
    * @return the range of causali estratto contos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.CausaliEstrattoConto> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the causali estratto contos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CausaliEstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of causali estratto contos
    * @param end the upper bound of the range of causali estratto contos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of causali estratto contos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.CausaliEstrattoConto> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the causali estratto contos from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of causali estratto contos.
    *
    * @return the number of causali estratto contos
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
