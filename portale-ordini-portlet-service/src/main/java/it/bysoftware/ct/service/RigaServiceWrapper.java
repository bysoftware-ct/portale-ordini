package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link RigaService}.
 *
 * @author Mario Torrisi
 * @see RigaService
 * @generated
 */
public class RigaServiceWrapper implements RigaService,
    ServiceWrapper<RigaService> {
    private RigaService _rigaService;

    public RigaServiceWrapper(RigaService rigaService) {
        _rigaService = rigaService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _rigaService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _rigaService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _rigaService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public RigaService getWrappedRigaService() {
        return _rigaService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedRigaService(RigaService rigaService) {
        _rigaService = rigaService;
    }

    @Override
    public RigaService getWrappedService() {
        return _rigaService;
    }

    @Override
    public void setWrappedService(RigaService rigaService) {
        _rigaService = rigaService;
    }
}
