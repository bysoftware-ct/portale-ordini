package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ContatoreSocioLocalService}.
 *
 * @author Mario Torrisi
 * @see ContatoreSocioLocalService
 * @generated
 */
public class ContatoreSocioLocalServiceWrapper
    implements ContatoreSocioLocalService,
        ServiceWrapper<ContatoreSocioLocalService> {
    private ContatoreSocioLocalService _contatoreSocioLocalService;

    public ContatoreSocioLocalServiceWrapper(
        ContatoreSocioLocalService contatoreSocioLocalService) {
        _contatoreSocioLocalService = contatoreSocioLocalService;
    }

    /**
    * Adds the contatore socio to the database. Also notifies the appropriate model listeners.
    *
    * @param contatoreSocio the contatore socio
    * @return the contatore socio that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ContatoreSocio addContatoreSocio(
        it.bysoftware.ct.model.ContatoreSocio contatoreSocio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _contatoreSocioLocalService.addContatoreSocio(contatoreSocio);
    }

    /**
    * Creates a new contatore socio with the primary key. Does not add the contatore socio to the database.
    *
    * @param contatoreSocioPK the primary key for the new contatore socio
    * @return the new contatore socio
    */
    @Override
    public it.bysoftware.ct.model.ContatoreSocio createContatoreSocio(
        it.bysoftware.ct.service.persistence.ContatoreSocioPK contatoreSocioPK) {
        return _contatoreSocioLocalService.createContatoreSocio(contatoreSocioPK);
    }

    /**
    * Deletes the contatore socio with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param contatoreSocioPK the primary key of the contatore socio
    * @return the contatore socio that was removed
    * @throws PortalException if a contatore socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ContatoreSocio deleteContatoreSocio(
        it.bysoftware.ct.service.persistence.ContatoreSocioPK contatoreSocioPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _contatoreSocioLocalService.deleteContatoreSocio(contatoreSocioPK);
    }

    /**
    * Deletes the contatore socio from the database. Also notifies the appropriate model listeners.
    *
    * @param contatoreSocio the contatore socio
    * @return the contatore socio that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ContatoreSocio deleteContatoreSocio(
        it.bysoftware.ct.model.ContatoreSocio contatoreSocio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _contatoreSocioLocalService.deleteContatoreSocio(contatoreSocio);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _contatoreSocioLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _contatoreSocioLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ContatoreSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _contatoreSocioLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ContatoreSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _contatoreSocioLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _contatoreSocioLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _contatoreSocioLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public it.bysoftware.ct.model.ContatoreSocio fetchContatoreSocio(
        it.bysoftware.ct.service.persistence.ContatoreSocioPK contatoreSocioPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _contatoreSocioLocalService.fetchContatoreSocio(contatoreSocioPK);
    }

    /**
    * Returns the contatore socio with the primary key.
    *
    * @param contatoreSocioPK the primary key of the contatore socio
    * @return the contatore socio
    * @throws PortalException if a contatore socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ContatoreSocio getContatoreSocio(
        it.bysoftware.ct.service.persistence.ContatoreSocioPK contatoreSocioPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _contatoreSocioLocalService.getContatoreSocio(contatoreSocioPK);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _contatoreSocioLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the contatore socios.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ContatoreSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of contatore socios
    * @param end the upper bound of the range of contatore socios (not inclusive)
    * @return the range of contatore socios
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.ContatoreSocio> getContatoreSocios(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _contatoreSocioLocalService.getContatoreSocios(start, end);
    }

    /**
    * Returns the number of contatore socios.
    *
    * @return the number of contatore socios
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getContatoreSociosCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _contatoreSocioLocalService.getContatoreSociosCount();
    }

    /**
    * Updates the contatore socio in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param contatoreSocio the contatore socio
    * @return the contatore socio that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ContatoreSocio updateContatoreSocio(
        it.bysoftware.ct.model.ContatoreSocio contatoreSocio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _contatoreSocioLocalService.updateContatoreSocio(contatoreSocio);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _contatoreSocioLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _contatoreSocioLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _contatoreSocioLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public ContatoreSocioLocalService getWrappedContatoreSocioLocalService() {
        return _contatoreSocioLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedContatoreSocioLocalService(
        ContatoreSocioLocalService contatoreSocioLocalService) {
        _contatoreSocioLocalService = contatoreSocioLocalService;
    }

    @Override
    public ContatoreSocioLocalService getWrappedService() {
        return _contatoreSocioLocalService;
    }

    @Override
    public void setWrappedService(
        ContatoreSocioLocalService contatoreSocioLocalService) {
        _contatoreSocioLocalService = contatoreSocioLocalService;
    }
}
