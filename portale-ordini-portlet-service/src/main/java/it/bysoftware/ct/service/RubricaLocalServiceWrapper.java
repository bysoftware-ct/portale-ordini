package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link RubricaLocalService}.
 *
 * @author Mario Torrisi
 * @see RubricaLocalService
 * @generated
 */
public class RubricaLocalServiceWrapper implements RubricaLocalService,
    ServiceWrapper<RubricaLocalService> {
    private RubricaLocalService _rubricaLocalService;

    public RubricaLocalServiceWrapper(RubricaLocalService rubricaLocalService) {
        _rubricaLocalService = rubricaLocalService;
    }

    /**
    * Adds the rubrica to the database. Also notifies the appropriate model listeners.
    *
    * @param rubrica the rubrica
    * @return the rubrica that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Rubrica addRubrica(
        it.bysoftware.ct.model.Rubrica rubrica)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rubricaLocalService.addRubrica(rubrica);
    }

    /**
    * Creates a new rubrica with the primary key. Does not add the rubrica to the database.
    *
    * @param rubricaPK the primary key for the new rubrica
    * @return the new rubrica
    */
    @Override
    public it.bysoftware.ct.model.Rubrica createRubrica(
        it.bysoftware.ct.service.persistence.RubricaPK rubricaPK) {
        return _rubricaLocalService.createRubrica(rubricaPK);
    }

    /**
    * Deletes the rubrica with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param rubricaPK the primary key of the rubrica
    * @return the rubrica that was removed
    * @throws PortalException if a rubrica with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Rubrica deleteRubrica(
        it.bysoftware.ct.service.persistence.RubricaPK rubricaPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _rubricaLocalService.deleteRubrica(rubricaPK);
    }

    /**
    * Deletes the rubrica from the database. Also notifies the appropriate model listeners.
    *
    * @param rubrica the rubrica
    * @return the rubrica that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Rubrica deleteRubrica(
        it.bysoftware.ct.model.Rubrica rubrica)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rubricaLocalService.deleteRubrica(rubrica);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _rubricaLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rubricaLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RubricaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _rubricaLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RubricaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rubricaLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rubricaLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rubricaLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public it.bysoftware.ct.model.Rubrica fetchRubrica(
        it.bysoftware.ct.service.persistence.RubricaPK rubricaPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rubricaLocalService.fetchRubrica(rubricaPK);
    }

    /**
    * Returns the rubrica with the primary key.
    *
    * @param rubricaPK the primary key of the rubrica
    * @return the rubrica
    * @throws PortalException if a rubrica with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Rubrica getRubrica(
        it.bysoftware.ct.service.persistence.RubricaPK rubricaPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _rubricaLocalService.getRubrica(rubricaPK);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _rubricaLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the rubricas.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RubricaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of rubricas
    * @param end the upper bound of the range of rubricas (not inclusive)
    * @return the range of rubricas
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.Rubrica> getRubricas(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rubricaLocalService.getRubricas(start, end);
    }

    /**
    * Returns the number of rubricas.
    *
    * @return the number of rubricas
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getRubricasCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rubricaLocalService.getRubricasCount();
    }

    /**
    * Updates the rubrica in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param rubrica the rubrica
    * @return the rubrica that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.Rubrica updateRubrica(
        it.bysoftware.ct.model.Rubrica rubrica)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _rubricaLocalService.updateRubrica(rubrica);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _rubricaLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _rubricaLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _rubricaLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.Rubrica> findContacts(
        java.lang.String code) {
        return _rubricaLocalService.findContacts(code);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public RubricaLocalService getWrappedRubricaLocalService() {
        return _rubricaLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedRubricaLocalService(
        RubricaLocalService rubricaLocalService) {
        _rubricaLocalService = rubricaLocalService;
    }

    @Override
    public RubricaLocalService getWrappedService() {
        return _rubricaLocalService;
    }

    @Override
    public void setWrappedService(RubricaLocalService rubricaLocalService) {
        _rubricaLocalService = rubricaLocalService;
    }
}
