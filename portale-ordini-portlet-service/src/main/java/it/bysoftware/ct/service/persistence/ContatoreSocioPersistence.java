package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.ContatoreSocio;

/**
 * The persistence interface for the contatore socio service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see ContatoreSocioPersistenceImpl
 * @see ContatoreSocioUtil
 * @generated
 */
public interface ContatoreSocioPersistence extends BasePersistence<ContatoreSocio> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link ContatoreSocioUtil} to access the contatore socio persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the contatore socio in the entity cache if it is enabled.
    *
    * @param contatoreSocio the contatore socio
    */
    public void cacheResult(
        it.bysoftware.ct.model.ContatoreSocio contatoreSocio);

    /**
    * Caches the contatore socios in the entity cache if it is enabled.
    *
    * @param contatoreSocios the contatore socios
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.ContatoreSocio> contatoreSocios);

    /**
    * Creates a new contatore socio with the primary key. Does not add the contatore socio to the database.
    *
    * @param contatoreSocioPK the primary key for the new contatore socio
    * @return the new contatore socio
    */
    public it.bysoftware.ct.model.ContatoreSocio create(
        it.bysoftware.ct.service.persistence.ContatoreSocioPK contatoreSocioPK);

    /**
    * Removes the contatore socio with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param contatoreSocioPK the primary key of the contatore socio
    * @return the contatore socio that was removed
    * @throws it.bysoftware.ct.NoSuchContatoreSocioException if a contatore socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ContatoreSocio remove(
        it.bysoftware.ct.service.persistence.ContatoreSocioPK contatoreSocioPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchContatoreSocioException;

    public it.bysoftware.ct.model.ContatoreSocio updateImpl(
        it.bysoftware.ct.model.ContatoreSocio contatoreSocio)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the contatore socio with the primary key or throws a {@link it.bysoftware.ct.NoSuchContatoreSocioException} if it could not be found.
    *
    * @param contatoreSocioPK the primary key of the contatore socio
    * @return the contatore socio
    * @throws it.bysoftware.ct.NoSuchContatoreSocioException if a contatore socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ContatoreSocio findByPrimaryKey(
        it.bysoftware.ct.service.persistence.ContatoreSocioPK contatoreSocioPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchContatoreSocioException;

    /**
    * Returns the contatore socio with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param contatoreSocioPK the primary key of the contatore socio
    * @return the contatore socio, or <code>null</code> if a contatore socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.ContatoreSocio fetchByPrimaryKey(
        it.bysoftware.ct.service.persistence.ContatoreSocioPK contatoreSocioPK)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the contatore socios.
    *
    * @return the contatore socios
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.ContatoreSocio> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the contatore socios.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ContatoreSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of contatore socios
    * @param end the upper bound of the range of contatore socios (not inclusive)
    * @return the range of contatore socios
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.ContatoreSocio> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the contatore socios.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ContatoreSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of contatore socios
    * @param end the upper bound of the range of contatore socios (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of contatore socios
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.ContatoreSocio> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the contatore socios from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of contatore socios.
    *
    * @return the number of contatore socios
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
