package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.Stati;

import java.util.List;

/**
 * The persistence utility for the stati service. This utility wraps {@link StatiPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see StatiPersistence
 * @see StatiPersistenceImpl
 * @generated
 */
public class StatiUtil {
    private static StatiPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Stati stati) {
        getPersistence().clearCache(stati);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Stati> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Stati> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Stati> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Stati update(Stati stati) throws SystemException {
        return getPersistence().update(stati);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Stati update(Stati stati, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(stati, serviceContext);
    }

    /**
    * Caches the stati in the entity cache if it is enabled.
    *
    * @param stati the stati
    */
    public static void cacheResult(it.bysoftware.ct.model.Stati stati) {
        getPersistence().cacheResult(stati);
    }

    /**
    * Caches the statis in the entity cache if it is enabled.
    *
    * @param statis the statis
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.Stati> statis) {
        getPersistence().cacheResult(statis);
    }

    /**
    * Creates a new stati with the primary key. Does not add the stati to the database.
    *
    * @param codiceStato the primary key for the new stati
    * @return the new stati
    */
    public static it.bysoftware.ct.model.Stati create(
        java.lang.String codiceStato) {
        return getPersistence().create(codiceStato);
    }

    /**
    * Removes the stati with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceStato the primary key of the stati
    * @return the stati that was removed
    * @throws it.bysoftware.ct.NoSuchStatiException if a stati with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Stati remove(
        java.lang.String codiceStato)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchStatiException {
        return getPersistence().remove(codiceStato);
    }

    public static it.bysoftware.ct.model.Stati updateImpl(
        it.bysoftware.ct.model.Stati stati)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(stati);
    }

    /**
    * Returns the stati with the primary key or throws a {@link it.bysoftware.ct.NoSuchStatiException} if it could not be found.
    *
    * @param codiceStato the primary key of the stati
    * @return the stati
    * @throws it.bysoftware.ct.NoSuchStatiException if a stati with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Stati findByPrimaryKey(
        java.lang.String codiceStato)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchStatiException {
        return getPersistence().findByPrimaryKey(codiceStato);
    }

    /**
    * Returns the stati with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param codiceStato the primary key of the stati
    * @return the stati, or <code>null</code> if a stati with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Stati fetchByPrimaryKey(
        java.lang.String codiceStato)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(codiceStato);
    }

    /**
    * Returns all the statis.
    *
    * @return the statis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Stati> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the statis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.StatiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of statis
    * @param end the upper bound of the range of statis (not inclusive)
    * @return the range of statis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Stati> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the statis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.StatiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of statis
    * @param end the upper bound of the range of statis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of statis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Stati> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the statis from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of statis.
    *
    * @return the number of statis
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static StatiPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (StatiPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    StatiPersistence.class.getName());

            ReferenceRegistry.registerReference(StatiUtil.class, "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(StatiPersistence persistence) {
    }
}
