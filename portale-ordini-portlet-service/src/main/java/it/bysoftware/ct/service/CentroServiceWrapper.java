package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CentroService}.
 *
 * @author Mario Torrisi
 * @see CentroService
 * @generated
 */
public class CentroServiceWrapper implements CentroService,
    ServiceWrapper<CentroService> {
    private CentroService _centroService;

    public CentroServiceWrapper(CentroService centroService) {
        _centroService = centroService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _centroService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _centroService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _centroService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public CentroService getWrappedCentroService() {
        return _centroService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedCentroService(CentroService centroService) {
        _centroService = centroService;
    }

    @Override
    public CentroService getWrappedService() {
        return _centroService;
    }

    @Override
    public void setWrappedService(CentroService centroService) {
        _centroService = centroService;
    }
}
