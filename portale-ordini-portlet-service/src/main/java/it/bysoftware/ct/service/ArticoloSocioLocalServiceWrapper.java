package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ArticoloSocioLocalService}.
 *
 * @author Mario Torrisi
 * @see ArticoloSocioLocalService
 * @generated
 */
public class ArticoloSocioLocalServiceWrapper
    implements ArticoloSocioLocalService,
        ServiceWrapper<ArticoloSocioLocalService> {
    private ArticoloSocioLocalService _articoloSocioLocalService;

    public ArticoloSocioLocalServiceWrapper(
        ArticoloSocioLocalService articoloSocioLocalService) {
        _articoloSocioLocalService = articoloSocioLocalService;
    }

    /**
    * Adds the articolo socio to the database. Also notifies the appropriate model listeners.
    *
    * @param articoloSocio the articolo socio
    * @return the articolo socio that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ArticoloSocio addArticoloSocio(
        it.bysoftware.ct.model.ArticoloSocio articoloSocio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _articoloSocioLocalService.addArticoloSocio(articoloSocio);
    }

    /**
    * Creates a new articolo socio with the primary key. Does not add the articolo socio to the database.
    *
    * @param articoloSocioPK the primary key for the new articolo socio
    * @return the new articolo socio
    */
    @Override
    public it.bysoftware.ct.model.ArticoloSocio createArticoloSocio(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK) {
        return _articoloSocioLocalService.createArticoloSocio(articoloSocioPK);
    }

    /**
    * Deletes the articolo socio with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param articoloSocioPK the primary key of the articolo socio
    * @return the articolo socio that was removed
    * @throws PortalException if a articolo socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ArticoloSocio deleteArticoloSocio(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _articoloSocioLocalService.deleteArticoloSocio(articoloSocioPK);
    }

    /**
    * Deletes the articolo socio from the database. Also notifies the appropriate model listeners.
    *
    * @param articoloSocio the articolo socio
    * @return the articolo socio that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ArticoloSocio deleteArticoloSocio(
        it.bysoftware.ct.model.ArticoloSocio articoloSocio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _articoloSocioLocalService.deleteArticoloSocio(articoloSocio);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _articoloSocioLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _articoloSocioLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _articoloSocioLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _articoloSocioLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _articoloSocioLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _articoloSocioLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public it.bysoftware.ct.model.ArticoloSocio fetchArticoloSocio(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _articoloSocioLocalService.fetchArticoloSocio(articoloSocioPK);
    }

    /**
    * Returns the articolo socio with the primary key.
    *
    * @param articoloSocioPK the primary key of the articolo socio
    * @return the articolo socio
    * @throws PortalException if a articolo socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ArticoloSocio getArticoloSocio(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK articoloSocioPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _articoloSocioLocalService.getArticoloSocio(articoloSocioPK);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _articoloSocioLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the articolo socios.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of articolo socios
    * @param end the upper bound of the range of articolo socios (not inclusive)
    * @return the range of articolo socios
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<it.bysoftware.ct.model.ArticoloSocio> getArticoloSocios(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _articoloSocioLocalService.getArticoloSocios(start, end);
    }

    /**
    * Returns the number of articolo socios.
    *
    * @return the number of articolo socios
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getArticoloSociosCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _articoloSocioLocalService.getArticoloSociosCount();
    }

    /**
    * Updates the articolo socio in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param articoloSocio the articolo socio
    * @return the articolo socio that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public it.bysoftware.ct.model.ArticoloSocio updateArticoloSocio(
        it.bysoftware.ct.model.ArticoloSocio articoloSocio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _articoloSocioLocalService.updateArticoloSocio(articoloSocio);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _articoloSocioLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _articoloSocioLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _articoloSocioLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    @Override
    public it.bysoftware.ct.model.ArticoloSocio findPartnerItems(
        java.lang.String partner, java.lang.String item,
        java.lang.String variant) {
        return _articoloSocioLocalService.findPartnerItems(partner, item,
            variant);
    }

    @Override
    public it.bysoftware.ct.model.ArticoloSocio findCompanyItem(
        java.lang.String partner, java.lang.String item,
        java.lang.String variant) {
        return _articoloSocioLocalService.findCompanyItem(partner, item, variant);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.ArticoloSocio> findAllPartnerItems(
        java.lang.String partner) {
        return _articoloSocioLocalService.findAllPartnerItems(partner);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.ArticoloSocio> findPartnerItems(
        java.lang.String partner, java.lang.String itemCode,
        java.lang.String itemVariant, java.lang.String prtnerItemCode,
        java.lang.String partnerItemVariant, boolean andSearch, int start,
        int end, com.liferay.portal.kernel.util.OrderByComparator comparator) {
        return _articoloSocioLocalService.findPartnerItems(partner, itemCode,
            itemVariant, prtnerItemCode, partnerItemVariant, andSearch, start,
            end, comparator);
    }

    @Override
    public int findPartnerItemsCount(java.lang.String partner,
        java.lang.String itemCode, java.lang.String itemVariant,
        java.lang.String partnerItemCode, java.lang.String partnerItemVariant,
        boolean andSearch) {
        return _articoloSocioLocalService.findPartnerItemsCount(partner,
            itemCode, itemVariant, partnerItemCode, partnerItemVariant,
            andSearch);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public ArticoloSocioLocalService getWrappedArticoloSocioLocalService() {
        return _articoloSocioLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedArticoloSocioLocalService(
        ArticoloSocioLocalService articoloSocioLocalService) {
        _articoloSocioLocalService = articoloSocioLocalService;
    }

    @Override
    public ArticoloSocioLocalService getWrappedService() {
        return _articoloSocioLocalService;
    }

    @Override
    public void setWrappedService(
        ArticoloSocioLocalService articoloSocioLocalService) {
        _articoloSocioLocalService = articoloSocioLocalService;
    }
}
