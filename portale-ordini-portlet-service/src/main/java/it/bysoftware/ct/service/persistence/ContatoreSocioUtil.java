package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.ContatoreSocio;

import java.util.List;

/**
 * The persistence utility for the contatore socio service. This utility wraps {@link ContatoreSocioPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see ContatoreSocioPersistence
 * @see ContatoreSocioPersistenceImpl
 * @generated
 */
public class ContatoreSocioUtil {
    private static ContatoreSocioPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(ContatoreSocio contatoreSocio) {
        getPersistence().clearCache(contatoreSocio);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<ContatoreSocio> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<ContatoreSocio> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<ContatoreSocio> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static ContatoreSocio update(ContatoreSocio contatoreSocio)
        throws SystemException {
        return getPersistence().update(contatoreSocio);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static ContatoreSocio update(ContatoreSocio contatoreSocio,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(contatoreSocio, serviceContext);
    }

    /**
    * Caches the contatore socio in the entity cache if it is enabled.
    *
    * @param contatoreSocio the contatore socio
    */
    public static void cacheResult(
        it.bysoftware.ct.model.ContatoreSocio contatoreSocio) {
        getPersistence().cacheResult(contatoreSocio);
    }

    /**
    * Caches the contatore socios in the entity cache if it is enabled.
    *
    * @param contatoreSocios the contatore socios
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.ContatoreSocio> contatoreSocios) {
        getPersistence().cacheResult(contatoreSocios);
    }

    /**
    * Creates a new contatore socio with the primary key. Does not add the contatore socio to the database.
    *
    * @param contatoreSocioPK the primary key for the new contatore socio
    * @return the new contatore socio
    */
    public static it.bysoftware.ct.model.ContatoreSocio create(
        it.bysoftware.ct.service.persistence.ContatoreSocioPK contatoreSocioPK) {
        return getPersistence().create(contatoreSocioPK);
    }

    /**
    * Removes the contatore socio with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param contatoreSocioPK the primary key of the contatore socio
    * @return the contatore socio that was removed
    * @throws it.bysoftware.ct.NoSuchContatoreSocioException if a contatore socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ContatoreSocio remove(
        it.bysoftware.ct.service.persistence.ContatoreSocioPK contatoreSocioPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchContatoreSocioException {
        return getPersistence().remove(contatoreSocioPK);
    }

    public static it.bysoftware.ct.model.ContatoreSocio updateImpl(
        it.bysoftware.ct.model.ContatoreSocio contatoreSocio)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(contatoreSocio);
    }

    /**
    * Returns the contatore socio with the primary key or throws a {@link it.bysoftware.ct.NoSuchContatoreSocioException} if it could not be found.
    *
    * @param contatoreSocioPK the primary key of the contatore socio
    * @return the contatore socio
    * @throws it.bysoftware.ct.NoSuchContatoreSocioException if a contatore socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ContatoreSocio findByPrimaryKey(
        it.bysoftware.ct.service.persistence.ContatoreSocioPK contatoreSocioPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchContatoreSocioException {
        return getPersistence().findByPrimaryKey(contatoreSocioPK);
    }

    /**
    * Returns the contatore socio with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param contatoreSocioPK the primary key of the contatore socio
    * @return the contatore socio, or <code>null</code> if a contatore socio with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ContatoreSocio fetchByPrimaryKey(
        it.bysoftware.ct.service.persistence.ContatoreSocioPK contatoreSocioPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(contatoreSocioPK);
    }

    /**
    * Returns all the contatore socios.
    *
    * @return the contatore socios
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ContatoreSocio> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the contatore socios.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ContatoreSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of contatore socios
    * @param end the upper bound of the range of contatore socios (not inclusive)
    * @return the range of contatore socios
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ContatoreSocio> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the contatore socios.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ContatoreSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of contatore socios
    * @param end the upper bound of the range of contatore socios (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of contatore socios
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ContatoreSocio> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the contatore socios from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of contatore socios.
    *
    * @return the number of contatore socios
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static ContatoreSocioPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (ContatoreSocioPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    ContatoreSocioPersistence.class.getName());

            ReferenceRegistry.registerReference(ContatoreSocioUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(ContatoreSocioPersistence persistence) {
    }
}
