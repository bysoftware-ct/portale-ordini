package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.VariantiPianta;

/**
 * The persistence interface for the varianti pianta service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see VariantiPiantaPersistenceImpl
 * @see VariantiPiantaUtil
 * @generated
 */
public interface VariantiPiantaPersistence extends BasePersistence<VariantiPianta> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link VariantiPiantaUtil} to access the varianti pianta persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the varianti piantas where idPianta = &#63;.
    *
    * @param idPianta the id pianta
    * @return the matching varianti piantas
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.VariantiPianta> findByVariantiPianta(
        long idPianta)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the varianti piantas where idPianta = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VariantiPiantaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param idPianta the id pianta
    * @param start the lower bound of the range of varianti piantas
    * @param end the upper bound of the range of varianti piantas (not inclusive)
    * @return the range of matching varianti piantas
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.VariantiPianta> findByVariantiPianta(
        long idPianta, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the varianti piantas where idPianta = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VariantiPiantaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param idPianta the id pianta
    * @param start the lower bound of the range of varianti piantas
    * @param end the upper bound of the range of varianti piantas (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching varianti piantas
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.VariantiPianta> findByVariantiPianta(
        long idPianta, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first varianti pianta in the ordered set where idPianta = &#63;.
    *
    * @param idPianta the id pianta
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching varianti pianta
    * @throws it.bysoftware.ct.NoSuchVariantiPiantaException if a matching varianti pianta could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.VariantiPianta findByVariantiPianta_First(
        long idPianta,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVariantiPiantaException;

    /**
    * Returns the first varianti pianta in the ordered set where idPianta = &#63;.
    *
    * @param idPianta the id pianta
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching varianti pianta, or <code>null</code> if a matching varianti pianta could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.VariantiPianta fetchByVariantiPianta_First(
        long idPianta,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last varianti pianta in the ordered set where idPianta = &#63;.
    *
    * @param idPianta the id pianta
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching varianti pianta
    * @throws it.bysoftware.ct.NoSuchVariantiPiantaException if a matching varianti pianta could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.VariantiPianta findByVariantiPianta_Last(
        long idPianta,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVariantiPiantaException;

    /**
    * Returns the last varianti pianta in the ordered set where idPianta = &#63;.
    *
    * @param idPianta the id pianta
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching varianti pianta, or <code>null</code> if a matching varianti pianta could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.VariantiPianta fetchByVariantiPianta_Last(
        long idPianta,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the varianti piantas before and after the current varianti pianta in the ordered set where idPianta = &#63;.
    *
    * @param id the primary key of the current varianti pianta
    * @param idPianta the id pianta
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next varianti pianta
    * @throws it.bysoftware.ct.NoSuchVariantiPiantaException if a varianti pianta with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.VariantiPianta[] findByVariantiPianta_PrevAndNext(
        long id, long idPianta,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVariantiPiantaException;

    /**
    * Removes all the varianti piantas where idPianta = &#63; from the database.
    *
    * @param idPianta the id pianta
    * @throws SystemException if a system exception occurred
    */
    public void removeByVariantiPianta(long idPianta)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of varianti piantas where idPianta = &#63;.
    *
    * @param idPianta the id pianta
    * @return the number of matching varianti piantas
    * @throws SystemException if a system exception occurred
    */
    public int countByVariantiPianta(long idPianta)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the varianti pianta in the entity cache if it is enabled.
    *
    * @param variantiPianta the varianti pianta
    */
    public void cacheResult(
        it.bysoftware.ct.model.VariantiPianta variantiPianta);

    /**
    * Caches the varianti piantas in the entity cache if it is enabled.
    *
    * @param variantiPiantas the varianti piantas
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.VariantiPianta> variantiPiantas);

    /**
    * Creates a new varianti pianta with the primary key. Does not add the varianti pianta to the database.
    *
    * @param id the primary key for the new varianti pianta
    * @return the new varianti pianta
    */
    public it.bysoftware.ct.model.VariantiPianta create(long id);

    /**
    * Removes the varianti pianta with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the varianti pianta
    * @return the varianti pianta that was removed
    * @throws it.bysoftware.ct.NoSuchVariantiPiantaException if a varianti pianta with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.VariantiPianta remove(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVariantiPiantaException;

    public it.bysoftware.ct.model.VariantiPianta updateImpl(
        it.bysoftware.ct.model.VariantiPianta variantiPianta)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the varianti pianta with the primary key or throws a {@link it.bysoftware.ct.NoSuchVariantiPiantaException} if it could not be found.
    *
    * @param id the primary key of the varianti pianta
    * @return the varianti pianta
    * @throws it.bysoftware.ct.NoSuchVariantiPiantaException if a varianti pianta with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.VariantiPianta findByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVariantiPiantaException;

    /**
    * Returns the varianti pianta with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the varianti pianta
    * @return the varianti pianta, or <code>null</code> if a varianti pianta with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.VariantiPianta fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the varianti piantas.
    *
    * @return the varianti piantas
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.VariantiPianta> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the varianti piantas.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VariantiPiantaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of varianti piantas
    * @param end the upper bound of the range of varianti piantas (not inclusive)
    * @return the range of varianti piantas
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.VariantiPianta> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the varianti piantas.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VariantiPiantaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of varianti piantas
    * @param end the upper bound of the range of varianti piantas (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of varianti piantas
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.VariantiPianta> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the varianti piantas from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of varianti piantas.
    *
    * @return the number of varianti piantas
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
