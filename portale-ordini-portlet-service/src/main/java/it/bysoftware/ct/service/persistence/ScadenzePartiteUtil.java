package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.ScadenzePartite;

import java.util.List;

/**
 * The persistence utility for the scadenze partite service. This utility wraps {@link ScadenzePartitePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see ScadenzePartitePersistence
 * @see ScadenzePartitePersistenceImpl
 * @generated
 */
public class ScadenzePartiteUtil {
    private static ScadenzePartitePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(ScadenzePartite scadenzePartite) {
        getPersistence().clearCache(scadenzePartite);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<ScadenzePartite> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<ScadenzePartite> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<ScadenzePartite> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static ScadenzePartite update(ScadenzePartite scadenzePartite)
        throws SystemException {
        return getPersistence().update(scadenzePartite);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static ScadenzePartite update(ScadenzePartite scadenzePartite,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(scadenzePartite, serviceContext);
    }

    /**
    * Caches the scadenze partite in the entity cache if it is enabled.
    *
    * @param scadenzePartite the scadenze partite
    */
    public static void cacheResult(
        it.bysoftware.ct.model.ScadenzePartite scadenzePartite) {
        getPersistence().cacheResult(scadenzePartite);
    }

    /**
    * Caches the scadenze partites in the entity cache if it is enabled.
    *
    * @param scadenzePartites the scadenze partites
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.ScadenzePartite> scadenzePartites) {
        getPersistence().cacheResult(scadenzePartites);
    }

    /**
    * Creates a new scadenze partite with the primary key. Does not add the scadenze partite to the database.
    *
    * @param scadenzePartitePK the primary key for the new scadenze partite
    * @return the new scadenze partite
    */
    public static it.bysoftware.ct.model.ScadenzePartite create(
        it.bysoftware.ct.service.persistence.ScadenzePartitePK scadenzePartitePK) {
        return getPersistence().create(scadenzePartitePK);
    }

    /**
    * Removes the scadenze partite with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param scadenzePartitePK the primary key of the scadenze partite
    * @return the scadenze partite that was removed
    * @throws it.bysoftware.ct.NoSuchScadenzePartiteException if a scadenze partite with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ScadenzePartite remove(
        it.bysoftware.ct.service.persistence.ScadenzePartitePK scadenzePartitePK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchScadenzePartiteException {
        return getPersistence().remove(scadenzePartitePK);
    }

    public static it.bysoftware.ct.model.ScadenzePartite updateImpl(
        it.bysoftware.ct.model.ScadenzePartite scadenzePartite)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(scadenzePartite);
    }

    /**
    * Returns the scadenze partite with the primary key or throws a {@link it.bysoftware.ct.NoSuchScadenzePartiteException} if it could not be found.
    *
    * @param scadenzePartitePK the primary key of the scadenze partite
    * @return the scadenze partite
    * @throws it.bysoftware.ct.NoSuchScadenzePartiteException if a scadenze partite with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ScadenzePartite findByPrimaryKey(
        it.bysoftware.ct.service.persistence.ScadenzePartitePK scadenzePartitePK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchScadenzePartiteException {
        return getPersistence().findByPrimaryKey(scadenzePartitePK);
    }

    /**
    * Returns the scadenze partite with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param scadenzePartitePK the primary key of the scadenze partite
    * @return the scadenze partite, or <code>null</code> if a scadenze partite with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ScadenzePartite fetchByPrimaryKey(
        it.bysoftware.ct.service.persistence.ScadenzePartitePK scadenzePartitePK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(scadenzePartitePK);
    }

    /**
    * Returns all the scadenze partites.
    *
    * @return the scadenze partites
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ScadenzePartite> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the scadenze partites.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ScadenzePartiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of scadenze partites
    * @param end the upper bound of the range of scadenze partites (not inclusive)
    * @return the range of scadenze partites
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ScadenzePartite> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the scadenze partites.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ScadenzePartiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of scadenze partites
    * @param end the upper bound of the range of scadenze partites (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of scadenze partites
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ScadenzePartite> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the scadenze partites from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of scadenze partites.
    *
    * @return the number of scadenze partites
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static ScadenzePartitePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (ScadenzePartitePersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    ScadenzePartitePersistence.class.getName());

            ReferenceRegistry.registerReference(ScadenzePartiteUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(ScadenzePartitePersistence persistence) {
    }
}
