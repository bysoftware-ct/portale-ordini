package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.TipoCarrello;

/**
 * The persistence interface for the tipo carrello service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see TipoCarrelloPersistenceImpl
 * @see TipoCarrelloUtil
 * @generated
 */
public interface TipoCarrelloPersistence extends BasePersistence<TipoCarrello> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link TipoCarrelloUtil} to access the tipo carrello persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the tipo carrello in the entity cache if it is enabled.
    *
    * @param tipoCarrello the tipo carrello
    */
    public void cacheResult(it.bysoftware.ct.model.TipoCarrello tipoCarrello);

    /**
    * Caches the tipo carrellos in the entity cache if it is enabled.
    *
    * @param tipoCarrellos the tipo carrellos
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.TipoCarrello> tipoCarrellos);

    /**
    * Creates a new tipo carrello with the primary key. Does not add the tipo carrello to the database.
    *
    * @param id the primary key for the new tipo carrello
    * @return the new tipo carrello
    */
    public it.bysoftware.ct.model.TipoCarrello create(long id);

    /**
    * Removes the tipo carrello with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the tipo carrello
    * @return the tipo carrello that was removed
    * @throws it.bysoftware.ct.NoSuchTipoCarrelloException if a tipo carrello with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TipoCarrello remove(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTipoCarrelloException;

    public it.bysoftware.ct.model.TipoCarrello updateImpl(
        it.bysoftware.ct.model.TipoCarrello tipoCarrello)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the tipo carrello with the primary key or throws a {@link it.bysoftware.ct.NoSuchTipoCarrelloException} if it could not be found.
    *
    * @param id the primary key of the tipo carrello
    * @return the tipo carrello
    * @throws it.bysoftware.ct.NoSuchTipoCarrelloException if a tipo carrello with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TipoCarrello findByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchTipoCarrelloException;

    /**
    * Returns the tipo carrello with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the tipo carrello
    * @return the tipo carrello, or <code>null</code> if a tipo carrello with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.TipoCarrello fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the tipo carrellos.
    *
    * @return the tipo carrellos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TipoCarrello> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the tipo carrellos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TipoCarrelloModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of tipo carrellos
    * @param end the upper bound of the range of tipo carrellos (not inclusive)
    * @return the range of tipo carrellos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TipoCarrello> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the tipo carrellos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TipoCarrelloModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of tipo carrellos
    * @param end the upper bound of the range of tipo carrellos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of tipo carrellos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.TipoCarrello> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the tipo carrellos from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of tipo carrellos.
    *
    * @return the number of tipo carrellos
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
