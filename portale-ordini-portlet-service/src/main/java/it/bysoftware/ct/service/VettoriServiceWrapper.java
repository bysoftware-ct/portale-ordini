package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VettoriService}.
 *
 * @author Mario Torrisi
 * @see VettoriService
 * @generated
 */
public class VettoriServiceWrapper implements VettoriService,
    ServiceWrapper<VettoriService> {
    private VettoriService _vettoriService;

    public VettoriServiceWrapper(VettoriService vettoriService) {
        _vettoriService = vettoriService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _vettoriService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _vettoriService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _vettoriService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public VettoriService getWrappedVettoriService() {
        return _vettoriService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedVettoriService(VettoriService vettoriService) {
        _vettoriService = vettoriService;
    }

    @Override
    public VettoriService getWrappedService() {
        return _vettoriService;
    }

    @Override
    public void setWrappedService(VettoriService vettoriService) {
        _vettoriService = vettoriService;
    }
}
