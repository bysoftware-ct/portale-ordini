package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.DescrizioniVarianti;

import java.util.List;

/**
 * The persistence utility for the descrizioni varianti service. This utility wraps {@link DescrizioniVariantiPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see DescrizioniVariantiPersistence
 * @see DescrizioniVariantiPersistenceImpl
 * @generated
 */
public class DescrizioniVariantiUtil {
    private static DescrizioniVariantiPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(DescrizioniVarianti descrizioniVarianti) {
        getPersistence().clearCache(descrizioniVarianti);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<DescrizioniVarianti> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<DescrizioniVarianti> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<DescrizioniVarianti> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static DescrizioniVarianti update(
        DescrizioniVarianti descrizioniVarianti) throws SystemException {
        return getPersistence().update(descrizioniVarianti);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static DescrizioniVarianti update(
        DescrizioniVarianti descrizioniVarianti, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(descrizioniVarianti, serviceContext);
    }

    /**
    * Returns all the descrizioni variantis where codiceArticolo = &#63;.
    *
    * @param codiceArticolo the codice articolo
    * @return the matching descrizioni variantis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.DescrizioniVarianti> findByCodiceArticolo(
        java.lang.String codiceArticolo)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByCodiceArticolo(codiceArticolo);
    }

    /**
    * Returns a range of all the descrizioni variantis where codiceArticolo = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DescrizioniVariantiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceArticolo the codice articolo
    * @param start the lower bound of the range of descrizioni variantis
    * @param end the upper bound of the range of descrizioni variantis (not inclusive)
    * @return the range of matching descrizioni variantis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.DescrizioniVarianti> findByCodiceArticolo(
        java.lang.String codiceArticolo, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByCodiceArticolo(codiceArticolo, start, end);
    }

    /**
    * Returns an ordered range of all the descrizioni variantis where codiceArticolo = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DescrizioniVariantiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceArticolo the codice articolo
    * @param start the lower bound of the range of descrizioni variantis
    * @param end the upper bound of the range of descrizioni variantis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching descrizioni variantis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.DescrizioniVarianti> findByCodiceArticolo(
        java.lang.String codiceArticolo, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCodiceArticolo(codiceArticolo, start, end,
            orderByComparator);
    }

    /**
    * Returns the first descrizioni varianti in the ordered set where codiceArticolo = &#63;.
    *
    * @param codiceArticolo the codice articolo
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching descrizioni varianti
    * @throws it.bysoftware.ct.NoSuchDescrizioniVariantiException if a matching descrizioni varianti could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DescrizioniVarianti findByCodiceArticolo_First(
        java.lang.String codiceArticolo,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchDescrizioniVariantiException {
        return getPersistence()
                   .findByCodiceArticolo_First(codiceArticolo, orderByComparator);
    }

    /**
    * Returns the first descrizioni varianti in the ordered set where codiceArticolo = &#63;.
    *
    * @param codiceArticolo the codice articolo
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching descrizioni varianti, or <code>null</code> if a matching descrizioni varianti could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DescrizioniVarianti fetchByCodiceArticolo_First(
        java.lang.String codiceArticolo,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCodiceArticolo_First(codiceArticolo,
            orderByComparator);
    }

    /**
    * Returns the last descrizioni varianti in the ordered set where codiceArticolo = &#63;.
    *
    * @param codiceArticolo the codice articolo
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching descrizioni varianti
    * @throws it.bysoftware.ct.NoSuchDescrizioniVariantiException if a matching descrizioni varianti could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DescrizioniVarianti findByCodiceArticolo_Last(
        java.lang.String codiceArticolo,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchDescrizioniVariantiException {
        return getPersistence()
                   .findByCodiceArticolo_Last(codiceArticolo, orderByComparator);
    }

    /**
    * Returns the last descrizioni varianti in the ordered set where codiceArticolo = &#63;.
    *
    * @param codiceArticolo the codice articolo
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching descrizioni varianti, or <code>null</code> if a matching descrizioni varianti could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DescrizioniVarianti fetchByCodiceArticolo_Last(
        java.lang.String codiceArticolo,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCodiceArticolo_Last(codiceArticolo, orderByComparator);
    }

    /**
    * Returns the descrizioni variantis before and after the current descrizioni varianti in the ordered set where codiceArticolo = &#63;.
    *
    * @param descrizioniVariantiPK the primary key of the current descrizioni varianti
    * @param codiceArticolo the codice articolo
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next descrizioni varianti
    * @throws it.bysoftware.ct.NoSuchDescrizioniVariantiException if a descrizioni varianti with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DescrizioniVarianti[] findByCodiceArticolo_PrevAndNext(
        it.bysoftware.ct.service.persistence.DescrizioniVariantiPK descrizioniVariantiPK,
        java.lang.String codiceArticolo,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchDescrizioniVariantiException {
        return getPersistence()
                   .findByCodiceArticolo_PrevAndNext(descrizioniVariantiPK,
            codiceArticolo, orderByComparator);
    }

    /**
    * Removes all the descrizioni variantis where codiceArticolo = &#63; from the database.
    *
    * @param codiceArticolo the codice articolo
    * @throws SystemException if a system exception occurred
    */
    public static void removeByCodiceArticolo(java.lang.String codiceArticolo)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByCodiceArticolo(codiceArticolo);
    }

    /**
    * Returns the number of descrizioni variantis where codiceArticolo = &#63;.
    *
    * @param codiceArticolo the codice articolo
    * @return the number of matching descrizioni variantis
    * @throws SystemException if a system exception occurred
    */
    public static int countByCodiceArticolo(java.lang.String codiceArticolo)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByCodiceArticolo(codiceArticolo);
    }

    /**
    * Caches the descrizioni varianti in the entity cache if it is enabled.
    *
    * @param descrizioniVarianti the descrizioni varianti
    */
    public static void cacheResult(
        it.bysoftware.ct.model.DescrizioniVarianti descrizioniVarianti) {
        getPersistence().cacheResult(descrizioniVarianti);
    }

    /**
    * Caches the descrizioni variantis in the entity cache if it is enabled.
    *
    * @param descrizioniVariantis the descrizioni variantis
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.DescrizioniVarianti> descrizioniVariantis) {
        getPersistence().cacheResult(descrizioniVariantis);
    }

    /**
    * Creates a new descrizioni varianti with the primary key. Does not add the descrizioni varianti to the database.
    *
    * @param descrizioniVariantiPK the primary key for the new descrizioni varianti
    * @return the new descrizioni varianti
    */
    public static it.bysoftware.ct.model.DescrizioniVarianti create(
        it.bysoftware.ct.service.persistence.DescrizioniVariantiPK descrizioniVariantiPK) {
        return getPersistence().create(descrizioniVariantiPK);
    }

    /**
    * Removes the descrizioni varianti with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param descrizioniVariantiPK the primary key of the descrizioni varianti
    * @return the descrizioni varianti that was removed
    * @throws it.bysoftware.ct.NoSuchDescrizioniVariantiException if a descrizioni varianti with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DescrizioniVarianti remove(
        it.bysoftware.ct.service.persistence.DescrizioniVariantiPK descrizioniVariantiPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchDescrizioniVariantiException {
        return getPersistence().remove(descrizioniVariantiPK);
    }

    public static it.bysoftware.ct.model.DescrizioniVarianti updateImpl(
        it.bysoftware.ct.model.DescrizioniVarianti descrizioniVarianti)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(descrizioniVarianti);
    }

    /**
    * Returns the descrizioni varianti with the primary key or throws a {@link it.bysoftware.ct.NoSuchDescrizioniVariantiException} if it could not be found.
    *
    * @param descrizioniVariantiPK the primary key of the descrizioni varianti
    * @return the descrizioni varianti
    * @throws it.bysoftware.ct.NoSuchDescrizioniVariantiException if a descrizioni varianti with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DescrizioniVarianti findByPrimaryKey(
        it.bysoftware.ct.service.persistence.DescrizioniVariantiPK descrizioniVariantiPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchDescrizioniVariantiException {
        return getPersistence().findByPrimaryKey(descrizioniVariantiPK);
    }

    /**
    * Returns the descrizioni varianti with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param descrizioniVariantiPK the primary key of the descrizioni varianti
    * @return the descrizioni varianti, or <code>null</code> if a descrizioni varianti with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.DescrizioniVarianti fetchByPrimaryKey(
        it.bysoftware.ct.service.persistence.DescrizioniVariantiPK descrizioniVariantiPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(descrizioniVariantiPK);
    }

    /**
    * Returns all the descrizioni variantis.
    *
    * @return the descrizioni variantis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.DescrizioniVarianti> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the descrizioni variantis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DescrizioniVariantiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of descrizioni variantis
    * @param end the upper bound of the range of descrizioni variantis (not inclusive)
    * @return the range of descrizioni variantis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.DescrizioniVarianti> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the descrizioni variantis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DescrizioniVariantiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of descrizioni variantis
    * @param end the upper bound of the range of descrizioni variantis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of descrizioni variantis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.DescrizioniVarianti> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the descrizioni variantis from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of descrizioni variantis.
    *
    * @return the number of descrizioni variantis
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static DescrizioniVariantiPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (DescrizioniVariantiPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    DescrizioniVariantiPersistence.class.getName());

            ReferenceRegistry.registerReference(DescrizioniVariantiUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(DescrizioniVariantiPersistence persistence) {
    }
}
