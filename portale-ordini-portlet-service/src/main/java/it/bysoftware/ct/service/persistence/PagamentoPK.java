package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class PagamentoPK implements Comparable<PagamentoPK>, Serializable {
    public int id;
    public int anno;
    public String codiceSoggetto;

    public PagamentoPK() {
    }

    public PagamentoPK(int id, int anno, String codiceSoggetto) {
        this.id = id;
        this.anno = anno;
        this.codiceSoggetto = codiceSoggetto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    public String getCodiceSoggetto() {
        return codiceSoggetto;
    }

    public void setCodiceSoggetto(String codiceSoggetto) {
        this.codiceSoggetto = codiceSoggetto;
    }

    @Override
    public int compareTo(PagamentoPK pk) {
        if (pk == null) {
            return -1;
        }

        int value = 0;

        if (id < pk.id) {
            value = -1;
        } else if (id > pk.id) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        if (anno < pk.anno) {
            value = -1;
        } else if (anno > pk.anno) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        value = codiceSoggetto.compareTo(pk.codiceSoggetto);

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PagamentoPK)) {
            return false;
        }

        PagamentoPK pk = (PagamentoPK) obj;

        if ((id == pk.id) && (anno == pk.anno) &&
                (codiceSoggetto.equals(pk.codiceSoggetto))) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (String.valueOf(id) + String.valueOf(anno) +
        String.valueOf(codiceSoggetto)).hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(15);

        sb.append(StringPool.OPEN_CURLY_BRACE);

        sb.append("id");
        sb.append(StringPool.EQUAL);
        sb.append(id);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("anno");
        sb.append(StringPool.EQUAL);
        sb.append(anno);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("codiceSoggetto");
        sb.append(StringPool.EQUAL);
        sb.append(codiceSoggetto);

        sb.append(StringPool.CLOSE_CURLY_BRACE);

        return sb.toString();
    }
}
