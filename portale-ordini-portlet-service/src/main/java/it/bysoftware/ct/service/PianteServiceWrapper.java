package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PianteService}.
 *
 * @author Mario Torrisi
 * @see PianteService
 * @generated
 */
public class PianteServiceWrapper implements PianteService,
    ServiceWrapper<PianteService> {
    private PianteService _pianteService;

    public PianteServiceWrapper(PianteService pianteService) {
        _pianteService = pianteService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _pianteService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _pianteService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _pianteService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public PianteService getWrappedPianteService() {
        return _pianteService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedPianteService(PianteService pianteService) {
        _pianteService = pianteService;
    }

    @Override
    public PianteService getWrappedService() {
        return _pianteService;
    }

    @Override
    public void setWrappedService(PianteService pianteService) {
        _pianteService = pianteService;
    }
}
