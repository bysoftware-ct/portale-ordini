package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.Piante;
import it.bysoftware.ct.service.PianteLocalServiceUtil;

/**
 * @author Mario Torrisi
 * @generated
 */
public abstract class PianteActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public PianteActionableDynamicQuery() throws SystemException {
        setBaseLocalService(PianteLocalServiceUtil.getService());
        setClass(Piante.class);

        setClassLoader(it.bysoftware.ct.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
