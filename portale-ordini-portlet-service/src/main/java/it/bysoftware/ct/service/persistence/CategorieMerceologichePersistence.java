package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.CategorieMerceologiche;

/**
 * The persistence interface for the categorie merceologiche service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CategorieMerceologichePersistenceImpl
 * @see CategorieMerceologicheUtil
 * @generated
 */
public interface CategorieMerceologichePersistence extends BasePersistence<CategorieMerceologiche> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link CategorieMerceologicheUtil} to access the categorie merceologiche persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the categorie merceologiche in the entity cache if it is enabled.
    *
    * @param categorieMerceologiche the categorie merceologiche
    */
    public void cacheResult(
        it.bysoftware.ct.model.CategorieMerceologiche categorieMerceologiche);

    /**
    * Caches the categorie merceologiches in the entity cache if it is enabled.
    *
    * @param categorieMerceologiches the categorie merceologiches
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.CategorieMerceologiche> categorieMerceologiches);

    /**
    * Creates a new categorie merceologiche with the primary key. Does not add the categorie merceologiche to the database.
    *
    * @param codiceCategoria the primary key for the new categorie merceologiche
    * @return the new categorie merceologiche
    */
    public it.bysoftware.ct.model.CategorieMerceologiche create(
        java.lang.String codiceCategoria);

    /**
    * Removes the categorie merceologiche with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceCategoria the primary key of the categorie merceologiche
    * @return the categorie merceologiche that was removed
    * @throws it.bysoftware.ct.NoSuchCategorieMerceologicheException if a categorie merceologiche with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.CategorieMerceologiche remove(
        java.lang.String codiceCategoria)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCategorieMerceologicheException;

    public it.bysoftware.ct.model.CategorieMerceologiche updateImpl(
        it.bysoftware.ct.model.CategorieMerceologiche categorieMerceologiche)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the categorie merceologiche with the primary key or throws a {@link it.bysoftware.ct.NoSuchCategorieMerceologicheException} if it could not be found.
    *
    * @param codiceCategoria the primary key of the categorie merceologiche
    * @return the categorie merceologiche
    * @throws it.bysoftware.ct.NoSuchCategorieMerceologicheException if a categorie merceologiche with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.CategorieMerceologiche findByPrimaryKey(
        java.lang.String codiceCategoria)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCategorieMerceologicheException;

    /**
    * Returns the categorie merceologiche with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param codiceCategoria the primary key of the categorie merceologiche
    * @return the categorie merceologiche, or <code>null</code> if a categorie merceologiche with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.CategorieMerceologiche fetchByPrimaryKey(
        java.lang.String codiceCategoria)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the categorie merceologiches.
    *
    * @return the categorie merceologiches
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.CategorieMerceologiche> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the categorie merceologiches.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CategorieMerceologicheModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of categorie merceologiches
    * @param end the upper bound of the range of categorie merceologiches (not inclusive)
    * @return the range of categorie merceologiches
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.CategorieMerceologiche> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the categorie merceologiches.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CategorieMerceologicheModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of categorie merceologiches
    * @param end the upper bound of the range of categorie merceologiches (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of categorie merceologiches
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.CategorieMerceologiche> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the categorie merceologiches from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of categorie merceologiches.
    *
    * @return the number of categorie merceologiches
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
