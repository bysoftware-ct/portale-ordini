package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.SpettanzeSoci;
import it.bysoftware.ct.service.SpettanzeSociLocalServiceUtil;

/**
 * @author Mario Torrisi
 * @generated
 */
public abstract class SpettanzeSociActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public SpettanzeSociActionableDynamicQuery() throws SystemException {
        setBaseLocalService(SpettanzeSociLocalServiceUtil.getService());
        setClass(SpettanzeSoci.class);

        setClassLoader(it.bysoftware.ct.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
