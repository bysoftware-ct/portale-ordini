package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.Varianti;

import java.util.List;

/**
 * The persistence utility for the varianti service. This utility wraps {@link VariantiPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see VariantiPersistence
 * @see VariantiPersistenceImpl
 * @generated
 */
public class VariantiUtil {
    private static VariantiPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Varianti varianti) {
        getPersistence().clearCache(varianti);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Varianti> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Varianti> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Varianti> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Varianti update(Varianti varianti) throws SystemException {
        return getPersistence().update(varianti);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Varianti update(Varianti varianti,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(varianti, serviceContext);
    }

    /**
    * Returns all the variantis where tipoVariante = &#63;.
    *
    * @param tipoVariante the tipo variante
    * @return the matching variantis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Varianti> findByTipoVariante(
        java.lang.String tipoVariante)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByTipoVariante(tipoVariante);
    }

    /**
    * Returns a range of all the variantis where tipoVariante = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VariantiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param tipoVariante the tipo variante
    * @param start the lower bound of the range of variantis
    * @param end the upper bound of the range of variantis (not inclusive)
    * @return the range of matching variantis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Varianti> findByTipoVariante(
        java.lang.String tipoVariante, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByTipoVariante(tipoVariante, start, end);
    }

    /**
    * Returns an ordered range of all the variantis where tipoVariante = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VariantiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param tipoVariante the tipo variante
    * @param start the lower bound of the range of variantis
    * @param end the upper bound of the range of variantis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching variantis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Varianti> findByTipoVariante(
        java.lang.String tipoVariante, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByTipoVariante(tipoVariante, start, end,
            orderByComparator);
    }

    /**
    * Returns the first varianti in the ordered set where tipoVariante = &#63;.
    *
    * @param tipoVariante the tipo variante
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching varianti
    * @throws it.bysoftware.ct.NoSuchVariantiException if a matching varianti could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Varianti findByTipoVariante_First(
        java.lang.String tipoVariante,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVariantiException {
        return getPersistence()
                   .findByTipoVariante_First(tipoVariante, orderByComparator);
    }

    /**
    * Returns the first varianti in the ordered set where tipoVariante = &#63;.
    *
    * @param tipoVariante the tipo variante
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching varianti, or <code>null</code> if a matching varianti could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Varianti fetchByTipoVariante_First(
        java.lang.String tipoVariante,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByTipoVariante_First(tipoVariante, orderByComparator);
    }

    /**
    * Returns the last varianti in the ordered set where tipoVariante = &#63;.
    *
    * @param tipoVariante the tipo variante
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching varianti
    * @throws it.bysoftware.ct.NoSuchVariantiException if a matching varianti could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Varianti findByTipoVariante_Last(
        java.lang.String tipoVariante,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVariantiException {
        return getPersistence()
                   .findByTipoVariante_Last(tipoVariante, orderByComparator);
    }

    /**
    * Returns the last varianti in the ordered set where tipoVariante = &#63;.
    *
    * @param tipoVariante the tipo variante
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching varianti, or <code>null</code> if a matching varianti could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Varianti fetchByTipoVariante_Last(
        java.lang.String tipoVariante,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByTipoVariante_Last(tipoVariante, orderByComparator);
    }

    /**
    * Returns the variantis before and after the current varianti in the ordered set where tipoVariante = &#63;.
    *
    * @param variantiPK the primary key of the current varianti
    * @param tipoVariante the tipo variante
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next varianti
    * @throws it.bysoftware.ct.NoSuchVariantiException if a varianti with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Varianti[] findByTipoVariante_PrevAndNext(
        it.bysoftware.ct.service.persistence.VariantiPK variantiPK,
        java.lang.String tipoVariante,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVariantiException {
        return getPersistence()
                   .findByTipoVariante_PrevAndNext(variantiPK, tipoVariante,
            orderByComparator);
    }

    /**
    * Removes all the variantis where tipoVariante = &#63; from the database.
    *
    * @param tipoVariante the tipo variante
    * @throws SystemException if a system exception occurred
    */
    public static void removeByTipoVariante(java.lang.String tipoVariante)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByTipoVariante(tipoVariante);
    }

    /**
    * Returns the number of variantis where tipoVariante = &#63;.
    *
    * @param tipoVariante the tipo variante
    * @return the number of matching variantis
    * @throws SystemException if a system exception occurred
    */
    public static int countByTipoVariante(java.lang.String tipoVariante)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByTipoVariante(tipoVariante);
    }

    /**
    * Caches the varianti in the entity cache if it is enabled.
    *
    * @param varianti the varianti
    */
    public static void cacheResult(it.bysoftware.ct.model.Varianti varianti) {
        getPersistence().cacheResult(varianti);
    }

    /**
    * Caches the variantis in the entity cache if it is enabled.
    *
    * @param variantis the variantis
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.Varianti> variantis) {
        getPersistence().cacheResult(variantis);
    }

    /**
    * Creates a new varianti with the primary key. Does not add the varianti to the database.
    *
    * @param variantiPK the primary key for the new varianti
    * @return the new varianti
    */
    public static it.bysoftware.ct.model.Varianti create(
        it.bysoftware.ct.service.persistence.VariantiPK variantiPK) {
        return getPersistence().create(variantiPK);
    }

    /**
    * Removes the varianti with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param variantiPK the primary key of the varianti
    * @return the varianti that was removed
    * @throws it.bysoftware.ct.NoSuchVariantiException if a varianti with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Varianti remove(
        it.bysoftware.ct.service.persistence.VariantiPK variantiPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVariantiException {
        return getPersistence().remove(variantiPK);
    }

    public static it.bysoftware.ct.model.Varianti updateImpl(
        it.bysoftware.ct.model.Varianti varianti)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(varianti);
    }

    /**
    * Returns the varianti with the primary key or throws a {@link it.bysoftware.ct.NoSuchVariantiException} if it could not be found.
    *
    * @param variantiPK the primary key of the varianti
    * @return the varianti
    * @throws it.bysoftware.ct.NoSuchVariantiException if a varianti with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Varianti findByPrimaryKey(
        it.bysoftware.ct.service.persistence.VariantiPK variantiPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVariantiException {
        return getPersistence().findByPrimaryKey(variantiPK);
    }

    /**
    * Returns the varianti with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param variantiPK the primary key of the varianti
    * @return the varianti, or <code>null</code> if a varianti with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Varianti fetchByPrimaryKey(
        it.bysoftware.ct.service.persistence.VariantiPK variantiPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(variantiPK);
    }

    /**
    * Returns all the variantis.
    *
    * @return the variantis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Varianti> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the variantis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VariantiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of variantis
    * @param end the upper bound of the range of variantis (not inclusive)
    * @return the range of variantis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Varianti> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the variantis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VariantiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of variantis
    * @param end the upper bound of the range of variantis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of variantis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Varianti> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the variantis from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of variantis.
    *
    * @return the number of variantis
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static VariantiPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (VariantiPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    VariantiPersistence.class.getName());

            ReferenceRegistry.registerReference(VariantiUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(VariantiPersistence persistence) {
    }
}
