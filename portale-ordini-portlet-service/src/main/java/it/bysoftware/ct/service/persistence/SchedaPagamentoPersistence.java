package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.SchedaPagamento;

/**
 * The persistence interface for the scheda pagamento service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see SchedaPagamentoPersistenceImpl
 * @see SchedaPagamentoUtil
 * @generated
 */
public interface SchedaPagamentoPersistence extends BasePersistence<SchedaPagamento> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link SchedaPagamentoUtil} to access the scheda pagamento persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @return the matching scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SchedaPagamento> findByCodiceFornitoreAnnoIdPagamento(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param start the lower bound of the range of scheda pagamentos
    * @param end the upper bound of the range of scheda pagamentos (not inclusive)
    * @return the range of matching scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SchedaPagamento> findByCodiceFornitoreAnnoIdPagamento(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param start the lower bound of the range of scheda pagamentos
    * @param end the upper bound of the range of scheda pagamentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SchedaPagamento> findByCodiceFornitoreAnnoIdPagamento(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching scheda pagamento
    * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a matching scheda pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SchedaPagamento findByCodiceFornitoreAnnoIdPagamento_First(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSchedaPagamentoException;

    /**
    * Returns the first scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching scheda pagamento, or <code>null</code> if a matching scheda pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SchedaPagamento fetchByCodiceFornitoreAnnoIdPagamento_First(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching scheda pagamento
    * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a matching scheda pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SchedaPagamento findByCodiceFornitoreAnnoIdPagamento_Last(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSchedaPagamentoException;

    /**
    * Returns the last scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching scheda pagamento, or <code>null</code> if a matching scheda pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SchedaPagamento fetchByCodiceFornitoreAnnoIdPagamento_Last(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the scheda pagamentos before and after the current scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param schedaPagamentoPK the primary key of the current scheda pagamento
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next scheda pagamento
    * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a scheda pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SchedaPagamento[] findByCodiceFornitoreAnnoIdPagamento_PrevAndNext(
        it.bysoftware.ct.service.persistence.SchedaPagamentoPK schedaPagamentoPK,
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSchedaPagamentoException;

    /**
    * Removes all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; from the database.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @throws SystemException if a system exception occurred
    */
    public void removeByCodiceFornitoreAnnoIdPagamento(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @return the number of matching scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public int countByCodiceFornitoreAnnoIdPagamento(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @return the matching scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SchedaPagamento> findByCodiceFornitoreAnnoIdPagamentoStato(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @param start the lower bound of the range of scheda pagamentos
    * @param end the upper bound of the range of scheda pagamentos (not inclusive)
    * @return the range of matching scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SchedaPagamento> findByCodiceFornitoreAnnoIdPagamentoStato(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @param start the lower bound of the range of scheda pagamentos
    * @param end the upper bound of the range of scheda pagamentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SchedaPagamento> findByCodiceFornitoreAnnoIdPagamentoStato(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching scheda pagamento
    * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a matching scheda pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SchedaPagamento findByCodiceFornitoreAnnoIdPagamentoStato_First(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSchedaPagamentoException;

    /**
    * Returns the first scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching scheda pagamento, or <code>null</code> if a matching scheda pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SchedaPagamento fetchByCodiceFornitoreAnnoIdPagamentoStato_First(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching scheda pagamento
    * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a matching scheda pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SchedaPagamento findByCodiceFornitoreAnnoIdPagamentoStato_Last(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSchedaPagamentoException;

    /**
    * Returns the last scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching scheda pagamento, or <code>null</code> if a matching scheda pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SchedaPagamento fetchByCodiceFornitoreAnnoIdPagamentoStato_Last(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the scheda pagamentos before and after the current scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
    *
    * @param schedaPagamentoPK the primary key of the current scheda pagamento
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next scheda pagamento
    * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a scheda pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SchedaPagamento[] findByCodiceFornitoreAnnoIdPagamentoStato_PrevAndNext(
        it.bysoftware.ct.service.persistence.SchedaPagamentoPK schedaPagamentoPK,
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSchedaPagamentoException;

    /**
    * Removes all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63; from the database.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @throws SystemException if a system exception occurred
    */
    public void removeByCodiceFornitoreAnnoIdPagamentoStato(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @return the number of matching scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public int countByCodiceFornitoreAnnoIdPagamentoStato(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the scheda pagamento in the entity cache if it is enabled.
    *
    * @param schedaPagamento the scheda pagamento
    */
    public void cacheResult(
        it.bysoftware.ct.model.SchedaPagamento schedaPagamento);

    /**
    * Caches the scheda pagamentos in the entity cache if it is enabled.
    *
    * @param schedaPagamentos the scheda pagamentos
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.SchedaPagamento> schedaPagamentos);

    /**
    * Creates a new scheda pagamento with the primary key. Does not add the scheda pagamento to the database.
    *
    * @param schedaPagamentoPK the primary key for the new scheda pagamento
    * @return the new scheda pagamento
    */
    public it.bysoftware.ct.model.SchedaPagamento create(
        it.bysoftware.ct.service.persistence.SchedaPagamentoPK schedaPagamentoPK);

    /**
    * Removes the scheda pagamento with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param schedaPagamentoPK the primary key of the scheda pagamento
    * @return the scheda pagamento that was removed
    * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a scheda pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SchedaPagamento remove(
        it.bysoftware.ct.service.persistence.SchedaPagamentoPK schedaPagamentoPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSchedaPagamentoException;

    public it.bysoftware.ct.model.SchedaPagamento updateImpl(
        it.bysoftware.ct.model.SchedaPagamento schedaPagamento)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the scheda pagamento with the primary key or throws a {@link it.bysoftware.ct.NoSuchSchedaPagamentoException} if it could not be found.
    *
    * @param schedaPagamentoPK the primary key of the scheda pagamento
    * @return the scheda pagamento
    * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a scheda pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SchedaPagamento findByPrimaryKey(
        it.bysoftware.ct.service.persistence.SchedaPagamentoPK schedaPagamentoPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSchedaPagamentoException;

    /**
    * Returns the scheda pagamento with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param schedaPagamentoPK the primary key of the scheda pagamento
    * @return the scheda pagamento, or <code>null</code> if a scheda pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.SchedaPagamento fetchByPrimaryKey(
        it.bysoftware.ct.service.persistence.SchedaPagamentoPK schedaPagamentoPK)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the scheda pagamentos.
    *
    * @return the scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SchedaPagamento> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the scheda pagamentos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of scheda pagamentos
    * @param end the upper bound of the range of scheda pagamentos (not inclusive)
    * @return the range of scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SchedaPagamento> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the scheda pagamentos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of scheda pagamentos
    * @param end the upper bound of the range of scheda pagamentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.SchedaPagamento> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the scheda pagamentos from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of scheda pagamentos.
    *
    * @return the number of scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
