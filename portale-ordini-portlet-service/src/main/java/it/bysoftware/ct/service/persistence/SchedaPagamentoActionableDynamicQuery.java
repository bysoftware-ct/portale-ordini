package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.SchedaPagamento;
import it.bysoftware.ct.service.SchedaPagamentoLocalServiceUtil;

/**
 * @author Mario Torrisi
 * @generated
 */
public abstract class SchedaPagamentoActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public SchedaPagamentoActionableDynamicQuery() throws SystemException {
        setBaseLocalService(SchedaPagamentoLocalServiceUtil.getService());
        setClass(SchedaPagamento.class);

        setClassLoader(it.bysoftware.ct.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("primaryKey.esercizioRegistrazione");
    }
}
