package it.bysoftware.ct.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for ScadenzePartite. This utility wraps
 * {@link it.bysoftware.ct.service.impl.ScadenzePartiteLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Mario Torrisi
 * @see ScadenzePartiteLocalService
 * @see it.bysoftware.ct.service.base.ScadenzePartiteLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.impl.ScadenzePartiteLocalServiceImpl
 * @generated
 */
public class ScadenzePartiteLocalServiceUtil {
    private static ScadenzePartiteLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link it.bysoftware.ct.service.impl.ScadenzePartiteLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the scadenze partite to the database. Also notifies the appropriate model listeners.
    *
    * @param scadenzePartite the scadenze partite
    * @return the scadenze partite that was added
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ScadenzePartite addScadenzePartite(
        it.bysoftware.ct.model.ScadenzePartite scadenzePartite)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addScadenzePartite(scadenzePartite);
    }

    /**
    * Creates a new scadenze partite with the primary key. Does not add the scadenze partite to the database.
    *
    * @param scadenzePartitePK the primary key for the new scadenze partite
    * @return the new scadenze partite
    */
    public static it.bysoftware.ct.model.ScadenzePartite createScadenzePartite(
        it.bysoftware.ct.service.persistence.ScadenzePartitePK scadenzePartitePK) {
        return getService().createScadenzePartite(scadenzePartitePK);
    }

    /**
    * Deletes the scadenze partite with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param scadenzePartitePK the primary key of the scadenze partite
    * @return the scadenze partite that was removed
    * @throws PortalException if a scadenze partite with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ScadenzePartite deleteScadenzePartite(
        it.bysoftware.ct.service.persistence.ScadenzePartitePK scadenzePartitePK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteScadenzePartite(scadenzePartitePK);
    }

    /**
    * Deletes the scadenze partite from the database. Also notifies the appropriate model listeners.
    *
    * @param scadenzePartite the scadenze partite
    * @return the scadenze partite that was removed
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ScadenzePartite deleteScadenzePartite(
        it.bysoftware.ct.model.ScadenzePartite scadenzePartite)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteScadenzePartite(scadenzePartite);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ScadenzePartiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ScadenzePartiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static it.bysoftware.ct.model.ScadenzePartite fetchScadenzePartite(
        it.bysoftware.ct.service.persistence.ScadenzePartitePK scadenzePartitePK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchScadenzePartite(scadenzePartitePK);
    }

    /**
    * Returns the scadenze partite with the primary key.
    *
    * @param scadenzePartitePK the primary key of the scadenze partite
    * @return the scadenze partite
    * @throws PortalException if a scadenze partite with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ScadenzePartite getScadenzePartite(
        it.bysoftware.ct.service.persistence.ScadenzePartitePK scadenzePartitePK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getScadenzePartite(scadenzePartitePK);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the scadenze partites.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ScadenzePartiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of scadenze partites
    * @param end the upper bound of the range of scadenze partites (not inclusive)
    * @return the range of scadenze partites
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.ScadenzePartite> getScadenzePartites(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getScadenzePartites(start, end);
    }

    /**
    * Returns the number of scadenze partites.
    *
    * @return the number of scadenze partites
    * @throws SystemException if a system exception occurred
    */
    public static int getScadenzePartitesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getScadenzePartitesCount();
    }

    /**
    * Updates the scadenze partite in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param scadenzePartite the scadenze partite
    * @return the scadenze partite that was updated
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.ScadenzePartite updateScadenzePartite(
        it.bysoftware.ct.model.ScadenzePartite scadenzePartite)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateScadenzePartite(scadenzePartite);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.util.List<it.bysoftware.ct.model.ScadenzePartite> getCustomersClosedEntries(
        java.util.Date from, java.util.Date to, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .getCustomersClosedEntries(from, to, start, end,
            orderByComparator);
    }

    public static int getCustomersClosedEntriesCount(java.util.Date from,
        java.util.Date to)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getCustomersClosedEntriesCount(from, to);
    }

    public static java.util.List<it.bysoftware.ct.model.ScadenzePartite> getCustomerClosedEntries(
        java.lang.String customer, java.util.Date from, java.util.Date to,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .getCustomerClosedEntries(customer, from, to, start, end,
            orderByComparator);
    }

    public static int getCustomerClosedEntriesCount(java.lang.String customer,
        java.util.Date from, java.util.Date to)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getCustomerClosedEntriesCount(customer, from, to);
    }

    public static java.util.List<it.bysoftware.ct.model.ScadenzePartite> getPartnerOpenedEntries(
        java.lang.String partnerCode, java.util.Date from, java.util.Date to,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .getPartnerOpenedEntries(partnerCode, from, to, start, end,
            orderByComparator);
    }

    public static int getPartnerOpenedEntriesCount(
        java.lang.String partnerCode, java.util.Date from, java.util.Date to)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPartnerOpenedEntriesCount(partnerCode, from, to);
    }

    public static void clearService() {
        _service = null;
    }

    public static ScadenzePartiteLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    ScadenzePartiteLocalService.class.getName());

            if (invokableLocalService instanceof ScadenzePartiteLocalService) {
                _service = (ScadenzePartiteLocalService) invokableLocalService;
            } else {
                _service = new ScadenzePartiteLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(ScadenzePartiteLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(ScadenzePartiteLocalService service) {
    }
}
