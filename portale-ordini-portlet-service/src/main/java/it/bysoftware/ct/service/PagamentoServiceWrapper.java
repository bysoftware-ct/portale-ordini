package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PagamentoService}.
 *
 * @author Mario Torrisi
 * @see PagamentoService
 * @generated
 */
public class PagamentoServiceWrapper implements PagamentoService,
    ServiceWrapper<PagamentoService> {
    private PagamentoService _pagamentoService;

    public PagamentoServiceWrapper(PagamentoService pagamentoService) {
        _pagamentoService = pagamentoService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _pagamentoService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _pagamentoService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _pagamentoService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public PagamentoService getWrappedPagamentoService() {
        return _pagamentoService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedPagamentoService(PagamentoService pagamentoService) {
        _pagamentoService = pagamentoService;
    }

    @Override
    public PagamentoService getWrappedService() {
        return _pagamentoService;
    }

    @Override
    public void setWrappedService(PagamentoService pagamentoService) {
        _pagamentoService = pagamentoService;
    }
}
