package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.SchedaPagamento;

import java.util.List;

/**
 * The persistence utility for the scheda pagamento service. This utility wraps {@link SchedaPagamentoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see SchedaPagamentoPersistence
 * @see SchedaPagamentoPersistenceImpl
 * @generated
 */
public class SchedaPagamentoUtil {
    private static SchedaPagamentoPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(SchedaPagamento schedaPagamento) {
        getPersistence().clearCache(schedaPagamento);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<SchedaPagamento> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<SchedaPagamento> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<SchedaPagamento> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static SchedaPagamento update(SchedaPagamento schedaPagamento)
        throws SystemException {
        return getPersistence().update(schedaPagamento);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static SchedaPagamento update(SchedaPagamento schedaPagamento,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(schedaPagamento, serviceContext);
    }

    /**
    * Returns all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @return the matching scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SchedaPagamento> findByCodiceFornitoreAnnoIdPagamento(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCodiceFornitoreAnnoIdPagamento(codiceSoggetto,
            annoPagamento, idPagamento);
    }

    /**
    * Returns a range of all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param start the lower bound of the range of scheda pagamentos
    * @param end the upper bound of the range of scheda pagamentos (not inclusive)
    * @return the range of matching scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SchedaPagamento> findByCodiceFornitoreAnnoIdPagamento(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCodiceFornitoreAnnoIdPagamento(codiceSoggetto,
            annoPagamento, idPagamento, start, end);
    }

    /**
    * Returns an ordered range of all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param start the lower bound of the range of scheda pagamentos
    * @param end the upper bound of the range of scheda pagamentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SchedaPagamento> findByCodiceFornitoreAnnoIdPagamento(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCodiceFornitoreAnnoIdPagamento(codiceSoggetto,
            annoPagamento, idPagamento, start, end, orderByComparator);
    }

    /**
    * Returns the first scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching scheda pagamento
    * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a matching scheda pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SchedaPagamento findByCodiceFornitoreAnnoIdPagamento_First(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSchedaPagamentoException {
        return getPersistence()
                   .findByCodiceFornitoreAnnoIdPagamento_First(codiceSoggetto,
            annoPagamento, idPagamento, orderByComparator);
    }

    /**
    * Returns the first scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching scheda pagamento, or <code>null</code> if a matching scheda pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SchedaPagamento fetchByCodiceFornitoreAnnoIdPagamento_First(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCodiceFornitoreAnnoIdPagamento_First(codiceSoggetto,
            annoPagamento, idPagamento, orderByComparator);
    }

    /**
    * Returns the last scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching scheda pagamento
    * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a matching scheda pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SchedaPagamento findByCodiceFornitoreAnnoIdPagamento_Last(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSchedaPagamentoException {
        return getPersistence()
                   .findByCodiceFornitoreAnnoIdPagamento_Last(codiceSoggetto,
            annoPagamento, idPagamento, orderByComparator);
    }

    /**
    * Returns the last scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching scheda pagamento, or <code>null</code> if a matching scheda pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SchedaPagamento fetchByCodiceFornitoreAnnoIdPagamento_Last(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCodiceFornitoreAnnoIdPagamento_Last(codiceSoggetto,
            annoPagamento, idPagamento, orderByComparator);
    }

    /**
    * Returns the scheda pagamentos before and after the current scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param schedaPagamentoPK the primary key of the current scheda pagamento
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next scheda pagamento
    * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a scheda pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SchedaPagamento[] findByCodiceFornitoreAnnoIdPagamento_PrevAndNext(
        it.bysoftware.ct.service.persistence.SchedaPagamentoPK schedaPagamentoPK,
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSchedaPagamentoException {
        return getPersistence()
                   .findByCodiceFornitoreAnnoIdPagamento_PrevAndNext(schedaPagamentoPK,
            codiceSoggetto, annoPagamento, idPagamento, orderByComparator);
    }

    /**
    * Removes all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; from the database.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @throws SystemException if a system exception occurred
    */
    public static void removeByCodiceFornitoreAnnoIdPagamento(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByCodiceFornitoreAnnoIdPagamento(codiceSoggetto,
            annoPagamento, idPagamento);
    }

    /**
    * Returns the number of scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @return the number of matching scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static int countByCodiceFornitoreAnnoIdPagamento(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByCodiceFornitoreAnnoIdPagamento(codiceSoggetto,
            annoPagamento, idPagamento);
    }

    /**
    * Returns all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @return the matching scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SchedaPagamento> findByCodiceFornitoreAnnoIdPagamentoStato(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCodiceFornitoreAnnoIdPagamentoStato(codiceSoggetto,
            annoPagamento, idPagamento, stato);
    }

    /**
    * Returns a range of all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @param start the lower bound of the range of scheda pagamentos
    * @param end the upper bound of the range of scheda pagamentos (not inclusive)
    * @return the range of matching scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SchedaPagamento> findByCodiceFornitoreAnnoIdPagamentoStato(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCodiceFornitoreAnnoIdPagamentoStato(codiceSoggetto,
            annoPagamento, idPagamento, stato, start, end);
    }

    /**
    * Returns an ordered range of all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @param start the lower bound of the range of scheda pagamentos
    * @param end the upper bound of the range of scheda pagamentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SchedaPagamento> findByCodiceFornitoreAnnoIdPagamentoStato(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByCodiceFornitoreAnnoIdPagamentoStato(codiceSoggetto,
            annoPagamento, idPagamento, stato, start, end, orderByComparator);
    }

    /**
    * Returns the first scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching scheda pagamento
    * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a matching scheda pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SchedaPagamento findByCodiceFornitoreAnnoIdPagamentoStato_First(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSchedaPagamentoException {
        return getPersistence()
                   .findByCodiceFornitoreAnnoIdPagamentoStato_First(codiceSoggetto,
            annoPagamento, idPagamento, stato, orderByComparator);
    }

    /**
    * Returns the first scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching scheda pagamento, or <code>null</code> if a matching scheda pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SchedaPagamento fetchByCodiceFornitoreAnnoIdPagamentoStato_First(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCodiceFornitoreAnnoIdPagamentoStato_First(codiceSoggetto,
            annoPagamento, idPagamento, stato, orderByComparator);
    }

    /**
    * Returns the last scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching scheda pagamento
    * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a matching scheda pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SchedaPagamento findByCodiceFornitoreAnnoIdPagamentoStato_Last(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSchedaPagamentoException {
        return getPersistence()
                   .findByCodiceFornitoreAnnoIdPagamentoStato_Last(codiceSoggetto,
            annoPagamento, idPagamento, stato, orderByComparator);
    }

    /**
    * Returns the last scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching scheda pagamento, or <code>null</code> if a matching scheda pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SchedaPagamento fetchByCodiceFornitoreAnnoIdPagamentoStato_Last(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByCodiceFornitoreAnnoIdPagamentoStato_Last(codiceSoggetto,
            annoPagamento, idPagamento, stato, orderByComparator);
    }

    /**
    * Returns the scheda pagamentos before and after the current scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
    *
    * @param schedaPagamentoPK the primary key of the current scheda pagamento
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next scheda pagamento
    * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a scheda pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SchedaPagamento[] findByCodiceFornitoreAnnoIdPagamentoStato_PrevAndNext(
        it.bysoftware.ct.service.persistence.SchedaPagamentoPK schedaPagamentoPK,
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSchedaPagamentoException {
        return getPersistence()
                   .findByCodiceFornitoreAnnoIdPagamentoStato_PrevAndNext(schedaPagamentoPK,
            codiceSoggetto, annoPagamento, idPagamento, stato, orderByComparator);
    }

    /**
    * Removes all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63; from the database.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @throws SystemException if a system exception occurred
    */
    public static void removeByCodiceFornitoreAnnoIdPagamentoStato(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato) throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByCodiceFornitoreAnnoIdPagamentoStato(codiceSoggetto,
            annoPagamento, idPagamento, stato);
    }

    /**
    * Returns the number of scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
    *
    * @param codiceSoggetto the codice soggetto
    * @param annoPagamento the anno pagamento
    * @param idPagamento the id pagamento
    * @param stato the stato
    * @return the number of matching scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static int countByCodiceFornitoreAnnoIdPagamentoStato(
        java.lang.String codiceSoggetto, int annoPagamento, int idPagamento,
        int stato) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByCodiceFornitoreAnnoIdPagamentoStato(codiceSoggetto,
            annoPagamento, idPagamento, stato);
    }

    /**
    * Caches the scheda pagamento in the entity cache if it is enabled.
    *
    * @param schedaPagamento the scheda pagamento
    */
    public static void cacheResult(
        it.bysoftware.ct.model.SchedaPagamento schedaPagamento) {
        getPersistence().cacheResult(schedaPagamento);
    }

    /**
    * Caches the scheda pagamentos in the entity cache if it is enabled.
    *
    * @param schedaPagamentos the scheda pagamentos
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.SchedaPagamento> schedaPagamentos) {
        getPersistence().cacheResult(schedaPagamentos);
    }

    /**
    * Creates a new scheda pagamento with the primary key. Does not add the scheda pagamento to the database.
    *
    * @param schedaPagamentoPK the primary key for the new scheda pagamento
    * @return the new scheda pagamento
    */
    public static it.bysoftware.ct.model.SchedaPagamento create(
        it.bysoftware.ct.service.persistence.SchedaPagamentoPK schedaPagamentoPK) {
        return getPersistence().create(schedaPagamentoPK);
    }

    /**
    * Removes the scheda pagamento with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param schedaPagamentoPK the primary key of the scheda pagamento
    * @return the scheda pagamento that was removed
    * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a scheda pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SchedaPagamento remove(
        it.bysoftware.ct.service.persistence.SchedaPagamentoPK schedaPagamentoPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSchedaPagamentoException {
        return getPersistence().remove(schedaPagamentoPK);
    }

    public static it.bysoftware.ct.model.SchedaPagamento updateImpl(
        it.bysoftware.ct.model.SchedaPagamento schedaPagamento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(schedaPagamento);
    }

    /**
    * Returns the scheda pagamento with the primary key or throws a {@link it.bysoftware.ct.NoSuchSchedaPagamentoException} if it could not be found.
    *
    * @param schedaPagamentoPK the primary key of the scheda pagamento
    * @return the scheda pagamento
    * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a scheda pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SchedaPagamento findByPrimaryKey(
        it.bysoftware.ct.service.persistence.SchedaPagamentoPK schedaPagamentoPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchSchedaPagamentoException {
        return getPersistence().findByPrimaryKey(schedaPagamentoPK);
    }

    /**
    * Returns the scheda pagamento with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param schedaPagamentoPK the primary key of the scheda pagamento
    * @return the scheda pagamento, or <code>null</code> if a scheda pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.SchedaPagamento fetchByPrimaryKey(
        it.bysoftware.ct.service.persistence.SchedaPagamentoPK schedaPagamentoPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(schedaPagamentoPK);
    }

    /**
    * Returns all the scheda pagamentos.
    *
    * @return the scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SchedaPagamento> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the scheda pagamentos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of scheda pagamentos
    * @param end the upper bound of the range of scheda pagamentos (not inclusive)
    * @return the range of scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SchedaPagamento> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the scheda pagamentos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of scheda pagamentos
    * @param end the upper bound of the range of scheda pagamentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.SchedaPagamento> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the scheda pagamentos from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of scheda pagamentos.
    *
    * @return the number of scheda pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static SchedaPagamentoPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (SchedaPagamentoPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    SchedaPagamentoPersistence.class.getName());

            ReferenceRegistry.registerReference(SchedaPagamentoUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(SchedaPagamentoPersistence persistence) {
    }
}
