package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.RigheFattureVedita;
import it.bysoftware.ct.service.RigheFattureVeditaLocalServiceUtil;

/**
 * @author Mario Torrisi
 * @generated
 */
public abstract class RigheFattureVeditaActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public RigheFattureVeditaActionableDynamicQuery() throws SystemException {
        setBaseLocalService(RigheFattureVeditaLocalServiceUtil.getService());
        setClass(RigheFattureVedita.class);

        setClassLoader(it.bysoftware.ct.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("primaryKey.anno");
    }
}
