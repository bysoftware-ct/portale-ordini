package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.CausaliContabili;

import java.util.List;

/**
 * The persistence utility for the causali contabili service. This utility wraps {@link CausaliContabiliPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CausaliContabiliPersistence
 * @see CausaliContabiliPersistenceImpl
 * @generated
 */
public class CausaliContabiliUtil {
    private static CausaliContabiliPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(CausaliContabili causaliContabili) {
        getPersistence().clearCache(causaliContabili);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<CausaliContabili> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<CausaliContabili> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<CausaliContabili> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static CausaliContabili update(CausaliContabili causaliContabili)
        throws SystemException {
        return getPersistence().update(causaliContabili);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static CausaliContabili update(CausaliContabili causaliContabili,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(causaliContabili, serviceContext);
    }

    /**
    * Caches the causali contabili in the entity cache if it is enabled.
    *
    * @param causaliContabili the causali contabili
    */
    public static void cacheResult(
        it.bysoftware.ct.model.CausaliContabili causaliContabili) {
        getPersistence().cacheResult(causaliContabili);
    }

    /**
    * Caches the causali contabilis in the entity cache if it is enabled.
    *
    * @param causaliContabilis the causali contabilis
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.CausaliContabili> causaliContabilis) {
        getPersistence().cacheResult(causaliContabilis);
    }

    /**
    * Creates a new causali contabili with the primary key. Does not add the causali contabili to the database.
    *
    * @param codiceCausaleCont the primary key for the new causali contabili
    * @return the new causali contabili
    */
    public static it.bysoftware.ct.model.CausaliContabili create(
        java.lang.String codiceCausaleCont) {
        return getPersistence().create(codiceCausaleCont);
    }

    /**
    * Removes the causali contabili with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param codiceCausaleCont the primary key of the causali contabili
    * @return the causali contabili that was removed
    * @throws it.bysoftware.ct.NoSuchCausaliContabiliException if a causali contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.CausaliContabili remove(
        java.lang.String codiceCausaleCont)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCausaliContabiliException {
        return getPersistence().remove(codiceCausaleCont);
    }

    public static it.bysoftware.ct.model.CausaliContabili updateImpl(
        it.bysoftware.ct.model.CausaliContabili causaliContabili)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(causaliContabili);
    }

    /**
    * Returns the causali contabili with the primary key or throws a {@link it.bysoftware.ct.NoSuchCausaliContabiliException} if it could not be found.
    *
    * @param codiceCausaleCont the primary key of the causali contabili
    * @return the causali contabili
    * @throws it.bysoftware.ct.NoSuchCausaliContabiliException if a causali contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.CausaliContabili findByPrimaryKey(
        java.lang.String codiceCausaleCont)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchCausaliContabiliException {
        return getPersistence().findByPrimaryKey(codiceCausaleCont);
    }

    /**
    * Returns the causali contabili with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param codiceCausaleCont the primary key of the causali contabili
    * @return the causali contabili, or <code>null</code> if a causali contabili with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.CausaliContabili fetchByPrimaryKey(
        java.lang.String codiceCausaleCont)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(codiceCausaleCont);
    }

    /**
    * Returns all the causali contabilis.
    *
    * @return the causali contabilis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.CausaliContabili> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the causali contabilis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CausaliContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of causali contabilis
    * @param end the upper bound of the range of causali contabilis (not inclusive)
    * @return the range of causali contabilis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.CausaliContabili> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the causali contabilis.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CausaliContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of causali contabilis
    * @param end the upper bound of the range of causali contabilis (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of causali contabilis
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.CausaliContabili> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the causali contabilis from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of causali contabilis.
    *
    * @return the number of causali contabilis
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static CausaliContabiliPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (CausaliContabiliPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    CausaliContabiliPersistence.class.getName());

            ReferenceRegistry.registerReference(CausaliContabiliUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(CausaliContabiliPersistence persistence) {
    }
}
