package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.Variante;

/**
 * The persistence interface for the variante service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see VariantePersistenceImpl
 * @see VarianteUtil
 * @generated
 */
public interface VariantePersistence extends BasePersistence<Variante> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link VarianteUtil} to access the variante persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the variante in the entity cache if it is enabled.
    *
    * @param variante the variante
    */
    public void cacheResult(it.bysoftware.ct.model.Variante variante);

    /**
    * Caches the variantes in the entity cache if it is enabled.
    *
    * @param variantes the variantes
    */
    public void cacheResult(
        java.util.List<it.bysoftware.ct.model.Variante> variantes);

    /**
    * Creates a new variante with the primary key. Does not add the variante to the database.
    *
    * @param id the primary key for the new variante
    * @return the new variante
    */
    public it.bysoftware.ct.model.Variante create(long id);

    /**
    * Removes the variante with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the variante
    * @return the variante that was removed
    * @throws it.bysoftware.ct.NoSuchVarianteException if a variante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Variante remove(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVarianteException;

    public it.bysoftware.ct.model.Variante updateImpl(
        it.bysoftware.ct.model.Variante variante)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the variante with the primary key or throws a {@link it.bysoftware.ct.NoSuchVarianteException} if it could not be found.
    *
    * @param id the primary key of the variante
    * @return the variante
    * @throws it.bysoftware.ct.NoSuchVarianteException if a variante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Variante findByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchVarianteException;

    /**
    * Returns the variante with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the variante
    * @return the variante, or <code>null</code> if a variante with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public it.bysoftware.ct.model.Variante fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the variantes.
    *
    * @return the variantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Variante> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the variantes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VarianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of variantes
    * @param end the upper bound of the range of variantes (not inclusive)
    * @return the range of variantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Variante> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the variantes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VarianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of variantes
    * @param end the upper bound of the range of variantes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of variantes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<it.bysoftware.ct.model.Variante> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the variantes from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of variantes.
    *
    * @return the number of variantes
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
