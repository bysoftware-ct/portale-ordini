package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.Pagamento;

import java.util.List;

/**
 * The persistence utility for the pagamento service. This utility wraps {@link PagamentoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see PagamentoPersistence
 * @see PagamentoPersistenceImpl
 * @generated
 */
public class PagamentoUtil {
    private static PagamentoPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Pagamento pagamento) {
        getPersistence().clearCache(pagamento);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Pagamento> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Pagamento> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Pagamento> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Pagamento update(Pagamento pagamento)
        throws SystemException {
        return getPersistence().update(pagamento);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Pagamento update(Pagamento pagamento,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(pagamento, serviceContext);
    }

    /**
    * Returns all the pagamentos where anno = &#63;.
    *
    * @param anno the anno
    * @return the matching pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Pagamento> findByAnno(
        int anno) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByAnno(anno);
    }

    /**
    * Returns a range of all the pagamentos where anno = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param anno the anno
    * @param start the lower bound of the range of pagamentos
    * @param end the upper bound of the range of pagamentos (not inclusive)
    * @return the range of matching pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Pagamento> findByAnno(
        int anno, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByAnno(anno, start, end);
    }

    /**
    * Returns an ordered range of all the pagamentos where anno = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param anno the anno
    * @param start the lower bound of the range of pagamentos
    * @param end the upper bound of the range of pagamentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Pagamento> findByAnno(
        int anno, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByAnno(anno, start, end, orderByComparator);
    }

    /**
    * Returns the first pagamento in the ordered set where anno = &#63;.
    *
    * @param anno the anno
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching pagamento
    * @throws it.bysoftware.ct.NoSuchPagamentoException if a matching pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Pagamento findByAnno_First(int anno,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPagamentoException {
        return getPersistence().findByAnno_First(anno, orderByComparator);
    }

    /**
    * Returns the first pagamento in the ordered set where anno = &#63;.
    *
    * @param anno the anno
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching pagamento, or <code>null</code> if a matching pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Pagamento fetchByAnno_First(int anno,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByAnno_First(anno, orderByComparator);
    }

    /**
    * Returns the last pagamento in the ordered set where anno = &#63;.
    *
    * @param anno the anno
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching pagamento
    * @throws it.bysoftware.ct.NoSuchPagamentoException if a matching pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Pagamento findByAnno_Last(int anno,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPagamentoException {
        return getPersistence().findByAnno_Last(anno, orderByComparator);
    }

    /**
    * Returns the last pagamento in the ordered set where anno = &#63;.
    *
    * @param anno the anno
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching pagamento, or <code>null</code> if a matching pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Pagamento fetchByAnno_Last(int anno,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByAnno_Last(anno, orderByComparator);
    }

    /**
    * Returns the pagamentos before and after the current pagamento in the ordered set where anno = &#63;.
    *
    * @param pagamentoPK the primary key of the current pagamento
    * @param anno the anno
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next pagamento
    * @throws it.bysoftware.ct.NoSuchPagamentoException if a pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Pagamento[] findByAnno_PrevAndNext(
        it.bysoftware.ct.service.persistence.PagamentoPK pagamentoPK, int anno,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPagamentoException {
        return getPersistence()
                   .findByAnno_PrevAndNext(pagamentoPK, anno, orderByComparator);
    }

    /**
    * Removes all the pagamentos where anno = &#63; from the database.
    *
    * @param anno the anno
    * @throws SystemException if a system exception occurred
    */
    public static void removeByAnno(int anno)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByAnno(anno);
    }

    /**
    * Returns the number of pagamentos where anno = &#63;.
    *
    * @param anno the anno
    * @return the number of matching pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static int countByAnno(int anno)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByAnno(anno);
    }

    /**
    * Returns all the pagamentos where anno = &#63; and stato = &#63;.
    *
    * @param anno the anno
    * @param stato the stato
    * @return the matching pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Pagamento> findByAnnoStato(
        int anno, int stato)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByAnnoStato(anno, stato);
    }

    /**
    * Returns a range of all the pagamentos where anno = &#63; and stato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param anno the anno
    * @param stato the stato
    * @param start the lower bound of the range of pagamentos
    * @param end the upper bound of the range of pagamentos (not inclusive)
    * @return the range of matching pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Pagamento> findByAnnoStato(
        int anno, int stato, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByAnnoStato(anno, stato, start, end);
    }

    /**
    * Returns an ordered range of all the pagamentos where anno = &#63; and stato = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param anno the anno
    * @param stato the stato
    * @param start the lower bound of the range of pagamentos
    * @param end the upper bound of the range of pagamentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Pagamento> findByAnnoStato(
        int anno, int stato, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByAnnoStato(anno, stato, start, end, orderByComparator);
    }

    /**
    * Returns the first pagamento in the ordered set where anno = &#63; and stato = &#63;.
    *
    * @param anno the anno
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching pagamento
    * @throws it.bysoftware.ct.NoSuchPagamentoException if a matching pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Pagamento findByAnnoStato_First(
        int anno, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPagamentoException {
        return getPersistence()
                   .findByAnnoStato_First(anno, stato, orderByComparator);
    }

    /**
    * Returns the first pagamento in the ordered set where anno = &#63; and stato = &#63;.
    *
    * @param anno the anno
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching pagamento, or <code>null</code> if a matching pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Pagamento fetchByAnnoStato_First(
        int anno, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByAnnoStato_First(anno, stato, orderByComparator);
    }

    /**
    * Returns the last pagamento in the ordered set where anno = &#63; and stato = &#63;.
    *
    * @param anno the anno
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching pagamento
    * @throws it.bysoftware.ct.NoSuchPagamentoException if a matching pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Pagamento findByAnnoStato_Last(
        int anno, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPagamentoException {
        return getPersistence()
                   .findByAnnoStato_Last(anno, stato, orderByComparator);
    }

    /**
    * Returns the last pagamento in the ordered set where anno = &#63; and stato = &#63;.
    *
    * @param anno the anno
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching pagamento, or <code>null</code> if a matching pagamento could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Pagamento fetchByAnnoStato_Last(
        int anno, int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByAnnoStato_Last(anno, stato, orderByComparator);
    }

    /**
    * Returns the pagamentos before and after the current pagamento in the ordered set where anno = &#63; and stato = &#63;.
    *
    * @param pagamentoPK the primary key of the current pagamento
    * @param anno the anno
    * @param stato the stato
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next pagamento
    * @throws it.bysoftware.ct.NoSuchPagamentoException if a pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Pagamento[] findByAnnoStato_PrevAndNext(
        it.bysoftware.ct.service.persistence.PagamentoPK pagamentoPK, int anno,
        int stato,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPagamentoException {
        return getPersistence()
                   .findByAnnoStato_PrevAndNext(pagamentoPK, anno, stato,
            orderByComparator);
    }

    /**
    * Removes all the pagamentos where anno = &#63; and stato = &#63; from the database.
    *
    * @param anno the anno
    * @param stato the stato
    * @throws SystemException if a system exception occurred
    */
    public static void removeByAnnoStato(int anno, int stato)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByAnnoStato(anno, stato);
    }

    /**
    * Returns the number of pagamentos where anno = &#63; and stato = &#63;.
    *
    * @param anno the anno
    * @param stato the stato
    * @return the number of matching pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static int countByAnnoStato(int anno, int stato)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByAnnoStato(anno, stato);
    }

    /**
    * Caches the pagamento in the entity cache if it is enabled.
    *
    * @param pagamento the pagamento
    */
    public static void cacheResult(it.bysoftware.ct.model.Pagamento pagamento) {
        getPersistence().cacheResult(pagamento);
    }

    /**
    * Caches the pagamentos in the entity cache if it is enabled.
    *
    * @param pagamentos the pagamentos
    */
    public static void cacheResult(
        java.util.List<it.bysoftware.ct.model.Pagamento> pagamentos) {
        getPersistence().cacheResult(pagamentos);
    }

    /**
    * Creates a new pagamento with the primary key. Does not add the pagamento to the database.
    *
    * @param pagamentoPK the primary key for the new pagamento
    * @return the new pagamento
    */
    public static it.bysoftware.ct.model.Pagamento create(
        it.bysoftware.ct.service.persistence.PagamentoPK pagamentoPK) {
        return getPersistence().create(pagamentoPK);
    }

    /**
    * Removes the pagamento with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param pagamentoPK the primary key of the pagamento
    * @return the pagamento that was removed
    * @throws it.bysoftware.ct.NoSuchPagamentoException if a pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Pagamento remove(
        it.bysoftware.ct.service.persistence.PagamentoPK pagamentoPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPagamentoException {
        return getPersistence().remove(pagamentoPK);
    }

    public static it.bysoftware.ct.model.Pagamento updateImpl(
        it.bysoftware.ct.model.Pagamento pagamento)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(pagamento);
    }

    /**
    * Returns the pagamento with the primary key or throws a {@link it.bysoftware.ct.NoSuchPagamentoException} if it could not be found.
    *
    * @param pagamentoPK the primary key of the pagamento
    * @return the pagamento
    * @throws it.bysoftware.ct.NoSuchPagamentoException if a pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Pagamento findByPrimaryKey(
        it.bysoftware.ct.service.persistence.PagamentoPK pagamentoPK)
        throws com.liferay.portal.kernel.exception.SystemException,
            it.bysoftware.ct.NoSuchPagamentoException {
        return getPersistence().findByPrimaryKey(pagamentoPK);
    }

    /**
    * Returns the pagamento with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param pagamentoPK the primary key of the pagamento
    * @return the pagamento, or <code>null</code> if a pagamento with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static it.bysoftware.ct.model.Pagamento fetchByPrimaryKey(
        it.bysoftware.ct.service.persistence.PagamentoPK pagamentoPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(pagamentoPK);
    }

    /**
    * Returns all the pagamentos.
    *
    * @return the pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Pagamento> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the pagamentos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of pagamentos
    * @param end the upper bound of the range of pagamentos (not inclusive)
    * @return the range of pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Pagamento> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the pagamentos.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of pagamentos
    * @param end the upper bound of the range of pagamentos (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<it.bysoftware.ct.model.Pagamento> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the pagamentos from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of pagamentos.
    *
    * @return the number of pagamentos
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static PagamentoPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (PagamentoPersistence) PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
                    PagamentoPersistence.class.getName());

            ReferenceRegistry.registerReference(PagamentoUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(PagamentoPersistence persistence) {
    }
}
