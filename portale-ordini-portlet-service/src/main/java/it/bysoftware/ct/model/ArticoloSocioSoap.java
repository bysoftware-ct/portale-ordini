package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.ArticoloSocioPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.ArticoloSocioServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.ArticoloSocioServiceSoap
 * @generated
 */
public class ArticoloSocioSoap implements Serializable {
    private String _associato;
    private String _codiceArticoloSocio;
    private String _codiceVarianteSocio;
    private String _codiceArticolo;
    private String _codiceVariante;

    public ArticoloSocioSoap() {
    }

    public static ArticoloSocioSoap toSoapModel(ArticoloSocio model) {
        ArticoloSocioSoap soapModel = new ArticoloSocioSoap();

        soapModel.setAssociato(model.getAssociato());
        soapModel.setCodiceArticoloSocio(model.getCodiceArticoloSocio());
        soapModel.setCodiceVarianteSocio(model.getCodiceVarianteSocio());
        soapModel.setCodiceArticolo(model.getCodiceArticolo());
        soapModel.setCodiceVariante(model.getCodiceVariante());

        return soapModel;
    }

    public static ArticoloSocioSoap[] toSoapModels(ArticoloSocio[] models) {
        ArticoloSocioSoap[] soapModels = new ArticoloSocioSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static ArticoloSocioSoap[][] toSoapModels(ArticoloSocio[][] models) {
        ArticoloSocioSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new ArticoloSocioSoap[models.length][models[0].length];
        } else {
            soapModels = new ArticoloSocioSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static ArticoloSocioSoap[] toSoapModels(List<ArticoloSocio> models) {
        List<ArticoloSocioSoap> soapModels = new ArrayList<ArticoloSocioSoap>(models.size());

        for (ArticoloSocio model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new ArticoloSocioSoap[soapModels.size()]);
    }

    public ArticoloSocioPK getPrimaryKey() {
        return new ArticoloSocioPK(_associato, _codiceArticoloSocio,
            _codiceVarianteSocio);
    }

    public void setPrimaryKey(ArticoloSocioPK pk) {
        setAssociato(pk.associato);
        setCodiceArticoloSocio(pk.codiceArticoloSocio);
        setCodiceVarianteSocio(pk.codiceVarianteSocio);
    }

    public String getAssociato() {
        return _associato;
    }

    public void setAssociato(String associato) {
        _associato = associato;
    }

    public String getCodiceArticoloSocio() {
        return _codiceArticoloSocio;
    }

    public void setCodiceArticoloSocio(String codiceArticoloSocio) {
        _codiceArticoloSocio = codiceArticoloSocio;
    }

    public String getCodiceVarianteSocio() {
        return _codiceVarianteSocio;
    }

    public void setCodiceVarianteSocio(String codiceVarianteSocio) {
        _codiceVarianteSocio = codiceVarianteSocio;
    }

    public String getCodiceArticolo() {
        return _codiceArticolo;
    }

    public void setCodiceArticolo(String codiceArticolo) {
        _codiceArticolo = codiceArticolo;
    }

    public String getCodiceVariante() {
        return _codiceVariante;
    }

    public void setCodiceVariante(String codiceVariante) {
        _codiceVariante = codiceVariante;
    }
}
