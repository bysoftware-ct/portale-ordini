package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.TestataDocumentoPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.TestataDocumentoServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.TestataDocumentoServiceSoap
 * @generated
 */
public class TestataDocumentoSoap implements Serializable {
    private int _anno;
    private String _codiceAttivita;
    private String _codiceCentro;
    private String _codiceDeposito;
    private int _protocollo;
    private String _codiceFornitore;
    private String _tipoDocumento;
    private Date _dataRegistrazione;
    private String _codiceSpedizione;
    private String _codiceCausaleTrasporto;
    private String _codiceCuraTrasporto;
    private String _codicePorto;
    private String _codiceAspettoEsteriore;
    private String _codiceVettore1;
    private String _codiceVettore2;
    private String _libStr1;
    private String _libStr2;
    private String _libStr3;
    private double _libDbl1;
    private double _libDbl2;
    private double _libDbl3;
    private long _libLng1;
    private long _libLng2;
    private long _libLng3;
    private Date _libDat1;
    private Date _libDat2;
    private Date _libDat3;
    private boolean _stato;
    private boolean _inviato;
    private String _codiceConsorzio;

    public TestataDocumentoSoap() {
    }

    public static TestataDocumentoSoap toSoapModel(TestataDocumento model) {
        TestataDocumentoSoap soapModel = new TestataDocumentoSoap();

        soapModel.setAnno(model.getAnno());
        soapModel.setCodiceAttivita(model.getCodiceAttivita());
        soapModel.setCodiceCentro(model.getCodiceCentro());
        soapModel.setCodiceDeposito(model.getCodiceDeposito());
        soapModel.setProtocollo(model.getProtocollo());
        soapModel.setCodiceFornitore(model.getCodiceFornitore());
        soapModel.setTipoDocumento(model.getTipoDocumento());
        soapModel.setDataRegistrazione(model.getDataRegistrazione());
        soapModel.setCodiceSpedizione(model.getCodiceSpedizione());
        soapModel.setCodiceCausaleTrasporto(model.getCodiceCausaleTrasporto());
        soapModel.setCodiceCuraTrasporto(model.getCodiceCuraTrasporto());
        soapModel.setCodicePorto(model.getCodicePorto());
        soapModel.setCodiceAspettoEsteriore(model.getCodiceAspettoEsteriore());
        soapModel.setCodiceVettore1(model.getCodiceVettore1());
        soapModel.setCodiceVettore2(model.getCodiceVettore2());
        soapModel.setLibStr1(model.getLibStr1());
        soapModel.setLibStr2(model.getLibStr2());
        soapModel.setLibStr3(model.getLibStr3());
        soapModel.setLibDbl1(model.getLibDbl1());
        soapModel.setLibDbl2(model.getLibDbl2());
        soapModel.setLibDbl3(model.getLibDbl3());
        soapModel.setLibLng1(model.getLibLng1());
        soapModel.setLibLng2(model.getLibLng2());
        soapModel.setLibLng3(model.getLibLng3());
        soapModel.setLibDat1(model.getLibDat1());
        soapModel.setLibDat2(model.getLibDat2());
        soapModel.setLibDat3(model.getLibDat3());
        soapModel.setStato(model.getStato());
        soapModel.setInviato(model.getInviato());
        soapModel.setCodiceConsorzio(model.getCodiceConsorzio());

        return soapModel;
    }

    public static TestataDocumentoSoap[] toSoapModels(TestataDocumento[] models) {
        TestataDocumentoSoap[] soapModels = new TestataDocumentoSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static TestataDocumentoSoap[][] toSoapModels(
        TestataDocumento[][] models) {
        TestataDocumentoSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new TestataDocumentoSoap[models.length][models[0].length];
        } else {
            soapModels = new TestataDocumentoSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static TestataDocumentoSoap[] toSoapModels(
        List<TestataDocumento> models) {
        List<TestataDocumentoSoap> soapModels = new ArrayList<TestataDocumentoSoap>(models.size());

        for (TestataDocumento model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new TestataDocumentoSoap[soapModels.size()]);
    }

    public TestataDocumentoPK getPrimaryKey() {
        return new TestataDocumentoPK(_anno, _codiceAttivita, _codiceCentro,
            _codiceDeposito, _protocollo, _codiceFornitore, _tipoDocumento);
    }

    public void setPrimaryKey(TestataDocumentoPK pk) {
        setAnno(pk.anno);
        setCodiceAttivita(pk.codiceAttivita);
        setCodiceCentro(pk.codiceCentro);
        setCodiceDeposito(pk.codiceDeposito);
        setProtocollo(pk.protocollo);
        setCodiceFornitore(pk.codiceFornitore);
        setTipoDocumento(pk.tipoDocumento);
    }

    public int getAnno() {
        return _anno;
    }

    public void setAnno(int anno) {
        _anno = anno;
    }

    public String getCodiceAttivita() {
        return _codiceAttivita;
    }

    public void setCodiceAttivita(String codiceAttivita) {
        _codiceAttivita = codiceAttivita;
    }

    public String getCodiceCentro() {
        return _codiceCentro;
    }

    public void setCodiceCentro(String codiceCentro) {
        _codiceCentro = codiceCentro;
    }

    public String getCodiceDeposito() {
        return _codiceDeposito;
    }

    public void setCodiceDeposito(String codiceDeposito) {
        _codiceDeposito = codiceDeposito;
    }

    public int getProtocollo() {
        return _protocollo;
    }

    public void setProtocollo(int protocollo) {
        _protocollo = protocollo;
    }

    public String getCodiceFornitore() {
        return _codiceFornitore;
    }

    public void setCodiceFornitore(String codiceFornitore) {
        _codiceFornitore = codiceFornitore;
    }

    public String getTipoDocumento() {
        return _tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        _tipoDocumento = tipoDocumento;
    }

    public Date getDataRegistrazione() {
        return _dataRegistrazione;
    }

    public void setDataRegistrazione(Date dataRegistrazione) {
        _dataRegistrazione = dataRegistrazione;
    }

    public String getCodiceSpedizione() {
        return _codiceSpedizione;
    }

    public void setCodiceSpedizione(String codiceSpedizione) {
        _codiceSpedizione = codiceSpedizione;
    }

    public String getCodiceCausaleTrasporto() {
        return _codiceCausaleTrasporto;
    }

    public void setCodiceCausaleTrasporto(String codiceCausaleTrasporto) {
        _codiceCausaleTrasporto = codiceCausaleTrasporto;
    }

    public String getCodiceCuraTrasporto() {
        return _codiceCuraTrasporto;
    }

    public void setCodiceCuraTrasporto(String codiceCuraTrasporto) {
        _codiceCuraTrasporto = codiceCuraTrasporto;
    }

    public String getCodicePorto() {
        return _codicePorto;
    }

    public void setCodicePorto(String codicePorto) {
        _codicePorto = codicePorto;
    }

    public String getCodiceAspettoEsteriore() {
        return _codiceAspettoEsteriore;
    }

    public void setCodiceAspettoEsteriore(String codiceAspettoEsteriore) {
        _codiceAspettoEsteriore = codiceAspettoEsteriore;
    }

    public String getCodiceVettore1() {
        return _codiceVettore1;
    }

    public void setCodiceVettore1(String codiceVettore1) {
        _codiceVettore1 = codiceVettore1;
    }

    public String getCodiceVettore2() {
        return _codiceVettore2;
    }

    public void setCodiceVettore2(String codiceVettore2) {
        _codiceVettore2 = codiceVettore2;
    }

    public String getLibStr1() {
        return _libStr1;
    }

    public void setLibStr1(String libStr1) {
        _libStr1 = libStr1;
    }

    public String getLibStr2() {
        return _libStr2;
    }

    public void setLibStr2(String libStr2) {
        _libStr2 = libStr2;
    }

    public String getLibStr3() {
        return _libStr3;
    }

    public void setLibStr3(String libStr3) {
        _libStr3 = libStr3;
    }

    public double getLibDbl1() {
        return _libDbl1;
    }

    public void setLibDbl1(double libDbl1) {
        _libDbl1 = libDbl1;
    }

    public double getLibDbl2() {
        return _libDbl2;
    }

    public void setLibDbl2(double libDbl2) {
        _libDbl2 = libDbl2;
    }

    public double getLibDbl3() {
        return _libDbl3;
    }

    public void setLibDbl3(double libDbl3) {
        _libDbl3 = libDbl3;
    }

    public long getLibLng1() {
        return _libLng1;
    }

    public void setLibLng1(long libLng1) {
        _libLng1 = libLng1;
    }

    public long getLibLng2() {
        return _libLng2;
    }

    public void setLibLng2(long libLng2) {
        _libLng2 = libLng2;
    }

    public long getLibLng3() {
        return _libLng3;
    }

    public void setLibLng3(long libLng3) {
        _libLng3 = libLng3;
    }

    public Date getLibDat1() {
        return _libDat1;
    }

    public void setLibDat1(Date libDat1) {
        _libDat1 = libDat1;
    }

    public Date getLibDat2() {
        return _libDat2;
    }

    public void setLibDat2(Date libDat2) {
        _libDat2 = libDat2;
    }

    public Date getLibDat3() {
        return _libDat3;
    }

    public void setLibDat3(Date libDat3) {
        _libDat3 = libDat3;
    }

    public boolean getStato() {
        return _stato;
    }

    public boolean isStato() {
        return _stato;
    }

    public void setStato(boolean stato) {
        _stato = stato;
    }

    public boolean getInviato() {
        return _inviato;
    }

    public boolean isInviato() {
        return _inviato;
    }

    public void setInviato(boolean inviato) {
        _inviato = inviato;
    }

    public String getCodiceConsorzio() {
        return _codiceConsorzio;
    }

    public void setCodiceConsorzio(String codiceConsorzio) {
        _codiceConsorzio = codiceConsorzio;
    }
}
