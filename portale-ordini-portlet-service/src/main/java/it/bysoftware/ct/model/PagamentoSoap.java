package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.PagamentoPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.PagamentoServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.PagamentoServiceSoap
 * @generated
 */
public class PagamentoSoap implements Serializable {
    private int _id;
    private int _anno;
    private String _codiceSoggetto;
    private Date _dataCreazione;
    private Date _dataPagamento;
    private double _credito;
    private double _importo;
    private double _saldo;
    private int _stato;
    private String _nota;

    public PagamentoSoap() {
    }

    public static PagamentoSoap toSoapModel(Pagamento model) {
        PagamentoSoap soapModel = new PagamentoSoap();

        soapModel.setId(model.getId());
        soapModel.setAnno(model.getAnno());
        soapModel.setCodiceSoggetto(model.getCodiceSoggetto());
        soapModel.setDataCreazione(model.getDataCreazione());
        soapModel.setDataPagamento(model.getDataPagamento());
        soapModel.setCredito(model.getCredito());
        soapModel.setImporto(model.getImporto());
        soapModel.setSaldo(model.getSaldo());
        soapModel.setStato(model.getStato());
        soapModel.setNota(model.getNota());

        return soapModel;
    }

    public static PagamentoSoap[] toSoapModels(Pagamento[] models) {
        PagamentoSoap[] soapModels = new PagamentoSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static PagamentoSoap[][] toSoapModels(Pagamento[][] models) {
        PagamentoSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new PagamentoSoap[models.length][models[0].length];
        } else {
            soapModels = new PagamentoSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static PagamentoSoap[] toSoapModels(List<Pagamento> models) {
        List<PagamentoSoap> soapModels = new ArrayList<PagamentoSoap>(models.size());

        for (Pagamento model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new PagamentoSoap[soapModels.size()]);
    }

    public PagamentoPK getPrimaryKey() {
        return new PagamentoPK(_id, _anno, _codiceSoggetto);
    }

    public void setPrimaryKey(PagamentoPK pk) {
        setId(pk.id);
        setAnno(pk.anno);
        setCodiceSoggetto(pk.codiceSoggetto);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public int getAnno() {
        return _anno;
    }

    public void setAnno(int anno) {
        _anno = anno;
    }

    public String getCodiceSoggetto() {
        return _codiceSoggetto;
    }

    public void setCodiceSoggetto(String codiceSoggetto) {
        _codiceSoggetto = codiceSoggetto;
    }

    public Date getDataCreazione() {
        return _dataCreazione;
    }

    public void setDataCreazione(Date dataCreazione) {
        _dataCreazione = dataCreazione;
    }

    public Date getDataPagamento() {
        return _dataPagamento;
    }

    public void setDataPagamento(Date dataPagamento) {
        _dataPagamento = dataPagamento;
    }

    public double getCredito() {
        return _credito;
    }

    public void setCredito(double credito) {
        _credito = credito;
    }

    public double getImporto() {
        return _importo;
    }

    public void setImporto(double importo) {
        _importo = importo;
    }

    public double getSaldo() {
        return _saldo;
    }

    public void setSaldo(double saldo) {
        _saldo = saldo;
    }

    public int getStato() {
        return _stato;
    }

    public void setStato(int stato) {
        _stato = stato;
    }

    public String getNota() {
        return _nota;
    }

    public void setNota(String nota) {
        _nota = nota;
    }
}
