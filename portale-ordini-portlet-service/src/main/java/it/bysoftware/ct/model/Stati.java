package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Stati service. Represents a row in the &quot;StatiEsteri&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see StatiModel
 * @see it.bysoftware.ct.model.impl.StatiImpl
 * @see it.bysoftware.ct.model.impl.StatiModelImpl
 * @generated
 */
public interface Stati extends StatiModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.StatiImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
