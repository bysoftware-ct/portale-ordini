package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the TestataFattureClienti service. Represents a row in the &quot;FattureClientiTestate&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see TestataFattureClientiModel
 * @see it.bysoftware.ct.model.impl.TestataFattureClientiImpl
 * @see it.bysoftware.ct.model.impl.TestataFattureClientiModelImpl
 * @generated
 */
public interface TestataFattureClienti extends TestataFattureClientiModel,
    PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.TestataFattureClientiImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
