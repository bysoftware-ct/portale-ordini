package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Variante service. Represents a row in the &quot;varianti&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see VarianteModel
 * @see it.bysoftware.ct.model.impl.VarianteImpl
 * @see it.bysoftware.ct.model.impl.VarianteModelImpl
 * @generated
 */
public interface Variante extends VarianteModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.VarianteImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
