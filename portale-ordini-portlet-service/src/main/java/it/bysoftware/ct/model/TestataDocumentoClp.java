package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.TestataDocumentoLocalServiceUtil;
import it.bysoftware.ct.service.persistence.TestataDocumentoPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class TestataDocumentoClp extends BaseModelImpl<TestataDocumento>
    implements TestataDocumento {
    private int _anno;
    private String _codiceAttivita;
    private String _codiceCentro;
    private String _codiceDeposito;
    private int _protocollo;
    private String _codiceFornitore;
    private String _tipoDocumento;
    private Date _dataRegistrazione;
    private String _codiceSpedizione;
    private String _codiceCausaleTrasporto;
    private String _codiceCuraTrasporto;
    private String _codicePorto;
    private String _codiceAspettoEsteriore;
    private String _codiceVettore1;
    private String _codiceVettore2;
    private String _libStr1;
    private String _libStr2;
    private String _libStr3;
    private double _libDbl1;
    private double _libDbl2;
    private double _libDbl3;
    private long _libLng1;
    private long _libLng2;
    private long _libLng3;
    private Date _libDat1;
    private Date _libDat2;
    private Date _libDat3;
    private boolean _stato;
    private boolean _inviato;
    private String _codiceConsorzio;
    private BaseModel<?> _testataDocumentoRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public TestataDocumentoClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return TestataDocumento.class;
    }

    @Override
    public String getModelClassName() {
        return TestataDocumento.class.getName();
    }

    @Override
    public TestataDocumentoPK getPrimaryKey() {
        return new TestataDocumentoPK(_anno, _codiceAttivita, _codiceCentro,
            _codiceDeposito, _protocollo, _codiceFornitore, _tipoDocumento);
    }

    @Override
    public void setPrimaryKey(TestataDocumentoPK primaryKey) {
        setAnno(primaryKey.anno);
        setCodiceAttivita(primaryKey.codiceAttivita);
        setCodiceCentro(primaryKey.codiceCentro);
        setCodiceDeposito(primaryKey.codiceDeposito);
        setProtocollo(primaryKey.protocollo);
        setCodiceFornitore(primaryKey.codiceFornitore);
        setTipoDocumento(primaryKey.tipoDocumento);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return new TestataDocumentoPK(_anno, _codiceAttivita, _codiceCentro,
            _codiceDeposito, _protocollo, _codiceFornitore, _tipoDocumento);
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((TestataDocumentoPK) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("anno", getAnno());
        attributes.put("codiceAttivita", getCodiceAttivita());
        attributes.put("codiceCentro", getCodiceCentro());
        attributes.put("codiceDeposito", getCodiceDeposito());
        attributes.put("protocollo", getProtocollo());
        attributes.put("codiceFornitore", getCodiceFornitore());
        attributes.put("tipoDocumento", getTipoDocumento());
        attributes.put("dataRegistrazione", getDataRegistrazione());
        attributes.put("codiceSpedizione", getCodiceSpedizione());
        attributes.put("codiceCausaleTrasporto", getCodiceCausaleTrasporto());
        attributes.put("codiceCuraTrasporto", getCodiceCuraTrasporto());
        attributes.put("codicePorto", getCodicePorto());
        attributes.put("codiceAspettoEsteriore", getCodiceAspettoEsteriore());
        attributes.put("codiceVettore1", getCodiceVettore1());
        attributes.put("codiceVettore2", getCodiceVettore2());
        attributes.put("libStr1", getLibStr1());
        attributes.put("libStr2", getLibStr2());
        attributes.put("libStr3", getLibStr3());
        attributes.put("libDbl1", getLibDbl1());
        attributes.put("libDbl2", getLibDbl2());
        attributes.put("libDbl3", getLibDbl3());
        attributes.put("libLng1", getLibLng1());
        attributes.put("libLng2", getLibLng2());
        attributes.put("libLng3", getLibLng3());
        attributes.put("libDat1", getLibDat1());
        attributes.put("libDat2", getLibDat2());
        attributes.put("libDat3", getLibDat3());
        attributes.put("stato", getStato());
        attributes.put("inviato", getInviato());
        attributes.put("codiceConsorzio", getCodiceConsorzio());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Integer anno = (Integer) attributes.get("anno");

        if (anno != null) {
            setAnno(anno);
        }

        String codiceAttivita = (String) attributes.get("codiceAttivita");

        if (codiceAttivita != null) {
            setCodiceAttivita(codiceAttivita);
        }

        String codiceCentro = (String) attributes.get("codiceCentro");

        if (codiceCentro != null) {
            setCodiceCentro(codiceCentro);
        }

        String codiceDeposito = (String) attributes.get("codiceDeposito");

        if (codiceDeposito != null) {
            setCodiceDeposito(codiceDeposito);
        }

        Integer protocollo = (Integer) attributes.get("protocollo");

        if (protocollo != null) {
            setProtocollo(protocollo);
        }

        String codiceFornitore = (String) attributes.get("codiceFornitore");

        if (codiceFornitore != null) {
            setCodiceFornitore(codiceFornitore);
        }

        String tipoDocumento = (String) attributes.get("tipoDocumento");

        if (tipoDocumento != null) {
            setTipoDocumento(tipoDocumento);
        }

        Date dataRegistrazione = (Date) attributes.get("dataRegistrazione");

        if (dataRegistrazione != null) {
            setDataRegistrazione(dataRegistrazione);
        }

        String codiceSpedizione = (String) attributes.get("codiceSpedizione");

        if (codiceSpedizione != null) {
            setCodiceSpedizione(codiceSpedizione);
        }

        String codiceCausaleTrasporto = (String) attributes.get(
                "codiceCausaleTrasporto");

        if (codiceCausaleTrasporto != null) {
            setCodiceCausaleTrasporto(codiceCausaleTrasporto);
        }

        String codiceCuraTrasporto = (String) attributes.get(
                "codiceCuraTrasporto");

        if (codiceCuraTrasporto != null) {
            setCodiceCuraTrasporto(codiceCuraTrasporto);
        }

        String codicePorto = (String) attributes.get("codicePorto");

        if (codicePorto != null) {
            setCodicePorto(codicePorto);
        }

        String codiceAspettoEsteriore = (String) attributes.get(
                "codiceAspettoEsteriore");

        if (codiceAspettoEsteriore != null) {
            setCodiceAspettoEsteriore(codiceAspettoEsteriore);
        }

        String codiceVettore1 = (String) attributes.get("codiceVettore1");

        if (codiceVettore1 != null) {
            setCodiceVettore1(codiceVettore1);
        }

        String codiceVettore2 = (String) attributes.get("codiceVettore2");

        if (codiceVettore2 != null) {
            setCodiceVettore2(codiceVettore2);
        }

        String libStr1 = (String) attributes.get("libStr1");

        if (libStr1 != null) {
            setLibStr1(libStr1);
        }

        String libStr2 = (String) attributes.get("libStr2");

        if (libStr2 != null) {
            setLibStr2(libStr2);
        }

        String libStr3 = (String) attributes.get("libStr3");

        if (libStr3 != null) {
            setLibStr3(libStr3);
        }

        Double libDbl1 = (Double) attributes.get("libDbl1");

        if (libDbl1 != null) {
            setLibDbl1(libDbl1);
        }

        Double libDbl2 = (Double) attributes.get("libDbl2");

        if (libDbl2 != null) {
            setLibDbl2(libDbl2);
        }

        Double libDbl3 = (Double) attributes.get("libDbl3");

        if (libDbl3 != null) {
            setLibDbl3(libDbl3);
        }

        Long libLng1 = (Long) attributes.get("libLng1");

        if (libLng1 != null) {
            setLibLng1(libLng1);
        }

        Long libLng2 = (Long) attributes.get("libLng2");

        if (libLng2 != null) {
            setLibLng2(libLng2);
        }

        Long libLng3 = (Long) attributes.get("libLng3");

        if (libLng3 != null) {
            setLibLng3(libLng3);
        }

        Date libDat1 = (Date) attributes.get("libDat1");

        if (libDat1 != null) {
            setLibDat1(libDat1);
        }

        Date libDat2 = (Date) attributes.get("libDat2");

        if (libDat2 != null) {
            setLibDat2(libDat2);
        }

        Date libDat3 = (Date) attributes.get("libDat3");

        if (libDat3 != null) {
            setLibDat3(libDat3);
        }

        Boolean stato = (Boolean) attributes.get("stato");

        if (stato != null) {
            setStato(stato);
        }

        Boolean inviato = (Boolean) attributes.get("inviato");

        if (inviato != null) {
            setInviato(inviato);
        }

        String codiceConsorzio = (String) attributes.get("codiceConsorzio");

        if (codiceConsorzio != null) {
            setCodiceConsorzio(codiceConsorzio);
        }
    }

    @Override
    public int getAnno() {
        return _anno;
    }

    @Override
    public void setAnno(int anno) {
        _anno = anno;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setAnno", int.class);

                method.invoke(_testataDocumentoRemoteModel, anno);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceAttivita() {
        return _codiceAttivita;
    }

    @Override
    public void setCodiceAttivita(String codiceAttivita) {
        _codiceAttivita = codiceAttivita;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceAttivita",
                        String.class);

                method.invoke(_testataDocumentoRemoteModel, codiceAttivita);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceCentro() {
        return _codiceCentro;
    }

    @Override
    public void setCodiceCentro(String codiceCentro) {
        _codiceCentro = codiceCentro;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceCentro", String.class);

                method.invoke(_testataDocumentoRemoteModel, codiceCentro);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceDeposito() {
        return _codiceDeposito;
    }

    @Override
    public void setCodiceDeposito(String codiceDeposito) {
        _codiceDeposito = codiceDeposito;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceDeposito",
                        String.class);

                method.invoke(_testataDocumentoRemoteModel, codiceDeposito);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getProtocollo() {
        return _protocollo;
    }

    @Override
    public void setProtocollo(int protocollo) {
        _protocollo = protocollo;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setProtocollo", int.class);

                method.invoke(_testataDocumentoRemoteModel, protocollo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceFornitore() {
        return _codiceFornitore;
    }

    @Override
    public void setCodiceFornitore(String codiceFornitore) {
        _codiceFornitore = codiceFornitore;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceFornitore",
                        String.class);

                method.invoke(_testataDocumentoRemoteModel, codiceFornitore);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getTipoDocumento() {
        return _tipoDocumento;
    }

    @Override
    public void setTipoDocumento(String tipoDocumento) {
        _tipoDocumento = tipoDocumento;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setTipoDocumento", String.class);

                method.invoke(_testataDocumentoRemoteModel, tipoDocumento);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getDataRegistrazione() {
        return _dataRegistrazione;
    }

    @Override
    public void setDataRegistrazione(Date dataRegistrazione) {
        _dataRegistrazione = dataRegistrazione;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setDataRegistrazione",
                        Date.class);

                method.invoke(_testataDocumentoRemoteModel, dataRegistrazione);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceSpedizione() {
        return _codiceSpedizione;
    }

    @Override
    public void setCodiceSpedizione(String codiceSpedizione) {
        _codiceSpedizione = codiceSpedizione;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceSpedizione",
                        String.class);

                method.invoke(_testataDocumentoRemoteModel, codiceSpedizione);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceCausaleTrasporto() {
        return _codiceCausaleTrasporto;
    }

    @Override
    public void setCodiceCausaleTrasporto(String codiceCausaleTrasporto) {
        _codiceCausaleTrasporto = codiceCausaleTrasporto;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceCausaleTrasporto",
                        String.class);

                method.invoke(_testataDocumentoRemoteModel,
                    codiceCausaleTrasporto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceCuraTrasporto() {
        return _codiceCuraTrasporto;
    }

    @Override
    public void setCodiceCuraTrasporto(String codiceCuraTrasporto) {
        _codiceCuraTrasporto = codiceCuraTrasporto;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceCuraTrasporto",
                        String.class);

                method.invoke(_testataDocumentoRemoteModel, codiceCuraTrasporto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodicePorto() {
        return _codicePorto;
    }

    @Override
    public void setCodicePorto(String codicePorto) {
        _codicePorto = codicePorto;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodicePorto", String.class);

                method.invoke(_testataDocumentoRemoteModel, codicePorto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceAspettoEsteriore() {
        return _codiceAspettoEsteriore;
    }

    @Override
    public void setCodiceAspettoEsteriore(String codiceAspettoEsteriore) {
        _codiceAspettoEsteriore = codiceAspettoEsteriore;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceAspettoEsteriore",
                        String.class);

                method.invoke(_testataDocumentoRemoteModel,
                    codiceAspettoEsteriore);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceVettore1() {
        return _codiceVettore1;
    }

    @Override
    public void setCodiceVettore1(String codiceVettore1) {
        _codiceVettore1 = codiceVettore1;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceVettore1",
                        String.class);

                method.invoke(_testataDocumentoRemoteModel, codiceVettore1);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceVettore2() {
        return _codiceVettore2;
    }

    @Override
    public void setCodiceVettore2(String codiceVettore2) {
        _codiceVettore2 = codiceVettore2;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceVettore2",
                        String.class);

                method.invoke(_testataDocumentoRemoteModel, codiceVettore2);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getLibStr1() {
        return _libStr1;
    }

    @Override
    public void setLibStr1(String libStr1) {
        _libStr1 = libStr1;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibStr1", String.class);

                method.invoke(_testataDocumentoRemoteModel, libStr1);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getLibStr2() {
        return _libStr2;
    }

    @Override
    public void setLibStr2(String libStr2) {
        _libStr2 = libStr2;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibStr2", String.class);

                method.invoke(_testataDocumentoRemoteModel, libStr2);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getLibStr3() {
        return _libStr3;
    }

    @Override
    public void setLibStr3(String libStr3) {
        _libStr3 = libStr3;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibStr3", String.class);

                method.invoke(_testataDocumentoRemoteModel, libStr3);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getLibDbl1() {
        return _libDbl1;
    }

    @Override
    public void setLibDbl1(double libDbl1) {
        _libDbl1 = libDbl1;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibDbl1", double.class);

                method.invoke(_testataDocumentoRemoteModel, libDbl1);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getLibDbl2() {
        return _libDbl2;
    }

    @Override
    public void setLibDbl2(double libDbl2) {
        _libDbl2 = libDbl2;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibDbl2", double.class);

                method.invoke(_testataDocumentoRemoteModel, libDbl2);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getLibDbl3() {
        return _libDbl3;
    }

    @Override
    public void setLibDbl3(double libDbl3) {
        _libDbl3 = libDbl3;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibDbl3", double.class);

                method.invoke(_testataDocumentoRemoteModel, libDbl3);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getLibLng1() {
        return _libLng1;
    }

    @Override
    public void setLibLng1(long libLng1) {
        _libLng1 = libLng1;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibLng1", long.class);

                method.invoke(_testataDocumentoRemoteModel, libLng1);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getLibLng2() {
        return _libLng2;
    }

    @Override
    public void setLibLng2(long libLng2) {
        _libLng2 = libLng2;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibLng2", long.class);

                method.invoke(_testataDocumentoRemoteModel, libLng2);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getLibLng3() {
        return _libLng3;
    }

    @Override
    public void setLibLng3(long libLng3) {
        _libLng3 = libLng3;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibLng3", long.class);

                method.invoke(_testataDocumentoRemoteModel, libLng3);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getLibDat1() {
        return _libDat1;
    }

    @Override
    public void setLibDat1(Date libDat1) {
        _libDat1 = libDat1;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibDat1", Date.class);

                method.invoke(_testataDocumentoRemoteModel, libDat1);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getLibDat2() {
        return _libDat2;
    }

    @Override
    public void setLibDat2(Date libDat2) {
        _libDat2 = libDat2;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibDat2", Date.class);

                method.invoke(_testataDocumentoRemoteModel, libDat2);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getLibDat3() {
        return _libDat3;
    }

    @Override
    public void setLibDat3(Date libDat3) {
        _libDat3 = libDat3;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibDat3", Date.class);

                method.invoke(_testataDocumentoRemoteModel, libDat3);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getStato() {
        return _stato;
    }

    @Override
    public boolean isStato() {
        return _stato;
    }

    @Override
    public void setStato(boolean stato) {
        _stato = stato;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setStato", boolean.class);

                method.invoke(_testataDocumentoRemoteModel, stato);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getInviato() {
        return _inviato;
    }

    @Override
    public boolean isInviato() {
        return _inviato;
    }

    @Override
    public void setInviato(boolean inviato) {
        _inviato = inviato;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setInviato", boolean.class);

                method.invoke(_testataDocumentoRemoteModel, inviato);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceConsorzio() {
        return _codiceConsorzio;
    }

    @Override
    public void setCodiceConsorzio(String codiceConsorzio) {
        _codiceConsorzio = codiceConsorzio;

        if (_testataDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _testataDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceConsorzio",
                        String.class);

                method.invoke(_testataDocumentoRemoteModel, codiceConsorzio);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getTestataDocumentoRemoteModel() {
        return _testataDocumentoRemoteModel;
    }

    public void setTestataDocumentoRemoteModel(
        BaseModel<?> testataDocumentoRemoteModel) {
        _testataDocumentoRemoteModel = testataDocumentoRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _testataDocumentoRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_testataDocumentoRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            TestataDocumentoLocalServiceUtil.addTestataDocumento(this);
        } else {
            TestataDocumentoLocalServiceUtil.updateTestataDocumento(this);
        }
    }

    @Override
    public TestataDocumento toEscapedModel() {
        return (TestataDocumento) ProxyUtil.newProxyInstance(TestataDocumento.class.getClassLoader(),
            new Class[] { TestataDocumento.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        TestataDocumentoClp clone = new TestataDocumentoClp();

        clone.setAnno(getAnno());
        clone.setCodiceAttivita(getCodiceAttivita());
        clone.setCodiceCentro(getCodiceCentro());
        clone.setCodiceDeposito(getCodiceDeposito());
        clone.setProtocollo(getProtocollo());
        clone.setCodiceFornitore(getCodiceFornitore());
        clone.setTipoDocumento(getTipoDocumento());
        clone.setDataRegistrazione(getDataRegistrazione());
        clone.setCodiceSpedizione(getCodiceSpedizione());
        clone.setCodiceCausaleTrasporto(getCodiceCausaleTrasporto());
        clone.setCodiceCuraTrasporto(getCodiceCuraTrasporto());
        clone.setCodicePorto(getCodicePorto());
        clone.setCodiceAspettoEsteriore(getCodiceAspettoEsteriore());
        clone.setCodiceVettore1(getCodiceVettore1());
        clone.setCodiceVettore2(getCodiceVettore2());
        clone.setLibStr1(getLibStr1());
        clone.setLibStr2(getLibStr2());
        clone.setLibStr3(getLibStr3());
        clone.setLibDbl1(getLibDbl1());
        clone.setLibDbl2(getLibDbl2());
        clone.setLibDbl3(getLibDbl3());
        clone.setLibLng1(getLibLng1());
        clone.setLibLng2(getLibLng2());
        clone.setLibLng3(getLibLng3());
        clone.setLibDat1(getLibDat1());
        clone.setLibDat2(getLibDat2());
        clone.setLibDat3(getLibDat3());
        clone.setStato(getStato());
        clone.setInviato(getInviato());
        clone.setCodiceConsorzio(getCodiceConsorzio());

        return clone;
    }

    @Override
    public int compareTo(TestataDocumento testataDocumento) {
        TestataDocumentoPK primaryKey = testataDocumento.getPrimaryKey();

        return getPrimaryKey().compareTo(primaryKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof TestataDocumentoClp)) {
            return false;
        }

        TestataDocumentoClp testataDocumento = (TestataDocumentoClp) obj;

        TestataDocumentoPK primaryKey = testataDocumento.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(61);

        sb.append("{anno=");
        sb.append(getAnno());
        sb.append(", codiceAttivita=");
        sb.append(getCodiceAttivita());
        sb.append(", codiceCentro=");
        sb.append(getCodiceCentro());
        sb.append(", codiceDeposito=");
        sb.append(getCodiceDeposito());
        sb.append(", protocollo=");
        sb.append(getProtocollo());
        sb.append(", codiceFornitore=");
        sb.append(getCodiceFornitore());
        sb.append(", tipoDocumento=");
        sb.append(getTipoDocumento());
        sb.append(", dataRegistrazione=");
        sb.append(getDataRegistrazione());
        sb.append(", codiceSpedizione=");
        sb.append(getCodiceSpedizione());
        sb.append(", codiceCausaleTrasporto=");
        sb.append(getCodiceCausaleTrasporto());
        sb.append(", codiceCuraTrasporto=");
        sb.append(getCodiceCuraTrasporto());
        sb.append(", codicePorto=");
        sb.append(getCodicePorto());
        sb.append(", codiceAspettoEsteriore=");
        sb.append(getCodiceAspettoEsteriore());
        sb.append(", codiceVettore1=");
        sb.append(getCodiceVettore1());
        sb.append(", codiceVettore2=");
        sb.append(getCodiceVettore2());
        sb.append(", libStr1=");
        sb.append(getLibStr1());
        sb.append(", libStr2=");
        sb.append(getLibStr2());
        sb.append(", libStr3=");
        sb.append(getLibStr3());
        sb.append(", libDbl1=");
        sb.append(getLibDbl1());
        sb.append(", libDbl2=");
        sb.append(getLibDbl2());
        sb.append(", libDbl3=");
        sb.append(getLibDbl3());
        sb.append(", libLng1=");
        sb.append(getLibLng1());
        sb.append(", libLng2=");
        sb.append(getLibLng2());
        sb.append(", libLng3=");
        sb.append(getLibLng3());
        sb.append(", libDat1=");
        sb.append(getLibDat1());
        sb.append(", libDat2=");
        sb.append(getLibDat2());
        sb.append(", libDat3=");
        sb.append(getLibDat3());
        sb.append(", stato=");
        sb.append(getStato());
        sb.append(", inviato=");
        sb.append(getInviato());
        sb.append(", codiceConsorzio=");
        sb.append(getCodiceConsorzio());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(94);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.TestataDocumento");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>anno</column-name><column-value><![CDATA[");
        sb.append(getAnno());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceAttivita</column-name><column-value><![CDATA[");
        sb.append(getCodiceAttivita());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceCentro</column-name><column-value><![CDATA[");
        sb.append(getCodiceCentro());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceDeposito</column-name><column-value><![CDATA[");
        sb.append(getCodiceDeposito());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>protocollo</column-name><column-value><![CDATA[");
        sb.append(getProtocollo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceFornitore</column-name><column-value><![CDATA[");
        sb.append(getCodiceFornitore());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tipoDocumento</column-name><column-value><![CDATA[");
        sb.append(getTipoDocumento());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>dataRegistrazione</column-name><column-value><![CDATA[");
        sb.append(getDataRegistrazione());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceSpedizione</column-name><column-value><![CDATA[");
        sb.append(getCodiceSpedizione());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceCausaleTrasporto</column-name><column-value><![CDATA[");
        sb.append(getCodiceCausaleTrasporto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceCuraTrasporto</column-name><column-value><![CDATA[");
        sb.append(getCodiceCuraTrasporto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codicePorto</column-name><column-value><![CDATA[");
        sb.append(getCodicePorto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceAspettoEsteriore</column-name><column-value><![CDATA[");
        sb.append(getCodiceAspettoEsteriore());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceVettore1</column-name><column-value><![CDATA[");
        sb.append(getCodiceVettore1());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceVettore2</column-name><column-value><![CDATA[");
        sb.append(getCodiceVettore2());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libStr1</column-name><column-value><![CDATA[");
        sb.append(getLibStr1());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libStr2</column-name><column-value><![CDATA[");
        sb.append(getLibStr2());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libStr3</column-name><column-value><![CDATA[");
        sb.append(getLibStr3());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libDbl1</column-name><column-value><![CDATA[");
        sb.append(getLibDbl1());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libDbl2</column-name><column-value><![CDATA[");
        sb.append(getLibDbl2());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libDbl3</column-name><column-value><![CDATA[");
        sb.append(getLibDbl3());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libLng1</column-name><column-value><![CDATA[");
        sb.append(getLibLng1());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libLng2</column-name><column-value><![CDATA[");
        sb.append(getLibLng2());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libLng3</column-name><column-value><![CDATA[");
        sb.append(getLibLng3());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libDat1</column-name><column-value><![CDATA[");
        sb.append(getLibDat1());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libDat2</column-name><column-value><![CDATA[");
        sb.append(getLibDat2());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libDat3</column-name><column-value><![CDATA[");
        sb.append(getLibDat3());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>stato</column-name><column-value><![CDATA[");
        sb.append(getStato());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>inviato</column-name><column-value><![CDATA[");
        sb.append(getInviato());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceConsorzio</column-name><column-value><![CDATA[");
        sb.append(getCodiceConsorzio());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
