package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.CausaliEstrattoContoLocalServiceUtil;
import it.bysoftware.ct.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class CausaliEstrattoContoClp extends BaseModelImpl<CausaliEstrattoConto>
    implements CausaliEstrattoConto {
    private String _codiceCausaleEC;
    private String _descrizioneCausaleEC;
    private BaseModel<?> _causaliEstrattoContoRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public CausaliEstrattoContoClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return CausaliEstrattoConto.class;
    }

    @Override
    public String getModelClassName() {
        return CausaliEstrattoConto.class.getName();
    }

    @Override
    public String getPrimaryKey() {
        return _codiceCausaleEC;
    }

    @Override
    public void setPrimaryKey(String primaryKey) {
        setCodiceCausaleEC(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _codiceCausaleEC;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((String) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceCausaleEC", getCodiceCausaleEC());
        attributes.put("descrizioneCausaleEC", getDescrizioneCausaleEC());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceCausaleEC = (String) attributes.get("codiceCausaleEC");

        if (codiceCausaleEC != null) {
            setCodiceCausaleEC(codiceCausaleEC);
        }

        String descrizioneCausaleEC = (String) attributes.get(
                "descrizioneCausaleEC");

        if (descrizioneCausaleEC != null) {
            setDescrizioneCausaleEC(descrizioneCausaleEC);
        }
    }

    @Override
    public String getCodiceCausaleEC() {
        return _codiceCausaleEC;
    }

    @Override
    public void setCodiceCausaleEC(String codiceCausaleEC) {
        _codiceCausaleEC = codiceCausaleEC;

        if (_causaliEstrattoContoRemoteModel != null) {
            try {
                Class<?> clazz = _causaliEstrattoContoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceCausaleEC",
                        String.class);

                method.invoke(_causaliEstrattoContoRemoteModel, codiceCausaleEC);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getDescrizioneCausaleEC() {
        return _descrizioneCausaleEC;
    }

    @Override
    public void setDescrizioneCausaleEC(String descrizioneCausaleEC) {
        _descrizioneCausaleEC = descrizioneCausaleEC;

        if (_causaliEstrattoContoRemoteModel != null) {
            try {
                Class<?> clazz = _causaliEstrattoContoRemoteModel.getClass();

                Method method = clazz.getMethod("setDescrizioneCausaleEC",
                        String.class);

                method.invoke(_causaliEstrattoContoRemoteModel,
                    descrizioneCausaleEC);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getCausaliEstrattoContoRemoteModel() {
        return _causaliEstrattoContoRemoteModel;
    }

    public void setCausaliEstrattoContoRemoteModel(
        BaseModel<?> causaliEstrattoContoRemoteModel) {
        _causaliEstrattoContoRemoteModel = causaliEstrattoContoRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _causaliEstrattoContoRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_causaliEstrattoContoRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            CausaliEstrattoContoLocalServiceUtil.addCausaliEstrattoConto(this);
        } else {
            CausaliEstrattoContoLocalServiceUtil.updateCausaliEstrattoConto(this);
        }
    }

    @Override
    public CausaliEstrattoConto toEscapedModel() {
        return (CausaliEstrattoConto) ProxyUtil.newProxyInstance(CausaliEstrattoConto.class.getClassLoader(),
            new Class[] { CausaliEstrattoConto.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        CausaliEstrattoContoClp clone = new CausaliEstrattoContoClp();

        clone.setCodiceCausaleEC(getCodiceCausaleEC());
        clone.setDescrizioneCausaleEC(getDescrizioneCausaleEC());

        return clone;
    }

    @Override
    public int compareTo(CausaliEstrattoConto causaliEstrattoConto) {
        String primaryKey = causaliEstrattoConto.getPrimaryKey();

        return getPrimaryKey().compareTo(primaryKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CausaliEstrattoContoClp)) {
            return false;
        }

        CausaliEstrattoContoClp causaliEstrattoConto = (CausaliEstrattoContoClp) obj;

        String primaryKey = causaliEstrattoConto.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(5);

        sb.append("{codiceCausaleEC=");
        sb.append(getCodiceCausaleEC());
        sb.append(", descrizioneCausaleEC=");
        sb.append(getDescrizioneCausaleEC());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(10);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.CausaliEstrattoConto");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>codiceCausaleEC</column-name><column-value><![CDATA[");
        sb.append(getCodiceCausaleEC());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>descrizioneCausaleEC</column-name><column-value><![CDATA[");
        sb.append(getDescrizioneCausaleEC());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
