package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.DescrizioniVariantiLocalServiceUtil;
import it.bysoftware.ct.service.persistence.DescrizioniVariantiPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class DescrizioniVariantiClp extends BaseModelImpl<DescrizioniVarianti>
    implements DescrizioniVarianti {
    private String _codiceArticolo;
    private String _codiceVariante;
    private String _descrizione;
    private BaseModel<?> _descrizioniVariantiRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public DescrizioniVariantiClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return DescrizioniVarianti.class;
    }

    @Override
    public String getModelClassName() {
        return DescrizioniVarianti.class.getName();
    }

    @Override
    public DescrizioniVariantiPK getPrimaryKey() {
        return new DescrizioniVariantiPK(_codiceArticolo, _codiceVariante);
    }

    @Override
    public void setPrimaryKey(DescrizioniVariantiPK primaryKey) {
        setCodiceArticolo(primaryKey.codiceArticolo);
        setCodiceVariante(primaryKey.codiceVariante);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return new DescrizioniVariantiPK(_codiceArticolo, _codiceVariante);
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((DescrizioniVariantiPK) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceArticolo", getCodiceArticolo());
        attributes.put("codiceVariante", getCodiceVariante());
        attributes.put("descrizione", getDescrizione());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceArticolo = (String) attributes.get("codiceArticolo");

        if (codiceArticolo != null) {
            setCodiceArticolo(codiceArticolo);
        }

        String codiceVariante = (String) attributes.get("codiceVariante");

        if (codiceVariante != null) {
            setCodiceVariante(codiceVariante);
        }

        String descrizione = (String) attributes.get("descrizione");

        if (descrizione != null) {
            setDescrizione(descrizione);
        }
    }

    @Override
    public String getCodiceArticolo() {
        return _codiceArticolo;
    }

    @Override
    public void setCodiceArticolo(String codiceArticolo) {
        _codiceArticolo = codiceArticolo;

        if (_descrizioniVariantiRemoteModel != null) {
            try {
                Class<?> clazz = _descrizioniVariantiRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceArticolo",
                        String.class);

                method.invoke(_descrizioniVariantiRemoteModel, codiceArticolo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceVariante() {
        return _codiceVariante;
    }

    @Override
    public void setCodiceVariante(String codiceVariante) {
        _codiceVariante = codiceVariante;

        if (_descrizioniVariantiRemoteModel != null) {
            try {
                Class<?> clazz = _descrizioniVariantiRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceVariante",
                        String.class);

                method.invoke(_descrizioniVariantiRemoteModel, codiceVariante);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getDescrizione() {
        return _descrizione;
    }

    @Override
    public void setDescrizione(String descrizione) {
        _descrizione = descrizione;

        if (_descrizioniVariantiRemoteModel != null) {
            try {
                Class<?> clazz = _descrizioniVariantiRemoteModel.getClass();

                Method method = clazz.getMethod("setDescrizione", String.class);

                method.invoke(_descrizioniVariantiRemoteModel, descrizione);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getDescrizioniVariantiRemoteModel() {
        return _descrizioniVariantiRemoteModel;
    }

    public void setDescrizioniVariantiRemoteModel(
        BaseModel<?> descrizioniVariantiRemoteModel) {
        _descrizioniVariantiRemoteModel = descrizioniVariantiRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _descrizioniVariantiRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_descrizioniVariantiRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            DescrizioniVariantiLocalServiceUtil.addDescrizioniVarianti(this);
        } else {
            DescrizioniVariantiLocalServiceUtil.updateDescrizioniVarianti(this);
        }
    }

    @Override
    public DescrizioniVarianti toEscapedModel() {
        return (DescrizioniVarianti) ProxyUtil.newProxyInstance(DescrizioniVarianti.class.getClassLoader(),
            new Class[] { DescrizioniVarianti.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        DescrizioniVariantiClp clone = new DescrizioniVariantiClp();

        clone.setCodiceArticolo(getCodiceArticolo());
        clone.setCodiceVariante(getCodiceVariante());
        clone.setDescrizione(getDescrizione());

        return clone;
    }

    @Override
    public int compareTo(DescrizioniVarianti descrizioniVarianti) {
        int value = 0;

        value = getDescrizione().compareTo(descrizioniVarianti.getDescrizione());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof DescrizioniVariantiClp)) {
            return false;
        }

        DescrizioniVariantiClp descrizioniVarianti = (DescrizioniVariantiClp) obj;

        DescrizioniVariantiPK primaryKey = descrizioniVarianti.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{codiceArticolo=");
        sb.append(getCodiceArticolo());
        sb.append(", codiceVariante=");
        sb.append(getCodiceVariante());
        sb.append(", descrizione=");
        sb.append(getDescrizione());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(13);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.DescrizioniVarianti");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>codiceArticolo</column-name><column-value><![CDATA[");
        sb.append(getCodiceArticolo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceVariante</column-name><column-value><![CDATA[");
        sb.append(getCodiceVariante());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>descrizione</column-name><column-value><![CDATA[");
        sb.append(getDescrizione());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
