package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.OrdineLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class OrdineClp extends BaseModelImpl<Ordine> implements Ordine {
    private long _id;
    private Date _dataInserimento;
    private String _idCliente;
    private int _stato;
    private double _importo;
    private String _note;
    private Date _dataConsegna;
    private String _idTrasportatore;
    private String _idTrasportatore2;
    private long _idAgente;
    private long _idTipoCarrello;
    private String _luogoCarico;
    private String _noteCliente;
    private int _anno;
    private int _numero;
    private String _centro;
    private boolean _modificato;
    private String _codiceIva;
    private String _cliente;
    private String _vettore;
    private String _pagamento;
    private boolean _ddtGenerati;
    private boolean _stampaEtichette;
    private String _numOrdineGT;
    private Date _dataOrdineGT;
    private BaseModel<?> _ordineRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public OrdineClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Ordine.class;
    }

    @Override
    public String getModelClassName() {
        return Ordine.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("dataInserimento", getDataInserimento());
        attributes.put("idCliente", getIdCliente());
        attributes.put("stato", getStato());
        attributes.put("importo", getImporto());
        attributes.put("note", getNote());
        attributes.put("dataConsegna", getDataConsegna());
        attributes.put("idTrasportatore", getIdTrasportatore());
        attributes.put("idTrasportatore2", getIdTrasportatore2());
        attributes.put("idAgente", getIdAgente());
        attributes.put("idTipoCarrello", getIdTipoCarrello());
        attributes.put("luogoCarico", getLuogoCarico());
        attributes.put("noteCliente", getNoteCliente());
        attributes.put("anno", getAnno());
        attributes.put("numero", getNumero());
        attributes.put("centro", getCentro());
        attributes.put("modificato", getModificato());
        attributes.put("codiceIva", getCodiceIva());
        attributes.put("cliente", getCliente());
        attributes.put("vettore", getVettore());
        attributes.put("pagamento", getPagamento());
        attributes.put("ddtGenerati", getDdtGenerati());
        attributes.put("stampaEtichette", getStampaEtichette());
        attributes.put("numOrdineGT", getNumOrdineGT());
        attributes.put("dataOrdineGT", getDataOrdineGT());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Date dataInserimento = (Date) attributes.get("dataInserimento");

        if (dataInserimento != null) {
            setDataInserimento(dataInserimento);
        }

        String idCliente = (String) attributes.get("idCliente");

        if (idCliente != null) {
            setIdCliente(idCliente);
        }

        Integer stato = (Integer) attributes.get("stato");

        if (stato != null) {
            setStato(stato);
        }

        Double importo = (Double) attributes.get("importo");

        if (importo != null) {
            setImporto(importo);
        }

        String note = (String) attributes.get("note");

        if (note != null) {
            setNote(note);
        }

        Date dataConsegna = (Date) attributes.get("dataConsegna");

        if (dataConsegna != null) {
            setDataConsegna(dataConsegna);
        }

        String idTrasportatore = (String) attributes.get("idTrasportatore");

        if (idTrasportatore != null) {
            setIdTrasportatore(idTrasportatore);
        }

        String idTrasportatore2 = (String) attributes.get("idTrasportatore2");

        if (idTrasportatore2 != null) {
            setIdTrasportatore2(idTrasportatore2);
        }

        Long idAgente = (Long) attributes.get("idAgente");

        if (idAgente != null) {
            setIdAgente(idAgente);
        }

        Long idTipoCarrello = (Long) attributes.get("idTipoCarrello");

        if (idTipoCarrello != null) {
            setIdTipoCarrello(idTipoCarrello);
        }

        String luogoCarico = (String) attributes.get("luogoCarico");

        if (luogoCarico != null) {
            setLuogoCarico(luogoCarico);
        }

        String noteCliente = (String) attributes.get("noteCliente");

        if (noteCliente != null) {
            setNoteCliente(noteCliente);
        }

        Integer anno = (Integer) attributes.get("anno");

        if (anno != null) {
            setAnno(anno);
        }

        Integer numero = (Integer) attributes.get("numero");

        if (numero != null) {
            setNumero(numero);
        }

        String centro = (String) attributes.get("centro");

        if (centro != null) {
            setCentro(centro);
        }

        Boolean modificato = (Boolean) attributes.get("modificato");

        if (modificato != null) {
            setModificato(modificato);
        }

        String codiceIva = (String) attributes.get("codiceIva");

        if (codiceIva != null) {
            setCodiceIva(codiceIva);
        }

        String cliente = (String) attributes.get("cliente");

        if (cliente != null) {
            setCliente(cliente);
        }

        String vettore = (String) attributes.get("vettore");

        if (vettore != null) {
            setVettore(vettore);
        }

        String pagamento = (String) attributes.get("pagamento");

        if (pagamento != null) {
            setPagamento(pagamento);
        }

        Boolean ddtGenerati = (Boolean) attributes.get("ddtGenerati");

        if (ddtGenerati != null) {
            setDdtGenerati(ddtGenerati);
        }

        Boolean stampaEtichette = (Boolean) attributes.get("stampaEtichette");

        if (stampaEtichette != null) {
            setStampaEtichette(stampaEtichette);
        }

        String numOrdineGT = (String) attributes.get("numOrdineGT");

        if (numOrdineGT != null) {
            setNumOrdineGT(numOrdineGT);
        }

        Date dataOrdineGT = (Date) attributes.get("dataOrdineGT");

        if (dataOrdineGT != null) {
            setDataOrdineGT(dataOrdineGT);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_ordineRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getDataInserimento() {
        return _dataInserimento;
    }

    @Override
    public void setDataInserimento(Date dataInserimento) {
        _dataInserimento = dataInserimento;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setDataInserimento", Date.class);

                method.invoke(_ordineRemoteModel, dataInserimento);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getIdCliente() {
        return _idCliente;
    }

    @Override
    public void setIdCliente(String idCliente) {
        _idCliente = idCliente;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setIdCliente", String.class);

                method.invoke(_ordineRemoteModel, idCliente);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getStato() {
        return _stato;
    }

    @Override
    public void setStato(int stato) {
        _stato = stato;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setStato", int.class);

                method.invoke(_ordineRemoteModel, stato);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getImporto() {
        return _importo;
    }

    @Override
    public void setImporto(double importo) {
        _importo = importo;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setImporto", double.class);

                method.invoke(_ordineRemoteModel, importo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNote() {
        return _note;
    }

    @Override
    public void setNote(String note) {
        _note = note;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setNote", String.class);

                method.invoke(_ordineRemoteModel, note);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getDataConsegna() {
        return _dataConsegna;
    }

    @Override
    public void setDataConsegna(Date dataConsegna) {
        _dataConsegna = dataConsegna;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setDataConsegna", Date.class);

                method.invoke(_ordineRemoteModel, dataConsegna);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getIdTrasportatore() {
        return _idTrasportatore;
    }

    @Override
    public void setIdTrasportatore(String idTrasportatore) {
        _idTrasportatore = idTrasportatore;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setIdTrasportatore",
                        String.class);

                method.invoke(_ordineRemoteModel, idTrasportatore);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getIdTrasportatore2() {
        return _idTrasportatore2;
    }

    @Override
    public void setIdTrasportatore2(String idTrasportatore2) {
        _idTrasportatore2 = idTrasportatore2;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setIdTrasportatore2",
                        String.class);

                method.invoke(_ordineRemoteModel, idTrasportatore2);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getIdAgente() {
        return _idAgente;
    }

    @Override
    public void setIdAgente(long idAgente) {
        _idAgente = idAgente;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setIdAgente", long.class);

                method.invoke(_ordineRemoteModel, idAgente);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getIdTipoCarrello() {
        return _idTipoCarrello;
    }

    @Override
    public void setIdTipoCarrello(long idTipoCarrello) {
        _idTipoCarrello = idTipoCarrello;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setIdTipoCarrello", long.class);

                method.invoke(_ordineRemoteModel, idTipoCarrello);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getLuogoCarico() {
        return _luogoCarico;
    }

    @Override
    public void setLuogoCarico(String luogoCarico) {
        _luogoCarico = luogoCarico;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setLuogoCarico", String.class);

                method.invoke(_ordineRemoteModel, luogoCarico);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNoteCliente() {
        return _noteCliente;
    }

    @Override
    public void setNoteCliente(String noteCliente) {
        _noteCliente = noteCliente;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setNoteCliente", String.class);

                method.invoke(_ordineRemoteModel, noteCliente);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getAnno() {
        return _anno;
    }

    @Override
    public void setAnno(int anno) {
        _anno = anno;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setAnno", int.class);

                method.invoke(_ordineRemoteModel, anno);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getNumero() {
        return _numero;
    }

    @Override
    public void setNumero(int numero) {
        _numero = numero;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setNumero", int.class);

                method.invoke(_ordineRemoteModel, numero);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCentro() {
        return _centro;
    }

    @Override
    public void setCentro(String centro) {
        _centro = centro;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setCentro", String.class);

                method.invoke(_ordineRemoteModel, centro);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getModificato() {
        return _modificato;
    }

    @Override
    public boolean isModificato() {
        return _modificato;
    }

    @Override
    public void setModificato(boolean modificato) {
        _modificato = modificato;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setModificato", boolean.class);

                method.invoke(_ordineRemoteModel, modificato);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceIva() {
        return _codiceIva;
    }

    @Override
    public void setCodiceIva(String codiceIva) {
        _codiceIva = codiceIva;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceIva", String.class);

                method.invoke(_ordineRemoteModel, codiceIva);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCliente() {
        return _cliente;
    }

    @Override
    public void setCliente(String cliente) {
        _cliente = cliente;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setCliente", String.class);

                method.invoke(_ordineRemoteModel, cliente);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getVettore() {
        return _vettore;
    }

    @Override
    public void setVettore(String vettore) {
        _vettore = vettore;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setVettore", String.class);

                method.invoke(_ordineRemoteModel, vettore);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getPagamento() {
        return _pagamento;
    }

    @Override
    public void setPagamento(String pagamento) {
        _pagamento = pagamento;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setPagamento", String.class);

                method.invoke(_ordineRemoteModel, pagamento);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getDdtGenerati() {
        return _ddtGenerati;
    }

    @Override
    public boolean isDdtGenerati() {
        return _ddtGenerati;
    }

    @Override
    public void setDdtGenerati(boolean ddtGenerati) {
        _ddtGenerati = ddtGenerati;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setDdtGenerati", boolean.class);

                method.invoke(_ordineRemoteModel, ddtGenerati);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getStampaEtichette() {
        return _stampaEtichette;
    }

    @Override
    public boolean isStampaEtichette() {
        return _stampaEtichette;
    }

    @Override
    public void setStampaEtichette(boolean stampaEtichette) {
        _stampaEtichette = stampaEtichette;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setStampaEtichette",
                        boolean.class);

                method.invoke(_ordineRemoteModel, stampaEtichette);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNumOrdineGT() {
        return _numOrdineGT;
    }

    @Override
    public void setNumOrdineGT(String numOrdineGT) {
        _numOrdineGT = numOrdineGT;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setNumOrdineGT", String.class);

                method.invoke(_ordineRemoteModel, numOrdineGT);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getDataOrdineGT() {
        return _dataOrdineGT;
    }

    @Override
    public void setDataOrdineGT(Date dataOrdineGT) {
        _dataOrdineGT = dataOrdineGT;

        if (_ordineRemoteModel != null) {
            try {
                Class<?> clazz = _ordineRemoteModel.getClass();

                Method method = clazz.getMethod("setDataOrdineGT", Date.class);

                method.invoke(_ordineRemoteModel, dataOrdineGT);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getOrdineRemoteModel() {
        return _ordineRemoteModel;
    }

    public void setOrdineRemoteModel(BaseModel<?> ordineRemoteModel) {
        _ordineRemoteModel = ordineRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _ordineRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_ordineRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            OrdineLocalServiceUtil.addOrdine(this);
        } else {
            OrdineLocalServiceUtil.updateOrdine(this);
        }
    }

    @Override
    public Ordine toEscapedModel() {
        return (Ordine) ProxyUtil.newProxyInstance(Ordine.class.getClassLoader(),
            new Class[] { Ordine.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        OrdineClp clone = new OrdineClp();

        clone.setId(getId());
        clone.setDataInserimento(getDataInserimento());
        clone.setIdCliente(getIdCliente());
        clone.setStato(getStato());
        clone.setImporto(getImporto());
        clone.setNote(getNote());
        clone.setDataConsegna(getDataConsegna());
        clone.setIdTrasportatore(getIdTrasportatore());
        clone.setIdTrasportatore2(getIdTrasportatore2());
        clone.setIdAgente(getIdAgente());
        clone.setIdTipoCarrello(getIdTipoCarrello());
        clone.setLuogoCarico(getLuogoCarico());
        clone.setNoteCliente(getNoteCliente());
        clone.setAnno(getAnno());
        clone.setNumero(getNumero());
        clone.setCentro(getCentro());
        clone.setModificato(getModificato());
        clone.setCodiceIva(getCodiceIva());
        clone.setCliente(getCliente());
        clone.setVettore(getVettore());
        clone.setPagamento(getPagamento());
        clone.setDdtGenerati(getDdtGenerati());
        clone.setStampaEtichette(getStampaEtichette());
        clone.setNumOrdineGT(getNumOrdineGT());
        clone.setDataOrdineGT(getDataOrdineGT());

        return clone;
    }

    @Override
    public int compareTo(Ordine ordine) {
        long primaryKey = ordine.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof OrdineClp)) {
            return false;
        }

        OrdineClp ordine = (OrdineClp) obj;

        long primaryKey = ordine.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(51);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", dataInserimento=");
        sb.append(getDataInserimento());
        sb.append(", idCliente=");
        sb.append(getIdCliente());
        sb.append(", stato=");
        sb.append(getStato());
        sb.append(", importo=");
        sb.append(getImporto());
        sb.append(", note=");
        sb.append(getNote());
        sb.append(", dataConsegna=");
        sb.append(getDataConsegna());
        sb.append(", idTrasportatore=");
        sb.append(getIdTrasportatore());
        sb.append(", idTrasportatore2=");
        sb.append(getIdTrasportatore2());
        sb.append(", idAgente=");
        sb.append(getIdAgente());
        sb.append(", idTipoCarrello=");
        sb.append(getIdTipoCarrello());
        sb.append(", luogoCarico=");
        sb.append(getLuogoCarico());
        sb.append(", noteCliente=");
        sb.append(getNoteCliente());
        sb.append(", anno=");
        sb.append(getAnno());
        sb.append(", numero=");
        sb.append(getNumero());
        sb.append(", centro=");
        sb.append(getCentro());
        sb.append(", modificato=");
        sb.append(getModificato());
        sb.append(", codiceIva=");
        sb.append(getCodiceIva());
        sb.append(", cliente=");
        sb.append(getCliente());
        sb.append(", vettore=");
        sb.append(getVettore());
        sb.append(", pagamento=");
        sb.append(getPagamento());
        sb.append(", ddtGenerati=");
        sb.append(getDdtGenerati());
        sb.append(", stampaEtichette=");
        sb.append(getStampaEtichette());
        sb.append(", numOrdineGT=");
        sb.append(getNumOrdineGT());
        sb.append(", dataOrdineGT=");
        sb.append(getDataOrdineGT());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(79);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.Ordine");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>dataInserimento</column-name><column-value><![CDATA[");
        sb.append(getDataInserimento());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>idCliente</column-name><column-value><![CDATA[");
        sb.append(getIdCliente());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>stato</column-name><column-value><![CDATA[");
        sb.append(getStato());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>importo</column-name><column-value><![CDATA[");
        sb.append(getImporto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>note</column-name><column-value><![CDATA[");
        sb.append(getNote());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>dataConsegna</column-name><column-value><![CDATA[");
        sb.append(getDataConsegna());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>idTrasportatore</column-name><column-value><![CDATA[");
        sb.append(getIdTrasportatore());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>idTrasportatore2</column-name><column-value><![CDATA[");
        sb.append(getIdTrasportatore2());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>idAgente</column-name><column-value><![CDATA[");
        sb.append(getIdAgente());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>idTipoCarrello</column-name><column-value><![CDATA[");
        sb.append(getIdTipoCarrello());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>luogoCarico</column-name><column-value><![CDATA[");
        sb.append(getLuogoCarico());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>noteCliente</column-name><column-value><![CDATA[");
        sb.append(getNoteCliente());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>anno</column-name><column-value><![CDATA[");
        sb.append(getAnno());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>numero</column-name><column-value><![CDATA[");
        sb.append(getNumero());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>centro</column-name><column-value><![CDATA[");
        sb.append(getCentro());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modificato</column-name><column-value><![CDATA[");
        sb.append(getModificato());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceIva</column-name><column-value><![CDATA[");
        sb.append(getCodiceIva());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>cliente</column-name><column-value><![CDATA[");
        sb.append(getCliente());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>vettore</column-name><column-value><![CDATA[");
        sb.append(getVettore());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>pagamento</column-name><column-value><![CDATA[");
        sb.append(getPagamento());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>ddtGenerati</column-name><column-value><![CDATA[");
        sb.append(getDdtGenerati());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>stampaEtichette</column-name><column-value><![CDATA[");
        sb.append(getStampaEtichette());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>numOrdineGT</column-name><column-value><![CDATA[");
        sb.append(getNumOrdineGT());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>dataOrdineGT</column-name><column-value><![CDATA[");
        sb.append(getDataOrdineGT());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
