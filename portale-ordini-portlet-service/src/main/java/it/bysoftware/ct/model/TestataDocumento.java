package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the TestataDocumento service. Represents a row in the &quot;_TestateDocumenti&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see TestataDocumentoModel
 * @see it.bysoftware.ct.model.impl.TestataDocumentoImpl
 * @see it.bysoftware.ct.model.impl.TestataDocumentoModelImpl
 * @generated
 */
public interface TestataDocumento extends TestataDocumentoModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.TestataDocumentoImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
