package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Sottoconti}.
 * </p>
 *
 * @author Mario Torrisi
 * @see Sottoconti
 * @generated
 */
public class SottocontiWrapper implements Sottoconti, ModelWrapper<Sottoconti> {
    private Sottoconti _sottoconti;

    public SottocontiWrapper(Sottoconti sottoconti) {
        _sottoconti = sottoconti;
    }

    @Override
    public Class<?> getModelClass() {
        return Sottoconti.class;
    }

    @Override
    public String getModelClassName() {
        return Sottoconti.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codice", getCodice());
        attributes.put("descrizione", getDescrizione());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codice = (String) attributes.get("codice");

        if (codice != null) {
            setCodice(codice);
        }

        String descrizione = (String) attributes.get("descrizione");

        if (descrizione != null) {
            setDescrizione(descrizione);
        }
    }

    /**
    * Returns the primary key of this sottoconti.
    *
    * @return the primary key of this sottoconti
    */
    @Override
    public java.lang.String getPrimaryKey() {
        return _sottoconti.getPrimaryKey();
    }

    /**
    * Sets the primary key of this sottoconti.
    *
    * @param primaryKey the primary key of this sottoconti
    */
    @Override
    public void setPrimaryKey(java.lang.String primaryKey) {
        _sottoconti.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the codice of this sottoconti.
    *
    * @return the codice of this sottoconti
    */
    @Override
    public java.lang.String getCodice() {
        return _sottoconti.getCodice();
    }

    /**
    * Sets the codice of this sottoconti.
    *
    * @param codice the codice of this sottoconti
    */
    @Override
    public void setCodice(java.lang.String codice) {
        _sottoconti.setCodice(codice);
    }

    /**
    * Returns the descrizione of this sottoconti.
    *
    * @return the descrizione of this sottoconti
    */
    @Override
    public java.lang.String getDescrizione() {
        return _sottoconti.getDescrizione();
    }

    /**
    * Sets the descrizione of this sottoconti.
    *
    * @param descrizione the descrizione of this sottoconti
    */
    @Override
    public void setDescrizione(java.lang.String descrizione) {
        _sottoconti.setDescrizione(descrizione);
    }

    @Override
    public boolean isNew() {
        return _sottoconti.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _sottoconti.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _sottoconti.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _sottoconti.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _sottoconti.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _sottoconti.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _sottoconti.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _sottoconti.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _sottoconti.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _sottoconti.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _sottoconti.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new SottocontiWrapper((Sottoconti) _sottoconti.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.Sottoconti sottoconti) {
        return _sottoconti.compareTo(sottoconti);
    }

    @Override
    public int hashCode() {
        return _sottoconti.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.Sottoconti> toCacheModel() {
        return _sottoconti.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.Sottoconti toEscapedModel() {
        return new SottocontiWrapper(_sottoconti.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.Sottoconti toUnescapedModel() {
        return new SottocontiWrapper(_sottoconti.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _sottoconti.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _sottoconti.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _sottoconti.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof SottocontiWrapper)) {
            return false;
        }

        SottocontiWrapper sottocontiWrapper = (SottocontiWrapper) obj;

        if (Validator.equals(_sottoconti, sottocontiWrapper._sottoconti)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Sottoconti getWrappedSottoconti() {
        return _sottoconti;
    }

    @Override
    public Sottoconti getWrappedModel() {
        return _sottoconti;
    }

    @Override
    public void resetOriginalValues() {
        _sottoconti.resetOriginalValues();
    }
}
