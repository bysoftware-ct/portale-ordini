package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ArticoliLocalServiceUtil;
import it.bysoftware.ct.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class ArticoliClp extends BaseModelImpl<Articoli> implements Articoli {
    private String _codiceArticolo;
    private String _categoriaMerceologica;
    private String _categoriaInventario;
    private String _descrizione;
    private String _descrizioneDocumento;
    private String _descrizioneFiscale;
    private String _unitaMisura;
    private double _tara;
    private String _codiceIVA;
    private double _prezzo1;
    private double _prezzo2;
    private double _prezzo3;
    private long _libLng1;
    private String _libStr1;
    private boolean _obsoleto;
    private String _tipoArticolo;
    private String _codiceProvvigione;
    private double _pesoLordo;
    private BaseModel<?> _articoliRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public ArticoliClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Articoli.class;
    }

    @Override
    public String getModelClassName() {
        return Articoli.class.getName();
    }

    @Override
    public String getPrimaryKey() {
        return _codiceArticolo;
    }

    @Override
    public void setPrimaryKey(String primaryKey) {
        setCodiceArticolo(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _codiceArticolo;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((String) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceArticolo", getCodiceArticolo());
        attributes.put("categoriaMerceologica", getCategoriaMerceologica());
        attributes.put("categoriaInventario", getCategoriaInventario());
        attributes.put("descrizione", getDescrizione());
        attributes.put("descrizioneDocumento", getDescrizioneDocumento());
        attributes.put("descrizioneFiscale", getDescrizioneFiscale());
        attributes.put("unitaMisura", getUnitaMisura());
        attributes.put("tara", getTara());
        attributes.put("codiceIVA", getCodiceIVA());
        attributes.put("prezzo1", getPrezzo1());
        attributes.put("prezzo2", getPrezzo2());
        attributes.put("prezzo3", getPrezzo3());
        attributes.put("libLng1", getLibLng1());
        attributes.put("libStr1", getLibStr1());
        attributes.put("obsoleto", getObsoleto());
        attributes.put("tipoArticolo", getTipoArticolo());
        attributes.put("codiceProvvigione", getCodiceProvvigione());
        attributes.put("pesoLordo", getPesoLordo());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceArticolo = (String) attributes.get("codiceArticolo");

        if (codiceArticolo != null) {
            setCodiceArticolo(codiceArticolo);
        }

        String categoriaMerceologica = (String) attributes.get(
                "categoriaMerceologica");

        if (categoriaMerceologica != null) {
            setCategoriaMerceologica(categoriaMerceologica);
        }

        String categoriaInventario = (String) attributes.get(
                "categoriaInventario");

        if (categoriaInventario != null) {
            setCategoriaInventario(categoriaInventario);
        }

        String descrizione = (String) attributes.get("descrizione");

        if (descrizione != null) {
            setDescrizione(descrizione);
        }

        String descrizioneDocumento = (String) attributes.get(
                "descrizioneDocumento");

        if (descrizioneDocumento != null) {
            setDescrizioneDocumento(descrizioneDocumento);
        }

        String descrizioneFiscale = (String) attributes.get(
                "descrizioneFiscale");

        if (descrizioneFiscale != null) {
            setDescrizioneFiscale(descrizioneFiscale);
        }

        String unitaMisura = (String) attributes.get("unitaMisura");

        if (unitaMisura != null) {
            setUnitaMisura(unitaMisura);
        }

        Double tara = (Double) attributes.get("tara");

        if (tara != null) {
            setTara(tara);
        }

        String codiceIVA = (String) attributes.get("codiceIVA");

        if (codiceIVA != null) {
            setCodiceIVA(codiceIVA);
        }

        Double prezzo1 = (Double) attributes.get("prezzo1");

        if (prezzo1 != null) {
            setPrezzo1(prezzo1);
        }

        Double prezzo2 = (Double) attributes.get("prezzo2");

        if (prezzo2 != null) {
            setPrezzo2(prezzo2);
        }

        Double prezzo3 = (Double) attributes.get("prezzo3");

        if (prezzo3 != null) {
            setPrezzo3(prezzo3);
        }

        Long libLng1 = (Long) attributes.get("libLng1");

        if (libLng1 != null) {
            setLibLng1(libLng1);
        }

        String libStr1 = (String) attributes.get("libStr1");

        if (libStr1 != null) {
            setLibStr1(libStr1);
        }

        Boolean obsoleto = (Boolean) attributes.get("obsoleto");

        if (obsoleto != null) {
            setObsoleto(obsoleto);
        }

        String tipoArticolo = (String) attributes.get("tipoArticolo");

        if (tipoArticolo != null) {
            setTipoArticolo(tipoArticolo);
        }

        String codiceProvvigione = (String) attributes.get("codiceProvvigione");

        if (codiceProvvigione != null) {
            setCodiceProvvigione(codiceProvvigione);
        }

        Double pesoLordo = (Double) attributes.get("pesoLordo");

        if (pesoLordo != null) {
            setPesoLordo(pesoLordo);
        }
    }

    @Override
    public String getCodiceArticolo() {
        return _codiceArticolo;
    }

    @Override
    public void setCodiceArticolo(String codiceArticolo) {
        _codiceArticolo = codiceArticolo;

        if (_articoliRemoteModel != null) {
            try {
                Class<?> clazz = _articoliRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceArticolo",
                        String.class);

                method.invoke(_articoliRemoteModel, codiceArticolo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCategoriaMerceologica() {
        return _categoriaMerceologica;
    }

    @Override
    public void setCategoriaMerceologica(String categoriaMerceologica) {
        _categoriaMerceologica = categoriaMerceologica;

        if (_articoliRemoteModel != null) {
            try {
                Class<?> clazz = _articoliRemoteModel.getClass();

                Method method = clazz.getMethod("setCategoriaMerceologica",
                        String.class);

                method.invoke(_articoliRemoteModel, categoriaMerceologica);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCategoriaInventario() {
        return _categoriaInventario;
    }

    @Override
    public void setCategoriaInventario(String categoriaInventario) {
        _categoriaInventario = categoriaInventario;

        if (_articoliRemoteModel != null) {
            try {
                Class<?> clazz = _articoliRemoteModel.getClass();

                Method method = clazz.getMethod("setCategoriaInventario",
                        String.class);

                method.invoke(_articoliRemoteModel, categoriaInventario);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getDescrizione() {
        return _descrizione;
    }

    @Override
    public void setDescrizione(String descrizione) {
        _descrizione = descrizione;

        if (_articoliRemoteModel != null) {
            try {
                Class<?> clazz = _articoliRemoteModel.getClass();

                Method method = clazz.getMethod("setDescrizione", String.class);

                method.invoke(_articoliRemoteModel, descrizione);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getDescrizioneDocumento() {
        return _descrizioneDocumento;
    }

    @Override
    public void setDescrizioneDocumento(String descrizioneDocumento) {
        _descrizioneDocumento = descrizioneDocumento;

        if (_articoliRemoteModel != null) {
            try {
                Class<?> clazz = _articoliRemoteModel.getClass();

                Method method = clazz.getMethod("setDescrizioneDocumento",
                        String.class);

                method.invoke(_articoliRemoteModel, descrizioneDocumento);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getDescrizioneFiscale() {
        return _descrizioneFiscale;
    }

    @Override
    public void setDescrizioneFiscale(String descrizioneFiscale) {
        _descrizioneFiscale = descrizioneFiscale;

        if (_articoliRemoteModel != null) {
            try {
                Class<?> clazz = _articoliRemoteModel.getClass();

                Method method = clazz.getMethod("setDescrizioneFiscale",
                        String.class);

                method.invoke(_articoliRemoteModel, descrizioneFiscale);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getUnitaMisura() {
        return _unitaMisura;
    }

    @Override
    public void setUnitaMisura(String unitaMisura) {
        _unitaMisura = unitaMisura;

        if (_articoliRemoteModel != null) {
            try {
                Class<?> clazz = _articoliRemoteModel.getClass();

                Method method = clazz.getMethod("setUnitaMisura", String.class);

                method.invoke(_articoliRemoteModel, unitaMisura);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getTara() {
        return _tara;
    }

    @Override
    public void setTara(double tara) {
        _tara = tara;

        if (_articoliRemoteModel != null) {
            try {
                Class<?> clazz = _articoliRemoteModel.getClass();

                Method method = clazz.getMethod("setTara", double.class);

                method.invoke(_articoliRemoteModel, tara);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceIVA() {
        return _codiceIVA;
    }

    @Override
    public void setCodiceIVA(String codiceIVA) {
        _codiceIVA = codiceIVA;

        if (_articoliRemoteModel != null) {
            try {
                Class<?> clazz = _articoliRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceIVA", String.class);

                method.invoke(_articoliRemoteModel, codiceIVA);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrezzo1() {
        return _prezzo1;
    }

    @Override
    public void setPrezzo1(double prezzo1) {
        _prezzo1 = prezzo1;

        if (_articoliRemoteModel != null) {
            try {
                Class<?> clazz = _articoliRemoteModel.getClass();

                Method method = clazz.getMethod("setPrezzo1", double.class);

                method.invoke(_articoliRemoteModel, prezzo1);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrezzo2() {
        return _prezzo2;
    }

    @Override
    public void setPrezzo2(double prezzo2) {
        _prezzo2 = prezzo2;

        if (_articoliRemoteModel != null) {
            try {
                Class<?> clazz = _articoliRemoteModel.getClass();

                Method method = clazz.getMethod("setPrezzo2", double.class);

                method.invoke(_articoliRemoteModel, prezzo2);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrezzo3() {
        return _prezzo3;
    }

    @Override
    public void setPrezzo3(double prezzo3) {
        _prezzo3 = prezzo3;

        if (_articoliRemoteModel != null) {
            try {
                Class<?> clazz = _articoliRemoteModel.getClass();

                Method method = clazz.getMethod("setPrezzo3", double.class);

                method.invoke(_articoliRemoteModel, prezzo3);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getLibLng1() {
        return _libLng1;
    }

    @Override
    public void setLibLng1(long libLng1) {
        _libLng1 = libLng1;

        if (_articoliRemoteModel != null) {
            try {
                Class<?> clazz = _articoliRemoteModel.getClass();

                Method method = clazz.getMethod("setLibLng1", long.class);

                method.invoke(_articoliRemoteModel, libLng1);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getLibStr1() {
        return _libStr1;
    }

    @Override
    public void setLibStr1(String libStr1) {
        _libStr1 = libStr1;

        if (_articoliRemoteModel != null) {
            try {
                Class<?> clazz = _articoliRemoteModel.getClass();

                Method method = clazz.getMethod("setLibStr1", String.class);

                method.invoke(_articoliRemoteModel, libStr1);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getObsoleto() {
        return _obsoleto;
    }

    @Override
    public boolean isObsoleto() {
        return _obsoleto;
    }

    @Override
    public void setObsoleto(boolean obsoleto) {
        _obsoleto = obsoleto;

        if (_articoliRemoteModel != null) {
            try {
                Class<?> clazz = _articoliRemoteModel.getClass();

                Method method = clazz.getMethod("setObsoleto", boolean.class);

                method.invoke(_articoliRemoteModel, obsoleto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getTipoArticolo() {
        return _tipoArticolo;
    }

    @Override
    public void setTipoArticolo(String tipoArticolo) {
        _tipoArticolo = tipoArticolo;

        if (_articoliRemoteModel != null) {
            try {
                Class<?> clazz = _articoliRemoteModel.getClass();

                Method method = clazz.getMethod("setTipoArticolo", String.class);

                method.invoke(_articoliRemoteModel, tipoArticolo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceProvvigione() {
        return _codiceProvvigione;
    }

    @Override
    public void setCodiceProvvigione(String codiceProvvigione) {
        _codiceProvvigione = codiceProvvigione;

        if (_articoliRemoteModel != null) {
            try {
                Class<?> clazz = _articoliRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceProvvigione",
                        String.class);

                method.invoke(_articoliRemoteModel, codiceProvvigione);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPesoLordo() {
        return _pesoLordo;
    }

    @Override
    public void setPesoLordo(double pesoLordo) {
        _pesoLordo = pesoLordo;

        if (_articoliRemoteModel != null) {
            try {
                Class<?> clazz = _articoliRemoteModel.getClass();

                Method method = clazz.getMethod("setPesoLordo", double.class);

                method.invoke(_articoliRemoteModel, pesoLordo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public it.bysoftware.ct.model.Piante getPianta(java.lang.String code) {
        try {
            String methodName = "getPianta";

            Class<?>[] parameterTypes = new Class<?>[] { java.lang.String.class };

            Object[] parameterValues = new Object[] { code };

            it.bysoftware.ct.model.Piante returnObj = (it.bysoftware.ct.model.Piante) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public BaseModel<?> getArticoliRemoteModel() {
        return _articoliRemoteModel;
    }

    public void setArticoliRemoteModel(BaseModel<?> articoliRemoteModel) {
        _articoliRemoteModel = articoliRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _articoliRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_articoliRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            ArticoliLocalServiceUtil.addArticoli(this);
        } else {
            ArticoliLocalServiceUtil.updateArticoli(this);
        }
    }

    @Override
    public Articoli toEscapedModel() {
        return (Articoli) ProxyUtil.newProxyInstance(Articoli.class.getClassLoader(),
            new Class[] { Articoli.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        ArticoliClp clone = new ArticoliClp();

        clone.setCodiceArticolo(getCodiceArticolo());
        clone.setCategoriaMerceologica(getCategoriaMerceologica());
        clone.setCategoriaInventario(getCategoriaInventario());
        clone.setDescrizione(getDescrizione());
        clone.setDescrizioneDocumento(getDescrizioneDocumento());
        clone.setDescrizioneFiscale(getDescrizioneFiscale());
        clone.setUnitaMisura(getUnitaMisura());
        clone.setTara(getTara());
        clone.setCodiceIVA(getCodiceIVA());
        clone.setPrezzo1(getPrezzo1());
        clone.setPrezzo2(getPrezzo2());
        clone.setPrezzo3(getPrezzo3());
        clone.setLibLng1(getLibLng1());
        clone.setLibStr1(getLibStr1());
        clone.setObsoleto(getObsoleto());
        clone.setTipoArticolo(getTipoArticolo());
        clone.setCodiceProvvigione(getCodiceProvvigione());
        clone.setPesoLordo(getPesoLordo());

        return clone;
    }

    @Override
    public int compareTo(Articoli articoli) {
        String primaryKey = articoli.getPrimaryKey();

        return getPrimaryKey().compareTo(primaryKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ArticoliClp)) {
            return false;
        }

        ArticoliClp articoli = (ArticoliClp) obj;

        String primaryKey = articoli.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(37);

        sb.append("{codiceArticolo=");
        sb.append(getCodiceArticolo());
        sb.append(", categoriaMerceologica=");
        sb.append(getCategoriaMerceologica());
        sb.append(", categoriaInventario=");
        sb.append(getCategoriaInventario());
        sb.append(", descrizione=");
        sb.append(getDescrizione());
        sb.append(", descrizioneDocumento=");
        sb.append(getDescrizioneDocumento());
        sb.append(", descrizioneFiscale=");
        sb.append(getDescrizioneFiscale());
        sb.append(", unitaMisura=");
        sb.append(getUnitaMisura());
        sb.append(", tara=");
        sb.append(getTara());
        sb.append(", codiceIVA=");
        sb.append(getCodiceIVA());
        sb.append(", prezzo1=");
        sb.append(getPrezzo1());
        sb.append(", prezzo2=");
        sb.append(getPrezzo2());
        sb.append(", prezzo3=");
        sb.append(getPrezzo3());
        sb.append(", libLng1=");
        sb.append(getLibLng1());
        sb.append(", libStr1=");
        sb.append(getLibStr1());
        sb.append(", obsoleto=");
        sb.append(getObsoleto());
        sb.append(", tipoArticolo=");
        sb.append(getTipoArticolo());
        sb.append(", codiceProvvigione=");
        sb.append(getCodiceProvvigione());
        sb.append(", pesoLordo=");
        sb.append(getPesoLordo());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(58);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.Articoli");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>codiceArticolo</column-name><column-value><![CDATA[");
        sb.append(getCodiceArticolo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>categoriaMerceologica</column-name><column-value><![CDATA[");
        sb.append(getCategoriaMerceologica());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>categoriaInventario</column-name><column-value><![CDATA[");
        sb.append(getCategoriaInventario());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>descrizione</column-name><column-value><![CDATA[");
        sb.append(getDescrizione());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>descrizioneDocumento</column-name><column-value><![CDATA[");
        sb.append(getDescrizioneDocumento());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>descrizioneFiscale</column-name><column-value><![CDATA[");
        sb.append(getDescrizioneFiscale());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>unitaMisura</column-name><column-value><![CDATA[");
        sb.append(getUnitaMisura());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tara</column-name><column-value><![CDATA[");
        sb.append(getTara());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceIVA</column-name><column-value><![CDATA[");
        sb.append(getCodiceIVA());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>prezzo1</column-name><column-value><![CDATA[");
        sb.append(getPrezzo1());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>prezzo2</column-name><column-value><![CDATA[");
        sb.append(getPrezzo2());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>prezzo3</column-name><column-value><![CDATA[");
        sb.append(getPrezzo3());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libLng1</column-name><column-value><![CDATA[");
        sb.append(getLibLng1());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libStr1</column-name><column-value><![CDATA[");
        sb.append(getLibStr1());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>obsoleto</column-name><column-value><![CDATA[");
        sb.append(getObsoleto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tipoArticolo</column-name><column-value><![CDATA[");
        sb.append(getTipoArticolo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceProvvigione</column-name><column-value><![CDATA[");
        sb.append(getCodiceProvvigione());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>pesoLordo</column-name><column-value><![CDATA[");
        sb.append(getPesoLordo());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
