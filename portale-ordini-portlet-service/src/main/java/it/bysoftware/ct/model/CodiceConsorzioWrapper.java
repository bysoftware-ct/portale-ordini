package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CodiceConsorzio}.
 * </p>
 *
 * @author Mario Torrisi
 * @see CodiceConsorzio
 * @generated
 */
public class CodiceConsorzioWrapper implements CodiceConsorzio,
    ModelWrapper<CodiceConsorzio> {
    private CodiceConsorzio _codiceConsorzio;

    public CodiceConsorzioWrapper(CodiceConsorzio codiceConsorzio) {
        _codiceConsorzio = codiceConsorzio;
    }

    @Override
    public Class<?> getModelClass() {
        return CodiceConsorzio.class;
    }

    @Override
    public String getModelClassName() {
        return CodiceConsorzio.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceSocio", getCodiceSocio());
        attributes.put("idLiferay", getIdLiferay());
        attributes.put("codiceConsorzio", getCodiceConsorzio());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceSocio = (String) attributes.get("codiceSocio");

        if (codiceSocio != null) {
            setCodiceSocio(codiceSocio);
        }

        Long idLiferay = (Long) attributes.get("idLiferay");

        if (idLiferay != null) {
            setIdLiferay(idLiferay);
        }

        String codiceConsorzio = (String) attributes.get("codiceConsorzio");

        if (codiceConsorzio != null) {
            setCodiceConsorzio(codiceConsorzio);
        }
    }

    /**
    * Returns the primary key of this codice consorzio.
    *
    * @return the primary key of this codice consorzio
    */
    @Override
    public java.lang.String getPrimaryKey() {
        return _codiceConsorzio.getPrimaryKey();
    }

    /**
    * Sets the primary key of this codice consorzio.
    *
    * @param primaryKey the primary key of this codice consorzio
    */
    @Override
    public void setPrimaryKey(java.lang.String primaryKey) {
        _codiceConsorzio.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the codice socio of this codice consorzio.
    *
    * @return the codice socio of this codice consorzio
    */
    @Override
    public java.lang.String getCodiceSocio() {
        return _codiceConsorzio.getCodiceSocio();
    }

    /**
    * Sets the codice socio of this codice consorzio.
    *
    * @param codiceSocio the codice socio of this codice consorzio
    */
    @Override
    public void setCodiceSocio(java.lang.String codiceSocio) {
        _codiceConsorzio.setCodiceSocio(codiceSocio);
    }

    /**
    * Returns the id liferay of this codice consorzio.
    *
    * @return the id liferay of this codice consorzio
    */
    @Override
    public long getIdLiferay() {
        return _codiceConsorzio.getIdLiferay();
    }

    /**
    * Sets the id liferay of this codice consorzio.
    *
    * @param idLiferay the id liferay of this codice consorzio
    */
    @Override
    public void setIdLiferay(long idLiferay) {
        _codiceConsorzio.setIdLiferay(idLiferay);
    }

    /**
    * Returns the codice consorzio of this codice consorzio.
    *
    * @return the codice consorzio of this codice consorzio
    */
    @Override
    public java.lang.String getCodiceConsorzio() {
        return _codiceConsorzio.getCodiceConsorzio();
    }

    /**
    * Sets the codice consorzio of this codice consorzio.
    *
    * @param codiceConsorzio the codice consorzio of this codice consorzio
    */
    @Override
    public void setCodiceConsorzio(java.lang.String codiceConsorzio) {
        _codiceConsorzio.setCodiceConsorzio(codiceConsorzio);
    }

    @Override
    public boolean isNew() {
        return _codiceConsorzio.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _codiceConsorzio.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _codiceConsorzio.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _codiceConsorzio.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _codiceConsorzio.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _codiceConsorzio.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _codiceConsorzio.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _codiceConsorzio.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _codiceConsorzio.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _codiceConsorzio.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _codiceConsorzio.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new CodiceConsorzioWrapper((CodiceConsorzio) _codiceConsorzio.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.CodiceConsorzio codiceConsorzio) {
        return _codiceConsorzio.compareTo(codiceConsorzio);
    }

    @Override
    public int hashCode() {
        return _codiceConsorzio.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.CodiceConsorzio> toCacheModel() {
        return _codiceConsorzio.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.CodiceConsorzio toEscapedModel() {
        return new CodiceConsorzioWrapper(_codiceConsorzio.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.CodiceConsorzio toUnescapedModel() {
        return new CodiceConsorzioWrapper(_codiceConsorzio.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _codiceConsorzio.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _codiceConsorzio.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _codiceConsorzio.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CodiceConsorzioWrapper)) {
            return false;
        }

        CodiceConsorzioWrapper codiceConsorzioWrapper = (CodiceConsorzioWrapper) obj;

        if (Validator.equals(_codiceConsorzio,
                    codiceConsorzioWrapper._codiceConsorzio)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public CodiceConsorzio getWrappedCodiceConsorzio() {
        return _codiceConsorzio;
    }

    @Override
    public CodiceConsorzio getWrappedModel() {
        return _codiceConsorzio;
    }

    @Override
    public void resetOriginalValues() {
        _codiceConsorzio.resetOriginalValues();
    }
}
