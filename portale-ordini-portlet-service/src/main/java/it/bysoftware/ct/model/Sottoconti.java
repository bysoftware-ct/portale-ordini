package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Sottoconti service. Represents a row in the &quot;Sottoconti&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see SottocontiModel
 * @see it.bysoftware.ct.model.impl.SottocontiImpl
 * @see it.bysoftware.ct.model.impl.SottocontiModelImpl
 * @generated
 */
public interface Sottoconti extends SottocontiModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.SottocontiImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
