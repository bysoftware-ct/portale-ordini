package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.VariantiLocalServiceUtil;
import it.bysoftware.ct.service.persistence.VariantiPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class VariantiClp extends BaseModelImpl<Varianti> implements Varianti {
    private String _codiceVariante;
    private String _tipoVariante;
    private String _descrizione;
    private BaseModel<?> _variantiRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public VariantiClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Varianti.class;
    }

    @Override
    public String getModelClassName() {
        return Varianti.class.getName();
    }

    @Override
    public VariantiPK getPrimaryKey() {
        return new VariantiPK(_codiceVariante, _tipoVariante);
    }

    @Override
    public void setPrimaryKey(VariantiPK primaryKey) {
        setCodiceVariante(primaryKey.codiceVariante);
        setTipoVariante(primaryKey.tipoVariante);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return new VariantiPK(_codiceVariante, _tipoVariante);
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((VariantiPK) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceVariante", getCodiceVariante());
        attributes.put("tipoVariante", getTipoVariante());
        attributes.put("descrizione", getDescrizione());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceVariante = (String) attributes.get("codiceVariante");

        if (codiceVariante != null) {
            setCodiceVariante(codiceVariante);
        }

        String tipoVariante = (String) attributes.get("tipoVariante");

        if (tipoVariante != null) {
            setTipoVariante(tipoVariante);
        }

        String descrizione = (String) attributes.get("descrizione");

        if (descrizione != null) {
            setDescrizione(descrizione);
        }
    }

    @Override
    public String getCodiceVariante() {
        return _codiceVariante;
    }

    @Override
    public void setCodiceVariante(String codiceVariante) {
        _codiceVariante = codiceVariante;

        if (_variantiRemoteModel != null) {
            try {
                Class<?> clazz = _variantiRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceVariante",
                        String.class);

                method.invoke(_variantiRemoteModel, codiceVariante);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getTipoVariante() {
        return _tipoVariante;
    }

    @Override
    public void setTipoVariante(String tipoVariante) {
        _tipoVariante = tipoVariante;

        if (_variantiRemoteModel != null) {
            try {
                Class<?> clazz = _variantiRemoteModel.getClass();

                Method method = clazz.getMethod("setTipoVariante", String.class);

                method.invoke(_variantiRemoteModel, tipoVariante);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getDescrizione() {
        return _descrizione;
    }

    @Override
    public void setDescrizione(String descrizione) {
        _descrizione = descrizione;

        if (_variantiRemoteModel != null) {
            try {
                Class<?> clazz = _variantiRemoteModel.getClass();

                Method method = clazz.getMethod("setDescrizione", String.class);

                method.invoke(_variantiRemoteModel, descrizione);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getVariantiRemoteModel() {
        return _variantiRemoteModel;
    }

    public void setVariantiRemoteModel(BaseModel<?> variantiRemoteModel) {
        _variantiRemoteModel = variantiRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _variantiRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_variantiRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            VariantiLocalServiceUtil.addVarianti(this);
        } else {
            VariantiLocalServiceUtil.updateVarianti(this);
        }
    }

    @Override
    public Varianti toEscapedModel() {
        return (Varianti) ProxyUtil.newProxyInstance(Varianti.class.getClassLoader(),
            new Class[] { Varianti.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        VariantiClp clone = new VariantiClp();

        clone.setCodiceVariante(getCodiceVariante());
        clone.setTipoVariante(getTipoVariante());
        clone.setDescrizione(getDescrizione());

        return clone;
    }

    @Override
    public int compareTo(Varianti varianti) {
        int value = 0;

        value = getDescrizione().compareTo(varianti.getDescrizione());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VariantiClp)) {
            return false;
        }

        VariantiClp varianti = (VariantiClp) obj;

        VariantiPK primaryKey = varianti.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{codiceVariante=");
        sb.append(getCodiceVariante());
        sb.append(", tipoVariante=");
        sb.append(getTipoVariante());
        sb.append(", descrizione=");
        sb.append(getDescrizione());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(13);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.Varianti");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>codiceVariante</column-name><column-value><![CDATA[");
        sb.append(getCodiceVariante());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tipoVariante</column-name><column-value><![CDATA[");
        sb.append(getTipoVariante());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>descrizione</column-name><column-value><![CDATA[");
        sb.append(getDescrizione());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
