package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.CategorieServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.CategorieServiceSoap
 * @generated
 */
public class CategorieSoap implements Serializable {
    private long _id;
    private String _nomeIta;
    private String _nomeEng;
    private String _nomeTed;
    private String _nomeFra;
    private String _nomeSpa;
    private boolean _predefinita;

    public CategorieSoap() {
    }

    public static CategorieSoap toSoapModel(Categorie model) {
        CategorieSoap soapModel = new CategorieSoap();

        soapModel.setId(model.getId());
        soapModel.setNomeIta(model.getNomeIta());
        soapModel.setNomeEng(model.getNomeEng());
        soapModel.setNomeTed(model.getNomeTed());
        soapModel.setNomeFra(model.getNomeFra());
        soapModel.setNomeSpa(model.getNomeSpa());
        soapModel.setPredefinita(model.getPredefinita());

        return soapModel;
    }

    public static CategorieSoap[] toSoapModels(Categorie[] models) {
        CategorieSoap[] soapModels = new CategorieSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static CategorieSoap[][] toSoapModels(Categorie[][] models) {
        CategorieSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new CategorieSoap[models.length][models[0].length];
        } else {
            soapModels = new CategorieSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static CategorieSoap[] toSoapModels(List<Categorie> models) {
        List<CategorieSoap> soapModels = new ArrayList<CategorieSoap>(models.size());

        for (Categorie model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new CategorieSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public String getNomeIta() {
        return _nomeIta;
    }

    public void setNomeIta(String nomeIta) {
        _nomeIta = nomeIta;
    }

    public String getNomeEng() {
        return _nomeEng;
    }

    public void setNomeEng(String nomeEng) {
        _nomeEng = nomeEng;
    }

    public String getNomeTed() {
        return _nomeTed;
    }

    public void setNomeTed(String nomeTed) {
        _nomeTed = nomeTed;
    }

    public String getNomeFra() {
        return _nomeFra;
    }

    public void setNomeFra(String nomeFra) {
        _nomeFra = nomeFra;
    }

    public String getNomeSpa() {
        return _nomeSpa;
    }

    public void setNomeSpa(String nomeSpa) {
        _nomeSpa = nomeSpa;
    }

    public boolean getPredefinita() {
        return _predefinita;
    }

    public boolean isPredefinita() {
        return _predefinita;
    }

    public void setPredefinita(boolean predefinita) {
        _predefinita = predefinita;
    }
}
