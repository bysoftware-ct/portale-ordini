package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Categorie service. Represents a row in the &quot;_categorie&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see CategorieModel
 * @see it.bysoftware.ct.model.impl.CategorieImpl
 * @see it.bysoftware.ct.model.impl.CategorieModelImpl
 * @generated
 */
public interface Categorie extends CategorieModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.CategorieImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
