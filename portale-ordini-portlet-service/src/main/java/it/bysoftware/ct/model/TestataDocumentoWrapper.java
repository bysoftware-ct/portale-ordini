package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link TestataDocumento}.
 * </p>
 *
 * @author Mario Torrisi
 * @see TestataDocumento
 * @generated
 */
public class TestataDocumentoWrapper implements TestataDocumento,
    ModelWrapper<TestataDocumento> {
    private TestataDocumento _testataDocumento;

    public TestataDocumentoWrapper(TestataDocumento testataDocumento) {
        _testataDocumento = testataDocumento;
    }

    @Override
    public Class<?> getModelClass() {
        return TestataDocumento.class;
    }

    @Override
    public String getModelClassName() {
        return TestataDocumento.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("anno", getAnno());
        attributes.put("codiceAttivita", getCodiceAttivita());
        attributes.put("codiceCentro", getCodiceCentro());
        attributes.put("codiceDeposito", getCodiceDeposito());
        attributes.put("protocollo", getProtocollo());
        attributes.put("codiceFornitore", getCodiceFornitore());
        attributes.put("tipoDocumento", getTipoDocumento());
        attributes.put("dataRegistrazione", getDataRegistrazione());
        attributes.put("codiceSpedizione", getCodiceSpedizione());
        attributes.put("codiceCausaleTrasporto", getCodiceCausaleTrasporto());
        attributes.put("codiceCuraTrasporto", getCodiceCuraTrasporto());
        attributes.put("codicePorto", getCodicePorto());
        attributes.put("codiceAspettoEsteriore", getCodiceAspettoEsteriore());
        attributes.put("codiceVettore1", getCodiceVettore1());
        attributes.put("codiceVettore2", getCodiceVettore2());
        attributes.put("libStr1", getLibStr1());
        attributes.put("libStr2", getLibStr2());
        attributes.put("libStr3", getLibStr3());
        attributes.put("libDbl1", getLibDbl1());
        attributes.put("libDbl2", getLibDbl2());
        attributes.put("libDbl3", getLibDbl3());
        attributes.put("libLng1", getLibLng1());
        attributes.put("libLng2", getLibLng2());
        attributes.put("libLng3", getLibLng3());
        attributes.put("libDat1", getLibDat1());
        attributes.put("libDat2", getLibDat2());
        attributes.put("libDat3", getLibDat3());
        attributes.put("stato", getStato());
        attributes.put("inviato", getInviato());
        attributes.put("codiceConsorzio", getCodiceConsorzio());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Integer anno = (Integer) attributes.get("anno");

        if (anno != null) {
            setAnno(anno);
        }

        String codiceAttivita = (String) attributes.get("codiceAttivita");

        if (codiceAttivita != null) {
            setCodiceAttivita(codiceAttivita);
        }

        String codiceCentro = (String) attributes.get("codiceCentro");

        if (codiceCentro != null) {
            setCodiceCentro(codiceCentro);
        }

        String codiceDeposito = (String) attributes.get("codiceDeposito");

        if (codiceDeposito != null) {
            setCodiceDeposito(codiceDeposito);
        }

        Integer protocollo = (Integer) attributes.get("protocollo");

        if (protocollo != null) {
            setProtocollo(protocollo);
        }

        String codiceFornitore = (String) attributes.get("codiceFornitore");

        if (codiceFornitore != null) {
            setCodiceFornitore(codiceFornitore);
        }

        String tipoDocumento = (String) attributes.get("tipoDocumento");

        if (tipoDocumento != null) {
            setTipoDocumento(tipoDocumento);
        }

        Date dataRegistrazione = (Date) attributes.get("dataRegistrazione");

        if (dataRegistrazione != null) {
            setDataRegistrazione(dataRegistrazione);
        }

        String codiceSpedizione = (String) attributes.get("codiceSpedizione");

        if (codiceSpedizione != null) {
            setCodiceSpedizione(codiceSpedizione);
        }

        String codiceCausaleTrasporto = (String) attributes.get(
                "codiceCausaleTrasporto");

        if (codiceCausaleTrasporto != null) {
            setCodiceCausaleTrasporto(codiceCausaleTrasporto);
        }

        String codiceCuraTrasporto = (String) attributes.get(
                "codiceCuraTrasporto");

        if (codiceCuraTrasporto != null) {
            setCodiceCuraTrasporto(codiceCuraTrasporto);
        }

        String codicePorto = (String) attributes.get("codicePorto");

        if (codicePorto != null) {
            setCodicePorto(codicePorto);
        }

        String codiceAspettoEsteriore = (String) attributes.get(
                "codiceAspettoEsteriore");

        if (codiceAspettoEsteriore != null) {
            setCodiceAspettoEsteriore(codiceAspettoEsteriore);
        }

        String codiceVettore1 = (String) attributes.get("codiceVettore1");

        if (codiceVettore1 != null) {
            setCodiceVettore1(codiceVettore1);
        }

        String codiceVettore2 = (String) attributes.get("codiceVettore2");

        if (codiceVettore2 != null) {
            setCodiceVettore2(codiceVettore2);
        }

        String libStr1 = (String) attributes.get("libStr1");

        if (libStr1 != null) {
            setLibStr1(libStr1);
        }

        String libStr2 = (String) attributes.get("libStr2");

        if (libStr2 != null) {
            setLibStr2(libStr2);
        }

        String libStr3 = (String) attributes.get("libStr3");

        if (libStr3 != null) {
            setLibStr3(libStr3);
        }

        Double libDbl1 = (Double) attributes.get("libDbl1");

        if (libDbl1 != null) {
            setLibDbl1(libDbl1);
        }

        Double libDbl2 = (Double) attributes.get("libDbl2");

        if (libDbl2 != null) {
            setLibDbl2(libDbl2);
        }

        Double libDbl3 = (Double) attributes.get("libDbl3");

        if (libDbl3 != null) {
            setLibDbl3(libDbl3);
        }

        Long libLng1 = (Long) attributes.get("libLng1");

        if (libLng1 != null) {
            setLibLng1(libLng1);
        }

        Long libLng2 = (Long) attributes.get("libLng2");

        if (libLng2 != null) {
            setLibLng2(libLng2);
        }

        Long libLng3 = (Long) attributes.get("libLng3");

        if (libLng3 != null) {
            setLibLng3(libLng3);
        }

        Date libDat1 = (Date) attributes.get("libDat1");

        if (libDat1 != null) {
            setLibDat1(libDat1);
        }

        Date libDat2 = (Date) attributes.get("libDat2");

        if (libDat2 != null) {
            setLibDat2(libDat2);
        }

        Date libDat3 = (Date) attributes.get("libDat3");

        if (libDat3 != null) {
            setLibDat3(libDat3);
        }

        Boolean stato = (Boolean) attributes.get("stato");

        if (stato != null) {
            setStato(stato);
        }

        Boolean inviato = (Boolean) attributes.get("inviato");

        if (inviato != null) {
            setInviato(inviato);
        }

        String codiceConsorzio = (String) attributes.get("codiceConsorzio");

        if (codiceConsorzio != null) {
            setCodiceConsorzio(codiceConsorzio);
        }
    }

    /**
    * Returns the primary key of this testata documento.
    *
    * @return the primary key of this testata documento
    */
    @Override
    public it.bysoftware.ct.service.persistence.TestataDocumentoPK getPrimaryKey() {
        return _testataDocumento.getPrimaryKey();
    }

    /**
    * Sets the primary key of this testata documento.
    *
    * @param primaryKey the primary key of this testata documento
    */
    @Override
    public void setPrimaryKey(
        it.bysoftware.ct.service.persistence.TestataDocumentoPK primaryKey) {
        _testataDocumento.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the anno of this testata documento.
    *
    * @return the anno of this testata documento
    */
    @Override
    public int getAnno() {
        return _testataDocumento.getAnno();
    }

    /**
    * Sets the anno of this testata documento.
    *
    * @param anno the anno of this testata documento
    */
    @Override
    public void setAnno(int anno) {
        _testataDocumento.setAnno(anno);
    }

    /**
    * Returns the codice attivita of this testata documento.
    *
    * @return the codice attivita of this testata documento
    */
    @Override
    public java.lang.String getCodiceAttivita() {
        return _testataDocumento.getCodiceAttivita();
    }

    /**
    * Sets the codice attivita of this testata documento.
    *
    * @param codiceAttivita the codice attivita of this testata documento
    */
    @Override
    public void setCodiceAttivita(java.lang.String codiceAttivita) {
        _testataDocumento.setCodiceAttivita(codiceAttivita);
    }

    /**
    * Returns the codice centro of this testata documento.
    *
    * @return the codice centro of this testata documento
    */
    @Override
    public java.lang.String getCodiceCentro() {
        return _testataDocumento.getCodiceCentro();
    }

    /**
    * Sets the codice centro of this testata documento.
    *
    * @param codiceCentro the codice centro of this testata documento
    */
    @Override
    public void setCodiceCentro(java.lang.String codiceCentro) {
        _testataDocumento.setCodiceCentro(codiceCentro);
    }

    /**
    * Returns the codice deposito of this testata documento.
    *
    * @return the codice deposito of this testata documento
    */
    @Override
    public java.lang.String getCodiceDeposito() {
        return _testataDocumento.getCodiceDeposito();
    }

    /**
    * Sets the codice deposito of this testata documento.
    *
    * @param codiceDeposito the codice deposito of this testata documento
    */
    @Override
    public void setCodiceDeposito(java.lang.String codiceDeposito) {
        _testataDocumento.setCodiceDeposito(codiceDeposito);
    }

    /**
    * Returns the protocollo of this testata documento.
    *
    * @return the protocollo of this testata documento
    */
    @Override
    public int getProtocollo() {
        return _testataDocumento.getProtocollo();
    }

    /**
    * Sets the protocollo of this testata documento.
    *
    * @param protocollo the protocollo of this testata documento
    */
    @Override
    public void setProtocollo(int protocollo) {
        _testataDocumento.setProtocollo(protocollo);
    }

    /**
    * Returns the codice fornitore of this testata documento.
    *
    * @return the codice fornitore of this testata documento
    */
    @Override
    public java.lang.String getCodiceFornitore() {
        return _testataDocumento.getCodiceFornitore();
    }

    /**
    * Sets the codice fornitore of this testata documento.
    *
    * @param codiceFornitore the codice fornitore of this testata documento
    */
    @Override
    public void setCodiceFornitore(java.lang.String codiceFornitore) {
        _testataDocumento.setCodiceFornitore(codiceFornitore);
    }

    /**
    * Returns the tipo documento of this testata documento.
    *
    * @return the tipo documento of this testata documento
    */
    @Override
    public java.lang.String getTipoDocumento() {
        return _testataDocumento.getTipoDocumento();
    }

    /**
    * Sets the tipo documento of this testata documento.
    *
    * @param tipoDocumento the tipo documento of this testata documento
    */
    @Override
    public void setTipoDocumento(java.lang.String tipoDocumento) {
        _testataDocumento.setTipoDocumento(tipoDocumento);
    }

    /**
    * Returns the data registrazione of this testata documento.
    *
    * @return the data registrazione of this testata documento
    */
    @Override
    public java.util.Date getDataRegistrazione() {
        return _testataDocumento.getDataRegistrazione();
    }

    /**
    * Sets the data registrazione of this testata documento.
    *
    * @param dataRegistrazione the data registrazione of this testata documento
    */
    @Override
    public void setDataRegistrazione(java.util.Date dataRegistrazione) {
        _testataDocumento.setDataRegistrazione(dataRegistrazione);
    }

    /**
    * Returns the codice spedizione of this testata documento.
    *
    * @return the codice spedizione of this testata documento
    */
    @Override
    public java.lang.String getCodiceSpedizione() {
        return _testataDocumento.getCodiceSpedizione();
    }

    /**
    * Sets the codice spedizione of this testata documento.
    *
    * @param codiceSpedizione the codice spedizione of this testata documento
    */
    @Override
    public void setCodiceSpedizione(java.lang.String codiceSpedizione) {
        _testataDocumento.setCodiceSpedizione(codiceSpedizione);
    }

    /**
    * Returns the codice causale trasporto of this testata documento.
    *
    * @return the codice causale trasporto of this testata documento
    */
    @Override
    public java.lang.String getCodiceCausaleTrasporto() {
        return _testataDocumento.getCodiceCausaleTrasporto();
    }

    /**
    * Sets the codice causale trasporto of this testata documento.
    *
    * @param codiceCausaleTrasporto the codice causale trasporto of this testata documento
    */
    @Override
    public void setCodiceCausaleTrasporto(
        java.lang.String codiceCausaleTrasporto) {
        _testataDocumento.setCodiceCausaleTrasporto(codiceCausaleTrasporto);
    }

    /**
    * Returns the codice cura trasporto of this testata documento.
    *
    * @return the codice cura trasporto of this testata documento
    */
    @Override
    public java.lang.String getCodiceCuraTrasporto() {
        return _testataDocumento.getCodiceCuraTrasporto();
    }

    /**
    * Sets the codice cura trasporto of this testata documento.
    *
    * @param codiceCuraTrasporto the codice cura trasporto of this testata documento
    */
    @Override
    public void setCodiceCuraTrasporto(java.lang.String codiceCuraTrasporto) {
        _testataDocumento.setCodiceCuraTrasporto(codiceCuraTrasporto);
    }

    /**
    * Returns the codice porto of this testata documento.
    *
    * @return the codice porto of this testata documento
    */
    @Override
    public java.lang.String getCodicePorto() {
        return _testataDocumento.getCodicePorto();
    }

    /**
    * Sets the codice porto of this testata documento.
    *
    * @param codicePorto the codice porto of this testata documento
    */
    @Override
    public void setCodicePorto(java.lang.String codicePorto) {
        _testataDocumento.setCodicePorto(codicePorto);
    }

    /**
    * Returns the codice aspetto esteriore of this testata documento.
    *
    * @return the codice aspetto esteriore of this testata documento
    */
    @Override
    public java.lang.String getCodiceAspettoEsteriore() {
        return _testataDocumento.getCodiceAspettoEsteriore();
    }

    /**
    * Sets the codice aspetto esteriore of this testata documento.
    *
    * @param codiceAspettoEsteriore the codice aspetto esteriore of this testata documento
    */
    @Override
    public void setCodiceAspettoEsteriore(
        java.lang.String codiceAspettoEsteriore) {
        _testataDocumento.setCodiceAspettoEsteriore(codiceAspettoEsteriore);
    }

    /**
    * Returns the codice vettore1 of this testata documento.
    *
    * @return the codice vettore1 of this testata documento
    */
    @Override
    public java.lang.String getCodiceVettore1() {
        return _testataDocumento.getCodiceVettore1();
    }

    /**
    * Sets the codice vettore1 of this testata documento.
    *
    * @param codiceVettore1 the codice vettore1 of this testata documento
    */
    @Override
    public void setCodiceVettore1(java.lang.String codiceVettore1) {
        _testataDocumento.setCodiceVettore1(codiceVettore1);
    }

    /**
    * Returns the codice vettore2 of this testata documento.
    *
    * @return the codice vettore2 of this testata documento
    */
    @Override
    public java.lang.String getCodiceVettore2() {
        return _testataDocumento.getCodiceVettore2();
    }

    /**
    * Sets the codice vettore2 of this testata documento.
    *
    * @param codiceVettore2 the codice vettore2 of this testata documento
    */
    @Override
    public void setCodiceVettore2(java.lang.String codiceVettore2) {
        _testataDocumento.setCodiceVettore2(codiceVettore2);
    }

    /**
    * Returns the lib str1 of this testata documento.
    *
    * @return the lib str1 of this testata documento
    */
    @Override
    public java.lang.String getLibStr1() {
        return _testataDocumento.getLibStr1();
    }

    /**
    * Sets the lib str1 of this testata documento.
    *
    * @param libStr1 the lib str1 of this testata documento
    */
    @Override
    public void setLibStr1(java.lang.String libStr1) {
        _testataDocumento.setLibStr1(libStr1);
    }

    /**
    * Returns the lib str2 of this testata documento.
    *
    * @return the lib str2 of this testata documento
    */
    @Override
    public java.lang.String getLibStr2() {
        return _testataDocumento.getLibStr2();
    }

    /**
    * Sets the lib str2 of this testata documento.
    *
    * @param libStr2 the lib str2 of this testata documento
    */
    @Override
    public void setLibStr2(java.lang.String libStr2) {
        _testataDocumento.setLibStr2(libStr2);
    }

    /**
    * Returns the lib str3 of this testata documento.
    *
    * @return the lib str3 of this testata documento
    */
    @Override
    public java.lang.String getLibStr3() {
        return _testataDocumento.getLibStr3();
    }

    /**
    * Sets the lib str3 of this testata documento.
    *
    * @param libStr3 the lib str3 of this testata documento
    */
    @Override
    public void setLibStr3(java.lang.String libStr3) {
        _testataDocumento.setLibStr3(libStr3);
    }

    /**
    * Returns the lib dbl1 of this testata documento.
    *
    * @return the lib dbl1 of this testata documento
    */
    @Override
    public double getLibDbl1() {
        return _testataDocumento.getLibDbl1();
    }

    /**
    * Sets the lib dbl1 of this testata documento.
    *
    * @param libDbl1 the lib dbl1 of this testata documento
    */
    @Override
    public void setLibDbl1(double libDbl1) {
        _testataDocumento.setLibDbl1(libDbl1);
    }

    /**
    * Returns the lib dbl2 of this testata documento.
    *
    * @return the lib dbl2 of this testata documento
    */
    @Override
    public double getLibDbl2() {
        return _testataDocumento.getLibDbl2();
    }

    /**
    * Sets the lib dbl2 of this testata documento.
    *
    * @param libDbl2 the lib dbl2 of this testata documento
    */
    @Override
    public void setLibDbl2(double libDbl2) {
        _testataDocumento.setLibDbl2(libDbl2);
    }

    /**
    * Returns the lib dbl3 of this testata documento.
    *
    * @return the lib dbl3 of this testata documento
    */
    @Override
    public double getLibDbl3() {
        return _testataDocumento.getLibDbl3();
    }

    /**
    * Sets the lib dbl3 of this testata documento.
    *
    * @param libDbl3 the lib dbl3 of this testata documento
    */
    @Override
    public void setLibDbl3(double libDbl3) {
        _testataDocumento.setLibDbl3(libDbl3);
    }

    /**
    * Returns the lib lng1 of this testata documento.
    *
    * @return the lib lng1 of this testata documento
    */
    @Override
    public long getLibLng1() {
        return _testataDocumento.getLibLng1();
    }

    /**
    * Sets the lib lng1 of this testata documento.
    *
    * @param libLng1 the lib lng1 of this testata documento
    */
    @Override
    public void setLibLng1(long libLng1) {
        _testataDocumento.setLibLng1(libLng1);
    }

    /**
    * Returns the lib lng2 of this testata documento.
    *
    * @return the lib lng2 of this testata documento
    */
    @Override
    public long getLibLng2() {
        return _testataDocumento.getLibLng2();
    }

    /**
    * Sets the lib lng2 of this testata documento.
    *
    * @param libLng2 the lib lng2 of this testata documento
    */
    @Override
    public void setLibLng2(long libLng2) {
        _testataDocumento.setLibLng2(libLng2);
    }

    /**
    * Returns the lib lng3 of this testata documento.
    *
    * @return the lib lng3 of this testata documento
    */
    @Override
    public long getLibLng3() {
        return _testataDocumento.getLibLng3();
    }

    /**
    * Sets the lib lng3 of this testata documento.
    *
    * @param libLng3 the lib lng3 of this testata documento
    */
    @Override
    public void setLibLng3(long libLng3) {
        _testataDocumento.setLibLng3(libLng3);
    }

    /**
    * Returns the lib dat1 of this testata documento.
    *
    * @return the lib dat1 of this testata documento
    */
    @Override
    public java.util.Date getLibDat1() {
        return _testataDocumento.getLibDat1();
    }

    /**
    * Sets the lib dat1 of this testata documento.
    *
    * @param libDat1 the lib dat1 of this testata documento
    */
    @Override
    public void setLibDat1(java.util.Date libDat1) {
        _testataDocumento.setLibDat1(libDat1);
    }

    /**
    * Returns the lib dat2 of this testata documento.
    *
    * @return the lib dat2 of this testata documento
    */
    @Override
    public java.util.Date getLibDat2() {
        return _testataDocumento.getLibDat2();
    }

    /**
    * Sets the lib dat2 of this testata documento.
    *
    * @param libDat2 the lib dat2 of this testata documento
    */
    @Override
    public void setLibDat2(java.util.Date libDat2) {
        _testataDocumento.setLibDat2(libDat2);
    }

    /**
    * Returns the lib dat3 of this testata documento.
    *
    * @return the lib dat3 of this testata documento
    */
    @Override
    public java.util.Date getLibDat3() {
        return _testataDocumento.getLibDat3();
    }

    /**
    * Sets the lib dat3 of this testata documento.
    *
    * @param libDat3 the lib dat3 of this testata documento
    */
    @Override
    public void setLibDat3(java.util.Date libDat3) {
        _testataDocumento.setLibDat3(libDat3);
    }

    /**
    * Returns the stato of this testata documento.
    *
    * @return the stato of this testata documento
    */
    @Override
    public boolean getStato() {
        return _testataDocumento.getStato();
    }

    /**
    * Returns <code>true</code> if this testata documento is stato.
    *
    * @return <code>true</code> if this testata documento is stato; <code>false</code> otherwise
    */
    @Override
    public boolean isStato() {
        return _testataDocumento.isStato();
    }

    /**
    * Sets whether this testata documento is stato.
    *
    * @param stato the stato of this testata documento
    */
    @Override
    public void setStato(boolean stato) {
        _testataDocumento.setStato(stato);
    }

    /**
    * Returns the inviato of this testata documento.
    *
    * @return the inviato of this testata documento
    */
    @Override
    public boolean getInviato() {
        return _testataDocumento.getInviato();
    }

    /**
    * Returns <code>true</code> if this testata documento is inviato.
    *
    * @return <code>true</code> if this testata documento is inviato; <code>false</code> otherwise
    */
    @Override
    public boolean isInviato() {
        return _testataDocumento.isInviato();
    }

    /**
    * Sets whether this testata documento is inviato.
    *
    * @param inviato the inviato of this testata documento
    */
    @Override
    public void setInviato(boolean inviato) {
        _testataDocumento.setInviato(inviato);
    }

    /**
    * Returns the codice consorzio of this testata documento.
    *
    * @return the codice consorzio of this testata documento
    */
    @Override
    public java.lang.String getCodiceConsorzio() {
        return _testataDocumento.getCodiceConsorzio();
    }

    /**
    * Sets the codice consorzio of this testata documento.
    *
    * @param codiceConsorzio the codice consorzio of this testata documento
    */
    @Override
    public void setCodiceConsorzio(java.lang.String codiceConsorzio) {
        _testataDocumento.setCodiceConsorzio(codiceConsorzio);
    }

    @Override
    public boolean isNew() {
        return _testataDocumento.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _testataDocumento.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _testataDocumento.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _testataDocumento.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _testataDocumento.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _testataDocumento.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _testataDocumento.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _testataDocumento.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _testataDocumento.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _testataDocumento.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _testataDocumento.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new TestataDocumentoWrapper((TestataDocumento) _testataDocumento.clone());
    }

    @Override
    public int compareTo(
        it.bysoftware.ct.model.TestataDocumento testataDocumento) {
        return _testataDocumento.compareTo(testataDocumento);
    }

    @Override
    public int hashCode() {
        return _testataDocumento.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.TestataDocumento> toCacheModel() {
        return _testataDocumento.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.TestataDocumento toEscapedModel() {
        return new TestataDocumentoWrapper(_testataDocumento.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.TestataDocumento toUnescapedModel() {
        return new TestataDocumentoWrapper(_testataDocumento.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _testataDocumento.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _testataDocumento.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _testataDocumento.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof TestataDocumentoWrapper)) {
            return false;
        }

        TestataDocumentoWrapper testataDocumentoWrapper = (TestataDocumentoWrapper) obj;

        if (Validator.equals(_testataDocumento,
                    testataDocumentoWrapper._testataDocumento)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public TestataDocumento getWrappedTestataDocumento() {
        return _testataDocumento;
    }

    @Override
    public TestataDocumento getWrappedModel() {
        return _testataDocumento;
    }

    @Override
    public void resetOriginalValues() {
        _testataDocumento.resetOriginalValues();
    }
}
