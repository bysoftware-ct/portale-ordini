package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.CarrelloLocalServiceUtil;
import it.bysoftware.ct.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class CarrelloClp extends BaseModelImpl<Carrello> implements Carrello {
    private long _id;
    private long _idOrdine;
    private long _progressivo;
    private double _importo;
    private boolean _chiuso;
    private boolean _generico;
    private boolean _lavorato;
    private BaseModel<?> _carrelloRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public CarrelloClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Carrello.class;
    }

    @Override
    public String getModelClassName() {
        return Carrello.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("idOrdine", getIdOrdine());
        attributes.put("progressivo", getProgressivo());
        attributes.put("importo", getImporto());
        attributes.put("chiuso", getChiuso());
        attributes.put("generico", getGenerico());
        attributes.put("lavorato", getLavorato());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long idOrdine = (Long) attributes.get("idOrdine");

        if (idOrdine != null) {
            setIdOrdine(idOrdine);
        }

        Long progressivo = (Long) attributes.get("progressivo");

        if (progressivo != null) {
            setProgressivo(progressivo);
        }

        Double importo = (Double) attributes.get("importo");

        if (importo != null) {
            setImporto(importo);
        }

        Boolean chiuso = (Boolean) attributes.get("chiuso");

        if (chiuso != null) {
            setChiuso(chiuso);
        }

        Boolean generico = (Boolean) attributes.get("generico");

        if (generico != null) {
            setGenerico(generico);
        }

        Boolean lavorato = (Boolean) attributes.get("lavorato");

        if (lavorato != null) {
            setLavorato(lavorato);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_carrelloRemoteModel != null) {
            try {
                Class<?> clazz = _carrelloRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_carrelloRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getIdOrdine() {
        return _idOrdine;
    }

    @Override
    public void setIdOrdine(long idOrdine) {
        _idOrdine = idOrdine;

        if (_carrelloRemoteModel != null) {
            try {
                Class<?> clazz = _carrelloRemoteModel.getClass();

                Method method = clazz.getMethod("setIdOrdine", long.class);

                method.invoke(_carrelloRemoteModel, idOrdine);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getProgressivo() {
        return _progressivo;
    }

    @Override
    public void setProgressivo(long progressivo) {
        _progressivo = progressivo;

        if (_carrelloRemoteModel != null) {
            try {
                Class<?> clazz = _carrelloRemoteModel.getClass();

                Method method = clazz.getMethod("setProgressivo", long.class);

                method.invoke(_carrelloRemoteModel, progressivo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getImporto() {
        return _importo;
    }

    @Override
    public void setImporto(double importo) {
        _importo = importo;

        if (_carrelloRemoteModel != null) {
            try {
                Class<?> clazz = _carrelloRemoteModel.getClass();

                Method method = clazz.getMethod("setImporto", double.class);

                method.invoke(_carrelloRemoteModel, importo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getChiuso() {
        return _chiuso;
    }

    @Override
    public boolean isChiuso() {
        return _chiuso;
    }

    @Override
    public void setChiuso(boolean chiuso) {
        _chiuso = chiuso;

        if (_carrelloRemoteModel != null) {
            try {
                Class<?> clazz = _carrelloRemoteModel.getClass();

                Method method = clazz.getMethod("setChiuso", boolean.class);

                method.invoke(_carrelloRemoteModel, chiuso);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getGenerico() {
        return _generico;
    }

    @Override
    public boolean isGenerico() {
        return _generico;
    }

    @Override
    public void setGenerico(boolean generico) {
        _generico = generico;

        if (_carrelloRemoteModel != null) {
            try {
                Class<?> clazz = _carrelloRemoteModel.getClass();

                Method method = clazz.getMethod("setGenerico", boolean.class);

                method.invoke(_carrelloRemoteModel, generico);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getLavorato() {
        return _lavorato;
    }

    @Override
    public boolean isLavorato() {
        return _lavorato;
    }

    @Override
    public void setLavorato(boolean lavorato) {
        _lavorato = lavorato;

        if (_carrelloRemoteModel != null) {
            try {
                Class<?> clazz = _carrelloRemoteModel.getClass();

                Method method = clazz.getMethod("setLavorato", boolean.class);

                method.invoke(_carrelloRemoteModel, lavorato);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getCarrelloRemoteModel() {
        return _carrelloRemoteModel;
    }

    public void setCarrelloRemoteModel(BaseModel<?> carrelloRemoteModel) {
        _carrelloRemoteModel = carrelloRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _carrelloRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_carrelloRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            CarrelloLocalServiceUtil.addCarrello(this);
        } else {
            CarrelloLocalServiceUtil.updateCarrello(this);
        }
    }

    @Override
    public Carrello toEscapedModel() {
        return (Carrello) ProxyUtil.newProxyInstance(Carrello.class.getClassLoader(),
            new Class[] { Carrello.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        CarrelloClp clone = new CarrelloClp();

        clone.setId(getId());
        clone.setIdOrdine(getIdOrdine());
        clone.setProgressivo(getProgressivo());
        clone.setImporto(getImporto());
        clone.setChiuso(getChiuso());
        clone.setGenerico(getGenerico());
        clone.setLavorato(getLavorato());

        return clone;
    }

    @Override
    public int compareTo(Carrello carrello) {
        long primaryKey = carrello.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CarrelloClp)) {
            return false;
        }

        CarrelloClp carrello = (CarrelloClp) obj;

        long primaryKey = carrello.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(15);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", idOrdine=");
        sb.append(getIdOrdine());
        sb.append(", progressivo=");
        sb.append(getProgressivo());
        sb.append(", importo=");
        sb.append(getImporto());
        sb.append(", chiuso=");
        sb.append(getChiuso());
        sb.append(", generico=");
        sb.append(getGenerico());
        sb.append(", lavorato=");
        sb.append(getLavorato());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(25);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.Carrello");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>idOrdine</column-name><column-value><![CDATA[");
        sb.append(getIdOrdine());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>progressivo</column-name><column-value><![CDATA[");
        sb.append(getProgressivo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>importo</column-name><column-value><![CDATA[");
        sb.append(getImporto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>chiuso</column-name><column-value><![CDATA[");
        sb.append(getChiuso());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>generico</column-name><column-value><![CDATA[");
        sb.append(getGenerico());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>lavorato</column-name><column-value><![CDATA[");
        sb.append(getLavorato());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
