package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.CausaliContabiliServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.CausaliContabiliServiceSoap
 * @generated
 */
public class CausaliContabiliSoap implements Serializable {
    private String _codiceCausaleCont;
    private String _codiceCausaleEC;
    private String _descrizioneCausaleCont;
    private int _testDareAvere;
    private int _tipoSoggettoMovimentato;

    public CausaliContabiliSoap() {
    }

    public static CausaliContabiliSoap toSoapModel(CausaliContabili model) {
        CausaliContabiliSoap soapModel = new CausaliContabiliSoap();

        soapModel.setCodiceCausaleCont(model.getCodiceCausaleCont());
        soapModel.setCodiceCausaleEC(model.getCodiceCausaleEC());
        soapModel.setDescrizioneCausaleCont(model.getDescrizioneCausaleCont());
        soapModel.setTestDareAvere(model.getTestDareAvere());
        soapModel.setTipoSoggettoMovimentato(model.getTipoSoggettoMovimentato());

        return soapModel;
    }

    public static CausaliContabiliSoap[] toSoapModels(CausaliContabili[] models) {
        CausaliContabiliSoap[] soapModels = new CausaliContabiliSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static CausaliContabiliSoap[][] toSoapModels(
        CausaliContabili[][] models) {
        CausaliContabiliSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new CausaliContabiliSoap[models.length][models[0].length];
        } else {
            soapModels = new CausaliContabiliSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static CausaliContabiliSoap[] toSoapModels(
        List<CausaliContabili> models) {
        List<CausaliContabiliSoap> soapModels = new ArrayList<CausaliContabiliSoap>(models.size());

        for (CausaliContabili model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new CausaliContabiliSoap[soapModels.size()]);
    }

    public String getPrimaryKey() {
        return _codiceCausaleCont;
    }

    public void setPrimaryKey(String pk) {
        setCodiceCausaleCont(pk);
    }

    public String getCodiceCausaleCont() {
        return _codiceCausaleCont;
    }

    public void setCodiceCausaleCont(String codiceCausaleCont) {
        _codiceCausaleCont = codiceCausaleCont;
    }

    public String getCodiceCausaleEC() {
        return _codiceCausaleEC;
    }

    public void setCodiceCausaleEC(String codiceCausaleEC) {
        _codiceCausaleEC = codiceCausaleEC;
    }

    public String getDescrizioneCausaleCont() {
        return _descrizioneCausaleCont;
    }

    public void setDescrizioneCausaleCont(String descrizioneCausaleCont) {
        _descrizioneCausaleCont = descrizioneCausaleCont;
    }

    public int getTestDareAvere() {
        return _testDareAvere;
    }

    public void setTestDareAvere(int testDareAvere) {
        _testDareAvere = testDareAvere;
    }

    public int getTipoSoggettoMovimentato() {
        return _tipoSoggettoMovimentato;
    }

    public void setTipoSoggettoMovimentato(int tipoSoggettoMovimentato) {
        _tipoSoggettoMovimentato = tipoSoggettoMovimentato;
    }
}
