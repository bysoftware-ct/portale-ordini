package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class AnagraficheClientiFornitoriClp extends BaseModelImpl<AnagraficheClientiFornitori>
    implements AnagraficheClientiFornitori {
    private boolean _attivoEC;
    private String _CAP;
    private String _codiceAnagrafica;
    private String _codiceFiscale;
    private String _codiceMnemonico;
    private String _comune;
    private String _indirizzo;
    private String _note;
    private String _partitaIVA;
    private String _ragioneSociale1;
    private String _ragioneSociale2;
    private String _siglaProvincia;
    private String _siglaStato;
    private boolean _personaFisica;
    private String _tipoSoggetto;
    private int _tipoSollecito;
    private int _codiceAzienda;
    private BaseModel<?> _anagraficheClientiFornitoriRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public AnagraficheClientiFornitoriClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return AnagraficheClientiFornitori.class;
    }

    @Override
    public String getModelClassName() {
        return AnagraficheClientiFornitori.class.getName();
    }

    @Override
    public String getPrimaryKey() {
        return _codiceAnagrafica;
    }

    @Override
    public void setPrimaryKey(String primaryKey) {
        setCodiceAnagrafica(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _codiceAnagrafica;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((String) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("attivoEC", getAttivoEC());
        attributes.put("CAP", getCAP());
        attributes.put("codiceAnagrafica", getCodiceAnagrafica());
        attributes.put("codiceFiscale", getCodiceFiscale());
        attributes.put("codiceMnemonico", getCodiceMnemonico());
        attributes.put("comune", getComune());
        attributes.put("indirizzo", getIndirizzo());
        attributes.put("note", getNote());
        attributes.put("partitaIVA", getPartitaIVA());
        attributes.put("ragioneSociale1", getRagioneSociale1());
        attributes.put("ragioneSociale2", getRagioneSociale2());
        attributes.put("siglaProvincia", getSiglaProvincia());
        attributes.put("siglaStato", getSiglaStato());
        attributes.put("personaFisica", getPersonaFisica());
        attributes.put("tipoSoggetto", getTipoSoggetto());
        attributes.put("tipoSollecito", getTipoSollecito());
        attributes.put("codiceAzienda", getCodiceAzienda());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Boolean attivoEC = (Boolean) attributes.get("attivoEC");

        if (attivoEC != null) {
            setAttivoEC(attivoEC);
        }

        String CAP = (String) attributes.get("CAP");

        if (CAP != null) {
            setCAP(CAP);
        }

        String codiceAnagrafica = (String) attributes.get("codiceAnagrafica");

        if (codiceAnagrafica != null) {
            setCodiceAnagrafica(codiceAnagrafica);
        }

        String codiceFiscale = (String) attributes.get("codiceFiscale");

        if (codiceFiscale != null) {
            setCodiceFiscale(codiceFiscale);
        }

        String codiceMnemonico = (String) attributes.get("codiceMnemonico");

        if (codiceMnemonico != null) {
            setCodiceMnemonico(codiceMnemonico);
        }

        String comune = (String) attributes.get("comune");

        if (comune != null) {
            setComune(comune);
        }

        String indirizzo = (String) attributes.get("indirizzo");

        if (indirizzo != null) {
            setIndirizzo(indirizzo);
        }

        String note = (String) attributes.get("note");

        if (note != null) {
            setNote(note);
        }

        String partitaIVA = (String) attributes.get("partitaIVA");

        if (partitaIVA != null) {
            setPartitaIVA(partitaIVA);
        }

        String ragioneSociale1 = (String) attributes.get("ragioneSociale1");

        if (ragioneSociale1 != null) {
            setRagioneSociale1(ragioneSociale1);
        }

        String ragioneSociale2 = (String) attributes.get("ragioneSociale2");

        if (ragioneSociale2 != null) {
            setRagioneSociale2(ragioneSociale2);
        }

        String siglaProvincia = (String) attributes.get("siglaProvincia");

        if (siglaProvincia != null) {
            setSiglaProvincia(siglaProvincia);
        }

        String siglaStato = (String) attributes.get("siglaStato");

        if (siglaStato != null) {
            setSiglaStato(siglaStato);
        }

        Boolean personaFisica = (Boolean) attributes.get("personaFisica");

        if (personaFisica != null) {
            setPersonaFisica(personaFisica);
        }

        String tipoSoggetto = (String) attributes.get("tipoSoggetto");

        if (tipoSoggetto != null) {
            setTipoSoggetto(tipoSoggetto);
        }

        Integer tipoSollecito = (Integer) attributes.get("tipoSollecito");

        if (tipoSollecito != null) {
            setTipoSollecito(tipoSollecito);
        }

        Integer codiceAzienda = (Integer) attributes.get("codiceAzienda");

        if (codiceAzienda != null) {
            setCodiceAzienda(codiceAzienda);
        }
    }

    @Override
    public boolean getAttivoEC() {
        return _attivoEC;
    }

    @Override
    public boolean isAttivoEC() {
        return _attivoEC;
    }

    @Override
    public void setAttivoEC(boolean attivoEC) {
        _attivoEC = attivoEC;

        if (_anagraficheClientiFornitoriRemoteModel != null) {
            try {
                Class<?> clazz = _anagraficheClientiFornitoriRemoteModel.getClass();

                Method method = clazz.getMethod("setAttivoEC", boolean.class);

                method.invoke(_anagraficheClientiFornitoriRemoteModel, attivoEC);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCAP() {
        return _CAP;
    }

    @Override
    public void setCAP(String CAP) {
        _CAP = CAP;

        if (_anagraficheClientiFornitoriRemoteModel != null) {
            try {
                Class<?> clazz = _anagraficheClientiFornitoriRemoteModel.getClass();

                Method method = clazz.getMethod("setCAP", String.class);

                method.invoke(_anagraficheClientiFornitoriRemoteModel, CAP);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceAnagrafica() {
        return _codiceAnagrafica;
    }

    @Override
    public void setCodiceAnagrafica(String codiceAnagrafica) {
        _codiceAnagrafica = codiceAnagrafica;

        if (_anagraficheClientiFornitoriRemoteModel != null) {
            try {
                Class<?> clazz = _anagraficheClientiFornitoriRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceAnagrafica",
                        String.class);

                method.invoke(_anagraficheClientiFornitoriRemoteModel,
                    codiceAnagrafica);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceFiscale() {
        return _codiceFiscale;
    }

    @Override
    public void setCodiceFiscale(String codiceFiscale) {
        _codiceFiscale = codiceFiscale;

        if (_anagraficheClientiFornitoriRemoteModel != null) {
            try {
                Class<?> clazz = _anagraficheClientiFornitoriRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceFiscale", String.class);

                method.invoke(_anagraficheClientiFornitoriRemoteModel,
                    codiceFiscale);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceMnemonico() {
        return _codiceMnemonico;
    }

    @Override
    public void setCodiceMnemonico(String codiceMnemonico) {
        _codiceMnemonico = codiceMnemonico;

        if (_anagraficheClientiFornitoriRemoteModel != null) {
            try {
                Class<?> clazz = _anagraficheClientiFornitoriRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceMnemonico",
                        String.class);

                method.invoke(_anagraficheClientiFornitoriRemoteModel,
                    codiceMnemonico);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getComune() {
        return _comune;
    }

    @Override
    public void setComune(String comune) {
        _comune = comune;

        if (_anagraficheClientiFornitoriRemoteModel != null) {
            try {
                Class<?> clazz = _anagraficheClientiFornitoriRemoteModel.getClass();

                Method method = clazz.getMethod("setComune", String.class);

                method.invoke(_anagraficheClientiFornitoriRemoteModel, comune);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getIndirizzo() {
        return _indirizzo;
    }

    @Override
    public void setIndirizzo(String indirizzo) {
        _indirizzo = indirizzo;

        if (_anagraficheClientiFornitoriRemoteModel != null) {
            try {
                Class<?> clazz = _anagraficheClientiFornitoriRemoteModel.getClass();

                Method method = clazz.getMethod("setIndirizzo", String.class);

                method.invoke(_anagraficheClientiFornitoriRemoteModel, indirizzo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNote() {
        return _note;
    }

    @Override
    public void setNote(String note) {
        _note = note;

        if (_anagraficheClientiFornitoriRemoteModel != null) {
            try {
                Class<?> clazz = _anagraficheClientiFornitoriRemoteModel.getClass();

                Method method = clazz.getMethod("setNote", String.class);

                method.invoke(_anagraficheClientiFornitoriRemoteModel, note);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getPartitaIVA() {
        return _partitaIVA;
    }

    @Override
    public void setPartitaIVA(String partitaIVA) {
        _partitaIVA = partitaIVA;

        if (_anagraficheClientiFornitoriRemoteModel != null) {
            try {
                Class<?> clazz = _anagraficheClientiFornitoriRemoteModel.getClass();

                Method method = clazz.getMethod("setPartitaIVA", String.class);

                method.invoke(_anagraficheClientiFornitoriRemoteModel,
                    partitaIVA);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getRagioneSociale1() {
        return _ragioneSociale1;
    }

    @Override
    public void setRagioneSociale1(String ragioneSociale1) {
        _ragioneSociale1 = ragioneSociale1;

        if (_anagraficheClientiFornitoriRemoteModel != null) {
            try {
                Class<?> clazz = _anagraficheClientiFornitoriRemoteModel.getClass();

                Method method = clazz.getMethod("setRagioneSociale1",
                        String.class);

                method.invoke(_anagraficheClientiFornitoriRemoteModel,
                    ragioneSociale1);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getRagioneSociale2() {
        return _ragioneSociale2;
    }

    @Override
    public void setRagioneSociale2(String ragioneSociale2) {
        _ragioneSociale2 = ragioneSociale2;

        if (_anagraficheClientiFornitoriRemoteModel != null) {
            try {
                Class<?> clazz = _anagraficheClientiFornitoriRemoteModel.getClass();

                Method method = clazz.getMethod("setRagioneSociale2",
                        String.class);

                method.invoke(_anagraficheClientiFornitoriRemoteModel,
                    ragioneSociale2);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getSiglaProvincia() {
        return _siglaProvincia;
    }

    @Override
    public void setSiglaProvincia(String siglaProvincia) {
        _siglaProvincia = siglaProvincia;

        if (_anagraficheClientiFornitoriRemoteModel != null) {
            try {
                Class<?> clazz = _anagraficheClientiFornitoriRemoteModel.getClass();

                Method method = clazz.getMethod("setSiglaProvincia",
                        String.class);

                method.invoke(_anagraficheClientiFornitoriRemoteModel,
                    siglaProvincia);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getSiglaStato() {
        return _siglaStato;
    }

    @Override
    public void setSiglaStato(String siglaStato) {
        _siglaStato = siglaStato;

        if (_anagraficheClientiFornitoriRemoteModel != null) {
            try {
                Class<?> clazz = _anagraficheClientiFornitoriRemoteModel.getClass();

                Method method = clazz.getMethod("setSiglaStato", String.class);

                method.invoke(_anagraficheClientiFornitoriRemoteModel,
                    siglaStato);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getPersonaFisica() {
        return _personaFisica;
    }

    @Override
    public boolean isPersonaFisica() {
        return _personaFisica;
    }

    @Override
    public void setPersonaFisica(boolean personaFisica) {
        _personaFisica = personaFisica;

        if (_anagraficheClientiFornitoriRemoteModel != null) {
            try {
                Class<?> clazz = _anagraficheClientiFornitoriRemoteModel.getClass();

                Method method = clazz.getMethod("setPersonaFisica",
                        boolean.class);

                method.invoke(_anagraficheClientiFornitoriRemoteModel,
                    personaFisica);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getTipoSoggetto() {
        return _tipoSoggetto;
    }

    @Override
    public void setTipoSoggetto(String tipoSoggetto) {
        _tipoSoggetto = tipoSoggetto;

        if (_anagraficheClientiFornitoriRemoteModel != null) {
            try {
                Class<?> clazz = _anagraficheClientiFornitoriRemoteModel.getClass();

                Method method = clazz.getMethod("setTipoSoggetto", String.class);

                method.invoke(_anagraficheClientiFornitoriRemoteModel,
                    tipoSoggetto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getTipoSollecito() {
        return _tipoSollecito;
    }

    @Override
    public void setTipoSollecito(int tipoSollecito) {
        _tipoSollecito = tipoSollecito;

        if (_anagraficheClientiFornitoriRemoteModel != null) {
            try {
                Class<?> clazz = _anagraficheClientiFornitoriRemoteModel.getClass();

                Method method = clazz.getMethod("setTipoSollecito", int.class);

                method.invoke(_anagraficheClientiFornitoriRemoteModel,
                    tipoSollecito);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getCodiceAzienda() {
        return _codiceAzienda;
    }

    @Override
    public void setCodiceAzienda(int codiceAzienda) {
        _codiceAzienda = codiceAzienda;

        if (_anagraficheClientiFornitoriRemoteModel != null) {
            try {
                Class<?> clazz = _anagraficheClientiFornitoriRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceAzienda", int.class);

                method.invoke(_anagraficheClientiFornitoriRemoteModel,
                    codiceAzienda);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public java.lang.String getPreferredEmail(java.lang.String code) {
        try {
            String methodName = "getPreferredEmail";

            Class<?>[] parameterTypes = new Class<?>[] { java.lang.String.class };

            Object[] parameterValues = new Object[] { code };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> getMovimentiVuoti(
        java.util.Date from, java.util.Date to, java.lang.String code,
        int start, int end) {
        try {
            String methodName = "getMovimentiVuoti";

            Class<?>[] parameterTypes = new Class<?>[] {
                    java.util.Date.class, java.util.Date.class,
                    java.lang.String.class, int.class, int.class
                };

            Object[] parameterValues = new Object[] { from, to, code, start, end };

            java.util.List<it.bysoftware.ct.model.MovimentoVuoto> returnObj = (java.util.List<it.bysoftware.ct.model.MovimentoVuoto>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> getMovimentiVuoti() {
        try {
            String methodName = "getMovimentiVuoti";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.util.List<it.bysoftware.ct.model.MovimentoVuoto> returnObj = (java.util.List<it.bysoftware.ct.model.MovimentoVuoto>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public BaseModel<?> getAnagraficheClientiFornitoriRemoteModel() {
        return _anagraficheClientiFornitoriRemoteModel;
    }

    public void setAnagraficheClientiFornitoriRemoteModel(
        BaseModel<?> anagraficheClientiFornitoriRemoteModel) {
        _anagraficheClientiFornitoriRemoteModel = anagraficheClientiFornitoriRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _anagraficheClientiFornitoriRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_anagraficheClientiFornitoriRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            AnagraficheClientiFornitoriLocalServiceUtil.addAnagraficheClientiFornitori(this);
        } else {
            AnagraficheClientiFornitoriLocalServiceUtil.updateAnagraficheClientiFornitori(this);
        }
    }

    @Override
    public AnagraficheClientiFornitori toEscapedModel() {
        return (AnagraficheClientiFornitori) ProxyUtil.newProxyInstance(AnagraficheClientiFornitori.class.getClassLoader(),
            new Class[] { AnagraficheClientiFornitori.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        AnagraficheClientiFornitoriClp clone = new AnagraficheClientiFornitoriClp();

        clone.setAttivoEC(getAttivoEC());
        clone.setCAP(getCAP());
        clone.setCodiceAnagrafica(getCodiceAnagrafica());
        clone.setCodiceFiscale(getCodiceFiscale());
        clone.setCodiceMnemonico(getCodiceMnemonico());
        clone.setComune(getComune());
        clone.setIndirizzo(getIndirizzo());
        clone.setNote(getNote());
        clone.setPartitaIVA(getPartitaIVA());
        clone.setRagioneSociale1(getRagioneSociale1());
        clone.setRagioneSociale2(getRagioneSociale2());
        clone.setSiglaProvincia(getSiglaProvincia());
        clone.setSiglaStato(getSiglaStato());
        clone.setPersonaFisica(getPersonaFisica());
        clone.setTipoSoggetto(getTipoSoggetto());
        clone.setTipoSollecito(getTipoSollecito());
        clone.setCodiceAzienda(getCodiceAzienda());

        return clone;
    }

    @Override
    public int compareTo(
        AnagraficheClientiFornitori anagraficheClientiFornitori) {
        int value = 0;

        value = getCodiceAnagrafica()
                    .compareTo(anagraficheClientiFornitori.getCodiceAnagrafica());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof AnagraficheClientiFornitoriClp)) {
            return false;
        }

        AnagraficheClientiFornitoriClp anagraficheClientiFornitori = (AnagraficheClientiFornitoriClp) obj;

        String primaryKey = anagraficheClientiFornitori.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(35);

        sb.append("{attivoEC=");
        sb.append(getAttivoEC());
        sb.append(", CAP=");
        sb.append(getCAP());
        sb.append(", codiceAnagrafica=");
        sb.append(getCodiceAnagrafica());
        sb.append(", codiceFiscale=");
        sb.append(getCodiceFiscale());
        sb.append(", codiceMnemonico=");
        sb.append(getCodiceMnemonico());
        sb.append(", comune=");
        sb.append(getComune());
        sb.append(", indirizzo=");
        sb.append(getIndirizzo());
        sb.append(", note=");
        sb.append(getNote());
        sb.append(", partitaIVA=");
        sb.append(getPartitaIVA());
        sb.append(", ragioneSociale1=");
        sb.append(getRagioneSociale1());
        sb.append(", ragioneSociale2=");
        sb.append(getRagioneSociale2());
        sb.append(", siglaProvincia=");
        sb.append(getSiglaProvincia());
        sb.append(", siglaStato=");
        sb.append(getSiglaStato());
        sb.append(", personaFisica=");
        sb.append(getPersonaFisica());
        sb.append(", tipoSoggetto=");
        sb.append(getTipoSoggetto());
        sb.append(", tipoSollecito=");
        sb.append(getTipoSollecito());
        sb.append(", codiceAzienda=");
        sb.append(getCodiceAzienda());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(55);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.AnagraficheClientiFornitori");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>attivoEC</column-name><column-value><![CDATA[");
        sb.append(getAttivoEC());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>CAP</column-name><column-value><![CDATA[");
        sb.append(getCAP());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceAnagrafica</column-name><column-value><![CDATA[");
        sb.append(getCodiceAnagrafica());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceFiscale</column-name><column-value><![CDATA[");
        sb.append(getCodiceFiscale());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceMnemonico</column-name><column-value><![CDATA[");
        sb.append(getCodiceMnemonico());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>comune</column-name><column-value><![CDATA[");
        sb.append(getComune());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>indirizzo</column-name><column-value><![CDATA[");
        sb.append(getIndirizzo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>note</column-name><column-value><![CDATA[");
        sb.append(getNote());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>partitaIVA</column-name><column-value><![CDATA[");
        sb.append(getPartitaIVA());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>ragioneSociale1</column-name><column-value><![CDATA[");
        sb.append(getRagioneSociale1());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>ragioneSociale2</column-name><column-value><![CDATA[");
        sb.append(getRagioneSociale2());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>siglaProvincia</column-name><column-value><![CDATA[");
        sb.append(getSiglaProvincia());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>siglaStato</column-name><column-value><![CDATA[");
        sb.append(getSiglaStato());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>personaFisica</column-name><column-value><![CDATA[");
        sb.append(getPersonaFisica());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tipoSoggetto</column-name><column-value><![CDATA[");
        sb.append(getTipoSoggetto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tipoSollecito</column-name><column-value><![CDATA[");
        sb.append(getTipoSollecito());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceAzienda</column-name><column-value><![CDATA[");
        sb.append(getCodiceAzienda());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
