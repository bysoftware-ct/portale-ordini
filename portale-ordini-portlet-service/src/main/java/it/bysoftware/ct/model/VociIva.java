package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the VociIva service. Represents a row in the &quot;VociIva&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see VociIvaModel
 * @see it.bysoftware.ct.model.impl.VociIvaImpl
 * @see it.bysoftware.ct.model.impl.VociIvaModelImpl
 * @generated
 */
public interface VociIva extends VociIvaModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.VociIvaImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
