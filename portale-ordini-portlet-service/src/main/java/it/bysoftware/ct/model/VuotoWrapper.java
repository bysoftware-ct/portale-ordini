package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Vuoto}.
 * </p>
 *
 * @author Mario Torrisi
 * @see Vuoto
 * @generated
 */
public class VuotoWrapper implements Vuoto, ModelWrapper<Vuoto> {
    private Vuoto _vuoto;

    public VuotoWrapper(Vuoto vuoto) {
        _vuoto = vuoto;
    }

    @Override
    public Class<?> getModelClass() {
        return Vuoto.class;
    }

    @Override
    public String getModelClassName() {
        return Vuoto.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceVuoto", getCodiceVuoto());
        attributes.put("nome", getNome());
        attributes.put("descrizione", getDescrizione());
        attributes.put("unitaMisura", getUnitaMisura());
        attributes.put("note", getNote());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceVuoto = (String) attributes.get("codiceVuoto");

        if (codiceVuoto != null) {
            setCodiceVuoto(codiceVuoto);
        }

        String nome = (String) attributes.get("nome");

        if (nome != null) {
            setNome(nome);
        }

        String descrizione = (String) attributes.get("descrizione");

        if (descrizione != null) {
            setDescrizione(descrizione);
        }

        String unitaMisura = (String) attributes.get("unitaMisura");

        if (unitaMisura != null) {
            setUnitaMisura(unitaMisura);
        }

        String note = (String) attributes.get("note");

        if (note != null) {
            setNote(note);
        }
    }

    /**
    * Returns the primary key of this vuoto.
    *
    * @return the primary key of this vuoto
    */
    @Override
    public java.lang.String getPrimaryKey() {
        return _vuoto.getPrimaryKey();
    }

    /**
    * Sets the primary key of this vuoto.
    *
    * @param primaryKey the primary key of this vuoto
    */
    @Override
    public void setPrimaryKey(java.lang.String primaryKey) {
        _vuoto.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the codice vuoto of this vuoto.
    *
    * @return the codice vuoto of this vuoto
    */
    @Override
    public java.lang.String getCodiceVuoto() {
        return _vuoto.getCodiceVuoto();
    }

    /**
    * Sets the codice vuoto of this vuoto.
    *
    * @param codiceVuoto the codice vuoto of this vuoto
    */
    @Override
    public void setCodiceVuoto(java.lang.String codiceVuoto) {
        _vuoto.setCodiceVuoto(codiceVuoto);
    }

    /**
    * Returns the nome of this vuoto.
    *
    * @return the nome of this vuoto
    */
    @Override
    public java.lang.String getNome() {
        return _vuoto.getNome();
    }

    /**
    * Sets the nome of this vuoto.
    *
    * @param nome the nome of this vuoto
    */
    @Override
    public void setNome(java.lang.String nome) {
        _vuoto.setNome(nome);
    }

    /**
    * Returns the descrizione of this vuoto.
    *
    * @return the descrizione of this vuoto
    */
    @Override
    public java.lang.String getDescrizione() {
        return _vuoto.getDescrizione();
    }

    /**
    * Sets the descrizione of this vuoto.
    *
    * @param descrizione the descrizione of this vuoto
    */
    @Override
    public void setDescrizione(java.lang.String descrizione) {
        _vuoto.setDescrizione(descrizione);
    }

    /**
    * Returns the unita misura of this vuoto.
    *
    * @return the unita misura of this vuoto
    */
    @Override
    public java.lang.String getUnitaMisura() {
        return _vuoto.getUnitaMisura();
    }

    /**
    * Sets the unita misura of this vuoto.
    *
    * @param unitaMisura the unita misura of this vuoto
    */
    @Override
    public void setUnitaMisura(java.lang.String unitaMisura) {
        _vuoto.setUnitaMisura(unitaMisura);
    }

    /**
    * Returns the note of this vuoto.
    *
    * @return the note of this vuoto
    */
    @Override
    public java.lang.String getNote() {
        return _vuoto.getNote();
    }

    /**
    * Sets the note of this vuoto.
    *
    * @param note the note of this vuoto
    */
    @Override
    public void setNote(java.lang.String note) {
        _vuoto.setNote(note);
    }

    @Override
    public boolean isNew() {
        return _vuoto.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _vuoto.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _vuoto.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _vuoto.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _vuoto.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _vuoto.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _vuoto.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _vuoto.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _vuoto.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _vuoto.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _vuoto.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new VuotoWrapper((Vuoto) _vuoto.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.Vuoto vuoto) {
        return _vuoto.compareTo(vuoto);
    }

    @Override
    public int hashCode() {
        return _vuoto.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.Vuoto> toCacheModel() {
        return _vuoto.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.Vuoto toEscapedModel() {
        return new VuotoWrapper(_vuoto.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.Vuoto toUnescapedModel() {
        return new VuotoWrapper(_vuoto.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _vuoto.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _vuoto.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _vuoto.persist();
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> getVuoti() {
        return _vuoto.getVuoti();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VuotoWrapper)) {
            return false;
        }

        VuotoWrapper vuotoWrapper = (VuotoWrapper) obj;

        if (Validator.equals(_vuoto, vuotoWrapper._vuoto)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Vuoto getWrappedVuoto() {
        return _vuoto;
    }

    @Override
    public Vuoto getWrappedModel() {
        return _vuoto;
    }

    @Override
    public void resetOriginalValues() {
        _vuoto.resetOriginalValues();
    }
}
