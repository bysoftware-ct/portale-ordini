package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.StatiServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.StatiServiceSoap
 * @generated
 */
public class StatiSoap implements Serializable {
    private boolean _black;
    private String _codiceStato;
    private String _codiceISO;
    private String _codicePaese;
    private String _stato;
    private String _codiceAzienda;

    public StatiSoap() {
    }

    public static StatiSoap toSoapModel(Stati model) {
        StatiSoap soapModel = new StatiSoap();

        soapModel.setBlack(model.getBlack());
        soapModel.setCodiceStato(model.getCodiceStato());
        soapModel.setCodiceISO(model.getCodiceISO());
        soapModel.setCodicePaese(model.getCodicePaese());
        soapModel.setStato(model.getStato());
        soapModel.setCodiceAzienda(model.getCodiceAzienda());

        return soapModel;
    }

    public static StatiSoap[] toSoapModels(Stati[] models) {
        StatiSoap[] soapModels = new StatiSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static StatiSoap[][] toSoapModels(Stati[][] models) {
        StatiSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new StatiSoap[models.length][models[0].length];
        } else {
            soapModels = new StatiSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static StatiSoap[] toSoapModels(List<Stati> models) {
        List<StatiSoap> soapModels = new ArrayList<StatiSoap>(models.size());

        for (Stati model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new StatiSoap[soapModels.size()]);
    }

    public String getPrimaryKey() {
        return _codiceStato;
    }

    public void setPrimaryKey(String pk) {
        setCodiceStato(pk);
    }

    public boolean getBlack() {
        return _black;
    }

    public boolean isBlack() {
        return _black;
    }

    public void setBlack(boolean black) {
        _black = black;
    }

    public String getCodiceStato() {
        return _codiceStato;
    }

    public void setCodiceStato(String codiceStato) {
        _codiceStato = codiceStato;
    }

    public String getCodiceISO() {
        return _codiceISO;
    }

    public void setCodiceISO(String codiceISO) {
        _codiceISO = codiceISO;
    }

    public String getCodicePaese() {
        return _codicePaese;
    }

    public void setCodicePaese(String codicePaese) {
        _codicePaese = codicePaese;
    }

    public String getStato() {
        return _stato;
    }

    public void setStato(String stato) {
        _stato = stato;
    }

    public String getCodiceAzienda() {
        return _codiceAzienda;
    }

    public void setCodiceAzienda(String codiceAzienda) {
        _codiceAzienda = codiceAzienda;
    }
}
