package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.ArticoliServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.ArticoliServiceSoap
 * @generated
 */
public class ArticoliSoap implements Serializable {
    private String _codiceArticolo;
    private String _categoriaMerceologica;
    private String _categoriaInventario;
    private String _descrizione;
    private String _descrizioneDocumento;
    private String _descrizioneFiscale;
    private String _unitaMisura;
    private double _tara;
    private String _codiceIVA;
    private double _prezzo1;
    private double _prezzo2;
    private double _prezzo3;
    private long _libLng1;
    private String _libStr1;
    private boolean _obsoleto;
    private String _tipoArticolo;
    private String _codiceProvvigione;
    private double _pesoLordo;

    public ArticoliSoap() {
    }

    public static ArticoliSoap toSoapModel(Articoli model) {
        ArticoliSoap soapModel = new ArticoliSoap();

        soapModel.setCodiceArticolo(model.getCodiceArticolo());
        soapModel.setCategoriaMerceologica(model.getCategoriaMerceologica());
        soapModel.setCategoriaInventario(model.getCategoriaInventario());
        soapModel.setDescrizione(model.getDescrizione());
        soapModel.setDescrizioneDocumento(model.getDescrizioneDocumento());
        soapModel.setDescrizioneFiscale(model.getDescrizioneFiscale());
        soapModel.setUnitaMisura(model.getUnitaMisura());
        soapModel.setTara(model.getTara());
        soapModel.setCodiceIVA(model.getCodiceIVA());
        soapModel.setPrezzo1(model.getPrezzo1());
        soapModel.setPrezzo2(model.getPrezzo2());
        soapModel.setPrezzo3(model.getPrezzo3());
        soapModel.setLibLng1(model.getLibLng1());
        soapModel.setLibStr1(model.getLibStr1());
        soapModel.setObsoleto(model.getObsoleto());
        soapModel.setTipoArticolo(model.getTipoArticolo());
        soapModel.setCodiceProvvigione(model.getCodiceProvvigione());
        soapModel.setPesoLordo(model.getPesoLordo());

        return soapModel;
    }

    public static ArticoliSoap[] toSoapModels(Articoli[] models) {
        ArticoliSoap[] soapModels = new ArticoliSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static ArticoliSoap[][] toSoapModels(Articoli[][] models) {
        ArticoliSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new ArticoliSoap[models.length][models[0].length];
        } else {
            soapModels = new ArticoliSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static ArticoliSoap[] toSoapModels(List<Articoli> models) {
        List<ArticoliSoap> soapModels = new ArrayList<ArticoliSoap>(models.size());

        for (Articoli model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new ArticoliSoap[soapModels.size()]);
    }

    public String getPrimaryKey() {
        return _codiceArticolo;
    }

    public void setPrimaryKey(String pk) {
        setCodiceArticolo(pk);
    }

    public String getCodiceArticolo() {
        return _codiceArticolo;
    }

    public void setCodiceArticolo(String codiceArticolo) {
        _codiceArticolo = codiceArticolo;
    }

    public String getCategoriaMerceologica() {
        return _categoriaMerceologica;
    }

    public void setCategoriaMerceologica(String categoriaMerceologica) {
        _categoriaMerceologica = categoriaMerceologica;
    }

    public String getCategoriaInventario() {
        return _categoriaInventario;
    }

    public void setCategoriaInventario(String categoriaInventario) {
        _categoriaInventario = categoriaInventario;
    }

    public String getDescrizione() {
        return _descrizione;
    }

    public void setDescrizione(String descrizione) {
        _descrizione = descrizione;
    }

    public String getDescrizioneDocumento() {
        return _descrizioneDocumento;
    }

    public void setDescrizioneDocumento(String descrizioneDocumento) {
        _descrizioneDocumento = descrizioneDocumento;
    }

    public String getDescrizioneFiscale() {
        return _descrizioneFiscale;
    }

    public void setDescrizioneFiscale(String descrizioneFiscale) {
        _descrizioneFiscale = descrizioneFiscale;
    }

    public String getUnitaMisura() {
        return _unitaMisura;
    }

    public void setUnitaMisura(String unitaMisura) {
        _unitaMisura = unitaMisura;
    }

    public double getTara() {
        return _tara;
    }

    public void setTara(double tara) {
        _tara = tara;
    }

    public String getCodiceIVA() {
        return _codiceIVA;
    }

    public void setCodiceIVA(String codiceIVA) {
        _codiceIVA = codiceIVA;
    }

    public double getPrezzo1() {
        return _prezzo1;
    }

    public void setPrezzo1(double prezzo1) {
        _prezzo1 = prezzo1;
    }

    public double getPrezzo2() {
        return _prezzo2;
    }

    public void setPrezzo2(double prezzo2) {
        _prezzo2 = prezzo2;
    }

    public double getPrezzo3() {
        return _prezzo3;
    }

    public void setPrezzo3(double prezzo3) {
        _prezzo3 = prezzo3;
    }

    public long getLibLng1() {
        return _libLng1;
    }

    public void setLibLng1(long libLng1) {
        _libLng1 = libLng1;
    }

    public String getLibStr1() {
        return _libStr1;
    }

    public void setLibStr1(String libStr1) {
        _libStr1 = libStr1;
    }

    public boolean getObsoleto() {
        return _obsoleto;
    }

    public boolean isObsoleto() {
        return _obsoleto;
    }

    public void setObsoleto(boolean obsoleto) {
        _obsoleto = obsoleto;
    }

    public String getTipoArticolo() {
        return _tipoArticolo;
    }

    public void setTipoArticolo(String tipoArticolo) {
        _tipoArticolo = tipoArticolo;
    }

    public String getCodiceProvvigione() {
        return _codiceProvvigione;
    }

    public void setCodiceProvvigione(String codiceProvvigione) {
        _codiceProvvigione = codiceProvvigione;
    }

    public double getPesoLordo() {
        return _pesoLordo;
    }

    public void setPesoLordo(double pesoLordo) {
        _pesoLordo = pesoLordo;
    }
}
