package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Variante}.
 * </p>
 *
 * @author Mario Torrisi
 * @see Variante
 * @generated
 */
public class VarianteWrapper implements Variante, ModelWrapper<Variante> {
    private Variante _variante;

    public VarianteWrapper(Variante variante) {
        _variante = variante;
    }

    @Override
    public Class<?> getModelClass() {
        return Variante.class;
    }

    @Override
    public String getModelClassName() {
        return Variante.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("idOpzione", getIdOpzione());
        attributes.put("variante", getVariante());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long idOpzione = (Long) attributes.get("idOpzione");

        if (idOpzione != null) {
            setIdOpzione(idOpzione);
        }

        String variante = (String) attributes.get("variante");

        if (variante != null) {
            setVariante(variante);
        }
    }

    /**
    * Returns the primary key of this variante.
    *
    * @return the primary key of this variante
    */
    @Override
    public long getPrimaryKey() {
        return _variante.getPrimaryKey();
    }

    /**
    * Sets the primary key of this variante.
    *
    * @param primaryKey the primary key of this variante
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _variante.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this variante.
    *
    * @return the ID of this variante
    */
    @Override
    public long getId() {
        return _variante.getId();
    }

    /**
    * Sets the ID of this variante.
    *
    * @param id the ID of this variante
    */
    @Override
    public void setId(long id) {
        _variante.setId(id);
    }

    /**
    * Returns the id opzione of this variante.
    *
    * @return the id opzione of this variante
    */
    @Override
    public long getIdOpzione() {
        return _variante.getIdOpzione();
    }

    /**
    * Sets the id opzione of this variante.
    *
    * @param idOpzione the id opzione of this variante
    */
    @Override
    public void setIdOpzione(long idOpzione) {
        _variante.setIdOpzione(idOpzione);
    }

    /**
    * Returns the variante of this variante.
    *
    * @return the variante of this variante
    */
    @Override
    public java.lang.String getVariante() {
        return _variante.getVariante();
    }

    /**
    * Sets the variante of this variante.
    *
    * @param variante the variante of this variante
    */
    @Override
    public void setVariante(java.lang.String variante) {
        _variante.setVariante(variante);
    }

    @Override
    public boolean isNew() {
        return _variante.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _variante.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _variante.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _variante.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _variante.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _variante.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _variante.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _variante.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _variante.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _variante.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _variante.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new VarianteWrapper((Variante) _variante.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.Variante variante) {
        return _variante.compareTo(variante);
    }

    @Override
    public int hashCode() {
        return _variante.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.Variante> toCacheModel() {
        return _variante.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.Variante toEscapedModel() {
        return new VarianteWrapper(_variante.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.Variante toUnescapedModel() {
        return new VarianteWrapper(_variante.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _variante.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _variante.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _variante.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VarianteWrapper)) {
            return false;
        }

        VarianteWrapper varianteWrapper = (VarianteWrapper) obj;

        if (Validator.equals(_variante, varianteWrapper._variante)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Variante getWrappedVariante() {
        return _variante;
    }

    @Override
    public Variante getWrappedModel() {
        return _variante;
    }

    @Override
    public void resetOriginalValues() {
        _variante.resetOriginalValues();
    }
}
