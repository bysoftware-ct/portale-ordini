package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.CentroLocalServiceUtil;
import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.persistence.CentroPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class CentroClp extends BaseModelImpl<Centro> implements Centro {
    private String _attivita;
    private String _centro;
    private BaseModel<?> _centroRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public CentroClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Centro.class;
    }

    @Override
    public String getModelClassName() {
        return Centro.class.getName();
    }

    @Override
    public CentroPK getPrimaryKey() {
        return new CentroPK(_attivita, _centro);
    }

    @Override
    public void setPrimaryKey(CentroPK primaryKey) {
        setAttivita(primaryKey.attivita);
        setCentro(primaryKey.centro);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return new CentroPK(_attivita, _centro);
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((CentroPK) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("attivita", getAttivita());
        attributes.put("centro", getCentro());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String attivita = (String) attributes.get("attivita");

        if (attivita != null) {
            setAttivita(attivita);
        }

        String centro = (String) attributes.get("centro");

        if (centro != null) {
            setCentro(centro);
        }
    }

    @Override
    public String getAttivita() {
        return _attivita;
    }

    @Override
    public void setAttivita(String attivita) {
        _attivita = attivita;

        if (_centroRemoteModel != null) {
            try {
                Class<?> clazz = _centroRemoteModel.getClass();

                Method method = clazz.getMethod("setAttivita", String.class);

                method.invoke(_centroRemoteModel, attivita);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCentro() {
        return _centro;
    }

    @Override
    public void setCentro(String centro) {
        _centro = centro;

        if (_centroRemoteModel != null) {
            try {
                Class<?> clazz = _centroRemoteModel.getClass();

                Method method = clazz.getMethod("setCentro", String.class);

                method.invoke(_centroRemoteModel, centro);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getCentroRemoteModel() {
        return _centroRemoteModel;
    }

    public void setCentroRemoteModel(BaseModel<?> centroRemoteModel) {
        _centroRemoteModel = centroRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _centroRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_centroRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            CentroLocalServiceUtil.addCentro(this);
        } else {
            CentroLocalServiceUtil.updateCentro(this);
        }
    }

    @Override
    public Centro toEscapedModel() {
        return (Centro) ProxyUtil.newProxyInstance(Centro.class.getClassLoader(),
            new Class[] { Centro.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        CentroClp clone = new CentroClp();

        clone.setAttivita(getAttivita());
        clone.setCentro(getCentro());

        return clone;
    }

    @Override
    public int compareTo(Centro centro) {
        CentroPK primaryKey = centro.getPrimaryKey();

        return getPrimaryKey().compareTo(primaryKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CentroClp)) {
            return false;
        }

        CentroClp centro = (CentroClp) obj;

        CentroPK primaryKey = centro.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(5);

        sb.append("{attivita=");
        sb.append(getAttivita());
        sb.append(", centro=");
        sb.append(getCentro());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(10);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.Centro");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>attivita</column-name><column-value><![CDATA[");
        sb.append(getAttivita());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>centro</column-name><column-value><![CDATA[");
        sb.append(getCentro());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
