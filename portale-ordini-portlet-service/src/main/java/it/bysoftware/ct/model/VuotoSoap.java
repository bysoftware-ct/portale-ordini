package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.VuotoServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.VuotoServiceSoap
 * @generated
 */
public class VuotoSoap implements Serializable {
    private String _codiceVuoto;
    private String _nome;
    private String _descrizione;
    private String _unitaMisura;
    private String _note;

    public VuotoSoap() {
    }

    public static VuotoSoap toSoapModel(Vuoto model) {
        VuotoSoap soapModel = new VuotoSoap();

        soapModel.setCodiceVuoto(model.getCodiceVuoto());
        soapModel.setNome(model.getNome());
        soapModel.setDescrizione(model.getDescrizione());
        soapModel.setUnitaMisura(model.getUnitaMisura());
        soapModel.setNote(model.getNote());

        return soapModel;
    }

    public static VuotoSoap[] toSoapModels(Vuoto[] models) {
        VuotoSoap[] soapModels = new VuotoSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static VuotoSoap[][] toSoapModels(Vuoto[][] models) {
        VuotoSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new VuotoSoap[models.length][models[0].length];
        } else {
            soapModels = new VuotoSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static VuotoSoap[] toSoapModels(List<Vuoto> models) {
        List<VuotoSoap> soapModels = new ArrayList<VuotoSoap>(models.size());

        for (Vuoto model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new VuotoSoap[soapModels.size()]);
    }

    public String getPrimaryKey() {
        return _codiceVuoto;
    }

    public void setPrimaryKey(String pk) {
        setCodiceVuoto(pk);
    }

    public String getCodiceVuoto() {
        return _codiceVuoto;
    }

    public void setCodiceVuoto(String codiceVuoto) {
        _codiceVuoto = codiceVuoto;
    }

    public String getNome() {
        return _nome;
    }

    public void setNome(String nome) {
        _nome = nome;
    }

    public String getDescrizione() {
        return _descrizione;
    }

    public void setDescrizione(String descrizione) {
        _descrizione = descrizione;
    }

    public String getUnitaMisura() {
        return _unitaMisura;
    }

    public void setUnitaMisura(String unitaMisura) {
        _unitaMisura = unitaMisura;
    }

    public String getNote() {
        return _note;
    }

    public void setNote(String note) {
        _note = note;
    }
}
