package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link TipoCarrello}.
 * </p>
 *
 * @author Mario Torrisi
 * @see TipoCarrello
 * @generated
 */
public class TipoCarrelloWrapper implements TipoCarrello,
    ModelWrapper<TipoCarrello> {
    private TipoCarrello _tipoCarrello;

    public TipoCarrelloWrapper(TipoCarrello tipoCarrello) {
        _tipoCarrello = tipoCarrello;
    }

    @Override
    public Class<?> getModelClass() {
        return TipoCarrello.class;
    }

    @Override
    public String getModelClassName() {
        return TipoCarrello.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("tipologia", getTipologia());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        String tipologia = (String) attributes.get("tipologia");

        if (tipologia != null) {
            setTipologia(tipologia);
        }
    }

    /**
    * Returns the primary key of this tipo carrello.
    *
    * @return the primary key of this tipo carrello
    */
    @Override
    public long getPrimaryKey() {
        return _tipoCarrello.getPrimaryKey();
    }

    /**
    * Sets the primary key of this tipo carrello.
    *
    * @param primaryKey the primary key of this tipo carrello
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _tipoCarrello.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this tipo carrello.
    *
    * @return the ID of this tipo carrello
    */
    @Override
    public long getId() {
        return _tipoCarrello.getId();
    }

    /**
    * Sets the ID of this tipo carrello.
    *
    * @param id the ID of this tipo carrello
    */
    @Override
    public void setId(long id) {
        _tipoCarrello.setId(id);
    }

    /**
    * Returns the tipologia of this tipo carrello.
    *
    * @return the tipologia of this tipo carrello
    */
    @Override
    public java.lang.String getTipologia() {
        return _tipoCarrello.getTipologia();
    }

    /**
    * Sets the tipologia of this tipo carrello.
    *
    * @param tipologia the tipologia of this tipo carrello
    */
    @Override
    public void setTipologia(java.lang.String tipologia) {
        _tipoCarrello.setTipologia(tipologia);
    }

    @Override
    public boolean isNew() {
        return _tipoCarrello.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _tipoCarrello.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _tipoCarrello.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _tipoCarrello.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _tipoCarrello.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _tipoCarrello.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _tipoCarrello.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _tipoCarrello.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _tipoCarrello.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _tipoCarrello.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _tipoCarrello.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new TipoCarrelloWrapper((TipoCarrello) _tipoCarrello.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.TipoCarrello tipoCarrello) {
        return _tipoCarrello.compareTo(tipoCarrello);
    }

    @Override
    public int hashCode() {
        return _tipoCarrello.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.TipoCarrello> toCacheModel() {
        return _tipoCarrello.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.TipoCarrello toEscapedModel() {
        return new TipoCarrelloWrapper(_tipoCarrello.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.TipoCarrello toUnescapedModel() {
        return new TipoCarrelloWrapper(_tipoCarrello.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _tipoCarrello.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _tipoCarrello.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _tipoCarrello.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof TipoCarrelloWrapper)) {
            return false;
        }

        TipoCarrelloWrapper tipoCarrelloWrapper = (TipoCarrelloWrapper) obj;

        if (Validator.equals(_tipoCarrello, tipoCarrelloWrapper._tipoCarrello)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public TipoCarrello getWrappedTipoCarrello() {
        return _tipoCarrello;
    }

    @Override
    public TipoCarrello getWrappedModel() {
        return _tipoCarrello;
    }

    @Override
    public void resetOriginalValues() {
        _tipoCarrello.resetOriginalValues();
    }
}
