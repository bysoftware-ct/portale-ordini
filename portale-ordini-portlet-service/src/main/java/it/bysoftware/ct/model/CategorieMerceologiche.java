package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the CategorieMerceologiche service. Represents a row in the &quot;CategorieMerceologiche&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see CategorieMerceologicheModel
 * @see it.bysoftware.ct.model.impl.CategorieMerceologicheImpl
 * @see it.bysoftware.ct.model.impl.CategorieMerceologicheModelImpl
 * @generated
 */
public interface CategorieMerceologiche extends CategorieMerceologicheModel,
    PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.CategorieMerceologicheImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
