package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.AnagraficheClientiFornitoriServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.AnagraficheClientiFornitoriServiceSoap
 * @generated
 */
public class AnagraficheClientiFornitoriSoap implements Serializable {
    private boolean _attivoEC;
    private String _CAP;
    private String _codiceAnagrafica;
    private String _codiceFiscale;
    private String _codiceMnemonico;
    private String _comune;
    private String _indirizzo;
    private String _note;
    private String _partitaIVA;
    private String _ragioneSociale1;
    private String _ragioneSociale2;
    private String _siglaProvincia;
    private String _siglaStato;
    private boolean _personaFisica;
    private String _tipoSoggetto;
    private int _tipoSollecito;
    private int _codiceAzienda;

    public AnagraficheClientiFornitoriSoap() {
    }

    public static AnagraficheClientiFornitoriSoap toSoapModel(
        AnagraficheClientiFornitori model) {
        AnagraficheClientiFornitoriSoap soapModel = new AnagraficheClientiFornitoriSoap();

        soapModel.setAttivoEC(model.getAttivoEC());
        soapModel.setCAP(model.getCAP());
        soapModel.setCodiceAnagrafica(model.getCodiceAnagrafica());
        soapModel.setCodiceFiscale(model.getCodiceFiscale());
        soapModel.setCodiceMnemonico(model.getCodiceMnemonico());
        soapModel.setComune(model.getComune());
        soapModel.setIndirizzo(model.getIndirizzo());
        soapModel.setNote(model.getNote());
        soapModel.setPartitaIVA(model.getPartitaIVA());
        soapModel.setRagioneSociale1(model.getRagioneSociale1());
        soapModel.setRagioneSociale2(model.getRagioneSociale2());
        soapModel.setSiglaProvincia(model.getSiglaProvincia());
        soapModel.setSiglaStato(model.getSiglaStato());
        soapModel.setPersonaFisica(model.getPersonaFisica());
        soapModel.setTipoSoggetto(model.getTipoSoggetto());
        soapModel.setTipoSollecito(model.getTipoSollecito());
        soapModel.setCodiceAzienda(model.getCodiceAzienda());

        return soapModel;
    }

    public static AnagraficheClientiFornitoriSoap[] toSoapModels(
        AnagraficheClientiFornitori[] models) {
        AnagraficheClientiFornitoriSoap[] soapModels = new AnagraficheClientiFornitoriSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static AnagraficheClientiFornitoriSoap[][] toSoapModels(
        AnagraficheClientiFornitori[][] models) {
        AnagraficheClientiFornitoriSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new AnagraficheClientiFornitoriSoap[models.length][models[0].length];
        } else {
            soapModels = new AnagraficheClientiFornitoriSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static AnagraficheClientiFornitoriSoap[] toSoapModels(
        List<AnagraficheClientiFornitori> models) {
        List<AnagraficheClientiFornitoriSoap> soapModels = new ArrayList<AnagraficheClientiFornitoriSoap>(models.size());

        for (AnagraficheClientiFornitori model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new AnagraficheClientiFornitoriSoap[soapModels.size()]);
    }

    public String getPrimaryKey() {
        return _codiceAnagrafica;
    }

    public void setPrimaryKey(String pk) {
        setCodiceAnagrafica(pk);
    }

    public boolean getAttivoEC() {
        return _attivoEC;
    }

    public boolean isAttivoEC() {
        return _attivoEC;
    }

    public void setAttivoEC(boolean attivoEC) {
        _attivoEC = attivoEC;
    }

    public String getCAP() {
        return _CAP;
    }

    public void setCAP(String CAP) {
        _CAP = CAP;
    }

    public String getCodiceAnagrafica() {
        return _codiceAnagrafica;
    }

    public void setCodiceAnagrafica(String codiceAnagrafica) {
        _codiceAnagrafica = codiceAnagrafica;
    }

    public String getCodiceFiscale() {
        return _codiceFiscale;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        _codiceFiscale = codiceFiscale;
    }

    public String getCodiceMnemonico() {
        return _codiceMnemonico;
    }

    public void setCodiceMnemonico(String codiceMnemonico) {
        _codiceMnemonico = codiceMnemonico;
    }

    public String getComune() {
        return _comune;
    }

    public void setComune(String comune) {
        _comune = comune;
    }

    public String getIndirizzo() {
        return _indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        _indirizzo = indirizzo;
    }

    public String getNote() {
        return _note;
    }

    public void setNote(String note) {
        _note = note;
    }

    public String getPartitaIVA() {
        return _partitaIVA;
    }

    public void setPartitaIVA(String partitaIVA) {
        _partitaIVA = partitaIVA;
    }

    public String getRagioneSociale1() {
        return _ragioneSociale1;
    }

    public void setRagioneSociale1(String ragioneSociale1) {
        _ragioneSociale1 = ragioneSociale1;
    }

    public String getRagioneSociale2() {
        return _ragioneSociale2;
    }

    public void setRagioneSociale2(String ragioneSociale2) {
        _ragioneSociale2 = ragioneSociale2;
    }

    public String getSiglaProvincia() {
        return _siglaProvincia;
    }

    public void setSiglaProvincia(String siglaProvincia) {
        _siglaProvincia = siglaProvincia;
    }

    public String getSiglaStato() {
        return _siglaStato;
    }

    public void setSiglaStato(String siglaStato) {
        _siglaStato = siglaStato;
    }

    public boolean getPersonaFisica() {
        return _personaFisica;
    }

    public boolean isPersonaFisica() {
        return _personaFisica;
    }

    public void setPersonaFisica(boolean personaFisica) {
        _personaFisica = personaFisica;
    }

    public String getTipoSoggetto() {
        return _tipoSoggetto;
    }

    public void setTipoSoggetto(String tipoSoggetto) {
        _tipoSoggetto = tipoSoggetto;
    }

    public int getTipoSollecito() {
        return _tipoSollecito;
    }

    public void setTipoSollecito(int tipoSollecito) {
        _tipoSollecito = tipoSollecito;
    }

    public int getCodiceAzienda() {
        return _codiceAzienda;
    }

    public void setCodiceAzienda(int codiceAzienda) {
        _codiceAzienda = codiceAzienda;
    }
}
