package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.SottocontiLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class SottocontiClp extends BaseModelImpl<Sottoconti>
    implements Sottoconti {
    private String _codice;
    private String _descrizione;
    private BaseModel<?> _sottocontiRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public SottocontiClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Sottoconti.class;
    }

    @Override
    public String getModelClassName() {
        return Sottoconti.class.getName();
    }

    @Override
    public String getPrimaryKey() {
        return _codice;
    }

    @Override
    public void setPrimaryKey(String primaryKey) {
        setCodice(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _codice;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((String) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codice", getCodice());
        attributes.put("descrizione", getDescrizione());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codice = (String) attributes.get("codice");

        if (codice != null) {
            setCodice(codice);
        }

        String descrizione = (String) attributes.get("descrizione");

        if (descrizione != null) {
            setDescrizione(descrizione);
        }
    }

    @Override
    public String getCodice() {
        return _codice;
    }

    @Override
    public void setCodice(String codice) {
        _codice = codice;

        if (_sottocontiRemoteModel != null) {
            try {
                Class<?> clazz = _sottocontiRemoteModel.getClass();

                Method method = clazz.getMethod("setCodice", String.class);

                method.invoke(_sottocontiRemoteModel, codice);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getDescrizione() {
        return _descrizione;
    }

    @Override
    public void setDescrizione(String descrizione) {
        _descrizione = descrizione;

        if (_sottocontiRemoteModel != null) {
            try {
                Class<?> clazz = _sottocontiRemoteModel.getClass();

                Method method = clazz.getMethod("setDescrizione", String.class);

                method.invoke(_sottocontiRemoteModel, descrizione);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getSottocontiRemoteModel() {
        return _sottocontiRemoteModel;
    }

    public void setSottocontiRemoteModel(BaseModel<?> sottocontiRemoteModel) {
        _sottocontiRemoteModel = sottocontiRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _sottocontiRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_sottocontiRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            SottocontiLocalServiceUtil.addSottoconti(this);
        } else {
            SottocontiLocalServiceUtil.updateSottoconti(this);
        }
    }

    @Override
    public Sottoconti toEscapedModel() {
        return (Sottoconti) ProxyUtil.newProxyInstance(Sottoconti.class.getClassLoader(),
            new Class[] { Sottoconti.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        SottocontiClp clone = new SottocontiClp();

        clone.setCodice(getCodice());
        clone.setDescrizione(getDescrizione());

        return clone;
    }

    @Override
    public int compareTo(Sottoconti sottoconti) {
        String primaryKey = sottoconti.getPrimaryKey();

        return getPrimaryKey().compareTo(primaryKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof SottocontiClp)) {
            return false;
        }

        SottocontiClp sottoconti = (SottocontiClp) obj;

        String primaryKey = sottoconti.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(5);

        sb.append("{codice=");
        sb.append(getCodice());
        sb.append(", descrizione=");
        sb.append(getDescrizione());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(10);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.Sottoconti");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>codice</column-name><column-value><![CDATA[");
        sb.append(getCodice());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>descrizione</column-name><column-value><![CDATA[");
        sb.append(getDescrizione());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
