package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.MovimentoVuotoLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class MovimentoVuotoClp extends BaseModelImpl<MovimentoVuoto>
    implements MovimentoVuoto {
    private long _idMovimento;
    private String _codiceVuoto;
    private Date _dataMovimento;
    private String _codiceSoggetto;
    private double _quantita;
    private int _tipoMovimento;
    private String _note;
    private int _uuid;
    private BaseModel<?> _movimentoVuotoRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public MovimentoVuotoClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return MovimentoVuoto.class;
    }

    @Override
    public String getModelClassName() {
        return MovimentoVuoto.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _idMovimento;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setIdMovimento(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _idMovimento;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("idMovimento", getIdMovimento());
        attributes.put("codiceVuoto", getCodiceVuoto());
        attributes.put("dataMovimento", getDataMovimento());
        attributes.put("codiceSoggetto", getCodiceSoggetto());
        attributes.put("quantita", getQuantita());
        attributes.put("tipoMovimento", getTipoMovimento());
        attributes.put("note", getNote());
        attributes.put("uuid", getUuid());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long idMovimento = (Long) attributes.get("idMovimento");

        if (idMovimento != null) {
            setIdMovimento(idMovimento);
        }

        String codiceVuoto = (String) attributes.get("codiceVuoto");

        if (codiceVuoto != null) {
            setCodiceVuoto(codiceVuoto);
        }

        Date dataMovimento = (Date) attributes.get("dataMovimento");

        if (dataMovimento != null) {
            setDataMovimento(dataMovimento);
        }

        String codiceSoggetto = (String) attributes.get("codiceSoggetto");

        if (codiceSoggetto != null) {
            setCodiceSoggetto(codiceSoggetto);
        }

        Double quantita = (Double) attributes.get("quantita");

        if (quantita != null) {
            setQuantita(quantita);
        }

        Integer tipoMovimento = (Integer) attributes.get("tipoMovimento");

        if (tipoMovimento != null) {
            setTipoMovimento(tipoMovimento);
        }

        String note = (String) attributes.get("note");

        if (note != null) {
            setNote(note);
        }

        Integer uuid = (Integer) attributes.get("uuid");

        if (uuid != null) {
            setUuid(uuid);
        }
    }

    @Override
    public long getIdMovimento() {
        return _idMovimento;
    }

    @Override
    public void setIdMovimento(long idMovimento) {
        _idMovimento = idMovimento;

        if (_movimentoVuotoRemoteModel != null) {
            try {
                Class<?> clazz = _movimentoVuotoRemoteModel.getClass();

                Method method = clazz.getMethod("setIdMovimento", long.class);

                method.invoke(_movimentoVuotoRemoteModel, idMovimento);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceVuoto() {
        return _codiceVuoto;
    }

    @Override
    public void setCodiceVuoto(String codiceVuoto) {
        _codiceVuoto = codiceVuoto;

        if (_movimentoVuotoRemoteModel != null) {
            try {
                Class<?> clazz = _movimentoVuotoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceVuoto", String.class);

                method.invoke(_movimentoVuotoRemoteModel, codiceVuoto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getDataMovimento() {
        return _dataMovimento;
    }

    @Override
    public void setDataMovimento(Date dataMovimento) {
        _dataMovimento = dataMovimento;

        if (_movimentoVuotoRemoteModel != null) {
            try {
                Class<?> clazz = _movimentoVuotoRemoteModel.getClass();

                Method method = clazz.getMethod("setDataMovimento", Date.class);

                method.invoke(_movimentoVuotoRemoteModel, dataMovimento);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceSoggetto() {
        return _codiceSoggetto;
    }

    @Override
    public void setCodiceSoggetto(String codiceSoggetto) {
        _codiceSoggetto = codiceSoggetto;

        if (_movimentoVuotoRemoteModel != null) {
            try {
                Class<?> clazz = _movimentoVuotoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceSoggetto",
                        String.class);

                method.invoke(_movimentoVuotoRemoteModel, codiceSoggetto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getQuantita() {
        return _quantita;
    }

    @Override
    public void setQuantita(double quantita) {
        _quantita = quantita;

        if (_movimentoVuotoRemoteModel != null) {
            try {
                Class<?> clazz = _movimentoVuotoRemoteModel.getClass();

                Method method = clazz.getMethod("setQuantita", double.class);

                method.invoke(_movimentoVuotoRemoteModel, quantita);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getTipoMovimento() {
        return _tipoMovimento;
    }

    @Override
    public void setTipoMovimento(int tipoMovimento) {
        _tipoMovimento = tipoMovimento;

        if (_movimentoVuotoRemoteModel != null) {
            try {
                Class<?> clazz = _movimentoVuotoRemoteModel.getClass();

                Method method = clazz.getMethod("setTipoMovimento", int.class);

                method.invoke(_movimentoVuotoRemoteModel, tipoMovimento);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNote() {
        return _note;
    }

    @Override
    public void setNote(String note) {
        _note = note;

        if (_movimentoVuotoRemoteModel != null) {
            try {
                Class<?> clazz = _movimentoVuotoRemoteModel.getClass();

                Method method = clazz.getMethod("setNote", String.class);

                method.invoke(_movimentoVuotoRemoteModel, note);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getUuid() {
        return _uuid;
    }

    @Override
    public void setUuid(int uuid) {
        _uuid = uuid;

        if (_movimentoVuotoRemoteModel != null) {
            try {
                Class<?> clazz = _movimentoVuotoRemoteModel.getClass();

                Method method = clazz.getMethod("setUuid", int.class);

                method.invoke(_movimentoVuotoRemoteModel, uuid);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getMovimentoVuotoRemoteModel() {
        return _movimentoVuotoRemoteModel;
    }

    public void setMovimentoVuotoRemoteModel(
        BaseModel<?> movimentoVuotoRemoteModel) {
        _movimentoVuotoRemoteModel = movimentoVuotoRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _movimentoVuotoRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_movimentoVuotoRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            MovimentoVuotoLocalServiceUtil.addMovimentoVuoto(this);
        } else {
            MovimentoVuotoLocalServiceUtil.updateMovimentoVuoto(this);
        }
    }

    @Override
    public MovimentoVuoto toEscapedModel() {
        return (MovimentoVuoto) ProxyUtil.newProxyInstance(MovimentoVuoto.class.getClassLoader(),
            new Class[] { MovimentoVuoto.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        MovimentoVuotoClp clone = new MovimentoVuotoClp();

        clone.setIdMovimento(getIdMovimento());
        clone.setCodiceVuoto(getCodiceVuoto());
        clone.setDataMovimento(getDataMovimento());
        clone.setCodiceSoggetto(getCodiceSoggetto());
        clone.setQuantita(getQuantita());
        clone.setTipoMovimento(getTipoMovimento());
        clone.setNote(getNote());
        clone.setUuid(getUuid());

        return clone;
    }

    @Override
    public int compareTo(MovimentoVuoto movimentoVuoto) {
        int value = 0;

        value = DateUtil.compareTo(getDataMovimento(),
                movimentoVuoto.getDataMovimento());

        if (value != 0) {
            return value;
        }

        if (getUuid() < movimentoVuoto.getUuid()) {
            value = -1;
        } else if (getUuid() > movimentoVuoto.getUuid()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof MovimentoVuotoClp)) {
            return false;
        }

        MovimentoVuotoClp movimentoVuoto = (MovimentoVuotoClp) obj;

        long primaryKey = movimentoVuoto.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(17);

        sb.append("{idMovimento=");
        sb.append(getIdMovimento());
        sb.append(", codiceVuoto=");
        sb.append(getCodiceVuoto());
        sb.append(", dataMovimento=");
        sb.append(getDataMovimento());
        sb.append(", codiceSoggetto=");
        sb.append(getCodiceSoggetto());
        sb.append(", quantita=");
        sb.append(getQuantita());
        sb.append(", tipoMovimento=");
        sb.append(getTipoMovimento());
        sb.append(", note=");
        sb.append(getNote());
        sb.append(", uuid=");
        sb.append(getUuid());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(28);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.MovimentoVuoto");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>idMovimento</column-name><column-value><![CDATA[");
        sb.append(getIdMovimento());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceVuoto</column-name><column-value><![CDATA[");
        sb.append(getCodiceVuoto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>dataMovimento</column-name><column-value><![CDATA[");
        sb.append(getDataMovimento());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceSoggetto</column-name><column-value><![CDATA[");
        sb.append(getCodiceSoggetto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>quantita</column-name><column-value><![CDATA[");
        sb.append(getQuantita());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tipoMovimento</column-name><column-value><![CDATA[");
        sb.append(getTipoMovimento());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>note</column-name><column-value><![CDATA[");
        sb.append(getNote());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>uuid</column-name><column-value><![CDATA[");
        sb.append(getUuid());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
