package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link MovimentoVuoto}.
 * </p>
 *
 * @author Mario Torrisi
 * @see MovimentoVuoto
 * @generated
 */
public class MovimentoVuotoWrapper implements MovimentoVuoto,
    ModelWrapper<MovimentoVuoto> {
    private MovimentoVuoto _movimentoVuoto;

    public MovimentoVuotoWrapper(MovimentoVuoto movimentoVuoto) {
        _movimentoVuoto = movimentoVuoto;
    }

    @Override
    public Class<?> getModelClass() {
        return MovimentoVuoto.class;
    }

    @Override
    public String getModelClassName() {
        return MovimentoVuoto.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("idMovimento", getIdMovimento());
        attributes.put("codiceVuoto", getCodiceVuoto());
        attributes.put("dataMovimento", getDataMovimento());
        attributes.put("codiceSoggetto", getCodiceSoggetto());
        attributes.put("quantita", getQuantita());
        attributes.put("tipoMovimento", getTipoMovimento());
        attributes.put("note", getNote());
        attributes.put("uuid", getUuid());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long idMovimento = (Long) attributes.get("idMovimento");

        if (idMovimento != null) {
            setIdMovimento(idMovimento);
        }

        String codiceVuoto = (String) attributes.get("codiceVuoto");

        if (codiceVuoto != null) {
            setCodiceVuoto(codiceVuoto);
        }

        Date dataMovimento = (Date) attributes.get("dataMovimento");

        if (dataMovimento != null) {
            setDataMovimento(dataMovimento);
        }

        String codiceSoggetto = (String) attributes.get("codiceSoggetto");

        if (codiceSoggetto != null) {
            setCodiceSoggetto(codiceSoggetto);
        }

        Double quantita = (Double) attributes.get("quantita");

        if (quantita != null) {
            setQuantita(quantita);
        }

        Integer tipoMovimento = (Integer) attributes.get("tipoMovimento");

        if (tipoMovimento != null) {
            setTipoMovimento(tipoMovimento);
        }

        String note = (String) attributes.get("note");

        if (note != null) {
            setNote(note);
        }

        Integer uuid = (Integer) attributes.get("uuid");

        if (uuid != null) {
            setUuid(uuid);
        }
    }

    /**
    * Returns the primary key of this movimento vuoto.
    *
    * @return the primary key of this movimento vuoto
    */
    @Override
    public long getPrimaryKey() {
        return _movimentoVuoto.getPrimaryKey();
    }

    /**
    * Sets the primary key of this movimento vuoto.
    *
    * @param primaryKey the primary key of this movimento vuoto
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _movimentoVuoto.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the id movimento of this movimento vuoto.
    *
    * @return the id movimento of this movimento vuoto
    */
    @Override
    public long getIdMovimento() {
        return _movimentoVuoto.getIdMovimento();
    }

    /**
    * Sets the id movimento of this movimento vuoto.
    *
    * @param idMovimento the id movimento of this movimento vuoto
    */
    @Override
    public void setIdMovimento(long idMovimento) {
        _movimentoVuoto.setIdMovimento(idMovimento);
    }

    /**
    * Returns the codice vuoto of this movimento vuoto.
    *
    * @return the codice vuoto of this movimento vuoto
    */
    @Override
    public java.lang.String getCodiceVuoto() {
        return _movimentoVuoto.getCodiceVuoto();
    }

    /**
    * Sets the codice vuoto of this movimento vuoto.
    *
    * @param codiceVuoto the codice vuoto of this movimento vuoto
    */
    @Override
    public void setCodiceVuoto(java.lang.String codiceVuoto) {
        _movimentoVuoto.setCodiceVuoto(codiceVuoto);
    }

    /**
    * Returns the data movimento of this movimento vuoto.
    *
    * @return the data movimento of this movimento vuoto
    */
    @Override
    public java.util.Date getDataMovimento() {
        return _movimentoVuoto.getDataMovimento();
    }

    /**
    * Sets the data movimento of this movimento vuoto.
    *
    * @param dataMovimento the data movimento of this movimento vuoto
    */
    @Override
    public void setDataMovimento(java.util.Date dataMovimento) {
        _movimentoVuoto.setDataMovimento(dataMovimento);
    }

    /**
    * Returns the codice soggetto of this movimento vuoto.
    *
    * @return the codice soggetto of this movimento vuoto
    */
    @Override
    public java.lang.String getCodiceSoggetto() {
        return _movimentoVuoto.getCodiceSoggetto();
    }

    /**
    * Sets the codice soggetto of this movimento vuoto.
    *
    * @param codiceSoggetto the codice soggetto of this movimento vuoto
    */
    @Override
    public void setCodiceSoggetto(java.lang.String codiceSoggetto) {
        _movimentoVuoto.setCodiceSoggetto(codiceSoggetto);
    }

    /**
    * Returns the quantita of this movimento vuoto.
    *
    * @return the quantita of this movimento vuoto
    */
    @Override
    public double getQuantita() {
        return _movimentoVuoto.getQuantita();
    }

    /**
    * Sets the quantita of this movimento vuoto.
    *
    * @param quantita the quantita of this movimento vuoto
    */
    @Override
    public void setQuantita(double quantita) {
        _movimentoVuoto.setQuantita(quantita);
    }

    /**
    * Returns the tipo movimento of this movimento vuoto.
    *
    * @return the tipo movimento of this movimento vuoto
    */
    @Override
    public int getTipoMovimento() {
        return _movimentoVuoto.getTipoMovimento();
    }

    /**
    * Sets the tipo movimento of this movimento vuoto.
    *
    * @param tipoMovimento the tipo movimento of this movimento vuoto
    */
    @Override
    public void setTipoMovimento(int tipoMovimento) {
        _movimentoVuoto.setTipoMovimento(tipoMovimento);
    }

    /**
    * Returns the note of this movimento vuoto.
    *
    * @return the note of this movimento vuoto
    */
    @Override
    public java.lang.String getNote() {
        return _movimentoVuoto.getNote();
    }

    /**
    * Sets the note of this movimento vuoto.
    *
    * @param note the note of this movimento vuoto
    */
    @Override
    public void setNote(java.lang.String note) {
        _movimentoVuoto.setNote(note);
    }

    /**
    * Returns the uuid of this movimento vuoto.
    *
    * @return the uuid of this movimento vuoto
    */
    @Override
    public int getUuid() {
        return _movimentoVuoto.getUuid();
    }

    /**
    * Sets the uuid of this movimento vuoto.
    *
    * @param uuid the uuid of this movimento vuoto
    */
    @Override
    public void setUuid(int uuid) {
        _movimentoVuoto.setUuid(uuid);
    }

    @Override
    public boolean isNew() {
        return _movimentoVuoto.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _movimentoVuoto.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _movimentoVuoto.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _movimentoVuoto.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _movimentoVuoto.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _movimentoVuoto.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _movimentoVuoto.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _movimentoVuoto.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _movimentoVuoto.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _movimentoVuoto.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _movimentoVuoto.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new MovimentoVuotoWrapper((MovimentoVuoto) _movimentoVuoto.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.MovimentoVuoto movimentoVuoto) {
        return _movimentoVuoto.compareTo(movimentoVuoto);
    }

    @Override
    public int hashCode() {
        return _movimentoVuoto.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.MovimentoVuoto> toCacheModel() {
        return _movimentoVuoto.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.MovimentoVuoto toEscapedModel() {
        return new MovimentoVuotoWrapper(_movimentoVuoto.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.MovimentoVuoto toUnescapedModel() {
        return new MovimentoVuotoWrapper(_movimentoVuoto.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _movimentoVuoto.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _movimentoVuoto.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _movimentoVuoto.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof MovimentoVuotoWrapper)) {
            return false;
        }

        MovimentoVuotoWrapper movimentoVuotoWrapper = (MovimentoVuotoWrapper) obj;

        if (Validator.equals(_movimentoVuoto,
                    movimentoVuotoWrapper._movimentoVuoto)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public MovimentoVuoto getWrappedMovimentoVuoto() {
        return _movimentoVuoto;
    }

    @Override
    public MovimentoVuoto getWrappedModel() {
        return _movimentoVuoto;
    }

    @Override
    public void resetOriginalValues() {
        _movimentoVuoto.resetOriginalValues();
    }
}
