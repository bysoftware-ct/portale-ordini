package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.TipoCarrelloServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.TipoCarrelloServiceSoap
 * @generated
 */
public class TipoCarrelloSoap implements Serializable {
    private long _id;
    private String _tipologia;

    public TipoCarrelloSoap() {
    }

    public static TipoCarrelloSoap toSoapModel(TipoCarrello model) {
        TipoCarrelloSoap soapModel = new TipoCarrelloSoap();

        soapModel.setId(model.getId());
        soapModel.setTipologia(model.getTipologia());

        return soapModel;
    }

    public static TipoCarrelloSoap[] toSoapModels(TipoCarrello[] models) {
        TipoCarrelloSoap[] soapModels = new TipoCarrelloSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static TipoCarrelloSoap[][] toSoapModels(TipoCarrello[][] models) {
        TipoCarrelloSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new TipoCarrelloSoap[models.length][models[0].length];
        } else {
            soapModels = new TipoCarrelloSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static TipoCarrelloSoap[] toSoapModels(List<TipoCarrello> models) {
        List<TipoCarrelloSoap> soapModels = new ArrayList<TipoCarrelloSoap>(models.size());

        for (TipoCarrello model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new TipoCarrelloSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public String getTipologia() {
        return _tipologia;
    }

    public void setTipologia(String tipologia) {
        _tipologia = tipologia;
    }
}
