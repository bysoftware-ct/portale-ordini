package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Vuoto service. Represents a row in the &quot;_vuoti&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see VuotoModel
 * @see it.bysoftware.ct.model.impl.VuotoImpl
 * @see it.bysoftware.ct.model.impl.VuotoModelImpl
 * @generated
 */
public interface Vuoto extends VuotoModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.VuotoImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> getVuoti();
}
