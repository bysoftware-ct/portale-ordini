package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.CodiceConsorzioServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.CodiceConsorzioServiceSoap
 * @generated
 */
public class CodiceConsorzioSoap implements Serializable {
    private String _codiceSocio;
    private long _idLiferay;
    private String _codiceConsorzio;

    public CodiceConsorzioSoap() {
    }

    public static CodiceConsorzioSoap toSoapModel(CodiceConsorzio model) {
        CodiceConsorzioSoap soapModel = new CodiceConsorzioSoap();

        soapModel.setCodiceSocio(model.getCodiceSocio());
        soapModel.setIdLiferay(model.getIdLiferay());
        soapModel.setCodiceConsorzio(model.getCodiceConsorzio());

        return soapModel;
    }

    public static CodiceConsorzioSoap[] toSoapModels(CodiceConsorzio[] models) {
        CodiceConsorzioSoap[] soapModels = new CodiceConsorzioSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static CodiceConsorzioSoap[][] toSoapModels(
        CodiceConsorzio[][] models) {
        CodiceConsorzioSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new CodiceConsorzioSoap[models.length][models[0].length];
        } else {
            soapModels = new CodiceConsorzioSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static CodiceConsorzioSoap[] toSoapModels(
        List<CodiceConsorzio> models) {
        List<CodiceConsorzioSoap> soapModels = new ArrayList<CodiceConsorzioSoap>(models.size());

        for (CodiceConsorzio model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new CodiceConsorzioSoap[soapModels.size()]);
    }

    public String getPrimaryKey() {
        return _codiceSocio;
    }

    public void setPrimaryKey(String pk) {
        setCodiceSocio(pk);
    }

    public String getCodiceSocio() {
        return _codiceSocio;
    }

    public void setCodiceSocio(String codiceSocio) {
        _codiceSocio = codiceSocio;
    }

    public long getIdLiferay() {
        return _idLiferay;
    }

    public void setIdLiferay(long idLiferay) {
        _idLiferay = idLiferay;
    }

    public String getCodiceConsorzio() {
        return _codiceConsorzio;
    }

    public void setCodiceConsorzio(String codiceConsorzio) {
        _codiceConsorzio = codiceConsorzio;
    }
}
