package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the ContatoreSocio service. Represents a row in the &quot;_contatori_soci&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see ContatoreSocioModel
 * @see it.bysoftware.ct.model.impl.ContatoreSocioImpl
 * @see it.bysoftware.ct.model.impl.ContatoreSocioModelImpl
 * @generated
 */
public interface ContatoreSocio extends ContatoreSocioModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.ContatoreSocioImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
