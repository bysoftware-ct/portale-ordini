package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.VuotoLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class VuotoClp extends BaseModelImpl<Vuoto> implements Vuoto {
    private String _codiceVuoto;
    private String _nome;
    private String _descrizione;
    private String _unitaMisura;
    private String _note;
    private BaseModel<?> _vuotoRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public VuotoClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Vuoto.class;
    }

    @Override
    public String getModelClassName() {
        return Vuoto.class.getName();
    }

    @Override
    public String getPrimaryKey() {
        return _codiceVuoto;
    }

    @Override
    public void setPrimaryKey(String primaryKey) {
        setCodiceVuoto(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _codiceVuoto;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((String) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceVuoto", getCodiceVuoto());
        attributes.put("nome", getNome());
        attributes.put("descrizione", getDescrizione());
        attributes.put("unitaMisura", getUnitaMisura());
        attributes.put("note", getNote());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceVuoto = (String) attributes.get("codiceVuoto");

        if (codiceVuoto != null) {
            setCodiceVuoto(codiceVuoto);
        }

        String nome = (String) attributes.get("nome");

        if (nome != null) {
            setNome(nome);
        }

        String descrizione = (String) attributes.get("descrizione");

        if (descrizione != null) {
            setDescrizione(descrizione);
        }

        String unitaMisura = (String) attributes.get("unitaMisura");

        if (unitaMisura != null) {
            setUnitaMisura(unitaMisura);
        }

        String note = (String) attributes.get("note");

        if (note != null) {
            setNote(note);
        }
    }

    @Override
    public String getCodiceVuoto() {
        return _codiceVuoto;
    }

    @Override
    public void setCodiceVuoto(String codiceVuoto) {
        _codiceVuoto = codiceVuoto;

        if (_vuotoRemoteModel != null) {
            try {
                Class<?> clazz = _vuotoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceVuoto", String.class);

                method.invoke(_vuotoRemoteModel, codiceVuoto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNome() {
        return _nome;
    }

    @Override
    public void setNome(String nome) {
        _nome = nome;

        if (_vuotoRemoteModel != null) {
            try {
                Class<?> clazz = _vuotoRemoteModel.getClass();

                Method method = clazz.getMethod("setNome", String.class);

                method.invoke(_vuotoRemoteModel, nome);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getDescrizione() {
        return _descrizione;
    }

    @Override
    public void setDescrizione(String descrizione) {
        _descrizione = descrizione;

        if (_vuotoRemoteModel != null) {
            try {
                Class<?> clazz = _vuotoRemoteModel.getClass();

                Method method = clazz.getMethod("setDescrizione", String.class);

                method.invoke(_vuotoRemoteModel, descrizione);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getUnitaMisura() {
        return _unitaMisura;
    }

    @Override
    public void setUnitaMisura(String unitaMisura) {
        _unitaMisura = unitaMisura;

        if (_vuotoRemoteModel != null) {
            try {
                Class<?> clazz = _vuotoRemoteModel.getClass();

                Method method = clazz.getMethod("setUnitaMisura", String.class);

                method.invoke(_vuotoRemoteModel, unitaMisura);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNote() {
        return _note;
    }

    @Override
    public void setNote(String note) {
        _note = note;

        if (_vuotoRemoteModel != null) {
            try {
                Class<?> clazz = _vuotoRemoteModel.getClass();

                Method method = clazz.getMethod("setNote", String.class);

                method.invoke(_vuotoRemoteModel, note);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> getVuoti() {
        try {
            String methodName = "getVuoti";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.util.List<it.bysoftware.ct.model.MovimentoVuoto> returnObj = (java.util.List<it.bysoftware.ct.model.MovimentoVuoto>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public BaseModel<?> getVuotoRemoteModel() {
        return _vuotoRemoteModel;
    }

    public void setVuotoRemoteModel(BaseModel<?> vuotoRemoteModel) {
        _vuotoRemoteModel = vuotoRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _vuotoRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_vuotoRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            VuotoLocalServiceUtil.addVuoto(this);
        } else {
            VuotoLocalServiceUtil.updateVuoto(this);
        }
    }

    @Override
    public Vuoto toEscapedModel() {
        return (Vuoto) ProxyUtil.newProxyInstance(Vuoto.class.getClassLoader(),
            new Class[] { Vuoto.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        VuotoClp clone = new VuotoClp();

        clone.setCodiceVuoto(getCodiceVuoto());
        clone.setNome(getNome());
        clone.setDescrizione(getDescrizione());
        clone.setUnitaMisura(getUnitaMisura());
        clone.setNote(getNote());

        return clone;
    }

    @Override
    public int compareTo(Vuoto vuoto) {
        String primaryKey = vuoto.getPrimaryKey();

        return getPrimaryKey().compareTo(primaryKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VuotoClp)) {
            return false;
        }

        VuotoClp vuoto = (VuotoClp) obj;

        String primaryKey = vuoto.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{codiceVuoto=");
        sb.append(getCodiceVuoto());
        sb.append(", nome=");
        sb.append(getNome());
        sb.append(", descrizione=");
        sb.append(getDescrizione());
        sb.append(", unitaMisura=");
        sb.append(getUnitaMisura());
        sb.append(", note=");
        sb.append(getNote());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(19);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.Vuoto");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>codiceVuoto</column-name><column-value><![CDATA[");
        sb.append(getCodiceVuoto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>nome</column-name><column-value><![CDATA[");
        sb.append(getNome());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>descrizione</column-name><column-value><![CDATA[");
        sb.append(getDescrizione());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>unitaMisura</column-name><column-value><![CDATA[");
        sb.append(getUnitaMisura());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>note</column-name><column-value><![CDATA[");
        sb.append(getNote());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
