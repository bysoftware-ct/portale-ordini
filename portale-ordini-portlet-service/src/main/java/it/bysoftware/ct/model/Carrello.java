package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Carrello service. Represents a row in the &quot;_carrelli&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see CarrelloModel
 * @see it.bysoftware.ct.model.impl.CarrelloImpl
 * @see it.bysoftware.ct.model.impl.CarrelloModelImpl
 * @generated
 */
public interface Carrello extends CarrelloModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.CarrelloImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
