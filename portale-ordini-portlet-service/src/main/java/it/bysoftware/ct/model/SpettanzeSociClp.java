package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.SpettanzeSociLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class SpettanzeSociClp extends BaseModelImpl<SpettanzeSoci>
    implements SpettanzeSoci {
    private long _id;
    private int _anno;
    private String _codiceAttivita;
    private String _codiceCentro;
    private int _numeroProtocollo;
    private String _codiceFornitore;
    private double _importo;
    private Date _dataCalcolo;
    private int _stato;
    private int _idPagamento;
    private int _annoPagamento;
    private BaseModel<?> _spettanzeSociRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public SpettanzeSociClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return SpettanzeSoci.class;
    }

    @Override
    public String getModelClassName() {
        return SpettanzeSoci.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("anno", getAnno());
        attributes.put("codiceAttivita", getCodiceAttivita());
        attributes.put("codiceCentro", getCodiceCentro());
        attributes.put("numeroProtocollo", getNumeroProtocollo());
        attributes.put("codiceFornitore", getCodiceFornitore());
        attributes.put("importo", getImporto());
        attributes.put("dataCalcolo", getDataCalcolo());
        attributes.put("stato", getStato());
        attributes.put("idPagamento", getIdPagamento());
        attributes.put("annoPagamento", getAnnoPagamento());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Integer anno = (Integer) attributes.get("anno");

        if (anno != null) {
            setAnno(anno);
        }

        String codiceAttivita = (String) attributes.get("codiceAttivita");

        if (codiceAttivita != null) {
            setCodiceAttivita(codiceAttivita);
        }

        String codiceCentro = (String) attributes.get("codiceCentro");

        if (codiceCentro != null) {
            setCodiceCentro(codiceCentro);
        }

        Integer numeroProtocollo = (Integer) attributes.get("numeroProtocollo");

        if (numeroProtocollo != null) {
            setNumeroProtocollo(numeroProtocollo);
        }

        String codiceFornitore = (String) attributes.get("codiceFornitore");

        if (codiceFornitore != null) {
            setCodiceFornitore(codiceFornitore);
        }

        Double importo = (Double) attributes.get("importo");

        if (importo != null) {
            setImporto(importo);
        }

        Date dataCalcolo = (Date) attributes.get("dataCalcolo");

        if (dataCalcolo != null) {
            setDataCalcolo(dataCalcolo);
        }

        Integer stato = (Integer) attributes.get("stato");

        if (stato != null) {
            setStato(stato);
        }

        Integer idPagamento = (Integer) attributes.get("idPagamento");

        if (idPagamento != null) {
            setIdPagamento(idPagamento);
        }

        Integer annoPagamento = (Integer) attributes.get("annoPagamento");

        if (annoPagamento != null) {
            setAnnoPagamento(annoPagamento);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_spettanzeSociRemoteModel != null) {
            try {
                Class<?> clazz = _spettanzeSociRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_spettanzeSociRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getAnno() {
        return _anno;
    }

    @Override
    public void setAnno(int anno) {
        _anno = anno;

        if (_spettanzeSociRemoteModel != null) {
            try {
                Class<?> clazz = _spettanzeSociRemoteModel.getClass();

                Method method = clazz.getMethod("setAnno", int.class);

                method.invoke(_spettanzeSociRemoteModel, anno);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceAttivita() {
        return _codiceAttivita;
    }

    @Override
    public void setCodiceAttivita(String codiceAttivita) {
        _codiceAttivita = codiceAttivita;

        if (_spettanzeSociRemoteModel != null) {
            try {
                Class<?> clazz = _spettanzeSociRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceAttivita",
                        String.class);

                method.invoke(_spettanzeSociRemoteModel, codiceAttivita);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceCentro() {
        return _codiceCentro;
    }

    @Override
    public void setCodiceCentro(String codiceCentro) {
        _codiceCentro = codiceCentro;

        if (_spettanzeSociRemoteModel != null) {
            try {
                Class<?> clazz = _spettanzeSociRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceCentro", String.class);

                method.invoke(_spettanzeSociRemoteModel, codiceCentro);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getNumeroProtocollo() {
        return _numeroProtocollo;
    }

    @Override
    public void setNumeroProtocollo(int numeroProtocollo) {
        _numeroProtocollo = numeroProtocollo;

        if (_spettanzeSociRemoteModel != null) {
            try {
                Class<?> clazz = _spettanzeSociRemoteModel.getClass();

                Method method = clazz.getMethod("setNumeroProtocollo", int.class);

                method.invoke(_spettanzeSociRemoteModel, numeroProtocollo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceFornitore() {
        return _codiceFornitore;
    }

    @Override
    public void setCodiceFornitore(String codiceFornitore) {
        _codiceFornitore = codiceFornitore;

        if (_spettanzeSociRemoteModel != null) {
            try {
                Class<?> clazz = _spettanzeSociRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceFornitore",
                        String.class);

                method.invoke(_spettanzeSociRemoteModel, codiceFornitore);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getImporto() {
        return _importo;
    }

    @Override
    public void setImporto(double importo) {
        _importo = importo;

        if (_spettanzeSociRemoteModel != null) {
            try {
                Class<?> clazz = _spettanzeSociRemoteModel.getClass();

                Method method = clazz.getMethod("setImporto", double.class);

                method.invoke(_spettanzeSociRemoteModel, importo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getDataCalcolo() {
        return _dataCalcolo;
    }

    @Override
    public void setDataCalcolo(Date dataCalcolo) {
        _dataCalcolo = dataCalcolo;

        if (_spettanzeSociRemoteModel != null) {
            try {
                Class<?> clazz = _spettanzeSociRemoteModel.getClass();

                Method method = clazz.getMethod("setDataCalcolo", Date.class);

                method.invoke(_spettanzeSociRemoteModel, dataCalcolo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getStato() {
        return _stato;
    }

    @Override
    public void setStato(int stato) {
        _stato = stato;

        if (_spettanzeSociRemoteModel != null) {
            try {
                Class<?> clazz = _spettanzeSociRemoteModel.getClass();

                Method method = clazz.getMethod("setStato", int.class);

                method.invoke(_spettanzeSociRemoteModel, stato);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getIdPagamento() {
        return _idPagamento;
    }

    @Override
    public void setIdPagamento(int idPagamento) {
        _idPagamento = idPagamento;

        if (_spettanzeSociRemoteModel != null) {
            try {
                Class<?> clazz = _spettanzeSociRemoteModel.getClass();

                Method method = clazz.getMethod("setIdPagamento", int.class);

                method.invoke(_spettanzeSociRemoteModel, idPagamento);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getAnnoPagamento() {
        return _annoPagamento;
    }

    @Override
    public void setAnnoPagamento(int annoPagamento) {
        _annoPagamento = annoPagamento;

        if (_spettanzeSociRemoteModel != null) {
            try {
                Class<?> clazz = _spettanzeSociRemoteModel.getClass();

                Method method = clazz.getMethod("setAnnoPagamento", int.class);

                method.invoke(_spettanzeSociRemoteModel, annoPagamento);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getSpettanzeSociRemoteModel() {
        return _spettanzeSociRemoteModel;
    }

    public void setSpettanzeSociRemoteModel(
        BaseModel<?> spettanzeSociRemoteModel) {
        _spettanzeSociRemoteModel = spettanzeSociRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _spettanzeSociRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_spettanzeSociRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            SpettanzeSociLocalServiceUtil.addSpettanzeSoci(this);
        } else {
            SpettanzeSociLocalServiceUtil.updateSpettanzeSoci(this);
        }
    }

    @Override
    public SpettanzeSoci toEscapedModel() {
        return (SpettanzeSoci) ProxyUtil.newProxyInstance(SpettanzeSoci.class.getClassLoader(),
            new Class[] { SpettanzeSoci.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        SpettanzeSociClp clone = new SpettanzeSociClp();

        clone.setId(getId());
        clone.setAnno(getAnno());
        clone.setCodiceAttivita(getCodiceAttivita());
        clone.setCodiceCentro(getCodiceCentro());
        clone.setNumeroProtocollo(getNumeroProtocollo());
        clone.setCodiceFornitore(getCodiceFornitore());
        clone.setImporto(getImporto());
        clone.setDataCalcolo(getDataCalcolo());
        clone.setStato(getStato());
        clone.setIdPagamento(getIdPagamento());
        clone.setAnnoPagamento(getAnnoPagamento());

        return clone;
    }

    @Override
    public int compareTo(SpettanzeSoci spettanzeSoci) {
        long primaryKey = spettanzeSoci.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof SpettanzeSociClp)) {
            return false;
        }

        SpettanzeSociClp spettanzeSoci = (SpettanzeSociClp) obj;

        long primaryKey = spettanzeSoci.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(23);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", anno=");
        sb.append(getAnno());
        sb.append(", codiceAttivita=");
        sb.append(getCodiceAttivita());
        sb.append(", codiceCentro=");
        sb.append(getCodiceCentro());
        sb.append(", numeroProtocollo=");
        sb.append(getNumeroProtocollo());
        sb.append(", codiceFornitore=");
        sb.append(getCodiceFornitore());
        sb.append(", importo=");
        sb.append(getImporto());
        sb.append(", dataCalcolo=");
        sb.append(getDataCalcolo());
        sb.append(", stato=");
        sb.append(getStato());
        sb.append(", idPagamento=");
        sb.append(getIdPagamento());
        sb.append(", annoPagamento=");
        sb.append(getAnnoPagamento());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(37);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.SpettanzeSoci");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>anno</column-name><column-value><![CDATA[");
        sb.append(getAnno());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceAttivita</column-name><column-value><![CDATA[");
        sb.append(getCodiceAttivita());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceCentro</column-name><column-value><![CDATA[");
        sb.append(getCodiceCentro());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>numeroProtocollo</column-name><column-value><![CDATA[");
        sb.append(getNumeroProtocollo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceFornitore</column-name><column-value><![CDATA[");
        sb.append(getCodiceFornitore());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>importo</column-name><column-value><![CDATA[");
        sb.append(getImporto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>dataCalcolo</column-name><column-value><![CDATA[");
        sb.append(getDataCalcolo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>stato</column-name><column-value><![CDATA[");
        sb.append(getStato());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>idPagamento</column-name><column-value><![CDATA[");
        sb.append(getIdPagamento());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>annoPagamento</column-name><column-value><![CDATA[");
        sb.append(getAnnoPagamento());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
