package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.RigaServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.RigaServiceSoap
 * @generated
 */
public class RigaSoap implements Serializable {
    private long _id;
    private long _idCarrello;
    private long _idArticolo;
    private long _progressivo;
    private double _prezzo;
    private double _importo;
    private int _pianteRipiano;
    private boolean _sormontate;
    private String _stringa;
    private String _opzioni;
    private double _importoVarianti;
    private String _codiceVariante;
    private String _codiceRegionale;
    private String _passaporto;
    private int _s2;
    private int _s3;
    private int _s4;
    private double _sconto;
    private double _prezzoFornitore;
    private double _prezzoEtichetta;
    private String _ean;
    private boolean _modificato;
    private boolean _ricevuto;

    public RigaSoap() {
    }

    public static RigaSoap toSoapModel(Riga model) {
        RigaSoap soapModel = new RigaSoap();

        soapModel.setId(model.getId());
        soapModel.setIdCarrello(model.getIdCarrello());
        soapModel.setIdArticolo(model.getIdArticolo());
        soapModel.setProgressivo(model.getProgressivo());
        soapModel.setPrezzo(model.getPrezzo());
        soapModel.setImporto(model.getImporto());
        soapModel.setPianteRipiano(model.getPianteRipiano());
        soapModel.setSormontate(model.getSormontate());
        soapModel.setStringa(model.getStringa());
        soapModel.setOpzioni(model.getOpzioni());
        soapModel.setImportoVarianti(model.getImportoVarianti());
        soapModel.setCodiceVariante(model.getCodiceVariante());
        soapModel.setCodiceRegionale(model.getCodiceRegionale());
        soapModel.setPassaporto(model.getPassaporto());
        soapModel.setS2(model.getS2());
        soapModel.setS3(model.getS3());
        soapModel.setS4(model.getS4());
        soapModel.setSconto(model.getSconto());
        soapModel.setPrezzoFornitore(model.getPrezzoFornitore());
        soapModel.setPrezzoEtichetta(model.getPrezzoEtichetta());
        soapModel.setEan(model.getEan());
        soapModel.setModificato(model.getModificato());
        soapModel.setRicevuto(model.getRicevuto());

        return soapModel;
    }

    public static RigaSoap[] toSoapModels(Riga[] models) {
        RigaSoap[] soapModels = new RigaSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static RigaSoap[][] toSoapModels(Riga[][] models) {
        RigaSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new RigaSoap[models.length][models[0].length];
        } else {
            soapModels = new RigaSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static RigaSoap[] toSoapModels(List<Riga> models) {
        List<RigaSoap> soapModels = new ArrayList<RigaSoap>(models.size());

        for (Riga model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new RigaSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getIdCarrello() {
        return _idCarrello;
    }

    public void setIdCarrello(long idCarrello) {
        _idCarrello = idCarrello;
    }

    public long getIdArticolo() {
        return _idArticolo;
    }

    public void setIdArticolo(long idArticolo) {
        _idArticolo = idArticolo;
    }

    public long getProgressivo() {
        return _progressivo;
    }

    public void setProgressivo(long progressivo) {
        _progressivo = progressivo;
    }

    public double getPrezzo() {
        return _prezzo;
    }

    public void setPrezzo(double prezzo) {
        _prezzo = prezzo;
    }

    public double getImporto() {
        return _importo;
    }

    public void setImporto(double importo) {
        _importo = importo;
    }

    public int getPianteRipiano() {
        return _pianteRipiano;
    }

    public void setPianteRipiano(int pianteRipiano) {
        _pianteRipiano = pianteRipiano;
    }

    public boolean getSormontate() {
        return _sormontate;
    }

    public boolean isSormontate() {
        return _sormontate;
    }

    public void setSormontate(boolean sormontate) {
        _sormontate = sormontate;
    }

    public String getStringa() {
        return _stringa;
    }

    public void setStringa(String stringa) {
        _stringa = stringa;
    }

    public String getOpzioni() {
        return _opzioni;
    }

    public void setOpzioni(String opzioni) {
        _opzioni = opzioni;
    }

    public double getImportoVarianti() {
        return _importoVarianti;
    }

    public void setImportoVarianti(double importoVarianti) {
        _importoVarianti = importoVarianti;
    }

    public String getCodiceVariante() {
        return _codiceVariante;
    }

    public void setCodiceVariante(String codiceVariante) {
        _codiceVariante = codiceVariante;
    }

    public String getCodiceRegionale() {
        return _codiceRegionale;
    }

    public void setCodiceRegionale(String codiceRegionale) {
        _codiceRegionale = codiceRegionale;
    }

    public String getPassaporto() {
        return _passaporto;
    }

    public void setPassaporto(String passaporto) {
        _passaporto = passaporto;
    }

    public int getS2() {
        return _s2;
    }

    public void setS2(int s2) {
        _s2 = s2;
    }

    public int getS3() {
        return _s3;
    }

    public void setS3(int s3) {
        _s3 = s3;
    }

    public int getS4() {
        return _s4;
    }

    public void setS4(int s4) {
        _s4 = s4;
    }

    public double getSconto() {
        return _sconto;
    }

    public void setSconto(double sconto) {
        _sconto = sconto;
    }

    public double getPrezzoFornitore() {
        return _prezzoFornitore;
    }

    public void setPrezzoFornitore(double prezzoFornitore) {
        _prezzoFornitore = prezzoFornitore;
    }

    public double getPrezzoEtichetta() {
        return _prezzoEtichetta;
    }

    public void setPrezzoEtichetta(double prezzoEtichetta) {
        _prezzoEtichetta = prezzoEtichetta;
    }

    public String getEan() {
        return _ean;
    }

    public void setEan(String ean) {
        _ean = ean;
    }

    public boolean getModificato() {
        return _modificato;
    }

    public boolean isModificato() {
        return _modificato;
    }

    public void setModificato(boolean modificato) {
        _modificato = modificato;
    }

    public boolean getRicevuto() {
        return _ricevuto;
    }

    public boolean isRicevuto() {
        return _ricevuto;
    }

    public void setRicevuto(boolean ricevuto) {
        _ricevuto = ricevuto;
    }
}
