package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the ScadenzePartite service. Represents a row in the &quot;ScadenzePartite&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see ScadenzePartiteModel
 * @see it.bysoftware.ct.model.impl.ScadenzePartiteImpl
 * @see it.bysoftware.ct.model.impl.ScadenzePartiteModelImpl
 * @generated
 */
public interface ScadenzePartite extends ScadenzePartiteModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.ScadenzePartiteImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
