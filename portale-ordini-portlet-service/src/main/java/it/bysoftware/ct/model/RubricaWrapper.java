package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Rubrica}.
 * </p>
 *
 * @author Mario Torrisi
 * @see Rubrica
 * @generated
 */
public class RubricaWrapper implements Rubrica, ModelWrapper<Rubrica> {
    private Rubrica _rubrica;

    public RubricaWrapper(Rubrica rubrica) {
        _rubrica = rubrica;
    }

    @Override
    public Class<?> getModelClass() {
        return Rubrica.class;
    }

    @Override
    public String getModelClassName() {
        return Rubrica.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceSoggetto", getCodiceSoggetto());
        attributes.put("numeroVoce", getNumeroVoce());
        attributes.put("descrizione", getDescrizione());
        attributes.put("telefono", getTelefono());
        attributes.put("cellulare", getCellulare());
        attributes.put("fax", getFax());
        attributes.put("eMailTo", getEMailTo());
        attributes.put("eMailCc", getEMailCc());
        attributes.put("eMailCcn", getEMailCcn());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceSoggetto = (String) attributes.get("codiceSoggetto");

        if (codiceSoggetto != null) {
            setCodiceSoggetto(codiceSoggetto);
        }

        Integer numeroVoce = (Integer) attributes.get("numeroVoce");

        if (numeroVoce != null) {
            setNumeroVoce(numeroVoce);
        }

        String descrizione = (String) attributes.get("descrizione");

        if (descrizione != null) {
            setDescrizione(descrizione);
        }

        String telefono = (String) attributes.get("telefono");

        if (telefono != null) {
            setTelefono(telefono);
        }

        String cellulare = (String) attributes.get("cellulare");

        if (cellulare != null) {
            setCellulare(cellulare);
        }

        String fax = (String) attributes.get("fax");

        if (fax != null) {
            setFax(fax);
        }

        String eMailTo = (String) attributes.get("eMailTo");

        if (eMailTo != null) {
            setEMailTo(eMailTo);
        }

        String eMailCc = (String) attributes.get("eMailCc");

        if (eMailCc != null) {
            setEMailCc(eMailCc);
        }

        String eMailCcn = (String) attributes.get("eMailCcn");

        if (eMailCcn != null) {
            setEMailCcn(eMailCcn);
        }
    }

    /**
    * Returns the primary key of this rubrica.
    *
    * @return the primary key of this rubrica
    */
    @Override
    public it.bysoftware.ct.service.persistence.RubricaPK getPrimaryKey() {
        return _rubrica.getPrimaryKey();
    }

    /**
    * Sets the primary key of this rubrica.
    *
    * @param primaryKey the primary key of this rubrica
    */
    @Override
    public void setPrimaryKey(
        it.bysoftware.ct.service.persistence.RubricaPK primaryKey) {
        _rubrica.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the codice soggetto of this rubrica.
    *
    * @return the codice soggetto of this rubrica
    */
    @Override
    public java.lang.String getCodiceSoggetto() {
        return _rubrica.getCodiceSoggetto();
    }

    /**
    * Sets the codice soggetto of this rubrica.
    *
    * @param codiceSoggetto the codice soggetto of this rubrica
    */
    @Override
    public void setCodiceSoggetto(java.lang.String codiceSoggetto) {
        _rubrica.setCodiceSoggetto(codiceSoggetto);
    }

    /**
    * Returns the numero voce of this rubrica.
    *
    * @return the numero voce of this rubrica
    */
    @Override
    public int getNumeroVoce() {
        return _rubrica.getNumeroVoce();
    }

    /**
    * Sets the numero voce of this rubrica.
    *
    * @param numeroVoce the numero voce of this rubrica
    */
    @Override
    public void setNumeroVoce(int numeroVoce) {
        _rubrica.setNumeroVoce(numeroVoce);
    }

    /**
    * Returns the descrizione of this rubrica.
    *
    * @return the descrizione of this rubrica
    */
    @Override
    public java.lang.String getDescrizione() {
        return _rubrica.getDescrizione();
    }

    /**
    * Sets the descrizione of this rubrica.
    *
    * @param descrizione the descrizione of this rubrica
    */
    @Override
    public void setDescrizione(java.lang.String descrizione) {
        _rubrica.setDescrizione(descrizione);
    }

    /**
    * Returns the telefono of this rubrica.
    *
    * @return the telefono of this rubrica
    */
    @Override
    public java.lang.String getTelefono() {
        return _rubrica.getTelefono();
    }

    /**
    * Sets the telefono of this rubrica.
    *
    * @param telefono the telefono of this rubrica
    */
    @Override
    public void setTelefono(java.lang.String telefono) {
        _rubrica.setTelefono(telefono);
    }

    /**
    * Returns the cellulare of this rubrica.
    *
    * @return the cellulare of this rubrica
    */
    @Override
    public java.lang.String getCellulare() {
        return _rubrica.getCellulare();
    }

    /**
    * Sets the cellulare of this rubrica.
    *
    * @param cellulare the cellulare of this rubrica
    */
    @Override
    public void setCellulare(java.lang.String cellulare) {
        _rubrica.setCellulare(cellulare);
    }

    /**
    * Returns the fax of this rubrica.
    *
    * @return the fax of this rubrica
    */
    @Override
    public java.lang.String getFax() {
        return _rubrica.getFax();
    }

    /**
    * Sets the fax of this rubrica.
    *
    * @param fax the fax of this rubrica
    */
    @Override
    public void setFax(java.lang.String fax) {
        _rubrica.setFax(fax);
    }

    /**
    * Returns the e mail to of this rubrica.
    *
    * @return the e mail to of this rubrica
    */
    @Override
    public java.lang.String getEMailTo() {
        return _rubrica.getEMailTo();
    }

    /**
    * Sets the e mail to of this rubrica.
    *
    * @param eMailTo the e mail to of this rubrica
    */
    @Override
    public void setEMailTo(java.lang.String eMailTo) {
        _rubrica.setEMailTo(eMailTo);
    }

    /**
    * Returns the e mail cc of this rubrica.
    *
    * @return the e mail cc of this rubrica
    */
    @Override
    public java.lang.String getEMailCc() {
        return _rubrica.getEMailCc();
    }

    /**
    * Sets the e mail cc of this rubrica.
    *
    * @param eMailCc the e mail cc of this rubrica
    */
    @Override
    public void setEMailCc(java.lang.String eMailCc) {
        _rubrica.setEMailCc(eMailCc);
    }

    /**
    * Returns the e mail ccn of this rubrica.
    *
    * @return the e mail ccn of this rubrica
    */
    @Override
    public java.lang.String getEMailCcn() {
        return _rubrica.getEMailCcn();
    }

    /**
    * Sets the e mail ccn of this rubrica.
    *
    * @param eMailCcn the e mail ccn of this rubrica
    */
    @Override
    public void setEMailCcn(java.lang.String eMailCcn) {
        _rubrica.setEMailCcn(eMailCcn);
    }

    @Override
    public boolean isNew() {
        return _rubrica.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _rubrica.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _rubrica.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _rubrica.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _rubrica.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _rubrica.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _rubrica.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _rubrica.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _rubrica.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _rubrica.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _rubrica.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new RubricaWrapper((Rubrica) _rubrica.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.Rubrica rubrica) {
        return _rubrica.compareTo(rubrica);
    }

    @Override
    public int hashCode() {
        return _rubrica.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.Rubrica> toCacheModel() {
        return _rubrica.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.Rubrica toEscapedModel() {
        return new RubricaWrapper(_rubrica.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.Rubrica toUnescapedModel() {
        return new RubricaWrapper(_rubrica.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _rubrica.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _rubrica.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _rubrica.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof RubricaWrapper)) {
            return false;
        }

        RubricaWrapper rubricaWrapper = (RubricaWrapper) obj;

        if (Validator.equals(_rubrica, rubricaWrapper._rubrica)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Rubrica getWrappedRubrica() {
        return _rubrica;
    }

    @Override
    public Rubrica getWrappedModel() {
        return _rubrica;
    }

    @Override
    public void resetOriginalValues() {
        _rubrica.resetOriginalValues();
    }
}
