package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.VariantiPiantaServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.VariantiPiantaServiceSoap
 * @generated
 */
public class VariantiPiantaSoap implements Serializable {
    private long _id;
    private long _idVariante;
    private long _idPianta;

    public VariantiPiantaSoap() {
    }

    public static VariantiPiantaSoap toSoapModel(VariantiPianta model) {
        VariantiPiantaSoap soapModel = new VariantiPiantaSoap();

        soapModel.setId(model.getId());
        soapModel.setIdVariante(model.getIdVariante());
        soapModel.setIdPianta(model.getIdPianta());

        return soapModel;
    }

    public static VariantiPiantaSoap[] toSoapModels(VariantiPianta[] models) {
        VariantiPiantaSoap[] soapModels = new VariantiPiantaSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static VariantiPiantaSoap[][] toSoapModels(VariantiPianta[][] models) {
        VariantiPiantaSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new VariantiPiantaSoap[models.length][models[0].length];
        } else {
            soapModels = new VariantiPiantaSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static VariantiPiantaSoap[] toSoapModels(List<VariantiPianta> models) {
        List<VariantiPiantaSoap> soapModels = new ArrayList<VariantiPiantaSoap>(models.size());

        for (VariantiPianta model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new VariantiPiantaSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getIdVariante() {
        return _idVariante;
    }

    public void setIdVariante(long idVariante) {
        _idVariante = idVariante;
    }

    public long getIdPianta() {
        return _idPianta;
    }

    public void setIdPianta(long idPianta) {
        _idPianta = idPianta;
    }
}
