package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Ordine service. Represents a row in the &quot;_ordini&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see OrdineModel
 * @see it.bysoftware.ct.model.impl.OrdineImpl
 * @see it.bysoftware.ct.model.impl.OrdineModelImpl
 * @generated
 */
public interface Ordine extends OrdineModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.OrdineImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
