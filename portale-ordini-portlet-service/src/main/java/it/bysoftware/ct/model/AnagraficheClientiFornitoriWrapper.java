package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link AnagraficheClientiFornitori}.
 * </p>
 *
 * @author Mario Torrisi
 * @see AnagraficheClientiFornitori
 * @generated
 */
public class AnagraficheClientiFornitoriWrapper
    implements AnagraficheClientiFornitori,
        ModelWrapper<AnagraficheClientiFornitori> {
    private AnagraficheClientiFornitori _anagraficheClientiFornitori;

    public AnagraficheClientiFornitoriWrapper(
        AnagraficheClientiFornitori anagraficheClientiFornitori) {
        _anagraficheClientiFornitori = anagraficheClientiFornitori;
    }

    @Override
    public Class<?> getModelClass() {
        return AnagraficheClientiFornitori.class;
    }

    @Override
    public String getModelClassName() {
        return AnagraficheClientiFornitori.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("attivoEC", getAttivoEC());
        attributes.put("CAP", getCAP());
        attributes.put("codiceAnagrafica", getCodiceAnagrafica());
        attributes.put("codiceFiscale", getCodiceFiscale());
        attributes.put("codiceMnemonico", getCodiceMnemonico());
        attributes.put("comune", getComune());
        attributes.put("indirizzo", getIndirizzo());
        attributes.put("note", getNote());
        attributes.put("partitaIVA", getPartitaIVA());
        attributes.put("ragioneSociale1", getRagioneSociale1());
        attributes.put("ragioneSociale2", getRagioneSociale2());
        attributes.put("siglaProvincia", getSiglaProvincia());
        attributes.put("siglaStato", getSiglaStato());
        attributes.put("personaFisica", getPersonaFisica());
        attributes.put("tipoSoggetto", getTipoSoggetto());
        attributes.put("tipoSollecito", getTipoSollecito());
        attributes.put("codiceAzienda", getCodiceAzienda());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Boolean attivoEC = (Boolean) attributes.get("attivoEC");

        if (attivoEC != null) {
            setAttivoEC(attivoEC);
        }

        String CAP = (String) attributes.get("CAP");

        if (CAP != null) {
            setCAP(CAP);
        }

        String codiceAnagrafica = (String) attributes.get("codiceAnagrafica");

        if (codiceAnagrafica != null) {
            setCodiceAnagrafica(codiceAnagrafica);
        }

        String codiceFiscale = (String) attributes.get("codiceFiscale");

        if (codiceFiscale != null) {
            setCodiceFiscale(codiceFiscale);
        }

        String codiceMnemonico = (String) attributes.get("codiceMnemonico");

        if (codiceMnemonico != null) {
            setCodiceMnemonico(codiceMnemonico);
        }

        String comune = (String) attributes.get("comune");

        if (comune != null) {
            setComune(comune);
        }

        String indirizzo = (String) attributes.get("indirizzo");

        if (indirizzo != null) {
            setIndirizzo(indirizzo);
        }

        String note = (String) attributes.get("note");

        if (note != null) {
            setNote(note);
        }

        String partitaIVA = (String) attributes.get("partitaIVA");

        if (partitaIVA != null) {
            setPartitaIVA(partitaIVA);
        }

        String ragioneSociale1 = (String) attributes.get("ragioneSociale1");

        if (ragioneSociale1 != null) {
            setRagioneSociale1(ragioneSociale1);
        }

        String ragioneSociale2 = (String) attributes.get("ragioneSociale2");

        if (ragioneSociale2 != null) {
            setRagioneSociale2(ragioneSociale2);
        }

        String siglaProvincia = (String) attributes.get("siglaProvincia");

        if (siglaProvincia != null) {
            setSiglaProvincia(siglaProvincia);
        }

        String siglaStato = (String) attributes.get("siglaStato");

        if (siglaStato != null) {
            setSiglaStato(siglaStato);
        }

        Boolean personaFisica = (Boolean) attributes.get("personaFisica");

        if (personaFisica != null) {
            setPersonaFisica(personaFisica);
        }

        String tipoSoggetto = (String) attributes.get("tipoSoggetto");

        if (tipoSoggetto != null) {
            setTipoSoggetto(tipoSoggetto);
        }

        Integer tipoSollecito = (Integer) attributes.get("tipoSollecito");

        if (tipoSollecito != null) {
            setTipoSollecito(tipoSollecito);
        }

        Integer codiceAzienda = (Integer) attributes.get("codiceAzienda");

        if (codiceAzienda != null) {
            setCodiceAzienda(codiceAzienda);
        }
    }

    /**
    * Returns the primary key of this anagrafiche clienti fornitori.
    *
    * @return the primary key of this anagrafiche clienti fornitori
    */
    @Override
    public java.lang.String getPrimaryKey() {
        return _anagraficheClientiFornitori.getPrimaryKey();
    }

    /**
    * Sets the primary key of this anagrafiche clienti fornitori.
    *
    * @param primaryKey the primary key of this anagrafiche clienti fornitori
    */
    @Override
    public void setPrimaryKey(java.lang.String primaryKey) {
        _anagraficheClientiFornitori.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the attivo e c of this anagrafiche clienti fornitori.
    *
    * @return the attivo e c of this anagrafiche clienti fornitori
    */
    @Override
    public boolean getAttivoEC() {
        return _anagraficheClientiFornitori.getAttivoEC();
    }

    /**
    * Returns <code>true</code> if this anagrafiche clienti fornitori is attivo e c.
    *
    * @return <code>true</code> if this anagrafiche clienti fornitori is attivo e c; <code>false</code> otherwise
    */
    @Override
    public boolean isAttivoEC() {
        return _anagraficheClientiFornitori.isAttivoEC();
    }

    /**
    * Sets whether this anagrafiche clienti fornitori is attivo e c.
    *
    * @param attivoEC the attivo e c of this anagrafiche clienti fornitori
    */
    @Override
    public void setAttivoEC(boolean attivoEC) {
        _anagraficheClientiFornitori.setAttivoEC(attivoEC);
    }

    /**
    * Returns the c a p of this anagrafiche clienti fornitori.
    *
    * @return the c a p of this anagrafiche clienti fornitori
    */
    @Override
    public java.lang.String getCAP() {
        return _anagraficheClientiFornitori.getCAP();
    }

    /**
    * Sets the c a p of this anagrafiche clienti fornitori.
    *
    * @param CAP the c a p of this anagrafiche clienti fornitori
    */
    @Override
    public void setCAP(java.lang.String CAP) {
        _anagraficheClientiFornitori.setCAP(CAP);
    }

    /**
    * Returns the codice anagrafica of this anagrafiche clienti fornitori.
    *
    * @return the codice anagrafica of this anagrafiche clienti fornitori
    */
    @Override
    public java.lang.String getCodiceAnagrafica() {
        return _anagraficheClientiFornitori.getCodiceAnagrafica();
    }

    /**
    * Sets the codice anagrafica of this anagrafiche clienti fornitori.
    *
    * @param codiceAnagrafica the codice anagrafica of this anagrafiche clienti fornitori
    */
    @Override
    public void setCodiceAnagrafica(java.lang.String codiceAnagrafica) {
        _anagraficheClientiFornitori.setCodiceAnagrafica(codiceAnagrafica);
    }

    /**
    * Returns the codice fiscale of this anagrafiche clienti fornitori.
    *
    * @return the codice fiscale of this anagrafiche clienti fornitori
    */
    @Override
    public java.lang.String getCodiceFiscale() {
        return _anagraficheClientiFornitori.getCodiceFiscale();
    }

    /**
    * Sets the codice fiscale of this anagrafiche clienti fornitori.
    *
    * @param codiceFiscale the codice fiscale of this anagrafiche clienti fornitori
    */
    @Override
    public void setCodiceFiscale(java.lang.String codiceFiscale) {
        _anagraficheClientiFornitori.setCodiceFiscale(codiceFiscale);
    }

    /**
    * Returns the codice mnemonico of this anagrafiche clienti fornitori.
    *
    * @return the codice mnemonico of this anagrafiche clienti fornitori
    */
    @Override
    public java.lang.String getCodiceMnemonico() {
        return _anagraficheClientiFornitori.getCodiceMnemonico();
    }

    /**
    * Sets the codice mnemonico of this anagrafiche clienti fornitori.
    *
    * @param codiceMnemonico the codice mnemonico of this anagrafiche clienti fornitori
    */
    @Override
    public void setCodiceMnemonico(java.lang.String codiceMnemonico) {
        _anagraficheClientiFornitori.setCodiceMnemonico(codiceMnemonico);
    }

    /**
    * Returns the comune of this anagrafiche clienti fornitori.
    *
    * @return the comune of this anagrafiche clienti fornitori
    */
    @Override
    public java.lang.String getComune() {
        return _anagraficheClientiFornitori.getComune();
    }

    /**
    * Sets the comune of this anagrafiche clienti fornitori.
    *
    * @param comune the comune of this anagrafiche clienti fornitori
    */
    @Override
    public void setComune(java.lang.String comune) {
        _anagraficheClientiFornitori.setComune(comune);
    }

    /**
    * Returns the indirizzo of this anagrafiche clienti fornitori.
    *
    * @return the indirizzo of this anagrafiche clienti fornitori
    */
    @Override
    public java.lang.String getIndirizzo() {
        return _anagraficheClientiFornitori.getIndirizzo();
    }

    /**
    * Sets the indirizzo of this anagrafiche clienti fornitori.
    *
    * @param indirizzo the indirizzo of this anagrafiche clienti fornitori
    */
    @Override
    public void setIndirizzo(java.lang.String indirizzo) {
        _anagraficheClientiFornitori.setIndirizzo(indirizzo);
    }

    /**
    * Returns the note of this anagrafiche clienti fornitori.
    *
    * @return the note of this anagrafiche clienti fornitori
    */
    @Override
    public java.lang.String getNote() {
        return _anagraficheClientiFornitori.getNote();
    }

    /**
    * Sets the note of this anagrafiche clienti fornitori.
    *
    * @param note the note of this anagrafiche clienti fornitori
    */
    @Override
    public void setNote(java.lang.String note) {
        _anagraficheClientiFornitori.setNote(note);
    }

    /**
    * Returns the partita i v a of this anagrafiche clienti fornitori.
    *
    * @return the partita i v a of this anagrafiche clienti fornitori
    */
    @Override
    public java.lang.String getPartitaIVA() {
        return _anagraficheClientiFornitori.getPartitaIVA();
    }

    /**
    * Sets the partita i v a of this anagrafiche clienti fornitori.
    *
    * @param partitaIVA the partita i v a of this anagrafiche clienti fornitori
    */
    @Override
    public void setPartitaIVA(java.lang.String partitaIVA) {
        _anagraficheClientiFornitori.setPartitaIVA(partitaIVA);
    }

    /**
    * Returns the ragione sociale1 of this anagrafiche clienti fornitori.
    *
    * @return the ragione sociale1 of this anagrafiche clienti fornitori
    */
    @Override
    public java.lang.String getRagioneSociale1() {
        return _anagraficheClientiFornitori.getRagioneSociale1();
    }

    /**
    * Sets the ragione sociale1 of this anagrafiche clienti fornitori.
    *
    * @param ragioneSociale1 the ragione sociale1 of this anagrafiche clienti fornitori
    */
    @Override
    public void setRagioneSociale1(java.lang.String ragioneSociale1) {
        _anagraficheClientiFornitori.setRagioneSociale1(ragioneSociale1);
    }

    /**
    * Returns the ragione sociale2 of this anagrafiche clienti fornitori.
    *
    * @return the ragione sociale2 of this anagrafiche clienti fornitori
    */
    @Override
    public java.lang.String getRagioneSociale2() {
        return _anagraficheClientiFornitori.getRagioneSociale2();
    }

    /**
    * Sets the ragione sociale2 of this anagrafiche clienti fornitori.
    *
    * @param ragioneSociale2 the ragione sociale2 of this anagrafiche clienti fornitori
    */
    @Override
    public void setRagioneSociale2(java.lang.String ragioneSociale2) {
        _anagraficheClientiFornitori.setRagioneSociale2(ragioneSociale2);
    }

    /**
    * Returns the sigla provincia of this anagrafiche clienti fornitori.
    *
    * @return the sigla provincia of this anagrafiche clienti fornitori
    */
    @Override
    public java.lang.String getSiglaProvincia() {
        return _anagraficheClientiFornitori.getSiglaProvincia();
    }

    /**
    * Sets the sigla provincia of this anagrafiche clienti fornitori.
    *
    * @param siglaProvincia the sigla provincia of this anagrafiche clienti fornitori
    */
    @Override
    public void setSiglaProvincia(java.lang.String siglaProvincia) {
        _anagraficheClientiFornitori.setSiglaProvincia(siglaProvincia);
    }

    /**
    * Returns the sigla stato of this anagrafiche clienti fornitori.
    *
    * @return the sigla stato of this anagrafiche clienti fornitori
    */
    @Override
    public java.lang.String getSiglaStato() {
        return _anagraficheClientiFornitori.getSiglaStato();
    }

    /**
    * Sets the sigla stato of this anagrafiche clienti fornitori.
    *
    * @param siglaStato the sigla stato of this anagrafiche clienti fornitori
    */
    @Override
    public void setSiglaStato(java.lang.String siglaStato) {
        _anagraficheClientiFornitori.setSiglaStato(siglaStato);
    }

    /**
    * Returns the persona fisica of this anagrafiche clienti fornitori.
    *
    * @return the persona fisica of this anagrafiche clienti fornitori
    */
    @Override
    public boolean getPersonaFisica() {
        return _anagraficheClientiFornitori.getPersonaFisica();
    }

    /**
    * Returns <code>true</code> if this anagrafiche clienti fornitori is persona fisica.
    *
    * @return <code>true</code> if this anagrafiche clienti fornitori is persona fisica; <code>false</code> otherwise
    */
    @Override
    public boolean isPersonaFisica() {
        return _anagraficheClientiFornitori.isPersonaFisica();
    }

    /**
    * Sets whether this anagrafiche clienti fornitori is persona fisica.
    *
    * @param personaFisica the persona fisica of this anagrafiche clienti fornitori
    */
    @Override
    public void setPersonaFisica(boolean personaFisica) {
        _anagraficheClientiFornitori.setPersonaFisica(personaFisica);
    }

    /**
    * Returns the tipo soggetto of this anagrafiche clienti fornitori.
    *
    * @return the tipo soggetto of this anagrafiche clienti fornitori
    */
    @Override
    public java.lang.String getTipoSoggetto() {
        return _anagraficheClientiFornitori.getTipoSoggetto();
    }

    /**
    * Sets the tipo soggetto of this anagrafiche clienti fornitori.
    *
    * @param tipoSoggetto the tipo soggetto of this anagrafiche clienti fornitori
    */
    @Override
    public void setTipoSoggetto(java.lang.String tipoSoggetto) {
        _anagraficheClientiFornitori.setTipoSoggetto(tipoSoggetto);
    }

    /**
    * Returns the tipo sollecito of this anagrafiche clienti fornitori.
    *
    * @return the tipo sollecito of this anagrafiche clienti fornitori
    */
    @Override
    public int getTipoSollecito() {
        return _anagraficheClientiFornitori.getTipoSollecito();
    }

    /**
    * Sets the tipo sollecito of this anagrafiche clienti fornitori.
    *
    * @param tipoSollecito the tipo sollecito of this anagrafiche clienti fornitori
    */
    @Override
    public void setTipoSollecito(int tipoSollecito) {
        _anagraficheClientiFornitori.setTipoSollecito(tipoSollecito);
    }

    /**
    * Returns the codice azienda of this anagrafiche clienti fornitori.
    *
    * @return the codice azienda of this anagrafiche clienti fornitori
    */
    @Override
    public int getCodiceAzienda() {
        return _anagraficheClientiFornitori.getCodiceAzienda();
    }

    /**
    * Sets the codice azienda of this anagrafiche clienti fornitori.
    *
    * @param codiceAzienda the codice azienda of this anagrafiche clienti fornitori
    */
    @Override
    public void setCodiceAzienda(int codiceAzienda) {
        _anagraficheClientiFornitori.setCodiceAzienda(codiceAzienda);
    }

    @Override
    public boolean isNew() {
        return _anagraficheClientiFornitori.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _anagraficheClientiFornitori.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _anagraficheClientiFornitori.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _anagraficheClientiFornitori.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _anagraficheClientiFornitori.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _anagraficheClientiFornitori.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _anagraficheClientiFornitori.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _anagraficheClientiFornitori.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _anagraficheClientiFornitori.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _anagraficheClientiFornitori.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _anagraficheClientiFornitori.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new AnagraficheClientiFornitoriWrapper((AnagraficheClientiFornitori) _anagraficheClientiFornitori.clone());
    }

    @Override
    public int compareTo(
        it.bysoftware.ct.model.AnagraficheClientiFornitori anagraficheClientiFornitori) {
        return _anagraficheClientiFornitori.compareTo(anagraficheClientiFornitori);
    }

    @Override
    public int hashCode() {
        return _anagraficheClientiFornitori.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.AnagraficheClientiFornitori> toCacheModel() {
        return _anagraficheClientiFornitori.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.AnagraficheClientiFornitori toEscapedModel() {
        return new AnagraficheClientiFornitoriWrapper(_anagraficheClientiFornitori.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.AnagraficheClientiFornitori toUnescapedModel() {
        return new AnagraficheClientiFornitoriWrapper(_anagraficheClientiFornitori.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _anagraficheClientiFornitori.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _anagraficheClientiFornitori.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _anagraficheClientiFornitori.persist();
    }

    @Override
    public java.lang.String getPreferredEmail(java.lang.String code) {
        return _anagraficheClientiFornitori.getPreferredEmail(code);
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> getMovimentiVuoti() {
        return _anagraficheClientiFornitori.getMovimentiVuoti();
    }

    @Override
    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> getMovimentiVuoti(
        java.util.Date from, java.util.Date to, java.lang.String code,
        int start, int end) {
        return _anagraficheClientiFornitori.getMovimentiVuoti(from, to, code,
            start, end);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof AnagraficheClientiFornitoriWrapper)) {
            return false;
        }

        AnagraficheClientiFornitoriWrapper anagraficheClientiFornitoriWrapper = (AnagraficheClientiFornitoriWrapper) obj;

        if (Validator.equals(_anagraficheClientiFornitori,
                    anagraficheClientiFornitoriWrapper._anagraficheClientiFornitori)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public AnagraficheClientiFornitori getWrappedAnagraficheClientiFornitori() {
        return _anagraficheClientiFornitori;
    }

    @Override
    public AnagraficheClientiFornitori getWrappedModel() {
        return _anagraficheClientiFornitori;
    }

    @Override
    public void resetOriginalValues() {
        _anagraficheClientiFornitori.resetOriginalValues();
    }
}
