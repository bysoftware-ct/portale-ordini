package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the AnagraficheClientiFornitori service. Represents a row in the &quot;AnagraficheClientiFornitori&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see AnagraficheClientiFornitoriModel
 * @see it.bysoftware.ct.model.impl.AnagraficheClientiFornitoriImpl
 * @see it.bysoftware.ct.model.impl.AnagraficheClientiFornitoriModelImpl
 * @generated
 */
public interface AnagraficheClientiFornitori
    extends AnagraficheClientiFornitoriModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.AnagraficheClientiFornitoriImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public java.lang.String getPreferredEmail(java.lang.String code);

    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> getMovimentiVuoti();

    public java.util.List<it.bysoftware.ct.model.MovimentoVuoto> getMovimentiVuoti(
        java.util.Date from, java.util.Date to, java.lang.String code,
        int start, int end);
}
