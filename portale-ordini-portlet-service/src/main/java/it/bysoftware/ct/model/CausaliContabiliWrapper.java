package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CausaliContabili}.
 * </p>
 *
 * @author Mario Torrisi
 * @see CausaliContabili
 * @generated
 */
public class CausaliContabiliWrapper implements CausaliContabili,
    ModelWrapper<CausaliContabili> {
    private CausaliContabili _causaliContabili;

    public CausaliContabiliWrapper(CausaliContabili causaliContabili) {
        _causaliContabili = causaliContabili;
    }

    @Override
    public Class<?> getModelClass() {
        return CausaliContabili.class;
    }

    @Override
    public String getModelClassName() {
        return CausaliContabili.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceCausaleCont", getCodiceCausaleCont());
        attributes.put("codiceCausaleEC", getCodiceCausaleEC());
        attributes.put("descrizioneCausaleCont", getDescrizioneCausaleCont());
        attributes.put("testDareAvere", getTestDareAvere());
        attributes.put("tipoSoggettoMovimentato", getTipoSoggettoMovimentato());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceCausaleCont = (String) attributes.get("codiceCausaleCont");

        if (codiceCausaleCont != null) {
            setCodiceCausaleCont(codiceCausaleCont);
        }

        String codiceCausaleEC = (String) attributes.get("codiceCausaleEC");

        if (codiceCausaleEC != null) {
            setCodiceCausaleEC(codiceCausaleEC);
        }

        String descrizioneCausaleCont = (String) attributes.get(
                "descrizioneCausaleCont");

        if (descrizioneCausaleCont != null) {
            setDescrizioneCausaleCont(descrizioneCausaleCont);
        }

        Integer testDareAvere = (Integer) attributes.get("testDareAvere");

        if (testDareAvere != null) {
            setTestDareAvere(testDareAvere);
        }

        Integer tipoSoggettoMovimentato = (Integer) attributes.get(
                "tipoSoggettoMovimentato");

        if (tipoSoggettoMovimentato != null) {
            setTipoSoggettoMovimentato(tipoSoggettoMovimentato);
        }
    }

    /**
    * Returns the primary key of this causali contabili.
    *
    * @return the primary key of this causali contabili
    */
    @Override
    public java.lang.String getPrimaryKey() {
        return _causaliContabili.getPrimaryKey();
    }

    /**
    * Sets the primary key of this causali contabili.
    *
    * @param primaryKey the primary key of this causali contabili
    */
    @Override
    public void setPrimaryKey(java.lang.String primaryKey) {
        _causaliContabili.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the codice causale cont of this causali contabili.
    *
    * @return the codice causale cont of this causali contabili
    */
    @Override
    public java.lang.String getCodiceCausaleCont() {
        return _causaliContabili.getCodiceCausaleCont();
    }

    /**
    * Sets the codice causale cont of this causali contabili.
    *
    * @param codiceCausaleCont the codice causale cont of this causali contabili
    */
    @Override
    public void setCodiceCausaleCont(java.lang.String codiceCausaleCont) {
        _causaliContabili.setCodiceCausaleCont(codiceCausaleCont);
    }

    /**
    * Returns the codice causale e c of this causali contabili.
    *
    * @return the codice causale e c of this causali contabili
    */
    @Override
    public java.lang.String getCodiceCausaleEC() {
        return _causaliContabili.getCodiceCausaleEC();
    }

    /**
    * Sets the codice causale e c of this causali contabili.
    *
    * @param codiceCausaleEC the codice causale e c of this causali contabili
    */
    @Override
    public void setCodiceCausaleEC(java.lang.String codiceCausaleEC) {
        _causaliContabili.setCodiceCausaleEC(codiceCausaleEC);
    }

    /**
    * Returns the descrizione causale cont of this causali contabili.
    *
    * @return the descrizione causale cont of this causali contabili
    */
    @Override
    public java.lang.String getDescrizioneCausaleCont() {
        return _causaliContabili.getDescrizioneCausaleCont();
    }

    /**
    * Sets the descrizione causale cont of this causali contabili.
    *
    * @param descrizioneCausaleCont the descrizione causale cont of this causali contabili
    */
    @Override
    public void setDescrizioneCausaleCont(
        java.lang.String descrizioneCausaleCont) {
        _causaliContabili.setDescrizioneCausaleCont(descrizioneCausaleCont);
    }

    /**
    * Returns the test dare avere of this causali contabili.
    *
    * @return the test dare avere of this causali contabili
    */
    @Override
    public int getTestDareAvere() {
        return _causaliContabili.getTestDareAvere();
    }

    /**
    * Sets the test dare avere of this causali contabili.
    *
    * @param testDareAvere the test dare avere of this causali contabili
    */
    @Override
    public void setTestDareAvere(int testDareAvere) {
        _causaliContabili.setTestDareAvere(testDareAvere);
    }

    /**
    * Returns the tipo soggetto movimentato of this causali contabili.
    *
    * @return the tipo soggetto movimentato of this causali contabili
    */
    @Override
    public int getTipoSoggettoMovimentato() {
        return _causaliContabili.getTipoSoggettoMovimentato();
    }

    /**
    * Sets the tipo soggetto movimentato of this causali contabili.
    *
    * @param tipoSoggettoMovimentato the tipo soggetto movimentato of this causali contabili
    */
    @Override
    public void setTipoSoggettoMovimentato(int tipoSoggettoMovimentato) {
        _causaliContabili.setTipoSoggettoMovimentato(tipoSoggettoMovimentato);
    }

    @Override
    public boolean isNew() {
        return _causaliContabili.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _causaliContabili.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _causaliContabili.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _causaliContabili.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _causaliContabili.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _causaliContabili.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _causaliContabili.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _causaliContabili.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _causaliContabili.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _causaliContabili.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _causaliContabili.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new CausaliContabiliWrapper((CausaliContabili) _causaliContabili.clone());
    }

    @Override
    public int compareTo(
        it.bysoftware.ct.model.CausaliContabili causaliContabili) {
        return _causaliContabili.compareTo(causaliContabili);
    }

    @Override
    public int hashCode() {
        return _causaliContabili.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.CausaliContabili> toCacheModel() {
        return _causaliContabili.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.CausaliContabili toEscapedModel() {
        return new CausaliContabiliWrapper(_causaliContabili.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.CausaliContabili toUnescapedModel() {
        return new CausaliContabiliWrapper(_causaliContabili.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _causaliContabili.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _causaliContabili.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _causaliContabili.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CausaliContabiliWrapper)) {
            return false;
        }

        CausaliContabiliWrapper causaliContabiliWrapper = (CausaliContabiliWrapper) obj;

        if (Validator.equals(_causaliContabili,
                    causaliContabiliWrapper._causaliContabili)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public CausaliContabili getWrappedCausaliContabili() {
        return _causaliContabili;
    }

    @Override
    public CausaliContabili getWrappedModel() {
        return _causaliContabili;
    }

    @Override
    public void resetOriginalValues() {
        _causaliContabili.resetOriginalValues();
    }
}
