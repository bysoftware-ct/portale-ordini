package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the CausaliEstrattoConto service. Represents a row in the &quot;CausaliEstrattoConto&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see CausaliEstrattoContoModel
 * @see it.bysoftware.ct.model.impl.CausaliEstrattoContoImpl
 * @see it.bysoftware.ct.model.impl.CausaliEstrattoContoModelImpl
 * @generated
 */
public interface CausaliEstrattoConto extends CausaliEstrattoContoModel,
    PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.CausaliEstrattoContoImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
