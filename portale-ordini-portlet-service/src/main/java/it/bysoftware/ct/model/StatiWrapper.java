package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Stati}.
 * </p>
 *
 * @author Mario Torrisi
 * @see Stati
 * @generated
 */
public class StatiWrapper implements Stati, ModelWrapper<Stati> {
    private Stati _stati;

    public StatiWrapper(Stati stati) {
        _stati = stati;
    }

    @Override
    public Class<?> getModelClass() {
        return Stati.class;
    }

    @Override
    public String getModelClassName() {
        return Stati.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("black", getBlack());
        attributes.put("codiceStato", getCodiceStato());
        attributes.put("codiceISO", getCodiceISO());
        attributes.put("codicePaese", getCodicePaese());
        attributes.put("stato", getStato());
        attributes.put("codiceAzienda", getCodiceAzienda());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Boolean black = (Boolean) attributes.get("black");

        if (black != null) {
            setBlack(black);
        }

        String codiceStato = (String) attributes.get("codiceStato");

        if (codiceStato != null) {
            setCodiceStato(codiceStato);
        }

        String codiceISO = (String) attributes.get("codiceISO");

        if (codiceISO != null) {
            setCodiceISO(codiceISO);
        }

        String codicePaese = (String) attributes.get("codicePaese");

        if (codicePaese != null) {
            setCodicePaese(codicePaese);
        }

        String stato = (String) attributes.get("stato");

        if (stato != null) {
            setStato(stato);
        }

        String codiceAzienda = (String) attributes.get("codiceAzienda");

        if (codiceAzienda != null) {
            setCodiceAzienda(codiceAzienda);
        }
    }

    /**
    * Returns the primary key of this stati.
    *
    * @return the primary key of this stati
    */
    @Override
    public java.lang.String getPrimaryKey() {
        return _stati.getPrimaryKey();
    }

    /**
    * Sets the primary key of this stati.
    *
    * @param primaryKey the primary key of this stati
    */
    @Override
    public void setPrimaryKey(java.lang.String primaryKey) {
        _stati.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the black of this stati.
    *
    * @return the black of this stati
    */
    @Override
    public boolean getBlack() {
        return _stati.getBlack();
    }

    /**
    * Returns <code>true</code> if this stati is black.
    *
    * @return <code>true</code> if this stati is black; <code>false</code> otherwise
    */
    @Override
    public boolean isBlack() {
        return _stati.isBlack();
    }

    /**
    * Sets whether this stati is black.
    *
    * @param black the black of this stati
    */
    @Override
    public void setBlack(boolean black) {
        _stati.setBlack(black);
    }

    /**
    * Returns the codice stato of this stati.
    *
    * @return the codice stato of this stati
    */
    @Override
    public java.lang.String getCodiceStato() {
        return _stati.getCodiceStato();
    }

    /**
    * Sets the codice stato of this stati.
    *
    * @param codiceStato the codice stato of this stati
    */
    @Override
    public void setCodiceStato(java.lang.String codiceStato) {
        _stati.setCodiceStato(codiceStato);
    }

    /**
    * Returns the codice i s o of this stati.
    *
    * @return the codice i s o of this stati
    */
    @Override
    public java.lang.String getCodiceISO() {
        return _stati.getCodiceISO();
    }

    /**
    * Sets the codice i s o of this stati.
    *
    * @param codiceISO the codice i s o of this stati
    */
    @Override
    public void setCodiceISO(java.lang.String codiceISO) {
        _stati.setCodiceISO(codiceISO);
    }

    /**
    * Returns the codice paese of this stati.
    *
    * @return the codice paese of this stati
    */
    @Override
    public java.lang.String getCodicePaese() {
        return _stati.getCodicePaese();
    }

    /**
    * Sets the codice paese of this stati.
    *
    * @param codicePaese the codice paese of this stati
    */
    @Override
    public void setCodicePaese(java.lang.String codicePaese) {
        _stati.setCodicePaese(codicePaese);
    }

    /**
    * Returns the stato of this stati.
    *
    * @return the stato of this stati
    */
    @Override
    public java.lang.String getStato() {
        return _stati.getStato();
    }

    /**
    * Sets the stato of this stati.
    *
    * @param stato the stato of this stati
    */
    @Override
    public void setStato(java.lang.String stato) {
        _stati.setStato(stato);
    }

    /**
    * Returns the codice azienda of this stati.
    *
    * @return the codice azienda of this stati
    */
    @Override
    public java.lang.String getCodiceAzienda() {
        return _stati.getCodiceAzienda();
    }

    /**
    * Sets the codice azienda of this stati.
    *
    * @param codiceAzienda the codice azienda of this stati
    */
    @Override
    public void setCodiceAzienda(java.lang.String codiceAzienda) {
        _stati.setCodiceAzienda(codiceAzienda);
    }

    @Override
    public boolean isNew() {
        return _stati.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _stati.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _stati.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _stati.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _stati.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _stati.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _stati.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _stati.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _stati.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _stati.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _stati.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new StatiWrapper((Stati) _stati.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.Stati stati) {
        return _stati.compareTo(stati);
    }

    @Override
    public int hashCode() {
        return _stati.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.Stati> toCacheModel() {
        return _stati.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.Stati toEscapedModel() {
        return new StatiWrapper(_stati.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.Stati toUnescapedModel() {
        return new StatiWrapper(_stati.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _stati.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _stati.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _stati.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof StatiWrapper)) {
            return false;
        }

        StatiWrapper statiWrapper = (StatiWrapper) obj;

        if (Validator.equals(_stati, statiWrapper._stati)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Stati getWrappedStati() {
        return _stati;
    }

    @Override
    public Stati getWrappedModel() {
        return _stati;
    }

    @Override
    public void resetOriginalValues() {
        _stati.resetOriginalValues();
    }
}
