package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Riga}.
 * </p>
 *
 * @author Mario Torrisi
 * @see Riga
 * @generated
 */
public class RigaWrapper implements Riga, ModelWrapper<Riga> {
    private Riga _riga;

    public RigaWrapper(Riga riga) {
        _riga = riga;
    }

    @Override
    public Class<?> getModelClass() {
        return Riga.class;
    }

    @Override
    public String getModelClassName() {
        return Riga.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("idCarrello", getIdCarrello());
        attributes.put("idArticolo", getIdArticolo());
        attributes.put("progressivo", getProgressivo());
        attributes.put("prezzo", getPrezzo());
        attributes.put("importo", getImporto());
        attributes.put("pianteRipiano", getPianteRipiano());
        attributes.put("sormontate", getSormontate());
        attributes.put("stringa", getStringa());
        attributes.put("opzioni", getOpzioni());
        attributes.put("importoVarianti", getImportoVarianti());
        attributes.put("codiceVariante", getCodiceVariante());
        attributes.put("codiceRegionale", getCodiceRegionale());
        attributes.put("passaporto", getPassaporto());
        attributes.put("s2", getS2());
        attributes.put("s3", getS3());
        attributes.put("s4", getS4());
        attributes.put("sconto", getSconto());
        attributes.put("prezzoFornitore", getPrezzoFornitore());
        attributes.put("prezzoEtichetta", getPrezzoEtichetta());
        attributes.put("ean", getEan());
        attributes.put("modificato", getModificato());
        attributes.put("ricevuto", getRicevuto());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long idCarrello = (Long) attributes.get("idCarrello");

        if (idCarrello != null) {
            setIdCarrello(idCarrello);
        }

        Long idArticolo = (Long) attributes.get("idArticolo");

        if (idArticolo != null) {
            setIdArticolo(idArticolo);
        }

        Long progressivo = (Long) attributes.get("progressivo");

        if (progressivo != null) {
            setProgressivo(progressivo);
        }

        Double prezzo = (Double) attributes.get("prezzo");

        if (prezzo != null) {
            setPrezzo(prezzo);
        }

        Double importo = (Double) attributes.get("importo");

        if (importo != null) {
            setImporto(importo);
        }

        Integer pianteRipiano = (Integer) attributes.get("pianteRipiano");

        if (pianteRipiano != null) {
            setPianteRipiano(pianteRipiano);
        }

        Boolean sormontate = (Boolean) attributes.get("sormontate");

        if (sormontate != null) {
            setSormontate(sormontate);
        }

        String stringa = (String) attributes.get("stringa");

        if (stringa != null) {
            setStringa(stringa);
        }

        String opzioni = (String) attributes.get("opzioni");

        if (opzioni != null) {
            setOpzioni(opzioni);
        }

        Double importoVarianti = (Double) attributes.get("importoVarianti");

        if (importoVarianti != null) {
            setImportoVarianti(importoVarianti);
        }

        String codiceVariante = (String) attributes.get("codiceVariante");

        if (codiceVariante != null) {
            setCodiceVariante(codiceVariante);
        }

        String codiceRegionale = (String) attributes.get("codiceRegionale");

        if (codiceRegionale != null) {
            setCodiceRegionale(codiceRegionale);
        }

        String passaporto = (String) attributes.get("passaporto");

        if (passaporto != null) {
            setPassaporto(passaporto);
        }

        Integer s2 = (Integer) attributes.get("s2");

        if (s2 != null) {
            setS2(s2);
        }

        Integer s3 = (Integer) attributes.get("s3");

        if (s3 != null) {
            setS3(s3);
        }

        Integer s4 = (Integer) attributes.get("s4");

        if (s4 != null) {
            setS4(s4);
        }

        Double sconto = (Double) attributes.get("sconto");

        if (sconto != null) {
            setSconto(sconto);
        }

        Double prezzoFornitore = (Double) attributes.get("prezzoFornitore");

        if (prezzoFornitore != null) {
            setPrezzoFornitore(prezzoFornitore);
        }

        Double prezzoEtichetta = (Double) attributes.get("prezzoEtichetta");

        if (prezzoEtichetta != null) {
            setPrezzoEtichetta(prezzoEtichetta);
        }

        String ean = (String) attributes.get("ean");

        if (ean != null) {
            setEan(ean);
        }

        Boolean modificato = (Boolean) attributes.get("modificato");

        if (modificato != null) {
            setModificato(modificato);
        }

        Boolean ricevuto = (Boolean) attributes.get("ricevuto");

        if (ricevuto != null) {
            setRicevuto(ricevuto);
        }
    }

    /**
    * Returns the primary key of this riga.
    *
    * @return the primary key of this riga
    */
    @Override
    public long getPrimaryKey() {
        return _riga.getPrimaryKey();
    }

    /**
    * Sets the primary key of this riga.
    *
    * @param primaryKey the primary key of this riga
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _riga.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this riga.
    *
    * @return the ID of this riga
    */
    @Override
    public long getId() {
        return _riga.getId();
    }

    /**
    * Sets the ID of this riga.
    *
    * @param id the ID of this riga
    */
    @Override
    public void setId(long id) {
        _riga.setId(id);
    }

    /**
    * Returns the id carrello of this riga.
    *
    * @return the id carrello of this riga
    */
    @Override
    public long getIdCarrello() {
        return _riga.getIdCarrello();
    }

    /**
    * Sets the id carrello of this riga.
    *
    * @param idCarrello the id carrello of this riga
    */
    @Override
    public void setIdCarrello(long idCarrello) {
        _riga.setIdCarrello(idCarrello);
    }

    /**
    * Returns the id articolo of this riga.
    *
    * @return the id articolo of this riga
    */
    @Override
    public long getIdArticolo() {
        return _riga.getIdArticolo();
    }

    /**
    * Sets the id articolo of this riga.
    *
    * @param idArticolo the id articolo of this riga
    */
    @Override
    public void setIdArticolo(long idArticolo) {
        _riga.setIdArticolo(idArticolo);
    }

    /**
    * Returns the progressivo of this riga.
    *
    * @return the progressivo of this riga
    */
    @Override
    public long getProgressivo() {
        return _riga.getProgressivo();
    }

    /**
    * Sets the progressivo of this riga.
    *
    * @param progressivo the progressivo of this riga
    */
    @Override
    public void setProgressivo(long progressivo) {
        _riga.setProgressivo(progressivo);
    }

    /**
    * Returns the prezzo of this riga.
    *
    * @return the prezzo of this riga
    */
    @Override
    public double getPrezzo() {
        return _riga.getPrezzo();
    }

    /**
    * Sets the prezzo of this riga.
    *
    * @param prezzo the prezzo of this riga
    */
    @Override
    public void setPrezzo(double prezzo) {
        _riga.setPrezzo(prezzo);
    }

    /**
    * Returns the importo of this riga.
    *
    * @return the importo of this riga
    */
    @Override
    public double getImporto() {
        return _riga.getImporto();
    }

    /**
    * Sets the importo of this riga.
    *
    * @param importo the importo of this riga
    */
    @Override
    public void setImporto(double importo) {
        _riga.setImporto(importo);
    }

    /**
    * Returns the piante ripiano of this riga.
    *
    * @return the piante ripiano of this riga
    */
    @Override
    public int getPianteRipiano() {
        return _riga.getPianteRipiano();
    }

    /**
    * Sets the piante ripiano of this riga.
    *
    * @param pianteRipiano the piante ripiano of this riga
    */
    @Override
    public void setPianteRipiano(int pianteRipiano) {
        _riga.setPianteRipiano(pianteRipiano);
    }

    /**
    * Returns the sormontate of this riga.
    *
    * @return the sormontate of this riga
    */
    @Override
    public boolean getSormontate() {
        return _riga.getSormontate();
    }

    /**
    * Returns <code>true</code> if this riga is sormontate.
    *
    * @return <code>true</code> if this riga is sormontate; <code>false</code> otherwise
    */
    @Override
    public boolean isSormontate() {
        return _riga.isSormontate();
    }

    /**
    * Sets whether this riga is sormontate.
    *
    * @param sormontate the sormontate of this riga
    */
    @Override
    public void setSormontate(boolean sormontate) {
        _riga.setSormontate(sormontate);
    }

    /**
    * Returns the stringa of this riga.
    *
    * @return the stringa of this riga
    */
    @Override
    public java.lang.String getStringa() {
        return _riga.getStringa();
    }

    /**
    * Sets the stringa of this riga.
    *
    * @param stringa the stringa of this riga
    */
    @Override
    public void setStringa(java.lang.String stringa) {
        _riga.setStringa(stringa);
    }

    /**
    * Returns the opzioni of this riga.
    *
    * @return the opzioni of this riga
    */
    @Override
    public java.lang.String getOpzioni() {
        return _riga.getOpzioni();
    }

    /**
    * Sets the opzioni of this riga.
    *
    * @param opzioni the opzioni of this riga
    */
    @Override
    public void setOpzioni(java.lang.String opzioni) {
        _riga.setOpzioni(opzioni);
    }

    /**
    * Returns the importo varianti of this riga.
    *
    * @return the importo varianti of this riga
    */
    @Override
    public double getImportoVarianti() {
        return _riga.getImportoVarianti();
    }

    /**
    * Sets the importo varianti of this riga.
    *
    * @param importoVarianti the importo varianti of this riga
    */
    @Override
    public void setImportoVarianti(double importoVarianti) {
        _riga.setImportoVarianti(importoVarianti);
    }

    /**
    * Returns the codice variante of this riga.
    *
    * @return the codice variante of this riga
    */
    @Override
    public java.lang.String getCodiceVariante() {
        return _riga.getCodiceVariante();
    }

    /**
    * Sets the codice variante of this riga.
    *
    * @param codiceVariante the codice variante of this riga
    */
    @Override
    public void setCodiceVariante(java.lang.String codiceVariante) {
        _riga.setCodiceVariante(codiceVariante);
    }

    /**
    * Returns the codice regionale of this riga.
    *
    * @return the codice regionale of this riga
    */
    @Override
    public java.lang.String getCodiceRegionale() {
        return _riga.getCodiceRegionale();
    }

    /**
    * Sets the codice regionale of this riga.
    *
    * @param codiceRegionale the codice regionale of this riga
    */
    @Override
    public void setCodiceRegionale(java.lang.String codiceRegionale) {
        _riga.setCodiceRegionale(codiceRegionale);
    }

    /**
    * Returns the passaporto of this riga.
    *
    * @return the passaporto of this riga
    */
    @Override
    public java.lang.String getPassaporto() {
        return _riga.getPassaporto();
    }

    /**
    * Sets the passaporto of this riga.
    *
    * @param passaporto the passaporto of this riga
    */
    @Override
    public void setPassaporto(java.lang.String passaporto) {
        _riga.setPassaporto(passaporto);
    }

    /**
    * Returns the s2 of this riga.
    *
    * @return the s2 of this riga
    */
    @Override
    public int getS2() {
        return _riga.getS2();
    }

    /**
    * Sets the s2 of this riga.
    *
    * @param s2 the s2 of this riga
    */
    @Override
    public void setS2(int s2) {
        _riga.setS2(s2);
    }

    /**
    * Returns the s3 of this riga.
    *
    * @return the s3 of this riga
    */
    @Override
    public int getS3() {
        return _riga.getS3();
    }

    /**
    * Sets the s3 of this riga.
    *
    * @param s3 the s3 of this riga
    */
    @Override
    public void setS3(int s3) {
        _riga.setS3(s3);
    }

    /**
    * Returns the s4 of this riga.
    *
    * @return the s4 of this riga
    */
    @Override
    public int getS4() {
        return _riga.getS4();
    }

    /**
    * Sets the s4 of this riga.
    *
    * @param s4 the s4 of this riga
    */
    @Override
    public void setS4(int s4) {
        _riga.setS4(s4);
    }

    /**
    * Returns the sconto of this riga.
    *
    * @return the sconto of this riga
    */
    @Override
    public double getSconto() {
        return _riga.getSconto();
    }

    /**
    * Sets the sconto of this riga.
    *
    * @param sconto the sconto of this riga
    */
    @Override
    public void setSconto(double sconto) {
        _riga.setSconto(sconto);
    }

    /**
    * Returns the prezzo fornitore of this riga.
    *
    * @return the prezzo fornitore of this riga
    */
    @Override
    public double getPrezzoFornitore() {
        return _riga.getPrezzoFornitore();
    }

    /**
    * Sets the prezzo fornitore of this riga.
    *
    * @param prezzoFornitore the prezzo fornitore of this riga
    */
    @Override
    public void setPrezzoFornitore(double prezzoFornitore) {
        _riga.setPrezzoFornitore(prezzoFornitore);
    }

    /**
    * Returns the prezzo etichetta of this riga.
    *
    * @return the prezzo etichetta of this riga
    */
    @Override
    public double getPrezzoEtichetta() {
        return _riga.getPrezzoEtichetta();
    }

    /**
    * Sets the prezzo etichetta of this riga.
    *
    * @param prezzoEtichetta the prezzo etichetta of this riga
    */
    @Override
    public void setPrezzoEtichetta(double prezzoEtichetta) {
        _riga.setPrezzoEtichetta(prezzoEtichetta);
    }

    /**
    * Returns the ean of this riga.
    *
    * @return the ean of this riga
    */
    @Override
    public java.lang.String getEan() {
        return _riga.getEan();
    }

    /**
    * Sets the ean of this riga.
    *
    * @param ean the ean of this riga
    */
    @Override
    public void setEan(java.lang.String ean) {
        _riga.setEan(ean);
    }

    /**
    * Returns the modificato of this riga.
    *
    * @return the modificato of this riga
    */
    @Override
    public boolean getModificato() {
        return _riga.getModificato();
    }

    /**
    * Returns <code>true</code> if this riga is modificato.
    *
    * @return <code>true</code> if this riga is modificato; <code>false</code> otherwise
    */
    @Override
    public boolean isModificato() {
        return _riga.isModificato();
    }

    /**
    * Sets whether this riga is modificato.
    *
    * @param modificato the modificato of this riga
    */
    @Override
    public void setModificato(boolean modificato) {
        _riga.setModificato(modificato);
    }

    /**
    * Returns the ricevuto of this riga.
    *
    * @return the ricevuto of this riga
    */
    @Override
    public boolean getRicevuto() {
        return _riga.getRicevuto();
    }

    /**
    * Returns <code>true</code> if this riga is ricevuto.
    *
    * @return <code>true</code> if this riga is ricevuto; <code>false</code> otherwise
    */
    @Override
    public boolean isRicevuto() {
        return _riga.isRicevuto();
    }

    /**
    * Sets whether this riga is ricevuto.
    *
    * @param ricevuto the ricevuto of this riga
    */
    @Override
    public void setRicevuto(boolean ricevuto) {
        _riga.setRicevuto(ricevuto);
    }

    @Override
    public boolean isNew() {
        return _riga.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _riga.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _riga.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _riga.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _riga.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _riga.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _riga.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _riga.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _riga.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _riga.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _riga.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new RigaWrapper((Riga) _riga.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.Riga riga) {
        return _riga.compareTo(riga);
    }

    @Override
    public int hashCode() {
        return _riga.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.Riga> toCacheModel() {
        return _riga.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.Riga toEscapedModel() {
        return new RigaWrapper(_riga.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.Riga toUnescapedModel() {
        return new RigaWrapper(_riga.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _riga.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _riga.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _riga.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof RigaWrapper)) {
            return false;
        }

        RigaWrapper rigaWrapper = (RigaWrapper) obj;

        if (Validator.equals(_riga, rigaWrapper._riga)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Riga getWrappedRiga() {
        return _riga;
    }

    @Override
    public Riga getWrappedModel() {
        return _riga;
    }

    @Override
    public void resetOriginalValues() {
        _riga.resetOriginalValues();
    }
}
