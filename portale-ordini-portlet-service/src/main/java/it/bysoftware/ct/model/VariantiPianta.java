package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the VariantiPianta service. Represents a row in the &quot;varianti_pianta&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see VariantiPiantaModel
 * @see it.bysoftware.ct.model.impl.VariantiPiantaImpl
 * @see it.bysoftware.ct.model.impl.VariantiPiantaModelImpl
 * @generated
 */
public interface VariantiPianta extends VariantiPiantaModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.VariantiPiantaImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
