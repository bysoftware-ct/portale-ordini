package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link DescrizioniVarianti}.
 * </p>
 *
 * @author Mario Torrisi
 * @see DescrizioniVarianti
 * @generated
 */
public class DescrizioniVariantiWrapper implements DescrizioniVarianti,
    ModelWrapper<DescrizioniVarianti> {
    private DescrizioniVarianti _descrizioniVarianti;

    public DescrizioniVariantiWrapper(DescrizioniVarianti descrizioniVarianti) {
        _descrizioniVarianti = descrizioniVarianti;
    }

    @Override
    public Class<?> getModelClass() {
        return DescrizioniVarianti.class;
    }

    @Override
    public String getModelClassName() {
        return DescrizioniVarianti.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceArticolo", getCodiceArticolo());
        attributes.put("codiceVariante", getCodiceVariante());
        attributes.put("descrizione", getDescrizione());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceArticolo = (String) attributes.get("codiceArticolo");

        if (codiceArticolo != null) {
            setCodiceArticolo(codiceArticolo);
        }

        String codiceVariante = (String) attributes.get("codiceVariante");

        if (codiceVariante != null) {
            setCodiceVariante(codiceVariante);
        }

        String descrizione = (String) attributes.get("descrizione");

        if (descrizione != null) {
            setDescrizione(descrizione);
        }
    }

    /**
    * Returns the primary key of this descrizioni varianti.
    *
    * @return the primary key of this descrizioni varianti
    */
    @Override
    public it.bysoftware.ct.service.persistence.DescrizioniVariantiPK getPrimaryKey() {
        return _descrizioniVarianti.getPrimaryKey();
    }

    /**
    * Sets the primary key of this descrizioni varianti.
    *
    * @param primaryKey the primary key of this descrizioni varianti
    */
    @Override
    public void setPrimaryKey(
        it.bysoftware.ct.service.persistence.DescrizioniVariantiPK primaryKey) {
        _descrizioniVarianti.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the codice articolo of this descrizioni varianti.
    *
    * @return the codice articolo of this descrizioni varianti
    */
    @Override
    public java.lang.String getCodiceArticolo() {
        return _descrizioniVarianti.getCodiceArticolo();
    }

    /**
    * Sets the codice articolo of this descrizioni varianti.
    *
    * @param codiceArticolo the codice articolo of this descrizioni varianti
    */
    @Override
    public void setCodiceArticolo(java.lang.String codiceArticolo) {
        _descrizioniVarianti.setCodiceArticolo(codiceArticolo);
    }

    /**
    * Returns the codice variante of this descrizioni varianti.
    *
    * @return the codice variante of this descrizioni varianti
    */
    @Override
    public java.lang.String getCodiceVariante() {
        return _descrizioniVarianti.getCodiceVariante();
    }

    /**
    * Sets the codice variante of this descrizioni varianti.
    *
    * @param codiceVariante the codice variante of this descrizioni varianti
    */
    @Override
    public void setCodiceVariante(java.lang.String codiceVariante) {
        _descrizioniVarianti.setCodiceVariante(codiceVariante);
    }

    /**
    * Returns the descrizione of this descrizioni varianti.
    *
    * @return the descrizione of this descrizioni varianti
    */
    @Override
    public java.lang.String getDescrizione() {
        return _descrizioniVarianti.getDescrizione();
    }

    /**
    * Sets the descrizione of this descrizioni varianti.
    *
    * @param descrizione the descrizione of this descrizioni varianti
    */
    @Override
    public void setDescrizione(java.lang.String descrizione) {
        _descrizioniVarianti.setDescrizione(descrizione);
    }

    @Override
    public boolean isNew() {
        return _descrizioniVarianti.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _descrizioniVarianti.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _descrizioniVarianti.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _descrizioniVarianti.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _descrizioniVarianti.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _descrizioniVarianti.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _descrizioniVarianti.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _descrizioniVarianti.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _descrizioniVarianti.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _descrizioniVarianti.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _descrizioniVarianti.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new DescrizioniVariantiWrapper((DescrizioniVarianti) _descrizioniVarianti.clone());
    }

    @Override
    public int compareTo(
        it.bysoftware.ct.model.DescrizioniVarianti descrizioniVarianti) {
        return _descrizioniVarianti.compareTo(descrizioniVarianti);
    }

    @Override
    public int hashCode() {
        return _descrizioniVarianti.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.DescrizioniVarianti> toCacheModel() {
        return _descrizioniVarianti.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.DescrizioniVarianti toEscapedModel() {
        return new DescrizioniVariantiWrapper(_descrizioniVarianti.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.DescrizioniVarianti toUnescapedModel() {
        return new DescrizioniVariantiWrapper(_descrizioniVarianti.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _descrizioniVarianti.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _descrizioniVarianti.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _descrizioniVarianti.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof DescrizioniVariantiWrapper)) {
            return false;
        }

        DescrizioniVariantiWrapper descrizioniVariantiWrapper = (DescrizioniVariantiWrapper) obj;

        if (Validator.equals(_descrizioniVarianti,
                    descrizioniVariantiWrapper._descrizioniVarianti)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public DescrizioniVarianti getWrappedDescrizioniVarianti() {
        return _descrizioniVarianti;
    }

    @Override
    public DescrizioniVarianti getWrappedModel() {
        return _descrizioniVarianti;
    }

    @Override
    public void resetOriginalValues() {
        _descrizioniVarianti.resetOriginalValues();
    }
}
