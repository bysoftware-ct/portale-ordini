package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Articoli}.
 * </p>
 *
 * @author Mario Torrisi
 * @see Articoli
 * @generated
 */
public class ArticoliWrapper implements Articoli, ModelWrapper<Articoli> {
    private Articoli _articoli;

    public ArticoliWrapper(Articoli articoli) {
        _articoli = articoli;
    }

    @Override
    public Class<?> getModelClass() {
        return Articoli.class;
    }

    @Override
    public String getModelClassName() {
        return Articoli.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceArticolo", getCodiceArticolo());
        attributes.put("categoriaMerceologica", getCategoriaMerceologica());
        attributes.put("categoriaInventario", getCategoriaInventario());
        attributes.put("descrizione", getDescrizione());
        attributes.put("descrizioneDocumento", getDescrizioneDocumento());
        attributes.put("descrizioneFiscale", getDescrizioneFiscale());
        attributes.put("unitaMisura", getUnitaMisura());
        attributes.put("tara", getTara());
        attributes.put("codiceIVA", getCodiceIVA());
        attributes.put("prezzo1", getPrezzo1());
        attributes.put("prezzo2", getPrezzo2());
        attributes.put("prezzo3", getPrezzo3());
        attributes.put("libLng1", getLibLng1());
        attributes.put("libStr1", getLibStr1());
        attributes.put("obsoleto", getObsoleto());
        attributes.put("tipoArticolo", getTipoArticolo());
        attributes.put("codiceProvvigione", getCodiceProvvigione());
        attributes.put("pesoLordo", getPesoLordo());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceArticolo = (String) attributes.get("codiceArticolo");

        if (codiceArticolo != null) {
            setCodiceArticolo(codiceArticolo);
        }

        String categoriaMerceologica = (String) attributes.get(
                "categoriaMerceologica");

        if (categoriaMerceologica != null) {
            setCategoriaMerceologica(categoriaMerceologica);
        }

        String categoriaInventario = (String) attributes.get(
                "categoriaInventario");

        if (categoriaInventario != null) {
            setCategoriaInventario(categoriaInventario);
        }

        String descrizione = (String) attributes.get("descrizione");

        if (descrizione != null) {
            setDescrizione(descrizione);
        }

        String descrizioneDocumento = (String) attributes.get(
                "descrizioneDocumento");

        if (descrizioneDocumento != null) {
            setDescrizioneDocumento(descrizioneDocumento);
        }

        String descrizioneFiscale = (String) attributes.get(
                "descrizioneFiscale");

        if (descrizioneFiscale != null) {
            setDescrizioneFiscale(descrizioneFiscale);
        }

        String unitaMisura = (String) attributes.get("unitaMisura");

        if (unitaMisura != null) {
            setUnitaMisura(unitaMisura);
        }

        Double tara = (Double) attributes.get("tara");

        if (tara != null) {
            setTara(tara);
        }

        String codiceIVA = (String) attributes.get("codiceIVA");

        if (codiceIVA != null) {
            setCodiceIVA(codiceIVA);
        }

        Double prezzo1 = (Double) attributes.get("prezzo1");

        if (prezzo1 != null) {
            setPrezzo1(prezzo1);
        }

        Double prezzo2 = (Double) attributes.get("prezzo2");

        if (prezzo2 != null) {
            setPrezzo2(prezzo2);
        }

        Double prezzo3 = (Double) attributes.get("prezzo3");

        if (prezzo3 != null) {
            setPrezzo3(prezzo3);
        }

        Long libLng1 = (Long) attributes.get("libLng1");

        if (libLng1 != null) {
            setLibLng1(libLng1);
        }

        String libStr1 = (String) attributes.get("libStr1");

        if (libStr1 != null) {
            setLibStr1(libStr1);
        }

        Boolean obsoleto = (Boolean) attributes.get("obsoleto");

        if (obsoleto != null) {
            setObsoleto(obsoleto);
        }

        String tipoArticolo = (String) attributes.get("tipoArticolo");

        if (tipoArticolo != null) {
            setTipoArticolo(tipoArticolo);
        }

        String codiceProvvigione = (String) attributes.get("codiceProvvigione");

        if (codiceProvvigione != null) {
            setCodiceProvvigione(codiceProvvigione);
        }

        Double pesoLordo = (Double) attributes.get("pesoLordo");

        if (pesoLordo != null) {
            setPesoLordo(pesoLordo);
        }
    }

    /**
    * Returns the primary key of this articoli.
    *
    * @return the primary key of this articoli
    */
    @Override
    public java.lang.String getPrimaryKey() {
        return _articoli.getPrimaryKey();
    }

    /**
    * Sets the primary key of this articoli.
    *
    * @param primaryKey the primary key of this articoli
    */
    @Override
    public void setPrimaryKey(java.lang.String primaryKey) {
        _articoli.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the codice articolo of this articoli.
    *
    * @return the codice articolo of this articoli
    */
    @Override
    public java.lang.String getCodiceArticolo() {
        return _articoli.getCodiceArticolo();
    }

    /**
    * Sets the codice articolo of this articoli.
    *
    * @param codiceArticolo the codice articolo of this articoli
    */
    @Override
    public void setCodiceArticolo(java.lang.String codiceArticolo) {
        _articoli.setCodiceArticolo(codiceArticolo);
    }

    /**
    * Returns the categoria merceologica of this articoli.
    *
    * @return the categoria merceologica of this articoli
    */
    @Override
    public java.lang.String getCategoriaMerceologica() {
        return _articoli.getCategoriaMerceologica();
    }

    /**
    * Sets the categoria merceologica of this articoli.
    *
    * @param categoriaMerceologica the categoria merceologica of this articoli
    */
    @Override
    public void setCategoriaMerceologica(java.lang.String categoriaMerceologica) {
        _articoli.setCategoriaMerceologica(categoriaMerceologica);
    }

    /**
    * Returns the categoria inventario of this articoli.
    *
    * @return the categoria inventario of this articoli
    */
    @Override
    public java.lang.String getCategoriaInventario() {
        return _articoli.getCategoriaInventario();
    }

    /**
    * Sets the categoria inventario of this articoli.
    *
    * @param categoriaInventario the categoria inventario of this articoli
    */
    @Override
    public void setCategoriaInventario(java.lang.String categoriaInventario) {
        _articoli.setCategoriaInventario(categoriaInventario);
    }

    /**
    * Returns the descrizione of this articoli.
    *
    * @return the descrizione of this articoli
    */
    @Override
    public java.lang.String getDescrizione() {
        return _articoli.getDescrizione();
    }

    /**
    * Sets the descrizione of this articoli.
    *
    * @param descrizione the descrizione of this articoli
    */
    @Override
    public void setDescrizione(java.lang.String descrizione) {
        _articoli.setDescrizione(descrizione);
    }

    /**
    * Returns the descrizione documento of this articoli.
    *
    * @return the descrizione documento of this articoli
    */
    @Override
    public java.lang.String getDescrizioneDocumento() {
        return _articoli.getDescrizioneDocumento();
    }

    /**
    * Sets the descrizione documento of this articoli.
    *
    * @param descrizioneDocumento the descrizione documento of this articoli
    */
    @Override
    public void setDescrizioneDocumento(java.lang.String descrizioneDocumento) {
        _articoli.setDescrizioneDocumento(descrizioneDocumento);
    }

    /**
    * Returns the descrizione fiscale of this articoli.
    *
    * @return the descrizione fiscale of this articoli
    */
    @Override
    public java.lang.String getDescrizioneFiscale() {
        return _articoli.getDescrizioneFiscale();
    }

    /**
    * Sets the descrizione fiscale of this articoli.
    *
    * @param descrizioneFiscale the descrizione fiscale of this articoli
    */
    @Override
    public void setDescrizioneFiscale(java.lang.String descrizioneFiscale) {
        _articoli.setDescrizioneFiscale(descrizioneFiscale);
    }

    /**
    * Returns the unita misura of this articoli.
    *
    * @return the unita misura of this articoli
    */
    @Override
    public java.lang.String getUnitaMisura() {
        return _articoli.getUnitaMisura();
    }

    /**
    * Sets the unita misura of this articoli.
    *
    * @param unitaMisura the unita misura of this articoli
    */
    @Override
    public void setUnitaMisura(java.lang.String unitaMisura) {
        _articoli.setUnitaMisura(unitaMisura);
    }

    /**
    * Returns the tara of this articoli.
    *
    * @return the tara of this articoli
    */
    @Override
    public double getTara() {
        return _articoli.getTara();
    }

    /**
    * Sets the tara of this articoli.
    *
    * @param tara the tara of this articoli
    */
    @Override
    public void setTara(double tara) {
        _articoli.setTara(tara);
    }

    /**
    * Returns the codice i v a of this articoli.
    *
    * @return the codice i v a of this articoli
    */
    @Override
    public java.lang.String getCodiceIVA() {
        return _articoli.getCodiceIVA();
    }

    /**
    * Sets the codice i v a of this articoli.
    *
    * @param codiceIVA the codice i v a of this articoli
    */
    @Override
    public void setCodiceIVA(java.lang.String codiceIVA) {
        _articoli.setCodiceIVA(codiceIVA);
    }

    /**
    * Returns the prezzo1 of this articoli.
    *
    * @return the prezzo1 of this articoli
    */
    @Override
    public double getPrezzo1() {
        return _articoli.getPrezzo1();
    }

    /**
    * Sets the prezzo1 of this articoli.
    *
    * @param prezzo1 the prezzo1 of this articoli
    */
    @Override
    public void setPrezzo1(double prezzo1) {
        _articoli.setPrezzo1(prezzo1);
    }

    /**
    * Returns the prezzo2 of this articoli.
    *
    * @return the prezzo2 of this articoli
    */
    @Override
    public double getPrezzo2() {
        return _articoli.getPrezzo2();
    }

    /**
    * Sets the prezzo2 of this articoli.
    *
    * @param prezzo2 the prezzo2 of this articoli
    */
    @Override
    public void setPrezzo2(double prezzo2) {
        _articoli.setPrezzo2(prezzo2);
    }

    /**
    * Returns the prezzo3 of this articoli.
    *
    * @return the prezzo3 of this articoli
    */
    @Override
    public double getPrezzo3() {
        return _articoli.getPrezzo3();
    }

    /**
    * Sets the prezzo3 of this articoli.
    *
    * @param prezzo3 the prezzo3 of this articoli
    */
    @Override
    public void setPrezzo3(double prezzo3) {
        _articoli.setPrezzo3(prezzo3);
    }

    /**
    * Returns the lib lng1 of this articoli.
    *
    * @return the lib lng1 of this articoli
    */
    @Override
    public long getLibLng1() {
        return _articoli.getLibLng1();
    }

    /**
    * Sets the lib lng1 of this articoli.
    *
    * @param libLng1 the lib lng1 of this articoli
    */
    @Override
    public void setLibLng1(long libLng1) {
        _articoli.setLibLng1(libLng1);
    }

    /**
    * Returns the lib str1 of this articoli.
    *
    * @return the lib str1 of this articoli
    */
    @Override
    public java.lang.String getLibStr1() {
        return _articoli.getLibStr1();
    }

    /**
    * Sets the lib str1 of this articoli.
    *
    * @param libStr1 the lib str1 of this articoli
    */
    @Override
    public void setLibStr1(java.lang.String libStr1) {
        _articoli.setLibStr1(libStr1);
    }

    /**
    * Returns the obsoleto of this articoli.
    *
    * @return the obsoleto of this articoli
    */
    @Override
    public boolean getObsoleto() {
        return _articoli.getObsoleto();
    }

    /**
    * Returns <code>true</code> if this articoli is obsoleto.
    *
    * @return <code>true</code> if this articoli is obsoleto; <code>false</code> otherwise
    */
    @Override
    public boolean isObsoleto() {
        return _articoli.isObsoleto();
    }

    /**
    * Sets whether this articoli is obsoleto.
    *
    * @param obsoleto the obsoleto of this articoli
    */
    @Override
    public void setObsoleto(boolean obsoleto) {
        _articoli.setObsoleto(obsoleto);
    }

    /**
    * Returns the tipo articolo of this articoli.
    *
    * @return the tipo articolo of this articoli
    */
    @Override
    public java.lang.String getTipoArticolo() {
        return _articoli.getTipoArticolo();
    }

    /**
    * Sets the tipo articolo of this articoli.
    *
    * @param tipoArticolo the tipo articolo of this articoli
    */
    @Override
    public void setTipoArticolo(java.lang.String tipoArticolo) {
        _articoli.setTipoArticolo(tipoArticolo);
    }

    /**
    * Returns the codice provvigione of this articoli.
    *
    * @return the codice provvigione of this articoli
    */
    @Override
    public java.lang.String getCodiceProvvigione() {
        return _articoli.getCodiceProvvigione();
    }

    /**
    * Sets the codice provvigione of this articoli.
    *
    * @param codiceProvvigione the codice provvigione of this articoli
    */
    @Override
    public void setCodiceProvvigione(java.lang.String codiceProvvigione) {
        _articoli.setCodiceProvvigione(codiceProvvigione);
    }

    /**
    * Returns the peso lordo of this articoli.
    *
    * @return the peso lordo of this articoli
    */
    @Override
    public double getPesoLordo() {
        return _articoli.getPesoLordo();
    }

    /**
    * Sets the peso lordo of this articoli.
    *
    * @param pesoLordo the peso lordo of this articoli
    */
    @Override
    public void setPesoLordo(double pesoLordo) {
        _articoli.setPesoLordo(pesoLordo);
    }

    @Override
    public boolean isNew() {
        return _articoli.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _articoli.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _articoli.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _articoli.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _articoli.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _articoli.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _articoli.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _articoli.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _articoli.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _articoli.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _articoli.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new ArticoliWrapper((Articoli) _articoli.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.Articoli articoli) {
        return _articoli.compareTo(articoli);
    }

    @Override
    public int hashCode() {
        return _articoli.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.Articoli> toCacheModel() {
        return _articoli.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.Articoli toEscapedModel() {
        return new ArticoliWrapper(_articoli.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.Articoli toUnescapedModel() {
        return new ArticoliWrapper(_articoli.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _articoli.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _articoli.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _articoli.persist();
    }

    @Override
    public it.bysoftware.ct.model.Piante getPianta(java.lang.String code) {
        return _articoli.getPianta(code);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ArticoliWrapper)) {
            return false;
        }

        ArticoliWrapper articoliWrapper = (ArticoliWrapper) obj;

        if (Validator.equals(_articoli, articoliWrapper._articoli)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Articoli getWrappedArticoli() {
        return _articoli;
    }

    @Override
    public Articoli getWrappedModel() {
        return _articoli;
    }

    @Override
    public void resetOriginalValues() {
        _articoli.resetOriginalValues();
    }
}
