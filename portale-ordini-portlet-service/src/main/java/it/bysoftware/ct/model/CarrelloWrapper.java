package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Carrello}.
 * </p>
 *
 * @author Mario Torrisi
 * @see Carrello
 * @generated
 */
public class CarrelloWrapper implements Carrello, ModelWrapper<Carrello> {
    private Carrello _carrello;

    public CarrelloWrapper(Carrello carrello) {
        _carrello = carrello;
    }

    @Override
    public Class<?> getModelClass() {
        return Carrello.class;
    }

    @Override
    public String getModelClassName() {
        return Carrello.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("idOrdine", getIdOrdine());
        attributes.put("progressivo", getProgressivo());
        attributes.put("importo", getImporto());
        attributes.put("chiuso", getChiuso());
        attributes.put("generico", getGenerico());
        attributes.put("lavorato", getLavorato());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long idOrdine = (Long) attributes.get("idOrdine");

        if (idOrdine != null) {
            setIdOrdine(idOrdine);
        }

        Long progressivo = (Long) attributes.get("progressivo");

        if (progressivo != null) {
            setProgressivo(progressivo);
        }

        Double importo = (Double) attributes.get("importo");

        if (importo != null) {
            setImporto(importo);
        }

        Boolean chiuso = (Boolean) attributes.get("chiuso");

        if (chiuso != null) {
            setChiuso(chiuso);
        }

        Boolean generico = (Boolean) attributes.get("generico");

        if (generico != null) {
            setGenerico(generico);
        }

        Boolean lavorato = (Boolean) attributes.get("lavorato");

        if (lavorato != null) {
            setLavorato(lavorato);
        }
    }

    /**
    * Returns the primary key of this carrello.
    *
    * @return the primary key of this carrello
    */
    @Override
    public long getPrimaryKey() {
        return _carrello.getPrimaryKey();
    }

    /**
    * Sets the primary key of this carrello.
    *
    * @param primaryKey the primary key of this carrello
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _carrello.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this carrello.
    *
    * @return the ID of this carrello
    */
    @Override
    public long getId() {
        return _carrello.getId();
    }

    /**
    * Sets the ID of this carrello.
    *
    * @param id the ID of this carrello
    */
    @Override
    public void setId(long id) {
        _carrello.setId(id);
    }

    /**
    * Returns the id ordine of this carrello.
    *
    * @return the id ordine of this carrello
    */
    @Override
    public long getIdOrdine() {
        return _carrello.getIdOrdine();
    }

    /**
    * Sets the id ordine of this carrello.
    *
    * @param idOrdine the id ordine of this carrello
    */
    @Override
    public void setIdOrdine(long idOrdine) {
        _carrello.setIdOrdine(idOrdine);
    }

    /**
    * Returns the progressivo of this carrello.
    *
    * @return the progressivo of this carrello
    */
    @Override
    public long getProgressivo() {
        return _carrello.getProgressivo();
    }

    /**
    * Sets the progressivo of this carrello.
    *
    * @param progressivo the progressivo of this carrello
    */
    @Override
    public void setProgressivo(long progressivo) {
        _carrello.setProgressivo(progressivo);
    }

    /**
    * Returns the importo of this carrello.
    *
    * @return the importo of this carrello
    */
    @Override
    public double getImporto() {
        return _carrello.getImporto();
    }

    /**
    * Sets the importo of this carrello.
    *
    * @param importo the importo of this carrello
    */
    @Override
    public void setImporto(double importo) {
        _carrello.setImporto(importo);
    }

    /**
    * Returns the chiuso of this carrello.
    *
    * @return the chiuso of this carrello
    */
    @Override
    public boolean getChiuso() {
        return _carrello.getChiuso();
    }

    /**
    * Returns <code>true</code> if this carrello is chiuso.
    *
    * @return <code>true</code> if this carrello is chiuso; <code>false</code> otherwise
    */
    @Override
    public boolean isChiuso() {
        return _carrello.isChiuso();
    }

    /**
    * Sets whether this carrello is chiuso.
    *
    * @param chiuso the chiuso of this carrello
    */
    @Override
    public void setChiuso(boolean chiuso) {
        _carrello.setChiuso(chiuso);
    }

    /**
    * Returns the generico of this carrello.
    *
    * @return the generico of this carrello
    */
    @Override
    public boolean getGenerico() {
        return _carrello.getGenerico();
    }

    /**
    * Returns <code>true</code> if this carrello is generico.
    *
    * @return <code>true</code> if this carrello is generico; <code>false</code> otherwise
    */
    @Override
    public boolean isGenerico() {
        return _carrello.isGenerico();
    }

    /**
    * Sets whether this carrello is generico.
    *
    * @param generico the generico of this carrello
    */
    @Override
    public void setGenerico(boolean generico) {
        _carrello.setGenerico(generico);
    }

    /**
    * Returns the lavorato of this carrello.
    *
    * @return the lavorato of this carrello
    */
    @Override
    public boolean getLavorato() {
        return _carrello.getLavorato();
    }

    /**
    * Returns <code>true</code> if this carrello is lavorato.
    *
    * @return <code>true</code> if this carrello is lavorato; <code>false</code> otherwise
    */
    @Override
    public boolean isLavorato() {
        return _carrello.isLavorato();
    }

    /**
    * Sets whether this carrello is lavorato.
    *
    * @param lavorato the lavorato of this carrello
    */
    @Override
    public void setLavorato(boolean lavorato) {
        _carrello.setLavorato(lavorato);
    }

    @Override
    public boolean isNew() {
        return _carrello.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _carrello.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _carrello.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _carrello.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _carrello.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _carrello.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _carrello.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _carrello.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _carrello.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _carrello.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _carrello.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new CarrelloWrapper((Carrello) _carrello.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.Carrello carrello) {
        return _carrello.compareTo(carrello);
    }

    @Override
    public int hashCode() {
        return _carrello.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.Carrello> toCacheModel() {
        return _carrello.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.Carrello toEscapedModel() {
        return new CarrelloWrapper(_carrello.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.Carrello toUnescapedModel() {
        return new CarrelloWrapper(_carrello.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _carrello.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _carrello.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _carrello.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CarrelloWrapper)) {
            return false;
        }

        CarrelloWrapper carrelloWrapper = (CarrelloWrapper) obj;

        if (Validator.equals(_carrello, carrelloWrapper._carrello)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Carrello getWrappedCarrello() {
        return _carrello;
    }

    @Override
    public Carrello getWrappedModel() {
        return _carrello;
    }

    @Override
    public void resetOriginalValues() {
        _carrello.resetOriginalValues();
    }
}
