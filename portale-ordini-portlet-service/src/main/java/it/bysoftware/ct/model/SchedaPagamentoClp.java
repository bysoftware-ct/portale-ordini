package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.SchedaPagamentoLocalServiceUtil;
import it.bysoftware.ct.service.persistence.SchedaPagamentoPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class SchedaPagamentoClp extends BaseModelImpl<SchedaPagamento>
    implements SchedaPagamento {
    private int _esercizioRegistrazione;
    private String _codiceSoggetto;
    private int _numeroPartita;
    private int _numeroScadenza;
    private boolean _tipoSoggetto;
    private int _esercizioRegistrazioneComp;
    private String _codiceSoggettoComp;
    private int _numeroPartitaComp;
    private int _numeroScadenzaComp;
    private boolean _tipoSoggettoComp;
    private double _differenza;
    private int _idPagamento;
    private int _annoPagamento;
    private int _stato;
    private String _nota;
    private BaseModel<?> _schedaPagamentoRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public SchedaPagamentoClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return SchedaPagamento.class;
    }

    @Override
    public String getModelClassName() {
        return SchedaPagamento.class.getName();
    }

    @Override
    public SchedaPagamentoPK getPrimaryKey() {
        return new SchedaPagamentoPK(_esercizioRegistrazione, _codiceSoggetto,
            _numeroPartita, _numeroScadenza, _tipoSoggetto);
    }

    @Override
    public void setPrimaryKey(SchedaPagamentoPK primaryKey) {
        setEsercizioRegistrazione(primaryKey.esercizioRegistrazione);
        setCodiceSoggetto(primaryKey.codiceSoggetto);
        setNumeroPartita(primaryKey.numeroPartita);
        setNumeroScadenza(primaryKey.numeroScadenza);
        setTipoSoggetto(primaryKey.tipoSoggetto);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return new SchedaPagamentoPK(_esercizioRegistrazione, _codiceSoggetto,
            _numeroPartita, _numeroScadenza, _tipoSoggetto);
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((SchedaPagamentoPK) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("esercizioRegistrazione", getEsercizioRegistrazione());
        attributes.put("codiceSoggetto", getCodiceSoggetto());
        attributes.put("numeroPartita", getNumeroPartita());
        attributes.put("numeroScadenza", getNumeroScadenza());
        attributes.put("tipoSoggetto", getTipoSoggetto());
        attributes.put("esercizioRegistrazioneComp",
            getEsercizioRegistrazioneComp());
        attributes.put("codiceSoggettoComp", getCodiceSoggettoComp());
        attributes.put("numeroPartitaComp", getNumeroPartitaComp());
        attributes.put("numeroScadenzaComp", getNumeroScadenzaComp());
        attributes.put("tipoSoggettoComp", getTipoSoggettoComp());
        attributes.put("differenza", getDifferenza());
        attributes.put("idPagamento", getIdPagamento());
        attributes.put("annoPagamento", getAnnoPagamento());
        attributes.put("stato", getStato());
        attributes.put("nota", getNota());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Integer esercizioRegistrazione = (Integer) attributes.get(
                "esercizioRegistrazione");

        if (esercizioRegistrazione != null) {
            setEsercizioRegistrazione(esercizioRegistrazione);
        }

        String codiceSoggetto = (String) attributes.get("codiceSoggetto");

        if (codiceSoggetto != null) {
            setCodiceSoggetto(codiceSoggetto);
        }

        Integer numeroPartita = (Integer) attributes.get("numeroPartita");

        if (numeroPartita != null) {
            setNumeroPartita(numeroPartita);
        }

        Integer numeroScadenza = (Integer) attributes.get("numeroScadenza");

        if (numeroScadenza != null) {
            setNumeroScadenza(numeroScadenza);
        }

        Boolean tipoSoggetto = (Boolean) attributes.get("tipoSoggetto");

        if (tipoSoggetto != null) {
            setTipoSoggetto(tipoSoggetto);
        }

        Integer esercizioRegistrazioneComp = (Integer) attributes.get(
                "esercizioRegistrazioneComp");

        if (esercizioRegistrazioneComp != null) {
            setEsercizioRegistrazioneComp(esercizioRegistrazioneComp);
        }

        String codiceSoggettoComp = (String) attributes.get(
                "codiceSoggettoComp");

        if (codiceSoggettoComp != null) {
            setCodiceSoggettoComp(codiceSoggettoComp);
        }

        Integer numeroPartitaComp = (Integer) attributes.get(
                "numeroPartitaComp");

        if (numeroPartitaComp != null) {
            setNumeroPartitaComp(numeroPartitaComp);
        }

        Integer numeroScadenzaComp = (Integer) attributes.get(
                "numeroScadenzaComp");

        if (numeroScadenzaComp != null) {
            setNumeroScadenzaComp(numeroScadenzaComp);
        }

        Boolean tipoSoggettoComp = (Boolean) attributes.get("tipoSoggettoComp");

        if (tipoSoggettoComp != null) {
            setTipoSoggettoComp(tipoSoggettoComp);
        }

        Double differenza = (Double) attributes.get("differenza");

        if (differenza != null) {
            setDifferenza(differenza);
        }

        Integer idPagamento = (Integer) attributes.get("idPagamento");

        if (idPagamento != null) {
            setIdPagamento(idPagamento);
        }

        Integer annoPagamento = (Integer) attributes.get("annoPagamento");

        if (annoPagamento != null) {
            setAnnoPagamento(annoPagamento);
        }

        Integer stato = (Integer) attributes.get("stato");

        if (stato != null) {
            setStato(stato);
        }

        String nota = (String) attributes.get("nota");

        if (nota != null) {
            setNota(nota);
        }
    }

    @Override
    public int getEsercizioRegistrazione() {
        return _esercizioRegistrazione;
    }

    @Override
    public void setEsercizioRegistrazione(int esercizioRegistrazione) {
        _esercizioRegistrazione = esercizioRegistrazione;

        if (_schedaPagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _schedaPagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setEsercizioRegistrazione",
                        int.class);

                method.invoke(_schedaPagamentoRemoteModel,
                    esercizioRegistrazione);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceSoggetto() {
        return _codiceSoggetto;
    }

    @Override
    public void setCodiceSoggetto(String codiceSoggetto) {
        _codiceSoggetto = codiceSoggetto;

        if (_schedaPagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _schedaPagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceSoggetto",
                        String.class);

                method.invoke(_schedaPagamentoRemoteModel, codiceSoggetto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getNumeroPartita() {
        return _numeroPartita;
    }

    @Override
    public void setNumeroPartita(int numeroPartita) {
        _numeroPartita = numeroPartita;

        if (_schedaPagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _schedaPagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setNumeroPartita", int.class);

                method.invoke(_schedaPagamentoRemoteModel, numeroPartita);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getNumeroScadenza() {
        return _numeroScadenza;
    }

    @Override
    public void setNumeroScadenza(int numeroScadenza) {
        _numeroScadenza = numeroScadenza;

        if (_schedaPagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _schedaPagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setNumeroScadenza", int.class);

                method.invoke(_schedaPagamentoRemoteModel, numeroScadenza);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getTipoSoggetto() {
        return _tipoSoggetto;
    }

    @Override
    public boolean isTipoSoggetto() {
        return _tipoSoggetto;
    }

    @Override
    public void setTipoSoggetto(boolean tipoSoggetto) {
        _tipoSoggetto = tipoSoggetto;

        if (_schedaPagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _schedaPagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setTipoSoggetto", boolean.class);

                method.invoke(_schedaPagamentoRemoteModel, tipoSoggetto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getEsercizioRegistrazioneComp() {
        return _esercizioRegistrazioneComp;
    }

    @Override
    public void setEsercizioRegistrazioneComp(int esercizioRegistrazioneComp) {
        _esercizioRegistrazioneComp = esercizioRegistrazioneComp;

        if (_schedaPagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _schedaPagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setEsercizioRegistrazioneComp",
                        int.class);

                method.invoke(_schedaPagamentoRemoteModel,
                    esercizioRegistrazioneComp);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceSoggettoComp() {
        return _codiceSoggettoComp;
    }

    @Override
    public void setCodiceSoggettoComp(String codiceSoggettoComp) {
        _codiceSoggettoComp = codiceSoggettoComp;

        if (_schedaPagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _schedaPagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceSoggettoComp",
                        String.class);

                method.invoke(_schedaPagamentoRemoteModel, codiceSoggettoComp);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getNumeroPartitaComp() {
        return _numeroPartitaComp;
    }

    @Override
    public void setNumeroPartitaComp(int numeroPartitaComp) {
        _numeroPartitaComp = numeroPartitaComp;

        if (_schedaPagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _schedaPagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setNumeroPartitaComp",
                        int.class);

                method.invoke(_schedaPagamentoRemoteModel, numeroPartitaComp);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getNumeroScadenzaComp() {
        return _numeroScadenzaComp;
    }

    @Override
    public void setNumeroScadenzaComp(int numeroScadenzaComp) {
        _numeroScadenzaComp = numeroScadenzaComp;

        if (_schedaPagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _schedaPagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setNumeroScadenzaComp",
                        int.class);

                method.invoke(_schedaPagamentoRemoteModel, numeroScadenzaComp);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getTipoSoggettoComp() {
        return _tipoSoggettoComp;
    }

    @Override
    public boolean isTipoSoggettoComp() {
        return _tipoSoggettoComp;
    }

    @Override
    public void setTipoSoggettoComp(boolean tipoSoggettoComp) {
        _tipoSoggettoComp = tipoSoggettoComp;

        if (_schedaPagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _schedaPagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setTipoSoggettoComp",
                        boolean.class);

                method.invoke(_schedaPagamentoRemoteModel, tipoSoggettoComp);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getDifferenza() {
        return _differenza;
    }

    @Override
    public void setDifferenza(double differenza) {
        _differenza = differenza;

        if (_schedaPagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _schedaPagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setDifferenza", double.class);

                method.invoke(_schedaPagamentoRemoteModel, differenza);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getIdPagamento() {
        return _idPagamento;
    }

    @Override
    public void setIdPagamento(int idPagamento) {
        _idPagamento = idPagamento;

        if (_schedaPagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _schedaPagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setIdPagamento", int.class);

                method.invoke(_schedaPagamentoRemoteModel, idPagamento);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getAnnoPagamento() {
        return _annoPagamento;
    }

    @Override
    public void setAnnoPagamento(int annoPagamento) {
        _annoPagamento = annoPagamento;

        if (_schedaPagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _schedaPagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setAnnoPagamento", int.class);

                method.invoke(_schedaPagamentoRemoteModel, annoPagamento);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getStato() {
        return _stato;
    }

    @Override
    public void setStato(int stato) {
        _stato = stato;

        if (_schedaPagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _schedaPagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setStato", int.class);

                method.invoke(_schedaPagamentoRemoteModel, stato);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNota() {
        return _nota;
    }

    @Override
    public void setNota(String nota) {
        _nota = nota;

        if (_schedaPagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _schedaPagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setNota", String.class);

                method.invoke(_schedaPagamentoRemoteModel, nota);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getSchedaPagamentoRemoteModel() {
        return _schedaPagamentoRemoteModel;
    }

    public void setSchedaPagamentoRemoteModel(
        BaseModel<?> schedaPagamentoRemoteModel) {
        _schedaPagamentoRemoteModel = schedaPagamentoRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _schedaPagamentoRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_schedaPagamentoRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            SchedaPagamentoLocalServiceUtil.addSchedaPagamento(this);
        } else {
            SchedaPagamentoLocalServiceUtil.updateSchedaPagamento(this);
        }
    }

    @Override
    public SchedaPagamento toEscapedModel() {
        return (SchedaPagamento) ProxyUtil.newProxyInstance(SchedaPagamento.class.getClassLoader(),
            new Class[] { SchedaPagamento.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        SchedaPagamentoClp clone = new SchedaPagamentoClp();

        clone.setEsercizioRegistrazione(getEsercizioRegistrazione());
        clone.setCodiceSoggetto(getCodiceSoggetto());
        clone.setNumeroPartita(getNumeroPartita());
        clone.setNumeroScadenza(getNumeroScadenza());
        clone.setTipoSoggetto(getTipoSoggetto());
        clone.setEsercizioRegistrazioneComp(getEsercizioRegistrazioneComp());
        clone.setCodiceSoggettoComp(getCodiceSoggettoComp());
        clone.setNumeroPartitaComp(getNumeroPartitaComp());
        clone.setNumeroScadenzaComp(getNumeroScadenzaComp());
        clone.setTipoSoggettoComp(getTipoSoggettoComp());
        clone.setDifferenza(getDifferenza());
        clone.setIdPagamento(getIdPagamento());
        clone.setAnnoPagamento(getAnnoPagamento());
        clone.setStato(getStato());
        clone.setNota(getNota());

        return clone;
    }

    @Override
    public int compareTo(SchedaPagamento schedaPagamento) {
        SchedaPagamentoPK primaryKey = schedaPagamento.getPrimaryKey();

        return getPrimaryKey().compareTo(primaryKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof SchedaPagamentoClp)) {
            return false;
        }

        SchedaPagamentoClp schedaPagamento = (SchedaPagamentoClp) obj;

        SchedaPagamentoPK primaryKey = schedaPagamento.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(31);

        sb.append("{esercizioRegistrazione=");
        sb.append(getEsercizioRegistrazione());
        sb.append(", codiceSoggetto=");
        sb.append(getCodiceSoggetto());
        sb.append(", numeroPartita=");
        sb.append(getNumeroPartita());
        sb.append(", numeroScadenza=");
        sb.append(getNumeroScadenza());
        sb.append(", tipoSoggetto=");
        sb.append(getTipoSoggetto());
        sb.append(", esercizioRegistrazioneComp=");
        sb.append(getEsercizioRegistrazioneComp());
        sb.append(", codiceSoggettoComp=");
        sb.append(getCodiceSoggettoComp());
        sb.append(", numeroPartitaComp=");
        sb.append(getNumeroPartitaComp());
        sb.append(", numeroScadenzaComp=");
        sb.append(getNumeroScadenzaComp());
        sb.append(", tipoSoggettoComp=");
        sb.append(getTipoSoggettoComp());
        sb.append(", differenza=");
        sb.append(getDifferenza());
        sb.append(", idPagamento=");
        sb.append(getIdPagamento());
        sb.append(", annoPagamento=");
        sb.append(getAnnoPagamento());
        sb.append(", stato=");
        sb.append(getStato());
        sb.append(", nota=");
        sb.append(getNota());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(49);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.SchedaPagamento");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>esercizioRegistrazione</column-name><column-value><![CDATA[");
        sb.append(getEsercizioRegistrazione());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceSoggetto</column-name><column-value><![CDATA[");
        sb.append(getCodiceSoggetto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>numeroPartita</column-name><column-value><![CDATA[");
        sb.append(getNumeroPartita());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>numeroScadenza</column-name><column-value><![CDATA[");
        sb.append(getNumeroScadenza());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tipoSoggetto</column-name><column-value><![CDATA[");
        sb.append(getTipoSoggetto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>esercizioRegistrazioneComp</column-name><column-value><![CDATA[");
        sb.append(getEsercizioRegistrazioneComp());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceSoggettoComp</column-name><column-value><![CDATA[");
        sb.append(getCodiceSoggettoComp());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>numeroPartitaComp</column-name><column-value><![CDATA[");
        sb.append(getNumeroPartitaComp());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>numeroScadenzaComp</column-name><column-value><![CDATA[");
        sb.append(getNumeroScadenzaComp());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tipoSoggettoComp</column-name><column-value><![CDATA[");
        sb.append(getTipoSoggettoComp());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>differenza</column-name><column-value><![CDATA[");
        sb.append(getDifferenza());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>idPagamento</column-name><column-value><![CDATA[");
        sb.append(getIdPagamento());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>annoPagamento</column-name><column-value><![CDATA[");
        sb.append(getAnnoPagamento());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>stato</column-name><column-value><![CDATA[");
        sb.append(getStato());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>nota</column-name><column-value><![CDATA[");
        sb.append(getNota());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
