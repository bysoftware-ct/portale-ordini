package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.PianteServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.PianteServiceSoap
 * @generated
 */
public class PianteSoap implements Serializable {
    private long _id;
    private String _nome;
    private String _vaso;
    private double _prezzo;
    private int _altezza;
    private int _piantePianale;
    private int _pianteCarrello;
    private boolean _attiva;
    private String _foto1;
    private String _foto2;
    private String _foto3;
    private int _altezzaPianta;
    private long _idCategoria;
    private String _idFornitore;
    private double _prezzoFornitore;
    private int _disponibilita;
    private String _codice;
    private String _forma;
    private int _altezzaChioma;
    private String _tempoConsegna;
    private double _prezzoB;
    private double _percScontoMistoAziendale;
    private double _prezzoACarrello;
    private double _prezzoBCarrello;
    private long _idForma;
    private double _costoRipiano;
    private Date _ultimoAggiornamentoFoto;
    private Date _scadenzaFoto;
    private int _giorniScadenzaFoto;
    private int _sormonto2Fila;
    private int _sormonto3Fila;
    private int _sormonto4Fila;
    private int _aggiuntaRipiano;
    private String _ean;
    private boolean _passaporto;
    private boolean _bloccoDisponibilita;
    private String _pathFile;
    private boolean _obsoleto;
    private double _prezzoEtichetta;
    private String _passaportoDefault;

    public PianteSoap() {
    }

    public static PianteSoap toSoapModel(Piante model) {
        PianteSoap soapModel = new PianteSoap();

        soapModel.setId(model.getId());
        soapModel.setNome(model.getNome());
        soapModel.setVaso(model.getVaso());
        soapModel.setPrezzo(model.getPrezzo());
        soapModel.setAltezza(model.getAltezza());
        soapModel.setPiantePianale(model.getPiantePianale());
        soapModel.setPianteCarrello(model.getPianteCarrello());
        soapModel.setAttiva(model.getAttiva());
        soapModel.setFoto1(model.getFoto1());
        soapModel.setFoto2(model.getFoto2());
        soapModel.setFoto3(model.getFoto3());
        soapModel.setAltezzaPianta(model.getAltezzaPianta());
        soapModel.setIdCategoria(model.getIdCategoria());
        soapModel.setIdFornitore(model.getIdFornitore());
        soapModel.setPrezzoFornitore(model.getPrezzoFornitore());
        soapModel.setDisponibilita(model.getDisponibilita());
        soapModel.setCodice(model.getCodice());
        soapModel.setForma(model.getForma());
        soapModel.setAltezzaChioma(model.getAltezzaChioma());
        soapModel.setTempoConsegna(model.getTempoConsegna());
        soapModel.setPrezzoB(model.getPrezzoB());
        soapModel.setPercScontoMistoAziendale(model.getPercScontoMistoAziendale());
        soapModel.setPrezzoACarrello(model.getPrezzoACarrello());
        soapModel.setPrezzoBCarrello(model.getPrezzoBCarrello());
        soapModel.setIdForma(model.getIdForma());
        soapModel.setCostoRipiano(model.getCostoRipiano());
        soapModel.setUltimoAggiornamentoFoto(model.getUltimoAggiornamentoFoto());
        soapModel.setScadenzaFoto(model.getScadenzaFoto());
        soapModel.setGiorniScadenzaFoto(model.getGiorniScadenzaFoto());
        soapModel.setSormonto2Fila(model.getSormonto2Fila());
        soapModel.setSormonto3Fila(model.getSormonto3Fila());
        soapModel.setSormonto4Fila(model.getSormonto4Fila());
        soapModel.setAggiuntaRipiano(model.getAggiuntaRipiano());
        soapModel.setEan(model.getEan());
        soapModel.setPassaporto(model.getPassaporto());
        soapModel.setBloccoDisponibilita(model.getBloccoDisponibilita());
        soapModel.setPathFile(model.getPathFile());
        soapModel.setObsoleto(model.getObsoleto());
        soapModel.setPrezzoEtichetta(model.getPrezzoEtichetta());
        soapModel.setPassaportoDefault(model.getPassaportoDefault());

        return soapModel;
    }

    public static PianteSoap[] toSoapModels(Piante[] models) {
        PianteSoap[] soapModels = new PianteSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static PianteSoap[][] toSoapModels(Piante[][] models) {
        PianteSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new PianteSoap[models.length][models[0].length];
        } else {
            soapModels = new PianteSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static PianteSoap[] toSoapModels(List<Piante> models) {
        List<PianteSoap> soapModels = new ArrayList<PianteSoap>(models.size());

        for (Piante model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new PianteSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public String getNome() {
        return _nome;
    }

    public void setNome(String nome) {
        _nome = nome;
    }

    public String getVaso() {
        return _vaso;
    }

    public void setVaso(String vaso) {
        _vaso = vaso;
    }

    public double getPrezzo() {
        return _prezzo;
    }

    public void setPrezzo(double prezzo) {
        _prezzo = prezzo;
    }

    public int getAltezza() {
        return _altezza;
    }

    public void setAltezza(int altezza) {
        _altezza = altezza;
    }

    public int getPiantePianale() {
        return _piantePianale;
    }

    public void setPiantePianale(int piantePianale) {
        _piantePianale = piantePianale;
    }

    public int getPianteCarrello() {
        return _pianteCarrello;
    }

    public void setPianteCarrello(int pianteCarrello) {
        _pianteCarrello = pianteCarrello;
    }

    public boolean getAttiva() {
        return _attiva;
    }

    public boolean isAttiva() {
        return _attiva;
    }

    public void setAttiva(boolean attiva) {
        _attiva = attiva;
    }

    public String getFoto1() {
        return _foto1;
    }

    public void setFoto1(String foto1) {
        _foto1 = foto1;
    }

    public String getFoto2() {
        return _foto2;
    }

    public void setFoto2(String foto2) {
        _foto2 = foto2;
    }

    public String getFoto3() {
        return _foto3;
    }

    public void setFoto3(String foto3) {
        _foto3 = foto3;
    }

    public int getAltezzaPianta() {
        return _altezzaPianta;
    }

    public void setAltezzaPianta(int altezzaPianta) {
        _altezzaPianta = altezzaPianta;
    }

    public long getIdCategoria() {
        return _idCategoria;
    }

    public void setIdCategoria(long idCategoria) {
        _idCategoria = idCategoria;
    }

    public String getIdFornitore() {
        return _idFornitore;
    }

    public void setIdFornitore(String idFornitore) {
        _idFornitore = idFornitore;
    }

    public double getPrezzoFornitore() {
        return _prezzoFornitore;
    }

    public void setPrezzoFornitore(double prezzoFornitore) {
        _prezzoFornitore = prezzoFornitore;
    }

    public int getDisponibilita() {
        return _disponibilita;
    }

    public void setDisponibilita(int disponibilita) {
        _disponibilita = disponibilita;
    }

    public String getCodice() {
        return _codice;
    }

    public void setCodice(String codice) {
        _codice = codice;
    }

    public String getForma() {
        return _forma;
    }

    public void setForma(String forma) {
        _forma = forma;
    }

    public int getAltezzaChioma() {
        return _altezzaChioma;
    }

    public void setAltezzaChioma(int altezzaChioma) {
        _altezzaChioma = altezzaChioma;
    }

    public String getTempoConsegna() {
        return _tempoConsegna;
    }

    public void setTempoConsegna(String tempoConsegna) {
        _tempoConsegna = tempoConsegna;
    }

    public double getPrezzoB() {
        return _prezzoB;
    }

    public void setPrezzoB(double prezzoB) {
        _prezzoB = prezzoB;
    }

    public double getPercScontoMistoAziendale() {
        return _percScontoMistoAziendale;
    }

    public void setPercScontoMistoAziendale(double percScontoMistoAziendale) {
        _percScontoMistoAziendale = percScontoMistoAziendale;
    }

    public double getPrezzoACarrello() {
        return _prezzoACarrello;
    }

    public void setPrezzoACarrello(double prezzoACarrello) {
        _prezzoACarrello = prezzoACarrello;
    }

    public double getPrezzoBCarrello() {
        return _prezzoBCarrello;
    }

    public void setPrezzoBCarrello(double prezzoBCarrello) {
        _prezzoBCarrello = prezzoBCarrello;
    }

    public long getIdForma() {
        return _idForma;
    }

    public void setIdForma(long idForma) {
        _idForma = idForma;
    }

    public double getCostoRipiano() {
        return _costoRipiano;
    }

    public void setCostoRipiano(double costoRipiano) {
        _costoRipiano = costoRipiano;
    }

    public Date getUltimoAggiornamentoFoto() {
        return _ultimoAggiornamentoFoto;
    }

    public void setUltimoAggiornamentoFoto(Date ultimoAggiornamentoFoto) {
        _ultimoAggiornamentoFoto = ultimoAggiornamentoFoto;
    }

    public Date getScadenzaFoto() {
        return _scadenzaFoto;
    }

    public void setScadenzaFoto(Date scadenzaFoto) {
        _scadenzaFoto = scadenzaFoto;
    }

    public int getGiorniScadenzaFoto() {
        return _giorniScadenzaFoto;
    }

    public void setGiorniScadenzaFoto(int giorniScadenzaFoto) {
        _giorniScadenzaFoto = giorniScadenzaFoto;
    }

    public int getSormonto2Fila() {
        return _sormonto2Fila;
    }

    public void setSormonto2Fila(int sormonto2Fila) {
        _sormonto2Fila = sormonto2Fila;
    }

    public int getSormonto3Fila() {
        return _sormonto3Fila;
    }

    public void setSormonto3Fila(int sormonto3Fila) {
        _sormonto3Fila = sormonto3Fila;
    }

    public int getSormonto4Fila() {
        return _sormonto4Fila;
    }

    public void setSormonto4Fila(int sormonto4Fila) {
        _sormonto4Fila = sormonto4Fila;
    }

    public int getAggiuntaRipiano() {
        return _aggiuntaRipiano;
    }

    public void setAggiuntaRipiano(int aggiuntaRipiano) {
        _aggiuntaRipiano = aggiuntaRipiano;
    }

    public String getEan() {
        return _ean;
    }

    public void setEan(String ean) {
        _ean = ean;
    }

    public boolean getPassaporto() {
        return _passaporto;
    }

    public boolean isPassaporto() {
        return _passaporto;
    }

    public void setPassaporto(boolean passaporto) {
        _passaporto = passaporto;
    }

    public boolean getBloccoDisponibilita() {
        return _bloccoDisponibilita;
    }

    public boolean isBloccoDisponibilita() {
        return _bloccoDisponibilita;
    }

    public void setBloccoDisponibilita(boolean bloccoDisponibilita) {
        _bloccoDisponibilita = bloccoDisponibilita;
    }

    public String getPathFile() {
        return _pathFile;
    }

    public void setPathFile(String pathFile) {
        _pathFile = pathFile;
    }

    public boolean getObsoleto() {
        return _obsoleto;
    }

    public boolean isObsoleto() {
        return _obsoleto;
    }

    public void setObsoleto(boolean obsoleto) {
        _obsoleto = obsoleto;
    }

    public double getPrezzoEtichetta() {
        return _prezzoEtichetta;
    }

    public void setPrezzoEtichetta(double prezzoEtichetta) {
        _prezzoEtichetta = prezzoEtichetta;
    }

    public String getPassaportoDefault() {
        return _passaportoDefault;
    }

    public void setPassaportoDefault(String passaportoDefault) {
        _passaportoDefault = passaportoDefault;
    }
}
