package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.TipoCarrelloLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class TipoCarrelloClp extends BaseModelImpl<TipoCarrello>
    implements TipoCarrello {
    private long _id;
    private String _tipologia;
    private BaseModel<?> _tipoCarrelloRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public TipoCarrelloClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return TipoCarrello.class;
    }

    @Override
    public String getModelClassName() {
        return TipoCarrello.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("tipologia", getTipologia());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        String tipologia = (String) attributes.get("tipologia");

        if (tipologia != null) {
            setTipologia(tipologia);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_tipoCarrelloRemoteModel != null) {
            try {
                Class<?> clazz = _tipoCarrelloRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_tipoCarrelloRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getTipologia() {
        return _tipologia;
    }

    @Override
    public void setTipologia(String tipologia) {
        _tipologia = tipologia;

        if (_tipoCarrelloRemoteModel != null) {
            try {
                Class<?> clazz = _tipoCarrelloRemoteModel.getClass();

                Method method = clazz.getMethod("setTipologia", String.class);

                method.invoke(_tipoCarrelloRemoteModel, tipologia);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getTipoCarrelloRemoteModel() {
        return _tipoCarrelloRemoteModel;
    }

    public void setTipoCarrelloRemoteModel(BaseModel<?> tipoCarrelloRemoteModel) {
        _tipoCarrelloRemoteModel = tipoCarrelloRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _tipoCarrelloRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_tipoCarrelloRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            TipoCarrelloLocalServiceUtil.addTipoCarrello(this);
        } else {
            TipoCarrelloLocalServiceUtil.updateTipoCarrello(this);
        }
    }

    @Override
    public TipoCarrello toEscapedModel() {
        return (TipoCarrello) ProxyUtil.newProxyInstance(TipoCarrello.class.getClassLoader(),
            new Class[] { TipoCarrello.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        TipoCarrelloClp clone = new TipoCarrelloClp();

        clone.setId(getId());
        clone.setTipologia(getTipologia());

        return clone;
    }

    @Override
    public int compareTo(TipoCarrello tipoCarrello) {
        long primaryKey = tipoCarrello.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof TipoCarrelloClp)) {
            return false;
        }

        TipoCarrelloClp tipoCarrello = (TipoCarrelloClp) obj;

        long primaryKey = tipoCarrello.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(5);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", tipologia=");
        sb.append(getTipologia());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(10);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.TipoCarrello");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tipologia</column-name><column-value><![CDATA[");
        sb.append(getTipologia());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
