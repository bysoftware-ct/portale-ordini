package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.CategorieLocalServiceUtil;
import it.bysoftware.ct.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class CategorieClp extends BaseModelImpl<Categorie> implements Categorie {
    private long _id;
    private String _nomeIta;
    private String _nomeEng;
    private String _nomeTed;
    private String _nomeFra;
    private String _nomeSpa;
    private boolean _predefinita;
    private BaseModel<?> _categorieRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public CategorieClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Categorie.class;
    }

    @Override
    public String getModelClassName() {
        return Categorie.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("nomeIta", getNomeIta());
        attributes.put("nomeEng", getNomeEng());
        attributes.put("nomeTed", getNomeTed());
        attributes.put("nomeFra", getNomeFra());
        attributes.put("nomeSpa", getNomeSpa());
        attributes.put("predefinita", getPredefinita());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        String nomeIta = (String) attributes.get("nomeIta");

        if (nomeIta != null) {
            setNomeIta(nomeIta);
        }

        String nomeEng = (String) attributes.get("nomeEng");

        if (nomeEng != null) {
            setNomeEng(nomeEng);
        }

        String nomeTed = (String) attributes.get("nomeTed");

        if (nomeTed != null) {
            setNomeTed(nomeTed);
        }

        String nomeFra = (String) attributes.get("nomeFra");

        if (nomeFra != null) {
            setNomeFra(nomeFra);
        }

        String nomeSpa = (String) attributes.get("nomeSpa");

        if (nomeSpa != null) {
            setNomeSpa(nomeSpa);
        }

        Boolean predefinita = (Boolean) attributes.get("predefinita");

        if (predefinita != null) {
            setPredefinita(predefinita);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_categorieRemoteModel != null) {
            try {
                Class<?> clazz = _categorieRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_categorieRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNomeIta() {
        return _nomeIta;
    }

    @Override
    public void setNomeIta(String nomeIta) {
        _nomeIta = nomeIta;

        if (_categorieRemoteModel != null) {
            try {
                Class<?> clazz = _categorieRemoteModel.getClass();

                Method method = clazz.getMethod("setNomeIta", String.class);

                method.invoke(_categorieRemoteModel, nomeIta);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNomeEng() {
        return _nomeEng;
    }

    @Override
    public void setNomeEng(String nomeEng) {
        _nomeEng = nomeEng;

        if (_categorieRemoteModel != null) {
            try {
                Class<?> clazz = _categorieRemoteModel.getClass();

                Method method = clazz.getMethod("setNomeEng", String.class);

                method.invoke(_categorieRemoteModel, nomeEng);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNomeTed() {
        return _nomeTed;
    }

    @Override
    public void setNomeTed(String nomeTed) {
        _nomeTed = nomeTed;

        if (_categorieRemoteModel != null) {
            try {
                Class<?> clazz = _categorieRemoteModel.getClass();

                Method method = clazz.getMethod("setNomeTed", String.class);

                method.invoke(_categorieRemoteModel, nomeTed);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNomeFra() {
        return _nomeFra;
    }

    @Override
    public void setNomeFra(String nomeFra) {
        _nomeFra = nomeFra;

        if (_categorieRemoteModel != null) {
            try {
                Class<?> clazz = _categorieRemoteModel.getClass();

                Method method = clazz.getMethod("setNomeFra", String.class);

                method.invoke(_categorieRemoteModel, nomeFra);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNomeSpa() {
        return _nomeSpa;
    }

    @Override
    public void setNomeSpa(String nomeSpa) {
        _nomeSpa = nomeSpa;

        if (_categorieRemoteModel != null) {
            try {
                Class<?> clazz = _categorieRemoteModel.getClass();

                Method method = clazz.getMethod("setNomeSpa", String.class);

                method.invoke(_categorieRemoteModel, nomeSpa);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getPredefinita() {
        return _predefinita;
    }

    @Override
    public boolean isPredefinita() {
        return _predefinita;
    }

    @Override
    public void setPredefinita(boolean predefinita) {
        _predefinita = predefinita;

        if (_categorieRemoteModel != null) {
            try {
                Class<?> clazz = _categorieRemoteModel.getClass();

                Method method = clazz.getMethod("setPredefinita", boolean.class);

                method.invoke(_categorieRemoteModel, predefinita);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getCategorieRemoteModel() {
        return _categorieRemoteModel;
    }

    public void setCategorieRemoteModel(BaseModel<?> categorieRemoteModel) {
        _categorieRemoteModel = categorieRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _categorieRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_categorieRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            CategorieLocalServiceUtil.addCategorie(this);
        } else {
            CategorieLocalServiceUtil.updateCategorie(this);
        }
    }

    @Override
    public Categorie toEscapedModel() {
        return (Categorie) ProxyUtil.newProxyInstance(Categorie.class.getClassLoader(),
            new Class[] { Categorie.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        CategorieClp clone = new CategorieClp();

        clone.setId(getId());
        clone.setNomeIta(getNomeIta());
        clone.setNomeEng(getNomeEng());
        clone.setNomeTed(getNomeTed());
        clone.setNomeFra(getNomeFra());
        clone.setNomeSpa(getNomeSpa());
        clone.setPredefinita(getPredefinita());

        return clone;
    }

    @Override
    public int compareTo(Categorie categorie) {
        long primaryKey = categorie.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CategorieClp)) {
            return false;
        }

        CategorieClp categorie = (CategorieClp) obj;

        long primaryKey = categorie.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(15);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", nomeIta=");
        sb.append(getNomeIta());
        sb.append(", nomeEng=");
        sb.append(getNomeEng());
        sb.append(", nomeTed=");
        sb.append(getNomeTed());
        sb.append(", nomeFra=");
        sb.append(getNomeFra());
        sb.append(", nomeSpa=");
        sb.append(getNomeSpa());
        sb.append(", predefinita=");
        sb.append(getPredefinita());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(25);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.Categorie");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>nomeIta</column-name><column-value><![CDATA[");
        sb.append(getNomeIta());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>nomeEng</column-name><column-value><![CDATA[");
        sb.append(getNomeEng());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>nomeTed</column-name><column-value><![CDATA[");
        sb.append(getNomeTed());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>nomeFra</column-name><column-value><![CDATA[");
        sb.append(getNomeFra());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>nomeSpa</column-name><column-value><![CDATA[");
        sb.append(getNomeSpa());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>predefinita</column-name><column-value><![CDATA[");
        sb.append(getPredefinita());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
