package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.DettagliOperazioniContabiliServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.DettagliOperazioniContabiliServiceSoap
 * @generated
 */
public class DettagliOperazioniContabiliSoap implements Serializable {
    private String _codiceOperazione;
    private int _progressivoLegame;
    private int _tipoLegame;
    private String _causaleContabile;
    private String _sottoconto;
    private String _naturaConto;
    private int _tipoImporto;

    public DettagliOperazioniContabiliSoap() {
    }

    public static DettagliOperazioniContabiliSoap toSoapModel(
        DettagliOperazioniContabili model) {
        DettagliOperazioniContabiliSoap soapModel = new DettagliOperazioniContabiliSoap();

        soapModel.setCodiceOperazione(model.getCodiceOperazione());
        soapModel.setProgressivoLegame(model.getProgressivoLegame());
        soapModel.setTipoLegame(model.getTipoLegame());
        soapModel.setCausaleContabile(model.getCausaleContabile());
        soapModel.setSottoconto(model.getSottoconto());
        soapModel.setNaturaConto(model.getNaturaConto());
        soapModel.setTipoImporto(model.getTipoImporto());

        return soapModel;
    }

    public static DettagliOperazioniContabiliSoap[] toSoapModels(
        DettagliOperazioniContabili[] models) {
        DettagliOperazioniContabiliSoap[] soapModels = new DettagliOperazioniContabiliSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static DettagliOperazioniContabiliSoap[][] toSoapModels(
        DettagliOperazioniContabili[][] models) {
        DettagliOperazioniContabiliSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new DettagliOperazioniContabiliSoap[models.length][models[0].length];
        } else {
            soapModels = new DettagliOperazioniContabiliSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static DettagliOperazioniContabiliSoap[] toSoapModels(
        List<DettagliOperazioniContabili> models) {
        List<DettagliOperazioniContabiliSoap> soapModels = new ArrayList<DettagliOperazioniContabiliSoap>(models.size());

        for (DettagliOperazioniContabili model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new DettagliOperazioniContabiliSoap[soapModels.size()]);
    }

    public DettagliOperazioniContabiliPK getPrimaryKey() {
        return new DettagliOperazioniContabiliPK(_codiceOperazione,
            _progressivoLegame);
    }

    public void setPrimaryKey(DettagliOperazioniContabiliPK pk) {
        setCodiceOperazione(pk.codiceOperazione);
        setProgressivoLegame(pk.progressivoLegame);
    }

    public String getCodiceOperazione() {
        return _codiceOperazione;
    }

    public void setCodiceOperazione(String codiceOperazione) {
        _codiceOperazione = codiceOperazione;
    }

    public int getProgressivoLegame() {
        return _progressivoLegame;
    }

    public void setProgressivoLegame(int progressivoLegame) {
        _progressivoLegame = progressivoLegame;
    }

    public int getTipoLegame() {
        return _tipoLegame;
    }

    public void setTipoLegame(int tipoLegame) {
        _tipoLegame = tipoLegame;
    }

    public String getCausaleContabile() {
        return _causaleContabile;
    }

    public void setCausaleContabile(String causaleContabile) {
        _causaleContabile = causaleContabile;
    }

    public String getSottoconto() {
        return _sottoconto;
    }

    public void setSottoconto(String sottoconto) {
        _sottoconto = sottoconto;
    }

    public String getNaturaConto() {
        return _naturaConto;
    }

    public void setNaturaConto(String naturaConto) {
        _naturaConto = naturaConto;
    }

    public int getTipoImporto() {
        return _tipoImporto;
    }

    public void setTipoImporto(int tipoImporto) {
        _tipoImporto = tipoImporto;
    }
}
