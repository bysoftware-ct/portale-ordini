package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.StatiLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class StatiClp extends BaseModelImpl<Stati> implements Stati {
    private boolean _black;
    private String _codiceStato;
    private String _codiceISO;
    private String _codicePaese;
    private String _stato;
    private String _codiceAzienda;
    private BaseModel<?> _statiRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public StatiClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Stati.class;
    }

    @Override
    public String getModelClassName() {
        return Stati.class.getName();
    }

    @Override
    public String getPrimaryKey() {
        return _codiceStato;
    }

    @Override
    public void setPrimaryKey(String primaryKey) {
        setCodiceStato(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _codiceStato;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((String) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("black", getBlack());
        attributes.put("codiceStato", getCodiceStato());
        attributes.put("codiceISO", getCodiceISO());
        attributes.put("codicePaese", getCodicePaese());
        attributes.put("stato", getStato());
        attributes.put("codiceAzienda", getCodiceAzienda());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Boolean black = (Boolean) attributes.get("black");

        if (black != null) {
            setBlack(black);
        }

        String codiceStato = (String) attributes.get("codiceStato");

        if (codiceStato != null) {
            setCodiceStato(codiceStato);
        }

        String codiceISO = (String) attributes.get("codiceISO");

        if (codiceISO != null) {
            setCodiceISO(codiceISO);
        }

        String codicePaese = (String) attributes.get("codicePaese");

        if (codicePaese != null) {
            setCodicePaese(codicePaese);
        }

        String stato = (String) attributes.get("stato");

        if (stato != null) {
            setStato(stato);
        }

        String codiceAzienda = (String) attributes.get("codiceAzienda");

        if (codiceAzienda != null) {
            setCodiceAzienda(codiceAzienda);
        }
    }

    @Override
    public boolean getBlack() {
        return _black;
    }

    @Override
    public boolean isBlack() {
        return _black;
    }

    @Override
    public void setBlack(boolean black) {
        _black = black;

        if (_statiRemoteModel != null) {
            try {
                Class<?> clazz = _statiRemoteModel.getClass();

                Method method = clazz.getMethod("setBlack", boolean.class);

                method.invoke(_statiRemoteModel, black);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceStato() {
        return _codiceStato;
    }

    @Override
    public void setCodiceStato(String codiceStato) {
        _codiceStato = codiceStato;

        if (_statiRemoteModel != null) {
            try {
                Class<?> clazz = _statiRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceStato", String.class);

                method.invoke(_statiRemoteModel, codiceStato);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceISO() {
        return _codiceISO;
    }

    @Override
    public void setCodiceISO(String codiceISO) {
        _codiceISO = codiceISO;

        if (_statiRemoteModel != null) {
            try {
                Class<?> clazz = _statiRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceISO", String.class);

                method.invoke(_statiRemoteModel, codiceISO);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodicePaese() {
        return _codicePaese;
    }

    @Override
    public void setCodicePaese(String codicePaese) {
        _codicePaese = codicePaese;

        if (_statiRemoteModel != null) {
            try {
                Class<?> clazz = _statiRemoteModel.getClass();

                Method method = clazz.getMethod("setCodicePaese", String.class);

                method.invoke(_statiRemoteModel, codicePaese);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getStato() {
        return _stato;
    }

    @Override
    public void setStato(String stato) {
        _stato = stato;

        if (_statiRemoteModel != null) {
            try {
                Class<?> clazz = _statiRemoteModel.getClass();

                Method method = clazz.getMethod("setStato", String.class);

                method.invoke(_statiRemoteModel, stato);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceAzienda() {
        return _codiceAzienda;
    }

    @Override
    public void setCodiceAzienda(String codiceAzienda) {
        _codiceAzienda = codiceAzienda;

        if (_statiRemoteModel != null) {
            try {
                Class<?> clazz = _statiRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceAzienda", String.class);

                method.invoke(_statiRemoteModel, codiceAzienda);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getStatiRemoteModel() {
        return _statiRemoteModel;
    }

    public void setStatiRemoteModel(BaseModel<?> statiRemoteModel) {
        _statiRemoteModel = statiRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _statiRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_statiRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            StatiLocalServiceUtil.addStati(this);
        } else {
            StatiLocalServiceUtil.updateStati(this);
        }
    }

    @Override
    public Stati toEscapedModel() {
        return (Stati) ProxyUtil.newProxyInstance(Stati.class.getClassLoader(),
            new Class[] { Stati.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        StatiClp clone = new StatiClp();

        clone.setBlack(getBlack());
        clone.setCodiceStato(getCodiceStato());
        clone.setCodiceISO(getCodiceISO());
        clone.setCodicePaese(getCodicePaese());
        clone.setStato(getStato());
        clone.setCodiceAzienda(getCodiceAzienda());

        return clone;
    }

    @Override
    public int compareTo(Stati stati) {
        int value = 0;

        value = getStato().compareTo(stati.getStato());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof StatiClp)) {
            return false;
        }

        StatiClp stati = (StatiClp) obj;

        String primaryKey = stati.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(13);

        sb.append("{black=");
        sb.append(getBlack());
        sb.append(", codiceStato=");
        sb.append(getCodiceStato());
        sb.append(", codiceISO=");
        sb.append(getCodiceISO());
        sb.append(", codicePaese=");
        sb.append(getCodicePaese());
        sb.append(", stato=");
        sb.append(getStato());
        sb.append(", codiceAzienda=");
        sb.append(getCodiceAzienda());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(22);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.Stati");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>black</column-name><column-value><![CDATA[");
        sb.append(getBlack());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceStato</column-name><column-value><![CDATA[");
        sb.append(getCodiceStato());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceISO</column-name><column-value><![CDATA[");
        sb.append(getCodiceISO());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codicePaese</column-name><column-value><![CDATA[");
        sb.append(getCodicePaese());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>stato</column-name><column-value><![CDATA[");
        sb.append(getStato());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceAzienda</column-name><column-value><![CDATA[");
        sb.append(getCodiceAzienda());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
