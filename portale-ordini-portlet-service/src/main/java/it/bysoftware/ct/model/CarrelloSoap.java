package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.CarrelloServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.CarrelloServiceSoap
 * @generated
 */
public class CarrelloSoap implements Serializable {
    private long _id;
    private long _idOrdine;
    private long _progressivo;
    private double _importo;
    private boolean _chiuso;
    private boolean _generico;
    private boolean _lavorato;

    public CarrelloSoap() {
    }

    public static CarrelloSoap toSoapModel(Carrello model) {
        CarrelloSoap soapModel = new CarrelloSoap();

        soapModel.setId(model.getId());
        soapModel.setIdOrdine(model.getIdOrdine());
        soapModel.setProgressivo(model.getProgressivo());
        soapModel.setImporto(model.getImporto());
        soapModel.setChiuso(model.getChiuso());
        soapModel.setGenerico(model.getGenerico());
        soapModel.setLavorato(model.getLavorato());

        return soapModel;
    }

    public static CarrelloSoap[] toSoapModels(Carrello[] models) {
        CarrelloSoap[] soapModels = new CarrelloSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static CarrelloSoap[][] toSoapModels(Carrello[][] models) {
        CarrelloSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new CarrelloSoap[models.length][models[0].length];
        } else {
            soapModels = new CarrelloSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static CarrelloSoap[] toSoapModels(List<Carrello> models) {
        List<CarrelloSoap> soapModels = new ArrayList<CarrelloSoap>(models.size());

        for (Carrello model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new CarrelloSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getIdOrdine() {
        return _idOrdine;
    }

    public void setIdOrdine(long idOrdine) {
        _idOrdine = idOrdine;
    }

    public long getProgressivo() {
        return _progressivo;
    }

    public void setProgressivo(long progressivo) {
        _progressivo = progressivo;
    }

    public double getImporto() {
        return _importo;
    }

    public void setImporto(double importo) {
        _importo = importo;
    }

    public boolean getChiuso() {
        return _chiuso;
    }

    public boolean isChiuso() {
        return _chiuso;
    }

    public void setChiuso(boolean chiuso) {
        _chiuso = chiuso;
    }

    public boolean getGenerico() {
        return _generico;
    }

    public boolean isGenerico() {
        return _generico;
    }

    public void setGenerico(boolean generico) {
        _generico = generico;
    }

    public boolean getLavorato() {
        return _lavorato;
    }

    public boolean isLavorato() {
        return _lavorato;
    }

    public void setLavorato(boolean lavorato) {
        _lavorato = lavorato;
    }
}
