package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Varianti}.
 * </p>
 *
 * @author Mario Torrisi
 * @see Varianti
 * @generated
 */
public class VariantiWrapper implements Varianti, ModelWrapper<Varianti> {
    private Varianti _varianti;

    public VariantiWrapper(Varianti varianti) {
        _varianti = varianti;
    }

    @Override
    public Class<?> getModelClass() {
        return Varianti.class;
    }

    @Override
    public String getModelClassName() {
        return Varianti.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceVariante", getCodiceVariante());
        attributes.put("tipoVariante", getTipoVariante());
        attributes.put("descrizione", getDescrizione());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceVariante = (String) attributes.get("codiceVariante");

        if (codiceVariante != null) {
            setCodiceVariante(codiceVariante);
        }

        String tipoVariante = (String) attributes.get("tipoVariante");

        if (tipoVariante != null) {
            setTipoVariante(tipoVariante);
        }

        String descrizione = (String) attributes.get("descrizione");

        if (descrizione != null) {
            setDescrizione(descrizione);
        }
    }

    /**
    * Returns the primary key of this varianti.
    *
    * @return the primary key of this varianti
    */
    @Override
    public it.bysoftware.ct.service.persistence.VariantiPK getPrimaryKey() {
        return _varianti.getPrimaryKey();
    }

    /**
    * Sets the primary key of this varianti.
    *
    * @param primaryKey the primary key of this varianti
    */
    @Override
    public void setPrimaryKey(
        it.bysoftware.ct.service.persistence.VariantiPK primaryKey) {
        _varianti.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the codice variante of this varianti.
    *
    * @return the codice variante of this varianti
    */
    @Override
    public java.lang.String getCodiceVariante() {
        return _varianti.getCodiceVariante();
    }

    /**
    * Sets the codice variante of this varianti.
    *
    * @param codiceVariante the codice variante of this varianti
    */
    @Override
    public void setCodiceVariante(java.lang.String codiceVariante) {
        _varianti.setCodiceVariante(codiceVariante);
    }

    /**
    * Returns the tipo variante of this varianti.
    *
    * @return the tipo variante of this varianti
    */
    @Override
    public java.lang.String getTipoVariante() {
        return _varianti.getTipoVariante();
    }

    /**
    * Sets the tipo variante of this varianti.
    *
    * @param tipoVariante the tipo variante of this varianti
    */
    @Override
    public void setTipoVariante(java.lang.String tipoVariante) {
        _varianti.setTipoVariante(tipoVariante);
    }

    /**
    * Returns the descrizione of this varianti.
    *
    * @return the descrizione of this varianti
    */
    @Override
    public java.lang.String getDescrizione() {
        return _varianti.getDescrizione();
    }

    /**
    * Sets the descrizione of this varianti.
    *
    * @param descrizione the descrizione of this varianti
    */
    @Override
    public void setDescrizione(java.lang.String descrizione) {
        _varianti.setDescrizione(descrizione);
    }

    @Override
    public boolean isNew() {
        return _varianti.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _varianti.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _varianti.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _varianti.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _varianti.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _varianti.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _varianti.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _varianti.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _varianti.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _varianti.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _varianti.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new VariantiWrapper((Varianti) _varianti.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.Varianti varianti) {
        return _varianti.compareTo(varianti);
    }

    @Override
    public int hashCode() {
        return _varianti.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.Varianti> toCacheModel() {
        return _varianti.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.Varianti toEscapedModel() {
        return new VariantiWrapper(_varianti.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.Varianti toUnescapedModel() {
        return new VariantiWrapper(_varianti.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _varianti.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _varianti.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _varianti.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VariantiWrapper)) {
            return false;
        }

        VariantiWrapper variantiWrapper = (VariantiWrapper) obj;

        if (Validator.equals(_varianti, variantiWrapper._varianti)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Varianti getWrappedVarianti() {
        return _varianti;
    }

    @Override
    public Varianti getWrappedModel() {
        return _varianti;
    }

    @Override
    public void resetOriginalValues() {
        _varianti.resetOriginalValues();
    }
}
