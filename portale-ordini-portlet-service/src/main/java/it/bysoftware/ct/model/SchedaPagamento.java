package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the SchedaPagamento service. Represents a row in the &quot;_schede_pagamenti&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see SchedaPagamentoModel
 * @see it.bysoftware.ct.model.impl.SchedaPagamentoImpl
 * @see it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl
 * @generated
 */
public interface SchedaPagamento extends SchedaPagamentoModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.SchedaPagamentoImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
