package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CausaliEstrattoConto}.
 * </p>
 *
 * @author Mario Torrisi
 * @see CausaliEstrattoConto
 * @generated
 */
public class CausaliEstrattoContoWrapper implements CausaliEstrattoConto,
    ModelWrapper<CausaliEstrattoConto> {
    private CausaliEstrattoConto _causaliEstrattoConto;

    public CausaliEstrattoContoWrapper(
        CausaliEstrattoConto causaliEstrattoConto) {
        _causaliEstrattoConto = causaliEstrattoConto;
    }

    @Override
    public Class<?> getModelClass() {
        return CausaliEstrattoConto.class;
    }

    @Override
    public String getModelClassName() {
        return CausaliEstrattoConto.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceCausaleEC", getCodiceCausaleEC());
        attributes.put("descrizioneCausaleEC", getDescrizioneCausaleEC());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceCausaleEC = (String) attributes.get("codiceCausaleEC");

        if (codiceCausaleEC != null) {
            setCodiceCausaleEC(codiceCausaleEC);
        }

        String descrizioneCausaleEC = (String) attributes.get(
                "descrizioneCausaleEC");

        if (descrizioneCausaleEC != null) {
            setDescrizioneCausaleEC(descrizioneCausaleEC);
        }
    }

    /**
    * Returns the primary key of this causali estratto conto.
    *
    * @return the primary key of this causali estratto conto
    */
    @Override
    public java.lang.String getPrimaryKey() {
        return _causaliEstrattoConto.getPrimaryKey();
    }

    /**
    * Sets the primary key of this causali estratto conto.
    *
    * @param primaryKey the primary key of this causali estratto conto
    */
    @Override
    public void setPrimaryKey(java.lang.String primaryKey) {
        _causaliEstrattoConto.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the codice causale e c of this causali estratto conto.
    *
    * @return the codice causale e c of this causali estratto conto
    */
    @Override
    public java.lang.String getCodiceCausaleEC() {
        return _causaliEstrattoConto.getCodiceCausaleEC();
    }

    /**
    * Sets the codice causale e c of this causali estratto conto.
    *
    * @param codiceCausaleEC the codice causale e c of this causali estratto conto
    */
    @Override
    public void setCodiceCausaleEC(java.lang.String codiceCausaleEC) {
        _causaliEstrattoConto.setCodiceCausaleEC(codiceCausaleEC);
    }

    /**
    * Returns the descrizione causale e c of this causali estratto conto.
    *
    * @return the descrizione causale e c of this causali estratto conto
    */
    @Override
    public java.lang.String getDescrizioneCausaleEC() {
        return _causaliEstrattoConto.getDescrizioneCausaleEC();
    }

    /**
    * Sets the descrizione causale e c of this causali estratto conto.
    *
    * @param descrizioneCausaleEC the descrizione causale e c of this causali estratto conto
    */
    @Override
    public void setDescrizioneCausaleEC(java.lang.String descrizioneCausaleEC) {
        _causaliEstrattoConto.setDescrizioneCausaleEC(descrizioneCausaleEC);
    }

    @Override
    public boolean isNew() {
        return _causaliEstrattoConto.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _causaliEstrattoConto.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _causaliEstrattoConto.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _causaliEstrattoConto.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _causaliEstrattoConto.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _causaliEstrattoConto.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _causaliEstrattoConto.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _causaliEstrattoConto.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _causaliEstrattoConto.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _causaliEstrattoConto.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _causaliEstrattoConto.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new CausaliEstrattoContoWrapper((CausaliEstrattoConto) _causaliEstrattoConto.clone());
    }

    @Override
    public int compareTo(
        it.bysoftware.ct.model.CausaliEstrattoConto causaliEstrattoConto) {
        return _causaliEstrattoConto.compareTo(causaliEstrattoConto);
    }

    @Override
    public int hashCode() {
        return _causaliEstrattoConto.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.CausaliEstrattoConto> toCacheModel() {
        return _causaliEstrattoConto.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.CausaliEstrattoConto toEscapedModel() {
        return new CausaliEstrattoContoWrapper(_causaliEstrattoConto.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.CausaliEstrattoConto toUnescapedModel() {
        return new CausaliEstrattoContoWrapper(_causaliEstrattoConto.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _causaliEstrattoConto.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _causaliEstrattoConto.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _causaliEstrattoConto.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CausaliEstrattoContoWrapper)) {
            return false;
        }

        CausaliEstrattoContoWrapper causaliEstrattoContoWrapper = (CausaliEstrattoContoWrapper) obj;

        if (Validator.equals(_causaliEstrattoConto,
                    causaliEstrattoContoWrapper._causaliEstrattoConto)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public CausaliEstrattoConto getWrappedCausaliEstrattoConto() {
        return _causaliEstrattoConto;
    }

    @Override
    public CausaliEstrattoConto getWrappedModel() {
        return _causaliEstrattoConto;
    }

    @Override
    public void resetOriginalValues() {
        _causaliEstrattoConto.resetOriginalValues();
    }
}
