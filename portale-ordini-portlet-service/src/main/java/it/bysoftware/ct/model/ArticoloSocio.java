package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the ArticoloSocio service. Represents a row in the &quot;_articoli_socio_consorzio&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see ArticoloSocioModel
 * @see it.bysoftware.ct.model.impl.ArticoloSocioImpl
 * @see it.bysoftware.ct.model.impl.ArticoloSocioModelImpl
 * @generated
 */
public interface ArticoloSocio extends ArticoloSocioModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.ArticoloSocioImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
