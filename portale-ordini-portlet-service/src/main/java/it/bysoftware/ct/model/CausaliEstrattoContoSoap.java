package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.CausaliEstrattoContoServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.CausaliEstrattoContoServiceSoap
 * @generated
 */
public class CausaliEstrattoContoSoap implements Serializable {
    private String _codiceCausaleEC;
    private String _descrizioneCausaleEC;

    public CausaliEstrattoContoSoap() {
    }

    public static CausaliEstrattoContoSoap toSoapModel(
        CausaliEstrattoConto model) {
        CausaliEstrattoContoSoap soapModel = new CausaliEstrattoContoSoap();

        soapModel.setCodiceCausaleEC(model.getCodiceCausaleEC());
        soapModel.setDescrizioneCausaleEC(model.getDescrizioneCausaleEC());

        return soapModel;
    }

    public static CausaliEstrattoContoSoap[] toSoapModels(
        CausaliEstrattoConto[] models) {
        CausaliEstrattoContoSoap[] soapModels = new CausaliEstrattoContoSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static CausaliEstrattoContoSoap[][] toSoapModels(
        CausaliEstrattoConto[][] models) {
        CausaliEstrattoContoSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new CausaliEstrattoContoSoap[models.length][models[0].length];
        } else {
            soapModels = new CausaliEstrattoContoSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static CausaliEstrattoContoSoap[] toSoapModels(
        List<CausaliEstrattoConto> models) {
        List<CausaliEstrattoContoSoap> soapModels = new ArrayList<CausaliEstrattoContoSoap>(models.size());

        for (CausaliEstrattoConto model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new CausaliEstrattoContoSoap[soapModels.size()]);
    }

    public String getPrimaryKey() {
        return _codiceCausaleEC;
    }

    public void setPrimaryKey(String pk) {
        setCodiceCausaleEC(pk);
    }

    public String getCodiceCausaleEC() {
        return _codiceCausaleEC;
    }

    public void setCodiceCausaleEC(String codiceCausaleEC) {
        _codiceCausaleEC = codiceCausaleEC;
    }

    public String getDescrizioneCausaleEC() {
        return _descrizioneCausaleEC;
    }

    public void setDescrizioneCausaleEC(String descrizioneCausaleEC) {
        _descrizioneCausaleEC = descrizioneCausaleEC;
    }
}
