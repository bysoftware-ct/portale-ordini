package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.CausaliContabiliLocalServiceUtil;
import it.bysoftware.ct.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class CausaliContabiliClp extends BaseModelImpl<CausaliContabili>
    implements CausaliContabili {
    private String _codiceCausaleCont;
    private String _codiceCausaleEC;
    private String _descrizioneCausaleCont;
    private int _testDareAvere;
    private int _tipoSoggettoMovimentato;
    private BaseModel<?> _causaliContabiliRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public CausaliContabiliClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return CausaliContabili.class;
    }

    @Override
    public String getModelClassName() {
        return CausaliContabili.class.getName();
    }

    @Override
    public String getPrimaryKey() {
        return _codiceCausaleCont;
    }

    @Override
    public void setPrimaryKey(String primaryKey) {
        setCodiceCausaleCont(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _codiceCausaleCont;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((String) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceCausaleCont", getCodiceCausaleCont());
        attributes.put("codiceCausaleEC", getCodiceCausaleEC());
        attributes.put("descrizioneCausaleCont", getDescrizioneCausaleCont());
        attributes.put("testDareAvere", getTestDareAvere());
        attributes.put("tipoSoggettoMovimentato", getTipoSoggettoMovimentato());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceCausaleCont = (String) attributes.get("codiceCausaleCont");

        if (codiceCausaleCont != null) {
            setCodiceCausaleCont(codiceCausaleCont);
        }

        String codiceCausaleEC = (String) attributes.get("codiceCausaleEC");

        if (codiceCausaleEC != null) {
            setCodiceCausaleEC(codiceCausaleEC);
        }

        String descrizioneCausaleCont = (String) attributes.get(
                "descrizioneCausaleCont");

        if (descrizioneCausaleCont != null) {
            setDescrizioneCausaleCont(descrizioneCausaleCont);
        }

        Integer testDareAvere = (Integer) attributes.get("testDareAvere");

        if (testDareAvere != null) {
            setTestDareAvere(testDareAvere);
        }

        Integer tipoSoggettoMovimentato = (Integer) attributes.get(
                "tipoSoggettoMovimentato");

        if (tipoSoggettoMovimentato != null) {
            setTipoSoggettoMovimentato(tipoSoggettoMovimentato);
        }
    }

    @Override
    public String getCodiceCausaleCont() {
        return _codiceCausaleCont;
    }

    @Override
    public void setCodiceCausaleCont(String codiceCausaleCont) {
        _codiceCausaleCont = codiceCausaleCont;

        if (_causaliContabiliRemoteModel != null) {
            try {
                Class<?> clazz = _causaliContabiliRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceCausaleCont",
                        String.class);

                method.invoke(_causaliContabiliRemoteModel, codiceCausaleCont);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceCausaleEC() {
        return _codiceCausaleEC;
    }

    @Override
    public void setCodiceCausaleEC(String codiceCausaleEC) {
        _codiceCausaleEC = codiceCausaleEC;

        if (_causaliContabiliRemoteModel != null) {
            try {
                Class<?> clazz = _causaliContabiliRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceCausaleEC",
                        String.class);

                method.invoke(_causaliContabiliRemoteModel, codiceCausaleEC);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getDescrizioneCausaleCont() {
        return _descrizioneCausaleCont;
    }

    @Override
    public void setDescrizioneCausaleCont(String descrizioneCausaleCont) {
        _descrizioneCausaleCont = descrizioneCausaleCont;

        if (_causaliContabiliRemoteModel != null) {
            try {
                Class<?> clazz = _causaliContabiliRemoteModel.getClass();

                Method method = clazz.getMethod("setDescrizioneCausaleCont",
                        String.class);

                method.invoke(_causaliContabiliRemoteModel,
                    descrizioneCausaleCont);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getTestDareAvere() {
        return _testDareAvere;
    }

    @Override
    public void setTestDareAvere(int testDareAvere) {
        _testDareAvere = testDareAvere;

        if (_causaliContabiliRemoteModel != null) {
            try {
                Class<?> clazz = _causaliContabiliRemoteModel.getClass();

                Method method = clazz.getMethod("setTestDareAvere", int.class);

                method.invoke(_causaliContabiliRemoteModel, testDareAvere);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getTipoSoggettoMovimentato() {
        return _tipoSoggettoMovimentato;
    }

    @Override
    public void setTipoSoggettoMovimentato(int tipoSoggettoMovimentato) {
        _tipoSoggettoMovimentato = tipoSoggettoMovimentato;

        if (_causaliContabiliRemoteModel != null) {
            try {
                Class<?> clazz = _causaliContabiliRemoteModel.getClass();

                Method method = clazz.getMethod("setTipoSoggettoMovimentato",
                        int.class);

                method.invoke(_causaliContabiliRemoteModel,
                    tipoSoggettoMovimentato);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getCausaliContabiliRemoteModel() {
        return _causaliContabiliRemoteModel;
    }

    public void setCausaliContabiliRemoteModel(
        BaseModel<?> causaliContabiliRemoteModel) {
        _causaliContabiliRemoteModel = causaliContabiliRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _causaliContabiliRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_causaliContabiliRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            CausaliContabiliLocalServiceUtil.addCausaliContabili(this);
        } else {
            CausaliContabiliLocalServiceUtil.updateCausaliContabili(this);
        }
    }

    @Override
    public CausaliContabili toEscapedModel() {
        return (CausaliContabili) ProxyUtil.newProxyInstance(CausaliContabili.class.getClassLoader(),
            new Class[] { CausaliContabili.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        CausaliContabiliClp clone = new CausaliContabiliClp();

        clone.setCodiceCausaleCont(getCodiceCausaleCont());
        clone.setCodiceCausaleEC(getCodiceCausaleEC());
        clone.setDescrizioneCausaleCont(getDescrizioneCausaleCont());
        clone.setTestDareAvere(getTestDareAvere());
        clone.setTipoSoggettoMovimentato(getTipoSoggettoMovimentato());

        return clone;
    }

    @Override
    public int compareTo(CausaliContabili causaliContabili) {
        String primaryKey = causaliContabili.getPrimaryKey();

        return getPrimaryKey().compareTo(primaryKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CausaliContabiliClp)) {
            return false;
        }

        CausaliContabiliClp causaliContabili = (CausaliContabiliClp) obj;

        String primaryKey = causaliContabili.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{codiceCausaleCont=");
        sb.append(getCodiceCausaleCont());
        sb.append(", codiceCausaleEC=");
        sb.append(getCodiceCausaleEC());
        sb.append(", descrizioneCausaleCont=");
        sb.append(getDescrizioneCausaleCont());
        sb.append(", testDareAvere=");
        sb.append(getTestDareAvere());
        sb.append(", tipoSoggettoMovimentato=");
        sb.append(getTipoSoggettoMovimentato());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(19);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.CausaliContabili");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>codiceCausaleCont</column-name><column-value><![CDATA[");
        sb.append(getCodiceCausaleCont());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceCausaleEC</column-name><column-value><![CDATA[");
        sb.append(getCodiceCausaleEC());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>descrizioneCausaleCont</column-name><column-value><![CDATA[");
        sb.append(getDescrizioneCausaleCont());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>testDareAvere</column-name><column-value><![CDATA[");
        sb.append(getTestDareAvere());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tipoSoggettoMovimentato</column-name><column-value><![CDATA[");
        sb.append(getTipoSoggettoMovimentato());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
