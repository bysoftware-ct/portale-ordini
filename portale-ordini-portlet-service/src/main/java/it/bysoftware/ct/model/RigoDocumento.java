package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the RigoDocumento service. Represents a row in the &quot;_RigheDocumenti&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see RigoDocumentoModel
 * @see it.bysoftware.ct.model.impl.RigoDocumentoImpl
 * @see it.bysoftware.ct.model.impl.RigoDocumentoModelImpl
 * @generated
 */
public interface RigoDocumento extends RigoDocumentoModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.RigoDocumentoImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
