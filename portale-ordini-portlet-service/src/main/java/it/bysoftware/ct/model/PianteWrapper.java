package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Piante}.
 * </p>
 *
 * @author Mario Torrisi
 * @see Piante
 * @generated
 */
public class PianteWrapper implements Piante, ModelWrapper<Piante> {
    private Piante _piante;

    public PianteWrapper(Piante piante) {
        _piante = piante;
    }

    @Override
    public Class<?> getModelClass() {
        return Piante.class;
    }

    @Override
    public String getModelClassName() {
        return Piante.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("nome", getNome());
        attributes.put("vaso", getVaso());
        attributes.put("prezzo", getPrezzo());
        attributes.put("altezza", getAltezza());
        attributes.put("piantePianale", getPiantePianale());
        attributes.put("pianteCarrello", getPianteCarrello());
        attributes.put("attiva", getAttiva());
        attributes.put("foto1", getFoto1());
        attributes.put("foto2", getFoto2());
        attributes.put("foto3", getFoto3());
        attributes.put("altezzaPianta", getAltezzaPianta());
        attributes.put("idCategoria", getIdCategoria());
        attributes.put("idFornitore", getIdFornitore());
        attributes.put("prezzoFornitore", getPrezzoFornitore());
        attributes.put("disponibilita", getDisponibilita());
        attributes.put("codice", getCodice());
        attributes.put("forma", getForma());
        attributes.put("altezzaChioma", getAltezzaChioma());
        attributes.put("tempoConsegna", getTempoConsegna());
        attributes.put("prezzoB", getPrezzoB());
        attributes.put("percScontoMistoAziendale", getPercScontoMistoAziendale());
        attributes.put("prezzoACarrello", getPrezzoACarrello());
        attributes.put("prezzoBCarrello", getPrezzoBCarrello());
        attributes.put("idForma", getIdForma());
        attributes.put("costoRipiano", getCostoRipiano());
        attributes.put("ultimoAggiornamentoFoto", getUltimoAggiornamentoFoto());
        attributes.put("scadenzaFoto", getScadenzaFoto());
        attributes.put("giorniScadenzaFoto", getGiorniScadenzaFoto());
        attributes.put("sormonto2Fila", getSormonto2Fila());
        attributes.put("sormonto3Fila", getSormonto3Fila());
        attributes.put("sormonto4Fila", getSormonto4Fila());
        attributes.put("aggiuntaRipiano", getAggiuntaRipiano());
        attributes.put("ean", getEan());
        attributes.put("passaporto", getPassaporto());
        attributes.put("bloccoDisponibilita", getBloccoDisponibilita());
        attributes.put("pathFile", getPathFile());
        attributes.put("obsoleto", getObsoleto());
        attributes.put("prezzoEtichetta", getPrezzoEtichetta());
        attributes.put("passaportoDefault", getPassaportoDefault());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        String nome = (String) attributes.get("nome");

        if (nome != null) {
            setNome(nome);
        }

        String vaso = (String) attributes.get("vaso");

        if (vaso != null) {
            setVaso(vaso);
        }

        Double prezzo = (Double) attributes.get("prezzo");

        if (prezzo != null) {
            setPrezzo(prezzo);
        }

        Integer altezza = (Integer) attributes.get("altezza");

        if (altezza != null) {
            setAltezza(altezza);
        }

        Integer piantePianale = (Integer) attributes.get("piantePianale");

        if (piantePianale != null) {
            setPiantePianale(piantePianale);
        }

        Integer pianteCarrello = (Integer) attributes.get("pianteCarrello");

        if (pianteCarrello != null) {
            setPianteCarrello(pianteCarrello);
        }

        Boolean attiva = (Boolean) attributes.get("attiva");

        if (attiva != null) {
            setAttiva(attiva);
        }

        String foto1 = (String) attributes.get("foto1");

        if (foto1 != null) {
            setFoto1(foto1);
        }

        String foto2 = (String) attributes.get("foto2");

        if (foto2 != null) {
            setFoto2(foto2);
        }

        String foto3 = (String) attributes.get("foto3");

        if (foto3 != null) {
            setFoto3(foto3);
        }

        Integer altezzaPianta = (Integer) attributes.get("altezzaPianta");

        if (altezzaPianta != null) {
            setAltezzaPianta(altezzaPianta);
        }

        Long idCategoria = (Long) attributes.get("idCategoria");

        if (idCategoria != null) {
            setIdCategoria(idCategoria);
        }

        String idFornitore = (String) attributes.get("idFornitore");

        if (idFornitore != null) {
            setIdFornitore(idFornitore);
        }

        Double prezzoFornitore = (Double) attributes.get("prezzoFornitore");

        if (prezzoFornitore != null) {
            setPrezzoFornitore(prezzoFornitore);
        }

        Integer disponibilita = (Integer) attributes.get("disponibilita");

        if (disponibilita != null) {
            setDisponibilita(disponibilita);
        }

        String codice = (String) attributes.get("codice");

        if (codice != null) {
            setCodice(codice);
        }

        String forma = (String) attributes.get("forma");

        if (forma != null) {
            setForma(forma);
        }

        Integer altezzaChioma = (Integer) attributes.get("altezzaChioma");

        if (altezzaChioma != null) {
            setAltezzaChioma(altezzaChioma);
        }

        String tempoConsegna = (String) attributes.get("tempoConsegna");

        if (tempoConsegna != null) {
            setTempoConsegna(tempoConsegna);
        }

        Double prezzoB = (Double) attributes.get("prezzoB");

        if (prezzoB != null) {
            setPrezzoB(prezzoB);
        }

        Double percScontoMistoAziendale = (Double) attributes.get(
                "percScontoMistoAziendale");

        if (percScontoMistoAziendale != null) {
            setPercScontoMistoAziendale(percScontoMistoAziendale);
        }

        Double prezzoACarrello = (Double) attributes.get("prezzoACarrello");

        if (prezzoACarrello != null) {
            setPrezzoACarrello(prezzoACarrello);
        }

        Double prezzoBCarrello = (Double) attributes.get("prezzoBCarrello");

        if (prezzoBCarrello != null) {
            setPrezzoBCarrello(prezzoBCarrello);
        }

        Long idForma = (Long) attributes.get("idForma");

        if (idForma != null) {
            setIdForma(idForma);
        }

        Double costoRipiano = (Double) attributes.get("costoRipiano");

        if (costoRipiano != null) {
            setCostoRipiano(costoRipiano);
        }

        Date ultimoAggiornamentoFoto = (Date) attributes.get(
                "ultimoAggiornamentoFoto");

        if (ultimoAggiornamentoFoto != null) {
            setUltimoAggiornamentoFoto(ultimoAggiornamentoFoto);
        }

        Date scadenzaFoto = (Date) attributes.get("scadenzaFoto");

        if (scadenzaFoto != null) {
            setScadenzaFoto(scadenzaFoto);
        }

        Integer giorniScadenzaFoto = (Integer) attributes.get(
                "giorniScadenzaFoto");

        if (giorniScadenzaFoto != null) {
            setGiorniScadenzaFoto(giorniScadenzaFoto);
        }

        Integer sormonto2Fila = (Integer) attributes.get("sormonto2Fila");

        if (sormonto2Fila != null) {
            setSormonto2Fila(sormonto2Fila);
        }

        Integer sormonto3Fila = (Integer) attributes.get("sormonto3Fila");

        if (sormonto3Fila != null) {
            setSormonto3Fila(sormonto3Fila);
        }

        Integer sormonto4Fila = (Integer) attributes.get("sormonto4Fila");

        if (sormonto4Fila != null) {
            setSormonto4Fila(sormonto4Fila);
        }

        Integer aggiuntaRipiano = (Integer) attributes.get("aggiuntaRipiano");

        if (aggiuntaRipiano != null) {
            setAggiuntaRipiano(aggiuntaRipiano);
        }

        String ean = (String) attributes.get("ean");

        if (ean != null) {
            setEan(ean);
        }

        Boolean passaporto = (Boolean) attributes.get("passaporto");

        if (passaporto != null) {
            setPassaporto(passaporto);
        }

        Boolean bloccoDisponibilita = (Boolean) attributes.get(
                "bloccoDisponibilita");

        if (bloccoDisponibilita != null) {
            setBloccoDisponibilita(bloccoDisponibilita);
        }

        String pathFile = (String) attributes.get("pathFile");

        if (pathFile != null) {
            setPathFile(pathFile);
        }

        Boolean obsoleto = (Boolean) attributes.get("obsoleto");

        if (obsoleto != null) {
            setObsoleto(obsoleto);
        }

        Double prezzoEtichetta = (Double) attributes.get("prezzoEtichetta");

        if (prezzoEtichetta != null) {
            setPrezzoEtichetta(prezzoEtichetta);
        }

        String passaportoDefault = (String) attributes.get("passaportoDefault");

        if (passaportoDefault != null) {
            setPassaportoDefault(passaportoDefault);
        }
    }

    /**
    * Returns the primary key of this piante.
    *
    * @return the primary key of this piante
    */
    @Override
    public long getPrimaryKey() {
        return _piante.getPrimaryKey();
    }

    /**
    * Sets the primary key of this piante.
    *
    * @param primaryKey the primary key of this piante
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _piante.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this piante.
    *
    * @return the ID of this piante
    */
    @Override
    public long getId() {
        return _piante.getId();
    }

    /**
    * Sets the ID of this piante.
    *
    * @param id the ID of this piante
    */
    @Override
    public void setId(long id) {
        _piante.setId(id);
    }

    /**
    * Returns the nome of this piante.
    *
    * @return the nome of this piante
    */
    @Override
    public java.lang.String getNome() {
        return _piante.getNome();
    }

    /**
    * Sets the nome of this piante.
    *
    * @param nome the nome of this piante
    */
    @Override
    public void setNome(java.lang.String nome) {
        _piante.setNome(nome);
    }

    /**
    * Returns the vaso of this piante.
    *
    * @return the vaso of this piante
    */
    @Override
    public java.lang.String getVaso() {
        return _piante.getVaso();
    }

    /**
    * Sets the vaso of this piante.
    *
    * @param vaso the vaso of this piante
    */
    @Override
    public void setVaso(java.lang.String vaso) {
        _piante.setVaso(vaso);
    }

    /**
    * Returns the prezzo of this piante.
    *
    * @return the prezzo of this piante
    */
    @Override
    public double getPrezzo() {
        return _piante.getPrezzo();
    }

    /**
    * Sets the prezzo of this piante.
    *
    * @param prezzo the prezzo of this piante
    */
    @Override
    public void setPrezzo(double prezzo) {
        _piante.setPrezzo(prezzo);
    }

    /**
    * Returns the altezza of this piante.
    *
    * @return the altezza of this piante
    */
    @Override
    public int getAltezza() {
        return _piante.getAltezza();
    }

    /**
    * Sets the altezza of this piante.
    *
    * @param altezza the altezza of this piante
    */
    @Override
    public void setAltezza(int altezza) {
        _piante.setAltezza(altezza);
    }

    /**
    * Returns the piante pianale of this piante.
    *
    * @return the piante pianale of this piante
    */
    @Override
    public int getPiantePianale() {
        return _piante.getPiantePianale();
    }

    /**
    * Sets the piante pianale of this piante.
    *
    * @param piantePianale the piante pianale of this piante
    */
    @Override
    public void setPiantePianale(int piantePianale) {
        _piante.setPiantePianale(piantePianale);
    }

    /**
    * Returns the piante carrello of this piante.
    *
    * @return the piante carrello of this piante
    */
    @Override
    public int getPianteCarrello() {
        return _piante.getPianteCarrello();
    }

    /**
    * Sets the piante carrello of this piante.
    *
    * @param pianteCarrello the piante carrello of this piante
    */
    @Override
    public void setPianteCarrello(int pianteCarrello) {
        _piante.setPianteCarrello(pianteCarrello);
    }

    /**
    * Returns the attiva of this piante.
    *
    * @return the attiva of this piante
    */
    @Override
    public boolean getAttiva() {
        return _piante.getAttiva();
    }

    /**
    * Returns <code>true</code> if this piante is attiva.
    *
    * @return <code>true</code> if this piante is attiva; <code>false</code> otherwise
    */
    @Override
    public boolean isAttiva() {
        return _piante.isAttiva();
    }

    /**
    * Sets whether this piante is attiva.
    *
    * @param attiva the attiva of this piante
    */
    @Override
    public void setAttiva(boolean attiva) {
        _piante.setAttiva(attiva);
    }

    /**
    * Returns the foto1 of this piante.
    *
    * @return the foto1 of this piante
    */
    @Override
    public java.lang.String getFoto1() {
        return _piante.getFoto1();
    }

    /**
    * Sets the foto1 of this piante.
    *
    * @param foto1 the foto1 of this piante
    */
    @Override
    public void setFoto1(java.lang.String foto1) {
        _piante.setFoto1(foto1);
    }

    /**
    * Returns the foto2 of this piante.
    *
    * @return the foto2 of this piante
    */
    @Override
    public java.lang.String getFoto2() {
        return _piante.getFoto2();
    }

    /**
    * Sets the foto2 of this piante.
    *
    * @param foto2 the foto2 of this piante
    */
    @Override
    public void setFoto2(java.lang.String foto2) {
        _piante.setFoto2(foto2);
    }

    /**
    * Returns the foto3 of this piante.
    *
    * @return the foto3 of this piante
    */
    @Override
    public java.lang.String getFoto3() {
        return _piante.getFoto3();
    }

    /**
    * Sets the foto3 of this piante.
    *
    * @param foto3 the foto3 of this piante
    */
    @Override
    public void setFoto3(java.lang.String foto3) {
        _piante.setFoto3(foto3);
    }

    /**
    * Returns the altezza pianta of this piante.
    *
    * @return the altezza pianta of this piante
    */
    @Override
    public int getAltezzaPianta() {
        return _piante.getAltezzaPianta();
    }

    /**
    * Sets the altezza pianta of this piante.
    *
    * @param altezzaPianta the altezza pianta of this piante
    */
    @Override
    public void setAltezzaPianta(int altezzaPianta) {
        _piante.setAltezzaPianta(altezzaPianta);
    }

    /**
    * Returns the id categoria of this piante.
    *
    * @return the id categoria of this piante
    */
    @Override
    public long getIdCategoria() {
        return _piante.getIdCategoria();
    }

    /**
    * Sets the id categoria of this piante.
    *
    * @param idCategoria the id categoria of this piante
    */
    @Override
    public void setIdCategoria(long idCategoria) {
        _piante.setIdCategoria(idCategoria);
    }

    /**
    * Returns the id fornitore of this piante.
    *
    * @return the id fornitore of this piante
    */
    @Override
    public java.lang.String getIdFornitore() {
        return _piante.getIdFornitore();
    }

    /**
    * Sets the id fornitore of this piante.
    *
    * @param idFornitore the id fornitore of this piante
    */
    @Override
    public void setIdFornitore(java.lang.String idFornitore) {
        _piante.setIdFornitore(idFornitore);
    }

    /**
    * Returns the prezzo fornitore of this piante.
    *
    * @return the prezzo fornitore of this piante
    */
    @Override
    public double getPrezzoFornitore() {
        return _piante.getPrezzoFornitore();
    }

    /**
    * Sets the prezzo fornitore of this piante.
    *
    * @param prezzoFornitore the prezzo fornitore of this piante
    */
    @Override
    public void setPrezzoFornitore(double prezzoFornitore) {
        _piante.setPrezzoFornitore(prezzoFornitore);
    }

    /**
    * Returns the disponibilita of this piante.
    *
    * @return the disponibilita of this piante
    */
    @Override
    public int getDisponibilita() {
        return _piante.getDisponibilita();
    }

    /**
    * Sets the disponibilita of this piante.
    *
    * @param disponibilita the disponibilita of this piante
    */
    @Override
    public void setDisponibilita(int disponibilita) {
        _piante.setDisponibilita(disponibilita);
    }

    /**
    * Returns the codice of this piante.
    *
    * @return the codice of this piante
    */
    @Override
    public java.lang.String getCodice() {
        return _piante.getCodice();
    }

    /**
    * Sets the codice of this piante.
    *
    * @param codice the codice of this piante
    */
    @Override
    public void setCodice(java.lang.String codice) {
        _piante.setCodice(codice);
    }

    /**
    * Returns the forma of this piante.
    *
    * @return the forma of this piante
    */
    @Override
    public java.lang.String getForma() {
        return _piante.getForma();
    }

    /**
    * Sets the forma of this piante.
    *
    * @param forma the forma of this piante
    */
    @Override
    public void setForma(java.lang.String forma) {
        _piante.setForma(forma);
    }

    /**
    * Returns the altezza chioma of this piante.
    *
    * @return the altezza chioma of this piante
    */
    @Override
    public int getAltezzaChioma() {
        return _piante.getAltezzaChioma();
    }

    /**
    * Sets the altezza chioma of this piante.
    *
    * @param altezzaChioma the altezza chioma of this piante
    */
    @Override
    public void setAltezzaChioma(int altezzaChioma) {
        _piante.setAltezzaChioma(altezzaChioma);
    }

    /**
    * Returns the tempo consegna of this piante.
    *
    * @return the tempo consegna of this piante
    */
    @Override
    public java.lang.String getTempoConsegna() {
        return _piante.getTempoConsegna();
    }

    /**
    * Sets the tempo consegna of this piante.
    *
    * @param tempoConsegna the tempo consegna of this piante
    */
    @Override
    public void setTempoConsegna(java.lang.String tempoConsegna) {
        _piante.setTempoConsegna(tempoConsegna);
    }

    /**
    * Returns the prezzo b of this piante.
    *
    * @return the prezzo b of this piante
    */
    @Override
    public double getPrezzoB() {
        return _piante.getPrezzoB();
    }

    /**
    * Sets the prezzo b of this piante.
    *
    * @param prezzoB the prezzo b of this piante
    */
    @Override
    public void setPrezzoB(double prezzoB) {
        _piante.setPrezzoB(prezzoB);
    }

    /**
    * Returns the perc sconto misto aziendale of this piante.
    *
    * @return the perc sconto misto aziendale of this piante
    */
    @Override
    public double getPercScontoMistoAziendale() {
        return _piante.getPercScontoMistoAziendale();
    }

    /**
    * Sets the perc sconto misto aziendale of this piante.
    *
    * @param percScontoMistoAziendale the perc sconto misto aziendale of this piante
    */
    @Override
    public void setPercScontoMistoAziendale(double percScontoMistoAziendale) {
        _piante.setPercScontoMistoAziendale(percScontoMistoAziendale);
    }

    /**
    * Returns the prezzo a carrello of this piante.
    *
    * @return the prezzo a carrello of this piante
    */
    @Override
    public double getPrezzoACarrello() {
        return _piante.getPrezzoACarrello();
    }

    /**
    * Sets the prezzo a carrello of this piante.
    *
    * @param prezzoACarrello the prezzo a carrello of this piante
    */
    @Override
    public void setPrezzoACarrello(double prezzoACarrello) {
        _piante.setPrezzoACarrello(prezzoACarrello);
    }

    /**
    * Returns the prezzo b carrello of this piante.
    *
    * @return the prezzo b carrello of this piante
    */
    @Override
    public double getPrezzoBCarrello() {
        return _piante.getPrezzoBCarrello();
    }

    /**
    * Sets the prezzo b carrello of this piante.
    *
    * @param prezzoBCarrello the prezzo b carrello of this piante
    */
    @Override
    public void setPrezzoBCarrello(double prezzoBCarrello) {
        _piante.setPrezzoBCarrello(prezzoBCarrello);
    }

    /**
    * Returns the id forma of this piante.
    *
    * @return the id forma of this piante
    */
    @Override
    public long getIdForma() {
        return _piante.getIdForma();
    }

    /**
    * Sets the id forma of this piante.
    *
    * @param idForma the id forma of this piante
    */
    @Override
    public void setIdForma(long idForma) {
        _piante.setIdForma(idForma);
    }

    /**
    * Returns the costo ripiano of this piante.
    *
    * @return the costo ripiano of this piante
    */
    @Override
    public double getCostoRipiano() {
        return _piante.getCostoRipiano();
    }

    /**
    * Sets the costo ripiano of this piante.
    *
    * @param costoRipiano the costo ripiano of this piante
    */
    @Override
    public void setCostoRipiano(double costoRipiano) {
        _piante.setCostoRipiano(costoRipiano);
    }

    /**
    * Returns the ultimo aggiornamento foto of this piante.
    *
    * @return the ultimo aggiornamento foto of this piante
    */
    @Override
    public java.util.Date getUltimoAggiornamentoFoto() {
        return _piante.getUltimoAggiornamentoFoto();
    }

    /**
    * Sets the ultimo aggiornamento foto of this piante.
    *
    * @param ultimoAggiornamentoFoto the ultimo aggiornamento foto of this piante
    */
    @Override
    public void setUltimoAggiornamentoFoto(
        java.util.Date ultimoAggiornamentoFoto) {
        _piante.setUltimoAggiornamentoFoto(ultimoAggiornamentoFoto);
    }

    /**
    * Returns the scadenza foto of this piante.
    *
    * @return the scadenza foto of this piante
    */
    @Override
    public java.util.Date getScadenzaFoto() {
        return _piante.getScadenzaFoto();
    }

    /**
    * Sets the scadenza foto of this piante.
    *
    * @param scadenzaFoto the scadenza foto of this piante
    */
    @Override
    public void setScadenzaFoto(java.util.Date scadenzaFoto) {
        _piante.setScadenzaFoto(scadenzaFoto);
    }

    /**
    * Returns the giorni scadenza foto of this piante.
    *
    * @return the giorni scadenza foto of this piante
    */
    @Override
    public int getGiorniScadenzaFoto() {
        return _piante.getGiorniScadenzaFoto();
    }

    /**
    * Sets the giorni scadenza foto of this piante.
    *
    * @param giorniScadenzaFoto the giorni scadenza foto of this piante
    */
    @Override
    public void setGiorniScadenzaFoto(int giorniScadenzaFoto) {
        _piante.setGiorniScadenzaFoto(giorniScadenzaFoto);
    }

    /**
    * Returns the sormonto2 fila of this piante.
    *
    * @return the sormonto2 fila of this piante
    */
    @Override
    public int getSormonto2Fila() {
        return _piante.getSormonto2Fila();
    }

    /**
    * Sets the sormonto2 fila of this piante.
    *
    * @param sormonto2Fila the sormonto2 fila of this piante
    */
    @Override
    public void setSormonto2Fila(int sormonto2Fila) {
        _piante.setSormonto2Fila(sormonto2Fila);
    }

    /**
    * Returns the sormonto3 fila of this piante.
    *
    * @return the sormonto3 fila of this piante
    */
    @Override
    public int getSormonto3Fila() {
        return _piante.getSormonto3Fila();
    }

    /**
    * Sets the sormonto3 fila of this piante.
    *
    * @param sormonto3Fila the sormonto3 fila of this piante
    */
    @Override
    public void setSormonto3Fila(int sormonto3Fila) {
        _piante.setSormonto3Fila(sormonto3Fila);
    }

    /**
    * Returns the sormonto4 fila of this piante.
    *
    * @return the sormonto4 fila of this piante
    */
    @Override
    public int getSormonto4Fila() {
        return _piante.getSormonto4Fila();
    }

    /**
    * Sets the sormonto4 fila of this piante.
    *
    * @param sormonto4Fila the sormonto4 fila of this piante
    */
    @Override
    public void setSormonto4Fila(int sormonto4Fila) {
        _piante.setSormonto4Fila(sormonto4Fila);
    }

    /**
    * Returns the aggiunta ripiano of this piante.
    *
    * @return the aggiunta ripiano of this piante
    */
    @Override
    public int getAggiuntaRipiano() {
        return _piante.getAggiuntaRipiano();
    }

    /**
    * Sets the aggiunta ripiano of this piante.
    *
    * @param aggiuntaRipiano the aggiunta ripiano of this piante
    */
    @Override
    public void setAggiuntaRipiano(int aggiuntaRipiano) {
        _piante.setAggiuntaRipiano(aggiuntaRipiano);
    }

    /**
    * Returns the ean of this piante.
    *
    * @return the ean of this piante
    */
    @Override
    public java.lang.String getEan() {
        return _piante.getEan();
    }

    /**
    * Sets the ean of this piante.
    *
    * @param ean the ean of this piante
    */
    @Override
    public void setEan(java.lang.String ean) {
        _piante.setEan(ean);
    }

    /**
    * Returns the passaporto of this piante.
    *
    * @return the passaporto of this piante
    */
    @Override
    public boolean getPassaporto() {
        return _piante.getPassaporto();
    }

    /**
    * Returns <code>true</code> if this piante is passaporto.
    *
    * @return <code>true</code> if this piante is passaporto; <code>false</code> otherwise
    */
    @Override
    public boolean isPassaporto() {
        return _piante.isPassaporto();
    }

    /**
    * Sets whether this piante is passaporto.
    *
    * @param passaporto the passaporto of this piante
    */
    @Override
    public void setPassaporto(boolean passaporto) {
        _piante.setPassaporto(passaporto);
    }

    /**
    * Returns the blocco disponibilita of this piante.
    *
    * @return the blocco disponibilita of this piante
    */
    @Override
    public boolean getBloccoDisponibilita() {
        return _piante.getBloccoDisponibilita();
    }

    /**
    * Returns <code>true</code> if this piante is blocco disponibilita.
    *
    * @return <code>true</code> if this piante is blocco disponibilita; <code>false</code> otherwise
    */
    @Override
    public boolean isBloccoDisponibilita() {
        return _piante.isBloccoDisponibilita();
    }

    /**
    * Sets whether this piante is blocco disponibilita.
    *
    * @param bloccoDisponibilita the blocco disponibilita of this piante
    */
    @Override
    public void setBloccoDisponibilita(boolean bloccoDisponibilita) {
        _piante.setBloccoDisponibilita(bloccoDisponibilita);
    }

    /**
    * Returns the path file of this piante.
    *
    * @return the path file of this piante
    */
    @Override
    public java.lang.String getPathFile() {
        return _piante.getPathFile();
    }

    /**
    * Sets the path file of this piante.
    *
    * @param pathFile the path file of this piante
    */
    @Override
    public void setPathFile(java.lang.String pathFile) {
        _piante.setPathFile(pathFile);
    }

    /**
    * Returns the obsoleto of this piante.
    *
    * @return the obsoleto of this piante
    */
    @Override
    public boolean getObsoleto() {
        return _piante.getObsoleto();
    }

    /**
    * Returns <code>true</code> if this piante is obsoleto.
    *
    * @return <code>true</code> if this piante is obsoleto; <code>false</code> otherwise
    */
    @Override
    public boolean isObsoleto() {
        return _piante.isObsoleto();
    }

    /**
    * Sets whether this piante is obsoleto.
    *
    * @param obsoleto the obsoleto of this piante
    */
    @Override
    public void setObsoleto(boolean obsoleto) {
        _piante.setObsoleto(obsoleto);
    }

    /**
    * Returns the prezzo etichetta of this piante.
    *
    * @return the prezzo etichetta of this piante
    */
    @Override
    public double getPrezzoEtichetta() {
        return _piante.getPrezzoEtichetta();
    }

    /**
    * Sets the prezzo etichetta of this piante.
    *
    * @param prezzoEtichetta the prezzo etichetta of this piante
    */
    @Override
    public void setPrezzoEtichetta(double prezzoEtichetta) {
        _piante.setPrezzoEtichetta(prezzoEtichetta);
    }

    /**
    * Returns the passaporto default of this piante.
    *
    * @return the passaporto default of this piante
    */
    @Override
    public java.lang.String getPassaportoDefault() {
        return _piante.getPassaportoDefault();
    }

    /**
    * Sets the passaporto default of this piante.
    *
    * @param passaportoDefault the passaporto default of this piante
    */
    @Override
    public void setPassaportoDefault(java.lang.String passaportoDefault) {
        _piante.setPassaportoDefault(passaportoDefault);
    }

    @Override
    public boolean isNew() {
        return _piante.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _piante.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _piante.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _piante.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _piante.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _piante.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _piante.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _piante.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _piante.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _piante.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _piante.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new PianteWrapper((Piante) _piante.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.Piante piante) {
        return _piante.compareTo(piante);
    }

    @Override
    public int hashCode() {
        return _piante.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.Piante> toCacheModel() {
        return _piante.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.Piante toEscapedModel() {
        return new PianteWrapper(_piante.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.Piante toUnescapedModel() {
        return new PianteWrapper(_piante.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _piante.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _piante.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _piante.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PianteWrapper)) {
            return false;
        }

        PianteWrapper pianteWrapper = (PianteWrapper) obj;

        if (Validator.equals(_piante, pianteWrapper._piante)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Piante getWrappedPiante() {
        return _piante;
    }

    @Override
    public Piante getWrappedModel() {
        return _piante;
    }

    @Override
    public void resetOriginalValues() {
        _piante.resetOriginalValues();
    }
}
