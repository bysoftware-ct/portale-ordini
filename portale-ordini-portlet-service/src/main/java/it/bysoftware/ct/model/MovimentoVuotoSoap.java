package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.MovimentoVuotoServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.MovimentoVuotoServiceSoap
 * @generated
 */
public class MovimentoVuotoSoap implements Serializable {
    private long _idMovimento;
    private String _codiceVuoto;
    private Date _dataMovimento;
    private String _codiceSoggetto;
    private double _quantita;
    private int _tipoMovimento;
    private String _note;
    private int _uuid;

    public MovimentoVuotoSoap() {
    }

    public static MovimentoVuotoSoap toSoapModel(MovimentoVuoto model) {
        MovimentoVuotoSoap soapModel = new MovimentoVuotoSoap();

        soapModel.setIdMovimento(model.getIdMovimento());
        soapModel.setCodiceVuoto(model.getCodiceVuoto());
        soapModel.setDataMovimento(model.getDataMovimento());
        soapModel.setCodiceSoggetto(model.getCodiceSoggetto());
        soapModel.setQuantita(model.getQuantita());
        soapModel.setTipoMovimento(model.getTipoMovimento());
        soapModel.setNote(model.getNote());
        soapModel.setUuid(model.getUuid());

        return soapModel;
    }

    public static MovimentoVuotoSoap[] toSoapModels(MovimentoVuoto[] models) {
        MovimentoVuotoSoap[] soapModels = new MovimentoVuotoSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static MovimentoVuotoSoap[][] toSoapModels(MovimentoVuoto[][] models) {
        MovimentoVuotoSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new MovimentoVuotoSoap[models.length][models[0].length];
        } else {
            soapModels = new MovimentoVuotoSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static MovimentoVuotoSoap[] toSoapModels(List<MovimentoVuoto> models) {
        List<MovimentoVuotoSoap> soapModels = new ArrayList<MovimentoVuotoSoap>(models.size());

        for (MovimentoVuoto model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new MovimentoVuotoSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _idMovimento;
    }

    public void setPrimaryKey(long pk) {
        setIdMovimento(pk);
    }

    public long getIdMovimento() {
        return _idMovimento;
    }

    public void setIdMovimento(long idMovimento) {
        _idMovimento = idMovimento;
    }

    public String getCodiceVuoto() {
        return _codiceVuoto;
    }

    public void setCodiceVuoto(String codiceVuoto) {
        _codiceVuoto = codiceVuoto;
    }

    public Date getDataMovimento() {
        return _dataMovimento;
    }

    public void setDataMovimento(Date dataMovimento) {
        _dataMovimento = dataMovimento;
    }

    public String getCodiceSoggetto() {
        return _codiceSoggetto;
    }

    public void setCodiceSoggetto(String codiceSoggetto) {
        _codiceSoggetto = codiceSoggetto;
    }

    public double getQuantita() {
        return _quantita;
    }

    public void setQuantita(double quantita) {
        _quantita = quantita;
    }

    public int getTipoMovimento() {
        return _tipoMovimento;
    }

    public void setTipoMovimento(int tipoMovimento) {
        _tipoMovimento = tipoMovimento;
    }

    public String getNote() {
        return _note;
    }

    public void setNote(String note) {
        _note = note;
    }

    public int getUuid() {
        return _uuid;
    }

    public void setUuid(int uuid) {
        _uuid = uuid;
    }
}
