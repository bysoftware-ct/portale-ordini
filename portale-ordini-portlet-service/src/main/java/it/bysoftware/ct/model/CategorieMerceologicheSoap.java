package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.CategorieMerceologicheServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.CategorieMerceologicheServiceSoap
 * @generated
 */
public class CategorieMerceologicheSoap implements Serializable {
    private String _codiceCategoria;
    private String _descrizione;

    public CategorieMerceologicheSoap() {
    }

    public static CategorieMerceologicheSoap toSoapModel(
        CategorieMerceologiche model) {
        CategorieMerceologicheSoap soapModel = new CategorieMerceologicheSoap();

        soapModel.setCodiceCategoria(model.getCodiceCategoria());
        soapModel.setDescrizione(model.getDescrizione());

        return soapModel;
    }

    public static CategorieMerceologicheSoap[] toSoapModels(
        CategorieMerceologiche[] models) {
        CategorieMerceologicheSoap[] soapModels = new CategorieMerceologicheSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static CategorieMerceologicheSoap[][] toSoapModels(
        CategorieMerceologiche[][] models) {
        CategorieMerceologicheSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new CategorieMerceologicheSoap[models.length][models[0].length];
        } else {
            soapModels = new CategorieMerceologicheSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static CategorieMerceologicheSoap[] toSoapModels(
        List<CategorieMerceologiche> models) {
        List<CategorieMerceologicheSoap> soapModels = new ArrayList<CategorieMerceologicheSoap>(models.size());

        for (CategorieMerceologiche model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new CategorieMerceologicheSoap[soapModels.size()]);
    }

    public String getPrimaryKey() {
        return _codiceCategoria;
    }

    public void setPrimaryKey(String pk) {
        setCodiceCategoria(pk);
    }

    public String getCodiceCategoria() {
        return _codiceCategoria;
    }

    public void setCodiceCategoria(String codiceCategoria) {
        _codiceCategoria = codiceCategoria;
    }

    public String getDescrizione() {
        return _descrizione;
    }

    public void setDescrizione(String descrizione) {
        _descrizione = descrizione;
    }
}
