package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.VarianteLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class VarianteClp extends BaseModelImpl<Variante> implements Variante {
    private long _id;
    private long _idOpzione;
    private String _variante;
    private BaseModel<?> _varianteRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public VarianteClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Variante.class;
    }

    @Override
    public String getModelClassName() {
        return Variante.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("idOpzione", getIdOpzione());
        attributes.put("variante", getVariante());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long idOpzione = (Long) attributes.get("idOpzione");

        if (idOpzione != null) {
            setIdOpzione(idOpzione);
        }

        String variante = (String) attributes.get("variante");

        if (variante != null) {
            setVariante(variante);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_varianteRemoteModel != null) {
            try {
                Class<?> clazz = _varianteRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_varianteRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getIdOpzione() {
        return _idOpzione;
    }

    @Override
    public void setIdOpzione(long idOpzione) {
        _idOpzione = idOpzione;

        if (_varianteRemoteModel != null) {
            try {
                Class<?> clazz = _varianteRemoteModel.getClass();

                Method method = clazz.getMethod("setIdOpzione", long.class);

                method.invoke(_varianteRemoteModel, idOpzione);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getVariante() {
        return _variante;
    }

    @Override
    public void setVariante(String variante) {
        _variante = variante;

        if (_varianteRemoteModel != null) {
            try {
                Class<?> clazz = _varianteRemoteModel.getClass();

                Method method = clazz.getMethod("setVariante", String.class);

                method.invoke(_varianteRemoteModel, variante);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getVarianteRemoteModel() {
        return _varianteRemoteModel;
    }

    public void setVarianteRemoteModel(BaseModel<?> varianteRemoteModel) {
        _varianteRemoteModel = varianteRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _varianteRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_varianteRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            VarianteLocalServiceUtil.addVariante(this);
        } else {
            VarianteLocalServiceUtil.updateVariante(this);
        }
    }

    @Override
    public Variante toEscapedModel() {
        return (Variante) ProxyUtil.newProxyInstance(Variante.class.getClassLoader(),
            new Class[] { Variante.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        VarianteClp clone = new VarianteClp();

        clone.setId(getId());
        clone.setIdOpzione(getIdOpzione());
        clone.setVariante(getVariante());

        return clone;
    }

    @Override
    public int compareTo(Variante variante) {
        long primaryKey = variante.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VarianteClp)) {
            return false;
        }

        VarianteClp variante = (VarianteClp) obj;

        long primaryKey = variante.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", idOpzione=");
        sb.append(getIdOpzione());
        sb.append(", variante=");
        sb.append(getVariante());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(13);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.Variante");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>idOpzione</column-name><column-value><![CDATA[");
        sb.append(getIdOpzione());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>variante</column-name><column-value><![CDATA[");
        sb.append(getVariante());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
