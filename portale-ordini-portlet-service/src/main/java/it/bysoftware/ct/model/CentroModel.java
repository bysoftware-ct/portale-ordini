package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import it.bysoftware.ct.service.persistence.CentroPK;

import java.io.Serializable;

/**
 * The base model interface for the Centro service. Represents a row in the &quot;AttivitaCentri&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link it.bysoftware.ct.model.impl.CentroModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link it.bysoftware.ct.model.impl.CentroImpl}.
 * </p>
 *
 * @author Mario Torrisi
 * @see Centro
 * @see it.bysoftware.ct.model.impl.CentroImpl
 * @see it.bysoftware.ct.model.impl.CentroModelImpl
 * @generated
 */
public interface CentroModel extends BaseModel<Centro> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. All methods that expect a centro model instance should use the {@link Centro} interface instead.
     */

    /**
     * Returns the primary key of this centro.
     *
     * @return the primary key of this centro
     */
    public CentroPK getPrimaryKey();

    /**
     * Sets the primary key of this centro.
     *
     * @param primaryKey the primary key of this centro
     */
    public void setPrimaryKey(CentroPK primaryKey);

    /**
     * Returns the attivita of this centro.
     *
     * @return the attivita of this centro
     */
    @AutoEscape
    public String getAttivita();

    /**
     * Sets the attivita of this centro.
     *
     * @param attivita the attivita of this centro
     */
    public void setAttivita(String attivita);

    /**
     * Returns the centro of this centro.
     *
     * @return the centro of this centro
     */
    @AutoEscape
    public String getCentro();

    /**
     * Sets the centro of this centro.
     *
     * @param centro the centro of this centro
     */
    public void setCentro(String centro);

    @Override
    public boolean isNew();

    @Override
    public void setNew(boolean n);

    @Override
    public boolean isCachedModel();

    @Override
    public void setCachedModel(boolean cachedModel);

    @Override
    public boolean isEscapedModel();

    @Override
    public Serializable getPrimaryKeyObj();

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj);

    @Override
    public ExpandoBridge getExpandoBridge();

    @Override
    public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

    @Override
    public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

    @Override
    public void setExpandoBridgeAttributes(ServiceContext serviceContext);

    @Override
    public Object clone();

    @Override
    public int compareTo(it.bysoftware.ct.model.Centro centro);

    @Override
    public int hashCode();

    @Override
    public CacheModel<it.bysoftware.ct.model.Centro> toCacheModel();

    @Override
    public it.bysoftware.ct.model.Centro toEscapedModel();

    @Override
    public it.bysoftware.ct.model.Centro toUnescapedModel();

    @Override
    public String toString();

    @Override
    public String toXmlString();
}
