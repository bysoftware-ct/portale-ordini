package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.OrdineServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.OrdineServiceSoap
 * @generated
 */
public class OrdineSoap implements Serializable {
    private long _id;
    private Date _dataInserimento;
    private String _idCliente;
    private int _stato;
    private double _importo;
    private String _note;
    private Date _dataConsegna;
    private String _idTrasportatore;
    private String _idTrasportatore2;
    private long _idAgente;
    private long _idTipoCarrello;
    private String _luogoCarico;
    private String _noteCliente;
    private int _anno;
    private int _numero;
    private String _centro;
    private boolean _modificato;
    private String _codiceIva;
    private String _cliente;
    private String _vettore;
    private String _pagamento;
    private boolean _ddtGenerati;
    private boolean _stampaEtichette;
    private String _numOrdineGT;
    private Date _dataOrdineGT;

    public OrdineSoap() {
    }

    public static OrdineSoap toSoapModel(Ordine model) {
        OrdineSoap soapModel = new OrdineSoap();

        soapModel.setId(model.getId());
        soapModel.setDataInserimento(model.getDataInserimento());
        soapModel.setIdCliente(model.getIdCliente());
        soapModel.setStato(model.getStato());
        soapModel.setImporto(model.getImporto());
        soapModel.setNote(model.getNote());
        soapModel.setDataConsegna(model.getDataConsegna());
        soapModel.setIdTrasportatore(model.getIdTrasportatore());
        soapModel.setIdTrasportatore2(model.getIdTrasportatore2());
        soapModel.setIdAgente(model.getIdAgente());
        soapModel.setIdTipoCarrello(model.getIdTipoCarrello());
        soapModel.setLuogoCarico(model.getLuogoCarico());
        soapModel.setNoteCliente(model.getNoteCliente());
        soapModel.setAnno(model.getAnno());
        soapModel.setNumero(model.getNumero());
        soapModel.setCentro(model.getCentro());
        soapModel.setModificato(model.getModificato());
        soapModel.setCodiceIva(model.getCodiceIva());
        soapModel.setCliente(model.getCliente());
        soapModel.setVettore(model.getVettore());
        soapModel.setPagamento(model.getPagamento());
        soapModel.setDdtGenerati(model.getDdtGenerati());
        soapModel.setStampaEtichette(model.getStampaEtichette());
        soapModel.setNumOrdineGT(model.getNumOrdineGT());
        soapModel.setDataOrdineGT(model.getDataOrdineGT());

        return soapModel;
    }

    public static OrdineSoap[] toSoapModels(Ordine[] models) {
        OrdineSoap[] soapModels = new OrdineSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static OrdineSoap[][] toSoapModels(Ordine[][] models) {
        OrdineSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new OrdineSoap[models.length][models[0].length];
        } else {
            soapModels = new OrdineSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static OrdineSoap[] toSoapModels(List<Ordine> models) {
        List<OrdineSoap> soapModels = new ArrayList<OrdineSoap>(models.size());

        for (Ordine model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new OrdineSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public Date getDataInserimento() {
        return _dataInserimento;
    }

    public void setDataInserimento(Date dataInserimento) {
        _dataInserimento = dataInserimento;
    }

    public String getIdCliente() {
        return _idCliente;
    }

    public void setIdCliente(String idCliente) {
        _idCliente = idCliente;
    }

    public int getStato() {
        return _stato;
    }

    public void setStato(int stato) {
        _stato = stato;
    }

    public double getImporto() {
        return _importo;
    }

    public void setImporto(double importo) {
        _importo = importo;
    }

    public String getNote() {
        return _note;
    }

    public void setNote(String note) {
        _note = note;
    }

    public Date getDataConsegna() {
        return _dataConsegna;
    }

    public void setDataConsegna(Date dataConsegna) {
        _dataConsegna = dataConsegna;
    }

    public String getIdTrasportatore() {
        return _idTrasportatore;
    }

    public void setIdTrasportatore(String idTrasportatore) {
        _idTrasportatore = idTrasportatore;
    }

    public String getIdTrasportatore2() {
        return _idTrasportatore2;
    }

    public void setIdTrasportatore2(String idTrasportatore2) {
        _idTrasportatore2 = idTrasportatore2;
    }

    public long getIdAgente() {
        return _idAgente;
    }

    public void setIdAgente(long idAgente) {
        _idAgente = idAgente;
    }

    public long getIdTipoCarrello() {
        return _idTipoCarrello;
    }

    public void setIdTipoCarrello(long idTipoCarrello) {
        _idTipoCarrello = idTipoCarrello;
    }

    public String getLuogoCarico() {
        return _luogoCarico;
    }

    public void setLuogoCarico(String luogoCarico) {
        _luogoCarico = luogoCarico;
    }

    public String getNoteCliente() {
        return _noteCliente;
    }

    public void setNoteCliente(String noteCliente) {
        _noteCliente = noteCliente;
    }

    public int getAnno() {
        return _anno;
    }

    public void setAnno(int anno) {
        _anno = anno;
    }

    public int getNumero() {
        return _numero;
    }

    public void setNumero(int numero) {
        _numero = numero;
    }

    public String getCentro() {
        return _centro;
    }

    public void setCentro(String centro) {
        _centro = centro;
    }

    public boolean getModificato() {
        return _modificato;
    }

    public boolean isModificato() {
        return _modificato;
    }

    public void setModificato(boolean modificato) {
        _modificato = modificato;
    }

    public String getCodiceIva() {
        return _codiceIva;
    }

    public void setCodiceIva(String codiceIva) {
        _codiceIva = codiceIva;
    }

    public String getCliente() {
        return _cliente;
    }

    public void setCliente(String cliente) {
        _cliente = cliente;
    }

    public String getVettore() {
        return _vettore;
    }

    public void setVettore(String vettore) {
        _vettore = vettore;
    }

    public String getPagamento() {
        return _pagamento;
    }

    public void setPagamento(String pagamento) {
        _pagamento = pagamento;
    }

    public boolean getDdtGenerati() {
        return _ddtGenerati;
    }

    public boolean isDdtGenerati() {
        return _ddtGenerati;
    }

    public void setDdtGenerati(boolean ddtGenerati) {
        _ddtGenerati = ddtGenerati;
    }

    public boolean getStampaEtichette() {
        return _stampaEtichette;
    }

    public boolean isStampaEtichette() {
        return _stampaEtichette;
    }

    public void setStampaEtichette(boolean stampaEtichette) {
        _stampaEtichette = stampaEtichette;
    }

    public String getNumOrdineGT() {
        return _numOrdineGT;
    }

    public void setNumOrdineGT(String numOrdineGT) {
        _numOrdineGT = numOrdineGT;
    }

    public Date getDataOrdineGT() {
        return _dataOrdineGT;
    }

    public void setDataOrdineGT(Date dataOrdineGT) {
        _dataOrdineGT = dataOrdineGT;
    }
}
