package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.ClientiFornitoriDatiAggServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.ClientiFornitoriDatiAggServiceSoap
 * @generated
 */
public class ClientiFornitoriDatiAggSoap implements Serializable {
    private String _codiceAnagrafica;
    private boolean _fornitore;
    private String _codiceAgente;
    private String _codiceEsenzione;
    private String _categoriaEconomica;
    private String _codiceRegionale;
    private String _codicePuntoVenditaGT;
    private String _tipoPagamento;
    private boolean _associato;
    private double _sconto;
    private String _codiceIBAN;

    public ClientiFornitoriDatiAggSoap() {
    }

    public static ClientiFornitoriDatiAggSoap toSoapModel(
        ClientiFornitoriDatiAgg model) {
        ClientiFornitoriDatiAggSoap soapModel = new ClientiFornitoriDatiAggSoap();

        soapModel.setCodiceAnagrafica(model.getCodiceAnagrafica());
        soapModel.setFornitore(model.getFornitore());
        soapModel.setCodiceAgente(model.getCodiceAgente());
        soapModel.setCodiceEsenzione(model.getCodiceEsenzione());
        soapModel.setCategoriaEconomica(model.getCategoriaEconomica());
        soapModel.setCodiceRegionale(model.getCodiceRegionale());
        soapModel.setCodicePuntoVenditaGT(model.getCodicePuntoVenditaGT());
        soapModel.setTipoPagamento(model.getTipoPagamento());
        soapModel.setAssociato(model.getAssociato());
        soapModel.setSconto(model.getSconto());
        soapModel.setCodiceIBAN(model.getCodiceIBAN());

        return soapModel;
    }

    public static ClientiFornitoriDatiAggSoap[] toSoapModels(
        ClientiFornitoriDatiAgg[] models) {
        ClientiFornitoriDatiAggSoap[] soapModels = new ClientiFornitoriDatiAggSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static ClientiFornitoriDatiAggSoap[][] toSoapModels(
        ClientiFornitoriDatiAgg[][] models) {
        ClientiFornitoriDatiAggSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new ClientiFornitoriDatiAggSoap[models.length][models[0].length];
        } else {
            soapModels = new ClientiFornitoriDatiAggSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static ClientiFornitoriDatiAggSoap[] toSoapModels(
        List<ClientiFornitoriDatiAgg> models) {
        List<ClientiFornitoriDatiAggSoap> soapModels = new ArrayList<ClientiFornitoriDatiAggSoap>(models.size());

        for (ClientiFornitoriDatiAgg model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new ClientiFornitoriDatiAggSoap[soapModels.size()]);
    }

    public ClientiFornitoriDatiAggPK getPrimaryKey() {
        return new ClientiFornitoriDatiAggPK(_codiceAnagrafica, _fornitore);
    }

    public void setPrimaryKey(ClientiFornitoriDatiAggPK pk) {
        setCodiceAnagrafica(pk.codiceAnagrafica);
        setFornitore(pk.fornitore);
    }

    public String getCodiceAnagrafica() {
        return _codiceAnagrafica;
    }

    public void setCodiceAnagrafica(String codiceAnagrafica) {
        _codiceAnagrafica = codiceAnagrafica;
    }

    public boolean getFornitore() {
        return _fornitore;
    }

    public boolean isFornitore() {
        return _fornitore;
    }

    public void setFornitore(boolean fornitore) {
        _fornitore = fornitore;
    }

    public String getCodiceAgente() {
        return _codiceAgente;
    }

    public void setCodiceAgente(String codiceAgente) {
        _codiceAgente = codiceAgente;
    }

    public String getCodiceEsenzione() {
        return _codiceEsenzione;
    }

    public void setCodiceEsenzione(String codiceEsenzione) {
        _codiceEsenzione = codiceEsenzione;
    }

    public String getCategoriaEconomica() {
        return _categoriaEconomica;
    }

    public void setCategoriaEconomica(String categoriaEconomica) {
        _categoriaEconomica = categoriaEconomica;
    }

    public String getCodiceRegionale() {
        return _codiceRegionale;
    }

    public void setCodiceRegionale(String codiceRegionale) {
        _codiceRegionale = codiceRegionale;
    }

    public String getCodicePuntoVenditaGT() {
        return _codicePuntoVenditaGT;
    }

    public void setCodicePuntoVenditaGT(String codicePuntoVenditaGT) {
        _codicePuntoVenditaGT = codicePuntoVenditaGT;
    }

    public String getTipoPagamento() {
        return _tipoPagamento;
    }

    public void setTipoPagamento(String tipoPagamento) {
        _tipoPagamento = tipoPagamento;
    }

    public boolean getAssociato() {
        return _associato;
    }

    public boolean isAssociato() {
        return _associato;
    }

    public void setAssociato(boolean associato) {
        _associato = associato;
    }

    public double getSconto() {
        return _sconto;
    }

    public void setSconto(double sconto) {
        _sconto = sconto;
    }

    public String getCodiceIBAN() {
        return _codiceIBAN;
    }

    public void setCodiceIBAN(String codiceIBAN) {
        _codiceIBAN = codiceIBAN;
    }
}
