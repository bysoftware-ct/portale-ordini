package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the DescrizioniVarianti service. Represents a row in the &quot;DescrizioniVariantiArt&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see DescrizioniVariantiModel
 * @see it.bysoftware.ct.model.impl.DescrizioniVariantiImpl
 * @see it.bysoftware.ct.model.impl.DescrizioniVariantiModelImpl
 * @generated
 */
public interface DescrizioniVarianti extends DescrizioniVariantiModel,
    PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.DescrizioniVariantiImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
