package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link VariantiPianta}.
 * </p>
 *
 * @author Mario Torrisi
 * @see VariantiPianta
 * @generated
 */
public class VariantiPiantaWrapper implements VariantiPianta,
    ModelWrapper<VariantiPianta> {
    private VariantiPianta _variantiPianta;

    public VariantiPiantaWrapper(VariantiPianta variantiPianta) {
        _variantiPianta = variantiPianta;
    }

    @Override
    public Class<?> getModelClass() {
        return VariantiPianta.class;
    }

    @Override
    public String getModelClassName() {
        return VariantiPianta.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("idVariante", getIdVariante());
        attributes.put("idPianta", getIdPianta());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long idVariante = (Long) attributes.get("idVariante");

        if (idVariante != null) {
            setIdVariante(idVariante);
        }

        Long idPianta = (Long) attributes.get("idPianta");

        if (idPianta != null) {
            setIdPianta(idPianta);
        }
    }

    /**
    * Returns the primary key of this varianti pianta.
    *
    * @return the primary key of this varianti pianta
    */
    @Override
    public long getPrimaryKey() {
        return _variantiPianta.getPrimaryKey();
    }

    /**
    * Sets the primary key of this varianti pianta.
    *
    * @param primaryKey the primary key of this varianti pianta
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _variantiPianta.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this varianti pianta.
    *
    * @return the ID of this varianti pianta
    */
    @Override
    public long getId() {
        return _variantiPianta.getId();
    }

    /**
    * Sets the ID of this varianti pianta.
    *
    * @param id the ID of this varianti pianta
    */
    @Override
    public void setId(long id) {
        _variantiPianta.setId(id);
    }

    /**
    * Returns the id variante of this varianti pianta.
    *
    * @return the id variante of this varianti pianta
    */
    @Override
    public long getIdVariante() {
        return _variantiPianta.getIdVariante();
    }

    /**
    * Sets the id variante of this varianti pianta.
    *
    * @param idVariante the id variante of this varianti pianta
    */
    @Override
    public void setIdVariante(long idVariante) {
        _variantiPianta.setIdVariante(idVariante);
    }

    /**
    * Returns the id pianta of this varianti pianta.
    *
    * @return the id pianta of this varianti pianta
    */
    @Override
    public long getIdPianta() {
        return _variantiPianta.getIdPianta();
    }

    /**
    * Sets the id pianta of this varianti pianta.
    *
    * @param idPianta the id pianta of this varianti pianta
    */
    @Override
    public void setIdPianta(long idPianta) {
        _variantiPianta.setIdPianta(idPianta);
    }

    @Override
    public boolean isNew() {
        return _variantiPianta.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _variantiPianta.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _variantiPianta.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _variantiPianta.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _variantiPianta.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _variantiPianta.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _variantiPianta.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _variantiPianta.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _variantiPianta.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _variantiPianta.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _variantiPianta.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new VariantiPiantaWrapper((VariantiPianta) _variantiPianta.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.VariantiPianta variantiPianta) {
        return _variantiPianta.compareTo(variantiPianta);
    }

    @Override
    public int hashCode() {
        return _variantiPianta.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.VariantiPianta> toCacheModel() {
        return _variantiPianta.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.VariantiPianta toEscapedModel() {
        return new VariantiPiantaWrapper(_variantiPianta.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.VariantiPianta toUnescapedModel() {
        return new VariantiPiantaWrapper(_variantiPianta.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _variantiPianta.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _variantiPianta.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _variantiPianta.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VariantiPiantaWrapper)) {
            return false;
        }

        VariantiPiantaWrapper variantiPiantaWrapper = (VariantiPiantaWrapper) obj;

        if (Validator.equals(_variantiPianta,
                    variantiPiantaWrapper._variantiPianta)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public VariantiPianta getWrappedVariantiPianta() {
        return _variantiPianta;
    }

    @Override
    public VariantiPianta getWrappedModel() {
        return _variantiPianta;
    }

    @Override
    public void resetOriginalValues() {
        _variantiPianta.resetOriginalValues();
    }
}
