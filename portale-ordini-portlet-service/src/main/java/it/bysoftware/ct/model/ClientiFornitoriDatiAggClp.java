package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil;
import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class ClientiFornitoriDatiAggClp extends BaseModelImpl<ClientiFornitoriDatiAgg>
    implements ClientiFornitoriDatiAgg {
    private String _codiceAnagrafica;
    private boolean _fornitore;
    private String _codiceAgente;
    private String _codiceEsenzione;
    private String _categoriaEconomica;
    private String _codiceRegionale;
    private String _codicePuntoVenditaGT;
    private String _tipoPagamento;
    private boolean _associato;
    private double _sconto;
    private String _codiceIBAN;
    private BaseModel<?> _clientiFornitoriDatiAggRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public ClientiFornitoriDatiAggClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return ClientiFornitoriDatiAgg.class;
    }

    @Override
    public String getModelClassName() {
        return ClientiFornitoriDatiAgg.class.getName();
    }

    @Override
    public ClientiFornitoriDatiAggPK getPrimaryKey() {
        return new ClientiFornitoriDatiAggPK(_codiceAnagrafica, _fornitore);
    }

    @Override
    public void setPrimaryKey(ClientiFornitoriDatiAggPK primaryKey) {
        setCodiceAnagrafica(primaryKey.codiceAnagrafica);
        setFornitore(primaryKey.fornitore);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return new ClientiFornitoriDatiAggPK(_codiceAnagrafica, _fornitore);
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((ClientiFornitoriDatiAggPK) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceAnagrafica", getCodiceAnagrafica());
        attributes.put("fornitore", getFornitore());
        attributes.put("codiceAgente", getCodiceAgente());
        attributes.put("codiceEsenzione", getCodiceEsenzione());
        attributes.put("categoriaEconomica", getCategoriaEconomica());
        attributes.put("codiceRegionale", getCodiceRegionale());
        attributes.put("codicePuntoVenditaGT", getCodicePuntoVenditaGT());
        attributes.put("tipoPagamento", getTipoPagamento());
        attributes.put("associato", getAssociato());
        attributes.put("sconto", getSconto());
        attributes.put("codiceIBAN", getCodiceIBAN());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceAnagrafica = (String) attributes.get("codiceAnagrafica");

        if (codiceAnagrafica != null) {
            setCodiceAnagrafica(codiceAnagrafica);
        }

        Boolean fornitore = (Boolean) attributes.get("fornitore");

        if (fornitore != null) {
            setFornitore(fornitore);
        }

        String codiceAgente = (String) attributes.get("codiceAgente");

        if (codiceAgente != null) {
            setCodiceAgente(codiceAgente);
        }

        String codiceEsenzione = (String) attributes.get("codiceEsenzione");

        if (codiceEsenzione != null) {
            setCodiceEsenzione(codiceEsenzione);
        }

        String categoriaEconomica = (String) attributes.get(
                "categoriaEconomica");

        if (categoriaEconomica != null) {
            setCategoriaEconomica(categoriaEconomica);
        }

        String codiceRegionale = (String) attributes.get("codiceRegionale");

        if (codiceRegionale != null) {
            setCodiceRegionale(codiceRegionale);
        }

        String codicePuntoVenditaGT = (String) attributes.get(
                "codicePuntoVenditaGT");

        if (codicePuntoVenditaGT != null) {
            setCodicePuntoVenditaGT(codicePuntoVenditaGT);
        }

        String tipoPagamento = (String) attributes.get("tipoPagamento");

        if (tipoPagamento != null) {
            setTipoPagamento(tipoPagamento);
        }

        Boolean associato = (Boolean) attributes.get("associato");

        if (associato != null) {
            setAssociato(associato);
        }

        Double sconto = (Double) attributes.get("sconto");

        if (sconto != null) {
            setSconto(sconto);
        }

        String codiceIBAN = (String) attributes.get("codiceIBAN");

        if (codiceIBAN != null) {
            setCodiceIBAN(codiceIBAN);
        }
    }

    @Override
    public String getCodiceAnagrafica() {
        return _codiceAnagrafica;
    }

    @Override
    public void setCodiceAnagrafica(String codiceAnagrafica) {
        _codiceAnagrafica = codiceAnagrafica;

        if (_clientiFornitoriDatiAggRemoteModel != null) {
            try {
                Class<?> clazz = _clientiFornitoriDatiAggRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceAnagrafica",
                        String.class);

                method.invoke(_clientiFornitoriDatiAggRemoteModel,
                    codiceAnagrafica);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getFornitore() {
        return _fornitore;
    }

    @Override
    public boolean isFornitore() {
        return _fornitore;
    }

    @Override
    public void setFornitore(boolean fornitore) {
        _fornitore = fornitore;

        if (_clientiFornitoriDatiAggRemoteModel != null) {
            try {
                Class<?> clazz = _clientiFornitoriDatiAggRemoteModel.getClass();

                Method method = clazz.getMethod("setFornitore", boolean.class);

                method.invoke(_clientiFornitoriDatiAggRemoteModel, fornitore);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceAgente() {
        return _codiceAgente;
    }

    @Override
    public void setCodiceAgente(String codiceAgente) {
        _codiceAgente = codiceAgente;

        if (_clientiFornitoriDatiAggRemoteModel != null) {
            try {
                Class<?> clazz = _clientiFornitoriDatiAggRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceAgente", String.class);

                method.invoke(_clientiFornitoriDatiAggRemoteModel, codiceAgente);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceEsenzione() {
        return _codiceEsenzione;
    }

    @Override
    public void setCodiceEsenzione(String codiceEsenzione) {
        _codiceEsenzione = codiceEsenzione;

        if (_clientiFornitoriDatiAggRemoteModel != null) {
            try {
                Class<?> clazz = _clientiFornitoriDatiAggRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceEsenzione",
                        String.class);

                method.invoke(_clientiFornitoriDatiAggRemoteModel,
                    codiceEsenzione);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCategoriaEconomica() {
        return _categoriaEconomica;
    }

    @Override
    public void setCategoriaEconomica(String categoriaEconomica) {
        _categoriaEconomica = categoriaEconomica;

        if (_clientiFornitoriDatiAggRemoteModel != null) {
            try {
                Class<?> clazz = _clientiFornitoriDatiAggRemoteModel.getClass();

                Method method = clazz.getMethod("setCategoriaEconomica",
                        String.class);

                method.invoke(_clientiFornitoriDatiAggRemoteModel,
                    categoriaEconomica);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceRegionale() {
        return _codiceRegionale;
    }

    @Override
    public void setCodiceRegionale(String codiceRegionale) {
        _codiceRegionale = codiceRegionale;

        if (_clientiFornitoriDatiAggRemoteModel != null) {
            try {
                Class<?> clazz = _clientiFornitoriDatiAggRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceRegionale",
                        String.class);

                method.invoke(_clientiFornitoriDatiAggRemoteModel,
                    codiceRegionale);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodicePuntoVenditaGT() {
        return _codicePuntoVenditaGT;
    }

    @Override
    public void setCodicePuntoVenditaGT(String codicePuntoVenditaGT) {
        _codicePuntoVenditaGT = codicePuntoVenditaGT;

        if (_clientiFornitoriDatiAggRemoteModel != null) {
            try {
                Class<?> clazz = _clientiFornitoriDatiAggRemoteModel.getClass();

                Method method = clazz.getMethod("setCodicePuntoVenditaGT",
                        String.class);

                method.invoke(_clientiFornitoriDatiAggRemoteModel,
                    codicePuntoVenditaGT);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getTipoPagamento() {
        return _tipoPagamento;
    }

    @Override
    public void setTipoPagamento(String tipoPagamento) {
        _tipoPagamento = tipoPagamento;

        if (_clientiFornitoriDatiAggRemoteModel != null) {
            try {
                Class<?> clazz = _clientiFornitoriDatiAggRemoteModel.getClass();

                Method method = clazz.getMethod("setTipoPagamento", String.class);

                method.invoke(_clientiFornitoriDatiAggRemoteModel, tipoPagamento);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getAssociato() {
        return _associato;
    }

    @Override
    public boolean isAssociato() {
        return _associato;
    }

    @Override
    public void setAssociato(boolean associato) {
        _associato = associato;

        if (_clientiFornitoriDatiAggRemoteModel != null) {
            try {
                Class<?> clazz = _clientiFornitoriDatiAggRemoteModel.getClass();

                Method method = clazz.getMethod("setAssociato", boolean.class);

                method.invoke(_clientiFornitoriDatiAggRemoteModel, associato);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getSconto() {
        return _sconto;
    }

    @Override
    public void setSconto(double sconto) {
        _sconto = sconto;

        if (_clientiFornitoriDatiAggRemoteModel != null) {
            try {
                Class<?> clazz = _clientiFornitoriDatiAggRemoteModel.getClass();

                Method method = clazz.getMethod("setSconto", double.class);

                method.invoke(_clientiFornitoriDatiAggRemoteModel, sconto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceIBAN() {
        return _codiceIBAN;
    }

    @Override
    public void setCodiceIBAN(String codiceIBAN) {
        _codiceIBAN = codiceIBAN;

        if (_clientiFornitoriDatiAggRemoteModel != null) {
            try {
                Class<?> clazz = _clientiFornitoriDatiAggRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceIBAN", String.class);

                method.invoke(_clientiFornitoriDatiAggRemoteModel, codiceIBAN);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getClientiFornitoriDatiAggRemoteModel() {
        return _clientiFornitoriDatiAggRemoteModel;
    }

    public void setClientiFornitoriDatiAggRemoteModel(
        BaseModel<?> clientiFornitoriDatiAggRemoteModel) {
        _clientiFornitoriDatiAggRemoteModel = clientiFornitoriDatiAggRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _clientiFornitoriDatiAggRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_clientiFornitoriDatiAggRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            ClientiFornitoriDatiAggLocalServiceUtil.addClientiFornitoriDatiAgg(this);
        } else {
            ClientiFornitoriDatiAggLocalServiceUtil.updateClientiFornitoriDatiAgg(this);
        }
    }

    @Override
    public ClientiFornitoriDatiAgg toEscapedModel() {
        return (ClientiFornitoriDatiAgg) ProxyUtil.newProxyInstance(ClientiFornitoriDatiAgg.class.getClassLoader(),
            new Class[] { ClientiFornitoriDatiAgg.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        ClientiFornitoriDatiAggClp clone = new ClientiFornitoriDatiAggClp();

        clone.setCodiceAnagrafica(getCodiceAnagrafica());
        clone.setFornitore(getFornitore());
        clone.setCodiceAgente(getCodiceAgente());
        clone.setCodiceEsenzione(getCodiceEsenzione());
        clone.setCategoriaEconomica(getCategoriaEconomica());
        clone.setCodiceRegionale(getCodiceRegionale());
        clone.setCodicePuntoVenditaGT(getCodicePuntoVenditaGT());
        clone.setTipoPagamento(getTipoPagamento());
        clone.setAssociato(getAssociato());
        clone.setSconto(getSconto());
        clone.setCodiceIBAN(getCodiceIBAN());

        return clone;
    }

    @Override
    public int compareTo(ClientiFornitoriDatiAgg clientiFornitoriDatiAgg) {
        ClientiFornitoriDatiAggPK primaryKey = clientiFornitoriDatiAgg.getPrimaryKey();

        return getPrimaryKey().compareTo(primaryKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ClientiFornitoriDatiAggClp)) {
            return false;
        }

        ClientiFornitoriDatiAggClp clientiFornitoriDatiAgg = (ClientiFornitoriDatiAggClp) obj;

        ClientiFornitoriDatiAggPK primaryKey = clientiFornitoriDatiAgg.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(23);

        sb.append("{codiceAnagrafica=");
        sb.append(getCodiceAnagrafica());
        sb.append(", fornitore=");
        sb.append(getFornitore());
        sb.append(", codiceAgente=");
        sb.append(getCodiceAgente());
        sb.append(", codiceEsenzione=");
        sb.append(getCodiceEsenzione());
        sb.append(", categoriaEconomica=");
        sb.append(getCategoriaEconomica());
        sb.append(", codiceRegionale=");
        sb.append(getCodiceRegionale());
        sb.append(", codicePuntoVenditaGT=");
        sb.append(getCodicePuntoVenditaGT());
        sb.append(", tipoPagamento=");
        sb.append(getTipoPagamento());
        sb.append(", associato=");
        sb.append(getAssociato());
        sb.append(", sconto=");
        sb.append(getSconto());
        sb.append(", codiceIBAN=");
        sb.append(getCodiceIBAN());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(37);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.ClientiFornitoriDatiAgg");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>codiceAnagrafica</column-name><column-value><![CDATA[");
        sb.append(getCodiceAnagrafica());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>fornitore</column-name><column-value><![CDATA[");
        sb.append(getFornitore());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceAgente</column-name><column-value><![CDATA[");
        sb.append(getCodiceAgente());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceEsenzione</column-name><column-value><![CDATA[");
        sb.append(getCodiceEsenzione());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>categoriaEconomica</column-name><column-value><![CDATA[");
        sb.append(getCategoriaEconomica());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceRegionale</column-name><column-value><![CDATA[");
        sb.append(getCodiceRegionale());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codicePuntoVenditaGT</column-name><column-value><![CDATA[");
        sb.append(getCodicePuntoVenditaGT());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tipoPagamento</column-name><column-value><![CDATA[");
        sb.append(getTipoPagamento());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>associato</column-name><column-value><![CDATA[");
        sb.append(getAssociato());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>sconto</column-name><column-value><![CDATA[");
        sb.append(getSconto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceIBAN</column-name><column-value><![CDATA[");
        sb.append(getCodiceIBAN());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
