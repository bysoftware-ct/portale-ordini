package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the ClientiFornitoriDatiAgg service. Represents a row in the &quot;ClientiFornitoriDatiAgg&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see ClientiFornitoriDatiAggModel
 * @see it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggImpl
 * @see it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl
 * @generated
 */
public interface ClientiFornitoriDatiAgg extends ClientiFornitoriDatiAggModel,
    PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
