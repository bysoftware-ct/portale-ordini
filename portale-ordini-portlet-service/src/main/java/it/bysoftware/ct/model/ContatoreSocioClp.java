package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.ContatoreSocioLocalServiceUtil;
import it.bysoftware.ct.service.persistence.ContatoreSocioPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class ContatoreSocioClp extends BaseModelImpl<ContatoreSocio>
    implements ContatoreSocio {
    private int _anno;
    private String _codiceSocio;
    private String _tipoDocumento;
    private int _numero;
    private BaseModel<?> _contatoreSocioRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public ContatoreSocioClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return ContatoreSocio.class;
    }

    @Override
    public String getModelClassName() {
        return ContatoreSocio.class.getName();
    }

    @Override
    public ContatoreSocioPK getPrimaryKey() {
        return new ContatoreSocioPK(_anno, _codiceSocio, _tipoDocumento);
    }

    @Override
    public void setPrimaryKey(ContatoreSocioPK primaryKey) {
        setAnno(primaryKey.anno);
        setCodiceSocio(primaryKey.codiceSocio);
        setTipoDocumento(primaryKey.tipoDocumento);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return new ContatoreSocioPK(_anno, _codiceSocio, _tipoDocumento);
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((ContatoreSocioPK) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("anno", getAnno());
        attributes.put("codiceSocio", getCodiceSocio());
        attributes.put("tipoDocumento", getTipoDocumento());
        attributes.put("numero", getNumero());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Integer anno = (Integer) attributes.get("anno");

        if (anno != null) {
            setAnno(anno);
        }

        String codiceSocio = (String) attributes.get("codiceSocio");

        if (codiceSocio != null) {
            setCodiceSocio(codiceSocio);
        }

        String tipoDocumento = (String) attributes.get("tipoDocumento");

        if (tipoDocumento != null) {
            setTipoDocumento(tipoDocumento);
        }

        Integer numero = (Integer) attributes.get("numero");

        if (numero != null) {
            setNumero(numero);
        }
    }

    @Override
    public int getAnno() {
        return _anno;
    }

    @Override
    public void setAnno(int anno) {
        _anno = anno;

        if (_contatoreSocioRemoteModel != null) {
            try {
                Class<?> clazz = _contatoreSocioRemoteModel.getClass();

                Method method = clazz.getMethod("setAnno", int.class);

                method.invoke(_contatoreSocioRemoteModel, anno);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceSocio() {
        return _codiceSocio;
    }

    @Override
    public void setCodiceSocio(String codiceSocio) {
        _codiceSocio = codiceSocio;

        if (_contatoreSocioRemoteModel != null) {
            try {
                Class<?> clazz = _contatoreSocioRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceSocio", String.class);

                method.invoke(_contatoreSocioRemoteModel, codiceSocio);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getTipoDocumento() {
        return _tipoDocumento;
    }

    @Override
    public void setTipoDocumento(String tipoDocumento) {
        _tipoDocumento = tipoDocumento;

        if (_contatoreSocioRemoteModel != null) {
            try {
                Class<?> clazz = _contatoreSocioRemoteModel.getClass();

                Method method = clazz.getMethod("setTipoDocumento", String.class);

                method.invoke(_contatoreSocioRemoteModel, tipoDocumento);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getNumero() {
        return _numero;
    }

    @Override
    public void setNumero(int numero) {
        _numero = numero;

        if (_contatoreSocioRemoteModel != null) {
            try {
                Class<?> clazz = _contatoreSocioRemoteModel.getClass();

                Method method = clazz.getMethod("setNumero", int.class);

                method.invoke(_contatoreSocioRemoteModel, numero);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getContatoreSocioRemoteModel() {
        return _contatoreSocioRemoteModel;
    }

    public void setContatoreSocioRemoteModel(
        BaseModel<?> contatoreSocioRemoteModel) {
        _contatoreSocioRemoteModel = contatoreSocioRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _contatoreSocioRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_contatoreSocioRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            ContatoreSocioLocalServiceUtil.addContatoreSocio(this);
        } else {
            ContatoreSocioLocalServiceUtil.updateContatoreSocio(this);
        }
    }

    @Override
    public ContatoreSocio toEscapedModel() {
        return (ContatoreSocio) ProxyUtil.newProxyInstance(ContatoreSocio.class.getClassLoader(),
            new Class[] { ContatoreSocio.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        ContatoreSocioClp clone = new ContatoreSocioClp();

        clone.setAnno(getAnno());
        clone.setCodiceSocio(getCodiceSocio());
        clone.setTipoDocumento(getTipoDocumento());
        clone.setNumero(getNumero());

        return clone;
    }

    @Override
    public int compareTo(ContatoreSocio contatoreSocio) {
        ContatoreSocioPK primaryKey = contatoreSocio.getPrimaryKey();

        return getPrimaryKey().compareTo(primaryKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ContatoreSocioClp)) {
            return false;
        }

        ContatoreSocioClp contatoreSocio = (ContatoreSocioClp) obj;

        ContatoreSocioPK primaryKey = contatoreSocio.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(9);

        sb.append("{anno=");
        sb.append(getAnno());
        sb.append(", codiceSocio=");
        sb.append(getCodiceSocio());
        sb.append(", tipoDocumento=");
        sb.append(getTipoDocumento());
        sb.append(", numero=");
        sb.append(getNumero());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(16);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.ContatoreSocio");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>anno</column-name><column-value><![CDATA[");
        sb.append(getAnno());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceSocio</column-name><column-value><![CDATA[");
        sb.append(getCodiceSocio());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tipoDocumento</column-name><column-value><![CDATA[");
        sb.append(getTipoDocumento());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>numero</column-name><column-value><![CDATA[");
        sb.append(getNumero());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
