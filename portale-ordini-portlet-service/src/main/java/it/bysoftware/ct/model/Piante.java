package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Piante service. Represents a row in the &quot;_piante&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see PianteModel
 * @see it.bysoftware.ct.model.impl.PianteImpl
 * @see it.bysoftware.ct.model.impl.PianteModelImpl
 * @generated
 */
public interface Piante extends PianteModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.PianteImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
