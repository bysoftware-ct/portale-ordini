package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the CausaliContabili service. Represents a row in the &quot;CausaliContabili&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see CausaliContabiliModel
 * @see it.bysoftware.ct.model.impl.CausaliContabiliImpl
 * @see it.bysoftware.ct.model.impl.CausaliContabiliModelImpl
 * @generated
 */
public interface CausaliContabili extends CausaliContabiliModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.CausaliContabiliImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
