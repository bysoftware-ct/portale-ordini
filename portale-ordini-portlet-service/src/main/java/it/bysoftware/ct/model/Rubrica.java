package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Rubrica service. Represents a row in the &quot;Rubrica&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see RubricaModel
 * @see it.bysoftware.ct.model.impl.RubricaImpl
 * @see it.bysoftware.ct.model.impl.RubricaModelImpl
 * @generated
 */
public interface Rubrica extends RubricaModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.RubricaImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
