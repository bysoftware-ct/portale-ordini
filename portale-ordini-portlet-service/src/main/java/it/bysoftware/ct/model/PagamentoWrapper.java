package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Pagamento}.
 * </p>
 *
 * @author Mario Torrisi
 * @see Pagamento
 * @generated
 */
public class PagamentoWrapper implements Pagamento, ModelWrapper<Pagamento> {
    private Pagamento _pagamento;

    public PagamentoWrapper(Pagamento pagamento) {
        _pagamento = pagamento;
    }

    @Override
    public Class<?> getModelClass() {
        return Pagamento.class;
    }

    @Override
    public String getModelClassName() {
        return Pagamento.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("anno", getAnno());
        attributes.put("codiceSoggetto", getCodiceSoggetto());
        attributes.put("dataCreazione", getDataCreazione());
        attributes.put("dataPagamento", getDataPagamento());
        attributes.put("credito", getCredito());
        attributes.put("importo", getImporto());
        attributes.put("saldo", getSaldo());
        attributes.put("stato", getStato());
        attributes.put("nota", getNota());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Integer id = (Integer) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Integer anno = (Integer) attributes.get("anno");

        if (anno != null) {
            setAnno(anno);
        }

        String codiceSoggetto = (String) attributes.get("codiceSoggetto");

        if (codiceSoggetto != null) {
            setCodiceSoggetto(codiceSoggetto);
        }

        Date dataCreazione = (Date) attributes.get("dataCreazione");

        if (dataCreazione != null) {
            setDataCreazione(dataCreazione);
        }

        Date dataPagamento = (Date) attributes.get("dataPagamento");

        if (dataPagamento != null) {
            setDataPagamento(dataPagamento);
        }

        Double credito = (Double) attributes.get("credito");

        if (credito != null) {
            setCredito(credito);
        }

        Double importo = (Double) attributes.get("importo");

        if (importo != null) {
            setImporto(importo);
        }

        Double saldo = (Double) attributes.get("saldo");

        if (saldo != null) {
            setSaldo(saldo);
        }

        Integer stato = (Integer) attributes.get("stato");

        if (stato != null) {
            setStato(stato);
        }

        String nota = (String) attributes.get("nota");

        if (nota != null) {
            setNota(nota);
        }
    }

    /**
    * Returns the primary key of this pagamento.
    *
    * @return the primary key of this pagamento
    */
    @Override
    public it.bysoftware.ct.service.persistence.PagamentoPK getPrimaryKey() {
        return _pagamento.getPrimaryKey();
    }

    /**
    * Sets the primary key of this pagamento.
    *
    * @param primaryKey the primary key of this pagamento
    */
    @Override
    public void setPrimaryKey(
        it.bysoftware.ct.service.persistence.PagamentoPK primaryKey) {
        _pagamento.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this pagamento.
    *
    * @return the ID of this pagamento
    */
    @Override
    public int getId() {
        return _pagamento.getId();
    }

    /**
    * Sets the ID of this pagamento.
    *
    * @param id the ID of this pagamento
    */
    @Override
    public void setId(int id) {
        _pagamento.setId(id);
    }

    /**
    * Returns the anno of this pagamento.
    *
    * @return the anno of this pagamento
    */
    @Override
    public int getAnno() {
        return _pagamento.getAnno();
    }

    /**
    * Sets the anno of this pagamento.
    *
    * @param anno the anno of this pagamento
    */
    @Override
    public void setAnno(int anno) {
        _pagamento.setAnno(anno);
    }

    /**
    * Returns the codice soggetto of this pagamento.
    *
    * @return the codice soggetto of this pagamento
    */
    @Override
    public java.lang.String getCodiceSoggetto() {
        return _pagamento.getCodiceSoggetto();
    }

    /**
    * Sets the codice soggetto of this pagamento.
    *
    * @param codiceSoggetto the codice soggetto of this pagamento
    */
    @Override
    public void setCodiceSoggetto(java.lang.String codiceSoggetto) {
        _pagamento.setCodiceSoggetto(codiceSoggetto);
    }

    /**
    * Returns the data creazione of this pagamento.
    *
    * @return the data creazione of this pagamento
    */
    @Override
    public java.util.Date getDataCreazione() {
        return _pagamento.getDataCreazione();
    }

    /**
    * Sets the data creazione of this pagamento.
    *
    * @param dataCreazione the data creazione of this pagamento
    */
    @Override
    public void setDataCreazione(java.util.Date dataCreazione) {
        _pagamento.setDataCreazione(dataCreazione);
    }

    /**
    * Returns the data pagamento of this pagamento.
    *
    * @return the data pagamento of this pagamento
    */
    @Override
    public java.util.Date getDataPagamento() {
        return _pagamento.getDataPagamento();
    }

    /**
    * Sets the data pagamento of this pagamento.
    *
    * @param dataPagamento the data pagamento of this pagamento
    */
    @Override
    public void setDataPagamento(java.util.Date dataPagamento) {
        _pagamento.setDataPagamento(dataPagamento);
    }

    /**
    * Returns the credito of this pagamento.
    *
    * @return the credito of this pagamento
    */
    @Override
    public double getCredito() {
        return _pagamento.getCredito();
    }

    /**
    * Sets the credito of this pagamento.
    *
    * @param credito the credito of this pagamento
    */
    @Override
    public void setCredito(double credito) {
        _pagamento.setCredito(credito);
    }

    /**
    * Returns the importo of this pagamento.
    *
    * @return the importo of this pagamento
    */
    @Override
    public double getImporto() {
        return _pagamento.getImporto();
    }

    /**
    * Sets the importo of this pagamento.
    *
    * @param importo the importo of this pagamento
    */
    @Override
    public void setImporto(double importo) {
        _pagamento.setImporto(importo);
    }

    /**
    * Returns the saldo of this pagamento.
    *
    * @return the saldo of this pagamento
    */
    @Override
    public double getSaldo() {
        return _pagamento.getSaldo();
    }

    /**
    * Sets the saldo of this pagamento.
    *
    * @param saldo the saldo of this pagamento
    */
    @Override
    public void setSaldo(double saldo) {
        _pagamento.setSaldo(saldo);
    }

    /**
    * Returns the stato of this pagamento.
    *
    * @return the stato of this pagamento
    */
    @Override
    public int getStato() {
        return _pagamento.getStato();
    }

    /**
    * Sets the stato of this pagamento.
    *
    * @param stato the stato of this pagamento
    */
    @Override
    public void setStato(int stato) {
        _pagamento.setStato(stato);
    }

    /**
    * Returns the nota of this pagamento.
    *
    * @return the nota of this pagamento
    */
    @Override
    public java.lang.String getNota() {
        return _pagamento.getNota();
    }

    /**
    * Sets the nota of this pagamento.
    *
    * @param nota the nota of this pagamento
    */
    @Override
    public void setNota(java.lang.String nota) {
        _pagamento.setNota(nota);
    }

    @Override
    public boolean isNew() {
        return _pagamento.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _pagamento.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _pagamento.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _pagamento.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _pagamento.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _pagamento.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _pagamento.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _pagamento.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _pagamento.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _pagamento.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _pagamento.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new PagamentoWrapper((Pagamento) _pagamento.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.Pagamento pagamento) {
        return _pagamento.compareTo(pagamento);
    }

    @Override
    public int hashCode() {
        return _pagamento.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.Pagamento> toCacheModel() {
        return _pagamento.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.Pagamento toEscapedModel() {
        return new PagamentoWrapper(_pagamento.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.Pagamento toUnescapedModel() {
        return new PagamentoWrapper(_pagamento.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _pagamento.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _pagamento.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _pagamento.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PagamentoWrapper)) {
            return false;
        }

        PagamentoWrapper pagamentoWrapper = (PagamentoWrapper) obj;

        if (Validator.equals(_pagamento, pagamentoWrapper._pagamento)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Pagamento getWrappedPagamento() {
        return _pagamento;
    }

    @Override
    public Pagamento getWrappedModel() {
        return _pagamento;
    }

    @Override
    public void resetOriginalValues() {
        _pagamento.resetOriginalValues();
    }
}
