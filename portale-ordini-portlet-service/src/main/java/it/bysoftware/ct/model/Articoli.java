package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Articoli service. Represents a row in the &quot;Articoli&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see ArticoliModel
 * @see it.bysoftware.ct.model.impl.ArticoliImpl
 * @see it.bysoftware.ct.model.impl.ArticoliModelImpl
 * @generated
 */
public interface Articoli extends ArticoliModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.ArticoliImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public it.bysoftware.ct.model.Piante getPianta(java.lang.String code);
}
