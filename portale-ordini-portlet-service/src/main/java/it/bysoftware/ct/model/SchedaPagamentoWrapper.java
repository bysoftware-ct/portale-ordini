package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link SchedaPagamento}.
 * </p>
 *
 * @author Mario Torrisi
 * @see SchedaPagamento
 * @generated
 */
public class SchedaPagamentoWrapper implements SchedaPagamento,
    ModelWrapper<SchedaPagamento> {
    private SchedaPagamento _schedaPagamento;

    public SchedaPagamentoWrapper(SchedaPagamento schedaPagamento) {
        _schedaPagamento = schedaPagamento;
    }

    @Override
    public Class<?> getModelClass() {
        return SchedaPagamento.class;
    }

    @Override
    public String getModelClassName() {
        return SchedaPagamento.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("esercizioRegistrazione", getEsercizioRegistrazione());
        attributes.put("codiceSoggetto", getCodiceSoggetto());
        attributes.put("numeroPartita", getNumeroPartita());
        attributes.put("numeroScadenza", getNumeroScadenza());
        attributes.put("tipoSoggetto", getTipoSoggetto());
        attributes.put("esercizioRegistrazioneComp",
            getEsercizioRegistrazioneComp());
        attributes.put("codiceSoggettoComp", getCodiceSoggettoComp());
        attributes.put("numeroPartitaComp", getNumeroPartitaComp());
        attributes.put("numeroScadenzaComp", getNumeroScadenzaComp());
        attributes.put("tipoSoggettoComp", getTipoSoggettoComp());
        attributes.put("differenza", getDifferenza());
        attributes.put("idPagamento", getIdPagamento());
        attributes.put("annoPagamento", getAnnoPagamento());
        attributes.put("stato", getStato());
        attributes.put("nota", getNota());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Integer esercizioRegistrazione = (Integer) attributes.get(
                "esercizioRegistrazione");

        if (esercizioRegistrazione != null) {
            setEsercizioRegistrazione(esercizioRegistrazione);
        }

        String codiceSoggetto = (String) attributes.get("codiceSoggetto");

        if (codiceSoggetto != null) {
            setCodiceSoggetto(codiceSoggetto);
        }

        Integer numeroPartita = (Integer) attributes.get("numeroPartita");

        if (numeroPartita != null) {
            setNumeroPartita(numeroPartita);
        }

        Integer numeroScadenza = (Integer) attributes.get("numeroScadenza");

        if (numeroScadenza != null) {
            setNumeroScadenza(numeroScadenza);
        }

        Boolean tipoSoggetto = (Boolean) attributes.get("tipoSoggetto");

        if (tipoSoggetto != null) {
            setTipoSoggetto(tipoSoggetto);
        }

        Integer esercizioRegistrazioneComp = (Integer) attributes.get(
                "esercizioRegistrazioneComp");

        if (esercizioRegistrazioneComp != null) {
            setEsercizioRegistrazioneComp(esercizioRegistrazioneComp);
        }

        String codiceSoggettoComp = (String) attributes.get(
                "codiceSoggettoComp");

        if (codiceSoggettoComp != null) {
            setCodiceSoggettoComp(codiceSoggettoComp);
        }

        Integer numeroPartitaComp = (Integer) attributes.get(
                "numeroPartitaComp");

        if (numeroPartitaComp != null) {
            setNumeroPartitaComp(numeroPartitaComp);
        }

        Integer numeroScadenzaComp = (Integer) attributes.get(
                "numeroScadenzaComp");

        if (numeroScadenzaComp != null) {
            setNumeroScadenzaComp(numeroScadenzaComp);
        }

        Boolean tipoSoggettoComp = (Boolean) attributes.get("tipoSoggettoComp");

        if (tipoSoggettoComp != null) {
            setTipoSoggettoComp(tipoSoggettoComp);
        }

        Double differenza = (Double) attributes.get("differenza");

        if (differenza != null) {
            setDifferenza(differenza);
        }

        Integer idPagamento = (Integer) attributes.get("idPagamento");

        if (idPagamento != null) {
            setIdPagamento(idPagamento);
        }

        Integer annoPagamento = (Integer) attributes.get("annoPagamento");

        if (annoPagamento != null) {
            setAnnoPagamento(annoPagamento);
        }

        Integer stato = (Integer) attributes.get("stato");

        if (stato != null) {
            setStato(stato);
        }

        String nota = (String) attributes.get("nota");

        if (nota != null) {
            setNota(nota);
        }
    }

    /**
    * Returns the primary key of this scheda pagamento.
    *
    * @return the primary key of this scheda pagamento
    */
    @Override
    public it.bysoftware.ct.service.persistence.SchedaPagamentoPK getPrimaryKey() {
        return _schedaPagamento.getPrimaryKey();
    }

    /**
    * Sets the primary key of this scheda pagamento.
    *
    * @param primaryKey the primary key of this scheda pagamento
    */
    @Override
    public void setPrimaryKey(
        it.bysoftware.ct.service.persistence.SchedaPagamentoPK primaryKey) {
        _schedaPagamento.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the esercizio registrazione of this scheda pagamento.
    *
    * @return the esercizio registrazione of this scheda pagamento
    */
    @Override
    public int getEsercizioRegistrazione() {
        return _schedaPagamento.getEsercizioRegistrazione();
    }

    /**
    * Sets the esercizio registrazione of this scheda pagamento.
    *
    * @param esercizioRegistrazione the esercizio registrazione of this scheda pagamento
    */
    @Override
    public void setEsercizioRegistrazione(int esercizioRegistrazione) {
        _schedaPagamento.setEsercizioRegistrazione(esercizioRegistrazione);
    }

    /**
    * Returns the codice soggetto of this scheda pagamento.
    *
    * @return the codice soggetto of this scheda pagamento
    */
    @Override
    public java.lang.String getCodiceSoggetto() {
        return _schedaPagamento.getCodiceSoggetto();
    }

    /**
    * Sets the codice soggetto of this scheda pagamento.
    *
    * @param codiceSoggetto the codice soggetto of this scheda pagamento
    */
    @Override
    public void setCodiceSoggetto(java.lang.String codiceSoggetto) {
        _schedaPagamento.setCodiceSoggetto(codiceSoggetto);
    }

    /**
    * Returns the numero partita of this scheda pagamento.
    *
    * @return the numero partita of this scheda pagamento
    */
    @Override
    public int getNumeroPartita() {
        return _schedaPagamento.getNumeroPartita();
    }

    /**
    * Sets the numero partita of this scheda pagamento.
    *
    * @param numeroPartita the numero partita of this scheda pagamento
    */
    @Override
    public void setNumeroPartita(int numeroPartita) {
        _schedaPagamento.setNumeroPartita(numeroPartita);
    }

    /**
    * Returns the numero scadenza of this scheda pagamento.
    *
    * @return the numero scadenza of this scheda pagamento
    */
    @Override
    public int getNumeroScadenza() {
        return _schedaPagamento.getNumeroScadenza();
    }

    /**
    * Sets the numero scadenza of this scheda pagamento.
    *
    * @param numeroScadenza the numero scadenza of this scheda pagamento
    */
    @Override
    public void setNumeroScadenza(int numeroScadenza) {
        _schedaPagamento.setNumeroScadenza(numeroScadenza);
    }

    /**
    * Returns the tipo soggetto of this scheda pagamento.
    *
    * @return the tipo soggetto of this scheda pagamento
    */
    @Override
    public boolean getTipoSoggetto() {
        return _schedaPagamento.getTipoSoggetto();
    }

    /**
    * Returns <code>true</code> if this scheda pagamento is tipo soggetto.
    *
    * @return <code>true</code> if this scheda pagamento is tipo soggetto; <code>false</code> otherwise
    */
    @Override
    public boolean isTipoSoggetto() {
        return _schedaPagamento.isTipoSoggetto();
    }

    /**
    * Sets whether this scheda pagamento is tipo soggetto.
    *
    * @param tipoSoggetto the tipo soggetto of this scheda pagamento
    */
    @Override
    public void setTipoSoggetto(boolean tipoSoggetto) {
        _schedaPagamento.setTipoSoggetto(tipoSoggetto);
    }

    /**
    * Returns the esercizio registrazione comp of this scheda pagamento.
    *
    * @return the esercizio registrazione comp of this scheda pagamento
    */
    @Override
    public int getEsercizioRegistrazioneComp() {
        return _schedaPagamento.getEsercizioRegistrazioneComp();
    }

    /**
    * Sets the esercizio registrazione comp of this scheda pagamento.
    *
    * @param esercizioRegistrazioneComp the esercizio registrazione comp of this scheda pagamento
    */
    @Override
    public void setEsercizioRegistrazioneComp(int esercizioRegistrazioneComp) {
        _schedaPagamento.setEsercizioRegistrazioneComp(esercizioRegistrazioneComp);
    }

    /**
    * Returns the codice soggetto comp of this scheda pagamento.
    *
    * @return the codice soggetto comp of this scheda pagamento
    */
    @Override
    public java.lang.String getCodiceSoggettoComp() {
        return _schedaPagamento.getCodiceSoggettoComp();
    }

    /**
    * Sets the codice soggetto comp of this scheda pagamento.
    *
    * @param codiceSoggettoComp the codice soggetto comp of this scheda pagamento
    */
    @Override
    public void setCodiceSoggettoComp(java.lang.String codiceSoggettoComp) {
        _schedaPagamento.setCodiceSoggettoComp(codiceSoggettoComp);
    }

    /**
    * Returns the numero partita comp of this scheda pagamento.
    *
    * @return the numero partita comp of this scheda pagamento
    */
    @Override
    public int getNumeroPartitaComp() {
        return _schedaPagamento.getNumeroPartitaComp();
    }

    /**
    * Sets the numero partita comp of this scheda pagamento.
    *
    * @param numeroPartitaComp the numero partita comp of this scheda pagamento
    */
    @Override
    public void setNumeroPartitaComp(int numeroPartitaComp) {
        _schedaPagamento.setNumeroPartitaComp(numeroPartitaComp);
    }

    /**
    * Returns the numero scadenza comp of this scheda pagamento.
    *
    * @return the numero scadenza comp of this scheda pagamento
    */
    @Override
    public int getNumeroScadenzaComp() {
        return _schedaPagamento.getNumeroScadenzaComp();
    }

    /**
    * Sets the numero scadenza comp of this scheda pagamento.
    *
    * @param numeroScadenzaComp the numero scadenza comp of this scheda pagamento
    */
    @Override
    public void setNumeroScadenzaComp(int numeroScadenzaComp) {
        _schedaPagamento.setNumeroScadenzaComp(numeroScadenzaComp);
    }

    /**
    * Returns the tipo soggetto comp of this scheda pagamento.
    *
    * @return the tipo soggetto comp of this scheda pagamento
    */
    @Override
    public boolean getTipoSoggettoComp() {
        return _schedaPagamento.getTipoSoggettoComp();
    }

    /**
    * Returns <code>true</code> if this scheda pagamento is tipo soggetto comp.
    *
    * @return <code>true</code> if this scheda pagamento is tipo soggetto comp; <code>false</code> otherwise
    */
    @Override
    public boolean isTipoSoggettoComp() {
        return _schedaPagamento.isTipoSoggettoComp();
    }

    /**
    * Sets whether this scheda pagamento is tipo soggetto comp.
    *
    * @param tipoSoggettoComp the tipo soggetto comp of this scheda pagamento
    */
    @Override
    public void setTipoSoggettoComp(boolean tipoSoggettoComp) {
        _schedaPagamento.setTipoSoggettoComp(tipoSoggettoComp);
    }

    /**
    * Returns the differenza of this scheda pagamento.
    *
    * @return the differenza of this scheda pagamento
    */
    @Override
    public double getDifferenza() {
        return _schedaPagamento.getDifferenza();
    }

    /**
    * Sets the differenza of this scheda pagamento.
    *
    * @param differenza the differenza of this scheda pagamento
    */
    @Override
    public void setDifferenza(double differenza) {
        _schedaPagamento.setDifferenza(differenza);
    }

    /**
    * Returns the id pagamento of this scheda pagamento.
    *
    * @return the id pagamento of this scheda pagamento
    */
    @Override
    public int getIdPagamento() {
        return _schedaPagamento.getIdPagamento();
    }

    /**
    * Sets the id pagamento of this scheda pagamento.
    *
    * @param idPagamento the id pagamento of this scheda pagamento
    */
    @Override
    public void setIdPagamento(int idPagamento) {
        _schedaPagamento.setIdPagamento(idPagamento);
    }

    /**
    * Returns the anno pagamento of this scheda pagamento.
    *
    * @return the anno pagamento of this scheda pagamento
    */
    @Override
    public int getAnnoPagamento() {
        return _schedaPagamento.getAnnoPagamento();
    }

    /**
    * Sets the anno pagamento of this scheda pagamento.
    *
    * @param annoPagamento the anno pagamento of this scheda pagamento
    */
    @Override
    public void setAnnoPagamento(int annoPagamento) {
        _schedaPagamento.setAnnoPagamento(annoPagamento);
    }

    /**
    * Returns the stato of this scheda pagamento.
    *
    * @return the stato of this scheda pagamento
    */
    @Override
    public int getStato() {
        return _schedaPagamento.getStato();
    }

    /**
    * Sets the stato of this scheda pagamento.
    *
    * @param stato the stato of this scheda pagamento
    */
    @Override
    public void setStato(int stato) {
        _schedaPagamento.setStato(stato);
    }

    /**
    * Returns the nota of this scheda pagamento.
    *
    * @return the nota of this scheda pagamento
    */
    @Override
    public java.lang.String getNota() {
        return _schedaPagamento.getNota();
    }

    /**
    * Sets the nota of this scheda pagamento.
    *
    * @param nota the nota of this scheda pagamento
    */
    @Override
    public void setNota(java.lang.String nota) {
        _schedaPagamento.setNota(nota);
    }

    @Override
    public boolean isNew() {
        return _schedaPagamento.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _schedaPagamento.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _schedaPagamento.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _schedaPagamento.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _schedaPagamento.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _schedaPagamento.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _schedaPagamento.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _schedaPagamento.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _schedaPagamento.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _schedaPagamento.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _schedaPagamento.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new SchedaPagamentoWrapper((SchedaPagamento) _schedaPagamento.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.SchedaPagamento schedaPagamento) {
        return _schedaPagamento.compareTo(schedaPagamento);
    }

    @Override
    public int hashCode() {
        return _schedaPagamento.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.SchedaPagamento> toCacheModel() {
        return _schedaPagamento.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.SchedaPagamento toEscapedModel() {
        return new SchedaPagamentoWrapper(_schedaPagamento.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.SchedaPagamento toUnescapedModel() {
        return new SchedaPagamentoWrapper(_schedaPagamento.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _schedaPagamento.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _schedaPagamento.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _schedaPagamento.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof SchedaPagamentoWrapper)) {
            return false;
        }

        SchedaPagamentoWrapper schedaPagamentoWrapper = (SchedaPagamentoWrapper) obj;

        if (Validator.equals(_schedaPagamento,
                    schedaPagamentoWrapper._schedaPagamento)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public SchedaPagamento getWrappedSchedaPagamento() {
        return _schedaPagamento;
    }

    @Override
    public SchedaPagamento getWrappedModel() {
        return _schedaPagamento;
    }

    @Override
    public void resetOriginalValues() {
        _schedaPagamento.resetOriginalValues();
    }
}
