package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the PianoPagamenti service. Represents a row in the &quot;Pagamenti&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see PianoPagamentiModel
 * @see it.bysoftware.ct.model.impl.PianoPagamentiImpl
 * @see it.bysoftware.ct.model.impl.PianoPagamentiModelImpl
 * @generated
 */
public interface PianoPagamenti extends PianoPagamentiModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.PianoPagamentiImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
