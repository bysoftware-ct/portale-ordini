package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the CodiceConsorzio service. Represents a row in the &quot;_codici_consorzio&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see CodiceConsorzioModel
 * @see it.bysoftware.ct.model.impl.CodiceConsorzioImpl
 * @see it.bysoftware.ct.model.impl.CodiceConsorzioModelImpl
 * @generated
 */
public interface CodiceConsorzio extends CodiceConsorzioModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.CodiceConsorzioImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
