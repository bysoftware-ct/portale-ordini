package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.PagamentoLocalServiceUtil;
import it.bysoftware.ct.service.persistence.PagamentoPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class PagamentoClp extends BaseModelImpl<Pagamento> implements Pagamento {
    private int _id;
    private int _anno;
    private String _codiceSoggetto;
    private Date _dataCreazione;
    private Date _dataPagamento;
    private double _credito;
    private double _importo;
    private double _saldo;
    private int _stato;
    private String _nota;
    private BaseModel<?> _pagamentoRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public PagamentoClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Pagamento.class;
    }

    @Override
    public String getModelClassName() {
        return Pagamento.class.getName();
    }

    @Override
    public PagamentoPK getPrimaryKey() {
        return new PagamentoPK(_id, _anno, _codiceSoggetto);
    }

    @Override
    public void setPrimaryKey(PagamentoPK primaryKey) {
        setId(primaryKey.id);
        setAnno(primaryKey.anno);
        setCodiceSoggetto(primaryKey.codiceSoggetto);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return new PagamentoPK(_id, _anno, _codiceSoggetto);
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((PagamentoPK) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("anno", getAnno());
        attributes.put("codiceSoggetto", getCodiceSoggetto());
        attributes.put("dataCreazione", getDataCreazione());
        attributes.put("dataPagamento", getDataPagamento());
        attributes.put("credito", getCredito());
        attributes.put("importo", getImporto());
        attributes.put("saldo", getSaldo());
        attributes.put("stato", getStato());
        attributes.put("nota", getNota());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Integer id = (Integer) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Integer anno = (Integer) attributes.get("anno");

        if (anno != null) {
            setAnno(anno);
        }

        String codiceSoggetto = (String) attributes.get("codiceSoggetto");

        if (codiceSoggetto != null) {
            setCodiceSoggetto(codiceSoggetto);
        }

        Date dataCreazione = (Date) attributes.get("dataCreazione");

        if (dataCreazione != null) {
            setDataCreazione(dataCreazione);
        }

        Date dataPagamento = (Date) attributes.get("dataPagamento");

        if (dataPagamento != null) {
            setDataPagamento(dataPagamento);
        }

        Double credito = (Double) attributes.get("credito");

        if (credito != null) {
            setCredito(credito);
        }

        Double importo = (Double) attributes.get("importo");

        if (importo != null) {
            setImporto(importo);
        }

        Double saldo = (Double) attributes.get("saldo");

        if (saldo != null) {
            setSaldo(saldo);
        }

        Integer stato = (Integer) attributes.get("stato");

        if (stato != null) {
            setStato(stato);
        }

        String nota = (String) attributes.get("nota");

        if (nota != null) {
            setNota(nota);
        }
    }

    @Override
    public int getId() {
        return _id;
    }

    @Override
    public void setId(int id) {
        _id = id;

        if (_pagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _pagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setId", int.class);

                method.invoke(_pagamentoRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getAnno() {
        return _anno;
    }

    @Override
    public void setAnno(int anno) {
        _anno = anno;

        if (_pagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _pagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setAnno", int.class);

                method.invoke(_pagamentoRemoteModel, anno);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceSoggetto() {
        return _codiceSoggetto;
    }

    @Override
    public void setCodiceSoggetto(String codiceSoggetto) {
        _codiceSoggetto = codiceSoggetto;

        if (_pagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _pagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceSoggetto",
                        String.class);

                method.invoke(_pagamentoRemoteModel, codiceSoggetto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getDataCreazione() {
        return _dataCreazione;
    }

    @Override
    public void setDataCreazione(Date dataCreazione) {
        _dataCreazione = dataCreazione;

        if (_pagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _pagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setDataCreazione", Date.class);

                method.invoke(_pagamentoRemoteModel, dataCreazione);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getDataPagamento() {
        return _dataPagamento;
    }

    @Override
    public void setDataPagamento(Date dataPagamento) {
        _dataPagamento = dataPagamento;

        if (_pagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _pagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setDataPagamento", Date.class);

                method.invoke(_pagamentoRemoteModel, dataPagamento);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getCredito() {
        return _credito;
    }

    @Override
    public void setCredito(double credito) {
        _credito = credito;

        if (_pagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _pagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCredito", double.class);

                method.invoke(_pagamentoRemoteModel, credito);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getImporto() {
        return _importo;
    }

    @Override
    public void setImporto(double importo) {
        _importo = importo;

        if (_pagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _pagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setImporto", double.class);

                method.invoke(_pagamentoRemoteModel, importo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getSaldo() {
        return _saldo;
    }

    @Override
    public void setSaldo(double saldo) {
        _saldo = saldo;

        if (_pagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _pagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setSaldo", double.class);

                method.invoke(_pagamentoRemoteModel, saldo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getStato() {
        return _stato;
    }

    @Override
    public void setStato(int stato) {
        _stato = stato;

        if (_pagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _pagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setStato", int.class);

                method.invoke(_pagamentoRemoteModel, stato);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNota() {
        return _nota;
    }

    @Override
    public void setNota(String nota) {
        _nota = nota;

        if (_pagamentoRemoteModel != null) {
            try {
                Class<?> clazz = _pagamentoRemoteModel.getClass();

                Method method = clazz.getMethod("setNota", String.class);

                method.invoke(_pagamentoRemoteModel, nota);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getPagamentoRemoteModel() {
        return _pagamentoRemoteModel;
    }

    public void setPagamentoRemoteModel(BaseModel<?> pagamentoRemoteModel) {
        _pagamentoRemoteModel = pagamentoRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _pagamentoRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_pagamentoRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            PagamentoLocalServiceUtil.addPagamento(this);
        } else {
            PagamentoLocalServiceUtil.updatePagamento(this);
        }
    }

    @Override
    public Pagamento toEscapedModel() {
        return (Pagamento) ProxyUtil.newProxyInstance(Pagamento.class.getClassLoader(),
            new Class[] { Pagamento.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        PagamentoClp clone = new PagamentoClp();

        clone.setId(getId());
        clone.setAnno(getAnno());
        clone.setCodiceSoggetto(getCodiceSoggetto());
        clone.setDataCreazione(getDataCreazione());
        clone.setDataPagamento(getDataPagamento());
        clone.setCredito(getCredito());
        clone.setImporto(getImporto());
        clone.setSaldo(getSaldo());
        clone.setStato(getStato());
        clone.setNota(getNota());

        return clone;
    }

    @Override
    public int compareTo(Pagamento pagamento) {
        PagamentoPK primaryKey = pagamento.getPrimaryKey();

        return getPrimaryKey().compareTo(primaryKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PagamentoClp)) {
            return false;
        }

        PagamentoClp pagamento = (PagamentoClp) obj;

        PagamentoPK primaryKey = pagamento.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(21);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", anno=");
        sb.append(getAnno());
        sb.append(", codiceSoggetto=");
        sb.append(getCodiceSoggetto());
        sb.append(", dataCreazione=");
        sb.append(getDataCreazione());
        sb.append(", dataPagamento=");
        sb.append(getDataPagamento());
        sb.append(", credito=");
        sb.append(getCredito());
        sb.append(", importo=");
        sb.append(getImporto());
        sb.append(", saldo=");
        sb.append(getSaldo());
        sb.append(", stato=");
        sb.append(getStato());
        sb.append(", nota=");
        sb.append(getNota());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(34);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.Pagamento");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>anno</column-name><column-value><![CDATA[");
        sb.append(getAnno());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceSoggetto</column-name><column-value><![CDATA[");
        sb.append(getCodiceSoggetto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>dataCreazione</column-name><column-value><![CDATA[");
        sb.append(getDataCreazione());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>dataPagamento</column-name><column-value><![CDATA[");
        sb.append(getDataPagamento());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>credito</column-name><column-value><![CDATA[");
        sb.append(getCredito());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>importo</column-name><column-value><![CDATA[");
        sb.append(getImporto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>saldo</column-name><column-value><![CDATA[");
        sb.append(getSaldo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>stato</column-name><column-value><![CDATA[");
        sb.append(getStato());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>nota</column-name><column-value><![CDATA[");
        sb.append(getNota());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
