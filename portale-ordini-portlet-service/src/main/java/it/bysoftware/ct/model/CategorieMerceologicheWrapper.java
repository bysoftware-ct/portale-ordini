package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CategorieMerceologiche}.
 * </p>
 *
 * @author Mario Torrisi
 * @see CategorieMerceologiche
 * @generated
 */
public class CategorieMerceologicheWrapper implements CategorieMerceologiche,
    ModelWrapper<CategorieMerceologiche> {
    private CategorieMerceologiche _categorieMerceologiche;

    public CategorieMerceologicheWrapper(
        CategorieMerceologiche categorieMerceologiche) {
        _categorieMerceologiche = categorieMerceologiche;
    }

    @Override
    public Class<?> getModelClass() {
        return CategorieMerceologiche.class;
    }

    @Override
    public String getModelClassName() {
        return CategorieMerceologiche.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceCategoria", getCodiceCategoria());
        attributes.put("descrizione", getDescrizione());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceCategoria = (String) attributes.get("codiceCategoria");

        if (codiceCategoria != null) {
            setCodiceCategoria(codiceCategoria);
        }

        String descrizione = (String) attributes.get("descrizione");

        if (descrizione != null) {
            setDescrizione(descrizione);
        }
    }

    /**
    * Returns the primary key of this categorie merceologiche.
    *
    * @return the primary key of this categorie merceologiche
    */
    @Override
    public java.lang.String getPrimaryKey() {
        return _categorieMerceologiche.getPrimaryKey();
    }

    /**
    * Sets the primary key of this categorie merceologiche.
    *
    * @param primaryKey the primary key of this categorie merceologiche
    */
    @Override
    public void setPrimaryKey(java.lang.String primaryKey) {
        _categorieMerceologiche.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the codice categoria of this categorie merceologiche.
    *
    * @return the codice categoria of this categorie merceologiche
    */
    @Override
    public java.lang.String getCodiceCategoria() {
        return _categorieMerceologiche.getCodiceCategoria();
    }

    /**
    * Sets the codice categoria of this categorie merceologiche.
    *
    * @param codiceCategoria the codice categoria of this categorie merceologiche
    */
    @Override
    public void setCodiceCategoria(java.lang.String codiceCategoria) {
        _categorieMerceologiche.setCodiceCategoria(codiceCategoria);
    }

    /**
    * Returns the descrizione of this categorie merceologiche.
    *
    * @return the descrizione of this categorie merceologiche
    */
    @Override
    public java.lang.String getDescrizione() {
        return _categorieMerceologiche.getDescrizione();
    }

    /**
    * Sets the descrizione of this categorie merceologiche.
    *
    * @param descrizione the descrizione of this categorie merceologiche
    */
    @Override
    public void setDescrizione(java.lang.String descrizione) {
        _categorieMerceologiche.setDescrizione(descrizione);
    }

    @Override
    public boolean isNew() {
        return _categorieMerceologiche.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _categorieMerceologiche.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _categorieMerceologiche.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _categorieMerceologiche.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _categorieMerceologiche.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _categorieMerceologiche.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _categorieMerceologiche.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _categorieMerceologiche.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _categorieMerceologiche.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _categorieMerceologiche.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _categorieMerceologiche.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new CategorieMerceologicheWrapper((CategorieMerceologiche) _categorieMerceologiche.clone());
    }

    @Override
    public int compareTo(
        it.bysoftware.ct.model.CategorieMerceologiche categorieMerceologiche) {
        return _categorieMerceologiche.compareTo(categorieMerceologiche);
    }

    @Override
    public int hashCode() {
        return _categorieMerceologiche.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.CategorieMerceologiche> toCacheModel() {
        return _categorieMerceologiche.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.CategorieMerceologiche toEscapedModel() {
        return new CategorieMerceologicheWrapper(_categorieMerceologiche.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.CategorieMerceologiche toUnescapedModel() {
        return new CategorieMerceologicheWrapper(_categorieMerceologiche.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _categorieMerceologiche.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _categorieMerceologiche.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _categorieMerceologiche.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CategorieMerceologicheWrapper)) {
            return false;
        }

        CategorieMerceologicheWrapper categorieMerceologicheWrapper = (CategorieMerceologicheWrapper) obj;

        if (Validator.equals(_categorieMerceologiche,
                    categorieMerceologicheWrapper._categorieMerceologiche)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public CategorieMerceologiche getWrappedCategorieMerceologiche() {
        return _categorieMerceologiche;
    }

    @Override
    public CategorieMerceologiche getWrappedModel() {
        return _categorieMerceologiche;
    }

    @Override
    public void resetOriginalValues() {
        _categorieMerceologiche.resetOriginalValues();
    }
}
