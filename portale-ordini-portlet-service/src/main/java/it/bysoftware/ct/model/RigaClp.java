package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.RigaLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class RigaClp extends BaseModelImpl<Riga> implements Riga {
    private long _id;
    private long _idCarrello;
    private long _idArticolo;
    private long _progressivo;
    private double _prezzo;
    private double _importo;
    private int _pianteRipiano;
    private boolean _sormontate;
    private String _stringa;
    private String _opzioni;
    private double _importoVarianti;
    private String _codiceVariante;
    private String _codiceRegionale;
    private String _passaporto;
    private int _s2;
    private int _s3;
    private int _s4;
    private double _sconto;
    private double _prezzoFornitore;
    private double _prezzoEtichetta;
    private String _ean;
    private boolean _modificato;
    private boolean _ricevuto;
    private BaseModel<?> _rigaRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public RigaClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Riga.class;
    }

    @Override
    public String getModelClassName() {
        return Riga.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("idCarrello", getIdCarrello());
        attributes.put("idArticolo", getIdArticolo());
        attributes.put("progressivo", getProgressivo());
        attributes.put("prezzo", getPrezzo());
        attributes.put("importo", getImporto());
        attributes.put("pianteRipiano", getPianteRipiano());
        attributes.put("sormontate", getSormontate());
        attributes.put("stringa", getStringa());
        attributes.put("opzioni", getOpzioni());
        attributes.put("importoVarianti", getImportoVarianti());
        attributes.put("codiceVariante", getCodiceVariante());
        attributes.put("codiceRegionale", getCodiceRegionale());
        attributes.put("passaporto", getPassaporto());
        attributes.put("s2", getS2());
        attributes.put("s3", getS3());
        attributes.put("s4", getS4());
        attributes.put("sconto", getSconto());
        attributes.put("prezzoFornitore", getPrezzoFornitore());
        attributes.put("prezzoEtichetta", getPrezzoEtichetta());
        attributes.put("ean", getEan());
        attributes.put("modificato", getModificato());
        attributes.put("ricevuto", getRicevuto());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long idCarrello = (Long) attributes.get("idCarrello");

        if (idCarrello != null) {
            setIdCarrello(idCarrello);
        }

        Long idArticolo = (Long) attributes.get("idArticolo");

        if (idArticolo != null) {
            setIdArticolo(idArticolo);
        }

        Long progressivo = (Long) attributes.get("progressivo");

        if (progressivo != null) {
            setProgressivo(progressivo);
        }

        Double prezzo = (Double) attributes.get("prezzo");

        if (prezzo != null) {
            setPrezzo(prezzo);
        }

        Double importo = (Double) attributes.get("importo");

        if (importo != null) {
            setImporto(importo);
        }

        Integer pianteRipiano = (Integer) attributes.get("pianteRipiano");

        if (pianteRipiano != null) {
            setPianteRipiano(pianteRipiano);
        }

        Boolean sormontate = (Boolean) attributes.get("sormontate");

        if (sormontate != null) {
            setSormontate(sormontate);
        }

        String stringa = (String) attributes.get("stringa");

        if (stringa != null) {
            setStringa(stringa);
        }

        String opzioni = (String) attributes.get("opzioni");

        if (opzioni != null) {
            setOpzioni(opzioni);
        }

        Double importoVarianti = (Double) attributes.get("importoVarianti");

        if (importoVarianti != null) {
            setImportoVarianti(importoVarianti);
        }

        String codiceVariante = (String) attributes.get("codiceVariante");

        if (codiceVariante != null) {
            setCodiceVariante(codiceVariante);
        }

        String codiceRegionale = (String) attributes.get("codiceRegionale");

        if (codiceRegionale != null) {
            setCodiceRegionale(codiceRegionale);
        }

        String passaporto = (String) attributes.get("passaporto");

        if (passaporto != null) {
            setPassaporto(passaporto);
        }

        Integer s2 = (Integer) attributes.get("s2");

        if (s2 != null) {
            setS2(s2);
        }

        Integer s3 = (Integer) attributes.get("s3");

        if (s3 != null) {
            setS3(s3);
        }

        Integer s4 = (Integer) attributes.get("s4");

        if (s4 != null) {
            setS4(s4);
        }

        Double sconto = (Double) attributes.get("sconto");

        if (sconto != null) {
            setSconto(sconto);
        }

        Double prezzoFornitore = (Double) attributes.get("prezzoFornitore");

        if (prezzoFornitore != null) {
            setPrezzoFornitore(prezzoFornitore);
        }

        Double prezzoEtichetta = (Double) attributes.get("prezzoEtichetta");

        if (prezzoEtichetta != null) {
            setPrezzoEtichetta(prezzoEtichetta);
        }

        String ean = (String) attributes.get("ean");

        if (ean != null) {
            setEan(ean);
        }

        Boolean modificato = (Boolean) attributes.get("modificato");

        if (modificato != null) {
            setModificato(modificato);
        }

        Boolean ricevuto = (Boolean) attributes.get("ricevuto");

        if (ricevuto != null) {
            setRicevuto(ricevuto);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_rigaRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getIdCarrello() {
        return _idCarrello;
    }

    @Override
    public void setIdCarrello(long idCarrello) {
        _idCarrello = idCarrello;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setIdCarrello", long.class);

                method.invoke(_rigaRemoteModel, idCarrello);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getIdArticolo() {
        return _idArticolo;
    }

    @Override
    public void setIdArticolo(long idArticolo) {
        _idArticolo = idArticolo;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setIdArticolo", long.class);

                method.invoke(_rigaRemoteModel, idArticolo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getProgressivo() {
        return _progressivo;
    }

    @Override
    public void setProgressivo(long progressivo) {
        _progressivo = progressivo;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setProgressivo", long.class);

                method.invoke(_rigaRemoteModel, progressivo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrezzo() {
        return _prezzo;
    }

    @Override
    public void setPrezzo(double prezzo) {
        _prezzo = prezzo;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setPrezzo", double.class);

                method.invoke(_rigaRemoteModel, prezzo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getImporto() {
        return _importo;
    }

    @Override
    public void setImporto(double importo) {
        _importo = importo;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setImporto", double.class);

                method.invoke(_rigaRemoteModel, importo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getPianteRipiano() {
        return _pianteRipiano;
    }

    @Override
    public void setPianteRipiano(int pianteRipiano) {
        _pianteRipiano = pianteRipiano;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setPianteRipiano", int.class);

                method.invoke(_rigaRemoteModel, pianteRipiano);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getSormontate() {
        return _sormontate;
    }

    @Override
    public boolean isSormontate() {
        return _sormontate;
    }

    @Override
    public void setSormontate(boolean sormontate) {
        _sormontate = sormontate;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setSormontate", boolean.class);

                method.invoke(_rigaRemoteModel, sormontate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getStringa() {
        return _stringa;
    }

    @Override
    public void setStringa(String stringa) {
        _stringa = stringa;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setStringa", String.class);

                method.invoke(_rigaRemoteModel, stringa);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getOpzioni() {
        return _opzioni;
    }

    @Override
    public void setOpzioni(String opzioni) {
        _opzioni = opzioni;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setOpzioni", String.class);

                method.invoke(_rigaRemoteModel, opzioni);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getImportoVarianti() {
        return _importoVarianti;
    }

    @Override
    public void setImportoVarianti(double importoVarianti) {
        _importoVarianti = importoVarianti;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setImportoVarianti",
                        double.class);

                method.invoke(_rigaRemoteModel, importoVarianti);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceVariante() {
        return _codiceVariante;
    }

    @Override
    public void setCodiceVariante(String codiceVariante) {
        _codiceVariante = codiceVariante;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceVariante",
                        String.class);

                method.invoke(_rigaRemoteModel, codiceVariante);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceRegionale() {
        return _codiceRegionale;
    }

    @Override
    public void setCodiceRegionale(String codiceRegionale) {
        _codiceRegionale = codiceRegionale;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceRegionale",
                        String.class);

                method.invoke(_rigaRemoteModel, codiceRegionale);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getPassaporto() {
        return _passaporto;
    }

    @Override
    public void setPassaporto(String passaporto) {
        _passaporto = passaporto;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setPassaporto", String.class);

                method.invoke(_rigaRemoteModel, passaporto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getS2() {
        return _s2;
    }

    @Override
    public void setS2(int s2) {
        _s2 = s2;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setS2", int.class);

                method.invoke(_rigaRemoteModel, s2);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getS3() {
        return _s3;
    }

    @Override
    public void setS3(int s3) {
        _s3 = s3;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setS3", int.class);

                method.invoke(_rigaRemoteModel, s3);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getS4() {
        return _s4;
    }

    @Override
    public void setS4(int s4) {
        _s4 = s4;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setS4", int.class);

                method.invoke(_rigaRemoteModel, s4);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getSconto() {
        return _sconto;
    }

    @Override
    public void setSconto(double sconto) {
        _sconto = sconto;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setSconto", double.class);

                method.invoke(_rigaRemoteModel, sconto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrezzoFornitore() {
        return _prezzoFornitore;
    }

    @Override
    public void setPrezzoFornitore(double prezzoFornitore) {
        _prezzoFornitore = prezzoFornitore;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setPrezzoFornitore",
                        double.class);

                method.invoke(_rigaRemoteModel, prezzoFornitore);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrezzoEtichetta() {
        return _prezzoEtichetta;
    }

    @Override
    public void setPrezzoEtichetta(double prezzoEtichetta) {
        _prezzoEtichetta = prezzoEtichetta;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setPrezzoEtichetta",
                        double.class);

                method.invoke(_rigaRemoteModel, prezzoEtichetta);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getEan() {
        return _ean;
    }

    @Override
    public void setEan(String ean) {
        _ean = ean;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setEan", String.class);

                method.invoke(_rigaRemoteModel, ean);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getModificato() {
        return _modificato;
    }

    @Override
    public boolean isModificato() {
        return _modificato;
    }

    @Override
    public void setModificato(boolean modificato) {
        _modificato = modificato;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setModificato", boolean.class);

                method.invoke(_rigaRemoteModel, modificato);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getRicevuto() {
        return _ricevuto;
    }

    @Override
    public boolean isRicevuto() {
        return _ricevuto;
    }

    @Override
    public void setRicevuto(boolean ricevuto) {
        _ricevuto = ricevuto;

        if (_rigaRemoteModel != null) {
            try {
                Class<?> clazz = _rigaRemoteModel.getClass();

                Method method = clazz.getMethod("setRicevuto", boolean.class);

                method.invoke(_rigaRemoteModel, ricevuto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getRigaRemoteModel() {
        return _rigaRemoteModel;
    }

    public void setRigaRemoteModel(BaseModel<?> rigaRemoteModel) {
        _rigaRemoteModel = rigaRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _rigaRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_rigaRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            RigaLocalServiceUtil.addRiga(this);
        } else {
            RigaLocalServiceUtil.updateRiga(this);
        }
    }

    @Override
    public Riga toEscapedModel() {
        return (Riga) ProxyUtil.newProxyInstance(Riga.class.getClassLoader(),
            new Class[] { Riga.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        RigaClp clone = new RigaClp();

        clone.setId(getId());
        clone.setIdCarrello(getIdCarrello());
        clone.setIdArticolo(getIdArticolo());
        clone.setProgressivo(getProgressivo());
        clone.setPrezzo(getPrezzo());
        clone.setImporto(getImporto());
        clone.setPianteRipiano(getPianteRipiano());
        clone.setSormontate(getSormontate());
        clone.setStringa(getStringa());
        clone.setOpzioni(getOpzioni());
        clone.setImportoVarianti(getImportoVarianti());
        clone.setCodiceVariante(getCodiceVariante());
        clone.setCodiceRegionale(getCodiceRegionale());
        clone.setPassaporto(getPassaporto());
        clone.setS2(getS2());
        clone.setS3(getS3());
        clone.setS4(getS4());
        clone.setSconto(getSconto());
        clone.setPrezzoFornitore(getPrezzoFornitore());
        clone.setPrezzoEtichetta(getPrezzoEtichetta());
        clone.setEan(getEan());
        clone.setModificato(getModificato());
        clone.setRicevuto(getRicevuto());

        return clone;
    }

    @Override
    public int compareTo(Riga riga) {
        long primaryKey = riga.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof RigaClp)) {
            return false;
        }

        RigaClp riga = (RigaClp) obj;

        long primaryKey = riga.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(47);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", idCarrello=");
        sb.append(getIdCarrello());
        sb.append(", idArticolo=");
        sb.append(getIdArticolo());
        sb.append(", progressivo=");
        sb.append(getProgressivo());
        sb.append(", prezzo=");
        sb.append(getPrezzo());
        sb.append(", importo=");
        sb.append(getImporto());
        sb.append(", pianteRipiano=");
        sb.append(getPianteRipiano());
        sb.append(", sormontate=");
        sb.append(getSormontate());
        sb.append(", stringa=");
        sb.append(getStringa());
        sb.append(", opzioni=");
        sb.append(getOpzioni());
        sb.append(", importoVarianti=");
        sb.append(getImportoVarianti());
        sb.append(", codiceVariante=");
        sb.append(getCodiceVariante());
        sb.append(", codiceRegionale=");
        sb.append(getCodiceRegionale());
        sb.append(", passaporto=");
        sb.append(getPassaporto());
        sb.append(", s2=");
        sb.append(getS2());
        sb.append(", s3=");
        sb.append(getS3());
        sb.append(", s4=");
        sb.append(getS4());
        sb.append(", sconto=");
        sb.append(getSconto());
        sb.append(", prezzoFornitore=");
        sb.append(getPrezzoFornitore());
        sb.append(", prezzoEtichetta=");
        sb.append(getPrezzoEtichetta());
        sb.append(", ean=");
        sb.append(getEan());
        sb.append(", modificato=");
        sb.append(getModificato());
        sb.append(", ricevuto=");
        sb.append(getRicevuto());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(73);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.Riga");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>idCarrello</column-name><column-value><![CDATA[");
        sb.append(getIdCarrello());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>idArticolo</column-name><column-value><![CDATA[");
        sb.append(getIdArticolo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>progressivo</column-name><column-value><![CDATA[");
        sb.append(getProgressivo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>prezzo</column-name><column-value><![CDATA[");
        sb.append(getPrezzo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>importo</column-name><column-value><![CDATA[");
        sb.append(getImporto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>pianteRipiano</column-name><column-value><![CDATA[");
        sb.append(getPianteRipiano());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>sormontate</column-name><column-value><![CDATA[");
        sb.append(getSormontate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>stringa</column-name><column-value><![CDATA[");
        sb.append(getStringa());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>opzioni</column-name><column-value><![CDATA[");
        sb.append(getOpzioni());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>importoVarianti</column-name><column-value><![CDATA[");
        sb.append(getImportoVarianti());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceVariante</column-name><column-value><![CDATA[");
        sb.append(getCodiceVariante());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceRegionale</column-name><column-value><![CDATA[");
        sb.append(getCodiceRegionale());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>passaporto</column-name><column-value><![CDATA[");
        sb.append(getPassaporto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>s2</column-name><column-value><![CDATA[");
        sb.append(getS2());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>s3</column-name><column-value><![CDATA[");
        sb.append(getS3());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>s4</column-name><column-value><![CDATA[");
        sb.append(getS4());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>sconto</column-name><column-value><![CDATA[");
        sb.append(getSconto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>prezzoFornitore</column-name><column-value><![CDATA[");
        sb.append(getPrezzoFornitore());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>prezzoEtichetta</column-name><column-value><![CDATA[");
        sb.append(getPrezzoEtichetta());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>ean</column-name><column-value><![CDATA[");
        sb.append(getEan());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modificato</column-name><column-value><![CDATA[");
        sb.append(getModificato());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>ricevuto</column-name><column-value><![CDATA[");
        sb.append(getRicevuto());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
