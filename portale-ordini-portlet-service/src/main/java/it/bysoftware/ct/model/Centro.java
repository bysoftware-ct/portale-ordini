package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Centro service. Represents a row in the &quot;AttivitaCentri&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see CentroModel
 * @see it.bysoftware.ct.model.impl.CentroImpl
 * @see it.bysoftware.ct.model.impl.CentroModelImpl
 * @generated
 */
public interface Centro extends CentroModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.CentroImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
