package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the DettagliOperazioniContabili service. Represents a row in the &quot;DettagliOperazioniContabili&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see DettagliOperazioniContabiliModel
 * @see it.bysoftware.ct.model.impl.DettagliOperazioniContabiliImpl
 * @see it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl
 * @generated
 */
public interface DettagliOperazioniContabili
    extends DettagliOperazioniContabiliModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
