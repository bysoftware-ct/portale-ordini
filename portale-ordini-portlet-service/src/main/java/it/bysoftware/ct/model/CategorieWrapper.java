package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Categorie}.
 * </p>
 *
 * @author Mario Torrisi
 * @see Categorie
 * @generated
 */
public class CategorieWrapper implements Categorie, ModelWrapper<Categorie> {
    private Categorie _categorie;

    public CategorieWrapper(Categorie categorie) {
        _categorie = categorie;
    }

    @Override
    public Class<?> getModelClass() {
        return Categorie.class;
    }

    @Override
    public String getModelClassName() {
        return Categorie.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("nomeIta", getNomeIta());
        attributes.put("nomeEng", getNomeEng());
        attributes.put("nomeTed", getNomeTed());
        attributes.put("nomeFra", getNomeFra());
        attributes.put("nomeSpa", getNomeSpa());
        attributes.put("predefinita", getPredefinita());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        String nomeIta = (String) attributes.get("nomeIta");

        if (nomeIta != null) {
            setNomeIta(nomeIta);
        }

        String nomeEng = (String) attributes.get("nomeEng");

        if (nomeEng != null) {
            setNomeEng(nomeEng);
        }

        String nomeTed = (String) attributes.get("nomeTed");

        if (nomeTed != null) {
            setNomeTed(nomeTed);
        }

        String nomeFra = (String) attributes.get("nomeFra");

        if (nomeFra != null) {
            setNomeFra(nomeFra);
        }

        String nomeSpa = (String) attributes.get("nomeSpa");

        if (nomeSpa != null) {
            setNomeSpa(nomeSpa);
        }

        Boolean predefinita = (Boolean) attributes.get("predefinita");

        if (predefinita != null) {
            setPredefinita(predefinita);
        }
    }

    /**
    * Returns the primary key of this categorie.
    *
    * @return the primary key of this categorie
    */
    @Override
    public long getPrimaryKey() {
        return _categorie.getPrimaryKey();
    }

    /**
    * Sets the primary key of this categorie.
    *
    * @param primaryKey the primary key of this categorie
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _categorie.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this categorie.
    *
    * @return the ID of this categorie
    */
    @Override
    public long getId() {
        return _categorie.getId();
    }

    /**
    * Sets the ID of this categorie.
    *
    * @param id the ID of this categorie
    */
    @Override
    public void setId(long id) {
        _categorie.setId(id);
    }

    /**
    * Returns the nome ita of this categorie.
    *
    * @return the nome ita of this categorie
    */
    @Override
    public java.lang.String getNomeIta() {
        return _categorie.getNomeIta();
    }

    /**
    * Sets the nome ita of this categorie.
    *
    * @param nomeIta the nome ita of this categorie
    */
    @Override
    public void setNomeIta(java.lang.String nomeIta) {
        _categorie.setNomeIta(nomeIta);
    }

    /**
    * Returns the nome eng of this categorie.
    *
    * @return the nome eng of this categorie
    */
    @Override
    public java.lang.String getNomeEng() {
        return _categorie.getNomeEng();
    }

    /**
    * Sets the nome eng of this categorie.
    *
    * @param nomeEng the nome eng of this categorie
    */
    @Override
    public void setNomeEng(java.lang.String nomeEng) {
        _categorie.setNomeEng(nomeEng);
    }

    /**
    * Returns the nome ted of this categorie.
    *
    * @return the nome ted of this categorie
    */
    @Override
    public java.lang.String getNomeTed() {
        return _categorie.getNomeTed();
    }

    /**
    * Sets the nome ted of this categorie.
    *
    * @param nomeTed the nome ted of this categorie
    */
    @Override
    public void setNomeTed(java.lang.String nomeTed) {
        _categorie.setNomeTed(nomeTed);
    }

    /**
    * Returns the nome fra of this categorie.
    *
    * @return the nome fra of this categorie
    */
    @Override
    public java.lang.String getNomeFra() {
        return _categorie.getNomeFra();
    }

    /**
    * Sets the nome fra of this categorie.
    *
    * @param nomeFra the nome fra of this categorie
    */
    @Override
    public void setNomeFra(java.lang.String nomeFra) {
        _categorie.setNomeFra(nomeFra);
    }

    /**
    * Returns the nome spa of this categorie.
    *
    * @return the nome spa of this categorie
    */
    @Override
    public java.lang.String getNomeSpa() {
        return _categorie.getNomeSpa();
    }

    /**
    * Sets the nome spa of this categorie.
    *
    * @param nomeSpa the nome spa of this categorie
    */
    @Override
    public void setNomeSpa(java.lang.String nomeSpa) {
        _categorie.setNomeSpa(nomeSpa);
    }

    /**
    * Returns the predefinita of this categorie.
    *
    * @return the predefinita of this categorie
    */
    @Override
    public boolean getPredefinita() {
        return _categorie.getPredefinita();
    }

    /**
    * Returns <code>true</code> if this categorie is predefinita.
    *
    * @return <code>true</code> if this categorie is predefinita; <code>false</code> otherwise
    */
    @Override
    public boolean isPredefinita() {
        return _categorie.isPredefinita();
    }

    /**
    * Sets whether this categorie is predefinita.
    *
    * @param predefinita the predefinita of this categorie
    */
    @Override
    public void setPredefinita(boolean predefinita) {
        _categorie.setPredefinita(predefinita);
    }

    @Override
    public boolean isNew() {
        return _categorie.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _categorie.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _categorie.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _categorie.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _categorie.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _categorie.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _categorie.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _categorie.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _categorie.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _categorie.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _categorie.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new CategorieWrapper((Categorie) _categorie.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.Categorie categorie) {
        return _categorie.compareTo(categorie);
    }

    @Override
    public int hashCode() {
        return _categorie.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.Categorie> toCacheModel() {
        return _categorie.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.Categorie toEscapedModel() {
        return new CategorieWrapper(_categorie.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.Categorie toUnescapedModel() {
        return new CategorieWrapper(_categorie.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _categorie.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _categorie.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _categorie.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CategorieWrapper)) {
            return false;
        }

        CategorieWrapper categorieWrapper = (CategorieWrapper) obj;

        if (Validator.equals(_categorie, categorieWrapper._categorie)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Categorie getWrappedCategorie() {
        return _categorie;
    }

    @Override
    public Categorie getWrappedModel() {
        return _categorie;
    }

    @Override
    public void resetOriginalValues() {
        _categorie.resetOriginalValues();
    }
}
