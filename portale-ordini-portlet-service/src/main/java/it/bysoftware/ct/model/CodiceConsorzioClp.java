package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.CodiceConsorzioLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class CodiceConsorzioClp extends BaseModelImpl<CodiceConsorzio>
    implements CodiceConsorzio {
    private String _codiceSocio;
    private long _idLiferay;
    private String _codiceConsorzio;
    private BaseModel<?> _codiceConsorzioRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public CodiceConsorzioClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return CodiceConsorzio.class;
    }

    @Override
    public String getModelClassName() {
        return CodiceConsorzio.class.getName();
    }

    @Override
    public String getPrimaryKey() {
        return _codiceSocio;
    }

    @Override
    public void setPrimaryKey(String primaryKey) {
        setCodiceSocio(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _codiceSocio;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((String) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceSocio", getCodiceSocio());
        attributes.put("idLiferay", getIdLiferay());
        attributes.put("codiceConsorzio", getCodiceConsorzio());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceSocio = (String) attributes.get("codiceSocio");

        if (codiceSocio != null) {
            setCodiceSocio(codiceSocio);
        }

        Long idLiferay = (Long) attributes.get("idLiferay");

        if (idLiferay != null) {
            setIdLiferay(idLiferay);
        }

        String codiceConsorzio = (String) attributes.get("codiceConsorzio");

        if (codiceConsorzio != null) {
            setCodiceConsorzio(codiceConsorzio);
        }
    }

    @Override
    public String getCodiceSocio() {
        return _codiceSocio;
    }

    @Override
    public void setCodiceSocio(String codiceSocio) {
        _codiceSocio = codiceSocio;

        if (_codiceConsorzioRemoteModel != null) {
            try {
                Class<?> clazz = _codiceConsorzioRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceSocio", String.class);

                method.invoke(_codiceConsorzioRemoteModel, codiceSocio);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getIdLiferay() {
        return _idLiferay;
    }

    @Override
    public void setIdLiferay(long idLiferay) {
        _idLiferay = idLiferay;

        if (_codiceConsorzioRemoteModel != null) {
            try {
                Class<?> clazz = _codiceConsorzioRemoteModel.getClass();

                Method method = clazz.getMethod("setIdLiferay", long.class);

                method.invoke(_codiceConsorzioRemoteModel, idLiferay);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceConsorzio() {
        return _codiceConsorzio;
    }

    @Override
    public void setCodiceConsorzio(String codiceConsorzio) {
        _codiceConsorzio = codiceConsorzio;

        if (_codiceConsorzioRemoteModel != null) {
            try {
                Class<?> clazz = _codiceConsorzioRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceConsorzio",
                        String.class);

                method.invoke(_codiceConsorzioRemoteModel, codiceConsorzio);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getCodiceConsorzioRemoteModel() {
        return _codiceConsorzioRemoteModel;
    }

    public void setCodiceConsorzioRemoteModel(
        BaseModel<?> codiceConsorzioRemoteModel) {
        _codiceConsorzioRemoteModel = codiceConsorzioRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _codiceConsorzioRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_codiceConsorzioRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            CodiceConsorzioLocalServiceUtil.addCodiceConsorzio(this);
        } else {
            CodiceConsorzioLocalServiceUtil.updateCodiceConsorzio(this);
        }
    }

    @Override
    public CodiceConsorzio toEscapedModel() {
        return (CodiceConsorzio) ProxyUtil.newProxyInstance(CodiceConsorzio.class.getClassLoader(),
            new Class[] { CodiceConsorzio.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        CodiceConsorzioClp clone = new CodiceConsorzioClp();

        clone.setCodiceSocio(getCodiceSocio());
        clone.setIdLiferay(getIdLiferay());
        clone.setCodiceConsorzio(getCodiceConsorzio());

        return clone;
    }

    @Override
    public int compareTo(CodiceConsorzio codiceConsorzio) {
        String primaryKey = codiceConsorzio.getPrimaryKey();

        return getPrimaryKey().compareTo(primaryKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CodiceConsorzioClp)) {
            return false;
        }

        CodiceConsorzioClp codiceConsorzio = (CodiceConsorzioClp) obj;

        String primaryKey = codiceConsorzio.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{codiceSocio=");
        sb.append(getCodiceSocio());
        sb.append(", idLiferay=");
        sb.append(getIdLiferay());
        sb.append(", codiceConsorzio=");
        sb.append(getCodiceConsorzio());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(13);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.CodiceConsorzio");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>codiceSocio</column-name><column-value><![CDATA[");
        sb.append(getCodiceSocio());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>idLiferay</column-name><column-value><![CDATA[");
        sb.append(getIdLiferay());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceConsorzio</column-name><column-value><![CDATA[");
        sb.append(getCodiceConsorzio());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
