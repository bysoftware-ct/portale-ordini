package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.CentroPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.CentroServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.CentroServiceSoap
 * @generated
 */
public class CentroSoap implements Serializable {
    private String _attivita;
    private String _centro;

    public CentroSoap() {
    }

    public static CentroSoap toSoapModel(Centro model) {
        CentroSoap soapModel = new CentroSoap();

        soapModel.setAttivita(model.getAttivita());
        soapModel.setCentro(model.getCentro());

        return soapModel;
    }

    public static CentroSoap[] toSoapModels(Centro[] models) {
        CentroSoap[] soapModels = new CentroSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static CentroSoap[][] toSoapModels(Centro[][] models) {
        CentroSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new CentroSoap[models.length][models[0].length];
        } else {
            soapModels = new CentroSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static CentroSoap[] toSoapModels(List<Centro> models) {
        List<CentroSoap> soapModels = new ArrayList<CentroSoap>(models.size());

        for (Centro model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new CentroSoap[soapModels.size()]);
    }

    public CentroPK getPrimaryKey() {
        return new CentroPK(_attivita, _centro);
    }

    public void setPrimaryKey(CentroPK pk) {
        setAttivita(pk.attivita);
        setCentro(pk.centro);
    }

    public String getAttivita() {
        return _attivita;
    }

    public void setAttivita(String attivita) {
        _attivita = attivita;
    }

    public String getCentro() {
        return _centro;
    }

    public void setCentro(String centro) {
        _centro = centro;
    }
}
