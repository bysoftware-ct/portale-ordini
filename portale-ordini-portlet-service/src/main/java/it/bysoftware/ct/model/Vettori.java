package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Vettori service. Represents a row in the &quot;Vettori&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see VettoriModel
 * @see it.bysoftware.ct.model.impl.VettoriImpl
 * @see it.bysoftware.ct.model.impl.VettoriModelImpl
 * @generated
 */
public interface Vettori extends VettoriModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.VettoriImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
