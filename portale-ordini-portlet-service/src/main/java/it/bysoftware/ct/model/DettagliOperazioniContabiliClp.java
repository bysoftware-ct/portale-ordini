package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.DettagliOperazioniContabiliLocalServiceUtil;
import it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class DettagliOperazioniContabiliClp extends BaseModelImpl<DettagliOperazioniContabili>
    implements DettagliOperazioniContabili {
    private String _codiceOperazione;
    private int _progressivoLegame;
    private int _tipoLegame;
    private String _causaleContabile;
    private String _sottoconto;
    private String _naturaConto;
    private int _tipoImporto;
    private BaseModel<?> _dettagliOperazioniContabiliRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public DettagliOperazioniContabiliClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return DettagliOperazioniContabili.class;
    }

    @Override
    public String getModelClassName() {
        return DettagliOperazioniContabili.class.getName();
    }

    @Override
    public DettagliOperazioniContabiliPK getPrimaryKey() {
        return new DettagliOperazioniContabiliPK(_codiceOperazione,
            _progressivoLegame);
    }

    @Override
    public void setPrimaryKey(DettagliOperazioniContabiliPK primaryKey) {
        setCodiceOperazione(primaryKey.codiceOperazione);
        setProgressivoLegame(primaryKey.progressivoLegame);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return new DettagliOperazioniContabiliPK(_codiceOperazione,
            _progressivoLegame);
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((DettagliOperazioniContabiliPK) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceOperazione", getCodiceOperazione());
        attributes.put("progressivoLegame", getProgressivoLegame());
        attributes.put("tipoLegame", getTipoLegame());
        attributes.put("causaleContabile", getCausaleContabile());
        attributes.put("sottoconto", getSottoconto());
        attributes.put("naturaConto", getNaturaConto());
        attributes.put("tipoImporto", getTipoImporto());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceOperazione = (String) attributes.get("codiceOperazione");

        if (codiceOperazione != null) {
            setCodiceOperazione(codiceOperazione);
        }

        Integer progressivoLegame = (Integer) attributes.get(
                "progressivoLegame");

        if (progressivoLegame != null) {
            setProgressivoLegame(progressivoLegame);
        }

        Integer tipoLegame = (Integer) attributes.get("tipoLegame");

        if (tipoLegame != null) {
            setTipoLegame(tipoLegame);
        }

        String causaleContabile = (String) attributes.get("causaleContabile");

        if (causaleContabile != null) {
            setCausaleContabile(causaleContabile);
        }

        String sottoconto = (String) attributes.get("sottoconto");

        if (sottoconto != null) {
            setSottoconto(sottoconto);
        }

        String naturaConto = (String) attributes.get("naturaConto");

        if (naturaConto != null) {
            setNaturaConto(naturaConto);
        }

        Integer tipoImporto = (Integer) attributes.get("tipoImporto");

        if (tipoImporto != null) {
            setTipoImporto(tipoImporto);
        }
    }

    @Override
    public String getCodiceOperazione() {
        return _codiceOperazione;
    }

    @Override
    public void setCodiceOperazione(String codiceOperazione) {
        _codiceOperazione = codiceOperazione;

        if (_dettagliOperazioniContabiliRemoteModel != null) {
            try {
                Class<?> clazz = _dettagliOperazioniContabiliRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceOperazione",
                        String.class);

                method.invoke(_dettagliOperazioniContabiliRemoteModel,
                    codiceOperazione);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getProgressivoLegame() {
        return _progressivoLegame;
    }

    @Override
    public void setProgressivoLegame(int progressivoLegame) {
        _progressivoLegame = progressivoLegame;

        if (_dettagliOperazioniContabiliRemoteModel != null) {
            try {
                Class<?> clazz = _dettagliOperazioniContabiliRemoteModel.getClass();

                Method method = clazz.getMethod("setProgressivoLegame",
                        int.class);

                method.invoke(_dettagliOperazioniContabiliRemoteModel,
                    progressivoLegame);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getTipoLegame() {
        return _tipoLegame;
    }

    @Override
    public void setTipoLegame(int tipoLegame) {
        _tipoLegame = tipoLegame;

        if (_dettagliOperazioniContabiliRemoteModel != null) {
            try {
                Class<?> clazz = _dettagliOperazioniContabiliRemoteModel.getClass();

                Method method = clazz.getMethod("setTipoLegame", int.class);

                method.invoke(_dettagliOperazioniContabiliRemoteModel,
                    tipoLegame);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCausaleContabile() {
        return _causaleContabile;
    }

    @Override
    public void setCausaleContabile(String causaleContabile) {
        _causaleContabile = causaleContabile;

        if (_dettagliOperazioniContabiliRemoteModel != null) {
            try {
                Class<?> clazz = _dettagliOperazioniContabiliRemoteModel.getClass();

                Method method = clazz.getMethod("setCausaleContabile",
                        String.class);

                method.invoke(_dettagliOperazioniContabiliRemoteModel,
                    causaleContabile);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getSottoconto() {
        return _sottoconto;
    }

    @Override
    public void setSottoconto(String sottoconto) {
        _sottoconto = sottoconto;

        if (_dettagliOperazioniContabiliRemoteModel != null) {
            try {
                Class<?> clazz = _dettagliOperazioniContabiliRemoteModel.getClass();

                Method method = clazz.getMethod("setSottoconto", String.class);

                method.invoke(_dettagliOperazioniContabiliRemoteModel,
                    sottoconto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNaturaConto() {
        return _naturaConto;
    }

    @Override
    public void setNaturaConto(String naturaConto) {
        _naturaConto = naturaConto;

        if (_dettagliOperazioniContabiliRemoteModel != null) {
            try {
                Class<?> clazz = _dettagliOperazioniContabiliRemoteModel.getClass();

                Method method = clazz.getMethod("setNaturaConto", String.class);

                method.invoke(_dettagliOperazioniContabiliRemoteModel,
                    naturaConto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getTipoImporto() {
        return _tipoImporto;
    }

    @Override
    public void setTipoImporto(int tipoImporto) {
        _tipoImporto = tipoImporto;

        if (_dettagliOperazioniContabiliRemoteModel != null) {
            try {
                Class<?> clazz = _dettagliOperazioniContabiliRemoteModel.getClass();

                Method method = clazz.getMethod("setTipoImporto", int.class);

                method.invoke(_dettagliOperazioniContabiliRemoteModel,
                    tipoImporto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getDettagliOperazioniContabiliRemoteModel() {
        return _dettagliOperazioniContabiliRemoteModel;
    }

    public void setDettagliOperazioniContabiliRemoteModel(
        BaseModel<?> dettagliOperazioniContabiliRemoteModel) {
        _dettagliOperazioniContabiliRemoteModel = dettagliOperazioniContabiliRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _dettagliOperazioniContabiliRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_dettagliOperazioniContabiliRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            DettagliOperazioniContabiliLocalServiceUtil.addDettagliOperazioniContabili(this);
        } else {
            DettagliOperazioniContabiliLocalServiceUtil.updateDettagliOperazioniContabili(this);
        }
    }

    @Override
    public DettagliOperazioniContabili toEscapedModel() {
        return (DettagliOperazioniContabili) ProxyUtil.newProxyInstance(DettagliOperazioniContabili.class.getClassLoader(),
            new Class[] { DettagliOperazioniContabili.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        DettagliOperazioniContabiliClp clone = new DettagliOperazioniContabiliClp();

        clone.setCodiceOperazione(getCodiceOperazione());
        clone.setProgressivoLegame(getProgressivoLegame());
        clone.setTipoLegame(getTipoLegame());
        clone.setCausaleContabile(getCausaleContabile());
        clone.setSottoconto(getSottoconto());
        clone.setNaturaConto(getNaturaConto());
        clone.setTipoImporto(getTipoImporto());

        return clone;
    }

    @Override
    public int compareTo(
        DettagliOperazioniContabili dettagliOperazioniContabili) {
        DettagliOperazioniContabiliPK primaryKey = dettagliOperazioniContabili.getPrimaryKey();

        return getPrimaryKey().compareTo(primaryKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof DettagliOperazioniContabiliClp)) {
            return false;
        }

        DettagliOperazioniContabiliClp dettagliOperazioniContabili = (DettagliOperazioniContabiliClp) obj;

        DettagliOperazioniContabiliPK primaryKey = dettagliOperazioniContabili.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(15);

        sb.append("{codiceOperazione=");
        sb.append(getCodiceOperazione());
        sb.append(", progressivoLegame=");
        sb.append(getProgressivoLegame());
        sb.append(", tipoLegame=");
        sb.append(getTipoLegame());
        sb.append(", causaleContabile=");
        sb.append(getCausaleContabile());
        sb.append(", sottoconto=");
        sb.append(getSottoconto());
        sb.append(", naturaConto=");
        sb.append(getNaturaConto());
        sb.append(", tipoImporto=");
        sb.append(getTipoImporto());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(25);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.DettagliOperazioniContabili");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>codiceOperazione</column-name><column-value><![CDATA[");
        sb.append(getCodiceOperazione());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>progressivoLegame</column-name><column-value><![CDATA[");
        sb.append(getProgressivoLegame());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tipoLegame</column-name><column-value><![CDATA[");
        sb.append(getTipoLegame());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>causaleContabile</column-name><column-value><![CDATA[");
        sb.append(getCausaleContabile());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>sottoconto</column-name><column-value><![CDATA[");
        sb.append(getSottoconto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>naturaConto</column-name><column-value><![CDATA[");
        sb.append(getNaturaConto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tipoImporto</column-name><column-value><![CDATA[");
        sb.append(getTipoImporto());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
