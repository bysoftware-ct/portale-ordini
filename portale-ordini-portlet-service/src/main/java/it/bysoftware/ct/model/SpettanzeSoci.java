package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the SpettanzeSoci service. Represents a row in the &quot;_spettanze_soci&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see SpettanzeSociModel
 * @see it.bysoftware.ct.model.impl.SpettanzeSociImpl
 * @see it.bysoftware.ct.model.impl.SpettanzeSociModelImpl
 * @generated
 */
public interface SpettanzeSoci extends SpettanzeSociModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.SpettanzeSociImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
