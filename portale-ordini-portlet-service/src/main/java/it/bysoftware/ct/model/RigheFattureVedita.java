package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the RigheFattureVedita service. Represents a row in the &quot;FattureClientiRighe&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see RigheFattureVeditaModel
 * @see it.bysoftware.ct.model.impl.RigheFattureVeditaImpl
 * @see it.bysoftware.ct.model.impl.RigheFattureVeditaModelImpl
 * @generated
 */
public interface RigheFattureVedita extends RigheFattureVeditaModel,
    PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.RigheFattureVeditaImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
