package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ArticoloSocioLocalServiceUtil;
import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.persistence.ArticoloSocioPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class ArticoloSocioClp extends BaseModelImpl<ArticoloSocio>
    implements ArticoloSocio {
    private String _associato;
    private String _codiceArticoloSocio;
    private String _codiceVarianteSocio;
    private String _codiceArticolo;
    private String _codiceVariante;
    private BaseModel<?> _articoloSocioRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public ArticoloSocioClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return ArticoloSocio.class;
    }

    @Override
    public String getModelClassName() {
        return ArticoloSocio.class.getName();
    }

    @Override
    public ArticoloSocioPK getPrimaryKey() {
        return new ArticoloSocioPK(_associato, _codiceArticoloSocio,
            _codiceVarianteSocio);
    }

    @Override
    public void setPrimaryKey(ArticoloSocioPK primaryKey) {
        setAssociato(primaryKey.associato);
        setCodiceArticoloSocio(primaryKey.codiceArticoloSocio);
        setCodiceVarianteSocio(primaryKey.codiceVarianteSocio);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return new ArticoloSocioPK(_associato, _codiceArticoloSocio,
            _codiceVarianteSocio);
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((ArticoloSocioPK) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("associato", getAssociato());
        attributes.put("codiceArticoloSocio", getCodiceArticoloSocio());
        attributes.put("codiceVarianteSocio", getCodiceVarianteSocio());
        attributes.put("codiceArticolo", getCodiceArticolo());
        attributes.put("codiceVariante", getCodiceVariante());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String associato = (String) attributes.get("associato");

        if (associato != null) {
            setAssociato(associato);
        }

        String codiceArticoloSocio = (String) attributes.get(
                "codiceArticoloSocio");

        if (codiceArticoloSocio != null) {
            setCodiceArticoloSocio(codiceArticoloSocio);
        }

        String codiceVarianteSocio = (String) attributes.get(
                "codiceVarianteSocio");

        if (codiceVarianteSocio != null) {
            setCodiceVarianteSocio(codiceVarianteSocio);
        }

        String codiceArticolo = (String) attributes.get("codiceArticolo");

        if (codiceArticolo != null) {
            setCodiceArticolo(codiceArticolo);
        }

        String codiceVariante = (String) attributes.get("codiceVariante");

        if (codiceVariante != null) {
            setCodiceVariante(codiceVariante);
        }
    }

    @Override
    public String getAssociato() {
        return _associato;
    }

    @Override
    public void setAssociato(String associato) {
        _associato = associato;

        if (_articoloSocioRemoteModel != null) {
            try {
                Class<?> clazz = _articoloSocioRemoteModel.getClass();

                Method method = clazz.getMethod("setAssociato", String.class);

                method.invoke(_articoloSocioRemoteModel, associato);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceArticoloSocio() {
        return _codiceArticoloSocio;
    }

    @Override
    public void setCodiceArticoloSocio(String codiceArticoloSocio) {
        _codiceArticoloSocio = codiceArticoloSocio;

        if (_articoloSocioRemoteModel != null) {
            try {
                Class<?> clazz = _articoloSocioRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceArticoloSocio",
                        String.class);

                method.invoke(_articoloSocioRemoteModel, codiceArticoloSocio);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceVarianteSocio() {
        return _codiceVarianteSocio;
    }

    @Override
    public void setCodiceVarianteSocio(String codiceVarianteSocio) {
        _codiceVarianteSocio = codiceVarianteSocio;

        if (_articoloSocioRemoteModel != null) {
            try {
                Class<?> clazz = _articoloSocioRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceVarianteSocio",
                        String.class);

                method.invoke(_articoloSocioRemoteModel, codiceVarianteSocio);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceArticolo() {
        return _codiceArticolo;
    }

    @Override
    public void setCodiceArticolo(String codiceArticolo) {
        _codiceArticolo = codiceArticolo;

        if (_articoloSocioRemoteModel != null) {
            try {
                Class<?> clazz = _articoloSocioRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceArticolo",
                        String.class);

                method.invoke(_articoloSocioRemoteModel, codiceArticolo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceVariante() {
        return _codiceVariante;
    }

    @Override
    public void setCodiceVariante(String codiceVariante) {
        _codiceVariante = codiceVariante;

        if (_articoloSocioRemoteModel != null) {
            try {
                Class<?> clazz = _articoloSocioRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceVariante",
                        String.class);

                method.invoke(_articoloSocioRemoteModel, codiceVariante);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getArticoloSocioRemoteModel() {
        return _articoloSocioRemoteModel;
    }

    public void setArticoloSocioRemoteModel(
        BaseModel<?> articoloSocioRemoteModel) {
        _articoloSocioRemoteModel = articoloSocioRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _articoloSocioRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_articoloSocioRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            ArticoloSocioLocalServiceUtil.addArticoloSocio(this);
        } else {
            ArticoloSocioLocalServiceUtil.updateArticoloSocio(this);
        }
    }

    @Override
    public ArticoloSocio toEscapedModel() {
        return (ArticoloSocio) ProxyUtil.newProxyInstance(ArticoloSocio.class.getClassLoader(),
            new Class[] { ArticoloSocio.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        ArticoloSocioClp clone = new ArticoloSocioClp();

        clone.setAssociato(getAssociato());
        clone.setCodiceArticoloSocio(getCodiceArticoloSocio());
        clone.setCodiceVarianteSocio(getCodiceVarianteSocio());
        clone.setCodiceArticolo(getCodiceArticolo());
        clone.setCodiceVariante(getCodiceVariante());

        return clone;
    }

    @Override
    public int compareTo(ArticoloSocio articoloSocio) {
        int value = 0;

        value = getCodiceArticolo().compareTo(articoloSocio.getCodiceArticolo());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ArticoloSocioClp)) {
            return false;
        }

        ArticoloSocioClp articoloSocio = (ArticoloSocioClp) obj;

        ArticoloSocioPK primaryKey = articoloSocio.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{associato=");
        sb.append(getAssociato());
        sb.append(", codiceArticoloSocio=");
        sb.append(getCodiceArticoloSocio());
        sb.append(", codiceVarianteSocio=");
        sb.append(getCodiceVarianteSocio());
        sb.append(", codiceArticolo=");
        sb.append(getCodiceArticolo());
        sb.append(", codiceVariante=");
        sb.append(getCodiceVariante());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(19);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.ArticoloSocio");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>associato</column-name><column-value><![CDATA[");
        sb.append(getAssociato());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceArticoloSocio</column-name><column-value><![CDATA[");
        sb.append(getCodiceArticoloSocio());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceVarianteSocio</column-name><column-value><![CDATA[");
        sb.append(getCodiceVarianteSocio());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceArticolo</column-name><column-value><![CDATA[");
        sb.append(getCodiceArticolo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceVariante</column-name><column-value><![CDATA[");
        sb.append(getCodiceVariante());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
