package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Varianti service. Represents a row in the &quot;DescrizioniVarianti&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see VariantiModel
 * @see it.bysoftware.ct.model.impl.VariantiImpl
 * @see it.bysoftware.ct.model.impl.VariantiModelImpl
 * @generated
 */
public interface Varianti extends VariantiModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.VariantiImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
