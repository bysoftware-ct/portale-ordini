package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the TipoCarrello service. Represents a row in the &quot;_tipi_carrello&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see TipoCarrelloModel
 * @see it.bysoftware.ct.model.impl.TipoCarrelloImpl
 * @see it.bysoftware.ct.model.impl.TipoCarrelloModelImpl
 * @generated
 */
public interface TipoCarrello extends TipoCarrelloModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.TipoCarrelloImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
