package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.RubricaPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.RubricaServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.RubricaServiceSoap
 * @generated
 */
public class RubricaSoap implements Serializable {
    private String _codiceSoggetto;
    private int _numeroVoce;
    private String _descrizione;
    private String _telefono;
    private String _cellulare;
    private String _fax;
    private String _eMailTo;
    private String _eMailCc;
    private String _eMailCcn;

    public RubricaSoap() {
    }

    public static RubricaSoap toSoapModel(Rubrica model) {
        RubricaSoap soapModel = new RubricaSoap();

        soapModel.setCodiceSoggetto(model.getCodiceSoggetto());
        soapModel.setNumeroVoce(model.getNumeroVoce());
        soapModel.setDescrizione(model.getDescrizione());
        soapModel.setTelefono(model.getTelefono());
        soapModel.setCellulare(model.getCellulare());
        soapModel.setFax(model.getFax());
        soapModel.setEMailTo(model.getEMailTo());
        soapModel.setEMailCc(model.getEMailCc());
        soapModel.setEMailCcn(model.getEMailCcn());

        return soapModel;
    }

    public static RubricaSoap[] toSoapModels(Rubrica[] models) {
        RubricaSoap[] soapModels = new RubricaSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static RubricaSoap[][] toSoapModels(Rubrica[][] models) {
        RubricaSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new RubricaSoap[models.length][models[0].length];
        } else {
            soapModels = new RubricaSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static RubricaSoap[] toSoapModels(List<Rubrica> models) {
        List<RubricaSoap> soapModels = new ArrayList<RubricaSoap>(models.size());

        for (Rubrica model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new RubricaSoap[soapModels.size()]);
    }

    public RubricaPK getPrimaryKey() {
        return new RubricaPK(_codiceSoggetto, _numeroVoce);
    }

    public void setPrimaryKey(RubricaPK pk) {
        setCodiceSoggetto(pk.codiceSoggetto);
        setNumeroVoce(pk.numeroVoce);
    }

    public String getCodiceSoggetto() {
        return _codiceSoggetto;
    }

    public void setCodiceSoggetto(String codiceSoggetto) {
        _codiceSoggetto = codiceSoggetto;
    }

    public int getNumeroVoce() {
        return _numeroVoce;
    }

    public void setNumeroVoce(int numeroVoce) {
        _numeroVoce = numeroVoce;
    }

    public String getDescrizione() {
        return _descrizione;
    }

    public void setDescrizione(String descrizione) {
        _descrizione = descrizione;
    }

    public String getTelefono() {
        return _telefono;
    }

    public void setTelefono(String telefono) {
        _telefono = telefono;
    }

    public String getCellulare() {
        return _cellulare;
    }

    public void setCellulare(String cellulare) {
        _cellulare = cellulare;
    }

    public String getFax() {
        return _fax;
    }

    public void setFax(String fax) {
        _fax = fax;
    }

    public String getEMailTo() {
        return _eMailTo;
    }

    public void setEMailTo(String eMailTo) {
        _eMailTo = eMailTo;
    }

    public String getEMailCc() {
        return _eMailCc;
    }

    public void setEMailCc(String eMailCc) {
        _eMailCc = eMailCc;
    }

    public String getEMailCcn() {
        return _eMailCcn;
    }

    public void setEMailCcn(String eMailCcn) {
        _eMailCcn = eMailCcn;
    }
}
