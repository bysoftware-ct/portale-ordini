package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ArticoloSocio}.
 * </p>
 *
 * @author Mario Torrisi
 * @see ArticoloSocio
 * @generated
 */
public class ArticoloSocioWrapper implements ArticoloSocio,
    ModelWrapper<ArticoloSocio> {
    private ArticoloSocio _articoloSocio;

    public ArticoloSocioWrapper(ArticoloSocio articoloSocio) {
        _articoloSocio = articoloSocio;
    }

    @Override
    public Class<?> getModelClass() {
        return ArticoloSocio.class;
    }

    @Override
    public String getModelClassName() {
        return ArticoloSocio.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("associato", getAssociato());
        attributes.put("codiceArticoloSocio", getCodiceArticoloSocio());
        attributes.put("codiceVarianteSocio", getCodiceVarianteSocio());
        attributes.put("codiceArticolo", getCodiceArticolo());
        attributes.put("codiceVariante", getCodiceVariante());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String associato = (String) attributes.get("associato");

        if (associato != null) {
            setAssociato(associato);
        }

        String codiceArticoloSocio = (String) attributes.get(
                "codiceArticoloSocio");

        if (codiceArticoloSocio != null) {
            setCodiceArticoloSocio(codiceArticoloSocio);
        }

        String codiceVarianteSocio = (String) attributes.get(
                "codiceVarianteSocio");

        if (codiceVarianteSocio != null) {
            setCodiceVarianteSocio(codiceVarianteSocio);
        }

        String codiceArticolo = (String) attributes.get("codiceArticolo");

        if (codiceArticolo != null) {
            setCodiceArticolo(codiceArticolo);
        }

        String codiceVariante = (String) attributes.get("codiceVariante");

        if (codiceVariante != null) {
            setCodiceVariante(codiceVariante);
        }
    }

    /**
    * Returns the primary key of this articolo socio.
    *
    * @return the primary key of this articolo socio
    */
    @Override
    public it.bysoftware.ct.service.persistence.ArticoloSocioPK getPrimaryKey() {
        return _articoloSocio.getPrimaryKey();
    }

    /**
    * Sets the primary key of this articolo socio.
    *
    * @param primaryKey the primary key of this articolo socio
    */
    @Override
    public void setPrimaryKey(
        it.bysoftware.ct.service.persistence.ArticoloSocioPK primaryKey) {
        _articoloSocio.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the associato of this articolo socio.
    *
    * @return the associato of this articolo socio
    */
    @Override
    public java.lang.String getAssociato() {
        return _articoloSocio.getAssociato();
    }

    /**
    * Sets the associato of this articolo socio.
    *
    * @param associato the associato of this articolo socio
    */
    @Override
    public void setAssociato(java.lang.String associato) {
        _articoloSocio.setAssociato(associato);
    }

    /**
    * Returns the codice articolo socio of this articolo socio.
    *
    * @return the codice articolo socio of this articolo socio
    */
    @Override
    public java.lang.String getCodiceArticoloSocio() {
        return _articoloSocio.getCodiceArticoloSocio();
    }

    /**
    * Sets the codice articolo socio of this articolo socio.
    *
    * @param codiceArticoloSocio the codice articolo socio of this articolo socio
    */
    @Override
    public void setCodiceArticoloSocio(java.lang.String codiceArticoloSocio) {
        _articoloSocio.setCodiceArticoloSocio(codiceArticoloSocio);
    }

    /**
    * Returns the codice variante socio of this articolo socio.
    *
    * @return the codice variante socio of this articolo socio
    */
    @Override
    public java.lang.String getCodiceVarianteSocio() {
        return _articoloSocio.getCodiceVarianteSocio();
    }

    /**
    * Sets the codice variante socio of this articolo socio.
    *
    * @param codiceVarianteSocio the codice variante socio of this articolo socio
    */
    @Override
    public void setCodiceVarianteSocio(java.lang.String codiceVarianteSocio) {
        _articoloSocio.setCodiceVarianteSocio(codiceVarianteSocio);
    }

    /**
    * Returns the codice articolo of this articolo socio.
    *
    * @return the codice articolo of this articolo socio
    */
    @Override
    public java.lang.String getCodiceArticolo() {
        return _articoloSocio.getCodiceArticolo();
    }

    /**
    * Sets the codice articolo of this articolo socio.
    *
    * @param codiceArticolo the codice articolo of this articolo socio
    */
    @Override
    public void setCodiceArticolo(java.lang.String codiceArticolo) {
        _articoloSocio.setCodiceArticolo(codiceArticolo);
    }

    /**
    * Returns the codice variante of this articolo socio.
    *
    * @return the codice variante of this articolo socio
    */
    @Override
    public java.lang.String getCodiceVariante() {
        return _articoloSocio.getCodiceVariante();
    }

    /**
    * Sets the codice variante of this articolo socio.
    *
    * @param codiceVariante the codice variante of this articolo socio
    */
    @Override
    public void setCodiceVariante(java.lang.String codiceVariante) {
        _articoloSocio.setCodiceVariante(codiceVariante);
    }

    @Override
    public boolean isNew() {
        return _articoloSocio.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _articoloSocio.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _articoloSocio.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _articoloSocio.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _articoloSocio.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _articoloSocio.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _articoloSocio.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _articoloSocio.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _articoloSocio.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _articoloSocio.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _articoloSocio.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new ArticoloSocioWrapper((ArticoloSocio) _articoloSocio.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.ArticoloSocio articoloSocio) {
        return _articoloSocio.compareTo(articoloSocio);
    }

    @Override
    public int hashCode() {
        return _articoloSocio.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.ArticoloSocio> toCacheModel() {
        return _articoloSocio.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.ArticoloSocio toEscapedModel() {
        return new ArticoloSocioWrapper(_articoloSocio.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.ArticoloSocio toUnescapedModel() {
        return new ArticoloSocioWrapper(_articoloSocio.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _articoloSocio.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _articoloSocio.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _articoloSocio.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ArticoloSocioWrapper)) {
            return false;
        }

        ArticoloSocioWrapper articoloSocioWrapper = (ArticoloSocioWrapper) obj;

        if (Validator.equals(_articoloSocio, articoloSocioWrapper._articoloSocio)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public ArticoloSocio getWrappedArticoloSocio() {
        return _articoloSocio;
    }

    @Override
    public ArticoloSocio getWrappedModel() {
        return _articoloSocio;
    }

    @Override
    public void resetOriginalValues() {
        _articoloSocio.resetOriginalValues();
    }
}
