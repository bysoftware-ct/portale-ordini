package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.VariantiPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.VariantiServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.VariantiServiceSoap
 * @generated
 */
public class VariantiSoap implements Serializable {
    private String _codiceVariante;
    private String _tipoVariante;
    private String _descrizione;

    public VariantiSoap() {
    }

    public static VariantiSoap toSoapModel(Varianti model) {
        VariantiSoap soapModel = new VariantiSoap();

        soapModel.setCodiceVariante(model.getCodiceVariante());
        soapModel.setTipoVariante(model.getTipoVariante());
        soapModel.setDescrizione(model.getDescrizione());

        return soapModel;
    }

    public static VariantiSoap[] toSoapModels(Varianti[] models) {
        VariantiSoap[] soapModels = new VariantiSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static VariantiSoap[][] toSoapModels(Varianti[][] models) {
        VariantiSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new VariantiSoap[models.length][models[0].length];
        } else {
            soapModels = new VariantiSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static VariantiSoap[] toSoapModels(List<Varianti> models) {
        List<VariantiSoap> soapModels = new ArrayList<VariantiSoap>(models.size());

        for (Varianti model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new VariantiSoap[soapModels.size()]);
    }

    public VariantiPK getPrimaryKey() {
        return new VariantiPK(_codiceVariante, _tipoVariante);
    }

    public void setPrimaryKey(VariantiPK pk) {
        setCodiceVariante(pk.codiceVariante);
        setTipoVariante(pk.tipoVariante);
    }

    public String getCodiceVariante() {
        return _codiceVariante;
    }

    public void setCodiceVariante(String codiceVariante) {
        _codiceVariante = codiceVariante;
    }

    public String getTipoVariante() {
        return _tipoVariante;
    }

    public void setTipoVariante(String tipoVariante) {
        _tipoVariante = tipoVariante;
    }

    public String getDescrizione() {
        return _descrizione;
    }

    public void setDescrizione(String descrizione) {
        _descrizione = descrizione;
    }
}
