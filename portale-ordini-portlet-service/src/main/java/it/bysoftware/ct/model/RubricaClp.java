package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.RubricaLocalServiceUtil;
import it.bysoftware.ct.service.persistence.RubricaPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class RubricaClp extends BaseModelImpl<Rubrica> implements Rubrica {
    private String _codiceSoggetto;
    private int _numeroVoce;
    private String _descrizione;
    private String _telefono;
    private String _cellulare;
    private String _fax;
    private String _eMailTo;
    private String _eMailCc;
    private String _eMailCcn;
    private BaseModel<?> _rubricaRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public RubricaClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Rubrica.class;
    }

    @Override
    public String getModelClassName() {
        return Rubrica.class.getName();
    }

    @Override
    public RubricaPK getPrimaryKey() {
        return new RubricaPK(_codiceSoggetto, _numeroVoce);
    }

    @Override
    public void setPrimaryKey(RubricaPK primaryKey) {
        setCodiceSoggetto(primaryKey.codiceSoggetto);
        setNumeroVoce(primaryKey.numeroVoce);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return new RubricaPK(_codiceSoggetto, _numeroVoce);
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((RubricaPK) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceSoggetto", getCodiceSoggetto());
        attributes.put("numeroVoce", getNumeroVoce());
        attributes.put("descrizione", getDescrizione());
        attributes.put("telefono", getTelefono());
        attributes.put("cellulare", getCellulare());
        attributes.put("fax", getFax());
        attributes.put("eMailTo", getEMailTo());
        attributes.put("eMailCc", getEMailCc());
        attributes.put("eMailCcn", getEMailCcn());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceSoggetto = (String) attributes.get("codiceSoggetto");

        if (codiceSoggetto != null) {
            setCodiceSoggetto(codiceSoggetto);
        }

        Integer numeroVoce = (Integer) attributes.get("numeroVoce");

        if (numeroVoce != null) {
            setNumeroVoce(numeroVoce);
        }

        String descrizione = (String) attributes.get("descrizione");

        if (descrizione != null) {
            setDescrizione(descrizione);
        }

        String telefono = (String) attributes.get("telefono");

        if (telefono != null) {
            setTelefono(telefono);
        }

        String cellulare = (String) attributes.get("cellulare");

        if (cellulare != null) {
            setCellulare(cellulare);
        }

        String fax = (String) attributes.get("fax");

        if (fax != null) {
            setFax(fax);
        }

        String eMailTo = (String) attributes.get("eMailTo");

        if (eMailTo != null) {
            setEMailTo(eMailTo);
        }

        String eMailCc = (String) attributes.get("eMailCc");

        if (eMailCc != null) {
            setEMailCc(eMailCc);
        }

        String eMailCcn = (String) attributes.get("eMailCcn");

        if (eMailCcn != null) {
            setEMailCcn(eMailCcn);
        }
    }

    @Override
    public String getCodiceSoggetto() {
        return _codiceSoggetto;
    }

    @Override
    public void setCodiceSoggetto(String codiceSoggetto) {
        _codiceSoggetto = codiceSoggetto;

        if (_rubricaRemoteModel != null) {
            try {
                Class<?> clazz = _rubricaRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceSoggetto",
                        String.class);

                method.invoke(_rubricaRemoteModel, codiceSoggetto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getNumeroVoce() {
        return _numeroVoce;
    }

    @Override
    public void setNumeroVoce(int numeroVoce) {
        _numeroVoce = numeroVoce;

        if (_rubricaRemoteModel != null) {
            try {
                Class<?> clazz = _rubricaRemoteModel.getClass();

                Method method = clazz.getMethod("setNumeroVoce", int.class);

                method.invoke(_rubricaRemoteModel, numeroVoce);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getDescrizione() {
        return _descrizione;
    }

    @Override
    public void setDescrizione(String descrizione) {
        _descrizione = descrizione;

        if (_rubricaRemoteModel != null) {
            try {
                Class<?> clazz = _rubricaRemoteModel.getClass();

                Method method = clazz.getMethod("setDescrizione", String.class);

                method.invoke(_rubricaRemoteModel, descrizione);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getTelefono() {
        return _telefono;
    }

    @Override
    public void setTelefono(String telefono) {
        _telefono = telefono;

        if (_rubricaRemoteModel != null) {
            try {
                Class<?> clazz = _rubricaRemoteModel.getClass();

                Method method = clazz.getMethod("setTelefono", String.class);

                method.invoke(_rubricaRemoteModel, telefono);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCellulare() {
        return _cellulare;
    }

    @Override
    public void setCellulare(String cellulare) {
        _cellulare = cellulare;

        if (_rubricaRemoteModel != null) {
            try {
                Class<?> clazz = _rubricaRemoteModel.getClass();

                Method method = clazz.getMethod("setCellulare", String.class);

                method.invoke(_rubricaRemoteModel, cellulare);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getFax() {
        return _fax;
    }

    @Override
    public void setFax(String fax) {
        _fax = fax;

        if (_rubricaRemoteModel != null) {
            try {
                Class<?> clazz = _rubricaRemoteModel.getClass();

                Method method = clazz.getMethod("setFax", String.class);

                method.invoke(_rubricaRemoteModel, fax);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getEMailTo() {
        return _eMailTo;
    }

    @Override
    public void setEMailTo(String eMailTo) {
        _eMailTo = eMailTo;

        if (_rubricaRemoteModel != null) {
            try {
                Class<?> clazz = _rubricaRemoteModel.getClass();

                Method method = clazz.getMethod("setEMailTo", String.class);

                method.invoke(_rubricaRemoteModel, eMailTo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getEMailCc() {
        return _eMailCc;
    }

    @Override
    public void setEMailCc(String eMailCc) {
        _eMailCc = eMailCc;

        if (_rubricaRemoteModel != null) {
            try {
                Class<?> clazz = _rubricaRemoteModel.getClass();

                Method method = clazz.getMethod("setEMailCc", String.class);

                method.invoke(_rubricaRemoteModel, eMailCc);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getEMailCcn() {
        return _eMailCcn;
    }

    @Override
    public void setEMailCcn(String eMailCcn) {
        _eMailCcn = eMailCcn;

        if (_rubricaRemoteModel != null) {
            try {
                Class<?> clazz = _rubricaRemoteModel.getClass();

                Method method = clazz.getMethod("setEMailCcn", String.class);

                method.invoke(_rubricaRemoteModel, eMailCcn);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getRubricaRemoteModel() {
        return _rubricaRemoteModel;
    }

    public void setRubricaRemoteModel(BaseModel<?> rubricaRemoteModel) {
        _rubricaRemoteModel = rubricaRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _rubricaRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_rubricaRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            RubricaLocalServiceUtil.addRubrica(this);
        } else {
            RubricaLocalServiceUtil.updateRubrica(this);
        }
    }

    @Override
    public Rubrica toEscapedModel() {
        return (Rubrica) ProxyUtil.newProxyInstance(Rubrica.class.getClassLoader(),
            new Class[] { Rubrica.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        RubricaClp clone = new RubricaClp();

        clone.setCodiceSoggetto(getCodiceSoggetto());
        clone.setNumeroVoce(getNumeroVoce());
        clone.setDescrizione(getDescrizione());
        clone.setTelefono(getTelefono());
        clone.setCellulare(getCellulare());
        clone.setFax(getFax());
        clone.setEMailTo(getEMailTo());
        clone.setEMailCc(getEMailCc());
        clone.setEMailCcn(getEMailCcn());

        return clone;
    }

    @Override
    public int compareTo(Rubrica rubrica) {
        int value = 0;

        if (getNumeroVoce() < rubrica.getNumeroVoce()) {
            value = -1;
        } else if (getNumeroVoce() > rubrica.getNumeroVoce()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof RubricaClp)) {
            return false;
        }

        RubricaClp rubrica = (RubricaClp) obj;

        RubricaPK primaryKey = rubrica.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(19);

        sb.append("{codiceSoggetto=");
        sb.append(getCodiceSoggetto());
        sb.append(", numeroVoce=");
        sb.append(getNumeroVoce());
        sb.append(", descrizione=");
        sb.append(getDescrizione());
        sb.append(", telefono=");
        sb.append(getTelefono());
        sb.append(", cellulare=");
        sb.append(getCellulare());
        sb.append(", fax=");
        sb.append(getFax());
        sb.append(", eMailTo=");
        sb.append(getEMailTo());
        sb.append(", eMailCc=");
        sb.append(getEMailCc());
        sb.append(", eMailCcn=");
        sb.append(getEMailCcn());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(31);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.Rubrica");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>codiceSoggetto</column-name><column-value><![CDATA[");
        sb.append(getCodiceSoggetto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>numeroVoce</column-name><column-value><![CDATA[");
        sb.append(getNumeroVoce());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>descrizione</column-name><column-value><![CDATA[");
        sb.append(getDescrizione());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>telefono</column-name><column-value><![CDATA[");
        sb.append(getTelefono());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>cellulare</column-name><column-value><![CDATA[");
        sb.append(getCellulare());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>fax</column-name><column-value><![CDATA[");
        sb.append(getFax());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>eMailTo</column-name><column-value><![CDATA[");
        sb.append(getEMailTo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>eMailCc</column-name><column-value><![CDATA[");
        sb.append(getEMailCc());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>eMailCcn</column-name><column-value><![CDATA[");
        sb.append(getEMailCcn());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
