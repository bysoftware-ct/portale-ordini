package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.VariantiPiantaLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class VariantiPiantaClp extends BaseModelImpl<VariantiPianta>
    implements VariantiPianta {
    private long _id;
    private long _idVariante;
    private long _idPianta;
    private BaseModel<?> _variantiPiantaRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public VariantiPiantaClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return VariantiPianta.class;
    }

    @Override
    public String getModelClassName() {
        return VariantiPianta.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("idVariante", getIdVariante());
        attributes.put("idPianta", getIdPianta());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long idVariante = (Long) attributes.get("idVariante");

        if (idVariante != null) {
            setIdVariante(idVariante);
        }

        Long idPianta = (Long) attributes.get("idPianta");

        if (idPianta != null) {
            setIdPianta(idPianta);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_variantiPiantaRemoteModel != null) {
            try {
                Class<?> clazz = _variantiPiantaRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_variantiPiantaRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getIdVariante() {
        return _idVariante;
    }

    @Override
    public void setIdVariante(long idVariante) {
        _idVariante = idVariante;

        if (_variantiPiantaRemoteModel != null) {
            try {
                Class<?> clazz = _variantiPiantaRemoteModel.getClass();

                Method method = clazz.getMethod("setIdVariante", long.class);

                method.invoke(_variantiPiantaRemoteModel, idVariante);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getIdPianta() {
        return _idPianta;
    }

    @Override
    public void setIdPianta(long idPianta) {
        _idPianta = idPianta;

        if (_variantiPiantaRemoteModel != null) {
            try {
                Class<?> clazz = _variantiPiantaRemoteModel.getClass();

                Method method = clazz.getMethod("setIdPianta", long.class);

                method.invoke(_variantiPiantaRemoteModel, idPianta);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getVariantiPiantaRemoteModel() {
        return _variantiPiantaRemoteModel;
    }

    public void setVariantiPiantaRemoteModel(
        BaseModel<?> variantiPiantaRemoteModel) {
        _variantiPiantaRemoteModel = variantiPiantaRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _variantiPiantaRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_variantiPiantaRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            VariantiPiantaLocalServiceUtil.addVariantiPianta(this);
        } else {
            VariantiPiantaLocalServiceUtil.updateVariantiPianta(this);
        }
    }

    @Override
    public VariantiPianta toEscapedModel() {
        return (VariantiPianta) ProxyUtil.newProxyInstance(VariantiPianta.class.getClassLoader(),
            new Class[] { VariantiPianta.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        VariantiPiantaClp clone = new VariantiPiantaClp();

        clone.setId(getId());
        clone.setIdVariante(getIdVariante());
        clone.setIdPianta(getIdPianta());

        return clone;
    }

    @Override
    public int compareTo(VariantiPianta variantiPianta) {
        long primaryKey = variantiPianta.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VariantiPiantaClp)) {
            return false;
        }

        VariantiPiantaClp variantiPianta = (VariantiPiantaClp) obj;

        long primaryKey = variantiPianta.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", idVariante=");
        sb.append(getIdVariante());
        sb.append(", idPianta=");
        sb.append(getIdPianta());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(13);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.VariantiPianta");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>idVariante</column-name><column-value><![CDATA[");
        sb.append(getIdVariante());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>idPianta</column-name><column-value><![CDATA[");
        sb.append(getIdPianta());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
