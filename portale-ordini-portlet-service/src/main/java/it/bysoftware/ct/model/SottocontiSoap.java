package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.SottocontiServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.SottocontiServiceSoap
 * @generated
 */
public class SottocontiSoap implements Serializable {
    private String _codice;
    private String _descrizione;

    public SottocontiSoap() {
    }

    public static SottocontiSoap toSoapModel(Sottoconti model) {
        SottocontiSoap soapModel = new SottocontiSoap();

        soapModel.setCodice(model.getCodice());
        soapModel.setDescrizione(model.getDescrizione());

        return soapModel;
    }

    public static SottocontiSoap[] toSoapModels(Sottoconti[] models) {
        SottocontiSoap[] soapModels = new SottocontiSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static SottocontiSoap[][] toSoapModels(Sottoconti[][] models) {
        SottocontiSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new SottocontiSoap[models.length][models[0].length];
        } else {
            soapModels = new SottocontiSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static SottocontiSoap[] toSoapModels(List<Sottoconti> models) {
        List<SottocontiSoap> soapModels = new ArrayList<SottocontiSoap>(models.size());

        for (Sottoconti model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new SottocontiSoap[soapModels.size()]);
    }

    public String getPrimaryKey() {
        return _codice;
    }

    public void setPrimaryKey(String pk) {
        setCodice(pk);
    }

    public String getCodice() {
        return _codice;
    }

    public void setCodice(String codice) {
        _codice = codice;
    }

    public String getDescrizione() {
        return _descrizione;
    }

    public void setDescrizione(String descrizione) {
        _descrizione = descrizione;
    }
}
