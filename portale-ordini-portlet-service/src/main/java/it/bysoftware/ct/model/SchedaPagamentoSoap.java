package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.SchedaPagamentoPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.SchedaPagamentoServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.SchedaPagamentoServiceSoap
 * @generated
 */
public class SchedaPagamentoSoap implements Serializable {
    private int _esercizioRegistrazione;
    private String _codiceSoggetto;
    private int _numeroPartita;
    private int _numeroScadenza;
    private boolean _tipoSoggetto;
    private int _esercizioRegistrazioneComp;
    private String _codiceSoggettoComp;
    private int _numeroPartitaComp;
    private int _numeroScadenzaComp;
    private boolean _tipoSoggettoComp;
    private double _differenza;
    private int _idPagamento;
    private int _annoPagamento;
    private int _stato;
    private String _nota;

    public SchedaPagamentoSoap() {
    }

    public static SchedaPagamentoSoap toSoapModel(SchedaPagamento model) {
        SchedaPagamentoSoap soapModel = new SchedaPagamentoSoap();

        soapModel.setEsercizioRegistrazione(model.getEsercizioRegistrazione());
        soapModel.setCodiceSoggetto(model.getCodiceSoggetto());
        soapModel.setNumeroPartita(model.getNumeroPartita());
        soapModel.setNumeroScadenza(model.getNumeroScadenza());
        soapModel.setTipoSoggetto(model.getTipoSoggetto());
        soapModel.setEsercizioRegistrazioneComp(model.getEsercizioRegistrazioneComp());
        soapModel.setCodiceSoggettoComp(model.getCodiceSoggettoComp());
        soapModel.setNumeroPartitaComp(model.getNumeroPartitaComp());
        soapModel.setNumeroScadenzaComp(model.getNumeroScadenzaComp());
        soapModel.setTipoSoggettoComp(model.getTipoSoggettoComp());
        soapModel.setDifferenza(model.getDifferenza());
        soapModel.setIdPagamento(model.getIdPagamento());
        soapModel.setAnnoPagamento(model.getAnnoPagamento());
        soapModel.setStato(model.getStato());
        soapModel.setNota(model.getNota());

        return soapModel;
    }

    public static SchedaPagamentoSoap[] toSoapModels(SchedaPagamento[] models) {
        SchedaPagamentoSoap[] soapModels = new SchedaPagamentoSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static SchedaPagamentoSoap[][] toSoapModels(
        SchedaPagamento[][] models) {
        SchedaPagamentoSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new SchedaPagamentoSoap[models.length][models[0].length];
        } else {
            soapModels = new SchedaPagamentoSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static SchedaPagamentoSoap[] toSoapModels(
        List<SchedaPagamento> models) {
        List<SchedaPagamentoSoap> soapModels = new ArrayList<SchedaPagamentoSoap>(models.size());

        for (SchedaPagamento model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new SchedaPagamentoSoap[soapModels.size()]);
    }

    public SchedaPagamentoPK getPrimaryKey() {
        return new SchedaPagamentoPK(_esercizioRegistrazione, _codiceSoggetto,
            _numeroPartita, _numeroScadenza, _tipoSoggetto);
    }

    public void setPrimaryKey(SchedaPagamentoPK pk) {
        setEsercizioRegistrazione(pk.esercizioRegistrazione);
        setCodiceSoggetto(pk.codiceSoggetto);
        setNumeroPartita(pk.numeroPartita);
        setNumeroScadenza(pk.numeroScadenza);
        setTipoSoggetto(pk.tipoSoggetto);
    }

    public int getEsercizioRegistrazione() {
        return _esercizioRegistrazione;
    }

    public void setEsercizioRegistrazione(int esercizioRegistrazione) {
        _esercizioRegistrazione = esercizioRegistrazione;
    }

    public String getCodiceSoggetto() {
        return _codiceSoggetto;
    }

    public void setCodiceSoggetto(String codiceSoggetto) {
        _codiceSoggetto = codiceSoggetto;
    }

    public int getNumeroPartita() {
        return _numeroPartita;
    }

    public void setNumeroPartita(int numeroPartita) {
        _numeroPartita = numeroPartita;
    }

    public int getNumeroScadenza() {
        return _numeroScadenza;
    }

    public void setNumeroScadenza(int numeroScadenza) {
        _numeroScadenza = numeroScadenza;
    }

    public boolean getTipoSoggetto() {
        return _tipoSoggetto;
    }

    public boolean isTipoSoggetto() {
        return _tipoSoggetto;
    }

    public void setTipoSoggetto(boolean tipoSoggetto) {
        _tipoSoggetto = tipoSoggetto;
    }

    public int getEsercizioRegistrazioneComp() {
        return _esercizioRegistrazioneComp;
    }

    public void setEsercizioRegistrazioneComp(int esercizioRegistrazioneComp) {
        _esercizioRegistrazioneComp = esercizioRegistrazioneComp;
    }

    public String getCodiceSoggettoComp() {
        return _codiceSoggettoComp;
    }

    public void setCodiceSoggettoComp(String codiceSoggettoComp) {
        _codiceSoggettoComp = codiceSoggettoComp;
    }

    public int getNumeroPartitaComp() {
        return _numeroPartitaComp;
    }

    public void setNumeroPartitaComp(int numeroPartitaComp) {
        _numeroPartitaComp = numeroPartitaComp;
    }

    public int getNumeroScadenzaComp() {
        return _numeroScadenzaComp;
    }

    public void setNumeroScadenzaComp(int numeroScadenzaComp) {
        _numeroScadenzaComp = numeroScadenzaComp;
    }

    public boolean getTipoSoggettoComp() {
        return _tipoSoggettoComp;
    }

    public boolean isTipoSoggettoComp() {
        return _tipoSoggettoComp;
    }

    public void setTipoSoggettoComp(boolean tipoSoggettoComp) {
        _tipoSoggettoComp = tipoSoggettoComp;
    }

    public double getDifferenza() {
        return _differenza;
    }

    public void setDifferenza(double differenza) {
        _differenza = differenza;
    }

    public int getIdPagamento() {
        return _idPagamento;
    }

    public void setIdPagamento(int idPagamento) {
        _idPagamento = idPagamento;
    }

    public int getAnnoPagamento() {
        return _annoPagamento;
    }

    public void setAnnoPagamento(int annoPagamento) {
        _annoPagamento = annoPagamento;
    }

    public int getStato() {
        return _stato;
    }

    public void setStato(int stato) {
        _stato = stato;
    }

    public String getNota() {
        return _nota;
    }

    public void setNota(String nota) {
        _nota = nota;
    }
}
