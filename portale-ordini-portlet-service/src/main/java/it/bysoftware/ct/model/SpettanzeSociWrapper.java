package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link SpettanzeSoci}.
 * </p>
 *
 * @author Mario Torrisi
 * @see SpettanzeSoci
 * @generated
 */
public class SpettanzeSociWrapper implements SpettanzeSoci,
    ModelWrapper<SpettanzeSoci> {
    private SpettanzeSoci _spettanzeSoci;

    public SpettanzeSociWrapper(SpettanzeSoci spettanzeSoci) {
        _spettanzeSoci = spettanzeSoci;
    }

    @Override
    public Class<?> getModelClass() {
        return SpettanzeSoci.class;
    }

    @Override
    public String getModelClassName() {
        return SpettanzeSoci.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("anno", getAnno());
        attributes.put("codiceAttivita", getCodiceAttivita());
        attributes.put("codiceCentro", getCodiceCentro());
        attributes.put("numeroProtocollo", getNumeroProtocollo());
        attributes.put("codiceFornitore", getCodiceFornitore());
        attributes.put("importo", getImporto());
        attributes.put("dataCalcolo", getDataCalcolo());
        attributes.put("stato", getStato());
        attributes.put("idPagamento", getIdPagamento());
        attributes.put("annoPagamento", getAnnoPagamento());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Integer anno = (Integer) attributes.get("anno");

        if (anno != null) {
            setAnno(anno);
        }

        String codiceAttivita = (String) attributes.get("codiceAttivita");

        if (codiceAttivita != null) {
            setCodiceAttivita(codiceAttivita);
        }

        String codiceCentro = (String) attributes.get("codiceCentro");

        if (codiceCentro != null) {
            setCodiceCentro(codiceCentro);
        }

        Integer numeroProtocollo = (Integer) attributes.get("numeroProtocollo");

        if (numeroProtocollo != null) {
            setNumeroProtocollo(numeroProtocollo);
        }

        String codiceFornitore = (String) attributes.get("codiceFornitore");

        if (codiceFornitore != null) {
            setCodiceFornitore(codiceFornitore);
        }

        Double importo = (Double) attributes.get("importo");

        if (importo != null) {
            setImporto(importo);
        }

        Date dataCalcolo = (Date) attributes.get("dataCalcolo");

        if (dataCalcolo != null) {
            setDataCalcolo(dataCalcolo);
        }

        Integer stato = (Integer) attributes.get("stato");

        if (stato != null) {
            setStato(stato);
        }

        Integer idPagamento = (Integer) attributes.get("idPagamento");

        if (idPagamento != null) {
            setIdPagamento(idPagamento);
        }

        Integer annoPagamento = (Integer) attributes.get("annoPagamento");

        if (annoPagamento != null) {
            setAnnoPagamento(annoPagamento);
        }
    }

    /**
    * Returns the primary key of this spettanze soci.
    *
    * @return the primary key of this spettanze soci
    */
    @Override
    public long getPrimaryKey() {
        return _spettanzeSoci.getPrimaryKey();
    }

    /**
    * Sets the primary key of this spettanze soci.
    *
    * @param primaryKey the primary key of this spettanze soci
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _spettanzeSoci.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this spettanze soci.
    *
    * @return the ID of this spettanze soci
    */
    @Override
    public long getId() {
        return _spettanzeSoci.getId();
    }

    /**
    * Sets the ID of this spettanze soci.
    *
    * @param id the ID of this spettanze soci
    */
    @Override
    public void setId(long id) {
        _spettanzeSoci.setId(id);
    }

    /**
    * Returns the anno of this spettanze soci.
    *
    * @return the anno of this spettanze soci
    */
    @Override
    public int getAnno() {
        return _spettanzeSoci.getAnno();
    }

    /**
    * Sets the anno of this spettanze soci.
    *
    * @param anno the anno of this spettanze soci
    */
    @Override
    public void setAnno(int anno) {
        _spettanzeSoci.setAnno(anno);
    }

    /**
    * Returns the codice attivita of this spettanze soci.
    *
    * @return the codice attivita of this spettanze soci
    */
    @Override
    public java.lang.String getCodiceAttivita() {
        return _spettanzeSoci.getCodiceAttivita();
    }

    /**
    * Sets the codice attivita of this spettanze soci.
    *
    * @param codiceAttivita the codice attivita of this spettanze soci
    */
    @Override
    public void setCodiceAttivita(java.lang.String codiceAttivita) {
        _spettanzeSoci.setCodiceAttivita(codiceAttivita);
    }

    /**
    * Returns the codice centro of this spettanze soci.
    *
    * @return the codice centro of this spettanze soci
    */
    @Override
    public java.lang.String getCodiceCentro() {
        return _spettanzeSoci.getCodiceCentro();
    }

    /**
    * Sets the codice centro of this spettanze soci.
    *
    * @param codiceCentro the codice centro of this spettanze soci
    */
    @Override
    public void setCodiceCentro(java.lang.String codiceCentro) {
        _spettanzeSoci.setCodiceCentro(codiceCentro);
    }

    /**
    * Returns the numero protocollo of this spettanze soci.
    *
    * @return the numero protocollo of this spettanze soci
    */
    @Override
    public int getNumeroProtocollo() {
        return _spettanzeSoci.getNumeroProtocollo();
    }

    /**
    * Sets the numero protocollo of this spettanze soci.
    *
    * @param numeroProtocollo the numero protocollo of this spettanze soci
    */
    @Override
    public void setNumeroProtocollo(int numeroProtocollo) {
        _spettanzeSoci.setNumeroProtocollo(numeroProtocollo);
    }

    /**
    * Returns the codice fornitore of this spettanze soci.
    *
    * @return the codice fornitore of this spettanze soci
    */
    @Override
    public java.lang.String getCodiceFornitore() {
        return _spettanzeSoci.getCodiceFornitore();
    }

    /**
    * Sets the codice fornitore of this spettanze soci.
    *
    * @param codiceFornitore the codice fornitore of this spettanze soci
    */
    @Override
    public void setCodiceFornitore(java.lang.String codiceFornitore) {
        _spettanzeSoci.setCodiceFornitore(codiceFornitore);
    }

    /**
    * Returns the importo of this spettanze soci.
    *
    * @return the importo of this spettanze soci
    */
    @Override
    public double getImporto() {
        return _spettanzeSoci.getImporto();
    }

    /**
    * Sets the importo of this spettanze soci.
    *
    * @param importo the importo of this spettanze soci
    */
    @Override
    public void setImporto(double importo) {
        _spettanzeSoci.setImporto(importo);
    }

    /**
    * Returns the data calcolo of this spettanze soci.
    *
    * @return the data calcolo of this spettanze soci
    */
    @Override
    public java.util.Date getDataCalcolo() {
        return _spettanzeSoci.getDataCalcolo();
    }

    /**
    * Sets the data calcolo of this spettanze soci.
    *
    * @param dataCalcolo the data calcolo of this spettanze soci
    */
    @Override
    public void setDataCalcolo(java.util.Date dataCalcolo) {
        _spettanzeSoci.setDataCalcolo(dataCalcolo);
    }

    /**
    * Returns the stato of this spettanze soci.
    *
    * @return the stato of this spettanze soci
    */
    @Override
    public int getStato() {
        return _spettanzeSoci.getStato();
    }

    /**
    * Sets the stato of this spettanze soci.
    *
    * @param stato the stato of this spettanze soci
    */
    @Override
    public void setStato(int stato) {
        _spettanzeSoci.setStato(stato);
    }

    /**
    * Returns the id pagamento of this spettanze soci.
    *
    * @return the id pagamento of this spettanze soci
    */
    @Override
    public int getIdPagamento() {
        return _spettanzeSoci.getIdPagamento();
    }

    /**
    * Sets the id pagamento of this spettanze soci.
    *
    * @param idPagamento the id pagamento of this spettanze soci
    */
    @Override
    public void setIdPagamento(int idPagamento) {
        _spettanzeSoci.setIdPagamento(idPagamento);
    }

    /**
    * Returns the anno pagamento of this spettanze soci.
    *
    * @return the anno pagamento of this spettanze soci
    */
    @Override
    public int getAnnoPagamento() {
        return _spettanzeSoci.getAnnoPagamento();
    }

    /**
    * Sets the anno pagamento of this spettanze soci.
    *
    * @param annoPagamento the anno pagamento of this spettanze soci
    */
    @Override
    public void setAnnoPagamento(int annoPagamento) {
        _spettanzeSoci.setAnnoPagamento(annoPagamento);
    }

    @Override
    public boolean isNew() {
        return _spettanzeSoci.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _spettanzeSoci.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _spettanzeSoci.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _spettanzeSoci.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _spettanzeSoci.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _spettanzeSoci.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _spettanzeSoci.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _spettanzeSoci.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _spettanzeSoci.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _spettanzeSoci.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _spettanzeSoci.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new SpettanzeSociWrapper((SpettanzeSoci) _spettanzeSoci.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.SpettanzeSoci spettanzeSoci) {
        return _spettanzeSoci.compareTo(spettanzeSoci);
    }

    @Override
    public int hashCode() {
        return _spettanzeSoci.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.SpettanzeSoci> toCacheModel() {
        return _spettanzeSoci.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.SpettanzeSoci toEscapedModel() {
        return new SpettanzeSociWrapper(_spettanzeSoci.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.SpettanzeSoci toUnescapedModel() {
        return new SpettanzeSociWrapper(_spettanzeSoci.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _spettanzeSoci.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _spettanzeSoci.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _spettanzeSoci.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof SpettanzeSociWrapper)) {
            return false;
        }

        SpettanzeSociWrapper spettanzeSociWrapper = (SpettanzeSociWrapper) obj;

        if (Validator.equals(_spettanzeSoci, spettanzeSociWrapper._spettanzeSoci)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public SpettanzeSoci getWrappedSpettanzeSoci() {
        return _spettanzeSoci;
    }

    @Override
    public SpettanzeSoci getWrappedModel() {
        return _spettanzeSoci;
    }

    @Override
    public void resetOriginalValues() {
        _spettanzeSoci.resetOriginalValues();
    }
}
