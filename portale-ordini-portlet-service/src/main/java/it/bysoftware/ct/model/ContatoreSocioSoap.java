package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.ContatoreSocioPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.ContatoreSocioServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.ContatoreSocioServiceSoap
 * @generated
 */
public class ContatoreSocioSoap implements Serializable {
    private int _anno;
    private String _codiceSocio;
    private String _tipoDocumento;
    private int _numero;

    public ContatoreSocioSoap() {
    }

    public static ContatoreSocioSoap toSoapModel(ContatoreSocio model) {
        ContatoreSocioSoap soapModel = new ContatoreSocioSoap();

        soapModel.setAnno(model.getAnno());
        soapModel.setCodiceSocio(model.getCodiceSocio());
        soapModel.setTipoDocumento(model.getTipoDocumento());
        soapModel.setNumero(model.getNumero());

        return soapModel;
    }

    public static ContatoreSocioSoap[] toSoapModels(ContatoreSocio[] models) {
        ContatoreSocioSoap[] soapModels = new ContatoreSocioSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static ContatoreSocioSoap[][] toSoapModels(ContatoreSocio[][] models) {
        ContatoreSocioSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new ContatoreSocioSoap[models.length][models[0].length];
        } else {
            soapModels = new ContatoreSocioSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static ContatoreSocioSoap[] toSoapModels(List<ContatoreSocio> models) {
        List<ContatoreSocioSoap> soapModels = new ArrayList<ContatoreSocioSoap>(models.size());

        for (ContatoreSocio model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new ContatoreSocioSoap[soapModels.size()]);
    }

    public ContatoreSocioPK getPrimaryKey() {
        return new ContatoreSocioPK(_anno, _codiceSocio, _tipoDocumento);
    }

    public void setPrimaryKey(ContatoreSocioPK pk) {
        setAnno(pk.anno);
        setCodiceSocio(pk.codiceSocio);
        setTipoDocumento(pk.tipoDocumento);
    }

    public int getAnno() {
        return _anno;
    }

    public void setAnno(int anno) {
        _anno = anno;
    }

    public String getCodiceSocio() {
        return _codiceSocio;
    }

    public void setCodiceSocio(String codiceSocio) {
        _codiceSocio = codiceSocio;
    }

    public String getTipoDocumento() {
        return _tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        _tipoDocumento = tipoDocumento;
    }

    public int getNumero() {
        return _numero;
    }

    public void setNumero(int numero) {
        _numero = numero;
    }
}
