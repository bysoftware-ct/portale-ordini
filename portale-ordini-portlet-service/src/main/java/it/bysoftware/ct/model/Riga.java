package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Riga service. Represents a row in the &quot;_righe&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see RigaModel
 * @see it.bysoftware.ct.model.impl.RigaImpl
 * @see it.bysoftware.ct.model.impl.RigaModelImpl
 * @generated
 */
public interface Riga extends RigaModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.RigaImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
