package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Ordine}.
 * </p>
 *
 * @author Mario Torrisi
 * @see Ordine
 * @generated
 */
public class OrdineWrapper implements Ordine, ModelWrapper<Ordine> {
    private Ordine _ordine;

    public OrdineWrapper(Ordine ordine) {
        _ordine = ordine;
    }

    @Override
    public Class<?> getModelClass() {
        return Ordine.class;
    }

    @Override
    public String getModelClassName() {
        return Ordine.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("dataInserimento", getDataInserimento());
        attributes.put("idCliente", getIdCliente());
        attributes.put("stato", getStato());
        attributes.put("importo", getImporto());
        attributes.put("note", getNote());
        attributes.put("dataConsegna", getDataConsegna());
        attributes.put("idTrasportatore", getIdTrasportatore());
        attributes.put("idTrasportatore2", getIdTrasportatore2());
        attributes.put("idAgente", getIdAgente());
        attributes.put("idTipoCarrello", getIdTipoCarrello());
        attributes.put("luogoCarico", getLuogoCarico());
        attributes.put("noteCliente", getNoteCliente());
        attributes.put("anno", getAnno());
        attributes.put("numero", getNumero());
        attributes.put("centro", getCentro());
        attributes.put("modificato", getModificato());
        attributes.put("codiceIva", getCodiceIva());
        attributes.put("cliente", getCliente());
        attributes.put("vettore", getVettore());
        attributes.put("pagamento", getPagamento());
        attributes.put("ddtGenerati", getDdtGenerati());
        attributes.put("stampaEtichette", getStampaEtichette());
        attributes.put("numOrdineGT", getNumOrdineGT());
        attributes.put("dataOrdineGT", getDataOrdineGT());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Date dataInserimento = (Date) attributes.get("dataInserimento");

        if (dataInserimento != null) {
            setDataInserimento(dataInserimento);
        }

        String idCliente = (String) attributes.get("idCliente");

        if (idCliente != null) {
            setIdCliente(idCliente);
        }

        Integer stato = (Integer) attributes.get("stato");

        if (stato != null) {
            setStato(stato);
        }

        Double importo = (Double) attributes.get("importo");

        if (importo != null) {
            setImporto(importo);
        }

        String note = (String) attributes.get("note");

        if (note != null) {
            setNote(note);
        }

        Date dataConsegna = (Date) attributes.get("dataConsegna");

        if (dataConsegna != null) {
            setDataConsegna(dataConsegna);
        }

        String idTrasportatore = (String) attributes.get("idTrasportatore");

        if (idTrasportatore != null) {
            setIdTrasportatore(idTrasportatore);
        }

        String idTrasportatore2 = (String) attributes.get("idTrasportatore2");

        if (idTrasportatore2 != null) {
            setIdTrasportatore2(idTrasportatore2);
        }

        Long idAgente = (Long) attributes.get("idAgente");

        if (idAgente != null) {
            setIdAgente(idAgente);
        }

        Long idTipoCarrello = (Long) attributes.get("idTipoCarrello");

        if (idTipoCarrello != null) {
            setIdTipoCarrello(idTipoCarrello);
        }

        String luogoCarico = (String) attributes.get("luogoCarico");

        if (luogoCarico != null) {
            setLuogoCarico(luogoCarico);
        }

        String noteCliente = (String) attributes.get("noteCliente");

        if (noteCliente != null) {
            setNoteCliente(noteCliente);
        }

        Integer anno = (Integer) attributes.get("anno");

        if (anno != null) {
            setAnno(anno);
        }

        Integer numero = (Integer) attributes.get("numero");

        if (numero != null) {
            setNumero(numero);
        }

        String centro = (String) attributes.get("centro");

        if (centro != null) {
            setCentro(centro);
        }

        Boolean modificato = (Boolean) attributes.get("modificato");

        if (modificato != null) {
            setModificato(modificato);
        }

        String codiceIva = (String) attributes.get("codiceIva");

        if (codiceIva != null) {
            setCodiceIva(codiceIva);
        }

        String cliente = (String) attributes.get("cliente");

        if (cliente != null) {
            setCliente(cliente);
        }

        String vettore = (String) attributes.get("vettore");

        if (vettore != null) {
            setVettore(vettore);
        }

        String pagamento = (String) attributes.get("pagamento");

        if (pagamento != null) {
            setPagamento(pagamento);
        }

        Boolean ddtGenerati = (Boolean) attributes.get("ddtGenerati");

        if (ddtGenerati != null) {
            setDdtGenerati(ddtGenerati);
        }

        Boolean stampaEtichette = (Boolean) attributes.get("stampaEtichette");

        if (stampaEtichette != null) {
            setStampaEtichette(stampaEtichette);
        }

        String numOrdineGT = (String) attributes.get("numOrdineGT");

        if (numOrdineGT != null) {
            setNumOrdineGT(numOrdineGT);
        }

        Date dataOrdineGT = (Date) attributes.get("dataOrdineGT");

        if (dataOrdineGT != null) {
            setDataOrdineGT(dataOrdineGT);
        }
    }

    /**
    * Returns the primary key of this ordine.
    *
    * @return the primary key of this ordine
    */
    @Override
    public long getPrimaryKey() {
        return _ordine.getPrimaryKey();
    }

    /**
    * Sets the primary key of this ordine.
    *
    * @param primaryKey the primary key of this ordine
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _ordine.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this ordine.
    *
    * @return the ID of this ordine
    */
    @Override
    public long getId() {
        return _ordine.getId();
    }

    /**
    * Sets the ID of this ordine.
    *
    * @param id the ID of this ordine
    */
    @Override
    public void setId(long id) {
        _ordine.setId(id);
    }

    /**
    * Returns the data inserimento of this ordine.
    *
    * @return the data inserimento of this ordine
    */
    @Override
    public java.util.Date getDataInserimento() {
        return _ordine.getDataInserimento();
    }

    /**
    * Sets the data inserimento of this ordine.
    *
    * @param dataInserimento the data inserimento of this ordine
    */
    @Override
    public void setDataInserimento(java.util.Date dataInserimento) {
        _ordine.setDataInserimento(dataInserimento);
    }

    /**
    * Returns the id cliente of this ordine.
    *
    * @return the id cliente of this ordine
    */
    @Override
    public java.lang.String getIdCliente() {
        return _ordine.getIdCliente();
    }

    /**
    * Sets the id cliente of this ordine.
    *
    * @param idCliente the id cliente of this ordine
    */
    @Override
    public void setIdCliente(java.lang.String idCliente) {
        _ordine.setIdCliente(idCliente);
    }

    /**
    * Returns the stato of this ordine.
    *
    * @return the stato of this ordine
    */
    @Override
    public int getStato() {
        return _ordine.getStato();
    }

    /**
    * Sets the stato of this ordine.
    *
    * @param stato the stato of this ordine
    */
    @Override
    public void setStato(int stato) {
        _ordine.setStato(stato);
    }

    /**
    * Returns the importo of this ordine.
    *
    * @return the importo of this ordine
    */
    @Override
    public double getImporto() {
        return _ordine.getImporto();
    }

    /**
    * Sets the importo of this ordine.
    *
    * @param importo the importo of this ordine
    */
    @Override
    public void setImporto(double importo) {
        _ordine.setImporto(importo);
    }

    /**
    * Returns the note of this ordine.
    *
    * @return the note of this ordine
    */
    @Override
    public java.lang.String getNote() {
        return _ordine.getNote();
    }

    /**
    * Sets the note of this ordine.
    *
    * @param note the note of this ordine
    */
    @Override
    public void setNote(java.lang.String note) {
        _ordine.setNote(note);
    }

    /**
    * Returns the data consegna of this ordine.
    *
    * @return the data consegna of this ordine
    */
    @Override
    public java.util.Date getDataConsegna() {
        return _ordine.getDataConsegna();
    }

    /**
    * Sets the data consegna of this ordine.
    *
    * @param dataConsegna the data consegna of this ordine
    */
    @Override
    public void setDataConsegna(java.util.Date dataConsegna) {
        _ordine.setDataConsegna(dataConsegna);
    }

    /**
    * Returns the id trasportatore of this ordine.
    *
    * @return the id trasportatore of this ordine
    */
    @Override
    public java.lang.String getIdTrasportatore() {
        return _ordine.getIdTrasportatore();
    }

    /**
    * Sets the id trasportatore of this ordine.
    *
    * @param idTrasportatore the id trasportatore of this ordine
    */
    @Override
    public void setIdTrasportatore(java.lang.String idTrasportatore) {
        _ordine.setIdTrasportatore(idTrasportatore);
    }

    /**
    * Returns the id trasportatore2 of this ordine.
    *
    * @return the id trasportatore2 of this ordine
    */
    @Override
    public java.lang.String getIdTrasportatore2() {
        return _ordine.getIdTrasportatore2();
    }

    /**
    * Sets the id trasportatore2 of this ordine.
    *
    * @param idTrasportatore2 the id trasportatore2 of this ordine
    */
    @Override
    public void setIdTrasportatore2(java.lang.String idTrasportatore2) {
        _ordine.setIdTrasportatore2(idTrasportatore2);
    }

    /**
    * Returns the id agente of this ordine.
    *
    * @return the id agente of this ordine
    */
    @Override
    public long getIdAgente() {
        return _ordine.getIdAgente();
    }

    /**
    * Sets the id agente of this ordine.
    *
    * @param idAgente the id agente of this ordine
    */
    @Override
    public void setIdAgente(long idAgente) {
        _ordine.setIdAgente(idAgente);
    }

    /**
    * Returns the id tipo carrello of this ordine.
    *
    * @return the id tipo carrello of this ordine
    */
    @Override
    public long getIdTipoCarrello() {
        return _ordine.getIdTipoCarrello();
    }

    /**
    * Sets the id tipo carrello of this ordine.
    *
    * @param idTipoCarrello the id tipo carrello of this ordine
    */
    @Override
    public void setIdTipoCarrello(long idTipoCarrello) {
        _ordine.setIdTipoCarrello(idTipoCarrello);
    }

    /**
    * Returns the luogo carico of this ordine.
    *
    * @return the luogo carico of this ordine
    */
    @Override
    public java.lang.String getLuogoCarico() {
        return _ordine.getLuogoCarico();
    }

    /**
    * Sets the luogo carico of this ordine.
    *
    * @param luogoCarico the luogo carico of this ordine
    */
    @Override
    public void setLuogoCarico(java.lang.String luogoCarico) {
        _ordine.setLuogoCarico(luogoCarico);
    }

    /**
    * Returns the note cliente of this ordine.
    *
    * @return the note cliente of this ordine
    */
    @Override
    public java.lang.String getNoteCliente() {
        return _ordine.getNoteCliente();
    }

    /**
    * Sets the note cliente of this ordine.
    *
    * @param noteCliente the note cliente of this ordine
    */
    @Override
    public void setNoteCliente(java.lang.String noteCliente) {
        _ordine.setNoteCliente(noteCliente);
    }

    /**
    * Returns the anno of this ordine.
    *
    * @return the anno of this ordine
    */
    @Override
    public int getAnno() {
        return _ordine.getAnno();
    }

    /**
    * Sets the anno of this ordine.
    *
    * @param anno the anno of this ordine
    */
    @Override
    public void setAnno(int anno) {
        _ordine.setAnno(anno);
    }

    /**
    * Returns the numero of this ordine.
    *
    * @return the numero of this ordine
    */
    @Override
    public int getNumero() {
        return _ordine.getNumero();
    }

    /**
    * Sets the numero of this ordine.
    *
    * @param numero the numero of this ordine
    */
    @Override
    public void setNumero(int numero) {
        _ordine.setNumero(numero);
    }

    /**
    * Returns the centro of this ordine.
    *
    * @return the centro of this ordine
    */
    @Override
    public java.lang.String getCentro() {
        return _ordine.getCentro();
    }

    /**
    * Sets the centro of this ordine.
    *
    * @param centro the centro of this ordine
    */
    @Override
    public void setCentro(java.lang.String centro) {
        _ordine.setCentro(centro);
    }

    /**
    * Returns the modificato of this ordine.
    *
    * @return the modificato of this ordine
    */
    @Override
    public boolean getModificato() {
        return _ordine.getModificato();
    }

    /**
    * Returns <code>true</code> if this ordine is modificato.
    *
    * @return <code>true</code> if this ordine is modificato; <code>false</code> otherwise
    */
    @Override
    public boolean isModificato() {
        return _ordine.isModificato();
    }

    /**
    * Sets whether this ordine is modificato.
    *
    * @param modificato the modificato of this ordine
    */
    @Override
    public void setModificato(boolean modificato) {
        _ordine.setModificato(modificato);
    }

    /**
    * Returns the codice iva of this ordine.
    *
    * @return the codice iva of this ordine
    */
    @Override
    public java.lang.String getCodiceIva() {
        return _ordine.getCodiceIva();
    }

    /**
    * Sets the codice iva of this ordine.
    *
    * @param codiceIva the codice iva of this ordine
    */
    @Override
    public void setCodiceIva(java.lang.String codiceIva) {
        _ordine.setCodiceIva(codiceIva);
    }

    /**
    * Returns the cliente of this ordine.
    *
    * @return the cliente of this ordine
    */
    @Override
    public java.lang.String getCliente() {
        return _ordine.getCliente();
    }

    /**
    * Sets the cliente of this ordine.
    *
    * @param cliente the cliente of this ordine
    */
    @Override
    public void setCliente(java.lang.String cliente) {
        _ordine.setCliente(cliente);
    }

    /**
    * Returns the vettore of this ordine.
    *
    * @return the vettore of this ordine
    */
    @Override
    public java.lang.String getVettore() {
        return _ordine.getVettore();
    }

    /**
    * Sets the vettore of this ordine.
    *
    * @param vettore the vettore of this ordine
    */
    @Override
    public void setVettore(java.lang.String vettore) {
        _ordine.setVettore(vettore);
    }

    /**
    * Returns the pagamento of this ordine.
    *
    * @return the pagamento of this ordine
    */
    @Override
    public java.lang.String getPagamento() {
        return _ordine.getPagamento();
    }

    /**
    * Sets the pagamento of this ordine.
    *
    * @param pagamento the pagamento of this ordine
    */
    @Override
    public void setPagamento(java.lang.String pagamento) {
        _ordine.setPagamento(pagamento);
    }

    /**
    * Returns the ddt generati of this ordine.
    *
    * @return the ddt generati of this ordine
    */
    @Override
    public boolean getDdtGenerati() {
        return _ordine.getDdtGenerati();
    }

    /**
    * Returns <code>true</code> if this ordine is ddt generati.
    *
    * @return <code>true</code> if this ordine is ddt generati; <code>false</code> otherwise
    */
    @Override
    public boolean isDdtGenerati() {
        return _ordine.isDdtGenerati();
    }

    /**
    * Sets whether this ordine is ddt generati.
    *
    * @param ddtGenerati the ddt generati of this ordine
    */
    @Override
    public void setDdtGenerati(boolean ddtGenerati) {
        _ordine.setDdtGenerati(ddtGenerati);
    }

    /**
    * Returns the stampa etichette of this ordine.
    *
    * @return the stampa etichette of this ordine
    */
    @Override
    public boolean getStampaEtichette() {
        return _ordine.getStampaEtichette();
    }

    /**
    * Returns <code>true</code> if this ordine is stampa etichette.
    *
    * @return <code>true</code> if this ordine is stampa etichette; <code>false</code> otherwise
    */
    @Override
    public boolean isStampaEtichette() {
        return _ordine.isStampaEtichette();
    }

    /**
    * Sets whether this ordine is stampa etichette.
    *
    * @param stampaEtichette the stampa etichette of this ordine
    */
    @Override
    public void setStampaEtichette(boolean stampaEtichette) {
        _ordine.setStampaEtichette(stampaEtichette);
    }

    /**
    * Returns the num ordine g t of this ordine.
    *
    * @return the num ordine g t of this ordine
    */
    @Override
    public java.lang.String getNumOrdineGT() {
        return _ordine.getNumOrdineGT();
    }

    /**
    * Sets the num ordine g t of this ordine.
    *
    * @param numOrdineGT the num ordine g t of this ordine
    */
    @Override
    public void setNumOrdineGT(java.lang.String numOrdineGT) {
        _ordine.setNumOrdineGT(numOrdineGT);
    }

    /**
    * Returns the data ordine g t of this ordine.
    *
    * @return the data ordine g t of this ordine
    */
    @Override
    public java.util.Date getDataOrdineGT() {
        return _ordine.getDataOrdineGT();
    }

    /**
    * Sets the data ordine g t of this ordine.
    *
    * @param dataOrdineGT the data ordine g t of this ordine
    */
    @Override
    public void setDataOrdineGT(java.util.Date dataOrdineGT) {
        _ordine.setDataOrdineGT(dataOrdineGT);
    }

    @Override
    public boolean isNew() {
        return _ordine.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _ordine.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _ordine.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _ordine.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _ordine.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _ordine.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _ordine.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _ordine.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _ordine.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _ordine.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _ordine.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new OrdineWrapper((Ordine) _ordine.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.Ordine ordine) {
        return _ordine.compareTo(ordine);
    }

    @Override
    public int hashCode() {
        return _ordine.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.Ordine> toCacheModel() {
        return _ordine.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.Ordine toEscapedModel() {
        return new OrdineWrapper(_ordine.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.Ordine toUnescapedModel() {
        return new OrdineWrapper(_ordine.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _ordine.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _ordine.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _ordine.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof OrdineWrapper)) {
            return false;
        }

        OrdineWrapper ordineWrapper = (OrdineWrapper) obj;

        if (Validator.equals(_ordine, ordineWrapper._ordine)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Ordine getWrappedOrdine() {
        return _ordine;
    }

    @Override
    public Ordine getWrappedModel() {
        return _ordine;
    }

    @Override
    public void resetOriginalValues() {
        _ordine.resetOriginalValues();
    }
}
