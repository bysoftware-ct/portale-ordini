package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Centro}.
 * </p>
 *
 * @author Mario Torrisi
 * @see Centro
 * @generated
 */
public class CentroWrapper implements Centro, ModelWrapper<Centro> {
    private Centro _centro;

    public CentroWrapper(Centro centro) {
        _centro = centro;
    }

    @Override
    public Class<?> getModelClass() {
        return Centro.class;
    }

    @Override
    public String getModelClassName() {
        return Centro.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("attivita", getAttivita());
        attributes.put("centro", getCentro());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String attivita = (String) attributes.get("attivita");

        if (attivita != null) {
            setAttivita(attivita);
        }

        String centro = (String) attributes.get("centro");

        if (centro != null) {
            setCentro(centro);
        }
    }

    /**
    * Returns the primary key of this centro.
    *
    * @return the primary key of this centro
    */
    @Override
    public it.bysoftware.ct.service.persistence.CentroPK getPrimaryKey() {
        return _centro.getPrimaryKey();
    }

    /**
    * Sets the primary key of this centro.
    *
    * @param primaryKey the primary key of this centro
    */
    @Override
    public void setPrimaryKey(
        it.bysoftware.ct.service.persistence.CentroPK primaryKey) {
        _centro.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the attivita of this centro.
    *
    * @return the attivita of this centro
    */
    @Override
    public java.lang.String getAttivita() {
        return _centro.getAttivita();
    }

    /**
    * Sets the attivita of this centro.
    *
    * @param attivita the attivita of this centro
    */
    @Override
    public void setAttivita(java.lang.String attivita) {
        _centro.setAttivita(attivita);
    }

    /**
    * Returns the centro of this centro.
    *
    * @return the centro of this centro
    */
    @Override
    public java.lang.String getCentro() {
        return _centro.getCentro();
    }

    /**
    * Sets the centro of this centro.
    *
    * @param centro the centro of this centro
    */
    @Override
    public void setCentro(java.lang.String centro) {
        _centro.setCentro(centro);
    }

    @Override
    public boolean isNew() {
        return _centro.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _centro.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _centro.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _centro.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _centro.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _centro.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _centro.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _centro.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _centro.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _centro.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _centro.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new CentroWrapper((Centro) _centro.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.Centro centro) {
        return _centro.compareTo(centro);
    }

    @Override
    public int hashCode() {
        return _centro.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.Centro> toCacheModel() {
        return _centro.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.Centro toEscapedModel() {
        return new CentroWrapper(_centro.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.Centro toUnescapedModel() {
        return new CentroWrapper(_centro.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _centro.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _centro.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _centro.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CentroWrapper)) {
            return false;
        }

        CentroWrapper centroWrapper = (CentroWrapper) obj;

        if (Validator.equals(_centro, centroWrapper._centro)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Centro getWrappedCentro() {
        return _centro;
    }

    @Override
    public Centro getWrappedModel() {
        return _centro;
    }

    @Override
    public void resetOriginalValues() {
        _centro.resetOriginalValues();
    }
}
