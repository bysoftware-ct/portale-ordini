package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.DescrizioniVariantiPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.DescrizioniVariantiServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.DescrizioniVariantiServiceSoap
 * @generated
 */
public class DescrizioniVariantiSoap implements Serializable {
    private String _codiceArticolo;
    private String _codiceVariante;
    private String _descrizione;

    public DescrizioniVariantiSoap() {
    }

    public static DescrizioniVariantiSoap toSoapModel(DescrizioniVarianti model) {
        DescrizioniVariantiSoap soapModel = new DescrizioniVariantiSoap();

        soapModel.setCodiceArticolo(model.getCodiceArticolo());
        soapModel.setCodiceVariante(model.getCodiceVariante());
        soapModel.setDescrizione(model.getDescrizione());

        return soapModel;
    }

    public static DescrizioniVariantiSoap[] toSoapModels(
        DescrizioniVarianti[] models) {
        DescrizioniVariantiSoap[] soapModels = new DescrizioniVariantiSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static DescrizioniVariantiSoap[][] toSoapModels(
        DescrizioniVarianti[][] models) {
        DescrizioniVariantiSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new DescrizioniVariantiSoap[models.length][models[0].length];
        } else {
            soapModels = new DescrizioniVariantiSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static DescrizioniVariantiSoap[] toSoapModels(
        List<DescrizioniVarianti> models) {
        List<DescrizioniVariantiSoap> soapModels = new ArrayList<DescrizioniVariantiSoap>(models.size());

        for (DescrizioniVarianti model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new DescrizioniVariantiSoap[soapModels.size()]);
    }

    public DescrizioniVariantiPK getPrimaryKey() {
        return new DescrizioniVariantiPK(_codiceArticolo, _codiceVariante);
    }

    public void setPrimaryKey(DescrizioniVariantiPK pk) {
        setCodiceArticolo(pk.codiceArticolo);
        setCodiceVariante(pk.codiceVariante);
    }

    public String getCodiceArticolo() {
        return _codiceArticolo;
    }

    public void setCodiceArticolo(String codiceArticolo) {
        _codiceArticolo = codiceArticolo;
    }

    public String getCodiceVariante() {
        return _codiceVariante;
    }

    public void setCodiceVariante(String codiceVariante) {
        _codiceVariante = codiceVariante;
    }

    public String getDescrizione() {
        return _descrizione;
    }

    public void setDescrizione(String descrizione) {
        _descrizione = descrizione;
    }
}
