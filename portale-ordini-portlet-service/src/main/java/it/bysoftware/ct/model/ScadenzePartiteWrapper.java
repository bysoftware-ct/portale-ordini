package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ScadenzePartite}.
 * </p>
 *
 * @author Mario Torrisi
 * @see ScadenzePartite
 * @generated
 */
public class ScadenzePartiteWrapper implements ScadenzePartite,
    ModelWrapper<ScadenzePartite> {
    private ScadenzePartite _scadenzePartite;

    public ScadenzePartiteWrapper(ScadenzePartite scadenzePartite) {
        _scadenzePartite = scadenzePartite;
    }

    @Override
    public Class<?> getModelClass() {
        return ScadenzePartite.class;
    }

    @Override
    public String getModelClassName() {
        return ScadenzePartite.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("tipoSoggetto", getTipoSoggetto());
        attributes.put("codiceSoggetto", getCodiceSoggetto());
        attributes.put("esercizioRegistrazione", getEsercizioRegistrazione());
        attributes.put("numeroPartita", getNumeroPartita());
        attributes.put("numeroScadenza", getNumeroScadenza());
        attributes.put("dataScadenza", getDataScadenza());
        attributes.put("codiceTipoPagam", getCodiceTipoPagam());
        attributes.put("stato", getStato());
        attributes.put("codiceBanca", getCodiceBanca());
        attributes.put("codiceContoCorrente", getCodiceContoCorrente());
        attributes.put("codiceAgente", getCodiceAgente());
        attributes.put("tipoCausale", getTipoCausale());
        attributes.put("esercizioDocumento", getEsercizioDocumento());
        attributes.put("protocolloDocumento", getProtocolloDocumento());
        attributes.put("codiceAttivita", getCodiceAttivita());
        attributes.put("codiceCentro", getCodiceCentro());
        attributes.put("dataRegistrazione", getDataRegistrazione());
        attributes.put("dataOperazione", getDataOperazione());
        attributes.put("dataAggiornamento", getDataAggiornamento());
        attributes.put("dataChiusura", getDataChiusura());
        attributes.put("codiceCausale", getCodiceCausale());
        attributes.put("dataDocumento", getDataDocumento());
        attributes.put("numeroDocumento", getNumeroDocumento());
        attributes.put("codiceTipoPagamOrig", getCodiceTipoPagamOrig());
        attributes.put("codiceTipoPagamScad", getCodiceTipoPagamScad());
        attributes.put("causaleEstrattoContoOrig", getCausaleEstrattoContoOrig());
        attributes.put("codiceDivisaOrig", getCodiceDivisaOrig());
        attributes.put("importoTotaleInt", getImportoTotaleInt());
        attributes.put("importoPagatoInt", getImportoPagatoInt());
        attributes.put("importoApertoInt", getImportoApertoInt());
        attributes.put("importoEspostoInt", getImportoEspostoInt());
        attributes.put("importoScadutoInt", getImportoScadutoInt());
        attributes.put("importoInsolutoInt", getImportoInsolutoInt());
        attributes.put("importoInContenziosoInt", getImportoInContenziosoInt());
        attributes.put("importoTotale", getImportoTotale());
        attributes.put("importoPagato", getImportoPagato());
        attributes.put("importoAperto", getImportoAperto());
        attributes.put("importoEsposto", getImportoEsposto());
        attributes.put("importoScaduto", getImportoScaduto());
        attributes.put("importoInsoluto", getImportoInsoluto());
        attributes.put("importoInContenzioso", getImportoInContenzioso());
        attributes.put("numeroLettereSollecito", getNumeroLettereSollecito());
        attributes.put("codiceDivisaInterna", getCodiceDivisaInterna());
        attributes.put("codiceEsercizioScadenzaConversione",
            getCodiceEsercizioScadenzaConversione());
        attributes.put("numeroPartitaScadenzaCoversione",
            getNumeroPartitaScadenzaCoversione());
        attributes.put("numeroScadenzaCoversione", getNumeroScadenzaCoversione());
        attributes.put("tipoCoversione", getTipoCoversione());
        attributes.put("codiceEsercizioScritturaConversione",
            getCodiceEsercizioScritturaConversione());
        attributes.put("numeroRegistrazioneScritturaConversione",
            getNumeroRegistrazioneScritturaConversione());
        attributes.put("dataRegistrazioneScritturaConversione",
            getDataRegistrazioneScritturaConversione());
        attributes.put("codiceDivisaIntScadenzaConversione",
            getCodiceDivisaIntScadenzaConversione());
        attributes.put("codiceDivisaScadenzaConversione",
            getCodiceDivisaScadenzaConversione());
        attributes.put("cambioScadenzaConversione",
            getCambioScadenzaConversione());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Boolean tipoSoggetto = (Boolean) attributes.get("tipoSoggetto");

        if (tipoSoggetto != null) {
            setTipoSoggetto(tipoSoggetto);
        }

        String codiceSoggetto = (String) attributes.get("codiceSoggetto");

        if (codiceSoggetto != null) {
            setCodiceSoggetto(codiceSoggetto);
        }

        Integer esercizioRegistrazione = (Integer) attributes.get(
                "esercizioRegistrazione");

        if (esercizioRegistrazione != null) {
            setEsercizioRegistrazione(esercizioRegistrazione);
        }

        Integer numeroPartita = (Integer) attributes.get("numeroPartita");

        if (numeroPartita != null) {
            setNumeroPartita(numeroPartita);
        }

        Integer numeroScadenza = (Integer) attributes.get("numeroScadenza");

        if (numeroScadenza != null) {
            setNumeroScadenza(numeroScadenza);
        }

        Date dataScadenza = (Date) attributes.get("dataScadenza");

        if (dataScadenza != null) {
            setDataScadenza(dataScadenza);
        }

        Integer codiceTipoPagam = (Integer) attributes.get("codiceTipoPagam");

        if (codiceTipoPagam != null) {
            setCodiceTipoPagam(codiceTipoPagam);
        }

        Integer stato = (Integer) attributes.get("stato");

        if (stato != null) {
            setStato(stato);
        }

        String codiceBanca = (String) attributes.get("codiceBanca");

        if (codiceBanca != null) {
            setCodiceBanca(codiceBanca);
        }

        String codiceContoCorrente = (String) attributes.get(
                "codiceContoCorrente");

        if (codiceContoCorrente != null) {
            setCodiceContoCorrente(codiceContoCorrente);
        }

        String codiceAgente = (String) attributes.get("codiceAgente");

        if (codiceAgente != null) {
            setCodiceAgente(codiceAgente);
        }

        String tipoCausale = (String) attributes.get("tipoCausale");

        if (tipoCausale != null) {
            setTipoCausale(tipoCausale);
        }

        Integer esercizioDocumento = (Integer) attributes.get(
                "esercizioDocumento");

        if (esercizioDocumento != null) {
            setEsercizioDocumento(esercizioDocumento);
        }

        Integer protocolloDocumento = (Integer) attributes.get(
                "protocolloDocumento");

        if (protocolloDocumento != null) {
            setProtocolloDocumento(protocolloDocumento);
        }

        String codiceAttivita = (String) attributes.get("codiceAttivita");

        if (codiceAttivita != null) {
            setCodiceAttivita(codiceAttivita);
        }

        String codiceCentro = (String) attributes.get("codiceCentro");

        if (codiceCentro != null) {
            setCodiceCentro(codiceCentro);
        }

        Date dataRegistrazione = (Date) attributes.get("dataRegistrazione");

        if (dataRegistrazione != null) {
            setDataRegistrazione(dataRegistrazione);
        }

        Date dataOperazione = (Date) attributes.get("dataOperazione");

        if (dataOperazione != null) {
            setDataOperazione(dataOperazione);
        }

        Date dataAggiornamento = (Date) attributes.get("dataAggiornamento");

        if (dataAggiornamento != null) {
            setDataAggiornamento(dataAggiornamento);
        }

        Date dataChiusura = (Date) attributes.get("dataChiusura");

        if (dataChiusura != null) {
            setDataChiusura(dataChiusura);
        }

        String codiceCausale = (String) attributes.get("codiceCausale");

        if (codiceCausale != null) {
            setCodiceCausale(codiceCausale);
        }

        Date dataDocumento = (Date) attributes.get("dataDocumento");

        if (dataDocumento != null) {
            setDataDocumento(dataDocumento);
        }

        Integer numeroDocumento = (Integer) attributes.get("numeroDocumento");

        if (numeroDocumento != null) {
            setNumeroDocumento(numeroDocumento);
        }

        String codiceTipoPagamOrig = (String) attributes.get(
                "codiceTipoPagamOrig");

        if (codiceTipoPagamOrig != null) {
            setCodiceTipoPagamOrig(codiceTipoPagamOrig);
        }

        String codiceTipoPagamScad = (String) attributes.get(
                "codiceTipoPagamScad");

        if (codiceTipoPagamScad != null) {
            setCodiceTipoPagamScad(codiceTipoPagamScad);
        }

        String causaleEstrattoContoOrig = (String) attributes.get(
                "causaleEstrattoContoOrig");

        if (causaleEstrattoContoOrig != null) {
            setCausaleEstrattoContoOrig(causaleEstrattoContoOrig);
        }

        String codiceDivisaOrig = (String) attributes.get("codiceDivisaOrig");

        if (codiceDivisaOrig != null) {
            setCodiceDivisaOrig(codiceDivisaOrig);
        }

        Double importoTotaleInt = (Double) attributes.get("importoTotaleInt");

        if (importoTotaleInt != null) {
            setImportoTotaleInt(importoTotaleInt);
        }

        Double importoPagatoInt = (Double) attributes.get("importoPagatoInt");

        if (importoPagatoInt != null) {
            setImportoPagatoInt(importoPagatoInt);
        }

        Double importoApertoInt = (Double) attributes.get("importoApertoInt");

        if (importoApertoInt != null) {
            setImportoApertoInt(importoApertoInt);
        }

        Double importoEspostoInt = (Double) attributes.get("importoEspostoInt");

        if (importoEspostoInt != null) {
            setImportoEspostoInt(importoEspostoInt);
        }

        Double importoScadutoInt = (Double) attributes.get("importoScadutoInt");

        if (importoScadutoInt != null) {
            setImportoScadutoInt(importoScadutoInt);
        }

        Double importoInsolutoInt = (Double) attributes.get(
                "importoInsolutoInt");

        if (importoInsolutoInt != null) {
            setImportoInsolutoInt(importoInsolutoInt);
        }

        Double importoInContenziosoInt = (Double) attributes.get(
                "importoInContenziosoInt");

        if (importoInContenziosoInt != null) {
            setImportoInContenziosoInt(importoInContenziosoInt);
        }

        Double importoTotale = (Double) attributes.get("importoTotale");

        if (importoTotale != null) {
            setImportoTotale(importoTotale);
        }

        Double importoPagato = (Double) attributes.get("importoPagato");

        if (importoPagato != null) {
            setImportoPagato(importoPagato);
        }

        Double importoAperto = (Double) attributes.get("importoAperto");

        if (importoAperto != null) {
            setImportoAperto(importoAperto);
        }

        Double importoEsposto = (Double) attributes.get("importoEsposto");

        if (importoEsposto != null) {
            setImportoEsposto(importoEsposto);
        }

        Double importoScaduto = (Double) attributes.get("importoScaduto");

        if (importoScaduto != null) {
            setImportoScaduto(importoScaduto);
        }

        Double importoInsoluto = (Double) attributes.get("importoInsoluto");

        if (importoInsoluto != null) {
            setImportoInsoluto(importoInsoluto);
        }

        Double importoInContenzioso = (Double) attributes.get(
                "importoInContenzioso");

        if (importoInContenzioso != null) {
            setImportoInContenzioso(importoInContenzioso);
        }

        Integer numeroLettereSollecito = (Integer) attributes.get(
                "numeroLettereSollecito");

        if (numeroLettereSollecito != null) {
            setNumeroLettereSollecito(numeroLettereSollecito);
        }

        String codiceDivisaInterna = (String) attributes.get(
                "codiceDivisaInterna");

        if (codiceDivisaInterna != null) {
            setCodiceDivisaInterna(codiceDivisaInterna);
        }

        Integer codiceEsercizioScadenzaConversione = (Integer) attributes.get(
                "codiceEsercizioScadenzaConversione");

        if (codiceEsercizioScadenzaConversione != null) {
            setCodiceEsercizioScadenzaConversione(codiceEsercizioScadenzaConversione);
        }

        Integer numeroPartitaScadenzaCoversione = (Integer) attributes.get(
                "numeroPartitaScadenzaCoversione");

        if (numeroPartitaScadenzaCoversione != null) {
            setNumeroPartitaScadenzaCoversione(numeroPartitaScadenzaCoversione);
        }

        Integer numeroScadenzaCoversione = (Integer) attributes.get(
                "numeroScadenzaCoversione");

        if (numeroScadenzaCoversione != null) {
            setNumeroScadenzaCoversione(numeroScadenzaCoversione);
        }

        Integer tipoCoversione = (Integer) attributes.get("tipoCoversione");

        if (tipoCoversione != null) {
            setTipoCoversione(tipoCoversione);
        }

        Integer codiceEsercizioScritturaConversione = (Integer) attributes.get(
                "codiceEsercizioScritturaConversione");

        if (codiceEsercizioScritturaConversione != null) {
            setCodiceEsercizioScritturaConversione(codiceEsercizioScritturaConversione);
        }

        Integer numeroRegistrazioneScritturaConversione = (Integer) attributes.get(
                "numeroRegistrazioneScritturaConversione");

        if (numeroRegistrazioneScritturaConversione != null) {
            setNumeroRegistrazioneScritturaConversione(numeroRegistrazioneScritturaConversione);
        }

        Date dataRegistrazioneScritturaConversione = (Date) attributes.get(
                "dataRegistrazioneScritturaConversione");

        if (dataRegistrazioneScritturaConversione != null) {
            setDataRegistrazioneScritturaConversione(dataRegistrazioneScritturaConversione);
        }

        String codiceDivisaIntScadenzaConversione = (String) attributes.get(
                "codiceDivisaIntScadenzaConversione");

        if (codiceDivisaIntScadenzaConversione != null) {
            setCodiceDivisaIntScadenzaConversione(codiceDivisaIntScadenzaConversione);
        }

        String codiceDivisaScadenzaConversione = (String) attributes.get(
                "codiceDivisaScadenzaConversione");

        if (codiceDivisaScadenzaConversione != null) {
            setCodiceDivisaScadenzaConversione(codiceDivisaScadenzaConversione);
        }

        Double cambioScadenzaConversione = (Double) attributes.get(
                "cambioScadenzaConversione");

        if (cambioScadenzaConversione != null) {
            setCambioScadenzaConversione(cambioScadenzaConversione);
        }
    }

    /**
    * Returns the primary key of this scadenze partite.
    *
    * @return the primary key of this scadenze partite
    */
    @Override
    public it.bysoftware.ct.service.persistence.ScadenzePartitePK getPrimaryKey() {
        return _scadenzePartite.getPrimaryKey();
    }

    /**
    * Sets the primary key of this scadenze partite.
    *
    * @param primaryKey the primary key of this scadenze partite
    */
    @Override
    public void setPrimaryKey(
        it.bysoftware.ct.service.persistence.ScadenzePartitePK primaryKey) {
        _scadenzePartite.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the tipo soggetto of this scadenze partite.
    *
    * @return the tipo soggetto of this scadenze partite
    */
    @Override
    public boolean getTipoSoggetto() {
        return _scadenzePartite.getTipoSoggetto();
    }

    /**
    * Returns <code>true</code> if this scadenze partite is tipo soggetto.
    *
    * @return <code>true</code> if this scadenze partite is tipo soggetto; <code>false</code> otherwise
    */
    @Override
    public boolean isTipoSoggetto() {
        return _scadenzePartite.isTipoSoggetto();
    }

    /**
    * Sets whether this scadenze partite is tipo soggetto.
    *
    * @param tipoSoggetto the tipo soggetto of this scadenze partite
    */
    @Override
    public void setTipoSoggetto(boolean tipoSoggetto) {
        _scadenzePartite.setTipoSoggetto(tipoSoggetto);
    }

    /**
    * Returns the codice soggetto of this scadenze partite.
    *
    * @return the codice soggetto of this scadenze partite
    */
    @Override
    public java.lang.String getCodiceSoggetto() {
        return _scadenzePartite.getCodiceSoggetto();
    }

    /**
    * Sets the codice soggetto of this scadenze partite.
    *
    * @param codiceSoggetto the codice soggetto of this scadenze partite
    */
    @Override
    public void setCodiceSoggetto(java.lang.String codiceSoggetto) {
        _scadenzePartite.setCodiceSoggetto(codiceSoggetto);
    }

    /**
    * Returns the esercizio registrazione of this scadenze partite.
    *
    * @return the esercizio registrazione of this scadenze partite
    */
    @Override
    public int getEsercizioRegistrazione() {
        return _scadenzePartite.getEsercizioRegistrazione();
    }

    /**
    * Sets the esercizio registrazione of this scadenze partite.
    *
    * @param esercizioRegistrazione the esercizio registrazione of this scadenze partite
    */
    @Override
    public void setEsercizioRegistrazione(int esercizioRegistrazione) {
        _scadenzePartite.setEsercizioRegistrazione(esercizioRegistrazione);
    }

    /**
    * Returns the numero partita of this scadenze partite.
    *
    * @return the numero partita of this scadenze partite
    */
    @Override
    public int getNumeroPartita() {
        return _scadenzePartite.getNumeroPartita();
    }

    /**
    * Sets the numero partita of this scadenze partite.
    *
    * @param numeroPartita the numero partita of this scadenze partite
    */
    @Override
    public void setNumeroPartita(int numeroPartita) {
        _scadenzePartite.setNumeroPartita(numeroPartita);
    }

    /**
    * Returns the numero scadenza of this scadenze partite.
    *
    * @return the numero scadenza of this scadenze partite
    */
    @Override
    public int getNumeroScadenza() {
        return _scadenzePartite.getNumeroScadenza();
    }

    /**
    * Sets the numero scadenza of this scadenze partite.
    *
    * @param numeroScadenza the numero scadenza of this scadenze partite
    */
    @Override
    public void setNumeroScadenza(int numeroScadenza) {
        _scadenzePartite.setNumeroScadenza(numeroScadenza);
    }

    /**
    * Returns the data scadenza of this scadenze partite.
    *
    * @return the data scadenza of this scadenze partite
    */
    @Override
    public java.util.Date getDataScadenza() {
        return _scadenzePartite.getDataScadenza();
    }

    /**
    * Sets the data scadenza of this scadenze partite.
    *
    * @param dataScadenza the data scadenza of this scadenze partite
    */
    @Override
    public void setDataScadenza(java.util.Date dataScadenza) {
        _scadenzePartite.setDataScadenza(dataScadenza);
    }

    /**
    * Returns the codice tipo pagam of this scadenze partite.
    *
    * @return the codice tipo pagam of this scadenze partite
    */
    @Override
    public int getCodiceTipoPagam() {
        return _scadenzePartite.getCodiceTipoPagam();
    }

    /**
    * Sets the codice tipo pagam of this scadenze partite.
    *
    * @param codiceTipoPagam the codice tipo pagam of this scadenze partite
    */
    @Override
    public void setCodiceTipoPagam(int codiceTipoPagam) {
        _scadenzePartite.setCodiceTipoPagam(codiceTipoPagam);
    }

    /**
    * Returns the stato of this scadenze partite.
    *
    * @return the stato of this scadenze partite
    */
    @Override
    public int getStato() {
        return _scadenzePartite.getStato();
    }

    /**
    * Sets the stato of this scadenze partite.
    *
    * @param stato the stato of this scadenze partite
    */
    @Override
    public void setStato(int stato) {
        _scadenzePartite.setStato(stato);
    }

    /**
    * Returns the codice banca of this scadenze partite.
    *
    * @return the codice banca of this scadenze partite
    */
    @Override
    public java.lang.String getCodiceBanca() {
        return _scadenzePartite.getCodiceBanca();
    }

    /**
    * Sets the codice banca of this scadenze partite.
    *
    * @param codiceBanca the codice banca of this scadenze partite
    */
    @Override
    public void setCodiceBanca(java.lang.String codiceBanca) {
        _scadenzePartite.setCodiceBanca(codiceBanca);
    }

    /**
    * Returns the codice conto corrente of this scadenze partite.
    *
    * @return the codice conto corrente of this scadenze partite
    */
    @Override
    public java.lang.String getCodiceContoCorrente() {
        return _scadenzePartite.getCodiceContoCorrente();
    }

    /**
    * Sets the codice conto corrente of this scadenze partite.
    *
    * @param codiceContoCorrente the codice conto corrente of this scadenze partite
    */
    @Override
    public void setCodiceContoCorrente(java.lang.String codiceContoCorrente) {
        _scadenzePartite.setCodiceContoCorrente(codiceContoCorrente);
    }

    /**
    * Returns the codice agente of this scadenze partite.
    *
    * @return the codice agente of this scadenze partite
    */
    @Override
    public java.lang.String getCodiceAgente() {
        return _scadenzePartite.getCodiceAgente();
    }

    /**
    * Sets the codice agente of this scadenze partite.
    *
    * @param codiceAgente the codice agente of this scadenze partite
    */
    @Override
    public void setCodiceAgente(java.lang.String codiceAgente) {
        _scadenzePartite.setCodiceAgente(codiceAgente);
    }

    /**
    * Returns the tipo causale of this scadenze partite.
    *
    * @return the tipo causale of this scadenze partite
    */
    @Override
    public java.lang.String getTipoCausale() {
        return _scadenzePartite.getTipoCausale();
    }

    /**
    * Sets the tipo causale of this scadenze partite.
    *
    * @param tipoCausale the tipo causale of this scadenze partite
    */
    @Override
    public void setTipoCausale(java.lang.String tipoCausale) {
        _scadenzePartite.setTipoCausale(tipoCausale);
    }

    /**
    * Returns the esercizio documento of this scadenze partite.
    *
    * @return the esercizio documento of this scadenze partite
    */
    @Override
    public int getEsercizioDocumento() {
        return _scadenzePartite.getEsercizioDocumento();
    }

    /**
    * Sets the esercizio documento of this scadenze partite.
    *
    * @param esercizioDocumento the esercizio documento of this scadenze partite
    */
    @Override
    public void setEsercizioDocumento(int esercizioDocumento) {
        _scadenzePartite.setEsercizioDocumento(esercizioDocumento);
    }

    /**
    * Returns the protocollo documento of this scadenze partite.
    *
    * @return the protocollo documento of this scadenze partite
    */
    @Override
    public int getProtocolloDocumento() {
        return _scadenzePartite.getProtocolloDocumento();
    }

    /**
    * Sets the protocollo documento of this scadenze partite.
    *
    * @param protocolloDocumento the protocollo documento of this scadenze partite
    */
    @Override
    public void setProtocolloDocumento(int protocolloDocumento) {
        _scadenzePartite.setProtocolloDocumento(protocolloDocumento);
    }

    /**
    * Returns the codice attivita of this scadenze partite.
    *
    * @return the codice attivita of this scadenze partite
    */
    @Override
    public java.lang.String getCodiceAttivita() {
        return _scadenzePartite.getCodiceAttivita();
    }

    /**
    * Sets the codice attivita of this scadenze partite.
    *
    * @param codiceAttivita the codice attivita of this scadenze partite
    */
    @Override
    public void setCodiceAttivita(java.lang.String codiceAttivita) {
        _scadenzePartite.setCodiceAttivita(codiceAttivita);
    }

    /**
    * Returns the codice centro of this scadenze partite.
    *
    * @return the codice centro of this scadenze partite
    */
    @Override
    public java.lang.String getCodiceCentro() {
        return _scadenzePartite.getCodiceCentro();
    }

    /**
    * Sets the codice centro of this scadenze partite.
    *
    * @param codiceCentro the codice centro of this scadenze partite
    */
    @Override
    public void setCodiceCentro(java.lang.String codiceCentro) {
        _scadenzePartite.setCodiceCentro(codiceCentro);
    }

    /**
    * Returns the data registrazione of this scadenze partite.
    *
    * @return the data registrazione of this scadenze partite
    */
    @Override
    public java.util.Date getDataRegistrazione() {
        return _scadenzePartite.getDataRegistrazione();
    }

    /**
    * Sets the data registrazione of this scadenze partite.
    *
    * @param dataRegistrazione the data registrazione of this scadenze partite
    */
    @Override
    public void setDataRegistrazione(java.util.Date dataRegistrazione) {
        _scadenzePartite.setDataRegistrazione(dataRegistrazione);
    }

    /**
    * Returns the data operazione of this scadenze partite.
    *
    * @return the data operazione of this scadenze partite
    */
    @Override
    public java.util.Date getDataOperazione() {
        return _scadenzePartite.getDataOperazione();
    }

    /**
    * Sets the data operazione of this scadenze partite.
    *
    * @param dataOperazione the data operazione of this scadenze partite
    */
    @Override
    public void setDataOperazione(java.util.Date dataOperazione) {
        _scadenzePartite.setDataOperazione(dataOperazione);
    }

    /**
    * Returns the data aggiornamento of this scadenze partite.
    *
    * @return the data aggiornamento of this scadenze partite
    */
    @Override
    public java.util.Date getDataAggiornamento() {
        return _scadenzePartite.getDataAggiornamento();
    }

    /**
    * Sets the data aggiornamento of this scadenze partite.
    *
    * @param dataAggiornamento the data aggiornamento of this scadenze partite
    */
    @Override
    public void setDataAggiornamento(java.util.Date dataAggiornamento) {
        _scadenzePartite.setDataAggiornamento(dataAggiornamento);
    }

    /**
    * Returns the data chiusura of this scadenze partite.
    *
    * @return the data chiusura of this scadenze partite
    */
    @Override
    public java.util.Date getDataChiusura() {
        return _scadenzePartite.getDataChiusura();
    }

    /**
    * Sets the data chiusura of this scadenze partite.
    *
    * @param dataChiusura the data chiusura of this scadenze partite
    */
    @Override
    public void setDataChiusura(java.util.Date dataChiusura) {
        _scadenzePartite.setDataChiusura(dataChiusura);
    }

    /**
    * Returns the codice causale of this scadenze partite.
    *
    * @return the codice causale of this scadenze partite
    */
    @Override
    public java.lang.String getCodiceCausale() {
        return _scadenzePartite.getCodiceCausale();
    }

    /**
    * Sets the codice causale of this scadenze partite.
    *
    * @param codiceCausale the codice causale of this scadenze partite
    */
    @Override
    public void setCodiceCausale(java.lang.String codiceCausale) {
        _scadenzePartite.setCodiceCausale(codiceCausale);
    }

    /**
    * Returns the data documento of this scadenze partite.
    *
    * @return the data documento of this scadenze partite
    */
    @Override
    public java.util.Date getDataDocumento() {
        return _scadenzePartite.getDataDocumento();
    }

    /**
    * Sets the data documento of this scadenze partite.
    *
    * @param dataDocumento the data documento of this scadenze partite
    */
    @Override
    public void setDataDocumento(java.util.Date dataDocumento) {
        _scadenzePartite.setDataDocumento(dataDocumento);
    }

    /**
    * Returns the numero documento of this scadenze partite.
    *
    * @return the numero documento of this scadenze partite
    */
    @Override
    public int getNumeroDocumento() {
        return _scadenzePartite.getNumeroDocumento();
    }

    /**
    * Sets the numero documento of this scadenze partite.
    *
    * @param numeroDocumento the numero documento of this scadenze partite
    */
    @Override
    public void setNumeroDocumento(int numeroDocumento) {
        _scadenzePartite.setNumeroDocumento(numeroDocumento);
    }

    /**
    * Returns the codice tipo pagam orig of this scadenze partite.
    *
    * @return the codice tipo pagam orig of this scadenze partite
    */
    @Override
    public java.lang.String getCodiceTipoPagamOrig() {
        return _scadenzePartite.getCodiceTipoPagamOrig();
    }

    /**
    * Sets the codice tipo pagam orig of this scadenze partite.
    *
    * @param codiceTipoPagamOrig the codice tipo pagam orig of this scadenze partite
    */
    @Override
    public void setCodiceTipoPagamOrig(java.lang.String codiceTipoPagamOrig) {
        _scadenzePartite.setCodiceTipoPagamOrig(codiceTipoPagamOrig);
    }

    /**
    * Returns the codice tipo pagam scad of this scadenze partite.
    *
    * @return the codice tipo pagam scad of this scadenze partite
    */
    @Override
    public java.lang.String getCodiceTipoPagamScad() {
        return _scadenzePartite.getCodiceTipoPagamScad();
    }

    /**
    * Sets the codice tipo pagam scad of this scadenze partite.
    *
    * @param codiceTipoPagamScad the codice tipo pagam scad of this scadenze partite
    */
    @Override
    public void setCodiceTipoPagamScad(java.lang.String codiceTipoPagamScad) {
        _scadenzePartite.setCodiceTipoPagamScad(codiceTipoPagamScad);
    }

    /**
    * Returns the causale estratto conto orig of this scadenze partite.
    *
    * @return the causale estratto conto orig of this scadenze partite
    */
    @Override
    public java.lang.String getCausaleEstrattoContoOrig() {
        return _scadenzePartite.getCausaleEstrattoContoOrig();
    }

    /**
    * Sets the causale estratto conto orig of this scadenze partite.
    *
    * @param causaleEstrattoContoOrig the causale estratto conto orig of this scadenze partite
    */
    @Override
    public void setCausaleEstrattoContoOrig(
        java.lang.String causaleEstrattoContoOrig) {
        _scadenzePartite.setCausaleEstrattoContoOrig(causaleEstrattoContoOrig);
    }

    /**
    * Returns the codice divisa orig of this scadenze partite.
    *
    * @return the codice divisa orig of this scadenze partite
    */
    @Override
    public java.lang.String getCodiceDivisaOrig() {
        return _scadenzePartite.getCodiceDivisaOrig();
    }

    /**
    * Sets the codice divisa orig of this scadenze partite.
    *
    * @param codiceDivisaOrig the codice divisa orig of this scadenze partite
    */
    @Override
    public void setCodiceDivisaOrig(java.lang.String codiceDivisaOrig) {
        _scadenzePartite.setCodiceDivisaOrig(codiceDivisaOrig);
    }

    /**
    * Returns the importo totale int of this scadenze partite.
    *
    * @return the importo totale int of this scadenze partite
    */
    @Override
    public double getImportoTotaleInt() {
        return _scadenzePartite.getImportoTotaleInt();
    }

    /**
    * Sets the importo totale int of this scadenze partite.
    *
    * @param importoTotaleInt the importo totale int of this scadenze partite
    */
    @Override
    public void setImportoTotaleInt(double importoTotaleInt) {
        _scadenzePartite.setImportoTotaleInt(importoTotaleInt);
    }

    /**
    * Returns the importo pagato int of this scadenze partite.
    *
    * @return the importo pagato int of this scadenze partite
    */
    @Override
    public double getImportoPagatoInt() {
        return _scadenzePartite.getImportoPagatoInt();
    }

    /**
    * Sets the importo pagato int of this scadenze partite.
    *
    * @param importoPagatoInt the importo pagato int of this scadenze partite
    */
    @Override
    public void setImportoPagatoInt(double importoPagatoInt) {
        _scadenzePartite.setImportoPagatoInt(importoPagatoInt);
    }

    /**
    * Returns the importo aperto int of this scadenze partite.
    *
    * @return the importo aperto int of this scadenze partite
    */
    @Override
    public double getImportoApertoInt() {
        return _scadenzePartite.getImportoApertoInt();
    }

    /**
    * Sets the importo aperto int of this scadenze partite.
    *
    * @param importoApertoInt the importo aperto int of this scadenze partite
    */
    @Override
    public void setImportoApertoInt(double importoApertoInt) {
        _scadenzePartite.setImportoApertoInt(importoApertoInt);
    }

    /**
    * Returns the importo esposto int of this scadenze partite.
    *
    * @return the importo esposto int of this scadenze partite
    */
    @Override
    public double getImportoEspostoInt() {
        return _scadenzePartite.getImportoEspostoInt();
    }

    /**
    * Sets the importo esposto int of this scadenze partite.
    *
    * @param importoEspostoInt the importo esposto int of this scadenze partite
    */
    @Override
    public void setImportoEspostoInt(double importoEspostoInt) {
        _scadenzePartite.setImportoEspostoInt(importoEspostoInt);
    }

    /**
    * Returns the importo scaduto int of this scadenze partite.
    *
    * @return the importo scaduto int of this scadenze partite
    */
    @Override
    public double getImportoScadutoInt() {
        return _scadenzePartite.getImportoScadutoInt();
    }

    /**
    * Sets the importo scaduto int of this scadenze partite.
    *
    * @param importoScadutoInt the importo scaduto int of this scadenze partite
    */
    @Override
    public void setImportoScadutoInt(double importoScadutoInt) {
        _scadenzePartite.setImportoScadutoInt(importoScadutoInt);
    }

    /**
    * Returns the importo insoluto int of this scadenze partite.
    *
    * @return the importo insoluto int of this scadenze partite
    */
    @Override
    public double getImportoInsolutoInt() {
        return _scadenzePartite.getImportoInsolutoInt();
    }

    /**
    * Sets the importo insoluto int of this scadenze partite.
    *
    * @param importoInsolutoInt the importo insoluto int of this scadenze partite
    */
    @Override
    public void setImportoInsolutoInt(double importoInsolutoInt) {
        _scadenzePartite.setImportoInsolutoInt(importoInsolutoInt);
    }

    /**
    * Returns the importo in contenzioso int of this scadenze partite.
    *
    * @return the importo in contenzioso int of this scadenze partite
    */
    @Override
    public double getImportoInContenziosoInt() {
        return _scadenzePartite.getImportoInContenziosoInt();
    }

    /**
    * Sets the importo in contenzioso int of this scadenze partite.
    *
    * @param importoInContenziosoInt the importo in contenzioso int of this scadenze partite
    */
    @Override
    public void setImportoInContenziosoInt(double importoInContenziosoInt) {
        _scadenzePartite.setImportoInContenziosoInt(importoInContenziosoInt);
    }

    /**
    * Returns the importo totale of this scadenze partite.
    *
    * @return the importo totale of this scadenze partite
    */
    @Override
    public double getImportoTotale() {
        return _scadenzePartite.getImportoTotale();
    }

    /**
    * Sets the importo totale of this scadenze partite.
    *
    * @param importoTotale the importo totale of this scadenze partite
    */
    @Override
    public void setImportoTotale(double importoTotale) {
        _scadenzePartite.setImportoTotale(importoTotale);
    }

    /**
    * Returns the importo pagato of this scadenze partite.
    *
    * @return the importo pagato of this scadenze partite
    */
    @Override
    public double getImportoPagato() {
        return _scadenzePartite.getImportoPagato();
    }

    /**
    * Sets the importo pagato of this scadenze partite.
    *
    * @param importoPagato the importo pagato of this scadenze partite
    */
    @Override
    public void setImportoPagato(double importoPagato) {
        _scadenzePartite.setImportoPagato(importoPagato);
    }

    /**
    * Returns the importo aperto of this scadenze partite.
    *
    * @return the importo aperto of this scadenze partite
    */
    @Override
    public double getImportoAperto() {
        return _scadenzePartite.getImportoAperto();
    }

    /**
    * Sets the importo aperto of this scadenze partite.
    *
    * @param importoAperto the importo aperto of this scadenze partite
    */
    @Override
    public void setImportoAperto(double importoAperto) {
        _scadenzePartite.setImportoAperto(importoAperto);
    }

    /**
    * Returns the importo esposto of this scadenze partite.
    *
    * @return the importo esposto of this scadenze partite
    */
    @Override
    public double getImportoEsposto() {
        return _scadenzePartite.getImportoEsposto();
    }

    /**
    * Sets the importo esposto of this scadenze partite.
    *
    * @param importoEsposto the importo esposto of this scadenze partite
    */
    @Override
    public void setImportoEsposto(double importoEsposto) {
        _scadenzePartite.setImportoEsposto(importoEsposto);
    }

    /**
    * Returns the importo scaduto of this scadenze partite.
    *
    * @return the importo scaduto of this scadenze partite
    */
    @Override
    public double getImportoScaduto() {
        return _scadenzePartite.getImportoScaduto();
    }

    /**
    * Sets the importo scaduto of this scadenze partite.
    *
    * @param importoScaduto the importo scaduto of this scadenze partite
    */
    @Override
    public void setImportoScaduto(double importoScaduto) {
        _scadenzePartite.setImportoScaduto(importoScaduto);
    }

    /**
    * Returns the importo insoluto of this scadenze partite.
    *
    * @return the importo insoluto of this scadenze partite
    */
    @Override
    public double getImportoInsoluto() {
        return _scadenzePartite.getImportoInsoluto();
    }

    /**
    * Sets the importo insoluto of this scadenze partite.
    *
    * @param importoInsoluto the importo insoluto of this scadenze partite
    */
    @Override
    public void setImportoInsoluto(double importoInsoluto) {
        _scadenzePartite.setImportoInsoluto(importoInsoluto);
    }

    /**
    * Returns the importo in contenzioso of this scadenze partite.
    *
    * @return the importo in contenzioso of this scadenze partite
    */
    @Override
    public double getImportoInContenzioso() {
        return _scadenzePartite.getImportoInContenzioso();
    }

    /**
    * Sets the importo in contenzioso of this scadenze partite.
    *
    * @param importoInContenzioso the importo in contenzioso of this scadenze partite
    */
    @Override
    public void setImportoInContenzioso(double importoInContenzioso) {
        _scadenzePartite.setImportoInContenzioso(importoInContenzioso);
    }

    /**
    * Returns the numero lettere sollecito of this scadenze partite.
    *
    * @return the numero lettere sollecito of this scadenze partite
    */
    @Override
    public int getNumeroLettereSollecito() {
        return _scadenzePartite.getNumeroLettereSollecito();
    }

    /**
    * Sets the numero lettere sollecito of this scadenze partite.
    *
    * @param numeroLettereSollecito the numero lettere sollecito of this scadenze partite
    */
    @Override
    public void setNumeroLettereSollecito(int numeroLettereSollecito) {
        _scadenzePartite.setNumeroLettereSollecito(numeroLettereSollecito);
    }

    /**
    * Returns the codice divisa interna of this scadenze partite.
    *
    * @return the codice divisa interna of this scadenze partite
    */
    @Override
    public java.lang.String getCodiceDivisaInterna() {
        return _scadenzePartite.getCodiceDivisaInterna();
    }

    /**
    * Sets the codice divisa interna of this scadenze partite.
    *
    * @param codiceDivisaInterna the codice divisa interna of this scadenze partite
    */
    @Override
    public void setCodiceDivisaInterna(java.lang.String codiceDivisaInterna) {
        _scadenzePartite.setCodiceDivisaInterna(codiceDivisaInterna);
    }

    /**
    * Returns the codice esercizio scadenza conversione of this scadenze partite.
    *
    * @return the codice esercizio scadenza conversione of this scadenze partite
    */
    @Override
    public int getCodiceEsercizioScadenzaConversione() {
        return _scadenzePartite.getCodiceEsercizioScadenzaConversione();
    }

    /**
    * Sets the codice esercizio scadenza conversione of this scadenze partite.
    *
    * @param codiceEsercizioScadenzaConversione the codice esercizio scadenza conversione of this scadenze partite
    */
    @Override
    public void setCodiceEsercizioScadenzaConversione(
        int codiceEsercizioScadenzaConversione) {
        _scadenzePartite.setCodiceEsercizioScadenzaConversione(codiceEsercizioScadenzaConversione);
    }

    /**
    * Returns the numero partita scadenza coversione of this scadenze partite.
    *
    * @return the numero partita scadenza coversione of this scadenze partite
    */
    @Override
    public int getNumeroPartitaScadenzaCoversione() {
        return _scadenzePartite.getNumeroPartitaScadenzaCoversione();
    }

    /**
    * Sets the numero partita scadenza coversione of this scadenze partite.
    *
    * @param numeroPartitaScadenzaCoversione the numero partita scadenza coversione of this scadenze partite
    */
    @Override
    public void setNumeroPartitaScadenzaCoversione(
        int numeroPartitaScadenzaCoversione) {
        _scadenzePartite.setNumeroPartitaScadenzaCoversione(numeroPartitaScadenzaCoversione);
    }

    /**
    * Returns the numero scadenza coversione of this scadenze partite.
    *
    * @return the numero scadenza coversione of this scadenze partite
    */
    @Override
    public int getNumeroScadenzaCoversione() {
        return _scadenzePartite.getNumeroScadenzaCoversione();
    }

    /**
    * Sets the numero scadenza coversione of this scadenze partite.
    *
    * @param numeroScadenzaCoversione the numero scadenza coversione of this scadenze partite
    */
    @Override
    public void setNumeroScadenzaCoversione(int numeroScadenzaCoversione) {
        _scadenzePartite.setNumeroScadenzaCoversione(numeroScadenzaCoversione);
    }

    /**
    * Returns the tipo coversione of this scadenze partite.
    *
    * @return the tipo coversione of this scadenze partite
    */
    @Override
    public int getTipoCoversione() {
        return _scadenzePartite.getTipoCoversione();
    }

    /**
    * Sets the tipo coversione of this scadenze partite.
    *
    * @param tipoCoversione the tipo coversione of this scadenze partite
    */
    @Override
    public void setTipoCoversione(int tipoCoversione) {
        _scadenzePartite.setTipoCoversione(tipoCoversione);
    }

    /**
    * Returns the codice esercizio scrittura conversione of this scadenze partite.
    *
    * @return the codice esercizio scrittura conversione of this scadenze partite
    */
    @Override
    public int getCodiceEsercizioScritturaConversione() {
        return _scadenzePartite.getCodiceEsercizioScritturaConversione();
    }

    /**
    * Sets the codice esercizio scrittura conversione of this scadenze partite.
    *
    * @param codiceEsercizioScritturaConversione the codice esercizio scrittura conversione of this scadenze partite
    */
    @Override
    public void setCodiceEsercizioScritturaConversione(
        int codiceEsercizioScritturaConversione) {
        _scadenzePartite.setCodiceEsercizioScritturaConversione(codiceEsercizioScritturaConversione);
    }

    /**
    * Returns the numero registrazione scrittura conversione of this scadenze partite.
    *
    * @return the numero registrazione scrittura conversione of this scadenze partite
    */
    @Override
    public int getNumeroRegistrazioneScritturaConversione() {
        return _scadenzePartite.getNumeroRegistrazioneScritturaConversione();
    }

    /**
    * Sets the numero registrazione scrittura conversione of this scadenze partite.
    *
    * @param numeroRegistrazioneScritturaConversione the numero registrazione scrittura conversione of this scadenze partite
    */
    @Override
    public void setNumeroRegistrazioneScritturaConversione(
        int numeroRegistrazioneScritturaConversione) {
        _scadenzePartite.setNumeroRegistrazioneScritturaConversione(numeroRegistrazioneScritturaConversione);
    }

    /**
    * Returns the data registrazione scrittura conversione of this scadenze partite.
    *
    * @return the data registrazione scrittura conversione of this scadenze partite
    */
    @Override
    public java.util.Date getDataRegistrazioneScritturaConversione() {
        return _scadenzePartite.getDataRegistrazioneScritturaConversione();
    }

    /**
    * Sets the data registrazione scrittura conversione of this scadenze partite.
    *
    * @param dataRegistrazioneScritturaConversione the data registrazione scrittura conversione of this scadenze partite
    */
    @Override
    public void setDataRegistrazioneScritturaConversione(
        java.util.Date dataRegistrazioneScritturaConversione) {
        _scadenzePartite.setDataRegistrazioneScritturaConversione(dataRegistrazioneScritturaConversione);
    }

    /**
    * Returns the codice divisa int scadenza conversione of this scadenze partite.
    *
    * @return the codice divisa int scadenza conversione of this scadenze partite
    */
    @Override
    public java.lang.String getCodiceDivisaIntScadenzaConversione() {
        return _scadenzePartite.getCodiceDivisaIntScadenzaConversione();
    }

    /**
    * Sets the codice divisa int scadenza conversione of this scadenze partite.
    *
    * @param codiceDivisaIntScadenzaConversione the codice divisa int scadenza conversione of this scadenze partite
    */
    @Override
    public void setCodiceDivisaIntScadenzaConversione(
        java.lang.String codiceDivisaIntScadenzaConversione) {
        _scadenzePartite.setCodiceDivisaIntScadenzaConversione(codiceDivisaIntScadenzaConversione);
    }

    /**
    * Returns the codice divisa scadenza conversione of this scadenze partite.
    *
    * @return the codice divisa scadenza conversione of this scadenze partite
    */
    @Override
    public java.lang.String getCodiceDivisaScadenzaConversione() {
        return _scadenzePartite.getCodiceDivisaScadenzaConversione();
    }

    /**
    * Sets the codice divisa scadenza conversione of this scadenze partite.
    *
    * @param codiceDivisaScadenzaConversione the codice divisa scadenza conversione of this scadenze partite
    */
    @Override
    public void setCodiceDivisaScadenzaConversione(
        java.lang.String codiceDivisaScadenzaConversione) {
        _scadenzePartite.setCodiceDivisaScadenzaConversione(codiceDivisaScadenzaConversione);
    }

    /**
    * Returns the cambio scadenza conversione of this scadenze partite.
    *
    * @return the cambio scadenza conversione of this scadenze partite
    */
    @Override
    public double getCambioScadenzaConversione() {
        return _scadenzePartite.getCambioScadenzaConversione();
    }

    /**
    * Sets the cambio scadenza conversione of this scadenze partite.
    *
    * @param cambioScadenzaConversione the cambio scadenza conversione of this scadenze partite
    */
    @Override
    public void setCambioScadenzaConversione(double cambioScadenzaConversione) {
        _scadenzePartite.setCambioScadenzaConversione(cambioScadenzaConversione);
    }

    @Override
    public boolean isNew() {
        return _scadenzePartite.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _scadenzePartite.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _scadenzePartite.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _scadenzePartite.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _scadenzePartite.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _scadenzePartite.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _scadenzePartite.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _scadenzePartite.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _scadenzePartite.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _scadenzePartite.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _scadenzePartite.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new ScadenzePartiteWrapper((ScadenzePartite) _scadenzePartite.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.ScadenzePartite scadenzePartite) {
        return _scadenzePartite.compareTo(scadenzePartite);
    }

    @Override
    public int hashCode() {
        return _scadenzePartite.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.ScadenzePartite> toCacheModel() {
        return _scadenzePartite.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.ScadenzePartite toEscapedModel() {
        return new ScadenzePartiteWrapper(_scadenzePartite.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.ScadenzePartite toUnescapedModel() {
        return new ScadenzePartiteWrapper(_scadenzePartite.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _scadenzePartite.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _scadenzePartite.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _scadenzePartite.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ScadenzePartiteWrapper)) {
            return false;
        }

        ScadenzePartiteWrapper scadenzePartiteWrapper = (ScadenzePartiteWrapper) obj;

        if (Validator.equals(_scadenzePartite,
                    scadenzePartiteWrapper._scadenzePartite)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public ScadenzePartite getWrappedScadenzePartite() {
        return _scadenzePartite;
    }

    @Override
    public ScadenzePartite getWrappedModel() {
        return _scadenzePartite;
    }

    @Override
    public void resetOriginalValues() {
        _scadenzePartite.resetOriginalValues();
    }
}
