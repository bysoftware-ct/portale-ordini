package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link DettagliOperazioniContabili}.
 * </p>
 *
 * @author Mario Torrisi
 * @see DettagliOperazioniContabili
 * @generated
 */
public class DettagliOperazioniContabiliWrapper
    implements DettagliOperazioniContabili,
        ModelWrapper<DettagliOperazioniContabili> {
    private DettagliOperazioniContabili _dettagliOperazioniContabili;

    public DettagliOperazioniContabiliWrapper(
        DettagliOperazioniContabili dettagliOperazioniContabili) {
        _dettagliOperazioniContabili = dettagliOperazioniContabili;
    }

    @Override
    public Class<?> getModelClass() {
        return DettagliOperazioniContabili.class;
    }

    @Override
    public String getModelClassName() {
        return DettagliOperazioniContabili.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceOperazione", getCodiceOperazione());
        attributes.put("progressivoLegame", getProgressivoLegame());
        attributes.put("tipoLegame", getTipoLegame());
        attributes.put("causaleContabile", getCausaleContabile());
        attributes.put("sottoconto", getSottoconto());
        attributes.put("naturaConto", getNaturaConto());
        attributes.put("tipoImporto", getTipoImporto());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceOperazione = (String) attributes.get("codiceOperazione");

        if (codiceOperazione != null) {
            setCodiceOperazione(codiceOperazione);
        }

        Integer progressivoLegame = (Integer) attributes.get(
                "progressivoLegame");

        if (progressivoLegame != null) {
            setProgressivoLegame(progressivoLegame);
        }

        Integer tipoLegame = (Integer) attributes.get("tipoLegame");

        if (tipoLegame != null) {
            setTipoLegame(tipoLegame);
        }

        String causaleContabile = (String) attributes.get("causaleContabile");

        if (causaleContabile != null) {
            setCausaleContabile(causaleContabile);
        }

        String sottoconto = (String) attributes.get("sottoconto");

        if (sottoconto != null) {
            setSottoconto(sottoconto);
        }

        String naturaConto = (String) attributes.get("naturaConto");

        if (naturaConto != null) {
            setNaturaConto(naturaConto);
        }

        Integer tipoImporto = (Integer) attributes.get("tipoImporto");

        if (tipoImporto != null) {
            setTipoImporto(tipoImporto);
        }
    }

    /**
    * Returns the primary key of this dettagli operazioni contabili.
    *
    * @return the primary key of this dettagli operazioni contabili
    */
    @Override
    public it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK getPrimaryKey() {
        return _dettagliOperazioniContabili.getPrimaryKey();
    }

    /**
    * Sets the primary key of this dettagli operazioni contabili.
    *
    * @param primaryKey the primary key of this dettagli operazioni contabili
    */
    @Override
    public void setPrimaryKey(
        it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPK primaryKey) {
        _dettagliOperazioniContabili.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the codice operazione of this dettagli operazioni contabili.
    *
    * @return the codice operazione of this dettagli operazioni contabili
    */
    @Override
    public java.lang.String getCodiceOperazione() {
        return _dettagliOperazioniContabili.getCodiceOperazione();
    }

    /**
    * Sets the codice operazione of this dettagli operazioni contabili.
    *
    * @param codiceOperazione the codice operazione of this dettagli operazioni contabili
    */
    @Override
    public void setCodiceOperazione(java.lang.String codiceOperazione) {
        _dettagliOperazioniContabili.setCodiceOperazione(codiceOperazione);
    }

    /**
    * Returns the progressivo legame of this dettagli operazioni contabili.
    *
    * @return the progressivo legame of this dettagli operazioni contabili
    */
    @Override
    public int getProgressivoLegame() {
        return _dettagliOperazioniContabili.getProgressivoLegame();
    }

    /**
    * Sets the progressivo legame of this dettagli operazioni contabili.
    *
    * @param progressivoLegame the progressivo legame of this dettagli operazioni contabili
    */
    @Override
    public void setProgressivoLegame(int progressivoLegame) {
        _dettagliOperazioniContabili.setProgressivoLegame(progressivoLegame);
    }

    /**
    * Returns the tipo legame of this dettagli operazioni contabili.
    *
    * @return the tipo legame of this dettagli operazioni contabili
    */
    @Override
    public int getTipoLegame() {
        return _dettagliOperazioniContabili.getTipoLegame();
    }

    /**
    * Sets the tipo legame of this dettagli operazioni contabili.
    *
    * @param tipoLegame the tipo legame of this dettagli operazioni contabili
    */
    @Override
    public void setTipoLegame(int tipoLegame) {
        _dettagliOperazioniContabili.setTipoLegame(tipoLegame);
    }

    /**
    * Returns the causale contabile of this dettagli operazioni contabili.
    *
    * @return the causale contabile of this dettagli operazioni contabili
    */
    @Override
    public java.lang.String getCausaleContabile() {
        return _dettagliOperazioniContabili.getCausaleContabile();
    }

    /**
    * Sets the causale contabile of this dettagli operazioni contabili.
    *
    * @param causaleContabile the causale contabile of this dettagli operazioni contabili
    */
    @Override
    public void setCausaleContabile(java.lang.String causaleContabile) {
        _dettagliOperazioniContabili.setCausaleContabile(causaleContabile);
    }

    /**
    * Returns the sottoconto of this dettagli operazioni contabili.
    *
    * @return the sottoconto of this dettagli operazioni contabili
    */
    @Override
    public java.lang.String getSottoconto() {
        return _dettagliOperazioniContabili.getSottoconto();
    }

    /**
    * Sets the sottoconto of this dettagli operazioni contabili.
    *
    * @param sottoconto the sottoconto of this dettagli operazioni contabili
    */
    @Override
    public void setSottoconto(java.lang.String sottoconto) {
        _dettagliOperazioniContabili.setSottoconto(sottoconto);
    }

    /**
    * Returns the natura conto of this dettagli operazioni contabili.
    *
    * @return the natura conto of this dettagli operazioni contabili
    */
    @Override
    public java.lang.String getNaturaConto() {
        return _dettagliOperazioniContabili.getNaturaConto();
    }

    /**
    * Sets the natura conto of this dettagli operazioni contabili.
    *
    * @param naturaConto the natura conto of this dettagli operazioni contabili
    */
    @Override
    public void setNaturaConto(java.lang.String naturaConto) {
        _dettagliOperazioniContabili.setNaturaConto(naturaConto);
    }

    /**
    * Returns the tipo importo of this dettagli operazioni contabili.
    *
    * @return the tipo importo of this dettagli operazioni contabili
    */
    @Override
    public int getTipoImporto() {
        return _dettagliOperazioniContabili.getTipoImporto();
    }

    /**
    * Sets the tipo importo of this dettagli operazioni contabili.
    *
    * @param tipoImporto the tipo importo of this dettagli operazioni contabili
    */
    @Override
    public void setTipoImporto(int tipoImporto) {
        _dettagliOperazioniContabili.setTipoImporto(tipoImporto);
    }

    @Override
    public boolean isNew() {
        return _dettagliOperazioniContabili.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _dettagliOperazioniContabili.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _dettagliOperazioniContabili.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _dettagliOperazioniContabili.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _dettagliOperazioniContabili.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _dettagliOperazioniContabili.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _dettagliOperazioniContabili.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _dettagliOperazioniContabili.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _dettagliOperazioniContabili.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _dettagliOperazioniContabili.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _dettagliOperazioniContabili.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new DettagliOperazioniContabiliWrapper((DettagliOperazioniContabili) _dettagliOperazioniContabili.clone());
    }

    @Override
    public int compareTo(
        it.bysoftware.ct.model.DettagliOperazioniContabili dettagliOperazioniContabili) {
        return _dettagliOperazioniContabili.compareTo(dettagliOperazioniContabili);
    }

    @Override
    public int hashCode() {
        return _dettagliOperazioniContabili.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.DettagliOperazioniContabili> toCacheModel() {
        return _dettagliOperazioniContabili.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.DettagliOperazioniContabili toEscapedModel() {
        return new DettagliOperazioniContabiliWrapper(_dettagliOperazioniContabili.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.DettagliOperazioniContabili toUnescapedModel() {
        return new DettagliOperazioniContabiliWrapper(_dettagliOperazioniContabili.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _dettagliOperazioniContabili.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _dettagliOperazioniContabili.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _dettagliOperazioniContabili.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof DettagliOperazioniContabiliWrapper)) {
            return false;
        }

        DettagliOperazioniContabiliWrapper dettagliOperazioniContabiliWrapper = (DettagliOperazioniContabiliWrapper) obj;

        if (Validator.equals(_dettagliOperazioniContabili,
                    dettagliOperazioniContabiliWrapper._dettagliOperazioniContabili)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public DettagliOperazioniContabili getWrappedDettagliOperazioniContabili() {
        return _dettagliOperazioniContabili;
    }

    @Override
    public DettagliOperazioniContabili getWrappedModel() {
        return _dettagliOperazioniContabili;
    }

    @Override
    public void resetOriginalValues() {
        _dettagliOperazioniContabili.resetOriginalValues();
    }
}
