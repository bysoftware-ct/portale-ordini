package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.RigoDocumentoLocalServiceUtil;
import it.bysoftware.ct.service.persistence.RigoDocumentoPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class RigoDocumentoClp extends BaseModelImpl<RigoDocumento>
    implements RigoDocumento {
    private int _anno;
    private String _codiceAttivita;
    private String _codiceCentro;
    private String _codiceDeposito;
    private int _protocollo;
    private String _codiceFornitore;
    private int _rigo;
    private String _tipoDocumento;
    private int _tipoRigo;
    private String _codiceArticolo;
    private String _codiceVariante;
    private String _descrizione;
    private double _quantita;
    private double _quantitaSecondaria;
    private double _prezzo;
    private double _sconto1;
    private double _sconto2;
    private double _sconto3;
    private String _libStr1;
    private String _libStr2;
    private String _libStr3;
    private double _libDbl1;
    private double _libDbl2;
    private double _libDbl3;
    private long _libLng1;
    private long _libLng2;
    private long _libLng3;
    private Date _libDat1;
    private Date _libDat2;
    private Date _libDat3;
    private double _importoNetto;
    private String _codiceIVA;
    private BaseModel<?> _rigoDocumentoRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public RigoDocumentoClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return RigoDocumento.class;
    }

    @Override
    public String getModelClassName() {
        return RigoDocumento.class.getName();
    }

    @Override
    public RigoDocumentoPK getPrimaryKey() {
        return new RigoDocumentoPK(_anno, _codiceAttivita, _codiceCentro,
            _codiceDeposito, _protocollo, _codiceFornitore, _rigo,
            _tipoDocumento);
    }

    @Override
    public void setPrimaryKey(RigoDocumentoPK primaryKey) {
        setAnno(primaryKey.anno);
        setCodiceAttivita(primaryKey.codiceAttivita);
        setCodiceCentro(primaryKey.codiceCentro);
        setCodiceDeposito(primaryKey.codiceDeposito);
        setProtocollo(primaryKey.protocollo);
        setCodiceFornitore(primaryKey.codiceFornitore);
        setRigo(primaryKey.rigo);
        setTipoDocumento(primaryKey.tipoDocumento);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return new RigoDocumentoPK(_anno, _codiceAttivita, _codiceCentro,
            _codiceDeposito, _protocollo, _codiceFornitore, _rigo,
            _tipoDocumento);
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((RigoDocumentoPK) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("anno", getAnno());
        attributes.put("codiceAttivita", getCodiceAttivita());
        attributes.put("codiceCentro", getCodiceCentro());
        attributes.put("codiceDeposito", getCodiceDeposito());
        attributes.put("protocollo", getProtocollo());
        attributes.put("codiceFornitore", getCodiceFornitore());
        attributes.put("rigo", getRigo());
        attributes.put("tipoDocumento", getTipoDocumento());
        attributes.put("tipoRigo", getTipoRigo());
        attributes.put("codiceArticolo", getCodiceArticolo());
        attributes.put("codiceVariante", getCodiceVariante());
        attributes.put("descrizione", getDescrizione());
        attributes.put("quantita", getQuantita());
        attributes.put("quantitaSecondaria", getQuantitaSecondaria());
        attributes.put("prezzo", getPrezzo());
        attributes.put("sconto1", getSconto1());
        attributes.put("sconto2", getSconto2());
        attributes.put("sconto3", getSconto3());
        attributes.put("libStr1", getLibStr1());
        attributes.put("libStr2", getLibStr2());
        attributes.put("libStr3", getLibStr3());
        attributes.put("libDbl1", getLibDbl1());
        attributes.put("libDbl2", getLibDbl2());
        attributes.put("libDbl3", getLibDbl3());
        attributes.put("libLng1", getLibLng1());
        attributes.put("libLng2", getLibLng2());
        attributes.put("libLng3", getLibLng3());
        attributes.put("libDat1", getLibDat1());
        attributes.put("libDat2", getLibDat2());
        attributes.put("libDat3", getLibDat3());
        attributes.put("importoNetto", getImportoNetto());
        attributes.put("codiceIVA", getCodiceIVA());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Integer anno = (Integer) attributes.get("anno");

        if (anno != null) {
            setAnno(anno);
        }

        String codiceAttivita = (String) attributes.get("codiceAttivita");

        if (codiceAttivita != null) {
            setCodiceAttivita(codiceAttivita);
        }

        String codiceCentro = (String) attributes.get("codiceCentro");

        if (codiceCentro != null) {
            setCodiceCentro(codiceCentro);
        }

        String codiceDeposito = (String) attributes.get("codiceDeposito");

        if (codiceDeposito != null) {
            setCodiceDeposito(codiceDeposito);
        }

        Integer protocollo = (Integer) attributes.get("protocollo");

        if (protocollo != null) {
            setProtocollo(protocollo);
        }

        String codiceFornitore = (String) attributes.get("codiceFornitore");

        if (codiceFornitore != null) {
            setCodiceFornitore(codiceFornitore);
        }

        Integer rigo = (Integer) attributes.get("rigo");

        if (rigo != null) {
            setRigo(rigo);
        }

        String tipoDocumento = (String) attributes.get("tipoDocumento");

        if (tipoDocumento != null) {
            setTipoDocumento(tipoDocumento);
        }

        Integer tipoRigo = (Integer) attributes.get("tipoRigo");

        if (tipoRigo != null) {
            setTipoRigo(tipoRigo);
        }

        String codiceArticolo = (String) attributes.get("codiceArticolo");

        if (codiceArticolo != null) {
            setCodiceArticolo(codiceArticolo);
        }

        String codiceVariante = (String) attributes.get("codiceVariante");

        if (codiceVariante != null) {
            setCodiceVariante(codiceVariante);
        }

        String descrizione = (String) attributes.get("descrizione");

        if (descrizione != null) {
            setDescrizione(descrizione);
        }

        Double quantita = (Double) attributes.get("quantita");

        if (quantita != null) {
            setQuantita(quantita);
        }

        Double quantitaSecondaria = (Double) attributes.get(
                "quantitaSecondaria");

        if (quantitaSecondaria != null) {
            setQuantitaSecondaria(quantitaSecondaria);
        }

        Double prezzo = (Double) attributes.get("prezzo");

        if (prezzo != null) {
            setPrezzo(prezzo);
        }

        Double sconto1 = (Double) attributes.get("sconto1");

        if (sconto1 != null) {
            setSconto1(sconto1);
        }

        Double sconto2 = (Double) attributes.get("sconto2");

        if (sconto2 != null) {
            setSconto2(sconto2);
        }

        Double sconto3 = (Double) attributes.get("sconto3");

        if (sconto3 != null) {
            setSconto3(sconto3);
        }

        String libStr1 = (String) attributes.get("libStr1");

        if (libStr1 != null) {
            setLibStr1(libStr1);
        }

        String libStr2 = (String) attributes.get("libStr2");

        if (libStr2 != null) {
            setLibStr2(libStr2);
        }

        String libStr3 = (String) attributes.get("libStr3");

        if (libStr3 != null) {
            setLibStr3(libStr3);
        }

        Double libDbl1 = (Double) attributes.get("libDbl1");

        if (libDbl1 != null) {
            setLibDbl1(libDbl1);
        }

        Double libDbl2 = (Double) attributes.get("libDbl2");

        if (libDbl2 != null) {
            setLibDbl2(libDbl2);
        }

        Double libDbl3 = (Double) attributes.get("libDbl3");

        if (libDbl3 != null) {
            setLibDbl3(libDbl3);
        }

        Long libLng1 = (Long) attributes.get("libLng1");

        if (libLng1 != null) {
            setLibLng1(libLng1);
        }

        Long libLng2 = (Long) attributes.get("libLng2");

        if (libLng2 != null) {
            setLibLng2(libLng2);
        }

        Long libLng3 = (Long) attributes.get("libLng3");

        if (libLng3 != null) {
            setLibLng3(libLng3);
        }

        Date libDat1 = (Date) attributes.get("libDat1");

        if (libDat1 != null) {
            setLibDat1(libDat1);
        }

        Date libDat2 = (Date) attributes.get("libDat2");

        if (libDat2 != null) {
            setLibDat2(libDat2);
        }

        Date libDat3 = (Date) attributes.get("libDat3");

        if (libDat3 != null) {
            setLibDat3(libDat3);
        }

        Double importoNetto = (Double) attributes.get("importoNetto");

        if (importoNetto != null) {
            setImportoNetto(importoNetto);
        }

        String codiceIVA = (String) attributes.get("codiceIVA");

        if (codiceIVA != null) {
            setCodiceIVA(codiceIVA);
        }
    }

    @Override
    public int getAnno() {
        return _anno;
    }

    @Override
    public void setAnno(int anno) {
        _anno = anno;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setAnno", int.class);

                method.invoke(_rigoDocumentoRemoteModel, anno);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceAttivita() {
        return _codiceAttivita;
    }

    @Override
    public void setCodiceAttivita(String codiceAttivita) {
        _codiceAttivita = codiceAttivita;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceAttivita",
                        String.class);

                method.invoke(_rigoDocumentoRemoteModel, codiceAttivita);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceCentro() {
        return _codiceCentro;
    }

    @Override
    public void setCodiceCentro(String codiceCentro) {
        _codiceCentro = codiceCentro;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceCentro", String.class);

                method.invoke(_rigoDocumentoRemoteModel, codiceCentro);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceDeposito() {
        return _codiceDeposito;
    }

    @Override
    public void setCodiceDeposito(String codiceDeposito) {
        _codiceDeposito = codiceDeposito;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceDeposito",
                        String.class);

                method.invoke(_rigoDocumentoRemoteModel, codiceDeposito);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getProtocollo() {
        return _protocollo;
    }

    @Override
    public void setProtocollo(int protocollo) {
        _protocollo = protocollo;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setProtocollo", int.class);

                method.invoke(_rigoDocumentoRemoteModel, protocollo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceFornitore() {
        return _codiceFornitore;
    }

    @Override
    public void setCodiceFornitore(String codiceFornitore) {
        _codiceFornitore = codiceFornitore;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceFornitore",
                        String.class);

                method.invoke(_rigoDocumentoRemoteModel, codiceFornitore);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getRigo() {
        return _rigo;
    }

    @Override
    public void setRigo(int rigo) {
        _rigo = rigo;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setRigo", int.class);

                method.invoke(_rigoDocumentoRemoteModel, rigo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getTipoDocumento() {
        return _tipoDocumento;
    }

    @Override
    public void setTipoDocumento(String tipoDocumento) {
        _tipoDocumento = tipoDocumento;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setTipoDocumento", String.class);

                method.invoke(_rigoDocumentoRemoteModel, tipoDocumento);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getTipoRigo() {
        return _tipoRigo;
    }

    @Override
    public void setTipoRigo(int tipoRigo) {
        _tipoRigo = tipoRigo;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setTipoRigo", int.class);

                method.invoke(_rigoDocumentoRemoteModel, tipoRigo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceArticolo() {
        return _codiceArticolo;
    }

    @Override
    public void setCodiceArticolo(String codiceArticolo) {
        _codiceArticolo = codiceArticolo;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceArticolo",
                        String.class);

                method.invoke(_rigoDocumentoRemoteModel, codiceArticolo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceVariante() {
        return _codiceVariante;
    }

    @Override
    public void setCodiceVariante(String codiceVariante) {
        _codiceVariante = codiceVariante;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceVariante",
                        String.class);

                method.invoke(_rigoDocumentoRemoteModel, codiceVariante);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getDescrizione() {
        return _descrizione;
    }

    @Override
    public void setDescrizione(String descrizione) {
        _descrizione = descrizione;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setDescrizione", String.class);

                method.invoke(_rigoDocumentoRemoteModel, descrizione);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getQuantita() {
        return _quantita;
    }

    @Override
    public void setQuantita(double quantita) {
        _quantita = quantita;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setQuantita", double.class);

                method.invoke(_rigoDocumentoRemoteModel, quantita);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getQuantitaSecondaria() {
        return _quantitaSecondaria;
    }

    @Override
    public void setQuantitaSecondaria(double quantitaSecondaria) {
        _quantitaSecondaria = quantitaSecondaria;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setQuantitaSecondaria",
                        double.class);

                method.invoke(_rigoDocumentoRemoteModel, quantitaSecondaria);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrezzo() {
        return _prezzo;
    }

    @Override
    public void setPrezzo(double prezzo) {
        _prezzo = prezzo;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setPrezzo", double.class);

                method.invoke(_rigoDocumentoRemoteModel, prezzo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getSconto1() {
        return _sconto1;
    }

    @Override
    public void setSconto1(double sconto1) {
        _sconto1 = sconto1;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setSconto1", double.class);

                method.invoke(_rigoDocumentoRemoteModel, sconto1);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getSconto2() {
        return _sconto2;
    }

    @Override
    public void setSconto2(double sconto2) {
        _sconto2 = sconto2;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setSconto2", double.class);

                method.invoke(_rigoDocumentoRemoteModel, sconto2);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getSconto3() {
        return _sconto3;
    }

    @Override
    public void setSconto3(double sconto3) {
        _sconto3 = sconto3;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setSconto3", double.class);

                method.invoke(_rigoDocumentoRemoteModel, sconto3);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getLibStr1() {
        return _libStr1;
    }

    @Override
    public void setLibStr1(String libStr1) {
        _libStr1 = libStr1;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibStr1", String.class);

                method.invoke(_rigoDocumentoRemoteModel, libStr1);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getLibStr2() {
        return _libStr2;
    }

    @Override
    public void setLibStr2(String libStr2) {
        _libStr2 = libStr2;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibStr2", String.class);

                method.invoke(_rigoDocumentoRemoteModel, libStr2);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getLibStr3() {
        return _libStr3;
    }

    @Override
    public void setLibStr3(String libStr3) {
        _libStr3 = libStr3;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibStr3", String.class);

                method.invoke(_rigoDocumentoRemoteModel, libStr3);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getLibDbl1() {
        return _libDbl1;
    }

    @Override
    public void setLibDbl1(double libDbl1) {
        _libDbl1 = libDbl1;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibDbl1", double.class);

                method.invoke(_rigoDocumentoRemoteModel, libDbl1);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getLibDbl2() {
        return _libDbl2;
    }

    @Override
    public void setLibDbl2(double libDbl2) {
        _libDbl2 = libDbl2;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibDbl2", double.class);

                method.invoke(_rigoDocumentoRemoteModel, libDbl2);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getLibDbl3() {
        return _libDbl3;
    }

    @Override
    public void setLibDbl3(double libDbl3) {
        _libDbl3 = libDbl3;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibDbl3", double.class);

                method.invoke(_rigoDocumentoRemoteModel, libDbl3);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getLibLng1() {
        return _libLng1;
    }

    @Override
    public void setLibLng1(long libLng1) {
        _libLng1 = libLng1;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibLng1", long.class);

                method.invoke(_rigoDocumentoRemoteModel, libLng1);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getLibLng2() {
        return _libLng2;
    }

    @Override
    public void setLibLng2(long libLng2) {
        _libLng2 = libLng2;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibLng2", long.class);

                method.invoke(_rigoDocumentoRemoteModel, libLng2);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getLibLng3() {
        return _libLng3;
    }

    @Override
    public void setLibLng3(long libLng3) {
        _libLng3 = libLng3;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibLng3", long.class);

                method.invoke(_rigoDocumentoRemoteModel, libLng3);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getLibDat1() {
        return _libDat1;
    }

    @Override
    public void setLibDat1(Date libDat1) {
        _libDat1 = libDat1;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibDat1", Date.class);

                method.invoke(_rigoDocumentoRemoteModel, libDat1);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getLibDat2() {
        return _libDat2;
    }

    @Override
    public void setLibDat2(Date libDat2) {
        _libDat2 = libDat2;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibDat2", Date.class);

                method.invoke(_rigoDocumentoRemoteModel, libDat2);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getLibDat3() {
        return _libDat3;
    }

    @Override
    public void setLibDat3(Date libDat3) {
        _libDat3 = libDat3;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setLibDat3", Date.class);

                method.invoke(_rigoDocumentoRemoteModel, libDat3);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getImportoNetto() {
        return _importoNetto;
    }

    @Override
    public void setImportoNetto(double importoNetto) {
        _importoNetto = importoNetto;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setImportoNetto", double.class);

                method.invoke(_rigoDocumentoRemoteModel, importoNetto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodiceIVA() {
        return _codiceIVA;
    }

    @Override
    public void setCodiceIVA(String codiceIVA) {
        _codiceIVA = codiceIVA;

        if (_rigoDocumentoRemoteModel != null) {
            try {
                Class<?> clazz = _rigoDocumentoRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceIVA", String.class);

                method.invoke(_rigoDocumentoRemoteModel, codiceIVA);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getRigoDocumentoRemoteModel() {
        return _rigoDocumentoRemoteModel;
    }

    public void setRigoDocumentoRemoteModel(
        BaseModel<?> rigoDocumentoRemoteModel) {
        _rigoDocumentoRemoteModel = rigoDocumentoRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _rigoDocumentoRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_rigoDocumentoRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            RigoDocumentoLocalServiceUtil.addRigoDocumento(this);
        } else {
            RigoDocumentoLocalServiceUtil.updateRigoDocumento(this);
        }
    }

    @Override
    public RigoDocumento toEscapedModel() {
        return (RigoDocumento) ProxyUtil.newProxyInstance(RigoDocumento.class.getClassLoader(),
            new Class[] { RigoDocumento.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        RigoDocumentoClp clone = new RigoDocumentoClp();

        clone.setAnno(getAnno());
        clone.setCodiceAttivita(getCodiceAttivita());
        clone.setCodiceCentro(getCodiceCentro());
        clone.setCodiceDeposito(getCodiceDeposito());
        clone.setProtocollo(getProtocollo());
        clone.setCodiceFornitore(getCodiceFornitore());
        clone.setRigo(getRigo());
        clone.setTipoDocumento(getTipoDocumento());
        clone.setTipoRigo(getTipoRigo());
        clone.setCodiceArticolo(getCodiceArticolo());
        clone.setCodiceVariante(getCodiceVariante());
        clone.setDescrizione(getDescrizione());
        clone.setQuantita(getQuantita());
        clone.setQuantitaSecondaria(getQuantitaSecondaria());
        clone.setPrezzo(getPrezzo());
        clone.setSconto1(getSconto1());
        clone.setSconto2(getSconto2());
        clone.setSconto3(getSconto3());
        clone.setLibStr1(getLibStr1());
        clone.setLibStr2(getLibStr2());
        clone.setLibStr3(getLibStr3());
        clone.setLibDbl1(getLibDbl1());
        clone.setLibDbl2(getLibDbl2());
        clone.setLibDbl3(getLibDbl3());
        clone.setLibLng1(getLibLng1());
        clone.setLibLng2(getLibLng2());
        clone.setLibLng3(getLibLng3());
        clone.setLibDat1(getLibDat1());
        clone.setLibDat2(getLibDat2());
        clone.setLibDat3(getLibDat3());
        clone.setImportoNetto(getImportoNetto());
        clone.setCodiceIVA(getCodiceIVA());

        return clone;
    }

    @Override
    public int compareTo(RigoDocumento rigoDocumento) {
        RigoDocumentoPK primaryKey = rigoDocumento.getPrimaryKey();

        return getPrimaryKey().compareTo(primaryKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof RigoDocumentoClp)) {
            return false;
        }

        RigoDocumentoClp rigoDocumento = (RigoDocumentoClp) obj;

        RigoDocumentoPK primaryKey = rigoDocumento.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(65);

        sb.append("{anno=");
        sb.append(getAnno());
        sb.append(", codiceAttivita=");
        sb.append(getCodiceAttivita());
        sb.append(", codiceCentro=");
        sb.append(getCodiceCentro());
        sb.append(", codiceDeposito=");
        sb.append(getCodiceDeposito());
        sb.append(", protocollo=");
        sb.append(getProtocollo());
        sb.append(", codiceFornitore=");
        sb.append(getCodiceFornitore());
        sb.append(", rigo=");
        sb.append(getRigo());
        sb.append(", tipoDocumento=");
        sb.append(getTipoDocumento());
        sb.append(", tipoRigo=");
        sb.append(getTipoRigo());
        sb.append(", codiceArticolo=");
        sb.append(getCodiceArticolo());
        sb.append(", codiceVariante=");
        sb.append(getCodiceVariante());
        sb.append(", descrizione=");
        sb.append(getDescrizione());
        sb.append(", quantita=");
        sb.append(getQuantita());
        sb.append(", quantitaSecondaria=");
        sb.append(getQuantitaSecondaria());
        sb.append(", prezzo=");
        sb.append(getPrezzo());
        sb.append(", sconto1=");
        sb.append(getSconto1());
        sb.append(", sconto2=");
        sb.append(getSconto2());
        sb.append(", sconto3=");
        sb.append(getSconto3());
        sb.append(", libStr1=");
        sb.append(getLibStr1());
        sb.append(", libStr2=");
        sb.append(getLibStr2());
        sb.append(", libStr3=");
        sb.append(getLibStr3());
        sb.append(", libDbl1=");
        sb.append(getLibDbl1());
        sb.append(", libDbl2=");
        sb.append(getLibDbl2());
        sb.append(", libDbl3=");
        sb.append(getLibDbl3());
        sb.append(", libLng1=");
        sb.append(getLibLng1());
        sb.append(", libLng2=");
        sb.append(getLibLng2());
        sb.append(", libLng3=");
        sb.append(getLibLng3());
        sb.append(", libDat1=");
        sb.append(getLibDat1());
        sb.append(", libDat2=");
        sb.append(getLibDat2());
        sb.append(", libDat3=");
        sb.append(getLibDat3());
        sb.append(", importoNetto=");
        sb.append(getImportoNetto());
        sb.append(", codiceIVA=");
        sb.append(getCodiceIVA());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(100);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.RigoDocumento");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>anno</column-name><column-value><![CDATA[");
        sb.append(getAnno());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceAttivita</column-name><column-value><![CDATA[");
        sb.append(getCodiceAttivita());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceCentro</column-name><column-value><![CDATA[");
        sb.append(getCodiceCentro());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceDeposito</column-name><column-value><![CDATA[");
        sb.append(getCodiceDeposito());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>protocollo</column-name><column-value><![CDATA[");
        sb.append(getProtocollo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceFornitore</column-name><column-value><![CDATA[");
        sb.append(getCodiceFornitore());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>rigo</column-name><column-value><![CDATA[");
        sb.append(getRigo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tipoDocumento</column-name><column-value><![CDATA[");
        sb.append(getTipoDocumento());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tipoRigo</column-name><column-value><![CDATA[");
        sb.append(getTipoRigo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceArticolo</column-name><column-value><![CDATA[");
        sb.append(getCodiceArticolo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceVariante</column-name><column-value><![CDATA[");
        sb.append(getCodiceVariante());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>descrizione</column-name><column-value><![CDATA[");
        sb.append(getDescrizione());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>quantita</column-name><column-value><![CDATA[");
        sb.append(getQuantita());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>quantitaSecondaria</column-name><column-value><![CDATA[");
        sb.append(getQuantitaSecondaria());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>prezzo</column-name><column-value><![CDATA[");
        sb.append(getPrezzo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>sconto1</column-name><column-value><![CDATA[");
        sb.append(getSconto1());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>sconto2</column-name><column-value><![CDATA[");
        sb.append(getSconto2());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>sconto3</column-name><column-value><![CDATA[");
        sb.append(getSconto3());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libStr1</column-name><column-value><![CDATA[");
        sb.append(getLibStr1());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libStr2</column-name><column-value><![CDATA[");
        sb.append(getLibStr2());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libStr3</column-name><column-value><![CDATA[");
        sb.append(getLibStr3());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libDbl1</column-name><column-value><![CDATA[");
        sb.append(getLibDbl1());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libDbl2</column-name><column-value><![CDATA[");
        sb.append(getLibDbl2());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libDbl3</column-name><column-value><![CDATA[");
        sb.append(getLibDbl3());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libLng1</column-name><column-value><![CDATA[");
        sb.append(getLibLng1());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libLng2</column-name><column-value><![CDATA[");
        sb.append(getLibLng2());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libLng3</column-name><column-value><![CDATA[");
        sb.append(getLibLng3());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libDat1</column-name><column-value><![CDATA[");
        sb.append(getLibDat1());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libDat2</column-name><column-value><![CDATA[");
        sb.append(getLibDat2());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>libDat3</column-name><column-value><![CDATA[");
        sb.append(getLibDat3());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>importoNetto</column-name><column-value><![CDATA[");
        sb.append(getImportoNetto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceIVA</column-name><column-value><![CDATA[");
        sb.append(getCodiceIVA());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
