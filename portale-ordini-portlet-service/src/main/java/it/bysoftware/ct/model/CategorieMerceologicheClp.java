package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.CategorieMerceologicheLocalServiceUtil;
import it.bysoftware.ct.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class CategorieMerceologicheClp extends BaseModelImpl<CategorieMerceologiche>
    implements CategorieMerceologiche {
    private String _codiceCategoria;
    private String _descrizione;
    private BaseModel<?> _categorieMerceologicheRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public CategorieMerceologicheClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return CategorieMerceologiche.class;
    }

    @Override
    public String getModelClassName() {
        return CategorieMerceologiche.class.getName();
    }

    @Override
    public String getPrimaryKey() {
        return _codiceCategoria;
    }

    @Override
    public void setPrimaryKey(String primaryKey) {
        setCodiceCategoria(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _codiceCategoria;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((String) primaryKeyObj);
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceCategoria", getCodiceCategoria());
        attributes.put("descrizione", getDescrizione());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceCategoria = (String) attributes.get("codiceCategoria");

        if (codiceCategoria != null) {
            setCodiceCategoria(codiceCategoria);
        }

        String descrizione = (String) attributes.get("descrizione");

        if (descrizione != null) {
            setDescrizione(descrizione);
        }
    }

    @Override
    public String getCodiceCategoria() {
        return _codiceCategoria;
    }

    @Override
    public void setCodiceCategoria(String codiceCategoria) {
        _codiceCategoria = codiceCategoria;

        if (_categorieMerceologicheRemoteModel != null) {
            try {
                Class<?> clazz = _categorieMerceologicheRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceCategoria",
                        String.class);

                method.invoke(_categorieMerceologicheRemoteModel,
                    codiceCategoria);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getDescrizione() {
        return _descrizione;
    }

    @Override
    public void setDescrizione(String descrizione) {
        _descrizione = descrizione;

        if (_categorieMerceologicheRemoteModel != null) {
            try {
                Class<?> clazz = _categorieMerceologicheRemoteModel.getClass();

                Method method = clazz.getMethod("setDescrizione", String.class);

                method.invoke(_categorieMerceologicheRemoteModel, descrizione);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getCategorieMerceologicheRemoteModel() {
        return _categorieMerceologicheRemoteModel;
    }

    public void setCategorieMerceologicheRemoteModel(
        BaseModel<?> categorieMerceologicheRemoteModel) {
        _categorieMerceologicheRemoteModel = categorieMerceologicheRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _categorieMerceologicheRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_categorieMerceologicheRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            CategorieMerceologicheLocalServiceUtil.addCategorieMerceologiche(this);
        } else {
            CategorieMerceologicheLocalServiceUtil.updateCategorieMerceologiche(this);
        }
    }

    @Override
    public CategorieMerceologiche toEscapedModel() {
        return (CategorieMerceologiche) ProxyUtil.newProxyInstance(CategorieMerceologiche.class.getClassLoader(),
            new Class[] { CategorieMerceologiche.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        CategorieMerceologicheClp clone = new CategorieMerceologicheClp();

        clone.setCodiceCategoria(getCodiceCategoria());
        clone.setDescrizione(getDescrizione());

        return clone;
    }

    @Override
    public int compareTo(CategorieMerceologiche categorieMerceologiche) {
        int value = 0;

        value = getDescrizione()
                    .compareTo(categorieMerceologiche.getDescrizione());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CategorieMerceologicheClp)) {
            return false;
        }

        CategorieMerceologicheClp categorieMerceologiche = (CategorieMerceologicheClp) obj;

        String primaryKey = categorieMerceologiche.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(5);

        sb.append("{codiceCategoria=");
        sb.append(getCodiceCategoria());
        sb.append(", descrizione=");
        sb.append(getDescrizione());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(10);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.CategorieMerceologiche");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>codiceCategoria</column-name><column-value><![CDATA[");
        sb.append(getCodiceCategoria());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>descrizione</column-name><column-value><![CDATA[");
        sb.append(getDescrizione());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
