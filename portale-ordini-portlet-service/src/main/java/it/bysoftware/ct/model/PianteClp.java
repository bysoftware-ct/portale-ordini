package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.PianteLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class PianteClp extends BaseModelImpl<Piante> implements Piante {
    private long _id;
    private String _nome;
    private String _vaso;
    private double _prezzo;
    private int _altezza;
    private int _piantePianale;
    private int _pianteCarrello;
    private boolean _attiva;
    private String _foto1;
    private String _foto2;
    private String _foto3;
    private int _altezzaPianta;
    private long _idCategoria;
    private String _idFornitore;
    private double _prezzoFornitore;
    private int _disponibilita;
    private String _codice;
    private String _forma;
    private int _altezzaChioma;
    private String _tempoConsegna;
    private double _prezzoB;
    private double _percScontoMistoAziendale;
    private double _prezzoACarrello;
    private double _prezzoBCarrello;
    private long _idForma;
    private double _costoRipiano;
    private Date _ultimoAggiornamentoFoto;
    private Date _scadenzaFoto;
    private int _giorniScadenzaFoto;
    private int _sormonto2Fila;
    private int _sormonto3Fila;
    private int _sormonto4Fila;
    private int _aggiuntaRipiano;
    private String _ean;
    private boolean _passaporto;
    private boolean _bloccoDisponibilita;
    private String _pathFile;
    private boolean _obsoleto;
    private double _prezzoEtichetta;
    private String _passaportoDefault;
    private BaseModel<?> _pianteRemoteModel;
    private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;

    public PianteClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Piante.class;
    }

    @Override
    public String getModelClassName() {
        return Piante.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("nome", getNome());
        attributes.put("vaso", getVaso());
        attributes.put("prezzo", getPrezzo());
        attributes.put("altezza", getAltezza());
        attributes.put("piantePianale", getPiantePianale());
        attributes.put("pianteCarrello", getPianteCarrello());
        attributes.put("attiva", getAttiva());
        attributes.put("foto1", getFoto1());
        attributes.put("foto2", getFoto2());
        attributes.put("foto3", getFoto3());
        attributes.put("altezzaPianta", getAltezzaPianta());
        attributes.put("idCategoria", getIdCategoria());
        attributes.put("idFornitore", getIdFornitore());
        attributes.put("prezzoFornitore", getPrezzoFornitore());
        attributes.put("disponibilita", getDisponibilita());
        attributes.put("codice", getCodice());
        attributes.put("forma", getForma());
        attributes.put("altezzaChioma", getAltezzaChioma());
        attributes.put("tempoConsegna", getTempoConsegna());
        attributes.put("prezzoB", getPrezzoB());
        attributes.put("percScontoMistoAziendale", getPercScontoMistoAziendale());
        attributes.put("prezzoACarrello", getPrezzoACarrello());
        attributes.put("prezzoBCarrello", getPrezzoBCarrello());
        attributes.put("idForma", getIdForma());
        attributes.put("costoRipiano", getCostoRipiano());
        attributes.put("ultimoAggiornamentoFoto", getUltimoAggiornamentoFoto());
        attributes.put("scadenzaFoto", getScadenzaFoto());
        attributes.put("giorniScadenzaFoto", getGiorniScadenzaFoto());
        attributes.put("sormonto2Fila", getSormonto2Fila());
        attributes.put("sormonto3Fila", getSormonto3Fila());
        attributes.put("sormonto4Fila", getSormonto4Fila());
        attributes.put("aggiuntaRipiano", getAggiuntaRipiano());
        attributes.put("ean", getEan());
        attributes.put("passaporto", getPassaporto());
        attributes.put("bloccoDisponibilita", getBloccoDisponibilita());
        attributes.put("pathFile", getPathFile());
        attributes.put("obsoleto", getObsoleto());
        attributes.put("prezzoEtichetta", getPrezzoEtichetta());
        attributes.put("passaportoDefault", getPassaportoDefault());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        String nome = (String) attributes.get("nome");

        if (nome != null) {
            setNome(nome);
        }

        String vaso = (String) attributes.get("vaso");

        if (vaso != null) {
            setVaso(vaso);
        }

        Double prezzo = (Double) attributes.get("prezzo");

        if (prezzo != null) {
            setPrezzo(prezzo);
        }

        Integer altezza = (Integer) attributes.get("altezza");

        if (altezza != null) {
            setAltezza(altezza);
        }

        Integer piantePianale = (Integer) attributes.get("piantePianale");

        if (piantePianale != null) {
            setPiantePianale(piantePianale);
        }

        Integer pianteCarrello = (Integer) attributes.get("pianteCarrello");

        if (pianteCarrello != null) {
            setPianteCarrello(pianteCarrello);
        }

        Boolean attiva = (Boolean) attributes.get("attiva");

        if (attiva != null) {
            setAttiva(attiva);
        }

        String foto1 = (String) attributes.get("foto1");

        if (foto1 != null) {
            setFoto1(foto1);
        }

        String foto2 = (String) attributes.get("foto2");

        if (foto2 != null) {
            setFoto2(foto2);
        }

        String foto3 = (String) attributes.get("foto3");

        if (foto3 != null) {
            setFoto3(foto3);
        }

        Integer altezzaPianta = (Integer) attributes.get("altezzaPianta");

        if (altezzaPianta != null) {
            setAltezzaPianta(altezzaPianta);
        }

        Long idCategoria = (Long) attributes.get("idCategoria");

        if (idCategoria != null) {
            setIdCategoria(idCategoria);
        }

        String idFornitore = (String) attributes.get("idFornitore");

        if (idFornitore != null) {
            setIdFornitore(idFornitore);
        }

        Double prezzoFornitore = (Double) attributes.get("prezzoFornitore");

        if (prezzoFornitore != null) {
            setPrezzoFornitore(prezzoFornitore);
        }

        Integer disponibilita = (Integer) attributes.get("disponibilita");

        if (disponibilita != null) {
            setDisponibilita(disponibilita);
        }

        String codice = (String) attributes.get("codice");

        if (codice != null) {
            setCodice(codice);
        }

        String forma = (String) attributes.get("forma");

        if (forma != null) {
            setForma(forma);
        }

        Integer altezzaChioma = (Integer) attributes.get("altezzaChioma");

        if (altezzaChioma != null) {
            setAltezzaChioma(altezzaChioma);
        }

        String tempoConsegna = (String) attributes.get("tempoConsegna");

        if (tempoConsegna != null) {
            setTempoConsegna(tempoConsegna);
        }

        Double prezzoB = (Double) attributes.get("prezzoB");

        if (prezzoB != null) {
            setPrezzoB(prezzoB);
        }

        Double percScontoMistoAziendale = (Double) attributes.get(
                "percScontoMistoAziendale");

        if (percScontoMistoAziendale != null) {
            setPercScontoMistoAziendale(percScontoMistoAziendale);
        }

        Double prezzoACarrello = (Double) attributes.get("prezzoACarrello");

        if (prezzoACarrello != null) {
            setPrezzoACarrello(prezzoACarrello);
        }

        Double prezzoBCarrello = (Double) attributes.get("prezzoBCarrello");

        if (prezzoBCarrello != null) {
            setPrezzoBCarrello(prezzoBCarrello);
        }

        Long idForma = (Long) attributes.get("idForma");

        if (idForma != null) {
            setIdForma(idForma);
        }

        Double costoRipiano = (Double) attributes.get("costoRipiano");

        if (costoRipiano != null) {
            setCostoRipiano(costoRipiano);
        }

        Date ultimoAggiornamentoFoto = (Date) attributes.get(
                "ultimoAggiornamentoFoto");

        if (ultimoAggiornamentoFoto != null) {
            setUltimoAggiornamentoFoto(ultimoAggiornamentoFoto);
        }

        Date scadenzaFoto = (Date) attributes.get("scadenzaFoto");

        if (scadenzaFoto != null) {
            setScadenzaFoto(scadenzaFoto);
        }

        Integer giorniScadenzaFoto = (Integer) attributes.get(
                "giorniScadenzaFoto");

        if (giorniScadenzaFoto != null) {
            setGiorniScadenzaFoto(giorniScadenzaFoto);
        }

        Integer sormonto2Fila = (Integer) attributes.get("sormonto2Fila");

        if (sormonto2Fila != null) {
            setSormonto2Fila(sormonto2Fila);
        }

        Integer sormonto3Fila = (Integer) attributes.get("sormonto3Fila");

        if (sormonto3Fila != null) {
            setSormonto3Fila(sormonto3Fila);
        }

        Integer sormonto4Fila = (Integer) attributes.get("sormonto4Fila");

        if (sormonto4Fila != null) {
            setSormonto4Fila(sormonto4Fila);
        }

        Integer aggiuntaRipiano = (Integer) attributes.get("aggiuntaRipiano");

        if (aggiuntaRipiano != null) {
            setAggiuntaRipiano(aggiuntaRipiano);
        }

        String ean = (String) attributes.get("ean");

        if (ean != null) {
            setEan(ean);
        }

        Boolean passaporto = (Boolean) attributes.get("passaporto");

        if (passaporto != null) {
            setPassaporto(passaporto);
        }

        Boolean bloccoDisponibilita = (Boolean) attributes.get(
                "bloccoDisponibilita");

        if (bloccoDisponibilita != null) {
            setBloccoDisponibilita(bloccoDisponibilita);
        }

        String pathFile = (String) attributes.get("pathFile");

        if (pathFile != null) {
            setPathFile(pathFile);
        }

        Boolean obsoleto = (Boolean) attributes.get("obsoleto");

        if (obsoleto != null) {
            setObsoleto(obsoleto);
        }

        Double prezzoEtichetta = (Double) attributes.get("prezzoEtichetta");

        if (prezzoEtichetta != null) {
            setPrezzoEtichetta(prezzoEtichetta);
        }

        String passaportoDefault = (String) attributes.get("passaportoDefault");

        if (passaportoDefault != null) {
            setPassaportoDefault(passaportoDefault);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_pianteRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNome() {
        return _nome;
    }

    @Override
    public void setNome(String nome) {
        _nome = nome;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setNome", String.class);

                method.invoke(_pianteRemoteModel, nome);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getVaso() {
        return _vaso;
    }

    @Override
    public void setVaso(String vaso) {
        _vaso = vaso;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setVaso", String.class);

                method.invoke(_pianteRemoteModel, vaso);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrezzo() {
        return _prezzo;
    }

    @Override
    public void setPrezzo(double prezzo) {
        _prezzo = prezzo;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setPrezzo", double.class);

                method.invoke(_pianteRemoteModel, prezzo);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getAltezza() {
        return _altezza;
    }

    @Override
    public void setAltezza(int altezza) {
        _altezza = altezza;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setAltezza", int.class);

                method.invoke(_pianteRemoteModel, altezza);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getPiantePianale() {
        return _piantePianale;
    }

    @Override
    public void setPiantePianale(int piantePianale) {
        _piantePianale = piantePianale;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setPiantePianale", int.class);

                method.invoke(_pianteRemoteModel, piantePianale);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getPianteCarrello() {
        return _pianteCarrello;
    }

    @Override
    public void setPianteCarrello(int pianteCarrello) {
        _pianteCarrello = pianteCarrello;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setPianteCarrello", int.class);

                method.invoke(_pianteRemoteModel, pianteCarrello);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getAttiva() {
        return _attiva;
    }

    @Override
    public boolean isAttiva() {
        return _attiva;
    }

    @Override
    public void setAttiva(boolean attiva) {
        _attiva = attiva;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setAttiva", boolean.class);

                method.invoke(_pianteRemoteModel, attiva);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getFoto1() {
        return _foto1;
    }

    @Override
    public void setFoto1(String foto1) {
        _foto1 = foto1;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setFoto1", String.class);

                method.invoke(_pianteRemoteModel, foto1);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getFoto2() {
        return _foto2;
    }

    @Override
    public void setFoto2(String foto2) {
        _foto2 = foto2;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setFoto2", String.class);

                method.invoke(_pianteRemoteModel, foto2);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getFoto3() {
        return _foto3;
    }

    @Override
    public void setFoto3(String foto3) {
        _foto3 = foto3;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setFoto3", String.class);

                method.invoke(_pianteRemoteModel, foto3);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getAltezzaPianta() {
        return _altezzaPianta;
    }

    @Override
    public void setAltezzaPianta(int altezzaPianta) {
        _altezzaPianta = altezzaPianta;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setAltezzaPianta", int.class);

                method.invoke(_pianteRemoteModel, altezzaPianta);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getIdCategoria() {
        return _idCategoria;
    }

    @Override
    public void setIdCategoria(long idCategoria) {
        _idCategoria = idCategoria;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setIdCategoria", long.class);

                method.invoke(_pianteRemoteModel, idCategoria);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getIdFornitore() {
        return _idFornitore;
    }

    @Override
    public void setIdFornitore(String idFornitore) {
        _idFornitore = idFornitore;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setIdFornitore", String.class);

                method.invoke(_pianteRemoteModel, idFornitore);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrezzoFornitore() {
        return _prezzoFornitore;
    }

    @Override
    public void setPrezzoFornitore(double prezzoFornitore) {
        _prezzoFornitore = prezzoFornitore;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setPrezzoFornitore",
                        double.class);

                method.invoke(_pianteRemoteModel, prezzoFornitore);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getDisponibilita() {
        return _disponibilita;
    }

    @Override
    public void setDisponibilita(int disponibilita) {
        _disponibilita = disponibilita;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setDisponibilita", int.class);

                method.invoke(_pianteRemoteModel, disponibilita);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCodice() {
        return _codice;
    }

    @Override
    public void setCodice(String codice) {
        _codice = codice;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setCodice", String.class);

                method.invoke(_pianteRemoteModel, codice);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getForma() {
        return _forma;
    }

    @Override
    public void setForma(String forma) {
        _forma = forma;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setForma", String.class);

                method.invoke(_pianteRemoteModel, forma);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getAltezzaChioma() {
        return _altezzaChioma;
    }

    @Override
    public void setAltezzaChioma(int altezzaChioma) {
        _altezzaChioma = altezzaChioma;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setAltezzaChioma", int.class);

                method.invoke(_pianteRemoteModel, altezzaChioma);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getTempoConsegna() {
        return _tempoConsegna;
    }

    @Override
    public void setTempoConsegna(String tempoConsegna) {
        _tempoConsegna = tempoConsegna;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setTempoConsegna", String.class);

                method.invoke(_pianteRemoteModel, tempoConsegna);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrezzoB() {
        return _prezzoB;
    }

    @Override
    public void setPrezzoB(double prezzoB) {
        _prezzoB = prezzoB;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setPrezzoB", double.class);

                method.invoke(_pianteRemoteModel, prezzoB);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPercScontoMistoAziendale() {
        return _percScontoMistoAziendale;
    }

    @Override
    public void setPercScontoMistoAziendale(double percScontoMistoAziendale) {
        _percScontoMistoAziendale = percScontoMistoAziendale;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setPercScontoMistoAziendale",
                        double.class);

                method.invoke(_pianteRemoteModel, percScontoMistoAziendale);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrezzoACarrello() {
        return _prezzoACarrello;
    }

    @Override
    public void setPrezzoACarrello(double prezzoACarrello) {
        _prezzoACarrello = prezzoACarrello;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setPrezzoACarrello",
                        double.class);

                method.invoke(_pianteRemoteModel, prezzoACarrello);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrezzoBCarrello() {
        return _prezzoBCarrello;
    }

    @Override
    public void setPrezzoBCarrello(double prezzoBCarrello) {
        _prezzoBCarrello = prezzoBCarrello;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setPrezzoBCarrello",
                        double.class);

                method.invoke(_pianteRemoteModel, prezzoBCarrello);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getIdForma() {
        return _idForma;
    }

    @Override
    public void setIdForma(long idForma) {
        _idForma = idForma;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setIdForma", long.class);

                method.invoke(_pianteRemoteModel, idForma);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getCostoRipiano() {
        return _costoRipiano;
    }

    @Override
    public void setCostoRipiano(double costoRipiano) {
        _costoRipiano = costoRipiano;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setCostoRipiano", double.class);

                method.invoke(_pianteRemoteModel, costoRipiano);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getUltimoAggiornamentoFoto() {
        return _ultimoAggiornamentoFoto;
    }

    @Override
    public void setUltimoAggiornamentoFoto(Date ultimoAggiornamentoFoto) {
        _ultimoAggiornamentoFoto = ultimoAggiornamentoFoto;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setUltimoAggiornamentoFoto",
                        Date.class);

                method.invoke(_pianteRemoteModel, ultimoAggiornamentoFoto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getScadenzaFoto() {
        return _scadenzaFoto;
    }

    @Override
    public void setScadenzaFoto(Date scadenzaFoto) {
        _scadenzaFoto = scadenzaFoto;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setScadenzaFoto", Date.class);

                method.invoke(_pianteRemoteModel, scadenzaFoto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getGiorniScadenzaFoto() {
        return _giorniScadenzaFoto;
    }

    @Override
    public void setGiorniScadenzaFoto(int giorniScadenzaFoto) {
        _giorniScadenzaFoto = giorniScadenzaFoto;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setGiorniScadenzaFoto",
                        int.class);

                method.invoke(_pianteRemoteModel, giorniScadenzaFoto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getSormonto2Fila() {
        return _sormonto2Fila;
    }

    @Override
    public void setSormonto2Fila(int sormonto2Fila) {
        _sormonto2Fila = sormonto2Fila;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setSormonto2Fila", int.class);

                method.invoke(_pianteRemoteModel, sormonto2Fila);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getSormonto3Fila() {
        return _sormonto3Fila;
    }

    @Override
    public void setSormonto3Fila(int sormonto3Fila) {
        _sormonto3Fila = sormonto3Fila;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setSormonto3Fila", int.class);

                method.invoke(_pianteRemoteModel, sormonto3Fila);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getSormonto4Fila() {
        return _sormonto4Fila;
    }

    @Override
    public void setSormonto4Fila(int sormonto4Fila) {
        _sormonto4Fila = sormonto4Fila;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setSormonto4Fila", int.class);

                method.invoke(_pianteRemoteModel, sormonto4Fila);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getAggiuntaRipiano() {
        return _aggiuntaRipiano;
    }

    @Override
    public void setAggiuntaRipiano(int aggiuntaRipiano) {
        _aggiuntaRipiano = aggiuntaRipiano;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setAggiuntaRipiano", int.class);

                method.invoke(_pianteRemoteModel, aggiuntaRipiano);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getEan() {
        return _ean;
    }

    @Override
    public void setEan(String ean) {
        _ean = ean;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setEan", String.class);

                method.invoke(_pianteRemoteModel, ean);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getPassaporto() {
        return _passaporto;
    }

    @Override
    public boolean isPassaporto() {
        return _passaporto;
    }

    @Override
    public void setPassaporto(boolean passaporto) {
        _passaporto = passaporto;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setPassaporto", boolean.class);

                method.invoke(_pianteRemoteModel, passaporto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getBloccoDisponibilita() {
        return _bloccoDisponibilita;
    }

    @Override
    public boolean isBloccoDisponibilita() {
        return _bloccoDisponibilita;
    }

    @Override
    public void setBloccoDisponibilita(boolean bloccoDisponibilita) {
        _bloccoDisponibilita = bloccoDisponibilita;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setBloccoDisponibilita",
                        boolean.class);

                method.invoke(_pianteRemoteModel, bloccoDisponibilita);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getPathFile() {
        return _pathFile;
    }

    @Override
    public void setPathFile(String pathFile) {
        _pathFile = pathFile;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setPathFile", String.class);

                method.invoke(_pianteRemoteModel, pathFile);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getObsoleto() {
        return _obsoleto;
    }

    @Override
    public boolean isObsoleto() {
        return _obsoleto;
    }

    @Override
    public void setObsoleto(boolean obsoleto) {
        _obsoleto = obsoleto;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setObsoleto", boolean.class);

                method.invoke(_pianteRemoteModel, obsoleto);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrezzoEtichetta() {
        return _prezzoEtichetta;
    }

    @Override
    public void setPrezzoEtichetta(double prezzoEtichetta) {
        _prezzoEtichetta = prezzoEtichetta;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setPrezzoEtichetta",
                        double.class);

                method.invoke(_pianteRemoteModel, prezzoEtichetta);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getPassaportoDefault() {
        return _passaportoDefault;
    }

    @Override
    public void setPassaportoDefault(String passaportoDefault) {
        _passaportoDefault = passaportoDefault;

        if (_pianteRemoteModel != null) {
            try {
                Class<?> clazz = _pianteRemoteModel.getClass();

                Method method = clazz.getMethod("setPassaportoDefault",
                        String.class);

                method.invoke(_pianteRemoteModel, passaportoDefault);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getPianteRemoteModel() {
        return _pianteRemoteModel;
    }

    public void setPianteRemoteModel(BaseModel<?> pianteRemoteModel) {
        _pianteRemoteModel = pianteRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _pianteRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_pianteRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            PianteLocalServiceUtil.addPiante(this);
        } else {
            PianteLocalServiceUtil.updatePiante(this);
        }
    }

    @Override
    public Piante toEscapedModel() {
        return (Piante) ProxyUtil.newProxyInstance(Piante.class.getClassLoader(),
            new Class[] { Piante.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        PianteClp clone = new PianteClp();

        clone.setId(getId());
        clone.setNome(getNome());
        clone.setVaso(getVaso());
        clone.setPrezzo(getPrezzo());
        clone.setAltezza(getAltezza());
        clone.setPiantePianale(getPiantePianale());
        clone.setPianteCarrello(getPianteCarrello());
        clone.setAttiva(getAttiva());
        clone.setFoto1(getFoto1());
        clone.setFoto2(getFoto2());
        clone.setFoto3(getFoto3());
        clone.setAltezzaPianta(getAltezzaPianta());
        clone.setIdCategoria(getIdCategoria());
        clone.setIdFornitore(getIdFornitore());
        clone.setPrezzoFornitore(getPrezzoFornitore());
        clone.setDisponibilita(getDisponibilita());
        clone.setCodice(getCodice());
        clone.setForma(getForma());
        clone.setAltezzaChioma(getAltezzaChioma());
        clone.setTempoConsegna(getTempoConsegna());
        clone.setPrezzoB(getPrezzoB());
        clone.setPercScontoMistoAziendale(getPercScontoMistoAziendale());
        clone.setPrezzoACarrello(getPrezzoACarrello());
        clone.setPrezzoBCarrello(getPrezzoBCarrello());
        clone.setIdForma(getIdForma());
        clone.setCostoRipiano(getCostoRipiano());
        clone.setUltimoAggiornamentoFoto(getUltimoAggiornamentoFoto());
        clone.setScadenzaFoto(getScadenzaFoto());
        clone.setGiorniScadenzaFoto(getGiorniScadenzaFoto());
        clone.setSormonto2Fila(getSormonto2Fila());
        clone.setSormonto3Fila(getSormonto3Fila());
        clone.setSormonto4Fila(getSormonto4Fila());
        clone.setAggiuntaRipiano(getAggiuntaRipiano());
        clone.setEan(getEan());
        clone.setPassaporto(getPassaporto());
        clone.setBloccoDisponibilita(getBloccoDisponibilita());
        clone.setPathFile(getPathFile());
        clone.setObsoleto(getObsoleto());
        clone.setPrezzoEtichetta(getPrezzoEtichetta());
        clone.setPassaportoDefault(getPassaportoDefault());

        return clone;
    }

    @Override
    public int compareTo(Piante piante) {
        int value = 0;

        value = getNome().compareTo(piante.getNome());

        if (value != 0) {
            return value;
        }

        value = getForma().compareTo(piante.getForma());

        if (value != 0) {
            return value;
        }

        value = getVaso().compareTo(piante.getVaso());

        if (value != 0) {
            return value;
        }

        if (getAltezzaPianta() < piante.getAltezzaPianta()) {
            value = -1;
        } else if (getAltezzaPianta() > piante.getAltezzaPianta()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PianteClp)) {
            return false;
        }

        PianteClp piante = (PianteClp) obj;

        long primaryKey = piante.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(81);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", nome=");
        sb.append(getNome());
        sb.append(", vaso=");
        sb.append(getVaso());
        sb.append(", prezzo=");
        sb.append(getPrezzo());
        sb.append(", altezza=");
        sb.append(getAltezza());
        sb.append(", piantePianale=");
        sb.append(getPiantePianale());
        sb.append(", pianteCarrello=");
        sb.append(getPianteCarrello());
        sb.append(", attiva=");
        sb.append(getAttiva());
        sb.append(", foto1=");
        sb.append(getFoto1());
        sb.append(", foto2=");
        sb.append(getFoto2());
        sb.append(", foto3=");
        sb.append(getFoto3());
        sb.append(", altezzaPianta=");
        sb.append(getAltezzaPianta());
        sb.append(", idCategoria=");
        sb.append(getIdCategoria());
        sb.append(", idFornitore=");
        sb.append(getIdFornitore());
        sb.append(", prezzoFornitore=");
        sb.append(getPrezzoFornitore());
        sb.append(", disponibilita=");
        sb.append(getDisponibilita());
        sb.append(", codice=");
        sb.append(getCodice());
        sb.append(", forma=");
        sb.append(getForma());
        sb.append(", altezzaChioma=");
        sb.append(getAltezzaChioma());
        sb.append(", tempoConsegna=");
        sb.append(getTempoConsegna());
        sb.append(", prezzoB=");
        sb.append(getPrezzoB());
        sb.append(", percScontoMistoAziendale=");
        sb.append(getPercScontoMistoAziendale());
        sb.append(", prezzoACarrello=");
        sb.append(getPrezzoACarrello());
        sb.append(", prezzoBCarrello=");
        sb.append(getPrezzoBCarrello());
        sb.append(", idForma=");
        sb.append(getIdForma());
        sb.append(", costoRipiano=");
        sb.append(getCostoRipiano());
        sb.append(", ultimoAggiornamentoFoto=");
        sb.append(getUltimoAggiornamentoFoto());
        sb.append(", scadenzaFoto=");
        sb.append(getScadenzaFoto());
        sb.append(", giorniScadenzaFoto=");
        sb.append(getGiorniScadenzaFoto());
        sb.append(", sormonto2Fila=");
        sb.append(getSormonto2Fila());
        sb.append(", sormonto3Fila=");
        sb.append(getSormonto3Fila());
        sb.append(", sormonto4Fila=");
        sb.append(getSormonto4Fila());
        sb.append(", aggiuntaRipiano=");
        sb.append(getAggiuntaRipiano());
        sb.append(", ean=");
        sb.append(getEan());
        sb.append(", passaporto=");
        sb.append(getPassaporto());
        sb.append(", bloccoDisponibilita=");
        sb.append(getBloccoDisponibilita());
        sb.append(", pathFile=");
        sb.append(getPathFile());
        sb.append(", obsoleto=");
        sb.append(getObsoleto());
        sb.append(", prezzoEtichetta=");
        sb.append(getPrezzoEtichetta());
        sb.append(", passaportoDefault=");
        sb.append(getPassaportoDefault());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(124);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.Piante");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>nome</column-name><column-value><![CDATA[");
        sb.append(getNome());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>vaso</column-name><column-value><![CDATA[");
        sb.append(getVaso());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>prezzo</column-name><column-value><![CDATA[");
        sb.append(getPrezzo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>altezza</column-name><column-value><![CDATA[");
        sb.append(getAltezza());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>piantePianale</column-name><column-value><![CDATA[");
        sb.append(getPiantePianale());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>pianteCarrello</column-name><column-value><![CDATA[");
        sb.append(getPianteCarrello());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>attiva</column-name><column-value><![CDATA[");
        sb.append(getAttiva());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>foto1</column-name><column-value><![CDATA[");
        sb.append(getFoto1());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>foto2</column-name><column-value><![CDATA[");
        sb.append(getFoto2());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>foto3</column-name><column-value><![CDATA[");
        sb.append(getFoto3());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>altezzaPianta</column-name><column-value><![CDATA[");
        sb.append(getAltezzaPianta());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>idCategoria</column-name><column-value><![CDATA[");
        sb.append(getIdCategoria());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>idFornitore</column-name><column-value><![CDATA[");
        sb.append(getIdFornitore());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>prezzoFornitore</column-name><column-value><![CDATA[");
        sb.append(getPrezzoFornitore());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>disponibilita</column-name><column-value><![CDATA[");
        sb.append(getDisponibilita());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codice</column-name><column-value><![CDATA[");
        sb.append(getCodice());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>forma</column-name><column-value><![CDATA[");
        sb.append(getForma());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>altezzaChioma</column-name><column-value><![CDATA[");
        sb.append(getAltezzaChioma());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tempoConsegna</column-name><column-value><![CDATA[");
        sb.append(getTempoConsegna());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>prezzoB</column-name><column-value><![CDATA[");
        sb.append(getPrezzoB());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>percScontoMistoAziendale</column-name><column-value><![CDATA[");
        sb.append(getPercScontoMistoAziendale());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>prezzoACarrello</column-name><column-value><![CDATA[");
        sb.append(getPrezzoACarrello());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>prezzoBCarrello</column-name><column-value><![CDATA[");
        sb.append(getPrezzoBCarrello());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>idForma</column-name><column-value><![CDATA[");
        sb.append(getIdForma());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>costoRipiano</column-name><column-value><![CDATA[");
        sb.append(getCostoRipiano());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>ultimoAggiornamentoFoto</column-name><column-value><![CDATA[");
        sb.append(getUltimoAggiornamentoFoto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>scadenzaFoto</column-name><column-value><![CDATA[");
        sb.append(getScadenzaFoto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>giorniScadenzaFoto</column-name><column-value><![CDATA[");
        sb.append(getGiorniScadenzaFoto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>sormonto2Fila</column-name><column-value><![CDATA[");
        sb.append(getSormonto2Fila());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>sormonto3Fila</column-name><column-value><![CDATA[");
        sb.append(getSormonto3Fila());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>sormonto4Fila</column-name><column-value><![CDATA[");
        sb.append(getSormonto4Fila());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>aggiuntaRipiano</column-name><column-value><![CDATA[");
        sb.append(getAggiuntaRipiano());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>ean</column-name><column-value><![CDATA[");
        sb.append(getEan());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>passaporto</column-name><column-value><![CDATA[");
        sb.append(getPassaporto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>bloccoDisponibilita</column-name><column-value><![CDATA[");
        sb.append(getBloccoDisponibilita());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>pathFile</column-name><column-value><![CDATA[");
        sb.append(getPathFile());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>obsoleto</column-name><column-value><![CDATA[");
        sb.append(getObsoleto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>prezzoEtichetta</column-name><column-value><![CDATA[");
        sb.append(getPrezzoEtichetta());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>passaportoDefault</column-name><column-value><![CDATA[");
        sb.append(getPassaportoDefault());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
