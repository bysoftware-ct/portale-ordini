package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ClientiFornitoriDatiAgg}.
 * </p>
 *
 * @author Mario Torrisi
 * @see ClientiFornitoriDatiAgg
 * @generated
 */
public class ClientiFornitoriDatiAggWrapper implements ClientiFornitoriDatiAgg,
    ModelWrapper<ClientiFornitoriDatiAgg> {
    private ClientiFornitoriDatiAgg _clientiFornitoriDatiAgg;

    public ClientiFornitoriDatiAggWrapper(
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg) {
        _clientiFornitoriDatiAgg = clientiFornitoriDatiAgg;
    }

    @Override
    public Class<?> getModelClass() {
        return ClientiFornitoriDatiAgg.class;
    }

    @Override
    public String getModelClassName() {
        return ClientiFornitoriDatiAgg.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceAnagrafica", getCodiceAnagrafica());
        attributes.put("fornitore", getFornitore());
        attributes.put("codiceAgente", getCodiceAgente());
        attributes.put("codiceEsenzione", getCodiceEsenzione());
        attributes.put("categoriaEconomica", getCategoriaEconomica());
        attributes.put("codiceRegionale", getCodiceRegionale());
        attributes.put("codicePuntoVenditaGT", getCodicePuntoVenditaGT());
        attributes.put("tipoPagamento", getTipoPagamento());
        attributes.put("associato", getAssociato());
        attributes.put("sconto", getSconto());
        attributes.put("codiceIBAN", getCodiceIBAN());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceAnagrafica = (String) attributes.get("codiceAnagrafica");

        if (codiceAnagrafica != null) {
            setCodiceAnagrafica(codiceAnagrafica);
        }

        Boolean fornitore = (Boolean) attributes.get("fornitore");

        if (fornitore != null) {
            setFornitore(fornitore);
        }

        String codiceAgente = (String) attributes.get("codiceAgente");

        if (codiceAgente != null) {
            setCodiceAgente(codiceAgente);
        }

        String codiceEsenzione = (String) attributes.get("codiceEsenzione");

        if (codiceEsenzione != null) {
            setCodiceEsenzione(codiceEsenzione);
        }

        String categoriaEconomica = (String) attributes.get(
                "categoriaEconomica");

        if (categoriaEconomica != null) {
            setCategoriaEconomica(categoriaEconomica);
        }

        String codiceRegionale = (String) attributes.get("codiceRegionale");

        if (codiceRegionale != null) {
            setCodiceRegionale(codiceRegionale);
        }

        String codicePuntoVenditaGT = (String) attributes.get(
                "codicePuntoVenditaGT");

        if (codicePuntoVenditaGT != null) {
            setCodicePuntoVenditaGT(codicePuntoVenditaGT);
        }

        String tipoPagamento = (String) attributes.get("tipoPagamento");

        if (tipoPagamento != null) {
            setTipoPagamento(tipoPagamento);
        }

        Boolean associato = (Boolean) attributes.get("associato");

        if (associato != null) {
            setAssociato(associato);
        }

        Double sconto = (Double) attributes.get("sconto");

        if (sconto != null) {
            setSconto(sconto);
        }

        String codiceIBAN = (String) attributes.get("codiceIBAN");

        if (codiceIBAN != null) {
            setCodiceIBAN(codiceIBAN);
        }
    }

    /**
    * Returns the primary key of this clienti fornitori dati agg.
    *
    * @return the primary key of this clienti fornitori dati agg
    */
    @Override
    public it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK getPrimaryKey() {
        return _clientiFornitoriDatiAgg.getPrimaryKey();
    }

    /**
    * Sets the primary key of this clienti fornitori dati agg.
    *
    * @param primaryKey the primary key of this clienti fornitori dati agg
    */
    @Override
    public void setPrimaryKey(
        it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK primaryKey) {
        _clientiFornitoriDatiAgg.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the codice anagrafica of this clienti fornitori dati agg.
    *
    * @return the codice anagrafica of this clienti fornitori dati agg
    */
    @Override
    public java.lang.String getCodiceAnagrafica() {
        return _clientiFornitoriDatiAgg.getCodiceAnagrafica();
    }

    /**
    * Sets the codice anagrafica of this clienti fornitori dati agg.
    *
    * @param codiceAnagrafica the codice anagrafica of this clienti fornitori dati agg
    */
    @Override
    public void setCodiceAnagrafica(java.lang.String codiceAnagrafica) {
        _clientiFornitoriDatiAgg.setCodiceAnagrafica(codiceAnagrafica);
    }

    /**
    * Returns the fornitore of this clienti fornitori dati agg.
    *
    * @return the fornitore of this clienti fornitori dati agg
    */
    @Override
    public boolean getFornitore() {
        return _clientiFornitoriDatiAgg.getFornitore();
    }

    /**
    * Returns <code>true</code> if this clienti fornitori dati agg is fornitore.
    *
    * @return <code>true</code> if this clienti fornitori dati agg is fornitore; <code>false</code> otherwise
    */
    @Override
    public boolean isFornitore() {
        return _clientiFornitoriDatiAgg.isFornitore();
    }

    /**
    * Sets whether this clienti fornitori dati agg is fornitore.
    *
    * @param fornitore the fornitore of this clienti fornitori dati agg
    */
    @Override
    public void setFornitore(boolean fornitore) {
        _clientiFornitoriDatiAgg.setFornitore(fornitore);
    }

    /**
    * Returns the codice agente of this clienti fornitori dati agg.
    *
    * @return the codice agente of this clienti fornitori dati agg
    */
    @Override
    public java.lang.String getCodiceAgente() {
        return _clientiFornitoriDatiAgg.getCodiceAgente();
    }

    /**
    * Sets the codice agente of this clienti fornitori dati agg.
    *
    * @param codiceAgente the codice agente of this clienti fornitori dati agg
    */
    @Override
    public void setCodiceAgente(java.lang.String codiceAgente) {
        _clientiFornitoriDatiAgg.setCodiceAgente(codiceAgente);
    }

    /**
    * Returns the codice esenzione of this clienti fornitori dati agg.
    *
    * @return the codice esenzione of this clienti fornitori dati agg
    */
    @Override
    public java.lang.String getCodiceEsenzione() {
        return _clientiFornitoriDatiAgg.getCodiceEsenzione();
    }

    /**
    * Sets the codice esenzione of this clienti fornitori dati agg.
    *
    * @param codiceEsenzione the codice esenzione of this clienti fornitori dati agg
    */
    @Override
    public void setCodiceEsenzione(java.lang.String codiceEsenzione) {
        _clientiFornitoriDatiAgg.setCodiceEsenzione(codiceEsenzione);
    }

    /**
    * Returns the categoria economica of this clienti fornitori dati agg.
    *
    * @return the categoria economica of this clienti fornitori dati agg
    */
    @Override
    public java.lang.String getCategoriaEconomica() {
        return _clientiFornitoriDatiAgg.getCategoriaEconomica();
    }

    /**
    * Sets the categoria economica of this clienti fornitori dati agg.
    *
    * @param categoriaEconomica the categoria economica of this clienti fornitori dati agg
    */
    @Override
    public void setCategoriaEconomica(java.lang.String categoriaEconomica) {
        _clientiFornitoriDatiAgg.setCategoriaEconomica(categoriaEconomica);
    }

    /**
    * Returns the codice regionale of this clienti fornitori dati agg.
    *
    * @return the codice regionale of this clienti fornitori dati agg
    */
    @Override
    public java.lang.String getCodiceRegionale() {
        return _clientiFornitoriDatiAgg.getCodiceRegionale();
    }

    /**
    * Sets the codice regionale of this clienti fornitori dati agg.
    *
    * @param codiceRegionale the codice regionale of this clienti fornitori dati agg
    */
    @Override
    public void setCodiceRegionale(java.lang.String codiceRegionale) {
        _clientiFornitoriDatiAgg.setCodiceRegionale(codiceRegionale);
    }

    /**
    * Returns the codice punto vendita g t of this clienti fornitori dati agg.
    *
    * @return the codice punto vendita g t of this clienti fornitori dati agg
    */
    @Override
    public java.lang.String getCodicePuntoVenditaGT() {
        return _clientiFornitoriDatiAgg.getCodicePuntoVenditaGT();
    }

    /**
    * Sets the codice punto vendita g t of this clienti fornitori dati agg.
    *
    * @param codicePuntoVenditaGT the codice punto vendita g t of this clienti fornitori dati agg
    */
    @Override
    public void setCodicePuntoVenditaGT(java.lang.String codicePuntoVenditaGT) {
        _clientiFornitoriDatiAgg.setCodicePuntoVenditaGT(codicePuntoVenditaGT);
    }

    /**
    * Returns the tipo pagamento of this clienti fornitori dati agg.
    *
    * @return the tipo pagamento of this clienti fornitori dati agg
    */
    @Override
    public java.lang.String getTipoPagamento() {
        return _clientiFornitoriDatiAgg.getTipoPagamento();
    }

    /**
    * Sets the tipo pagamento of this clienti fornitori dati agg.
    *
    * @param tipoPagamento the tipo pagamento of this clienti fornitori dati agg
    */
    @Override
    public void setTipoPagamento(java.lang.String tipoPagamento) {
        _clientiFornitoriDatiAgg.setTipoPagamento(tipoPagamento);
    }

    /**
    * Returns the associato of this clienti fornitori dati agg.
    *
    * @return the associato of this clienti fornitori dati agg
    */
    @Override
    public boolean getAssociato() {
        return _clientiFornitoriDatiAgg.getAssociato();
    }

    /**
    * Returns <code>true</code> if this clienti fornitori dati agg is associato.
    *
    * @return <code>true</code> if this clienti fornitori dati agg is associato; <code>false</code> otherwise
    */
    @Override
    public boolean isAssociato() {
        return _clientiFornitoriDatiAgg.isAssociato();
    }

    /**
    * Sets whether this clienti fornitori dati agg is associato.
    *
    * @param associato the associato of this clienti fornitori dati agg
    */
    @Override
    public void setAssociato(boolean associato) {
        _clientiFornitoriDatiAgg.setAssociato(associato);
    }

    /**
    * Returns the sconto of this clienti fornitori dati agg.
    *
    * @return the sconto of this clienti fornitori dati agg
    */
    @Override
    public double getSconto() {
        return _clientiFornitoriDatiAgg.getSconto();
    }

    /**
    * Sets the sconto of this clienti fornitori dati agg.
    *
    * @param sconto the sconto of this clienti fornitori dati agg
    */
    @Override
    public void setSconto(double sconto) {
        _clientiFornitoriDatiAgg.setSconto(sconto);
    }

    /**
    * Returns the codice i b a n of this clienti fornitori dati agg.
    *
    * @return the codice i b a n of this clienti fornitori dati agg
    */
    @Override
    public java.lang.String getCodiceIBAN() {
        return _clientiFornitoriDatiAgg.getCodiceIBAN();
    }

    /**
    * Sets the codice i b a n of this clienti fornitori dati agg.
    *
    * @param codiceIBAN the codice i b a n of this clienti fornitori dati agg
    */
    @Override
    public void setCodiceIBAN(java.lang.String codiceIBAN) {
        _clientiFornitoriDatiAgg.setCodiceIBAN(codiceIBAN);
    }

    @Override
    public boolean isNew() {
        return _clientiFornitoriDatiAgg.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _clientiFornitoriDatiAgg.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _clientiFornitoriDatiAgg.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _clientiFornitoriDatiAgg.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _clientiFornitoriDatiAgg.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _clientiFornitoriDatiAgg.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _clientiFornitoriDatiAgg.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _clientiFornitoriDatiAgg.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _clientiFornitoriDatiAgg.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _clientiFornitoriDatiAgg.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _clientiFornitoriDatiAgg.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new ClientiFornitoriDatiAggWrapper((ClientiFornitoriDatiAgg) _clientiFornitoriDatiAgg.clone());
    }

    @Override
    public int compareTo(
        it.bysoftware.ct.model.ClientiFornitoriDatiAgg clientiFornitoriDatiAgg) {
        return _clientiFornitoriDatiAgg.compareTo(clientiFornitoriDatiAgg);
    }

    @Override
    public int hashCode() {
        return _clientiFornitoriDatiAgg.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.ClientiFornitoriDatiAgg> toCacheModel() {
        return _clientiFornitoriDatiAgg.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.ClientiFornitoriDatiAgg toEscapedModel() {
        return new ClientiFornitoriDatiAggWrapper(_clientiFornitoriDatiAgg.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.ClientiFornitoriDatiAgg toUnescapedModel() {
        return new ClientiFornitoriDatiAggWrapper(_clientiFornitoriDatiAgg.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _clientiFornitoriDatiAgg.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _clientiFornitoriDatiAgg.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _clientiFornitoriDatiAgg.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ClientiFornitoriDatiAggWrapper)) {
            return false;
        }

        ClientiFornitoriDatiAggWrapper clientiFornitoriDatiAggWrapper = (ClientiFornitoriDatiAggWrapper) obj;

        if (Validator.equals(_clientiFornitoriDatiAgg,
                    clientiFornitoriDatiAggWrapper._clientiFornitoriDatiAgg)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public ClientiFornitoriDatiAgg getWrappedClientiFornitoriDatiAgg() {
        return _clientiFornitoriDatiAgg;
    }

    @Override
    public ClientiFornitoriDatiAgg getWrappedModel() {
        return _clientiFornitoriDatiAgg;
    }

    @Override
    public void resetOriginalValues() {
        _clientiFornitoriDatiAgg.resetOriginalValues();
    }
}
