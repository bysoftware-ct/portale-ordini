package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the MovimentoVuoto service. Represents a row in the &quot;_movimenti_vuoti&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see MovimentoVuotoModel
 * @see it.bysoftware.ct.model.impl.MovimentoVuotoImpl
 * @see it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl
 * @generated
 */
public interface MovimentoVuoto extends MovimentoVuotoModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.MovimentoVuotoImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
