package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Pagamento service. Represents a row in the &quot;_pagamenti&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see PagamentoModel
 * @see it.bysoftware.ct.model.impl.PagamentoImpl
 * @see it.bysoftware.ct.model.impl.PagamentoModelImpl
 * @generated
 */
public interface Pagamento extends PagamentoModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.PagamentoImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
