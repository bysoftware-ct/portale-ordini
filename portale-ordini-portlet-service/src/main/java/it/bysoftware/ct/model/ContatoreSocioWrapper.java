package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ContatoreSocio}.
 * </p>
 *
 * @author Mario Torrisi
 * @see ContatoreSocio
 * @generated
 */
public class ContatoreSocioWrapper implements ContatoreSocio,
    ModelWrapper<ContatoreSocio> {
    private ContatoreSocio _contatoreSocio;

    public ContatoreSocioWrapper(ContatoreSocio contatoreSocio) {
        _contatoreSocio = contatoreSocio;
    }

    @Override
    public Class<?> getModelClass() {
        return ContatoreSocio.class;
    }

    @Override
    public String getModelClassName() {
        return ContatoreSocio.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("anno", getAnno());
        attributes.put("codiceSocio", getCodiceSocio());
        attributes.put("tipoDocumento", getTipoDocumento());
        attributes.put("numero", getNumero());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Integer anno = (Integer) attributes.get("anno");

        if (anno != null) {
            setAnno(anno);
        }

        String codiceSocio = (String) attributes.get("codiceSocio");

        if (codiceSocio != null) {
            setCodiceSocio(codiceSocio);
        }

        String tipoDocumento = (String) attributes.get("tipoDocumento");

        if (tipoDocumento != null) {
            setTipoDocumento(tipoDocumento);
        }

        Integer numero = (Integer) attributes.get("numero");

        if (numero != null) {
            setNumero(numero);
        }
    }

    /**
    * Returns the primary key of this contatore socio.
    *
    * @return the primary key of this contatore socio
    */
    @Override
    public it.bysoftware.ct.service.persistence.ContatoreSocioPK getPrimaryKey() {
        return _contatoreSocio.getPrimaryKey();
    }

    /**
    * Sets the primary key of this contatore socio.
    *
    * @param primaryKey the primary key of this contatore socio
    */
    @Override
    public void setPrimaryKey(
        it.bysoftware.ct.service.persistence.ContatoreSocioPK primaryKey) {
        _contatoreSocio.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the anno of this contatore socio.
    *
    * @return the anno of this contatore socio
    */
    @Override
    public int getAnno() {
        return _contatoreSocio.getAnno();
    }

    /**
    * Sets the anno of this contatore socio.
    *
    * @param anno the anno of this contatore socio
    */
    @Override
    public void setAnno(int anno) {
        _contatoreSocio.setAnno(anno);
    }

    /**
    * Returns the codice socio of this contatore socio.
    *
    * @return the codice socio of this contatore socio
    */
    @Override
    public java.lang.String getCodiceSocio() {
        return _contatoreSocio.getCodiceSocio();
    }

    /**
    * Sets the codice socio of this contatore socio.
    *
    * @param codiceSocio the codice socio of this contatore socio
    */
    @Override
    public void setCodiceSocio(java.lang.String codiceSocio) {
        _contatoreSocio.setCodiceSocio(codiceSocio);
    }

    /**
    * Returns the tipo documento of this contatore socio.
    *
    * @return the tipo documento of this contatore socio
    */
    @Override
    public java.lang.String getTipoDocumento() {
        return _contatoreSocio.getTipoDocumento();
    }

    /**
    * Sets the tipo documento of this contatore socio.
    *
    * @param tipoDocumento the tipo documento of this contatore socio
    */
    @Override
    public void setTipoDocumento(java.lang.String tipoDocumento) {
        _contatoreSocio.setTipoDocumento(tipoDocumento);
    }

    /**
    * Returns the numero of this contatore socio.
    *
    * @return the numero of this contatore socio
    */
    @Override
    public int getNumero() {
        return _contatoreSocio.getNumero();
    }

    /**
    * Sets the numero of this contatore socio.
    *
    * @param numero the numero of this contatore socio
    */
    @Override
    public void setNumero(int numero) {
        _contatoreSocio.setNumero(numero);
    }

    @Override
    public boolean isNew() {
        return _contatoreSocio.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _contatoreSocio.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _contatoreSocio.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _contatoreSocio.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _contatoreSocio.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _contatoreSocio.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _contatoreSocio.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _contatoreSocio.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _contatoreSocio.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _contatoreSocio.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _contatoreSocio.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new ContatoreSocioWrapper((ContatoreSocio) _contatoreSocio.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.ContatoreSocio contatoreSocio) {
        return _contatoreSocio.compareTo(contatoreSocio);
    }

    @Override
    public int hashCode() {
        return _contatoreSocio.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.ContatoreSocio> toCacheModel() {
        return _contatoreSocio.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.ContatoreSocio toEscapedModel() {
        return new ContatoreSocioWrapper(_contatoreSocio.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.ContatoreSocio toUnescapedModel() {
        return new ContatoreSocioWrapper(_contatoreSocio.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _contatoreSocio.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _contatoreSocio.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _contatoreSocio.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ContatoreSocioWrapper)) {
            return false;
        }

        ContatoreSocioWrapper contatoreSocioWrapper = (ContatoreSocioWrapper) obj;

        if (Validator.equals(_contatoreSocio,
                    contatoreSocioWrapper._contatoreSocio)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public ContatoreSocio getWrappedContatoreSocio() {
        return _contatoreSocio;
    }

    @Override
    public ContatoreSocio getWrappedModel() {
        return _contatoreSocio;
    }

    @Override
    public void resetOriginalValues() {
        _contatoreSocio.resetOriginalValues();
    }
}
