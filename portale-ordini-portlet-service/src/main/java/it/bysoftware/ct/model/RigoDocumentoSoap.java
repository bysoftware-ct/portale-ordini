package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.RigoDocumentoPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.RigoDocumentoServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.RigoDocumentoServiceSoap
 * @generated
 */
public class RigoDocumentoSoap implements Serializable {
    private int _anno;
    private String _codiceAttivita;
    private String _codiceCentro;
    private String _codiceDeposito;
    private int _protocollo;
    private String _codiceFornitore;
    private int _rigo;
    private String _tipoDocumento;
    private int _tipoRigo;
    private String _codiceArticolo;
    private String _codiceVariante;
    private String _descrizione;
    private double _quantita;
    private double _quantitaSecondaria;
    private double _prezzo;
    private double _sconto1;
    private double _sconto2;
    private double _sconto3;
    private String _libStr1;
    private String _libStr2;
    private String _libStr3;
    private double _libDbl1;
    private double _libDbl2;
    private double _libDbl3;
    private long _libLng1;
    private long _libLng2;
    private long _libLng3;
    private Date _libDat1;
    private Date _libDat2;
    private Date _libDat3;
    private double _importoNetto;
    private String _codiceIVA;

    public RigoDocumentoSoap() {
    }

    public static RigoDocumentoSoap toSoapModel(RigoDocumento model) {
        RigoDocumentoSoap soapModel = new RigoDocumentoSoap();

        soapModel.setAnno(model.getAnno());
        soapModel.setCodiceAttivita(model.getCodiceAttivita());
        soapModel.setCodiceCentro(model.getCodiceCentro());
        soapModel.setCodiceDeposito(model.getCodiceDeposito());
        soapModel.setProtocollo(model.getProtocollo());
        soapModel.setCodiceFornitore(model.getCodiceFornitore());
        soapModel.setRigo(model.getRigo());
        soapModel.setTipoDocumento(model.getTipoDocumento());
        soapModel.setTipoRigo(model.getTipoRigo());
        soapModel.setCodiceArticolo(model.getCodiceArticolo());
        soapModel.setCodiceVariante(model.getCodiceVariante());
        soapModel.setDescrizione(model.getDescrizione());
        soapModel.setQuantita(model.getQuantita());
        soapModel.setQuantitaSecondaria(model.getQuantitaSecondaria());
        soapModel.setPrezzo(model.getPrezzo());
        soapModel.setSconto1(model.getSconto1());
        soapModel.setSconto2(model.getSconto2());
        soapModel.setSconto3(model.getSconto3());
        soapModel.setLibStr1(model.getLibStr1());
        soapModel.setLibStr2(model.getLibStr2());
        soapModel.setLibStr3(model.getLibStr3());
        soapModel.setLibDbl1(model.getLibDbl1());
        soapModel.setLibDbl2(model.getLibDbl2());
        soapModel.setLibDbl3(model.getLibDbl3());
        soapModel.setLibLng1(model.getLibLng1());
        soapModel.setLibLng2(model.getLibLng2());
        soapModel.setLibLng3(model.getLibLng3());
        soapModel.setLibDat1(model.getLibDat1());
        soapModel.setLibDat2(model.getLibDat2());
        soapModel.setLibDat3(model.getLibDat3());
        soapModel.setImportoNetto(model.getImportoNetto());
        soapModel.setCodiceIVA(model.getCodiceIVA());

        return soapModel;
    }

    public static RigoDocumentoSoap[] toSoapModels(RigoDocumento[] models) {
        RigoDocumentoSoap[] soapModels = new RigoDocumentoSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static RigoDocumentoSoap[][] toSoapModels(RigoDocumento[][] models) {
        RigoDocumentoSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new RigoDocumentoSoap[models.length][models[0].length];
        } else {
            soapModels = new RigoDocumentoSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static RigoDocumentoSoap[] toSoapModels(List<RigoDocumento> models) {
        List<RigoDocumentoSoap> soapModels = new ArrayList<RigoDocumentoSoap>(models.size());

        for (RigoDocumento model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new RigoDocumentoSoap[soapModels.size()]);
    }

    public RigoDocumentoPK getPrimaryKey() {
        return new RigoDocumentoPK(_anno, _codiceAttivita, _codiceCentro,
            _codiceDeposito, _protocollo, _codiceFornitore, _rigo,
            _tipoDocumento);
    }

    public void setPrimaryKey(RigoDocumentoPK pk) {
        setAnno(pk.anno);
        setCodiceAttivita(pk.codiceAttivita);
        setCodiceCentro(pk.codiceCentro);
        setCodiceDeposito(pk.codiceDeposito);
        setProtocollo(pk.protocollo);
        setCodiceFornitore(pk.codiceFornitore);
        setRigo(pk.rigo);
        setTipoDocumento(pk.tipoDocumento);
    }

    public int getAnno() {
        return _anno;
    }

    public void setAnno(int anno) {
        _anno = anno;
    }

    public String getCodiceAttivita() {
        return _codiceAttivita;
    }

    public void setCodiceAttivita(String codiceAttivita) {
        _codiceAttivita = codiceAttivita;
    }

    public String getCodiceCentro() {
        return _codiceCentro;
    }

    public void setCodiceCentro(String codiceCentro) {
        _codiceCentro = codiceCentro;
    }

    public String getCodiceDeposito() {
        return _codiceDeposito;
    }

    public void setCodiceDeposito(String codiceDeposito) {
        _codiceDeposito = codiceDeposito;
    }

    public int getProtocollo() {
        return _protocollo;
    }

    public void setProtocollo(int protocollo) {
        _protocollo = protocollo;
    }

    public String getCodiceFornitore() {
        return _codiceFornitore;
    }

    public void setCodiceFornitore(String codiceFornitore) {
        _codiceFornitore = codiceFornitore;
    }

    public int getRigo() {
        return _rigo;
    }

    public void setRigo(int rigo) {
        _rigo = rigo;
    }

    public String getTipoDocumento() {
        return _tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        _tipoDocumento = tipoDocumento;
    }

    public int getTipoRigo() {
        return _tipoRigo;
    }

    public void setTipoRigo(int tipoRigo) {
        _tipoRigo = tipoRigo;
    }

    public String getCodiceArticolo() {
        return _codiceArticolo;
    }

    public void setCodiceArticolo(String codiceArticolo) {
        _codiceArticolo = codiceArticolo;
    }

    public String getCodiceVariante() {
        return _codiceVariante;
    }

    public void setCodiceVariante(String codiceVariante) {
        _codiceVariante = codiceVariante;
    }

    public String getDescrizione() {
        return _descrizione;
    }

    public void setDescrizione(String descrizione) {
        _descrizione = descrizione;
    }

    public double getQuantita() {
        return _quantita;
    }

    public void setQuantita(double quantita) {
        _quantita = quantita;
    }

    public double getQuantitaSecondaria() {
        return _quantitaSecondaria;
    }

    public void setQuantitaSecondaria(double quantitaSecondaria) {
        _quantitaSecondaria = quantitaSecondaria;
    }

    public double getPrezzo() {
        return _prezzo;
    }

    public void setPrezzo(double prezzo) {
        _prezzo = prezzo;
    }

    public double getSconto1() {
        return _sconto1;
    }

    public void setSconto1(double sconto1) {
        _sconto1 = sconto1;
    }

    public double getSconto2() {
        return _sconto2;
    }

    public void setSconto2(double sconto2) {
        _sconto2 = sconto2;
    }

    public double getSconto3() {
        return _sconto3;
    }

    public void setSconto3(double sconto3) {
        _sconto3 = sconto3;
    }

    public String getLibStr1() {
        return _libStr1;
    }

    public void setLibStr1(String libStr1) {
        _libStr1 = libStr1;
    }

    public String getLibStr2() {
        return _libStr2;
    }

    public void setLibStr2(String libStr2) {
        _libStr2 = libStr2;
    }

    public String getLibStr3() {
        return _libStr3;
    }

    public void setLibStr3(String libStr3) {
        _libStr3 = libStr3;
    }

    public double getLibDbl1() {
        return _libDbl1;
    }

    public void setLibDbl1(double libDbl1) {
        _libDbl1 = libDbl1;
    }

    public double getLibDbl2() {
        return _libDbl2;
    }

    public void setLibDbl2(double libDbl2) {
        _libDbl2 = libDbl2;
    }

    public double getLibDbl3() {
        return _libDbl3;
    }

    public void setLibDbl3(double libDbl3) {
        _libDbl3 = libDbl3;
    }

    public long getLibLng1() {
        return _libLng1;
    }

    public void setLibLng1(long libLng1) {
        _libLng1 = libLng1;
    }

    public long getLibLng2() {
        return _libLng2;
    }

    public void setLibLng2(long libLng2) {
        _libLng2 = libLng2;
    }

    public long getLibLng3() {
        return _libLng3;
    }

    public void setLibLng3(long libLng3) {
        _libLng3 = libLng3;
    }

    public Date getLibDat1() {
        return _libDat1;
    }

    public void setLibDat1(Date libDat1) {
        _libDat1 = libDat1;
    }

    public Date getLibDat2() {
        return _libDat2;
    }

    public void setLibDat2(Date libDat2) {
        _libDat2 = libDat2;
    }

    public Date getLibDat3() {
        return _libDat3;
    }

    public void setLibDat3(Date libDat3) {
        _libDat3 = libDat3;
    }

    public double getImportoNetto() {
        return _importoNetto;
    }

    public void setImportoNetto(double importoNetto) {
        _importoNetto = importoNetto;
    }

    public String getCodiceIVA() {
        return _codiceIVA;
    }

    public void setCodiceIVA(String codiceIVA) {
        _codiceIVA = codiceIVA;
    }
}
