package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.SpettanzeSociServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.SpettanzeSociServiceSoap
 * @generated
 */
public class SpettanzeSociSoap implements Serializable {
    private long _id;
    private int _anno;
    private String _codiceAttivita;
    private String _codiceCentro;
    private int _numeroProtocollo;
    private String _codiceFornitore;
    private double _importo;
    private Date _dataCalcolo;
    private int _stato;
    private int _idPagamento;
    private int _annoPagamento;

    public SpettanzeSociSoap() {
    }

    public static SpettanzeSociSoap toSoapModel(SpettanzeSoci model) {
        SpettanzeSociSoap soapModel = new SpettanzeSociSoap();

        soapModel.setId(model.getId());
        soapModel.setAnno(model.getAnno());
        soapModel.setCodiceAttivita(model.getCodiceAttivita());
        soapModel.setCodiceCentro(model.getCodiceCentro());
        soapModel.setNumeroProtocollo(model.getNumeroProtocollo());
        soapModel.setCodiceFornitore(model.getCodiceFornitore());
        soapModel.setImporto(model.getImporto());
        soapModel.setDataCalcolo(model.getDataCalcolo());
        soapModel.setStato(model.getStato());
        soapModel.setIdPagamento(model.getIdPagamento());
        soapModel.setAnnoPagamento(model.getAnnoPagamento());

        return soapModel;
    }

    public static SpettanzeSociSoap[] toSoapModels(SpettanzeSoci[] models) {
        SpettanzeSociSoap[] soapModels = new SpettanzeSociSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static SpettanzeSociSoap[][] toSoapModels(SpettanzeSoci[][] models) {
        SpettanzeSociSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new SpettanzeSociSoap[models.length][models[0].length];
        } else {
            soapModels = new SpettanzeSociSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static SpettanzeSociSoap[] toSoapModels(List<SpettanzeSoci> models) {
        List<SpettanzeSociSoap> soapModels = new ArrayList<SpettanzeSociSoap>(models.size());

        for (SpettanzeSoci model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new SpettanzeSociSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public int getAnno() {
        return _anno;
    }

    public void setAnno(int anno) {
        _anno = anno;
    }

    public String getCodiceAttivita() {
        return _codiceAttivita;
    }

    public void setCodiceAttivita(String codiceAttivita) {
        _codiceAttivita = codiceAttivita;
    }

    public String getCodiceCentro() {
        return _codiceCentro;
    }

    public void setCodiceCentro(String codiceCentro) {
        _codiceCentro = codiceCentro;
    }

    public int getNumeroProtocollo() {
        return _numeroProtocollo;
    }

    public void setNumeroProtocollo(int numeroProtocollo) {
        _numeroProtocollo = numeroProtocollo;
    }

    public String getCodiceFornitore() {
        return _codiceFornitore;
    }

    public void setCodiceFornitore(String codiceFornitore) {
        _codiceFornitore = codiceFornitore;
    }

    public double getImporto() {
        return _importo;
    }

    public void setImporto(double importo) {
        _importo = importo;
    }

    public Date getDataCalcolo() {
        return _dataCalcolo;
    }

    public void setDataCalcolo(Date dataCalcolo) {
        _dataCalcolo = dataCalcolo;
    }

    public int getStato() {
        return _stato;
    }

    public void setStato(int stato) {
        _stato = stato;
    }

    public int getIdPagamento() {
        return _idPagamento;
    }

    public void setIdPagamento(int idPagamento) {
        _idPagamento = idPagamento;
    }

    public int getAnnoPagamento() {
        return _annoPagamento;
    }

    public void setAnnoPagamento(int annoPagamento) {
        _annoPagamento = annoPagamento;
    }
}
