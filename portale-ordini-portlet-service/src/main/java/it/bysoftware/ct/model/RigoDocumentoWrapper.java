package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link RigoDocumento}.
 * </p>
 *
 * @author Mario Torrisi
 * @see RigoDocumento
 * @generated
 */
public class RigoDocumentoWrapper implements RigoDocumento,
    ModelWrapper<RigoDocumento> {
    private RigoDocumento _rigoDocumento;

    public RigoDocumentoWrapper(RigoDocumento rigoDocumento) {
        _rigoDocumento = rigoDocumento;
    }

    @Override
    public Class<?> getModelClass() {
        return RigoDocumento.class;
    }

    @Override
    public String getModelClassName() {
        return RigoDocumento.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("anno", getAnno());
        attributes.put("codiceAttivita", getCodiceAttivita());
        attributes.put("codiceCentro", getCodiceCentro());
        attributes.put("codiceDeposito", getCodiceDeposito());
        attributes.put("protocollo", getProtocollo());
        attributes.put("codiceFornitore", getCodiceFornitore());
        attributes.put("rigo", getRigo());
        attributes.put("tipoDocumento", getTipoDocumento());
        attributes.put("tipoRigo", getTipoRigo());
        attributes.put("codiceArticolo", getCodiceArticolo());
        attributes.put("codiceVariante", getCodiceVariante());
        attributes.put("descrizione", getDescrizione());
        attributes.put("quantita", getQuantita());
        attributes.put("quantitaSecondaria", getQuantitaSecondaria());
        attributes.put("prezzo", getPrezzo());
        attributes.put("sconto1", getSconto1());
        attributes.put("sconto2", getSconto2());
        attributes.put("sconto3", getSconto3());
        attributes.put("libStr1", getLibStr1());
        attributes.put("libStr2", getLibStr2());
        attributes.put("libStr3", getLibStr3());
        attributes.put("libDbl1", getLibDbl1());
        attributes.put("libDbl2", getLibDbl2());
        attributes.put("libDbl3", getLibDbl3());
        attributes.put("libLng1", getLibLng1());
        attributes.put("libLng2", getLibLng2());
        attributes.put("libLng3", getLibLng3());
        attributes.put("libDat1", getLibDat1());
        attributes.put("libDat2", getLibDat2());
        attributes.put("libDat3", getLibDat3());
        attributes.put("importoNetto", getImportoNetto());
        attributes.put("codiceIVA", getCodiceIVA());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Integer anno = (Integer) attributes.get("anno");

        if (anno != null) {
            setAnno(anno);
        }

        String codiceAttivita = (String) attributes.get("codiceAttivita");

        if (codiceAttivita != null) {
            setCodiceAttivita(codiceAttivita);
        }

        String codiceCentro = (String) attributes.get("codiceCentro");

        if (codiceCentro != null) {
            setCodiceCentro(codiceCentro);
        }

        String codiceDeposito = (String) attributes.get("codiceDeposito");

        if (codiceDeposito != null) {
            setCodiceDeposito(codiceDeposito);
        }

        Integer protocollo = (Integer) attributes.get("protocollo");

        if (protocollo != null) {
            setProtocollo(protocollo);
        }

        String codiceFornitore = (String) attributes.get("codiceFornitore");

        if (codiceFornitore != null) {
            setCodiceFornitore(codiceFornitore);
        }

        Integer rigo = (Integer) attributes.get("rigo");

        if (rigo != null) {
            setRigo(rigo);
        }

        String tipoDocumento = (String) attributes.get("tipoDocumento");

        if (tipoDocumento != null) {
            setTipoDocumento(tipoDocumento);
        }

        Integer tipoRigo = (Integer) attributes.get("tipoRigo");

        if (tipoRigo != null) {
            setTipoRigo(tipoRigo);
        }

        String codiceArticolo = (String) attributes.get("codiceArticolo");

        if (codiceArticolo != null) {
            setCodiceArticolo(codiceArticolo);
        }

        String codiceVariante = (String) attributes.get("codiceVariante");

        if (codiceVariante != null) {
            setCodiceVariante(codiceVariante);
        }

        String descrizione = (String) attributes.get("descrizione");

        if (descrizione != null) {
            setDescrizione(descrizione);
        }

        Double quantita = (Double) attributes.get("quantita");

        if (quantita != null) {
            setQuantita(quantita);
        }

        Double quantitaSecondaria = (Double) attributes.get(
                "quantitaSecondaria");

        if (quantitaSecondaria != null) {
            setQuantitaSecondaria(quantitaSecondaria);
        }

        Double prezzo = (Double) attributes.get("prezzo");

        if (prezzo != null) {
            setPrezzo(prezzo);
        }

        Double sconto1 = (Double) attributes.get("sconto1");

        if (sconto1 != null) {
            setSconto1(sconto1);
        }

        Double sconto2 = (Double) attributes.get("sconto2");

        if (sconto2 != null) {
            setSconto2(sconto2);
        }

        Double sconto3 = (Double) attributes.get("sconto3");

        if (sconto3 != null) {
            setSconto3(sconto3);
        }

        String libStr1 = (String) attributes.get("libStr1");

        if (libStr1 != null) {
            setLibStr1(libStr1);
        }

        String libStr2 = (String) attributes.get("libStr2");

        if (libStr2 != null) {
            setLibStr2(libStr2);
        }

        String libStr3 = (String) attributes.get("libStr3");

        if (libStr3 != null) {
            setLibStr3(libStr3);
        }

        Double libDbl1 = (Double) attributes.get("libDbl1");

        if (libDbl1 != null) {
            setLibDbl1(libDbl1);
        }

        Double libDbl2 = (Double) attributes.get("libDbl2");

        if (libDbl2 != null) {
            setLibDbl2(libDbl2);
        }

        Double libDbl3 = (Double) attributes.get("libDbl3");

        if (libDbl3 != null) {
            setLibDbl3(libDbl3);
        }

        Long libLng1 = (Long) attributes.get("libLng1");

        if (libLng1 != null) {
            setLibLng1(libLng1);
        }

        Long libLng2 = (Long) attributes.get("libLng2");

        if (libLng2 != null) {
            setLibLng2(libLng2);
        }

        Long libLng3 = (Long) attributes.get("libLng3");

        if (libLng3 != null) {
            setLibLng3(libLng3);
        }

        Date libDat1 = (Date) attributes.get("libDat1");

        if (libDat1 != null) {
            setLibDat1(libDat1);
        }

        Date libDat2 = (Date) attributes.get("libDat2");

        if (libDat2 != null) {
            setLibDat2(libDat2);
        }

        Date libDat3 = (Date) attributes.get("libDat3");

        if (libDat3 != null) {
            setLibDat3(libDat3);
        }

        Double importoNetto = (Double) attributes.get("importoNetto");

        if (importoNetto != null) {
            setImportoNetto(importoNetto);
        }

        String codiceIVA = (String) attributes.get("codiceIVA");

        if (codiceIVA != null) {
            setCodiceIVA(codiceIVA);
        }
    }

    /**
    * Returns the primary key of this rigo documento.
    *
    * @return the primary key of this rigo documento
    */
    @Override
    public it.bysoftware.ct.service.persistence.RigoDocumentoPK getPrimaryKey() {
        return _rigoDocumento.getPrimaryKey();
    }

    /**
    * Sets the primary key of this rigo documento.
    *
    * @param primaryKey the primary key of this rigo documento
    */
    @Override
    public void setPrimaryKey(
        it.bysoftware.ct.service.persistence.RigoDocumentoPK primaryKey) {
        _rigoDocumento.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the anno of this rigo documento.
    *
    * @return the anno of this rigo documento
    */
    @Override
    public int getAnno() {
        return _rigoDocumento.getAnno();
    }

    /**
    * Sets the anno of this rigo documento.
    *
    * @param anno the anno of this rigo documento
    */
    @Override
    public void setAnno(int anno) {
        _rigoDocumento.setAnno(anno);
    }

    /**
    * Returns the codice attivita of this rigo documento.
    *
    * @return the codice attivita of this rigo documento
    */
    @Override
    public java.lang.String getCodiceAttivita() {
        return _rigoDocumento.getCodiceAttivita();
    }

    /**
    * Sets the codice attivita of this rigo documento.
    *
    * @param codiceAttivita the codice attivita of this rigo documento
    */
    @Override
    public void setCodiceAttivita(java.lang.String codiceAttivita) {
        _rigoDocumento.setCodiceAttivita(codiceAttivita);
    }

    /**
    * Returns the codice centro of this rigo documento.
    *
    * @return the codice centro of this rigo documento
    */
    @Override
    public java.lang.String getCodiceCentro() {
        return _rigoDocumento.getCodiceCentro();
    }

    /**
    * Sets the codice centro of this rigo documento.
    *
    * @param codiceCentro the codice centro of this rigo documento
    */
    @Override
    public void setCodiceCentro(java.lang.String codiceCentro) {
        _rigoDocumento.setCodiceCentro(codiceCentro);
    }

    /**
    * Returns the codice deposito of this rigo documento.
    *
    * @return the codice deposito of this rigo documento
    */
    @Override
    public java.lang.String getCodiceDeposito() {
        return _rigoDocumento.getCodiceDeposito();
    }

    /**
    * Sets the codice deposito of this rigo documento.
    *
    * @param codiceDeposito the codice deposito of this rigo documento
    */
    @Override
    public void setCodiceDeposito(java.lang.String codiceDeposito) {
        _rigoDocumento.setCodiceDeposito(codiceDeposito);
    }

    /**
    * Returns the protocollo of this rigo documento.
    *
    * @return the protocollo of this rigo documento
    */
    @Override
    public int getProtocollo() {
        return _rigoDocumento.getProtocollo();
    }

    /**
    * Sets the protocollo of this rigo documento.
    *
    * @param protocollo the protocollo of this rigo documento
    */
    @Override
    public void setProtocollo(int protocollo) {
        _rigoDocumento.setProtocollo(protocollo);
    }

    /**
    * Returns the codice fornitore of this rigo documento.
    *
    * @return the codice fornitore of this rigo documento
    */
    @Override
    public java.lang.String getCodiceFornitore() {
        return _rigoDocumento.getCodiceFornitore();
    }

    /**
    * Sets the codice fornitore of this rigo documento.
    *
    * @param codiceFornitore the codice fornitore of this rigo documento
    */
    @Override
    public void setCodiceFornitore(java.lang.String codiceFornitore) {
        _rigoDocumento.setCodiceFornitore(codiceFornitore);
    }

    /**
    * Returns the rigo of this rigo documento.
    *
    * @return the rigo of this rigo documento
    */
    @Override
    public int getRigo() {
        return _rigoDocumento.getRigo();
    }

    /**
    * Sets the rigo of this rigo documento.
    *
    * @param rigo the rigo of this rigo documento
    */
    @Override
    public void setRigo(int rigo) {
        _rigoDocumento.setRigo(rigo);
    }

    /**
    * Returns the tipo documento of this rigo documento.
    *
    * @return the tipo documento of this rigo documento
    */
    @Override
    public java.lang.String getTipoDocumento() {
        return _rigoDocumento.getTipoDocumento();
    }

    /**
    * Sets the tipo documento of this rigo documento.
    *
    * @param tipoDocumento the tipo documento of this rigo documento
    */
    @Override
    public void setTipoDocumento(java.lang.String tipoDocumento) {
        _rigoDocumento.setTipoDocumento(tipoDocumento);
    }

    /**
    * Returns the tipo rigo of this rigo documento.
    *
    * @return the tipo rigo of this rigo documento
    */
    @Override
    public int getTipoRigo() {
        return _rigoDocumento.getTipoRigo();
    }

    /**
    * Sets the tipo rigo of this rigo documento.
    *
    * @param tipoRigo the tipo rigo of this rigo documento
    */
    @Override
    public void setTipoRigo(int tipoRigo) {
        _rigoDocumento.setTipoRigo(tipoRigo);
    }

    /**
    * Returns the codice articolo of this rigo documento.
    *
    * @return the codice articolo of this rigo documento
    */
    @Override
    public java.lang.String getCodiceArticolo() {
        return _rigoDocumento.getCodiceArticolo();
    }

    /**
    * Sets the codice articolo of this rigo documento.
    *
    * @param codiceArticolo the codice articolo of this rigo documento
    */
    @Override
    public void setCodiceArticolo(java.lang.String codiceArticolo) {
        _rigoDocumento.setCodiceArticolo(codiceArticolo);
    }

    /**
    * Returns the codice variante of this rigo documento.
    *
    * @return the codice variante of this rigo documento
    */
    @Override
    public java.lang.String getCodiceVariante() {
        return _rigoDocumento.getCodiceVariante();
    }

    /**
    * Sets the codice variante of this rigo documento.
    *
    * @param codiceVariante the codice variante of this rigo documento
    */
    @Override
    public void setCodiceVariante(java.lang.String codiceVariante) {
        _rigoDocumento.setCodiceVariante(codiceVariante);
    }

    /**
    * Returns the descrizione of this rigo documento.
    *
    * @return the descrizione of this rigo documento
    */
    @Override
    public java.lang.String getDescrizione() {
        return _rigoDocumento.getDescrizione();
    }

    /**
    * Sets the descrizione of this rigo documento.
    *
    * @param descrizione the descrizione of this rigo documento
    */
    @Override
    public void setDescrizione(java.lang.String descrizione) {
        _rigoDocumento.setDescrizione(descrizione);
    }

    /**
    * Returns the quantita of this rigo documento.
    *
    * @return the quantita of this rigo documento
    */
    @Override
    public double getQuantita() {
        return _rigoDocumento.getQuantita();
    }

    /**
    * Sets the quantita of this rigo documento.
    *
    * @param quantita the quantita of this rigo documento
    */
    @Override
    public void setQuantita(double quantita) {
        _rigoDocumento.setQuantita(quantita);
    }

    /**
    * Returns the quantita secondaria of this rigo documento.
    *
    * @return the quantita secondaria of this rigo documento
    */
    @Override
    public double getQuantitaSecondaria() {
        return _rigoDocumento.getQuantitaSecondaria();
    }

    /**
    * Sets the quantita secondaria of this rigo documento.
    *
    * @param quantitaSecondaria the quantita secondaria of this rigo documento
    */
    @Override
    public void setQuantitaSecondaria(double quantitaSecondaria) {
        _rigoDocumento.setQuantitaSecondaria(quantitaSecondaria);
    }

    /**
    * Returns the prezzo of this rigo documento.
    *
    * @return the prezzo of this rigo documento
    */
    @Override
    public double getPrezzo() {
        return _rigoDocumento.getPrezzo();
    }

    /**
    * Sets the prezzo of this rigo documento.
    *
    * @param prezzo the prezzo of this rigo documento
    */
    @Override
    public void setPrezzo(double prezzo) {
        _rigoDocumento.setPrezzo(prezzo);
    }

    /**
    * Returns the sconto1 of this rigo documento.
    *
    * @return the sconto1 of this rigo documento
    */
    @Override
    public double getSconto1() {
        return _rigoDocumento.getSconto1();
    }

    /**
    * Sets the sconto1 of this rigo documento.
    *
    * @param sconto1 the sconto1 of this rigo documento
    */
    @Override
    public void setSconto1(double sconto1) {
        _rigoDocumento.setSconto1(sconto1);
    }

    /**
    * Returns the sconto2 of this rigo documento.
    *
    * @return the sconto2 of this rigo documento
    */
    @Override
    public double getSconto2() {
        return _rigoDocumento.getSconto2();
    }

    /**
    * Sets the sconto2 of this rigo documento.
    *
    * @param sconto2 the sconto2 of this rigo documento
    */
    @Override
    public void setSconto2(double sconto2) {
        _rigoDocumento.setSconto2(sconto2);
    }

    /**
    * Returns the sconto3 of this rigo documento.
    *
    * @return the sconto3 of this rigo documento
    */
    @Override
    public double getSconto3() {
        return _rigoDocumento.getSconto3();
    }

    /**
    * Sets the sconto3 of this rigo documento.
    *
    * @param sconto3 the sconto3 of this rigo documento
    */
    @Override
    public void setSconto3(double sconto3) {
        _rigoDocumento.setSconto3(sconto3);
    }

    /**
    * Returns the lib str1 of this rigo documento.
    *
    * @return the lib str1 of this rigo documento
    */
    @Override
    public java.lang.String getLibStr1() {
        return _rigoDocumento.getLibStr1();
    }

    /**
    * Sets the lib str1 of this rigo documento.
    *
    * @param libStr1 the lib str1 of this rigo documento
    */
    @Override
    public void setLibStr1(java.lang.String libStr1) {
        _rigoDocumento.setLibStr1(libStr1);
    }

    /**
    * Returns the lib str2 of this rigo documento.
    *
    * @return the lib str2 of this rigo documento
    */
    @Override
    public java.lang.String getLibStr2() {
        return _rigoDocumento.getLibStr2();
    }

    /**
    * Sets the lib str2 of this rigo documento.
    *
    * @param libStr2 the lib str2 of this rigo documento
    */
    @Override
    public void setLibStr2(java.lang.String libStr2) {
        _rigoDocumento.setLibStr2(libStr2);
    }

    /**
    * Returns the lib str3 of this rigo documento.
    *
    * @return the lib str3 of this rigo documento
    */
    @Override
    public java.lang.String getLibStr3() {
        return _rigoDocumento.getLibStr3();
    }

    /**
    * Sets the lib str3 of this rigo documento.
    *
    * @param libStr3 the lib str3 of this rigo documento
    */
    @Override
    public void setLibStr3(java.lang.String libStr3) {
        _rigoDocumento.setLibStr3(libStr3);
    }

    /**
    * Returns the lib dbl1 of this rigo documento.
    *
    * @return the lib dbl1 of this rigo documento
    */
    @Override
    public double getLibDbl1() {
        return _rigoDocumento.getLibDbl1();
    }

    /**
    * Sets the lib dbl1 of this rigo documento.
    *
    * @param libDbl1 the lib dbl1 of this rigo documento
    */
    @Override
    public void setLibDbl1(double libDbl1) {
        _rigoDocumento.setLibDbl1(libDbl1);
    }

    /**
    * Returns the lib dbl2 of this rigo documento.
    *
    * @return the lib dbl2 of this rigo documento
    */
    @Override
    public double getLibDbl2() {
        return _rigoDocumento.getLibDbl2();
    }

    /**
    * Sets the lib dbl2 of this rigo documento.
    *
    * @param libDbl2 the lib dbl2 of this rigo documento
    */
    @Override
    public void setLibDbl2(double libDbl2) {
        _rigoDocumento.setLibDbl2(libDbl2);
    }

    /**
    * Returns the lib dbl3 of this rigo documento.
    *
    * @return the lib dbl3 of this rigo documento
    */
    @Override
    public double getLibDbl3() {
        return _rigoDocumento.getLibDbl3();
    }

    /**
    * Sets the lib dbl3 of this rigo documento.
    *
    * @param libDbl3 the lib dbl3 of this rigo documento
    */
    @Override
    public void setLibDbl3(double libDbl3) {
        _rigoDocumento.setLibDbl3(libDbl3);
    }

    /**
    * Returns the lib lng1 of this rigo documento.
    *
    * @return the lib lng1 of this rigo documento
    */
    @Override
    public long getLibLng1() {
        return _rigoDocumento.getLibLng1();
    }

    /**
    * Sets the lib lng1 of this rigo documento.
    *
    * @param libLng1 the lib lng1 of this rigo documento
    */
    @Override
    public void setLibLng1(long libLng1) {
        _rigoDocumento.setLibLng1(libLng1);
    }

    /**
    * Returns the lib lng2 of this rigo documento.
    *
    * @return the lib lng2 of this rigo documento
    */
    @Override
    public long getLibLng2() {
        return _rigoDocumento.getLibLng2();
    }

    /**
    * Sets the lib lng2 of this rigo documento.
    *
    * @param libLng2 the lib lng2 of this rigo documento
    */
    @Override
    public void setLibLng2(long libLng2) {
        _rigoDocumento.setLibLng2(libLng2);
    }

    /**
    * Returns the lib lng3 of this rigo documento.
    *
    * @return the lib lng3 of this rigo documento
    */
    @Override
    public long getLibLng3() {
        return _rigoDocumento.getLibLng3();
    }

    /**
    * Sets the lib lng3 of this rigo documento.
    *
    * @param libLng3 the lib lng3 of this rigo documento
    */
    @Override
    public void setLibLng3(long libLng3) {
        _rigoDocumento.setLibLng3(libLng3);
    }

    /**
    * Returns the lib dat1 of this rigo documento.
    *
    * @return the lib dat1 of this rigo documento
    */
    @Override
    public java.util.Date getLibDat1() {
        return _rigoDocumento.getLibDat1();
    }

    /**
    * Sets the lib dat1 of this rigo documento.
    *
    * @param libDat1 the lib dat1 of this rigo documento
    */
    @Override
    public void setLibDat1(java.util.Date libDat1) {
        _rigoDocumento.setLibDat1(libDat1);
    }

    /**
    * Returns the lib dat2 of this rigo documento.
    *
    * @return the lib dat2 of this rigo documento
    */
    @Override
    public java.util.Date getLibDat2() {
        return _rigoDocumento.getLibDat2();
    }

    /**
    * Sets the lib dat2 of this rigo documento.
    *
    * @param libDat2 the lib dat2 of this rigo documento
    */
    @Override
    public void setLibDat2(java.util.Date libDat2) {
        _rigoDocumento.setLibDat2(libDat2);
    }

    /**
    * Returns the lib dat3 of this rigo documento.
    *
    * @return the lib dat3 of this rigo documento
    */
    @Override
    public java.util.Date getLibDat3() {
        return _rigoDocumento.getLibDat3();
    }

    /**
    * Sets the lib dat3 of this rigo documento.
    *
    * @param libDat3 the lib dat3 of this rigo documento
    */
    @Override
    public void setLibDat3(java.util.Date libDat3) {
        _rigoDocumento.setLibDat3(libDat3);
    }

    /**
    * Returns the importo netto of this rigo documento.
    *
    * @return the importo netto of this rigo documento
    */
    @Override
    public double getImportoNetto() {
        return _rigoDocumento.getImportoNetto();
    }

    /**
    * Sets the importo netto of this rigo documento.
    *
    * @param importoNetto the importo netto of this rigo documento
    */
    @Override
    public void setImportoNetto(double importoNetto) {
        _rigoDocumento.setImportoNetto(importoNetto);
    }

    /**
    * Returns the codice i v a of this rigo documento.
    *
    * @return the codice i v a of this rigo documento
    */
    @Override
    public java.lang.String getCodiceIVA() {
        return _rigoDocumento.getCodiceIVA();
    }

    /**
    * Sets the codice i v a of this rigo documento.
    *
    * @param codiceIVA the codice i v a of this rigo documento
    */
    @Override
    public void setCodiceIVA(java.lang.String codiceIVA) {
        _rigoDocumento.setCodiceIVA(codiceIVA);
    }

    @Override
    public boolean isNew() {
        return _rigoDocumento.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _rigoDocumento.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _rigoDocumento.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _rigoDocumento.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _rigoDocumento.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _rigoDocumento.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _rigoDocumento.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _rigoDocumento.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _rigoDocumento.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _rigoDocumento.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _rigoDocumento.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new RigoDocumentoWrapper((RigoDocumento) _rigoDocumento.clone());
    }

    @Override
    public int compareTo(it.bysoftware.ct.model.RigoDocumento rigoDocumento) {
        return _rigoDocumento.compareTo(rigoDocumento);
    }

    @Override
    public int hashCode() {
        return _rigoDocumento.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.RigoDocumento> toCacheModel() {
        return _rigoDocumento.toCacheModel();
    }

    @Override
    public it.bysoftware.ct.model.RigoDocumento toEscapedModel() {
        return new RigoDocumentoWrapper(_rigoDocumento.toEscapedModel());
    }

    @Override
    public it.bysoftware.ct.model.RigoDocumento toUnescapedModel() {
        return new RigoDocumentoWrapper(_rigoDocumento.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _rigoDocumento.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _rigoDocumento.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _rigoDocumento.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof RigoDocumentoWrapper)) {
            return false;
        }

        RigoDocumentoWrapper rigoDocumentoWrapper = (RigoDocumentoWrapper) obj;

        if (Validator.equals(_rigoDocumento, rigoDocumentoWrapper._rigoDocumento)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public RigoDocumento getWrappedRigoDocumento() {
        return _rigoDocumento;
    }

    @Override
    public RigoDocumento getWrappedModel() {
        return _rigoDocumento;
    }

    @Override
    public void resetOriginalValues() {
        _rigoDocumento.resetOriginalValues();
    }
}
