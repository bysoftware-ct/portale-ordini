package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.VarianteServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.VarianteServiceSoap
 * @generated
 */
public class VarianteSoap implements Serializable {
    private long _id;
    private long _idOpzione;
    private String _variante;

    public VarianteSoap() {
    }

    public static VarianteSoap toSoapModel(Variante model) {
        VarianteSoap soapModel = new VarianteSoap();

        soapModel.setId(model.getId());
        soapModel.setIdOpzione(model.getIdOpzione());
        soapModel.setVariante(model.getVariante());

        return soapModel;
    }

    public static VarianteSoap[] toSoapModels(Variante[] models) {
        VarianteSoap[] soapModels = new VarianteSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static VarianteSoap[][] toSoapModels(Variante[][] models) {
        VarianteSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new VarianteSoap[models.length][models[0].length];
        } else {
            soapModels = new VarianteSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static VarianteSoap[] toSoapModels(List<Variante> models) {
        List<VarianteSoap> soapModels = new ArrayList<VarianteSoap>(models.size());

        for (Variante model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new VarianteSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getIdOpzione() {
        return _idOpzione;
    }

    public void setIdOpzione(long idOpzione) {
        _idOpzione = idOpzione;
    }

    public String getVariante() {
        return _variante;
    }

    public void setVariante(String variante) {
        _variante = variante;
    }
}
