var progressBars = new Array();
var cartsTables = new Array();
var modal;
var recordSelected;
var activeTable;

function replaceAll(str, find, replace) {
    var x = find.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
    return str.replace(new RegExp(x, 'g'), replace);
}

function currencyFormatter(amount) {
    if (isNaN(amount)) {
        if (amount === null) {
            amount = 0;
        } else {
            amount = amount.value;
        }
    }
    return parseFloat(Math.round(amount * 100) / 100).toFixed(2);
}

function labelFormatter(itemOver) {
    return itemOver.value ? Liferay.Language.get('yes') : Liferay.Language.get('no');
}

function drawProgressBars() {
    AUI().use('aui-progressbar', function(A) {
        if (orderJSON !== null && orderJSON.carts) {
            orderJSON.carts.forEach(function(item) {
                var progressDiv = A.one('#myProgressBar_' + item.id);
                if (progressDiv != null) {
                    progressDiv.empty();
                    A.one('#' + portletNamespace + 'cartTotal_' + item.id).set('value', currencyFormatter(item.importo));
                    A.one('#' + portletNamespace + 'cartPlantTotal_' + item.id).set('value', item.plants);
                } else {
                    AUI().use('aui-node', function(A) {
                        var hiddenDiv = A.one('#ciao').cloneNode(true);
                        hiddenDiv.removeClass('hidden');
                        
                        var newDiv = replaceAll(hiddenDiv.getHTML(), '{0}', item.id);
                        newDiv = replaceAll(newDiv, '{1}', currencyFormatter(currencyFormatter(item.importo)));
                        newDiv = replaceAll(newDiv, '{2}', item.plants);
                        if (item.chiuso === true) {
                        	newDiv = replaceAll(newDiv, 'icon-unlock', 'icon-lock');
                        	newDiv = replaceAll(newDiv, ' Chiudi ', ' Apri ');
						} else {
							newDiv = replaceAll(newDiv, 'icon-lock', 'icon-unlock');
                        	newDiv = replaceAll(newDiv,  ' Apri ', ' Chiudi ');
						}
                        
                        var holder = A.one('#contentHolder');
                        A.Node.create(newDiv).appendTo(holder);
                        A.all(".delete-cart").detach('click');
                        A.all(".delete-cart").on('click', function(e){
            				var tmp = e._currentTarget.id.split("_");
            				var r = confirm('Confermi l\'eliminazione del carrello: ' + tmp[tmp.length - 1]);
            				if (r == true) {
            	                deleteCart(tmp[tmp.length - 1]);
            				}
            			});
                        A.all(".lock-unlock-cart").detach('click');
                        A.all(".lock-unlock-cart").on('click', function(e){
            				var tmp = e._currentTarget.id.split("_");
//            				var r = confirm('Confermi l\'eliminazione del carrello: ' + tmp[tmp.length - 1]);
//            				if (r == true) {
            	            lockUnlockCart(tmp[tmp.length - 1]);
//            				}
            			});
                    });
                }
                console.log(item.usedHeight);
                if (item.usedHeight >= 0) {
                	progressBars.push(new A.ProgressBar({
                        boundingBox : '#myProgressBar_' + item.id,
                        label : item.usedHeight + '/' + defCartHeight,
                        max : defCartHeight,
                        min : 0,
                        on : {
                            complete : function(e) {
                                this.set('label', Liferay.Language.get('complete'));
                                this.set('cssClass', 'progress-success');
                                AUI().use('aui-base', function(A) {
                                    var locked = A.one('#' + portletNamespace + 'lockCart_' + item.id);
                                    locked.removeClass('icon-unlock');
                                    locked.set('cssClass', 'icon-lock');
                                    locked.set('label', Liferay.Language.get('unlock'));
                                    console.log(locked);
                                });
                            },
                        },
                        value : item.usedHeight,
                        width : 500
                    }).render());
                }
            });
        }
    });
}

function addNote(table, e) {
	var recordSelected = table.getActiveRecord();
    var recordData = recordSelected.getAttrs();
    
    var cartId = e.target._state.data.idCarrello.value;
    var rowId = e.target._state.data.id.value;
    
    orderJSON.carts.forEach(function(cart, i) {
    	console.log(cart);
    	if (cart.id === cartId) {
    		cart.rows.forEach(function(row, i) {
    			console.log(row);
    			if (row.id === rowId) {
//    				row.opzioni += " " + e.newVal
    				row.opzioni = e.newVal;
    				row.modificato = true;
    				console.log(row);
    			}
    		});
    	}
    });
}

function changeEAN(table, e) {
	var recordSelected = table.getActiveRecord();
    var recordData = recordSelected.getAttrs();
    
    var cartId = e.target._state.data.idCarrello.value;
    var rowId = e.target._state.data.id.value;
    
    orderJSON.carts.forEach(function(cart, i) {
    	console.log(cart);
    	if (cart.id === cartId) {
    		cart.rows.forEach(function(row, i) {
    			//console.log(row);
    			if (row.id === rowId) {
    				row.ean = e.newVal;
    				row.modificato = true;
    				console.log(row);
    			}
    		});
    	}
    });
}


function changeSuppPrice(table, e) {
    var recordSelected = table.getActiveRecord();
    var recordData = recordSelected.getAttrs();
    var prezzoFornitore = recordData.prezzoFornitore;
    recordSelected.setAttrs({
    	prezzoFornitore: currencyFormatter(prezzoFornitore)
    });
    
    var cartId = e.target._state.data.idCarrello.value;
    var rowId = e.target._state.data.id.value;

    orderJSON.carts.forEach(function(cart, i) {
        if (cart.id === cartId) {
        	cart.rows.forEach(function(row, i) {
                if (row.id === rowId) {
                	row.prezzoFornitore = parseFloat(e.newVal);
                	row.modificato = true;
                }
        	});
        }
    });
}

function changeLabelPrice(table, e) {
    var recordSelected = table.getActiveRecord();
    var recordData = recordSelected.getAttrs();
    var prezzoEtichetta = recordData.prezzoEtichetta;
    recordSelected.setAttrs({
    	prezzoEtichetta: currencyFormatter(prezzoEtichetta)
    });
    
    var cartId = e.target._state.data.idCarrello.value;
    var rowId = e.target._state.data.id.value;

    orderJSON.carts.forEach(function(cart, i) {
        if (cart.id === cartId) {
        	cart.rows.forEach(function(row, i) {
                if (row.id === rowId) {
                	row.prezzoEtichetta = parseFloat(e.newVal);
                	row.modificato = true;
                }
        	});
        }
    });
}

function computeAmount(table, e) {
    var recordSelected = table.getActiveRecord();
    var recordData = recordSelected.getAttrs();
    var prezzoSc = recordData.prezzoUn * (1 - recordData.sconto / 100);
    var prezzo = prezzoSc + recordData.importoVarianti;
    var amount = (prezzoSc + recordData.importoVarianti) * recordData.pianteRipiano;
    recordSelected.setAttrs({
    	importo: currencyFormatter(amount),
    	prezzo: currencyFormatter(prezzo), 
    	prezzoSc: currencyFormatter(prezzoSc),
    });
    
    var cartId = e.target._state.data.idCarrello.value;
    var rowId = e.target._state.data.id.value;

    orderJSON.importo = 0;
    orderJSON.carts.forEach(function(cart, i) {
        if (cart.id === cartId) {
            cart.importo = 0;
            cart.rows.forEach(function(row, i) {
                if (row.id === rowId) {
                    row.importo = parseFloat(e.target.changed.importo);
                    if (e.attrName === 'pianteRipiano') {
                        row.pianteRipiano = parseInt(e.newVal);
                    } else if (e.attrName === 'sconto') {
                        row.sconto = parseFloat(e.newVal);
                        row.prezzoSc = row.prezzoUn * (1 - parseFloat(e.newVal) / 100);
                        row.prezzo = row.prezzoSc + row.importoVarianti;
                    } else if (e.attrName === 'importoVarianti') {
                    	var diff = 0.0;
//                    	if (row.importoVarianti !== 0) {
                    		diff = parseFloat(e.newVal) - parseFloat(e.prevVal);
                    		row.importoVarianti = parseFloat(e.newVal)
//                    	} else {
//                    		row.importoVarianti = parseFloat(e.newVal);
//                    	}
                    	row.prezzo += diff; //row.importoVarianti;
                    } else if (e.attrName === 'prezzoUn') {
//                    	row.pianteRipiano = parseInt(e.newVal);
//                    	row.prezzo = parseInt(e.newVal);
                    	row.prezzoUn = parseFloat(e.newVal);
                    	row.prezzoSc = row.prezzoUn * (1 - parseFloat(row.sconto) / 100);
                    	row.prezzo = row.prezzoSc + row.importoVarianti;
                    }
                    row.importo = row.prezzo * row.pianteRipiano;
                    row.modificato = true;
                }
                cart.importo += row.importo;
            });
        }
        orderJSON.importo += cart.importo;
    });
    
}

function computeTotals(table) {
    var plantsNumber = 0;
    var cartTotal = 0.0;
    var cartId;
    table.data.toJSON().forEach(function(row, i) {
        cartTotal += parseFloat(row.importo);
        plantsNumber += parseInt(row.pianteRipiano);
        cartId = row.idCarrello;
    });
    
    AUI().use('aui-base', function (A) {
        A.one('#' + portletNamespace + 'cartTotal_' + cartId).val(cartTotal.toFixed(2));
        A.one('#' + portletNamespace + 'cartPlantTotal_' + cartId).val(plantsNumber);
    });
}

function drawTables() {
    AUI().use('aui-datatable', 'aui-datatype', 'datatable-sort', function(A) {
        var numberEditor = new A.TextCellEditor({
            inputFormatter : A.DataType.String.evaluate,
            validator : {
                rules : {
                    value : {
                        number : true,
                        required : true
                    }
                }
            }
        });
        if (orderJSON !== null && orderJSON.carts) {
            orderJSON.carts.forEach(function(item, index) {
                var tableDiv = A.one('#myDataTable_' + item.id);
                if (tableDiv != null) {
                    tableDiv.empty();
                }
                var nestedCols = [ {
                    className: 'hidden',
                    key : 'idArticolo',
                    label : Liferay.Language.get('code')
                }, {
                    key : 'itemCode',
                    label : Liferay.Language.get('code')
                },{
                    allowHTML : true,
                    key: 'img',
                    label : 'Foto'//Liferay.Language.get('picture'),
                }, {
                    editor : new A.TextAreaCellEditor(),
                    key : 'name',
                    label : Liferay.Language.get('name')
                }, {
                    className: 'hidden',
                    key : 'codiceVariante',
                    label : Liferay.Language.get('variant-code')
                }, {
                    editor : new A.TextAreaCellEditor(),
                    key : 'stringa',
                    label : 'Var.' //Liferay.Language.get('variant')
                }, {
                    key: 'editVar',
                    emptyCellValue : '<button type="button" name="editVar" class="btn icon-pencil"/>',
                    label : ' ',
                    allowHTML : true
                }, {
                    editor : new A.TextCellEditor({
                        inputFormatter : A.DataType.String.evaluate,
                        validator : {
                            rules : {
                                value : {
                                    number : true,
                                    required : true
                                }
                            }
                        }
                    }),
                    key : 'prezzoFornitore',
                    label : 'Pr. for.', //Liferay.Language.get('price'),
                    formatter: currencyFormatter
                }, {
                    editor : new A.TextCellEditor({
                        inputFormatter : A.DataType.String.evaluate,
                        validator : {
                            rules : {
                                value : {
                                    number : true,
                                    required : true
                                }
                            }
                        }
                    }),
                    key : 'prezzoUn',
                    label : 'Pr. un.', //Liferay.Language.get('price'),
                    formatter: currencyFormatter
                }, {
                    key: 'prezzo3Btn',
                    emptyCellValue : '<button type="button" name="prezzo3" class="btn btn-small btn-info icon-refresh"/>',
                    label :  'Per Pian.',
                    allowHTML : true
                }, {
                	className: 'hidden',
                    key: 'prezzo3'
                }, {
                    editor : new A.TextCellEditor({
                        inputFormatter : A.DataType.String.evaluate,
                        validator : {
                            rules : {
                                value : {
                                    number : true,
                                    required : true
                                }
                            }
                        }
                    }),
                    key : 'sconto',
                    label : '% Sc.'//Liferay.Language.get('price'),
                }, {
                    key : 'prezzoSc',
                    label : 'Pr. sc.', //Liferay.Language.get('price'),
                    formatter: currencyFormatter
                }, {
                    editor : new A.TextCellEditor({
                        inputFormatter : A.DataType.String.evaluate,
                        validator : {
                            rules : {
                                value : {
                                    number : true,
                                    required : true
                                }
                            }
                        }
                    }),
                    key : 'importoVarianti',
                    label : 'Pr. var.', //Liferay.Language.get('variant-price'),
                    formatter: currencyFormatter
                }, {
                	label: 'Pr. net.',
                    key : 'prezzo',
                    formatter: currencyFormatter
                }, {
                    editor : new A.TextCellEditor({
                        inputFormatter : A.DataType.String.evaluate,
                        validator : {
                            rules : {
                                value : {
                                    number : true,
                                    required : true
                                }
                            }
                        }
                    }),
                    key : 'pianteRipiano',
                    label : 'Piante'//Liferay.Language.get('plant-per-platform')
                }, {
                    key: 'sormontate',
                    label : 'S.',//Liferay.Language.get('item-2-over'),
                    formatter: labelFormatter
                }, {
                    key: 's2Btn',
                    emptyCellValue : '<button type="button" name="s2" class="btn btn-mini btn-success icon-plus"/>'
                        + '<button type="button" name="mins2" class="btn btn-mini btn-danger icon-minus "/>',
                    label : Liferay.Language.get('item-2-over'),
                    allowHTML : true
                }, {
                	className: 'hidden',
                    key: 's2'
                }, {
                    key: 's3Btn',
                    emptyCellValue : '<button type="button" name="s3" class="btn btn-mini btn-success icon-plus"/>'
                        + '<button type="button" name="mins3" class="btn btn-mini btn-danger icon-minus "/>',
                    label : Liferay.Language.get('item-3-over'),
                    allowHTML : true
                }, {
                	className: 'hidden',
                    key: 's3'
                }, {
                    key: 's4Btn',
                    emptyCellValue : '<button type="button" name="s4" class="btn btn-mini btn-success icon-plus"/>'
                        + '<button type="button" name="mins4" class="btn btn-mini btn-danger icon-minus  "/>',
                    label : Liferay.Language.get('item-4-over'),
                    allowHTML : true
                }, {
                	className: 'hidden',
                    key: 's4',
                }, {
//                    editor : numberEditor,
                    key : 'importo',
                    label : 'Totale',//Liferay.Language.get('platform-amount'),
                    formatter: currencyFormatter
                }, {
                    editor : new A.TextCellEditor(),
                    key: 'ean',
                    label : Liferay.Language.get('barcode'),
                },{
                    editor : new A.TextCellEditor(),
                    key: 'prezzoEtichetta',
                    label : Liferay.Language.get('label-price'),
                },{
                	allowHTML : true,
                    editor : new A.TextAreaCellEditor(),
                    key : 'opzioni',
                    label : Liferay.Language.get('note'),
                }, {
                    key: 'deleteRow',
                    emptyCellValue : '<button type="button" name="deleteRow" class="btn btn-mini btn-warning icon-trash" />',
                    label : ' ', //Liferay.Language.get('delete-platform'),
                    allowHTML : true
                }];

                var remoteData = item.rows.sort(function(a, b) {
                    return  b.progressivo - a.progressivo;
                });
                var dt = new A.DataTable({
//                    boundingBox : '#myDataTable_' + item.id,
                    columns : nestedCols,
                    data : remoteData,
                    editEvent : 'dblclick',
                    plugins : [ {
                        cfg : {
                            highlightRange : false
                        },
                        fn : A.Plugin.DataTableHighlight
                    } ]
                }).render('#myDataTable_' + item.id);
                dt.get('boundingBox').unselectable();
                cartsTables.push(dt);
                dt.delegate('click', function(e, table) {
                    rowAction(e, table);
                }, 'tr', this, dt);
//                dt.after('*:prezzoChange', function (e) {
//                    computeAmount(dt, e);
//                    computeTotals(dt);
//                });
                dt.after('*:prezzoUnChange', function (e) {
                    computeAmount(dt, e);
                    computeTotals(dt);
                });
                dt.after('*:scontoChange', function (e) {
                    computeAmount(dt, e);
                    computeTotals(dt);
                });
                dt.after('*:importoVariantiChange', function (e) {
                    computeAmount(dt, e);
                    computeTotals(dt);
                });
                dt.after('*:pianteRipianoChange', function (e) {
                    console.log(e);
                    computeAmount(dt, e);
                    computeTotals(dt);
                });
                dt.after('*:opzioniChange', function (e) {
                    addNote(dt, e);
                });
                dt.after('*:prezzoFornitoreChange', function (e) {
                	changeSuppPrice(dt, e);
                });
                dt.after('*:prezzoEtichettaChange', function (e) {
                	changeLabelPrice(dt, e);
                });
                dt.after('*:eanChange', function (e) {
                    changeEAN(dt, e);
                });
            });
        }
    });
}
function addItemOver(rowId, cartId, itemOver) {
	updateItemOver(rowId, cartId, itemOver, true);
}

function removeItemOver(rowId, cartId, itemOver) {
	updateItemOver(rowId, cartId, itemOver, false);
}

function updateItemOver(rowId, cartId, itemOver, add) {
    AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
        var errorBlock=A.one('#group-error-block');
        A.io.request(updatePlatItemsUrl, {
            method: 'POST',
            data: {
                rowId: rowId,
                cartId: cartId,
                itemOver: itemOver,
                add: add,
                currentOrder: JSON.stringify(orderJSON)
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                    var data = JSON.parse(this.get('responseData'));
                    switch (data.code) {
                    case 0:
                        orderJSON = JSON.parse(data.message);
                        console.log(orderJSON);
                        drawProgressBars();
                        drawTables();
                        A.one('#' + portletNamespace + 'orderJSON').set('value', data.message);
                        break;
                    case 10:
                    	if (errorBlock != null) {
                    		errorBlock.empty();
						}
                        var errorMessageNode = A.Node.create('<div id="errMsg" class="portlet-msg-error">' 
                                + data.message + '</div>');
                        errorMessageNode.appendTo(errorBlock);
                        setTimeout(function(){
                    		A.one("#errMsg").toggle();
                    	}, 3000);
                        break;
                    case 11:
                    	alert("Operazione non permessa in un carrello generico");
                        break;
                    default:
                        break;
                    }
                    
                    modal.hide();
                },
                error: function name() {
                    modal.hide();
                }
            }
        });
    });
}

function deleteRow(rowId, cartId) {
    AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
        var errorBlock=A.one('#group-error-block');
        orderJSON.carts.forEach(function(elt, i) {
        	if (elt.id === cartId && elt.rows.length === 1) {
        		A.one('#cart_' + cartId).remove(true);
        	}
        });
        A.io.request(removePlatformUrl, {
            method: 'POST',
            data: {
                rowId: rowId,
                cartId: cartId,
                currentOrder: JSON.stringify(orderJSON)
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                    var data = JSON.parse(this.get('responseData'));
                    switch (data.code) {
                    case 0:
                        orderJSON = JSON.parse(data.message);
                        console.log(orderJSON);
                        drawProgressBars();
                        drawTables();
                        A.one('#' + portletNamespace + 'orderJSON').set('value', data.message);
                        break;
                    case 6:
                        var errorMessageNode = A.Node.create('<div class="portlet-msg-error">' 
                                + Liferay.Language.get('generic-error-message-x', [data.message]) + '</div>');
                        errorMessageNode.appendTo(errorBlock);
                        break;

                    default:
                        break;
                    }
                    
                    modal.hide();
                },
                error: function name() {
                    modal.hide();
                }
            }
        });
    });
}

function addPrintLabelCost(labelCost, checked) {
	AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
		var thisModal = new A.Modal({
	        bodyContent : '<div class="loading-animation"/>',
	        centered : true,
	        headerContent : '<h3>Loading...</h3>',
	        modal : true,
	        render : '#modal',
	        close: false,
	        zIndex: 99999,
	        width : 450
	    }).render();
		orderJSON.stampaEtichette = checked;
		orderJSON.carts.forEach(function(cart) {
		    cart.rows.forEach(function(row) {
	    		row.prezzoFornitore += labelCost;
		    });
		});
		drawProgressBars();
		drawTables();
		thisModal.hide();
	});
}

function lockUnlockCart(cartId) {
	AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
		var errorBlock=A.one('#group-error-block');
		var successBlock=A.one('#group-success-block');
        A.io.request(lockUnlockCartUrl, {
        	method: 'POST',
            data: {
                cartId: cartId,
                currentOrder: JSON.stringify(orderJSON)
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                	modal.hide();
                	var data = JSON.parse(this.get('responseData'));
                	switch (data.code) {
	                    case 0:
	//                    	var mess = JSON.parse(data.message);
	                    	if (successBlock != null) {
								successBlock.empty();
							}
	                    	var messageNode = A.Node.create('<div id="test" class="portlet-msg-success">' 
	                                + data.message + '</div>');
	                    	messageNode.appendTo(successBlock);
	                    	setTimeout(function(){
	                    		A.one("#test").toggle();
	                    	}, 1500);
	                        orderJSON = JSON.parse(data.data);
//	                        drawProgressBars();
//	                        drawTables();
	                        console.log(A.one('#' + portletNamespace + 'lockCart_' + cartId));
	                        var x = A.one('#' + portletNamespace + 'lockCart_' + cartId);
	                        var icon = x._node.children[0].children[0];
	                        var span = x._node.children[0].children[1];
	                        icon.classList.forEach(function(item){
	                        	if (item.indexOf('icon-unlock') >= 0) {
	                        		icon.classList.remove('icon-unlock');
	                        		icon.classList.add('icon-lock');
	                        		span.innerHTML = ' Apri ';
	                        	} else {
	                        		icon.classList.remove('icon-lock');
	                        		icon.classList.add('icon-unlock');
	                        		span.innerHTML = ' Chiudi ';
	                        	}
	                        });
	                        A.one('#' + portletNamespace + 'orderJSON').set('value', data.data);
	                    	break;
	                    case 6:
	                        var errorMessageNode = A.Node.create('<div class="portlet-msg-error">' 
	                                + Liferay.Language.get('generic-error-message-x', [data.message]) + '</div>');
	                        errorMessageNode.appendTo(errorBlock);
	                        break;
                	}
                },
                error: function name() {
                    modal.hide();
                }
            }
        });
    });
}

function deleteCart(cartId) {
	AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
		var errorBlock=A.one('#group-error-block');
		var successBlock=A.one('#group-success-block');
        A.io.request(deleteCartUrl, {
        	method: 'POST',
            data: {
                cartId: cartId,
                currentOrder: JSON.stringify(orderJSON)
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                	modal.hide();
                	var data = JSON.parse(this.get('responseData'));
                	switch (data.code) {
	                    case 0:
	//                    	var mess = JSON.parse(data.message);
	                    	if (successBlock != null) {
								successBlock.empty();
							}
	                    	var messageNode = A.Node.create('<div id="test" class="portlet-msg-success">' 
	                                + data.message + '</div>');
	                    	messageNode.appendTo(successBlock);
	                    	setTimeout(function(){
	                    		A.one("#test").toggle();
	                    	}, 1500);
	                        orderJSON = JSON.parse(data.data);
	                        A.one('#cart_' + cartId).remove(true);//.empty();
	                        A.one('#' + portletNamespace + 'orderJSON').set('value', data.data);
	                    	break;
	                    case 6:
	                        var errorMessageNode = A.Node.create('<div class="portlet-msg-error">' 
	                                + Liferay.Language.get('generic-error-message-x', [data.message]) + '</div>');
	                        errorMessageNode.appendTo(errorBlock);
	                        break;
                	}
                },
                error: function name() {
                    modal.hide();
                }
            }
        });
    });
}

function rowAction(e, table) {
    var nodeName = e.target._node.name;
    var rowId = table.getRecord(e.currentTarget).toJSON().id;
    var cartid = table.getRecord(e.currentTarget).toJSON().idCarrello;
    var itemCode = table.getRecord(e.currentTarget).toJSON().itemCode;
    var itemName = table.getRecord(e.currentTarget).toJSON().name;
    var varCode = table.getRecord(e.currentTarget).toJSON().codiceVariante;
    var varDescr = table.getRecord(e.currentTarget).toJSON().stringa;
    var price3 = table.getRecord(e.currentTarget).toJSON().prezzo3;
    var itemOver = [];
    switch(nodeName) { 
        case 'deleteRow':
            var result = confirm('Confermi l\'eliminazione del rigo selezionato');
            if (result) {
                deleteRow(rowId, cartid);
            }
            break;
        case 's4':
        	itemOver.push('s4');
        case 's3':
        	itemOver.push('s3');
        case 's2':
        	itemOver.push('s2');
        	addItemOver(rowId, cartid, itemOver);
        	break;
        case 'mins2':
        	itemOver.push('s2');
        case 'mins3':
        	itemOver.push('s3');
        case 'mins4':
        	itemOver.push('s4');
        	removeItemOver(rowId, cartid, itemOver);
        	break;
        case 'editVar':
        	editVar(rowId, cartid, itemCode, itemName, varCode, varDescr);
        	break;
        case 'prezzo3':
        	loadItem(rowId, cartid, itemCode)
        	break;
        default:
            break;
    }
}

function loadItem(rowId, cartId, itemCode) {
	AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
        var errorBlock=A.one('#group-error-block');
        A.io.request(loadItemUrl, {
            method: 'POST',
            data: {
                rowId: rowId,
                cartId: cartId,
                itemCode: itemCode,
                currentOrder: JSON.stringify(orderJSON)
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                    var data = JSON.parse(this.get('responseData'));
                    switch (data.code) {
                    case 0:
                        itemJSON = JSON.parse(data.data);
                        console.log(itemJSON);
                        usePrice3(rowId, cartId, itemJSON.prezzo3);
                        break;
                    case 6:
                        var errorMessageNode = A.Node.create('<div class="portlet-msg-error">' 
                                + Liferay.Language.get('generic-error-message-x', [data.message]) + '</div>');
                        errorMessageNode.appendTo(errorBlock);
                        break;
                    default:
                        break;
                    }
                    
                    modal.hide();
                },
                error: function name() {
                    modal.hide();
                }
            }
        });
    });
}

function usePrice3(rowId, cartId, price3) {
	orderJSON.importo = 0;
	orderJSON.carts.forEach(function(cart, i) {
		if (cart.id === cartId) {
			cart.importo = 0
			cart.rows.forEach(function(row, j) {
				if (row.id === rowId) {
					row.prezzoUn = price3;
					row.prezzoSc = row.prezzoUn * (1 - parseFloat(row.sconto) / 100);
					row.prezzo = row.prezzoSc + row.importoVarianti;
					row.importo = row.prezzo * row.pianteRipiano;
					row.modificato = true;
				}
				cart.importo += row.importo;
			});
		}
		orderJSON.importo += cart.importo;
	});
	drawProgressBars();
	drawTables();
	AUI().use('aui-base', 'aui-io-request', 'aui-modal', function(A) {
		A.one('#' + portletNamespace + 'orderJSON').set('value', JSON.stringify(orderJSON));
	});
}

function editVar(rowId, cartid, itemCode, itemName, varCode, varDescr) {
	AUI().use('aui-base', 'aui-io-request', 'aui-modal', function(A) {
		url = editVarUrl.replace('placeholder', rowId + '&cartId='
				+ cartid + '&itemCode=' + itemCode + '&itemName=' + itemName
				+ '&varCode=' + varCode + '&varDescr=' + varDescr);
		console.log(url);
	    Liferay.Util.openWindow({
	        dialog : {
	            centered : true,
	            modal : true,
	            width: 800
	        },
	        id : portletNamespace + 'varDialog',
	        title : 'Modifica Variante',
	        uri : url
	    });
	});
}

function updateVariant(data) {
	orderJSON.carts.forEach(function(cart, i) {
		if (data.cartId === cart.id) {
			cart.rows.forEach(function(row, j) {
				if (data.rowId === row.id) {
					if (row.codiceVariante === '') {
						row.stringa = data.stringa;
						row.importoVarianti = data.dettagliPianta.prezzo2 - data.dettagliPianta.prezzo1;
						row.prezzo = row.prezzoSc + row.importoVarianti;
						row.importo = row.prezzo * row.pianteRipiano;
						cart.importo += row.pianteRipiano * row.importoVarianti;
						orderJSON.importo += row.pianteRipiano * row.importoVarianti;
					} else if (row.codiceVariante !== '' && data.varCode === '') {
						row.stringa = '';
						var prVar = data.dettagliPianta.prezzo2 - data.dettagliPianta.prezzo1;
						row.importoVarianti -= prVar;
						row.prezzo = row.prezzoSc;
						row.importo = row.prezzo * row.pianteRipiano;
						cart.importo -= row.pianteRipiano * prVar;
						orderJSON.importo -= row.pianteRipiano * prVar;
					}
					row.codiceVariante = data.varCode;
					row.modificato = true;
				}
			});
		}
	});
	drawProgressBars();
	drawTables();
	AUI().use('aui-base', 'aui-io-request', 'aui-modal', function(A) {
		A.one('#' + portletNamespace + 'orderJSON').set('value', data.message);
	});
}

function drawDatePickers(){
    AUI().use('aui-datepicker', function(A) {
        var orderDatePicker = new A.DatePicker({
            trigger : '#' + portletNamespace + 'orderDate',
            mask : '%d/%m/%Y',
            popover : {
                position : 'top',
                toolbars : {
                    header : [ {
                        icon : 'icon-trash',
                        label : 'Cancella',
                        on : {
                            click : function() {
                                orderDate.clearSelection();
                            }
                        }
                    } ]
                },
                zIndex : 1
            },
            on : {
                selectionChange : function(event) {
                    console.log(event.newSelection);
                }
            },
            after : {
                selectionChange : function(event) {
                    var myDate = A.DataType.Date.addDays(new
                            Date(orderDatePicker.getSelectedDates()) , +7);
                    var day = myDate.getDate();
                    var month = myDate.getMonth() + 1;
                    var year = myDate.getFullYear();
                    
                    var myDateString = ('0' + myDate.getDate()).slice(-2) + '/'
                     + ('0' + (myDate.getMonth()+1)).slice(-2) + '/'
                     + myDate.getFullYear();
                    A.one('#' + portletNamespace + 'shipDate').set('value',
                            myDateString);
                }
            }
        });
        var shipDatePicker = new A.DatePicker({
            trigger : '#' + portletNamespace + 'shipDate',
            mask : '%d/%m/%Y',
            popover : {
                position : 'top',
                toolbars : {
                    header : [ {
                        icon : 'icon-trash',
                        label : 'Cancella',
                        on : {
                            click : function() {
                                orderDate.clearSelection();
                            }
                        }
                    } ]
                },
                zIndex : 1
            },
            on : {
                selectionChange : function(event) {
                    console.log(event.newSelection);
                }
            }
        });
        var GTDatePicker = new A.DatePicker({
            trigger : '#' + portletNamespace + 'GTorderDate',
            mask : '%d/%m/%Y',
            popover : {
                position : 'top',
                toolbars : {
                    header : [ {
                        icon : 'icon-trash',
                        label : 'Cancella',
                        on : {
                            click : function() {
                                orderDate.clearSelection();
                            }
                        }
                    } ]
                },
                zIndex : 1
            },
            on : {
                selectionChange : function(event) {
                    console.log(event.newSelection);
                }
            }
        });
    });
}

function addQuant(id) {
	AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
		var test = A.one('#' + portletNamespace + 'printBarcode').get('value');
        A.io.request(addQuantUrl, {
            method: 'POST',
            data: {
                idPlant: id,
//                customerId: customerCode,
                customerCode: customerCode,
                codeVariant: A.one('#' + portletNamespace + 'itemVariants').get('value'),
                quantity: A.one('#' + portletNamespace + 'itemQuantity').get('value'),
                orderDate: A.one('#' + portletNamespace + 'orderDate').get('value'),
                shipDate: A.one('#' + portletNamespace + 'shipDate').get('value'),
                price1: A.one('#' + portletNamespace + 'itemPrice1').get('value'),
                price2: A.one('#' + portletNamespace + 'itemPrice2').get('value'),
                discount: A.one('#' + portletNamespace + 'discount').get('value'),
                supplierPrice: ((test === "true") ? 
                		(0.10 + Number(A.one('#' + portletNamespace + 'supplierPrice').get('value'))).toString() :
                			A.one('#' + portletNamespace + 'supplierPrice').get('value')
                ), // cost,
                currentOrder: JSON.stringify(orderJSON)
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                    var data = JSON.parse(this.get('responseData'));
                    if (data.code === 0){
                        orderJSON = JSON.parse(data.message);
                        drawProgressBars();
                        drawTables();
                        A.one('#' + portletNamespace + 'orderJSON').set('value', data.message);
                    }
                    modal.hide();
                },
                error: function name() {
                    modal.hide();
                }
            }
        });
    });
}

function addPlatform(id) {
    AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
    	var test = A.one('#' + portletNamespace + 'printBarcode').get('value');
        A.io.request(addPlatformUrl, {
            method: 'POST',
            data: {
                idPlant: id,
//                customerId: customerCode,
                customerCode: customerCode,
                codeVariant: A.one('#' + portletNamespace + 'itemVariants').get('value'),
                quantity: A.one('#' + portletNamespace + 'platQuantity').get('value'),
                orderDate: A.one('#' + portletNamespace + 'orderDate').get('value'),
                shipDate: A.one('#' + portletNamespace + 'shipDate').get('value'),
                price1: A.one('#' + portletNamespace + 'itemPrice1').get('value'),
                price2: A.one('#' + portletNamespace + 'itemPrice2').get('value'),
                discount: A.one('#' + portletNamespace + 'discount').get('value'),
                supplierPrice: ((test === "true") ? 
                		(0.10 + Number(A.one('#' + portletNamespace + 'supplierPrice').get('value'))).toString() :
                			A.one('#' + portletNamespace + 'supplierPrice').get('value')
                ), // cost,
                currentOrder: JSON.stringify(orderJSON)
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                    var data = JSON.parse(this.get('responseData'));
                    if (data.code === 0){
                        orderJSON = JSON.parse(data.message);
                        console.log(orderJSON);
                        drawProgressBars();
                        drawTables();
                        A.one('#' + portletNamespace + 'orderJSON').set('value', data.message);
                    }
                    modal.hide();
                },
                error: function name() {
                    modal.hide();
                }
            }
        });
    });
}

function addCart(id) {
    AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
    	var test = A.one('#' + portletNamespace + 'printBarcode').get('value');
        A.io.request(addCartUrl, {
            method: 'POST',
            data: {
                idPlant: id,
//              customerId: customerCode,
                customerCode: customerCode,
                codeVariant: A.one('#' + portletNamespace + 'itemVariants').get('value'),
                quantity: A.one('#' + portletNamespace + 'cartQuantity').get('value'),
                item2Over: A.one('#' + portletNamespace + 'item2Over').get('value'),
                item3Over: A.one('#' + portletNamespace + 'item3Over').get('value'),
                item4Over: A.one('#' + portletNamespace + 'item4Over').get('value'),
                orderDate: A.one('#' + portletNamespace + 'orderDate').get('value'),
                shipDate: A.one('#' + portletNamespace + 'shipDate').get('value'),
                price1: A.one('#' + portletNamespace + 'itemPrice1').get('value'),
                price2: A.one('#' + portletNamespace + 'itemPrice2').get('value'),
                supplierPrice: ((test === "true") ? 
                		(0.10 + Number(A.one('#' + portletNamespace + 'supplierPrice').get('value'))).toString() :
                			A.one('#' + portletNamespace + 'supplierPrice').get('value')
                ), // cost,
                discount: A.one('#' + portletNamespace + 'discount').get('value'),
                currentOrder: JSON.stringify(orderJSON)
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                    var data = JSON.parse(this.get('responseData'));
                    if (data.code === 0){
                        orderJSON = JSON.parse(data.message);
                        console.log(orderJSON);
                        drawProgressBars();
                        drawTables();
                        A.one('#' + portletNamespace + 'orderJSON').set('value', data.message);
                    }
                    modal.hide();
                },
                error: function name() {
                    modal.hide();
                }
            }
        });
    });
}

function saveOrder() {
    AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
    	var date = A.one('#' + portletNamespace + 'orderDate').get('value');
    	date = date.split('/');
    	var orderDate = new Date(date[2] + '-' + date[1] + '-' + date[0]).getTime();
    	date = A.one('#' + portletNamespace + 'shipDate').get('value');
    	date = date.split('/');
    	var shipDate = new Date(date[2] + '-' + date[1] + '-' + date[0]).getTime();
    	var temp =  A.one('#' + portletNamespace + 'GTorderDate');
    	date = (temp !== null) ? A.one('#' + portletNamespace + 'GTorderDate').get('value') : '';
    	date = date.split('/');
    	var GTorderDate = date !== '' ? new Date(date[2] + '-' + date[1] + '-' + date[0]).getTime() : '';
    	var printLabels = A.one('#' + portletNamespace + 'printBarcode').get('value');
        A.io.request(saveOrderURL, {
            method: 'POST',
            data: {
                customerCode: customerCode,
                orderId: A.one('#' + portletNamespace + 'orderId').get('value'),
                dataInserimento: orderDate, // A.one('#' + portletNamespace + 'orderDate').get('value'),
                dataConsegna: shipDate, // A.one('#' + portletNamespace + 'shipDate').get('value'),
                billingCenter: A.one('#' + portletNamespace + 'centers').get('value'),
                vatSelected: A.one('#' + portletNamespace + 'vatSelected').get('value'),
                carrierCode: A.one('#' + portletNamespace + 'carriers').get('value'),
                cartTypeId: A.one('#' + portletNamespace + 'cartsType').get('value'),
                note: escape(A.one('#' + portletNamespace + 'note').get('value')),
                customerNote: A.one('#' + portletNamespace + 'customerNote').get('value'),
                loadPlace: A.one('#' + portletNamespace + 'loadPlace').get('value'),
                printBarcode: printLabels === "true" ? true : false, //A.one('#' + portletNamespace + 'printBarcode').get('value'),
                GTorderDate: GTorderDate,
                GTorderNum: (temp !== null) ? A.one('#' + portletNamespace + 'GTorderNum').get('value'): '',
                currentOrder: JSON.stringify(orderJSON)
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                    var data = JSON.parse(this.get('responseData'));
                    if (data.code === 0){
                        alert('Ordine salvato correttamente.');
                        var order = JSON.parse(data.message);
                        var activeTab;
                        if (A.one('#order-items').hasClass('active')) {
                        	activeTab = 'order-items';
                        }
                        if (A.one('#order-items').hasClass('active')) {
                        	activeTab = 'order-body';
                        }
                        if (A.one('#order-footer').hasClass('active')) {
                        	activeTab = 'order-footer';
                        }
                        var url = refreshURL + '&' + portletNamespace + 'orderId=' + order.id
                        			+ '&' + portletNamespace + 'customerId=' + order.idCliente
                        			+ '&' + portletNamespace + 'carriers=' + order.idTrasportatore
                        			+ '&' + portletNamespace + 'centers=' + order.centro
                        			+ '&' + portletNamespace + 'vats=' + order.codiceIva
                        			+ '&' + portletNamespace + 'cartsType=' + order.idTipoCarrello
                        			+ '&' + portletNamespace + 'orderDate=' + order.dataInserimento
                        			+ '&' + portletNamespace + 'shipDate=' + order.dataConsegna
                        			+ '&' + portletNamespace + 'loadPlace=' + order.luogoCarico
                        			+ '&' + portletNamespace + 'activeTab=' + activeTab
                        			+ '&' + portletNamespace + 'printBarcode=' + order.stampaEtichette
                        			+ '&' + portletNamespace + 'GTorderDate=' + order.dataOrdineGT
                        			+ '&' + portletNamespace + 'GTorderNum=' + order.numOrdineGT;
                        A.one(document.createElement('a')).attr('href',url).simulate('click');
                    }
                    modal.hide();
                },
                error: function () {
                    modal.hide();
                }
            }
        });
    });
}

function sendConfirm() {
    AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
        A.io.request(sendConfirmURL, {
            method: 'POST',
            data: {
                customerCode: customerCode,
                orderId: A.one('#' + portletNamespace + 'orderId').get('value'),
                customerEmail: A.one('#' + portletNamespace + 'email').get('value')
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                    var data = JSON.parse(this.get('responseData'));
                    console.log(data);
                    modal.hide();
                    var msg;
                    if (data.code === 0) {
                    	var confirmations = JSON.parse(data.message);
                    	msg = 'Conferme d\'ordine inviate con successo ai seguenti soci:\n';
                    	confirmations.forEach(function(item, i) {
                    		msg += item.recipient.ragioneSociale1 + " " + item.recipient.ragioneSociale2 + '\n';
                    	});
                    } else if (data.code === 4) {
                    	msg = "Non è stato possibile inviare la conferma d'ordine," +
                    			" accertarsi di aver salvato tutte le modifiche prima di inviare nuovamente le conferme."
                    } else {
                    	msg = 'Si è verificato un errore durante l\'invio della'
                            + ' conferma d\'ordine.\n\n' + data.message
                    }
                    alert(msg);
                },
                error: function () {
                    modal.hide();
                    alert('Si è verificato un errore durante l\'invio della'
                    + ' conferma d\'ordine.\n\n');
                }
            }
        });
    });
}

function printCartLabel() {
    AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
        A.io.request(printCartLabelURL, {
            method: 'POST',
            data: {
                orderId: A.one('#' + portletNamespace + 'orderId').get('value'),
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                    var data = JSON.parse(this.get('responseData'));
                    console.log(data);
                    modal.hide();
                    if (data.code === 0) {
                        var win = window.open(data.message.toLocaleString());
                        if (win) {
                            window.focus();
                        } else {
                            var timer = window.setTimeout(function(){
                                if (win) {
                                    win.focus();
                                }
                            }, 200 );
                        }
                    } else {
                        alert('Si è verificato un errore durante la stampa dell'
                                + '\'etichetta.\n\n' + data.message);
                    }
                    
                },
                error: function () {
                    modal.hide();
                }
            }
        });
    });
}
