<%@page import="it.bysoftware.ct.service.OrdineLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="../init.jsp"%>

<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    String orderId = String.valueOf(row.getParameter("orderId"));
    String partnerCode = String.valueOf(row.getParameter("supplier_id"));
    String backUrl = (String) row.getParameter("backUrl");
%>

<liferay-ui:icon-menu>
	<liferay-portlet:renderURL varImpl="loadSupplierOrderURL">
		<portlet:param name="mvcPath" value="/html/receiveitems/order.jsp" />
		<portlet:param name="backUrl" value="<%=backUrl%>" />
		<portlet:param name="order_id" value="<%=orderId%>" />
		<portlet:param name="supplier_id" value="<%=partnerCode%>" />
	</liferay-portlet:renderURL>
	   <liferay-ui:icon image="search" url="${loadSupplierOrderURL}" message="view" />
</liferay-ui:icon-menu>