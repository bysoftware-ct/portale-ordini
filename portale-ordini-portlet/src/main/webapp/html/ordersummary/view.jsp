<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.model.Role"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.liferay.portal.service.ClassNameLocalServiceUtil"%>
<%@page
    import="com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil"%>
<%@page import="com.liferay.portlet.expando.model.ExpandoValue"%>
<%@page import="it.bysoftware.ct.utils.ResourceId"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK"%>
<%@page import="it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.ClientiFornitoriDatiAgg"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<%@page import="it.bysoftware.ct.utils.OrderState"%>
<%@page import="it.bysoftware.ct.service.OrdineLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../init.jsp"%>

<%
String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
    String partnerCode = "";
    ExpandoValue value = ExpandoValueLocalServiceUtil.getValue(
        user.getCompanyId(),
        ClassNameLocalServiceUtil.getClassNameId(User.class),
        "CUSTOM_FIELDS", Constants.REGISTRY_CODE, user.getUserId());
    if (value != null) {
        if (!value.getData().equals("0")) {
		    partnerCode = value.getData();
		    List<Role> roles = user.getRoles();
		    for (Role role : roles) {
		        if (role.getName().equals(Constants.SPECIAL_PARTNER)) {
		            break;
		        }
		    }
        }
    } else {
        partnerCode = "-1";
    }
    SimpleDateFormat sdf = new SimpleDateFormat();
    boolean showSaved = ParamUtil.getBoolean(renderRequest,
			"showSaved", false);
    String partnerSelected = ParamUtil.getString(renderRequest,
			"partnerSelected", "");
	long fromLng = ParamUtil.getLong(renderRequest, "dateFrom", 0);
	/* System.out.println(fromLng); */
	Calendar calendarFrom = Calendar.getInstance();
	if (fromLng > 0) {
		calendarFrom.setTimeInMillis(fromLng);
	} else {
		calendarFrom = Utils.today();
	}
	Date from = calendarFrom.getTime();

	Calendar calendarTo = Calendar.getInstance();
	long toLng = ParamUtil.getLong(renderRequest, "dateTo", 0);
	/* System.out.println(toLng); */
	if (toLng > 0) {
		calendarTo.setTimeInMillis(toLng);
	} else {
		calendarTo = Utils.today();
		calendarTo.add(Calendar.DAY_OF_MONTH, 7);
	}
	Date to = calendarTo.getTime();
	
	List<Ordine> orders = new ArrayList<Ordine>();
    List<Object[]> tmp = new ArrayList<Object[]>();
    if (!partnerCode.isEmpty()) {
        tmp = OrdineLocalServiceUtil.getOrderBetweenDatePerPartner(partnerCode,
        		from, to);
    } else {
    	if (!"".equals(partnerSelected)) {
    		tmp = OrdineLocalServiceUtil.getOrderBetweenDatePerPartner(
    				partnerSelected, from, to);
        } else {
        	tmp = OrdineLocalServiceUtil.getOrderBetweenDate(from, to);
        }
        
    }
    
    for (Object[] object : tmp) {
        long orderId = Long.parseLong(String.valueOf(object[0]));
        Ordine o = OrdineLocalServiceUtil.getOrdine(orderId);
        if (partnerCode.isEmpty()){
        	if (showSaved) {
            	orders.add(o);
        	} else if (o.getStato() != OrderState.SAVED.getValue()) {
        		orders.add(o);
        	}
        } else {
        	if (o.getStato() != OrderState.SAVED.getValue()) {
            	orders.add(o);
        	}
        }
    }
    
    int i = 0;
	
	
	List<AnagraficheClientiFornitori> listPartners = new ArrayList<AnagraficheClientiFornitori>();
%>

<portlet:resourceURL var="printSummaryURL" 
    id="<%=ResourceId.printSummary.name() %>" />
    
<liferay-portlet:renderURL varImpl="reloadURL">
	<portlet:param name="mvcPath" value="/html/ordersummary/view.jsp" />
</liferay-portlet:renderURL>

<liferay-portlet:renderURL varImpl="iteratorURL">
	<portlet:param name="partnerSelected" value="<%=String.valueOf(partnerSelected) %>"/>
	<portlet:param name="showSaved" value="<%=String.valueOf(showSaved) %>"/>
	<portlet:param name="dateFrom" value="<%=String.valueOf(calendarFrom.getTimeInMillis()) %>"/>
	<portlet:param name="dateTo" value="<%=String.valueOf(calendarTo.getTimeInMillis()) %>"/>
    <portlet:param name="mvcPath" value="/html/ordersummary/view.jsp" />
</liferay-portlet:renderURL>

<liferay-ui:panel title="search" collapsible="true">
	<aui:form id="aForm" name="aForm" action="<%=reloadURL%>" method="post">
		<span class="aui-search-bar" title="search-entries">
			<c:if test="<%="".equals(partnerCode)%>">
				<aui:layout>
					<aui:column first="true" columnWidth="50">
						<%
							listPartners = AnagraficheClientiFornitoriLocalServiceUtil
													.findSuppliers();
						%>
						<aui:select name="partnerSelected" showEmptyOption="true"
							label="partner">
							<%
								for (AnagraficheClientiFornitori partner : listPartners) {
							%>
							<aui:option value="<%=partner.getCodiceAnagrafica()%>"
								label="<%=partner.getRagioneSociale1()%>"
								selected="<%=partnerSelected.equals(partner
												.getCodiceAnagrafica())%>" />
							<%
								}
							%>
						</aui:select>
					</aui:column>
					<aui:column last="true" columnWidth="50">
						<aui:input id="showSaved" type="checkbox" name="showSaved" 
	    					label="show-saved" inlineLabel="true" checked="<%= showSaved %>"/>
	    			</aui:column>
	    		</aui:layout>
			</c:if>
			<aui:layout>
				<aui:column first="true" columnWidth="50">		
					<liferay-ui:message key="ship-date-from" />
					<liferay-ui:input-date name="fromDate" cssClass="input-small"
						dayValue="<%=calendarFrom.get(Calendar.DAY_OF_MONTH)%>"
						dayParam="fromDay"
						monthValue="<%=calendarFrom.get(Calendar.MONTH)%>"
						monthParam="fromMonth"
						yearValue="<%=calendarFrom.get(Calendar.YEAR)%>"
						yearParam="fromYear" />
				</aui:column>
				<aui:column last="true" columnWidth="50">
					<liferay-ui:message key="to" />
					<liferay-ui:input-date name="toDate" cssClass="input-small"
						dayValue="<%=calendarTo.get(Calendar.DAY_OF_MONTH)%>"
						dayParam="toDay" monthValue="<%=calendarTo.get(Calendar.MONTH)%>"
						monthParam="toMonth"
						yearValue="<%=calendarTo.get(Calendar.YEAR)%>" yearParam="toYear" />
				</aui:column>
			</aui:layout> 
			<aui:button-row>
				<aui:button name="searchBtn" type="button" value="search" />
				<aui:button type="cancel" onclick="this.form.reset()" />
			</aui:button-row>
		</span>
	</aui:form>
</liferay-ui:panel>

<aui:nav-bar>
	<aui:nav>
		<aui:nav-item id="printBtn" iconCssClass="icon-print" label="print"
			selected='<%="print".equals(toolbarItem)%>' />
	</aui:nav>
</aui:nav-bar>

<liferay-ui:search-container delta="75" emptyResultsMessage="no-orders"
     iteratorURL="<%=iteratorURL%>" >
    <liferay-ui:search-container-results>
        <%
            results = ListUtil.subList(orders,
                                    searchContainer.getStart(),
                                    searchContainer.getEnd());
                            total = orders.size();
                            pageContext.setAttribute("results", results);
                            pageContext.setAttribute("total", total);
        %>
    </liferay-ui:search-container-results>
    <liferay-ui:search-container-row
        className="it.bysoftware.ct.model.Ordine" modelVar="order">
        <%
        sdf.applyLocalizedPattern("dd/MM/yyyy");
        String orderDate = sdf.format(order.getDataInserimento());
        String shipDate = sdf.format(order.getDataConsegna());
        AnagraficheClientiFornitori c = AnagraficheClientiFornitoriLocalServiceUtil.
                fetchAnagraficheClientiFornitori(String.valueOf(order.getIdCliente()));
        AnagraficheClientiFornitori partner = null;
        %>
        <liferay-ui:search-container-column-text name="number"
            value="<%= order.getNumero() + "/" + order.getCentro() %>" />
        <liferay-ui:search-container-column-text value="<%= shipDate %>"name="ship-date" />
        <c:if test="<%= !partnerCode.isEmpty() %>">
	        <liferay-ui:search-container-column-text
	            value="<%=c != null ? c.getCodiceAnagrafica() : ""%>" name="customer" />
        </c:if>
        
        <c:if test="<%= partnerCode.isEmpty() %>">
        	<liferay-ui:search-container-column-text
            	value="<%=c != null ? c.getRagioneSociale1() : ""%>" name="customer" />
            <%
            partner = AnagraficheClientiFornitoriLocalServiceUtil.
                    fetchAnagraficheClientiFornitori(String.valueOf(tmp.get(i)[1]));
            if (Validator.isNotNull(partner)) {
                ClientiFornitoriDatiAgg data = ClientiFornitoriDatiAggLocalServiceUtil.
                        getClientiFornitoriDatiAgg(new ClientiFornitoriDatiAggPK(
                                String.valueOf(tmp.get(i)[1]), true));
            %>
                <liferay-ui:search-container-column-text name="supplier"
                    value="<%= partner.getRagioneSociale1() %>"/>
            <%
            }
            i++; 
            %>
        </c:if>
        <liferay-ui:search-container-column-text name="status">
            <aui:workflow-status showIcon="<%= false %>" showLabel="<%= false %>"
                status="<%= order.getStato() %>" />
        </liferay-ui:search-container-column-text>
        <liferay-ui:search-container-row-parameter name="fromLng"
                        value="<%=fromLng %>" />
       	<liferay-ui:search-container-row-parameter name="toLng"
                        value="<%=toLng %>" />
        <liferay-ui:search-container-row-parameter name="showCustomerDetails"
                        value="<%=partnerCode.isEmpty() %>" />
        <liferay-ui:search-container-row-parameter name="showSaved"
                        value="<%=showSaved %>" />
        <liferay-ui:search-container-row-parameter name="orderId"
                        value="<%=order.getId() %>" />
        <liferay-ui:search-container-row-parameter name="partnerCode"
                        value="<%=(partnerCode.isEmpty()) ? partner.getCodiceAnagrafica() : partnerCode %>" />
        <liferay-ui:search-container-column-jsp path="/html/ordersummary/action.jsp" />
        <liferay-ui:search-container-row-parameter name="backUrl"
            value="<%= iteratorURL.toString() %>" />
    </liferay-ui:search-container-row>
    <liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
        paginate="true" />
</liferay-ui:search-container>

<aui:script>

var printSummaryURL = '<%= printSummaryURL %>';

AUI().use('aui-base', 'datatype-number', function(A) {
    A.one('#<portlet:namespace/>searchBtn').on('click', function() {
        var fromDateStr = A.one('#<portlet:namespace />fromDate').get('value');
        var fromDate = parseDate(fromDateStr);
        var toDateStr = A.one('#<portlet:namespace/>toDate').get('value');
        var toDate = parseDate(toDateStr);
        if (A.one('#<portlet:namespace/>partnerSelected') !== null) {
	        var partnerSelected = A.one('#<portlet:namespace/>partnerSelected').get('value');
	        var showSaved = A.one('#<portlet:namespace/>showSaved').get('value');
	        window.location.href = '<%=reloadURL%>&<portlet:namespace/>dateFrom=' + fromDate.getTime() + '&<portlet:namespace/>dateTo=' + toDate.getTime() + '&<portlet:namespace/>partnerSelected=' + partnerSelected + '&<portlet:namespace/>showSaved=' + showSaved;
        } else {
        	window.location.href = '<%=reloadURL%>&<portlet:namespace/>dateFrom=' + fromDate.getTime() + '&<portlet:namespace/>dateTo=' + toDate.getTime();
        }
    });
});

AUI().use('aui-base', 'datatype-number', function(A) {
	if (A.one("#<portlet:namespace/>printBtn")) {
		A.one("#<portlet:namespace/>printBtn").on('click', function() {
		 	var fromDateStr = A.one('#<portlet:namespace />fromDate').get('value');
	        var fromDate = parseDate(fromDateStr);
	        var toDateStr = A.one('#<portlet:namespace/>toDate').get('value');
	        var toDate = parseDate(toDateStr);
            var msg = 'L\'intervallo di date specificato non è valido.';
            if (fromDate <= toDate) {
            	printSummaryReport();
            } else {
                alert(msg);
            }
        });
	}
});

function parseDate(string) {
    var tmp = string.split('/');
    return new Date(tmp[2] + '-' + tmp[1] + '-' + tmp[0]);
}

function printSummaryReport() {
    AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
    	var fromDateStr = A.one('#<portlet:namespace />fromDate').get('value');
    	var toDateStr = A.one('#<portlet:namespace />toDate').get('value');
    	var fromDate = parseDate(fromDateStr).getTime();
    	var toDate = parseDate(toDateStr).getTime();
        A.io.request(printSummaryURL, {
            method: 'POST',
            data: {
            	<portlet:namespace />fromDate: fromDate,
            	<portlet:namespace />toDate: toDate,
            	<portlet:namespace />showSaved: '<%=showSaved%>',
            	<portlet:namespace />partnerCode: '<%= partnerCode.isEmpty() ? partnerSelected : partnerCode%>',
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                    var data = JSON.parse(this.get('responseData'));
                    modal.hide();
                    if (data.code === 0) {
                    	if (data.message.startsWith("JVB")) {
                    		data.message = "data:application/pdf;base64," + data.message;
                    	    downloadFileObject(data.message);
                    	} else if (data.message.startsWith("data:application/pdf;base64")) {
                    	    downloadFileObject(data.message);
                    	} else {
                    	    alert("Not a valid Base64 PDF string. Please check");
                    	}
                   	} else {
                   		alert('Si è verificato un errore durante la stampa del'
                   				+ ' report.\n\n' + data.message);
                   	}
                    
                },
                error: function () {
                    modal.hide();
                }
            }
        });
    });
}

function downloadFileObject(base64String) {
	const linkSource = base64String;
	const downloadLink = document.createElement("a");
	const fileName = (Math.random() + 1).toString(36).substring(2) + ".pdf";
	downloadLink.href = linkSource;
	downloadLink.download = fileName;
	downloadLink.click();
}
</aui:script>