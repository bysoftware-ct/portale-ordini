<%@page import="it.bysoftware.ct.service.OrdineLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="../init.jsp"%>

<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    Ordine order = (Ordine) row.getObject();
    String fromLng = String.valueOf(row.getParameter("fromLng"));
    String toLng = String.valueOf(row.getParameter("toLng"));
    String orderId = String.valueOf(row.getParameter("orderId"));
    String partnerCode = String.valueOf(row.getParameter("partnerCode"));
    String backUrl = (String) row.getParameter("backUrl");
    boolean showCustomerDetails = Boolean.parseBoolean(String.valueOf(row.getParameter("showCustomerDetails")));
%>

<liferay-ui:icon-menu>
	<liferay-portlet:renderURL varImpl="viewOrderURL">
		<portlet:param name="mvcPath" value="/html/ordersummary/order.jsp" />
		<portlet:param name="backUrl" value="<%=backUrl%>" />
		<portlet:param name="fromLng" value="<%=fromLng%>" />
		<portlet:param name="toLng" value="<%=toLng%>" />
		<portlet:param name="orderId" value="<%=String.valueOf(order.getId())%>" />
		<portlet:param name="customerCode" value="<%=String.valueOf(order.getIdCliente())%>" />
		<portlet:param name="partnerCode" value="<%=partnerCode%>" />
		<portlet:param name="showCustomerDetails" value="<%=String.valueOf(showCustomerDetails)%>" />
	</liferay-portlet:renderURL>
   	<liferay-ui:icon image="search" url="${viewOrderURL}" message="view" />
</liferay-ui:icon-menu>