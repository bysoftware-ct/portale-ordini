<%@page import="com.liferay.portal.model.User"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="com.liferay.portal.service.ClassNameLocalServiceUtil"%>
<%@page import="com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil"%>
<%@page import="com.liferay.portlet.expando.model.ExpandoValue"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="it.bysoftware.ct.service.CategorieMerceologicheLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.CategorieMerceologiche"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="it.bysoftware.ct.utils.ResourceId"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.service.PianteLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Piante"%>
<%@page import="it.bysoftware.ct.service.ArticoliLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Articoli"%>
<%@page import="it.bysoftware.ct.service.RigaLocalServiceUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="it.bysoftware.ct.model.Riga"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="it.bysoftware.ct.service.CarrelloLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Carrello"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<%@page import="it.bysoftware.ct.service.OrdineLocalServiceUtil"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../init.jsp"%>

<%
    long orderId = ParamUtil.getLong(renderRequest, "orderId", 0);
	long fromLng = ParamUtil.getLong(renderRequest, "fromLng", 0);
	long toLng = ParamUtil.getLong(renderRequest, "toLng", 0);
    String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
    String backUrl = ParamUtil.getString(renderRequest, "backUrl");
    String customerCode = ParamUtil.getString(renderRequest,
            "customerCode");
    String partnerCode = ParamUtil.getString(renderRequest,
            "partnerCode");
    boolean showCustomerDetails = ParamUtil.getBoolean(renderRequest,
            "showCustomerDetails", true);
    ExpandoValue value = ExpandoValueLocalServiceUtil.getValue(
            user.getCompanyId(),
            ClassNameLocalServiceUtil.getClassNameId(User.class),
            "CUSTOM_FIELDS", Constants.REGISTRY_CODE, user.getUserId());
    
    List<Piante> items = new ArrayList<Piante>();
    AnagraficheClientiFornitori customer = null;
    AnagraficheClientiFornitori supplier = null;
    Ordine order = null;
    String lbl = "";
    String oldPassport = "";
    int idx = 0;
    Map<String, Integer> itemsCounter = new HashMap<String, Integer>();
    int itemsTotal = 0;
    if (orderId > 0) {
        customer = AnagraficheClientiFornitoriLocalServiceUtil
                .getAnagraficheClientiFornitori(customerCode);
        supplier = AnagraficheClientiFornitoriLocalServiceUtil
                .getAnagraficheClientiFornitori(partnerCode);
        order = OrdineLocalServiceUtil.getOrdine(orderId);
        lbl = LanguageUtil.format(pageContext, "order-x",
                new String[] { String.valueOf(order.getNumero()),
                        String.valueOf(order.getCentro()) });
        Map<String, Piante> itemsWrapper = new HashMap<String, Piante>();
        Map<String, String> passportWrapper = new HashMap<String, String>();
        List<Carrello> carts = CarrelloLocalServiceUtil
                .findCartsInOrder(orderId);
        for (Carrello cart : carts) {
            List<Riga> rows = RigaLocalServiceUtil.findRowsInCart(cart
                    .getId());
            String oldCode = "";
            for (Riga r : rows) {
                Piante p = PianteLocalServiceUtil.getPiante(r
                        .getIdArticolo());
                if (supplier.getCodiceAnagrafica().equals(p.getIdFornitore())) {
                    if (!r.getPassaporto().isEmpty()) {
                        oldPassport = r.getPassaporto();
                    }
                    if (itemsCounter.containsKey(p.getCodice())) {
                        itemsCounter.put(p.getCodice(), r.getPianteRipiano() 
                                + itemsCounter.get(p.getCodice()));
                    } else {
                        itemsCounter.put(p.getCodice(), r.getPianteRipiano());
                    }
                }
                if (partnerCode.equals(p.getIdFornitore()) && p.getPassaporto()) {
                    itemsWrapper.put(p.getCodice(), p);
                    passportWrapper.put(p.getCodice(), r.getPassaporto());
//                     oldCode = p.getCodice();
                }
//                 itemsWrapper.put(p.getCodice(), p);
            }
        }
        for (String k : itemsWrapper.keySet()) {
            items.add(itemsWrapper.get(k));
        }
    }
    _log.info(showCustomerDetails);
%>
<portlet:resourceURL var="printSummaryURL" 
    id="<%=ResourceId.printSummary.name() %>" />
<liferay-portlet:actionURL name="saveDefaultPassportNumber" var="saveDefaultPassportNumber" >
    <liferay-portlet:param name="orderId" value="<%= String.valueOf(order.getId()) %>"/>
    <liferay-portlet:param name="partnerCode" value="<%= partnerCode %>"/>
    <liferay-portlet:param name="customerCode" value="<%= customerCode %>"/>
    <liferay-portlet:param name="backUrl" value="<%= backUrl %>"/>
</liferay-portlet:actionURL>
<aui:nav-bar>
	<aui:nav>
		<aui:nav-item id="printBtn" iconCssClass="icon-print" label="print"
			selected='<%="print".equals(toolbarItem)%>' />
	</aui:nav>
</aui:nav-bar>
<c:choose>
	<c:when test="<%=order != null%>">
	    <liferay-ui:header title="<%=lbl %>" backURL="<%=backUrl %>" />
		<liferay-portlet:renderURL varImpl="iteratorURL">
			<portlet:param name="partnerCode" value="<%=partnerCode %>"/>
			<portlet:param name="backUrl" value="<%=backUrl %>"/>
			<portlet:param name="orderId" value="<%=String.valueOf(orderId) %>"/>
			<portlet:param name="customerCode" value="<%=customerCode %>"/>
			<portlet:param name="mvcPath" value="/html/passport/order.jsp" />
		</liferay-portlet:renderURL>
		
			<aui:layout>
			    <aui:column columnWidth="45" first="true">
			    	<c:choose>
						<c:when test="<%=!showCustomerDetails%>">
							<aui:input name="customerName" id="customerName" type="textarea"
								label="customer" disabled="true" resizable="false"
								cssClass="input-xlarge" inlineField="true"
								value="<%=customer.getCodiceAnagrafica()%>" />
						</c:when>
						<c:otherwise>
							<aui:input name="customerName" id="customerName" type="textarea"
								label="customer" disabled="true" resizable="false"
								cssClass="input-xlarge" inlineField="true"
								value="<%=customer.getRagioneSociale1() + customer.getRagioneSociale2()%>" />
						</c:otherwise>
					</c:choose>
					<aui:input name="address" id="address" type="textarea"
						label="address" disabled="true" cssClass="input-xlarge"
						inlineField="true" value="<%=Utils.getFullAddress(customer)%>" />
				</aui:column>
				<c:if test="<%=value == null %>">
				    <aui:column columnWidth="45" last="true">
						<aui:input name="supplierName" id="supplierName" type="textarea"
		                        label="supplier" disabled="true" resizable="false"
		                        cssClass="input-xlarge" inlineField="true"
		                        value="<%=supplier.getRagioneSociale1() + "\n"
		                                        + supplier.getRagioneSociale2()%>" />
	                    <aui:input name="address" id="address" type="textarea"
	                        label="address" disabled="true" cssClass="input-xlarge"
	                        inlineField="true" value="<%=Utils.getFullAddress(supplier)%>" />
                    </aui:column>
                </c:if>
			</aui:layout>
		<aui:fieldset label="items-summary">
			<aui:form id="aForm" name="aForm" method="post">
				<liferay-ui:search-container delta="75" emptyResultsMessage="no-orders"
					 iteratorURL="<%=iteratorURL%>" >
					<liferay-ui:search-container-results>
						<%
						    results = ListUtil.subList(items,
						                            searchContainer.getStart(),
						                            searchContainer.getEnd());
						                    total = items.size();
						                    pageContext.setAttribute("results", results);
						                    pageContext.setAttribute("total", total);
						%>
					</liferay-ui:search-container-results>
					<liferay-ui:search-container-row keyProperty="codice"
						className="it.bysoftware.ct.model.Piante" modelVar="item">
						<%
						String forma = "";
			            Articoli art = ArticoliLocalServiceUtil.fetchArticoli(item.getCodice());
			            if(art != null) {
			                try {
			                    CategorieMerceologiche cat = CategorieMerceologicheLocalServiceUtil.fetchCategorieMerceologiche(art.getCategoriaMerceologica());
			                    if (cat != null) {
			                        forma = cat.getDescrizione();
			                    }
			                } catch (Exception ex) {
			                    _log.debug(ex.getMessage());
			                }
			            }
			            idx++;
			            itemsTotal = itemsTotal + itemsCounter.get(item.getCodice());
						%>
						<liferay-ui:search-container-column-text property="codice" name="code" />
						<liferay-ui:search-container-column-text property="nome" name="name" />
						<liferay-ui:search-container-column-text name="shape" value="<%=forma %>"/>
						<liferay-ui:search-container-column-text property="vaso" name="vase" />
						<liferay-ui:search-container-column-text name="H" property="altezzaPianta"/>
						<liferay-ui:search-container-column-text name="quantity" value="<%=String.valueOf(itemsCounter.get(item.getCodice())) %>" />
					</liferay-ui:search-container-row>
					<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
						paginate="true" />
				</liferay-ui:search-container>
			</aui:form>
		</aui:fieldset>
		<aui:input name="items-total" value="<%=String.valueOf(itemsTotal) %>" label="cart-total" disabled="true" />
	</c:when>
	<c:otherwise>
		<div class="portlet-msg-info">
			<liferay-ui:message key="no-order" />
		</div>
	</c:otherwise>
</c:choose>

<aui:script>
var printSummaryURL = '<%= printSummaryURL %>';

AUI().use('aui-base', 'datatype-number', function(A) {
	if (A.one("#<portlet:namespace/>printBtn")) {
		A.one("#<portlet:namespace/>printBtn").on('click', function() {
            	printSummaryReport();
        });
	}
});

function printSummaryReport() {
    AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
        A.io.request(printSummaryURL, {
            method: 'POST',
            data: {
            	<portlet:namespace />fromDate: '<%=fromLng %>',
            	<portlet:namespace />toDate: '<%=toLng %>',
            	<portlet:namespace />orderId: '<%=orderId%>',
            	<portlet:namespace />showSaved: '<%=showCustomerDetails%>',
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                    var data = JSON.parse(this.get('responseData'));
                    modal.hide();
                    if (data.code === 0) {
                    	if (data.message.startsWith("JVB")) {
                    		data.message = "data:application/pdf;base64," + data.message;
                    	    downloadFileObject(data.message);
                    	} else if (data.message.startsWith("data:application/pdf;base64")) {
                    	    downloadFileObject(data.message);
                    	} else {
                    	    alert("Not a valid Base64 PDF string. Please check");
                    	}
                   	} else {
                   		alert('Si è verificato un errore durante la stampa del'
                   				+ ' report.\n\n' + data.message);
                   	}
                    
                },
                error: function () {
                    modal.hide();
                }
            }
        });
    });
}

function downloadFileObject(base64String) {
	const linkSource = base64String;
	const downloadLink = document.createElement("a");
	const fileName = (Math.random() + 1).toString(36).substring(2) + ".pdf";
	downloadLink.href = linkSource;
	downloadLink.download = fileName;
	downloadLink.click();
}
</aui:script>

<%!private static Log _log = LogFactoryUtil
            .getLog("docroot.html.passport.order_jsp");%>