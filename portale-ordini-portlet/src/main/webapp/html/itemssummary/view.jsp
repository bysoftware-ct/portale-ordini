<%@page import="java.util.TreeMap"%>
<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="it.bysoftware.ct.utils.OrderState"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="it.bysoftware.ct.service.PianteLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Piante"%>
<%@page import="it.bysoftware.ct.service.OrdineLocalServiceUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Calendar"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../init.jsp"%>

<%
	String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
   
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    boolean showSaved = ParamUtil.getBoolean(renderRequest,
			"showSaved", false);
    String itemSelected = ParamUtil.getString(request, "itemSelected", "");
	long fromLng = ParamUtil.getLong(renderRequest, "dateFrom", 0);

	Calendar calendarFrom = Calendar.getInstance();
	if (fromLng > 0) {
		calendarFrom.setTimeInMillis(fromLng);
	} else {
		calendarFrom = Utils.today();
	}
	Date from = calendarFrom.getTime();

	Calendar calendarTo = Calendar.getInstance();
	long toLng = ParamUtil.getLong(renderRequest, "dateTo", 0);
	if (toLng > 0) {
		calendarTo.setTimeInMillis(toLng);
	} else {
		calendarTo = Utils.today();
		calendarTo.add(Calendar.DAY_OF_MONTH, 7);
	}
	Date to = calendarTo.getTime();
	
    List<Object[]> tmp = new ArrayList<Object[]>();
   
   	if (!"".equals(itemSelected)) {
   		tmp = OrdineLocalServiceUtil.getOrderBetweenDatePerItem(
   				itemSelected, from, to);
    } else {
    	tmp = OrdineLocalServiceUtil.getItemsInOrderBetweenDate(from, to);
    }
   	Map<String, List<Ordine>> itemsInorder = new TreeMap<String, List<Ordine>>();
   	/* List<Piante> itemsInorder = new ArrayList<Piante>(); */
    for (Object[] object : tmp) {
        long orderId = Long.parseLong(String.valueOf(object[0]));
        Ordine o = OrdineLocalServiceUtil.getOrdine(orderId);
        String cod = String.valueOf(object[1]);
       	if (showSaved) {
       		if (itemsInorder.keySet().contains(cod)) {
               	itemsInorder.get(cod).add(o);
       		} else {
       			List<Ordine> orders = new ArrayList<Ordine>();
       			orders.add(o);
       			itemsInorder.put(cod, orders);
       		}
       	} else if (o.getStato() != OrderState.SAVED.getValue()) {
       		if (itemsInorder.keySet().contains(cod)) {
               	itemsInorder.get(cod).add(o);
       		} else {
       			List<Ordine> orders = new ArrayList<Ordine>();
       			orders.add(o);
       			itemsInorder.put(cod, orders);
       		}
       	}
        
    }
	
%>

<liferay-portlet:renderURL varImpl="reloadURL">
	<portlet:param name="mvcPath" value="/html/itemssummary/view.jsp" />
</liferay-portlet:renderURL>

<%-- <liferay-ui:panel title="search" collapsible="true"> --%>
<aui:form id="aForm" name="aForm" action="<%=reloadURL%>" method="post">
	<span class="aui-search-bar" title="search-entries">
		<aui:layout>
			<aui:column first="true" columnWidth="50">
				<aui:select name="itemSelected" showEmptyOption="true"
					label="items">
					<%
						for (String k : itemsInorder.keySet()) {
							Piante p = PianteLocalServiceUtil.findPlants(k);
					%>
					<aui:option value="<%=p.getCodice()%>"
						label="<%=p.getCodice() + " - " + p.getNome() + " Vaso: " + p.getVaso() + " " + p.getForma() + " H. " + p.getAltezzaPianta()%>"
						selected="<%=itemSelected.equals(p.getCodice())%>" />
					<%
						}
					%>
				</aui:select>
			</aui:column>
			<aui:column last="true" columnWidth="50">
				<aui:input id="showSaved" type="checkbox" name="showSaved" 
   					label="show-saved" inlineLabel="true" checked="<%= showSaved %>"/>
   			</aui:column>
   		</aui:layout>
		<aui:layout>
			<aui:column first="true" columnWidth="50">		
				<liferay-ui:message key="ship-date-from" />
				<liferay-ui:input-date name="fromDate" cssClass="input-small"
					dayValue="<%=calendarFrom.get(Calendar.DAY_OF_MONTH)%>"
					dayParam="fromDay"
					monthValue="<%=calendarFrom.get(Calendar.MONTH)%>"
					monthParam="fromMonth"
					yearValue="<%=calendarFrom.get(Calendar.YEAR)%>"
					yearParam="fromYear" />
			</aui:column>
			<aui:column last="true" columnWidth="50">
				<liferay-ui:message key="to" />
				<liferay-ui:input-date name="toDate" cssClass="input-small"
					dayValue="<%=calendarTo.get(Calendar.DAY_OF_MONTH)%>"
					dayParam="toDay" monthValue="<%=calendarTo.get(Calendar.MONTH)%>"
					monthParam="toMonth"
					yearValue="<%=calendarTo.get(Calendar.YEAR)%>" yearParam="toYear" />
			</aui:column>
		</aui:layout> 
		<aui:button-row>
			<aui:button name="searchBtn" type="button" value="search" />
			<aui:button type="cancel" onclick="this.form.reset()" />
		</aui:button-row>
	</span>
</aui:form>
<%-- </liferay-ui:panel> --%>

<%
	for (String k : itemsInorder.keySet()) {
		List<Ordine> orders = itemsInorder.get(k);
		Piante p = PianteLocalServiceUtil.findPlants(k);
		String title = p.getCodice() + " - " + p.getNome() + " Vaso: " + p.getVaso() + " "
			+ p.getForma() + " H. " + p.getAltezzaPianta();
%>
	<liferay-ui:panel title="<%= title %>" id="<%= "item-" + String.valueOf(p.getId()) %>" state="collapsed">
		<liferay-portlet:renderURL varImpl="iteratorURL" >
			<portlet:param name="showSaved" value="<%=String.valueOf(showSaved) %>"/>
			<portlet:param name="itemSelected" value="<%=String.valueOf(itemSelected) %>"/>
			<portlet:param name="dateFrom" value="<%=String.valueOf(calendarFrom.getTimeInMillis()) %>"/>
			<portlet:param name="dateTo" value="<%=String.valueOf(calendarTo.getTimeInMillis()) %>"/>
		    <portlet:param name="mvcPath" value="/html/itemssummary/view.jsp" />
		</liferay-portlet:renderURL>
		<liferay-ui:search-container delta="75" emptyResultsMessage="no-orders"
			 iteratorURL="<%=iteratorURL%>" curParam="<%=k %>">
			 <liferay-ui:search-container-results>
					<%
					    results = ListUtil.subList(orders,
					                            searchContainer.getStart(),
					                            searchContainer.getEnd());
					                    total = orders.size();
					                    pageContext.setAttribute("results", results);
					                    pageContext.setAttribute("total", total);
					%>
				</liferay-ui:search-container-results>
				<liferay-ui:search-container-row keyProperty="id" className="it.bysoftware.ct.model.Ordine" modelVar="order">
					<liferay-portlet:renderURL varImpl="rowURL">
	                	<portlet:param name="orderId" value="<%= String.valueOf(order.getId()) %>" />
	            	</liferay-portlet:renderURL>
		            <liferay-ui:search-container-column-text name="number" value="<%=order.getNumero() + " / " + order.getCentro() %>" orderable="<%= true %>"/>
		            <%
		            String orderDateStr = sdf.format(order.getDataInserimento());
		            String shipDateStr = sdf.format(order.getDataConsegna());
		            AnagraficheClientiFornitori customer =
		                    AnagraficheClientiFornitoriLocalServiceUtil.fetchAnagraficheClientiFornitori(order.getIdCliente());
		            String[] customerInfo = new String[] { "", "", "", "", "", "" };
		            if (customer != null) {
		            	customerInfo[0] = customer.getRagioneSociale1() + " " + customer.getRagioneSociale2();
		            	customerInfo[1] = customer.getIndirizzo();
		            	customerInfo[2] = customer.getCAP();
		            	customerInfo[3] = customer.getComune();
		            	customerInfo[4] = customer.getSiglaProvincia();
		            	customerInfo[5] = customer.getSiglaStato();
		            }
		            %>
		            <liferay-ui:search-container-column-text name="code" value="<%= order.getIdCliente() %>"/>
		            <liferay-ui:search-container-column-text name="company-name" orderable="true">
		            	<liferay-ui:message key="company-name-x" arguments="<%= customerInfo %>" />
		            </liferay-ui:search-container-column-text>
		            <liferay-ui:search-container-column-text name="order-date"  value="<%=orderDateStr %>" orderable="<%= true %>"/>
		            <liferay-ui:search-container-column-text name="ship-date"  value="<%=shipDateStr %>" orderable="<%= true %>"/>
		            <liferay-ui:search-container-column-text name="carrier" value="<%= order.getVettore() %>" orderable="<%= true %>"/>
		            <liferay-ui:search-container-column-text name="note" >
	            	<c:if test="<%= !order.getNote().isEmpty() %>">
	            		<input id="inputNote_<%= order.getId()%>" type="hidden" value="<%=order.getNote() %>" />
	            		<input id="myNote_<%= order.getId()%>" class="note-btn" type="button" value="Vedi" />
	            		
	            	</c:if>
	            </liferay-ui:search-container-column-text>
	            <liferay-ui:search-container-row-parameter name="backURL"
            			value="<%= iteratorURL.toString() %>" />
            	<liferay-ui:search-container-column-jsp path="/html/itemssummary/action.jsp" />
			</liferay-ui:search-container-row>
			<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
							paginate="true" />
		</liferay-ui:search-container>
	</liferay-ui:panel>
<%
	}
%>

<aui:script>
AUI().use('aui-base', 'datatype-number', function(A) {
    A.one('#<portlet:namespace/>searchBtn').on('click', function() {
        var fromDateStr = A.one('#<portlet:namespace />fromDate').get('value');
        var fromDate = parseDate(fromDateStr);
        var toDateStr = A.one('#<portlet:namespace/>toDate').get('value');
        var toDate = parseDate(toDateStr);
        if (A.one('#<portlet:namespace/>itemSelected') !== null) {
	        var itemSelected = A.one('#<portlet:namespace/>itemSelected').get('value');
	        var showSaved = A.one('#<portlet:namespace/>showSaved').get('value');
	        window.location.href = '<%=reloadURL%>&<portlet:namespace/>dateFrom=' + fromDate.getTime() + '&<portlet:namespace/>dateTo=' + toDate.getTime() + '&<portlet:namespace/>itemSelected=' + itemSelected + '&<portlet:namespace/>showSaved=' + showSaved;
        } else {
        	window.location.href = '<%=reloadURL%>&<portlet:namespace/>dateFrom=' + fromDate.getTime() + '&<portlet:namespace/>dateTo=' + toDate.getTime();
        }
    });
});

function parseDate(string) {
    var tmp = string.split('/');
    return new Date(tmp[2] + '-' + tmp[1] + '-' + tmp[0]);
}
</aui:script>