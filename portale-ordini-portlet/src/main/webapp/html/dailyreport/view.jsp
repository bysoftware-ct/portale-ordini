<%@page import="it.bysoftware.ct.utils.ResourceId"%>
<%@page import="it.bysoftware.ct.DailyReport"%>
<%@page import="it.bysoftware.ct.model.TestataDocumento"%>
<%@page
	import="it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK"%>
<%@page
	import="it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.ClientiFornitoriDatiAgg"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page
	import="it.bysoftware.ct.service.TestataDocumentoLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.OrdineLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="java.util.Calendar"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="../init.jsp"%>

<%
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
	String shipDate = ParamUtil.getString(renderRequest, "shipDate", "");
	long dateLng = 0;
	Date from = null;
	Calendar calendarFrom = Calendar.getInstance();
	if (!shipDate.isEmpty()){
		from = sdf.parse(shipDate);
		calendarFrom.setTime(from);
	} else {
		calendarFrom = Utils.today();
		from = calendarFrom.getTime();
	}
	String partnerSelected = ParamUtil.getString(renderRequest, "partnerSelected", "");
	List<AnagraficheClientiFornitori> partners = AnagraficheClientiFornitoriLocalServiceUtil.findSuppliers();
%>
<liferay-portlet:renderURL varImpl="dailyreportURL">
	<portlet:param name="mvcPath" value="/html/dailyreport/view.jsp" />
</liferay-portlet:renderURL>
<liferay-ui:panel title="search" collapsible="true">
	<aui:form id="aForm" name="aForm" action="<%=dailyreportURL%>"
		method="post">
		<span class="aui-search-bar" title="search-entries"> <aui:layout>
				<aui:column first="true" columnWidth="50">
					<liferay-ui:message key="ship-date" />
					<liferay-ui:input-date name="shipDate" cssClass="input-small"
						dayValue="<%=calendarFrom.get(Calendar.DAY_OF_MONTH)%>"
						dayParam="fromDay"
						monthValue="<%=calendarFrom.get(Calendar.MONTH)%>"
						monthParam="fromMonth"
						yearValue="<%=calendarFrom.get(Calendar.YEAR)%>"
						yearParam="fromYear" />
				</aui:column>
				<aui:column last="true" columnWidth="50">
					<aui:select name="partnerSelected" showEmptyOption="true"
						label="partner">
						<%
							for (AnagraficheClientiFornitori partner : partners) {
						%>
						<aui:option value="<%=partner.getCodiceAnagrafica()%>"
							label="<%=partner.getRagioneSociale1()%>"
							selected="<%=partnerSelected.equals(partner
											.getCodiceAnagrafica())%>" />
						<%
							}
						%>
					</aui:select>
				</aui:column>
			</aui:layout> <aui:button-row>
				<aui:button name="searchBtn" type="submit" value="search" />
				<aui:button type="cancel" onclick="this.form.reset()" />
			</aui:button-row>
		</span>
	</aui:form>
</liferay-ui:panel>
<portlet:resourceURL var="printDailyReportURL" 
    		id="<%=ResourceId.printDailyReport.name() %>" />
<c:choose>
	<c:when test="<%=partnerSelected.isEmpty()%>">
		<div class="alert alert-warn">
			<liferay-ui:message key="select-supplier" />
		</div>
	</c:when>
	<c:otherwise>
		<aui:nav-bar>
			<aui:nav>
				<aui:nav-item id="printBtn" iconCssClass="icon-print" label="print"
					selected='<%="print".equals(toolbarItem)%>' />
			</aui:nav>
		</aui:nav-bar>
		<%
			List<TestataDocumento> list = TestataDocumentoLocalServiceUtil.findDocumentsByDatePartnerType(from, partnerSelected, DailyReport.DDM);
		%>
		<liferay-portlet:renderURL varImpl="iteratorURL1">
			<portlet:param name="shipDate" value="<%=sdf.format(from)%>" />
			<portlet:param name="partnerSelected"
				value="<%=String.valueOf(partnerSelected)%>" />
			<portlet:param name="mvcPath" value="/html/dailyreport/view.jsp" />
		</liferay-portlet:renderURL>
		<liferay-ui:search-container delta="75"
			emptyResultsMessage="Nessun documento trovato"
			iteratorURL="<%=iteratorURL1%>">
			<liferay-ui:search-container-results>
				<%
					results = ListUtil.subList(list,
						                                    searchContainer.getStart(),
						                                    searchContainer.getEnd());
						                            total = list.size();
						                            pageContext.setAttribute("results", results);
						                            pageContext.setAttribute("total", total);
				%>
			</liferay-ui:search-container-results>
			<liferay-ui:search-container-row
				className="it.bysoftware.ct.model.TestataDocumento"
				modelVar="document">
				<%
					String docDate = sdf.format(document.getDataRegistrazione());
					        	Ordine order = OrdineLocalServiceUtil.getOrdine(document.getLibLng1());
						        String date = sdf.format(order.getDataInserimento());
						        AnagraficheClientiFornitori c = AnagraficheClientiFornitoriLocalServiceUtil.
						                fetchAnagraficheClientiFornitori(String.valueOf(order.getIdCliente()));
				%>
				<liferay-ui:search-container-column-text name="DDT"
					value="<%=document.getProtocollo() + "/" + document.getCodiceCentro()%>" />
				<liferay-ui:search-container-column-text name="confirm-number"
					value="<%=order.getNumero() + "/" + order.getCentro()%>" />
				<liferay-ui:search-container-column-text value="<%=date%>"
					name="date" />
				<%-- 			        <liferay-ui:search-container-column-text value="<%= deliveryDate %>"name="ship-date" /> --%>
				<liferay-ui:search-container-column-text
					value="<%=c != null ? c.getRagioneSociale1() : ""%>"
					name="customer" />
				<%
					AnagraficheClientiFornitori partner = AnagraficheClientiFornitoriLocalServiceUtil.
						                    fetchAnagraficheClientiFornitori(partnerSelected);
						            if (Validator.isNotNull(partner)) {
						                ClientiFornitoriDatiAgg data = ClientiFornitoriDatiAggLocalServiceUtil.
						                        getClientiFornitoriDatiAgg(new ClientiFornitoriDatiAggPK(
						                                partnerSelected, true));
				%>
				<liferay-ui:search-container-column-text name="supplier"
					value="<%=partner.getRagioneSociale1()%>" />
				<c:if test="<%=!data.getAssociato()%>">
					<liferay-ui:search-container-column-text name="">
						<liferay-ui:icon image="organization_icon" message="company" />
					</liferay-ui:search-container-column-text>
				</c:if>
				<c:if test="<%=data.getAssociato()%>">
					<liferay-ui:search-container-column-text name="">
						<liferay-ui:icon image="user_icon" message="partner" />
					</liferay-ui:search-container-column-text>
				</c:if>
				<%
					}
				%>
			</liferay-ui:search-container-row>
			<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
				paginate="true" />
		</liferay-ui:search-container>
	</c:otherwise>
</c:choose>

<aui:script>
	var printDailyReportURL = '<%= printDailyReportURL %>';
	
	AUI().use('aui-base', 'datatype-number', function(A) {
		if (A.one("#<portlet:namespace/>printBtn")) {
			A.one("#<portlet:namespace/>printBtn").on('click', function() {
	            var partnerSelected = A.one('#<portlet:namespace/>partnerSelected').get('value');
	            var msg = 'Prima di procedere alla stampa del report selezionare almeno un socio.';
	            if (partnerSelected !== '') {
	            	printDailyReport(partnerSelected);
	            } else {
	                alert(msg);
	            }
	        });
		}
    });
	
	function printDailyReport() {
	    AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
	    	var date = A.one('#<portlet:namespace />shipDate').get('value');
        	date = date.split('/');
        	var shipDate = new Date(date[2] + '-' + date[1] + '-' + date[0]).getTime();
	        A.io.request(printDailyReportURL, {
	            method: 'POST',
	            data: {
	            	<portlet:namespace />date: shipDate,
	            	<portlet:namespace />partnerCode: '<%=partnerSelected%>',
	            },
	            on: {
	                start: function() {
	                    modal = new A.Modal({
	                        bodyContent : '<div class="loading-animation"/>',
	                        centered : true,
	                        headerContent : '<h3>Loading...</h3>',
	                        modal : true,
	                        render : '#modal',
	                        close: false,
	                        zIndex: 99999,
	                        width : 450
	                    }).render();
	                },
	                success: function () {
	                    var data = JSON.parse(this.get('responseData'));
	                    console.log(data);
	                    modal.hide();
	                    if (data.code === 0) {
	                        var win = window.open(data.message.toLocaleString());
	                        if (win) {
	                            window.focus();
	                        } else {
	                            var timer = window.setTimeout(function(){
	                                if (win) {
	                                    win.focus();
	                                }
	                            }, 200 );
	                        }
	                    } else {
	                        alert('Si � verificato un errore durante la stampa del'
	                        		+ ' report.\n\n' + data.message);
	                    }
	                    
	                },
	                error: function () {
	                    modal.hide();
	                }
	            }
	        });
	    });
	}
</aui:script>
	




