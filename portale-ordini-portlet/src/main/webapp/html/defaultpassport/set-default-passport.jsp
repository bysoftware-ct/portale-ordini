<%@page import="it.bysoftware.ct.model.Articoli"%>
<%@page import="it.bysoftware.ct.model.CategorieMerceologiche"%>
<%@page import="it.bysoftware.ct.model.Piante"%>
<%@page import="it.bysoftware.ct.service.ArticoliLocalServiceUtil"%>
<%@page
	import="it.bysoftware.ct.service.CategorieMerceologicheLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.PianteLocalServiceUtil"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="../init.jsp"%>

<%
	long piantaId = ParamUtil.getLong(renderRequest, "piantaId", 0);
	String backUrl = ParamUtil.getString(renderRequest, "backUrl");
	String prevUrl = ParamUtil.getString(renderRequest, "prevUrl");
	String partnerCode = ParamUtil.getString(renderRequest, "partnerCode");
	Piante pianta = PianteLocalServiceUtil.getPiante(piantaId);
	Articoli item = ArticoliLocalServiceUtil.fetchArticoli(pianta
			.getCodice());
	String shape = "";
	if (item != null) {
		CategorieMerceologiche c = CategorieMerceologicheLocalServiceUtil
				.fetchCategorieMerceologiche(item
						.getCategoriaMerceologica());
		if (c != null) {
			shape = c.getDescrizione();
		}
	}
%>

<liferay-ui:header title="set-default-passport" backURL="<%=backUrl%>" />

<liferay-portlet:actionURL name="saveDefaultPassport"
	var="saveDefaultPassportURL">
	<portlet:param name="prevUrl" value="<%=prevUrl%>" />
	<portlet:param name="backUrl" value="<%=backUrl%>" />
	<portlet:param name="partnerCode" value="<%=partnerCode%>" />
	<%-- 	<portlet:param name="backUrl" value="<%=prevUrl%>" /> --%>
	<portlet:param name="piantaId"
		value="<%=String.valueOf(pianta.getId())%>" />
</liferay-portlet:actionURL>

<aui:input name="code" type="text" cssClass="input-small"
	inlineField="true" label="code" value="<%=pianta.getCodice()%>"
	disabled="true" />
<aui:input name="nome" type="text" inlineField="true" label="name"
	value="<%=pianta.getNome()%>" disabled="true" />
<aui:input name="forma" type="text" inlineField="true" label="shape"
	value="<%=shape%>" disabled="true" />
<aui:input name="vaso" type="text" inlineField="true"
	cssClass="input-mini" label="vase" value="<%=pianta.getVaso()%>"
	disabled="true" />
<aui:input name="altezza" type="text" inlineField="true"
	cssClass="input-mini" label="height" value="<%=pianta.getAltezza()%>"
	disabled="true" />
<aui:form id="aForm" name="aForm" action="<%=saveDefaultPassportURL%>"
	method="post">
	<aui:input name="default-passport" type="text" label="default-passport"
		value="<%=pianta.getPassaportoDefault()%>">
		<aui:validator name="required" />
	</aui:input>
	<aui:button-row>
		<aui:button type="submit" value="save" />
		<aui:button type="cancel" onclick="this.form.reset()" />
	</aui:button-row>
</aui:form>