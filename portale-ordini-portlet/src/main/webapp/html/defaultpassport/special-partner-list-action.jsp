<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@include file="../init.jsp"%>
<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    AnagraficheClientiFornitori partner = (AnagraficheClientiFornitori) row
            .getObject();
    String backUrl = (String) row.getParameter("backUrl");
%>
<liferay-ui:icon-menu>
	<liferay-portlet:renderURL varImpl="viewOrderURL">
		<portlet:param name="mvcPath" value="/html/defaultpassport/default-passport-list.jsp" />
		<portlet:param name="backUrl" value="<%=backUrl%>" />
		<portlet:param name="partnerCode"
			value="<%=partner.getCodiceAnagrafica()%>" />
	</liferay-portlet:renderURL>
	<liferay-ui:icon image="search" url="${viewOrderURL}" message="view" />
</liferay-ui:icon-menu>
