<%@page import="com.liferay.portal.kernel.dao.search.DisplayTerms"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.kernel.util.OrderByComparator"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.liferay.portal.service.ClassNameLocalServiceUtil"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portlet.expando.model.ExpandoValue"%>
<%@page
	import="com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="it.bysoftware.ct.model.Articoli"%>
<%@page import="it.bysoftware.ct.model.CategorieMerceologiche"%>
<%@page import="it.bysoftware.ct.model.Piante"%>
<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.ArticoliLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.CategorieMerceologicheLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.PianteLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ include file="../init.jsp"%>

<%
	String toolbarItem = ParamUtil.getString(renderRequest,
			"toolbarItem", "view-all");
	String sortByCol = ParamUtil.getString(request, "orderByCol", "name"); 
	String sortByType = ParamUtil.getString(request, "orderByType", "asc"); 
	String itemCode = ParamUtil.getString(request, "itemCode");
	String itemDesc = ParamUtil.getString(request, "itemDesc");
	String backUrl = ParamUtil.getString(renderRequest, "backUrl");
	String currentURL = PortalUtil.getCurrentURL(request);
	boolean isCompany = UserLocalServiceUtil.hasRoleUser(
			themeDisplay.getCompanyId(), Constants.COMPANY,
			themeDisplay.getUserId(), false);
	String partnerCode = ParamUtil.getString(renderRequest,
			"partnerCode", null);
	
	if (!isCompany) {
        ExpandoValue value = ExpandoValueLocalServiceUtil.getValue(
                user.getCompanyId(),
                ClassNameLocalServiceUtil.getClassNameId(User.class),
                "CUSTOM_FIELDS", Constants.REGISTRY_CODE,
                user.getUserId());
        partnerCode = value.getString();
    }
	
	String title = LanguageUtil.get(portletConfig, user.getLocale(), "default-passport-list-x");
    String companyName = "";
    if (partnerCode != null) {
//         list = ArticoloSocioLocalServiceUtil
//                 .findAllPartnerItems(partnerCode);
        AnagraficheClientiFornitori partner = AnagraficheClientiFornitoriLocalServiceUtil.
                getAnagraficheClientiFornitori(partnerCode);
        companyName = partner.getRagioneSociale1() + " " + partner.getRagioneSociale2();
        title = LanguageUtil.format(user.getLocale(), title, new Object[] { "- " + companyName });
    }
%>

<liferay-ui:header backURL="<%=backUrl%>" title="<%=title %>" />

<liferay-portlet:renderURL varImpl="searchURL">
    <portlet:param name="backUrl" value="<%=backUrl%>" />
    <portlet:param name="partnerCode" value="<%=partnerCode%>" />
    <portlet:param name="mvcPath" value="/html/defaultpassport/default-passport-list.jsp" />
</liferay-portlet:renderURL>

<aui:form action="<%=searchURL %>" method="post" name="orderForm">
    <liferay-portlet:renderURLParams varImpl="searchURL" />
	<liferay-portlet:renderURL varImpl="iteratorURL">
	    <portlet:param name="backUrl" value="<%=backUrl%>" />
		<portlet:param name="itemDesc" value="<%=itemDesc%>" />
		<portlet:param name="itemCode" value="<%=itemCode%>" />
		<portlet:param name="partnerCode" value="<%=partnerCode%>" />
		<portlet:param name="itemCode" value="<%=itemCode%>" />
		<portlet:param name="mvcPath"
			value="/html/defaultpassport/default-passport-list.jsp" />
	</liferay-portlet:renderURL>
	<liferay-ui:search-container delta="20" displayTerms="<%= new DisplayTerms(renderRequest) %>" 
	    emptyResultsMessage="no-default-passport" iteratorURL="<%=iteratorURL%>" orderByCol="<%= sortByCol %>" orderByType="<%= sortByType %>">
	    <aui:nav-bar>
	        <aui:nav-bar-search cssClass="pull-right" >
	            <liferay-ui:search-form page="/html/defaultpassport/search.jsp" servletContext="<%= application %>"/>
	        </aui:nav-bar-search>
	    </aui:nav-bar>
		<liferay-ui:search-container-results>
			<%
				OrderByComparator orderByComparator =        
            		Utils.getPianteByComparator(sortByCol, sortByType);
			    DisplayTerms displayTerms = searchContainer.getDisplayTerms();
			    List<Piante> list = new ArrayList<Piante>();
	            if (displayTerms.isAdvancedSearch()) {
	                list = PianteLocalServiceUtil.findPlants(true, true,
	                		partnerCode, itemDesc, itemCode, false,
	                		orderByComparator);
	            } else {
	                String searchkeywords = displayTerms.getKeywords();
	                String numbesearchkeywords = String.valueOf(0);
	                if (Validator.isNumber(searchkeywords)) {
	                    numbesearchkeywords = searchkeywords;
	                }
	                list = PianteLocalServiceUtil.findPlants(true, true,
	                		partnerCode, searchkeywords, "", false,
	                		orderByComparator);  
	            }
			    results = ListUtil.subList(list,
			                    searchContainer.getStart(),
			                    searchContainer.getEnd());
			            total = list.size();
	
			            pageContext.setAttribute("results", results);
			            pageContext.setAttribute("total", total);
			%>
		</liferay-ui:search-container-results>
		<liferay-ui:search-container-row
			className="it.bysoftware.ct.model.Piante"
			modelVar="plant">
			<liferay-ui:search-container-column-text property="codice"
				name="code" orderable="<%= true %>"/>
			<%
			    Articoli item = ArticoliLocalServiceUtil
			                    .fetchArticoli(plant.getCodice());
				String shape = "";
			    if (item != null) {
			        CategorieMerceologiche c =
			        		CategorieMerceologicheLocalServiceUtil.
			        			fetchCategorieMerceologiche(
			        					item.getCategoriaMerceologica());
                    if (c != null) {
                        shape = c.getDescrizione();
                    }
			    }
			%>
			<liferay-ui:search-container-column-text name="name"
				value="<%=item != null ? item.getDescrizione() : ""%>" orderable="<%= true %>"/>
			<liferay-ui:search-container-column-text name="shape" value="<%=shape %>" />
            <liferay-ui:search-container-column-text property="vaso" name="vase" />
            <liferay-ui:search-container-column-text property="altezzaPianta" name="H" />
            <liferay-ui:search-container-column-text property="passaportoDefault" name="Passaporto default" />
<%-- 			<c:if test="<%=isCompany%>"> --%>
				<liferay-ui:search-container-row-parameter name="backUrl"
					value="<%=iteratorURL.toString()%>" />
				<liferay-ui:search-container-row-parameter name="partnerCode"
					value="<%=partnerCode%>" />
				<liferay-ui:search-container-column-jsp
					path="/html/defaultpassport/default-passport-list-action.jsp" />
<%-- 			</c:if> --%>
		</liferay-ui:search-container-row>
		<liferay-ui:search-iterator searchContainer="<%=searchContainer%>" />
	</liferay-ui:search-container>
</aui:form>