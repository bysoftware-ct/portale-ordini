<%@page import="com.liferay.portal.kernel.dao.search.DisplayTerms"%>
<%@page import="com.liferay.portal.kernel.dao.search.SearchContainer"%>
<%@ include file="../init.jsp"%>
<%
    SearchContainer searchContainer = (SearchContainer) request
            .getAttribute("liferay-ui:search:searchContainer");
    DisplayTerms displayTerms = searchContainer.getDisplayTerms();
    String itemCode = ParamUtil.getString(request, "itemCode");
    String itemDesc = ParamUtil.getString(request, "itemDesc");
%>

<liferay-ui:search-toggle buttonLabel="search"
    displayTerms="<%=displayTerms%>" id="toggle_id_search">
    <aui:input label="code" name="itemCode" value="<%=itemCode %>" />
    <aui:input label="name" name="itemDesc" value="<%=itemDesc %>" />
</liferay-ui:search-toggle>