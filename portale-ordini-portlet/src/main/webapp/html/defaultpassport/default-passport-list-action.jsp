<%@page import="it.bysoftware.ct.model.Piante"%>
<%@page import="it.bysoftware.ct.service.OrdineLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="../init.jsp"%>

<%
	ResultRow row = (ResultRow) request
			.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Piante p = (Piante) row.getObject();
	String backUrl = (String) row.getParameter("backUrl");
	String itemDesc = (String) row.getParameter("itemDesc");
	String itemCode = (String) row.getParameter("itemCode");
	String partnerCode = (String) row.getParameter("partnerCode");
%>

<liferay-ui:icon-menu>
	<liferay-portlet:renderURL varImpl="setDefaultPassportURL">
		<portlet:param name="mvcPath"
			value="/html/defaultpassport/set-default-passport.jsp" />
		<portlet:param name="backUrl" value="<%=backUrl%>" />
		<portlet:param name="piantaId" value="<%=String.valueOf(p.getId())%>" />
		<portlet:param name="partnerCode" value="<%=partnerCode%>" />
	</liferay-portlet:renderURL>
	<c:choose>
		<c:when
			test="<%=p.getPassaportoDefault() != null
								&& !p.getPassaportoDefault().isEmpty()%>">
			<liferay-ui:icon image="edit" url="${setDefaultPassportURL}"
				message="set-default-passport" />
			<liferay-portlet:actionURL name="deleteDefaultPassport"
				var="deleteDefaultPassportURL">
				<portlet:param name="piantaId"
					value="<%=String.valueOf(p.getId())%>" />
				<portlet:param name="itemCode"
					value="<%=itemCode%>" />
				<portlet:param name="itemDesc"
					value="<%=itemDesc%>" />
				<portlet:param name="partnerCode" value="<%=partnerCode%>" />
			</liferay-portlet:actionURL>
			<liferay-ui:icon-delete message="delete"
				url="${deleteDefaultPassportURL}"
				confirmation="confirm-default-passport-delete" />
		</c:when>
		<c:otherwise>
			<liferay-ui:icon image="add" url="${setDefaultPassportURL}"
				message="edit-default-passport" />
		</c:otherwise>
	</c:choose>
</liferay-ui:icon-menu>