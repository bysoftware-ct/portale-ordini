<%@page import="java.util.ArrayList"%>
<%@page import="com.liferay.util.portlet.PortletProps"%>
<%@page import="it.bysoftware.ct.service.DettagliOperazioniContabiliLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.DettagliOperazioniContabili"%>
<%@page import="it.bysoftware.ct.service.SottocontiLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Sottoconti"%>
<%@page import="java.util.Calendar"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="it.bysoftware.ct.service.persistence.PagamentoPK"%>
<%@page import="it.bysoftware.ct.service.PagamentoLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Pagamento"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="it.bysoftware.ct.service.persistence.ScadenzePartitePK"%>
<%@page import="it.bysoftware.ct.service.ScadenzePartiteLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.ScadenzePartite"%>
<%@page import="com.liferay.portal.kernel.json.JSONObject"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.json.JSONArray"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page
	import="it.bysoftware.ct.service.SchedaPagamentoLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.SchedaPagamento"%>
<%@page import="java.util.List"%>
<%@include file="../init.jsp"%>

<%
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    DecimalFormat nf = new DecimalFormat("#0.00");
    String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
    String backUrl = ParamUtil.getString(renderRequest, "backUrl");
    String selectedBoards = ParamUtil.getString(renderRequest, "selectedBoards", "[]");
	String partnerCode = ParamUtil.getString(renderRequest, "partnerCode");
	int year = ParamUtil.getInteger(renderRequest, "year");
	int paymentId = ParamUtil.getInteger(renderRequest, "paymentId");
	int paymentYear = ParamUtil.getInteger(renderRequest, "paymentYear");
	int state = ParamUtil.getInteger(renderRequest, "state", 0);
	AnagraficheClientiFornitori partner = AnagraficheClientiFornitoriLocalServiceUtil.
	    getAnagraficheClientiFornitori(partnerCode);
	List<SchedaPagamento> list = SchedaPagamentoLocalServiceUtil.
	    findByCustomerYearIdPaymentAndState(partnerCode, paymentYear, paymentId, state);
	Pagamento prevPayment = null;
	if (Calendar.getInstance().get(Calendar.MONTH) + 1 == 1) {
	    prevPayment = PagamentoLocalServiceUtil.fetchPagamento(new PagamentoPK(12, paymentYear -1, partnerCode));
	} else {
	    prevPayment = PagamentoLocalServiceUtil.fetchPagamento(new PagamentoPK(paymentId -1, paymentYear, partnerCode));
	}
	double prevBalance = 0;
	if (prevPayment != null) {
	    prevBalance = prevPayment.getSaldo();
	}
	Pagamento payment = PagamentoLocalServiceUtil.getPagamento(new PagamentoPK(paymentId, paymentYear, partnerCode));
	double amount = payment.getCredito() + prevBalance;
	
	JSONArray payments = JSONFactoryUtil.createJSONArray();
	double totalAmount = 0;
	double totalRewarded = 0;
    double totalUnpayed = 0;
    double totalPayed = 0;
    int id = 0;
	for (SchedaPagamento s : list) {
	    if (s.getTipoSoggetto()) {
		    JSONObject paymentBoard = JSONFactoryUtil.createJSONObject();
		    paymentBoard.put("payed", "false");
		    paymentBoard.put("paymentBoardPK", JSONFactoryUtil.createJSONObject(
	                    JSONFactoryUtil.looseSerialize(s.getPrimaryKeyObj())));
		    ScadenzePartite invoice = ScadenzePartiteLocalServiceUtil.
	                    getScadenzePartite(new ScadenzePartitePK(s.getTipoSoggetto(),
	                            s.getCodiceSoggetto(), s.getEsercizioRegistrazione(),
	                            s.getNumeroPartita(), s.getNumeroScadenza()));
		    paymentBoard.put("invoice", "Fattura: " + invoice.getNumeroDocumento() + "/" + invoice.getCodiceCentro() + " del: " + sdf.format(invoice.getDataDocumento()));
		    double amount2Pay = invoice.getImportoTotale() - invoice.getImportoPagato();
	        paymentBoard.put("amount", amount2Pay);//invoice.getImportoAperto());
	        totalAmount += amount2Pay;//invoice.getImportoAperto();
		    if (s.getEsercizioRegistrazioneComp() > 0) {
		        JSONObject paymentComp = JSONFactoryUtil.createJSONObject();
		        ScadenzePartite invoiceComp = ScadenzePartiteLocalServiceUtil.
		                    getScadenzePartite(new ScadenzePartitePK(s.getTipoSoggettoComp(),
		                            s.getCodiceSoggettoComp(), s.getEsercizioRegistrazioneComp(),
		                            s.getNumeroPartitaComp(), s.getNumeroScadenzaComp()));
		        paymentBoard.put("paymentBoardCompPK", JSONFactoryUtil.createJSONObject(
		                    JSONFactoryUtil.looseSerialize(invoiceComp.getPrimaryKeyObj())));
		        double rewarded = invoiceComp.getImportoTotale() - invoiceComp.getImportoPagato();
		        if (rewarded > amount2Pay) {
		            paymentBoard.put("rewarded", amount2Pay);//invoiceComp.getImportoAperto());
                    paymentBoard.put("note", "Scalata fattura a voi intestata: N. " + invoiceComp.getNumeroDocumento() + "/" + invoiceComp.getCodiceCentro() + " di &euro; " + nf.format(rewarded) + " residuo &euro; " + nf.format(rewarded - amount2Pay));
                    totalRewarded += amount2Pay;//invoice.getImportoAperto();
		        } else {
		            paymentBoard.put("rewarded", rewarded);//invoiceComp.getImportoAperto());
		            paymentBoard.put("note", "Scalata fattura a voi intestata: N. " + invoiceComp.getNumeroDocumento() + "/" + invoiceComp.getCodiceCentro() + " di &euro; " + nf.format(rewarded));
		            totalRewarded += rewarded;//invoice.getImportoAperto();
		        }
		        
		        paymentBoard.put("diff", s.getDifferenza());//nf.format(s.getDifferenza()));
		        totalUnpayed += s.getDifferenza();
		    } else {
		        paymentBoard.put("paymentBoardCompPK", JSONFactoryUtil.createJSONObject());
		        paymentBoard.put("rewarded", 0);
                paymentBoard.put("diff", amount2Pay);//nf.format(invoice.getImportoTotale() - invoice.getImportoPagato()));//invoice.getImportoAperto()));
                totalUnpayed += amount2Pay;//invoice.getImportoAperto();
                paymentBoard.put("note", "");
		    }
		    paymentBoard.put("payedAmount", 0);
		    paymentBoard.put("id", id++);
            payments.put(paymentBoard);
	    }
	}
	String defAccountingOp = PortletProps.get("def-accounting-operation");
	String[] bankSubs = PortletProps.get("bank-subaccount-code").split(",");
	String[] cashSubs = PortletProps.get("cash-subaccount-code").split(",");
	String defBankSub = bankSubs[0];
	String defCashSub = cashSubs[0];
	List<Sottoconti> subAccounts = new ArrayList<Sottoconti>();
	for (String s : bankSubs) {
	    Sottoconti sub = SottocontiLocalServiceUtil.fetchSottoconti(s);
        if(sub != null) {
            subAccounts.add(sub);
        }
	}
	
	for (String s : cashSubs) {
        Sottoconti sub = SottocontiLocalServiceUtil.fetchSottoconti(s);
        if(sub != null) {
            subAccounts.add(sub);
        }
    }
	
// 	List<DettagliOperazioniContabili> accountingOps =
// 	        DettagliOperazioniContabiliLocalServiceUtil.findByAccountingOperation(defAccountingOp);
	
// 	for (DettagliOperazioniContabili accountingOp : accountingOps) {
// 	    if (!accountingOp.getSottoconto().isEmpty()) {
// 	        subStr[0] = accountingOp.getSottoconto();
// 	        subStr[1] = "";
	        
// 	    }
// 	}
// 	 = SottocontiLocalServiceUtil.getSottocontis(0,
// 	        SottocontiLocalServiceUtil.getSottocontisCount());
	JSONArray JSONSubs = JSONFactoryUtil.createJSONArray(JSONFactoryUtil.looseSerializeDeep(subAccounts));
%>

<liferay-ui:header backURL="<%= backUrl%>"
	title="<%=partner.getRagioneSociale1() + " " + partner.getRagioneSociale2()%>" />
<liferay-portlet:actionURL name="payPartner" var="payPartner" >
    <liferay-portlet:param name="backUrl" value="<%=backUrl %>"/>
    <liferay-portlet:param name="partnerCode" value="<%=partnerCode %>"/>
    <liferay-portlet:param name="year" value="<%=String.valueOf(year) %>"/>
    <liferay-portlet:param name="paymentId" value="<%=String.valueOf(paymentId) %>"/>
    <liferay-portlet:param name="state" value="<%=String.valueOf(state) %>"/>
</liferay-portlet:actionURL>
<aui:nav-bar>
    <aui:nav>
       <aui:nav-item id="saveBtn" cssClass="green" iconCssClass=" icon-euro green" label="pay" 
            selected='<%="save".equals(toolbarItem)%>' />
    </aui:nav>
</aui:nav-bar>
<span id="group-error-block" ></span>
<aui:form method="post" name="fm" >  
	<aui:layout cssClass="wrapper">
	    <aui:column columnWidth="20" first="true">
	        <aui:input id="selectedBoards" name="selectedBoards" type="hidden" value="<%=selectedBoards %>"/>
	        <aui:input id="give-credit" name="give-credit" cssClass="input-small" prefix="&euro;"
	            value="<%=Utils.roundDouble(payment.getCredito(), 2) %>" disabled="true" label="give-credit" />
	    </aui:column>
        <aui:column columnWidth="20">
            <aui:input id="prevBalance" name="prevBalance" cssClass="input-small" prefix="&euro;"
                value="<%=Utils.roundDouble(prevBalance, 2) %>" disabled="true" label="prev-balance" />
        </aui:column>
        <aui:column columnWidth="20" last="">
            <aui:input id="amount" name="amount" cssClass="input-small" prefix="&euro;"
                value="<%=Utils.roundDouble(amount, 2) %>" disabled="true" label="amount" />
        </aui:column>
	</aui:layout>
	
	<div class="content" id="myDataTable"></div>
	
	<aui:layout cssClass="wrapper">
	    <aui:column columnWidth="20" first="true">
	        <aui:input id="totalAmount" name="totalAmount" cssClass="input-small" 
	            prefix="&euro;" disabled="true" value="<%=totalAmount %>" label="total-amount"/>
	    </aui:column>
	    <aui:column columnWidth="20" >
	        <aui:input id="totalRewarded" name="totalRewarded" cssClass="input-small"
	            prefix="&euro;" disabled="true" value="<%=totalRewarded %>" label="total-rewarded"/>
	    </aui:column>
	    <aui:column columnWidth="20" >
	        <aui:input id="totalUnpayed" name="totalUnpayed" cssClass="input-small" 
	            prefix="&euro;" disabled="true" value="<%=totalUnpayed %>" label="total-unpayed"/>
	    </aui:column>
	    <aui:column columnWidth="20" >
	        <aui:input id="totalPayed" name="totalPayed" cssClass="input-small" type="hidden" 
                value="0.00" />
	        <aui:input id="total-payed" name="totalPayed" cssClass="input-small" 
	            prefix="&euro;" disabled="true" value="0.00" label="total-payed"/>
	    </aui:column>
	    <aui:column columnWidth="20" last="true">
	        <aui:input id="newBalance" name="newBalance" cssClass="input-small" type="hidden"
                value="<%=totalUnpayed %>" label="new-balance"/>
	        <aui:input id="new-balance" name="newBalance" cssClass="input-small" 
	            prefix="&euro;" disabled="true" value="<%=totalUnpayed %>" label="new-balance"/>
	    </aui:column>
	</aui:layout>
</aui:form>
<aui:script>
var remoteData = <%= payments %>;
var JSONSubs = <%=JSONSubs %>;
var defBankSub = '<%=defBankSub %>';
var defCashSub = '<%=defCashSub %>';

AUI().use('aui-base', 'aui-datatable', 'aui-datatype', 'datatable-sort', 'datatable-mutable', function(A) {
// 	 var subs = [];
// 	 JSONSubs.forEach(function(item, i) {
// 		 subs.push( item.descrizione + ' - ' + item.codice);
//      });
    var subs = {};
    JSONSubs.forEach(function(item, i) {
//          subs.push( item.descrizione + ' - ' + item.codice);
            subs[item.codice] = item.descrizione
     });
	 var nestedCols = [{
		 emptyCellValue : '<input type="checkbox" name="select" class="select" />',
         label: '<input id="select-all" type="checkbox" name="select-all" class="select-all" title="Paga tutte"/>',
         allowHTML : true
     }, {
    	 className: 'hidden',
         key: 'payed',
         label: '#'
     }, {
         key: 'invoice',
         label: 'Fattura'
     }, {
         key: 'amount',
         label: 'Importo',
         formatter: currencyFormatter
     }, { 
         key: 'rewarded',
         label: 'Imp. compensato', 
         formatter: currencyFormatter
     }, {
         key: 'diff',
         label: 'Da pagare', 
         formatter: redFormatter
     }, {
         key: 'payedAmount',
         label: 'Pagato', 
         formatter: greenFormatter
     }, {
    	 editor: new A.DropDownCellEditor({
    		 options: subs
   		 }),
   		 key: 'subs',
   		 label: 'Sottoconto'
     }, {
    	 className: 'hidden',
         key: 'subAccount'
     }, {
    	 allowHTML : true,
         editor: new A.TextAreaCellEditor(),
         key: 'note',
         label: 'Nota'
     }];
	 
	 var dataTable = new A.DataTable({
         columns: nestedCols,
         data: remoteData,
         editEvent: 'click',
         plugins: [{
             cfg: {
                 highlightRange: false
             },
             fn: A.Plugin.DataTableHighlight
         }]
     }).render('#myDataTable');
	 dataTable.get('boundingBox').unselectable();
	 dataTable.delegate('click', function(e, table) {
         rowAction(e, table);
     }, 'tr', this, dataTable);
	 dataTable.after('*:subsChange', function (e) {
         updateEntry(e, dataTable)
     });
	 dataTable.after('*:noteChange', function (e) {
         updateEntry(e, dataTable)
     });
});

AUI().use('liferay-util-list-fields', function(A) {
    A.one('#<portlet:namespace/>saveBtn').on('click', function(event) {
    	var selectedBoards = JSON.parse(A.one('#<portlet:namespace/>selectedBoards').get('value'));
    	if (selectedBoards.length === 0 ) {
    		alert('Nessun pagamento selezionato.');
    	} else {
   			if (confirm('Procedere al salvataggio dei pagamenti effettuati?') === true) {
   	            submitForm(document.<portlet:namespace />fm, '<%= payPartner.toString() %>');
   	        }
    	}
    });
});

function greenFormatter(amount) {
    amount.className += 'green';
    return currencyFormatter(amount.value);
}

function redFormatter(amount) {
    amount.className += 'red';
    return currencyFormatter(amount.value);
}

function currencyFormatter(amount) {
    if (isNaN(amount)) {
    	if (isNaN(amount.value)) {
    		console.log(amount.value);
    		return 0;	
    	} else {
    		amount = amount.value;
    	}
    }
    return parseFloat(Math.round(amount * 100) / 100).toFixed(2);
}

function rowAction(e, table) {
    var nodeName = e.target._node.name;
    switch(nodeName) { 
        case 'select':
        	AUI().use('aui-base', function(A) {
	        	var record = table.getRecord(e.currentTarget);
	        	if (record.getAttrs().payed === 'false' || record.getAttrs().payed === '') {
	        		record.setAttrs({
	        			payed: 'true',
	                    payedAmount: record.getAttrs().diff,
	                    diff: 0, 
	                    subs: record.getAttrs().diff > 0 ? defBankSub : ''
	                }, {sync: true});
	        	} else if (record.getAttrs().payed === 'true') {
	        		record.setAttrs({
	        			payed: 'false',
	        			diff: record.getAttrs().payedAmount,
	        			payedAmount: 0,
	        			subs: '',
	        			note: ''
	                }, {sync: true});
	        	}
	        	if (record.getAttrs().payed === 'false') {
	        		A.one('#select-all').set('checked', false);
	        	} else {
	        		var checked = true;
	        		table.data._items.forEach(function(item, i) {
	        			if (item.getAttrs().payed === 'false') {
	        				checked = false;
	        			}
	        		});
	        		A.one('#select-all').set('checked', checked);
	        	}
        	});
            break;
        case 'select-all':
        	AUI().use('aui-base', function(A) {
	        	table.data._items.forEach(function(item, i) {
	        		console.log(item.getAttrs());
	        		if (item.getAttrs().payed === 'false') {
	        			item.setAttrs({
	        				payed: 'true',
	                        payedAmount: item.getAttrs().diff,
	                        diff: 0, 
	                        subs: item.getAttrs().diff ? defBankSub : ''
	                    }, {sync: true});
	        		} else if (!A.one('#select-all').get('checked')) {
	        			item.setAttrs({
	        				payed: 'false',
	        				diff: item.getAttrs().payedAmount,
	        				payedAmount: 0,
	        				subs: '',
	        				note: ''
	                    }, {sync: true});
	        		}
	        	});
                var x= A.one('#myDataTable');
                x.all('.select')._nodes.forEach(function(item, i) {
                	item.checked = A.one('#select-all').get('checked');
                });
            });
            break;
        default:
            break;
    }
    var totalAmount = 0;
    var totalRewarded = 0;
    var totalUnpayed = 0;
    var totalPayed = 0;
    var payed = [];
    table.data._items.forEach(function(item, i) {
    	totalAmount += item.getAttrs().amount;
    	totalRewarded += item.getAttrs().rewarded;
    	totalUnpayed += parseFloat(item.getAttrs().diff);
    	totalPayed += parseFloat(item.getAttrs().payedAmount);
        payed.push(item);
    });
    AUI().use('aui-base', function(A) {
    	A.one('#<portlet:namespace/>totalAmount').set('value', currencyFormatter(totalAmount));
        A.one('#<portlet:namespace/>totalRewarded').set('value', currencyFormatter(totalRewarded));
        A.one('#<portlet:namespace/>totalUnpayed').set('value', currencyFormatter(totalUnpayed));
        A.one('#<portlet:namespace/>totalPayed').set('value', currencyFormatter(totalPayed));
        A.one('#<portlet:namespace/>newBalance').set('value', currencyFormatter(
        		parseFloat(A.one('#<portlet:namespace/>amount').get('value')) - totalPayed));
        A.one('#<portlet:namespace/>total-payed').set('value', A.one('#<portlet:namespace/>totalPayed').get('value'));
        A.one('#<portlet:namespace/>new-balance').set('value', A.one('#<portlet:namespace/>newBalance').get('value'));
        A.one('#<portlet:namespace/>selectedBoards').set('value', JSON.stringify(payed));

    })
}

function updateEntry(e, table) {
	AUI().use('aui-base', function(A) {
		var editedRecord = e.target.getAttrs();
        var selectedBoards = JSON.parse(A.one('#<portlet:namespace/>selectedBoards').get('value'));
        selectedBoards.forEach(function(item, i) {
        	if (item.id === editedRecord.id) {
        		switch(e.attrName) { 
	                case 'note':
	                	item.note = e.newVal;
	                    break;
	                case 'subs':
	                	item.subs = e.newVal;
	                    break;
	                default:
	                	break;
                }
            }
        });
		A.one('#<portlet:namespace/>selectedBoards').set('value', JSON.stringify(selectedBoards));
	});
}

</aui:script>
	

