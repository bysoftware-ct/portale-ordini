<%@page import="it.bysoftware.ct.utils.Constants"%>
<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../../init.jsp"%>

<%
    String emailList = GetterUtil.getString(portletPreferences.getValue(
            Constants.CREDIT_BOARD_CC_LIST, ""));
%>

<liferay-portlet:actionURL portletConfiguration="true"
    var="configurationURL" />

<aui:form action="<%=configurationURL%>" method="post" name="fm">
    <aui:input name="<%=Constants.CMD%>" type="text"
        value="<%=Constants.UPDATE%>" />

    <aui:input name="preferences--emailList--" type="textarea"
        label="recipients-list" value="<%=emailList%>">
    </aui:input>
        
    <aui:button-row>
        <aui:button type="submit" />
    </aui:button-row>
</aui:form>