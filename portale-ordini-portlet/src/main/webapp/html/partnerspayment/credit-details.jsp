<%@page import="it.bysoftware.ct.utils.ResourceId"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="it.bysoftware.ct.service.persistence.TestataFattureClientiPK"%>
<%@page import="it.bysoftware.ct.service.TestataFattureClientiLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.TestataDocumentoLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.TestataFattureClienti"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="it.bysoftware.ct.utils.EntryComparator"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="it.bysoftware.ct.service.SpettanzeSociLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.SpettanzeSoci"%>
<%@page import="java.util.List"%>
<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8" />
<%@include file="../init.jsp"%>

<%
	String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
	String partnerCode = ParamUtil.getString(renderRequest, "partnerCode");
    String customerSelected = ParamUtil.getString(renderRequest, "customerSelected");
    String backUrl = ParamUtil.getString(renderRequest, "backUrl");
    boolean add = ParamUtil.getBoolean(renderRequest, "add", false);
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.DATE, 1);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    Date from = calendar.getTime();
    calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
    Date to = calendar.getTime();
    List<SpettanzeSoci> list = SpettanzeSociLocalServiceUtil.
            findByPartnerAndComputeDate(partnerCode, from, to, 0,
                    SpettanzeSociLocalServiceUtil.getSpettanzeSocisCount(), null);
    
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    NumberFormat nf = new DecimalFormat("#0.00", new DecimalFormatSymbols(renderRequest.getLocale()));
    String title = LanguageUtil.format(pageContext, "credit-x", new String[] {
            AnagraficheClientiFornitoriLocalServiceUtil.getAnagraficheClientiFornitori(partnerCode).getRagioneSociale1()});
%>

<liferay-ui:header title="<%=title %>" backURL="<%=backUrl %>" />
<c:if test="<%=add %>">
    <liferay-portlet:actionURL name="addPartnerCredit" var="addPartnerCreditURL">
        <liferay-portlet:param name="add"
            value="<%=String.valueOf(add)%>" />
	    <liferay-portlet:param name="partnerCode"
	        value="<%=partnerCode%>" />
	    <liferay-portlet:param name="backUrl"
	        value="<%=backUrl%>" />
    </liferay-portlet:actionURL>
    <aui:form id="aForm" name="aForm" action="<%=addPartnerCreditURL%>"
	    method="post">
	    <%
	    List<AnagraficheClientiFornitori> customers = AnagraficheClientiFornitoriLocalServiceUtil.findCustomers();
	    %>
	    <aui:input name="credit" type="text" cssClass="input-mini" label="credit" >
	        <aui:validator name="required" />
	        <aui:validator name="number" />
	    </aui:input>
	    <aui:select name="customerSelected" showEmptyOption="true" label="customer">
           <%for (AnagraficheClientiFornitori customer : customers) { %>
               <aui:option value="<%=customer.getCodiceAnagrafica() %>"
                   label="<%=customer.getRagioneSociale1() %>"
                   selected="<%=customerSelected.equals(customer.getCodiceAnagrafica()) %>"/>
           <%} %>
        </aui:select>
        <aui:select name="invoiceSelected" showEmptyOption="true" label="invoice" />
	    <aui:button-row>
	        <aui:button type="submit" value="save" />
	        <aui:button type="cancel" onclick="this.form.reset()" />
	    </aui:button-row>
	</aui:form>
</c:if>
<liferay-portlet:resourceURL var="selectInvoicesUrl" id="<%=ResourceId.selectInvoices.name() %>" />
<portlet:resourceURL var="printCreditsURL" 
    id="<%=ResourceId.printCredits.name() %>" />
<aui:nav-bar>
    <aui:nav>
        <aui:nav-item id="printBtn" iconCssClass="icon-print" label="print" 
           selected='<%="print".equals(toolbarItem)%>' />
    </aui:nav>
</aui:nav-bar>
<liferay-portlet:renderURL varImpl="iteratorURL">
    <portlet:param name="backUrl" value="<%= backUrl %>"/>
    <portlet:param name="partnerCode" value="<%= partnerCode %>"/>
    <portlet:param name="mvcPath" value="/html/partnerspayment/credit-details.jsp" />
</liferay-portlet:renderURL>
<liferay-ui:search-container delta="20" emptyResultsMessage="no-credit" iteratorURL="<%=iteratorURL%>">
    <liferay-ui:search-container-results>
        <%
            results = ListUtil.subList(list,
                                    searchContainer.getStart(),
                                    searchContainer.getEnd());
                            total = list.size();
                            pageContext.setAttribute("results", results);
                            pageContext.setAttribute("total", total);
        %>
    </liferay-ui:search-container-results>
    <liferay-ui:search-container-row className="SpettanzeSoci" modelVar="credit">
        <c:choose>
	        <c:when test="<%=credit.getCodiceAttivita() == null || credit.getCodiceCentro() == null || credit.getNumeroProtocollo() == 0 %>">
	            <liferay-ui:search-container-column-text value="" name="company-name" />
	           <liferay-ui:search-container-column-text value="" name="invoice" />
	        </c:when>
            <c:otherwise>
	            <%
		            TestataFattureClienti invoice = TestataFattureClientiLocalServiceUtil.
		            getTestataFattureClienti(new TestataFattureClientiPK(
		                    credit.getAnno(), credit.getCodiceAttivita(),
		                    credit.getCodiceCentro(), credit.getNumeroProtocollo()));
		            AnagraficheClientiFornitori customer = AnagraficheClientiFornitoriLocalServiceUtil.
		                    getAnagraficheClientiFornitori(invoice.getCodiceCliente());
		        %>
		        <liferay-ui:search-container-column-text value="<%= customer.getRagioneSociale1() + " " + customer.getRagioneSociale2() %>"
		            name="company-name" />
		        <liferay-ui:search-container-column-text value="<%= invoice.getNumeroDocumento() + "/" + invoice.getCodiceCentro() %>"
		            name="invoice" />
            </c:otherwise>
        </c:choose>
        <liferay-ui:search-container-column-text value="<%= String.valueOf(Utils.roundDouble(credit.getImporto(), 2)) %>"
            name="amount" />
    </liferay-ui:search-container-row>
    <liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
        paginate="true" />
</liferay-ui:search-container>

<aui:script>
// var selectInvoicesUrl = '<%= selectInvoicesUrl %>';

// AUI().use('aui-base', function(A) {
// 	A.one('#<portlet:namespace/>customerSelected').on('change', function(e) {
// 		AUI().use('aui-io-request', 'aui-modal', function (A) {
// 			var modal;
// 			A.io.request(selectInvoicesUrl, {
//                 method: 'POST',
//                 data: {
//                     <portlet:namespace/>customerSelected: A.one('#<portlet:namespace/>customerSelected').get('value'),
//                 },
//                 on: {
//                     start: function() {
//                         modal = new A.Modal({
//                             bodyContent : '<div class="loading-animation"/>',
//                             centered : true,
//                             headerContent : '<h3>Loading...</h3>',
//                             modal : true,
//                             render : '#modal',
//                             close: false,
//                             zIndex: 99999,
//                             width : 450
//                         }).render();
//                     },
//                     success: function () {
//                         var res = JSON.parse(this.get('responseData'));
// //                         console.log(data);
//                         var data = JSON.parse(res.message);
//                         var options = '<option value="" />';
//                         for (i = 0; i < data.length; i++) {
//                         	var key = {};
//                         	key.tipoSoggetto = data[i].tipoSoggetto;
//                         	key.codiceSoggetto = data[i].codiceSoggetto;
//                         	key.esercizioRegistrazione = data[i].esercizioRegistrazione;
//                         	key.numeroPartita = data[i].numeroPartita;
//                         	key.numeroScadenza = data[i].numeroScadenza;
//                         	var d = new Date(data[i].dataDocumento);
//                             options += '<option value="' + encodeURI(JSON.stringify(key)) + '">' + data[i].numeroDocumento + '/'+ data[i].codiceCentro + ' del ' + d.toLocaleDateString() + '</option>';
//                         }
//                         A.one('#<portlet:namespace/>invoiceSelected').setHTML(options);
//                         modal.hide();
//                     },
//                     error: function name() {
//                         modal.hide();
//                         cosole.log("ERRORE");
//                     }
//                 }
//             });
// 		});
// 	});
// });
var printCredits = '<%= printCreditsURL %>';
AUI().use('aui-base', 'aui-io-request', 'aui-modal', function(A) {
    A.one('#<portlet:namespace />printBtn').on('click', function(event) {
        A.io.request(printCredits, {
            method: 'POST',
            data: {
            	<portlet:namespace />partnerCode: '<%=partnerCode%>',
            	<portlet:namespace />from: '<%=from.getTime()%>',
            	<portlet:namespace />to: '<%=to.getTime()%>'
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                    var data = JSON.parse(this.get('responseData'));
                    console.log(data);
                    modal.hide();
                    if (data.code === 0) {
                        var win = window.open(data.message.toLocaleString());
                        if (win) {
                            window.focus();
                        } else {
                            var timer = window.setTimeout(function(){
                                if (win) {
                                    win.focus();
                                }
                            }, 200 );
                        }
                    } else {
                        alert('Si e\' verificato un errore durante la stampa del'
                                + ' buono.\n\n' + data.message);
                    }
                    
                },
                error: function () {
                    modal.hide();
                }
            }
        });
    });
});
</aui:script>