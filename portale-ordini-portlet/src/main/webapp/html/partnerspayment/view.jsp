<%@page import="com.liferay.portal.kernel.util.DateUtil"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="it.bysoftware.ct.model.ClientiFornitoriDatiAgg"%>
<%@page import="it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK"%>
<%@page import="it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="it.bysoftware.ct.utils.EntryComparator"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page
	import="it.bysoftware.ct.service.ScadenzePartiteLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.ScadenzePartite"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="../init.jsp"%>

<%
    String exceptionArgs = ParamUtil.getString(renderRequest, "exceptionArgs", "");
    Object[] args = exceptionArgs.split(StringPool.COMMA);
    String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
    long fromLng = ParamUtil.getLong(renderRequest, "dateFrom", 0);
    Calendar calendarFrom = Calendar.getInstance();
    if (fromLng > 0) {
        calendarFrom.setTimeInMillis(fromLng);
    } else {
        calendarFrom.add(Calendar.MONTH, -1);
        calendarFrom.set(Calendar.DAY_OF_MONTH, 1);
        calendarFrom.set(Calendar.HOUR_OF_DAY,0);
        calendarFrom.set(Calendar.MINUTE,0);
        calendarFrom.set(Calendar.SECOND,0);
        calendarFrom.set(Calendar.MILLISECOND, 0);
    }
    
    Date from = calendarFrom.getTime();
    Calendar calendarTo = Calendar.getInstance();
    long toLng = ParamUtil.getLong(renderRequest, "dateTo", 0);
    if (toLng > 0) {
        calendarTo.setTimeInMillis(toLng);
    } else {
        calendarTo.add(Calendar.MONTH, -1);
        calendarTo.set(Calendar.DAY_OF_MONTH, calendarFrom.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendarTo.set(Calendar.HOUR_OF_DAY,0);
        calendarTo.set(Calendar.MINUTE,0);
        calendarTo.set(Calendar.SECOND,0);
        calendarTo.set(Calendar.MILLISECOND, 0);
    }
    Date to = calendarTo.getTime();
    List<ScadenzePartite> tmp = ScadenzePartiteLocalServiceUtil.
            getCustomersClosedEntries(from, to, 0, ScadenzePartiteLocalServiceUtil.
                    getCustomersClosedEntriesCount(from, to), new EntryComparator());
    List<ScadenzePartite> entries = new ArrayList<ScadenzePartite>();
    for (ScadenzePartite entry : tmp) {
        ClientiFornitoriDatiAgg c = ClientiFornitoriDatiAggLocalServiceUtil.
                fetchClientiFornitoriDatiAgg(new ClientiFornitoriDatiAggPK(entry.getCodiceSoggetto(), true));
        if (c == null) {
            entries.add(entry);
        }
    }
    
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    NumberFormat nf = new DecimalFormat("#0.00", new DecimalFormatSymbols(renderRequest.getLocale()));
%>

<liferay-portlet:renderURL varImpl="reloadURL">
	<portlet:param name="mvcPath" value="/html/partnerspayment/view.jsp" />
</liferay-portlet:renderURL>
<liferay-portlet:renderURL varImpl="viewURL">
    <portlet:param name="mvcPath" value="/html/partnerspayment/view-generated-credits.jsp" />
</liferay-portlet:renderURL>
<liferay-portlet:actionURL name="generate" var="generate" />
<aui:form method="post" name="fm" >
    <aui:nav-bar>
	    <aui:nav>
	        <aui:nav-item id="generateBtn" iconCssClass="icon-cog" label="generate"
	           selected='<%="generate".equals(toolbarItem)%>' />
           <aui:nav-item id="viewBtn" iconCssClass="icon-search" label="view-generated"
               selected='<%="view".equals(toolbarItem)%>' />
	        <aui:nav-item id="reloadBtn" iconCssClass=" icon-refresh" label="refresh"
	           selected='<%="reload".equals(toolbarItem)%>' />
	    </aui:nav>
	    <aui:nav-bar-search cssClass="pull-right" >
	        <liferay-ui:input-date name="fromDate" 
	            dayValue="<%= calendarFrom.get(Calendar.DAY_OF_MONTH) %>" dayParam="fromDay"
	            monthValue="<%= calendarFrom.get(Calendar.MONTH) %>" monthParam="fromMonth"
	            yearValue="<%= calendarFrom.get(Calendar.YEAR) %>" yearParam="fromYear" />
	        <liferay-ui:input-date name="toDate" 
	            dayValue="<%= calendarTo.get(Calendar.DAY_OF_MONTH) %>" dayParam="toDay"
	            monthValue="<%= calendarTo.get(Calendar.MONTH) %>" monthParam="toMonth"
	            yearValue="<%= calendarTo.get(Calendar.YEAR) %>" yearParam="toYear" />
	    </aui:nav-bar-search>
    </aui:nav-bar>
    <liferay-ui:error key="error-generating-payments">
        <liferay-ui:message key="error-generating-payments-x" arguments="<%=args %>" />
    </liferay-ui:error>
    <liferay-portlet:renderURL varImpl="iteratorURL">
        <portlet:param name="dateFrom" value="<%=String.valueOf(fromLng) %>"/>
        <portlet:param name="dateTo" value="<%=String.valueOf(toLng) %>"/>
        <portlet:param name="mvcPath" value="/html/partnerspayment/view.jsp" />
    </liferay-portlet:renderURL>
    <aui:input name="documentIds" type="hidden" />
	<liferay-ui:search-container delta="20" emptyResultsMessage="no-entries"
	     iteratorURL="<%=iteratorURL%>"
	     rowChecker="<%=new RowChecker(renderResponse)%>" >
	    <liferay-ui:search-container-results>
		<%
		    results = ListUtil.subList(entries,
                          searchContainer.getStart(),
                          searchContainer.getEnd());
            total = entries.size();
            pageContext.setAttribute("results", results);
            pageContext.setAttribute("total", total);
		%>
		</liferay-ui:search-container-results>
		<liferay-ui:search-container-row
			className="it.bysoftware.ct.model.ScadenzePartite" modelVar="entry" keyProperty="numeroDocumento" rowVar="curRow">
			<liferay-ui:search-container-column-text property="codiceSoggetto" name="code" />
			<%
			curRow.setPrimaryKey(JSONFactoryUtil.looseSerialize(entry.getPrimaryKeyObj()));
			AnagraficheClientiFornitori a = AnagraficheClientiFornitoriLocalServiceUtil.
			        fetchAnagraficheClientiFornitori(entry.getCodiceSoggetto());
			%>
			<liferay-ui:search-container-column-text name="company-name" 
			    value="<%= a != null ? a.getRagioneSociale1() + a.getRagioneSociale2() : ""%>"/>
		    <liferay-ui:search-container-column-text name="closing-date" value="<%= sdf.format(entry.getDataChiusura()) %>" />
			<liferay-ui:search-container-column-text name="invoice"
			    value="<%="N. " + entry.getNumeroDocumento() + "/" + entry.getCodiceCentro() + " del: " + sdf.format(entry.getDataRegistrazione()) %>" />
			<liferay-ui:search-container-column-text name="amount" value="<%= nf.format(entry.getImportoPagato())%>"/>
			<liferay-ui:search-container-row-parameter name="" value="" />
		</liferay-ui:search-container-row>
		<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
			paginate="true" />
	</liferay-ui:search-container>
</aui:form>

<%!private static Log _log = LogFactoryUtil
            .getLog("docroot.html.partnerspayment.view_jsp");%>

<aui:script>

	AUI().use('liferay-util-list-fields', function(A) {
	    A.one('#<portlet:namespace/>generateBtn').on('click', function(event) {
	        var checkBoxValue = Liferay.Util.listCheckedExcept(document.<portlet:namespace />fm, '<portlet:namespace />allRowIds');
	        if (checkBoxValue == '' || checkBoxValue == null) {
	            alert('Seleziona almeno un documento');
	            return false;
	        }
	        if (confirm('Procedere alla generazione dei pagamenti documenti selezionati?')) {
	            document.<portlet:namespace />fm.<portlet:namespace />documentIds.value = '{"entries":['+ checkBoxValue + ']}';
	            submitForm(document.<portlet:namespace />fm, '<%= generate.toString() %>');
	        }
	
	    });
	});

	AUI().use('aui-base', 'datatype-number', function(A) {
	    A.one('#<portlet:namespace/>reloadBtn').on('click', function() {
			var fromDateStr = A.one('#<portlet:namespace />fromDate').get('value');
			var fromDate = parseDate(fromDateStr);
			var toDateStr = A.one('#<portlet:namespace/>toDate').get('value');
			var toDate = parseDate(toDateStr);
			window.location.href = '<%=reloadURL%>&<portlet:namespace/>dateFrom=' + fromDate.getTime() + '&<portlet:namespace/>dateTo=' + toDate.getTime();
		});
	});
	
	AUI().use('aui-base', 'datatype-number', function(A) {
        A.one('#<portlet:namespace/>viewBtn').on('click', function() {
            window.location.href = '<%=viewURL%>';
        });
    });
	
	function parseDate(string) {
		var tmp = string.split('/');
        return new Date(tmp[2] + '-' + tmp[1] + '-' + tmp[0]);
	}
</aui:script>