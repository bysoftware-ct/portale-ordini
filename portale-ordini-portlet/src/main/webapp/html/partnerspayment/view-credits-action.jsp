<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8" />
<%@include file="../init.jsp"%>

<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    Object[] credit = (Object[]) row.getParameter("credit");
    int generated = Integer.parseInt(credit[2].toString());
    String backUrl = (String) row.getParameter("backUrl");
%>

<liferay-ui:icon-menu>
    <liferay-portlet:renderURL varImpl="editCreditBoardURL">
        <portlet:param name="mvcPath"
            value="/html/partnerspayment/edit-credit-board.jsp" />
        <portlet:param name="backUrl" value="<%= backUrl %>" />
        <portlet:param name="partnerCode"
            value="<%=String.valueOf(credit[1])%>" />
        <portlet:param name="credit"
            value="<%=String.valueOf(credit[3])%>" />
    </liferay-portlet:renderURL>
    <liferay-portlet:renderURL varImpl="viewCreditBoardURL">
        <portlet:param name="mvcPath"
            value="/html/partnerspayment/view-payment-board.jsp" />
        <portlet:param name="backUrl" value="<%= backUrl %>" />
        <portlet:param name="year"
            value="<%=String.valueOf(credit[5])%>" />
        <portlet:param name="partnerCode"
            value="<%=String.valueOf(credit[1])%>" />
        <portlet:param name="credit"
            value="<%=String.valueOf(credit[3])%>" />
        <portlet:param name="paymentId"
            value="<%=String.valueOf(credit[4])%>" />
        <portlet:param name="paymentYear"
            value="<%=String.valueOf(Utils.getYear())%>" />
<%--         <c:if test="<%= generated > 1%>"> --%>
<%-- 	        <portlet:param name="state" --%>
<%-- 	             value="<%=String.valueOf(credit[2])%>" /> --%>
<%--         </c:if> --%>
    </liferay-portlet:renderURL>
    <c:if test="<%=generated == 1%>">
        <liferay-ui:icon image="edit" url="${editCreditBoardURL}" message="edit-payment-board" />
        <liferay-ui:icon image="view" url="${viewCreditBoardURL}" message="view-payment-board" />
    </c:if>
    <c:if test="<%=generated == 2%>">
        <liferay-ui:icon image="view" url="${viewCreditBoardURL}" message="view-payment-board" />
    </c:if>
    <c:if test="<%=generated == 0%>">
        <liferay-ui:icon image="add_article" url="${editCreditBoardURL}" message="add-payment-board" />
        <liferay-portlet:renderURL varImpl="addCreditsURL">
            <portlet:param name="mvcPath"
                value="/html/partnerspayment/credit-details.jsp" />
            <portlet:param name="backUrl" value="<%= backUrl %>" />
            <portlet:param name="add" value="true" />
            <portlet:param name="partnerCode"
                value="<%=String.valueOf(credit[1])%>" />
        </liferay-portlet:renderURL>
        <liferay-ui:icon image="add" url="${addCreditsURL}" message="add-partner-credit" />
    </c:if>
    <liferay-portlet:renderURL varImpl="creditDetailsURL">
        <portlet:param name="mvcPath"
            value="/html/partnerspayment/credit-details.jsp" />
        <portlet:param name="backUrl" value="<%= backUrl %>" />
        <portlet:param name="partnerCode"
            value="<%=String.valueOf(credit[1])%>" />
    </liferay-portlet:renderURL>
    <liferay-ui:icon image="search" url="${creditDetailsURL}" message="view-credit" />
</liferay-ui:icon-menu>