<%@page import="it.bysoftware.ct.model.Pagamento"%>
<%@page import="it.bysoftware.ct.service.persistence.PagamentoPK"%>
<%@page import="it.bysoftware.ct.service.PagamentoLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.json.JSONObject"%>
<%@page import="it.bysoftware.ct.utils.ResourceId"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="com.liferay.portal.kernel.util.KeyValuePair"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="it.bysoftware.ct.utils.EntryState"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="it.bysoftware.ct.utils.EntryComparator"%>   
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page
	import="it.bysoftware.ct.service.ScadenzePartiteLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.ScadenzePartite"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../init.jsp"%>

<%
    String backUrl = ParamUtil.getString(renderRequest, "backUrl");
    String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
    String partnerCode = ParamUtil.getString(renderRequest, "partnerCode");
    String entryIds = ParamUtil.getString(renderRequest, "entryIds", "{}");
    AnagraficheClientiFornitori partner = AnagraficheClientiFornitoriLocalServiceUtil.
            getAnagraficheClientiFornitori(partnerCode);
    double credit = ParamUtil.getDouble(renderRequest, "credit", 0.0);
    int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
    Pagamento oldPayment = null;
    double prevBalance = 0.0;
    if (month == 1) {
        oldPayment = PagamentoLocalServiceUtil.fetchPagamento(new PagamentoPK(12, Calendar.getInstance().get(Calendar.YEAR) -1, partnerCode));
    } else {
        oldPayment = PagamentoLocalServiceUtil.fetchPagamento(new PagamentoPK(month - 1, Calendar.getInstance().get(Calendar.YEAR), partnerCode));
    }
    
    if (oldPayment != null) {
        prevBalance = oldPayment.getSaldo();
    }
    
    double balance = credit + prevBalance;
    double totalGive = ParamUtil.getDouble(renderRequest, "totalGive", 0.0);
    double totalHave = ParamUtil.getDouble(renderRequest, "totalHave", 0.0);
    Calendar calendarTo = Calendar.getInstance();
	long toLng = ParamUtil.getLong(renderRequest, "dateTo", 0);
	if (toLng > 0) {
	    calendarTo.setTimeInMillis(toLng);
	} 
	
	Date to = calendarTo.getTime();
	List<ScadenzePartite> entries =
	        ScadenzePartiteLocalServiceUtil.
	        getPartnerOpenedEntries(partnerCode, null, to, 0, ScadenzePartiteLocalServiceUtil.
	                getPartnerOpenedEntriesCount(partnerCode, null, to), new EntryComparator());	        
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	DecimalFormat nf = new DecimalFormat("#0.00");
%>
<liferay-ui:header backURL="<%=backUrl%>"
	title="<%=partner.getRagioneSociale1() + " " + partner.getRagioneSociale2()%>" />
	
<liferay-portlet:renderURL varImpl="iteratorURL">
	<portlet:param name="backUrl" value="<%=backUrl%>" />
	<portlet:param name="partnerCode" value="<%=partnerCode%>" />
	<portlet:param name="credit" value="<%=String.valueOf(credit)%>" />
	<portlet:param name="dateTo" value="<%=String.valueOf(toLng)%>" />
	<portlet:param name="mvcPath"
		value="/html/partnerspayment/edit-credit-board.jsp" />
</liferay-portlet:renderURL>
<liferay-portlet:resourceURL var="selectEntriesUrl" id="<%=ResourceId.selectEntries.name() %>" />
<liferay-portlet:actionURL name="save" var="save" >
    <liferay-portlet:param name="backUrl" value="<%=backUrl %>"/>
    <liferay-portlet:param name="partnerCode" value="<%=partnerCode %>"/>
    <liferay-portlet:param name="credit" value="<%=String.valueOf(credit) %>"/>
    <liferay-portlet:param name="prevBalance" value="<%=String.valueOf(prevBalance) %>"/>
    <liferay-portlet:param name="dateTo" value="<%=String.valueOf(toLng) %>"/>
</liferay-portlet:actionURL>
<span id="group-error-block" ></span>
<aui:form method="post" name="fm" >
	<aui:layout> 
	    <aui:column columnWidth="50" first="true">
	       <aui:input name="documentIds" type="hidden" /> 
	       <aui:nav-bar>
	           <aui:nav>
	               <aui:nav-item id="confirmBtn" iconCssClass="icon-arrow-right" label="confirm"
	                   selected='<%="confirm".equals(toolbarItem)%>' />
<%-- 	               <aui:nav-item id="saveBtn" iconCssClass=" icon-save" label="save"  --%>
<%-- 	                   selected='<%="save".equals(toolbarItem)%>' /> --%>
	           </aui:nav>
           </aui:nav-bar>
	       <liferay-ui:search-container delta="30" emptyResultsMessage="no-entries" 
	            iteratorURL="<%=iteratorURL %>" rowChecker="<%=new RowChecker(renderResponse)%>"  > 
	           <liferay-ui:search-container-results> 
	           <% 
		             results = ListUtil.subList(entries,
		                           searchContainer.getStart(),
		                           searchContainer.getEnd());
		             total = entries.size();
		             pageContext.setAttribute("results", results);
		             pageContext.setAttribute("total", total);
	           %> 
	           </liferay-ui:search-container-results> 
	           <liferay-ui:search-container-row 
	               className="it.bysoftware.ct.model.ScadenzePartite" modelVar="entry" keyProperty="numeroDocumento" rowVar="curRow"> 
	               <% 
			             curRow.setPrimaryKey(JSONFactoryUtil.looseSerialize(entry.getPrimaryKeyObj()));
			             double giveAmount = 0.0;
			             double haveAmount = 0.0;
			             if (entry.getTipoSoggetto()) {
			                 haveAmount = entry.getImportoTotale() - entry.getImportoPagato(); //entry.getImportoAperto();
			             } else {
			                 giveAmount = entry.getImportoTotale() - entry.getImportoPagato(); //entry.getImportoAperto();
			             }
	               %> 
	               <liferay-ui:search-container-column-text name="invoice" 
	                   value="<%="N. " + entry.getNumeroDocumento() + "/" + entry.getCodiceCentro() + " del: " + sdf.format(entry.getDataRegistrazione()) %>" /> 
	               <liferay-ui:search-container-column-text cssClass="green" name="credit"
	                   value="<%= giveAmount > 0 ? nf.format(giveAmount) : ""%>"/> 
	               <liferay-ui:search-container-column-text cssClass="red" name="debt"
	                   value="<%=haveAmount > 0 ? nf.format(haveAmount) : ""%>" /> 
	           </liferay-ui:search-container-row> 
	           <liferay-ui:search-iterator searchContainer="<%=searchContainer%>" paginate="true" /> 
	       </liferay-ui:search-container> 
	   </aui:column> 
	   <aui:column columnWidth="50" first="last">
	       <aui:input name="entryIds" type="hidden" /> 
	       <aui:nav-bar>
               <aui:nav>
<%--                    <aui:nav-item id="confirmBtn" iconCssClass="icon-ok" label="confirm" --%>
<%--                        selected='<%="save".equals(toolbarItem)%>' /> --%>
                  <aui:nav-item id="saveBtn" iconCssClass=" icon-save" label="save" 
                       selected='<%="save".equals(toolbarItem)%>' />
               </aui:nav>
           </aui:nav-bar>
           <div class="wrapper">
               <aui:button-row cssClass="content">
                   <aui:input id="give-credit" name="give-credit" cssClass="input-mini"
                       value="<%=Utils.roundDouble(credit, 2) %>" prefix="€" disabled="true" label="give-credit"
                       inlineField="true" />
                   <aui:input id="prev-balance" name="prev-balance" cssClass="input-mini"
                       value="<%=nf.format(prevBalance)%>" prefix="€" disabled="true" label="prev-balance"
                       inlineField="true" />
                   <aui:input id="balance" name="balance" cssClass="input-mini"
                       value="<%=Utils.roundDouble(balance, 2)%>" prefix="€" disabled="true" label="credit"
                       inlineField="true" />
               </aui:button-row>
			   <div class="content" id="myDataTable" ></div>
			   <aui:button-row cssClass="content">
				   <aui:input id="totalGive" name="totalGive" cssClass="input-mini"
					   value="<%=nf.format(totalGive)%>" prefix="€" disabled="true" label="total-give"
					   inlineField="true" />
				   <aui:input id="totalHave" name="totalHave" cssClass="input-mini"
		               value="<%=nf.format(totalHave)%>" prefix="€" disabled="true" label="total-have"
		               inlineField="true" />
	               <aui:input id="newBalance" name="newBalance" type="hidden"
	                   value="<%=nf.format(totalGive - totalHave)%>" />
	               <aui:input id="payedAmount" name="payedAmount" type="hidden"
                       value="<%=nf.format(totalGive - totalHave)%>" />
				   <aui:input id="new-balance" name="new-balance" cssClass="input-mini"
		               value="<%=nf.format(totalGive - totalHave)%>" prefix="€" disabled="true" label="new-balance"
		               inlineField="true" />
               </aui:button-row>
           </div>
	   </aui:column> 
	</aui:layout> 
</aui:form>
<aui:script>
    var selectEntriesUrl = '<%= selectEntriesUrl %>';
    var entryIds = '<%= entryIds%>';
    var modal;
    
    AUI().ready(function(A) {
    	if (entryIds !== '{}') {
	    	var tmp = JSON.parse(entryIds);
	    	drawTable(tmp.entries);
    	}
    });
    
	function drawTable(remoteData) {
		AUI().use('aui-base', 'aui-datatable', 'aui-datatype', 'datatable-sort', 'datatable-mutable', function(A) {
// 	        var remoteData = [{
// 	                id: '1',
// 	                entry: 'N. X/XXX del: dd/MM/aaaa',
// 	                give: 0.0,
// 	                have: 0.0,
// 	                ref: ''
// 	            }, {
// 	                id: '2',
// 	                entry: 'N. X/XXX del: dd/MM/aaaa',
// 	                give: 0.0,
// 	                have: 0.0,
// 	                ref: '1'
// 	            }];
            var recordSelected;
	        var ids = [];
	        var totGive = 0;
	        var totHave = 0;
	        remoteData.forEach(function(item, i) {
	            ids[i] = item.id;
	            totGive += parseFloat(item.give);
	            totHave += parseFloat(item.have);
	        });
	        A.one('#myDataTable').empty();
	        A.one('#<portlet:namespace/>totalGive').set('value', currencyFormatter(totGive));
	        A.one('#<portlet:namespace/>totalHave').set('value', currencyFormatter(totHave));
	        var balance = A.one('#<portlet:namespace/>balance').get('value');
	        A.one('#<portlet:namespace/>new-balance').set('value', currencyFormatter(balance - (totGive - totHave)));
	        A.one('#<portlet:namespace/>newBalance').set('value', currencyFormatter(balance - (totGive - totHave)));
	        A.one('#<portlet:namespace/>payedAmount').set('value', currencyFormatter(totGive - totHave));
	        var nestedCols = [{
	                key: 'id',
	                label: '#'
	            }, {
	                key: 'entry',
	                label: 'Partita'
	            }, {
	                key: 'give',
	                label: 'Dare',
	                formatter: giveFormatter
	            },{ 
	                key: 'have',
	                label: 'Avere',
	                formatter: haveFormatter
	            }, {
	                editor: new A.DropDownCellEditor(
	                  {
	                    options: ids
	                  }
	                ),
	                key: 'ref',
	                label: 'Rif.'
	            },{
                    key: 'diff',
                    label: 'Diff.'
                }];
	        var dataTable = new A.DataTable({
	            columns: nestedCols,
	            data: remoteData,
	            editEvent: 'dblclick',
	            plugins: [{
	                cfg: {
	                    highlightRange: false
	                },
	                fn: A.Plugin.DataTableHighlight
	            }]
	        }).render('#myDataTable');
	        dataTable.get('boundingBox').unselectable();
	        dataTable.delegate('click', function(e, table) {
	        	recordSelected = table.getRecord(e.currentTarget);
// 	        	console.log(recordSelected);
            }, 'tr', this, dataTable);
	        dataTable.after('*:refChange', function (e, table) {
                var entryIds = A.one('#<portlet:namespace/>entryIds').get('value');
                var rowId = recordSelected.getAttrs().id;
                if (entryIds.indexOf('entries') === -1){
                	entryIds = '{"entries": '+ entryIds + '}';
                }
                var tableData = e.currentTarget.getAttrs().data;
                console.log(e);
                tableData._items.forEach(function(item, i) {
                	var entry = item.getAttrs();
                	if (entry.id == e.newVal) {
                		item.setAttrs({diff: currencyFormatter(entry.give - recordSelected.getAttrs().have)});
                	} else if (e.prevVal && entry.id == e.prevVal) {
                		item.setAttrs({diff: ''});
                	}
                });
                var entryIdsJson = JSON.parse(entryIds);
                var newEntryIds = [];
                entryIdsJson.entries.forEach(function(item, i) {
                	if (item.id == e.newVal) {
                		item.ref = rowId;
                		item.diff = currencyFormatter(item.give - recordSelected.getAttrs().have);
                	} else if (item.id == rowId) {
                		item.ref = parseInt(e.newVal);
                	}  else if (e.prevVal && item.id == e.prevVal) {
                		item.diff = 0;
                		delete item.ref;
                    }
                	newEntryIds.push(item);
                });
//                 console.log(JSON.stringify(newEntryIds));
                A.one('#<portlet:namespace/>entryIds').set('value', JSON.stringify(newEntryIds));
            }, 'tr', this, dataTable);
	    });
	}

	AUI().use('liferay-util-list-fields', function(A) {
        A.one('#<portlet:namespace/>confirmBtn').on('click', function(event) {
            var checkBoxValue = Liferay.Util.listCheckedExcept(document.<portlet:namespace />fm, '<portlet:namespace />allRowIds');
            if (checkBoxValue == '' || checkBoxValue == null) {
                alert('Seleziona almeno un documento');
                return false;
            }
//             if (confirm('Procedere alla generazione dei pagamenti documenti selezionati?')) {
                document.<portlet:namespace />fm.<portlet:namespace />documentIds.value = '"entries":['+ checkBoxValue + ']';
                var documentIds = A.one('#<portlet:namespace/>documentIds').get('value');
                AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
                    var errorBlock=A.one('#group-error-block');
                    A.io.request(selectEntriesUrl, {
                        method: 'POST',
                        data: {
                        	<portlet:namespace/>data: '{' + documentIds + '}',
                        },
                        on: {
                            start: function() {
                                modal = new A.Modal({
                                    bodyContent : '<div class="loading-animation"/>',
                                    centered : true,
                                    headerContent : '<h3>Loading...</h3>',
                                    modal : true,
                                    render : '#modal',
                                    close: false,
                                    zIndex: 99999,
                                    width : 450
                                }).render();
                            },
                            success: function () {
                                var data = JSON.parse(this.get('responseData'));
//                                 console.log(data);
                                switch (data.code) {
                                case 0:
                                	var tmp = JSON.parse(data.message);
                                	var entries = [];
                                	tmp.forEach(function(item, i) {
                                		item = JSON.parse(item);
                                		var entry = {
                               				id: i + 1,
                               				entryPK: {
                               					tipoSoggetto: item.tipoSoggetto,
                                                codiceSoggetto: item.codiceSoggetto,
                                                esercizioRegistrazione: item.esercizioRegistrazione,
                                                numeroPartita: item.numeroPartita,
                                                numeroScadenza: item.numeroScadenza
                               				},
                               				entry: 'N. ' + item.numeroDocumento + '/' + item.codiceCentro + ' del: ' + convertDate(new Date(item.dataRegistrazione)),
                                            ref: item.ref
                                        };
                                        if (item.tipoSoggetto == true) {
                                        	entry.give = currencyFormatter(item.importoTotale - item.importoPagato);
                                        	entry.have = 0.0;
                                        } else {
                                        	entry.give = 0.0;
                                            entry.have = currencyFormatter(item.importoTotale - item.importoPagato);
                                        }
                                        entries.push(entry)
                                    });
                                	drawTable(entries);
                                    A.one('#<portlet:namespace/>entryIds').set('value', JSON.stringify(entries));
                                    break;
                                case 6:
                                    var errorMessageNode = A.Node.create('<div class="portlet-msg-error">' 
                                            + Liferay.Language.get('generic-error-message-x', [data.message]) + '</div>');
                                    errorMessageNode.appendTo(errorBlock);
                                    break;

                                default:
                                    break;
                                }
                                
                                modal.hide();
                            },
                            error: function name() {
                                modal.hide();
                                cosole.log("ERRORE");
                            }
                        }
                    });
                });
//             }
    
        });
    });
	AUI().use('liferay-util-list-fields', function(A) {
		A.one('#<portlet:namespace/>saveBtn').on('click', function(event) {
		    if (confirm('Procedere alla generazione dei pagamenti?')) {
		    	var entryIds = A.one('#<portlet:namespace/>entryIds').get('value');
		    	if (entryIds.indexOf('entries') === -1 ) {
		    		document.<portlet:namespace />fm.<portlet:namespace />entryIds.value = '{"entries": '+ entryIds + '}';
		    	} else {
		    		document.<portlet:namespace />fm.<portlet:namespace />entryIds.value = entryIds;
		    	}
		        submitForm(document.<portlet:namespace />fm, '<%= save.toString() %>');
		    }
		
		});
	});
	

	function giveFormatter(amount) {
		amount.className += 'green';
		currencyFormatter(amount.value);
	}
	function haveFormatter(amount) {
		amount.className += 'red';
		currencyFormatter(amount.value);
	}
	function currencyFormatter(amount) {
		if (isNaN(amount)) {
			return '';
		}
		return parseFloat(Math.round(amount * 100) / 100).toFixed(2);
	}
	function convertDate(inputFormat) {
		function pad(s) {
			return (s < 10) ? '0' + s : s;
		}
		var d = new Date(inputFormat);
		return [ pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear() ]
				.join('/');
	}
</aui:script>
	

