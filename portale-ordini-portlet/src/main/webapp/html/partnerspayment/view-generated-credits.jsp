<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="it.bysoftware.ct.utils.PaymentState"%>
<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="it.bysoftware.ct.service.SpettanzeSociLocalServiceUtil"%>
<%@page import="java.util.List"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="../init.jsp"%>

<%
    List<AnagraficheClientiFornitori> tmp = AnagraficheClientiFornitoriLocalServiceUtil
            .findAssociates();
    String code = ParamUtil.getString(request, "code", "");
    String companyName = ParamUtil.getString(request, "companyName", "");
	long fromLng = ParamUtil.getLong(renderRequest, "dateFrom", 0);
	Calendar calendarFrom = Calendar.getInstance();
	if (fromLng > 0) {
	    calendarFrom.setTimeInMillis(fromLng);
	} else {
	    calendarFrom.set(Calendar.DATE, 1);
	    calendarFrom.set(Calendar.HOUR_OF_DAY, 0);
	    calendarFrom.set(Calendar.MINUTE, 0);
	    calendarFrom.set(Calendar.SECOND, 0);
	    calendarFrom.set(Calendar.MILLISECOND, 0);
	}
	
	Date from = calendarFrom.getTime();
	
	Calendar calendarTo = Calendar.getInstance();
	long toLng = ParamUtil.getLong(renderRequest, "dateTo", 0);
	if (toLng > 0) {
	    calendarTo.setTimeInMillis(toLng);
	} else {
	    calendarTo.set(Calendar.DATE, calendarTo.getActualMaximum(Calendar.DAY_OF_MONTH));
	    calendarTo.set(Calendar.HOUR_OF_DAY, 0);
	    calendarTo.set(Calendar.MINUTE, 0);
	    calendarTo.set(Calendar.SECOND, 0);
	    calendarTo.set(Calendar.MILLISECOND, 0);
	}
	Date to = calendarTo.getTime();

	List<Object[]> credits = SpettanzeSociLocalServiceUtil.getCreditAmountPerPartner(null, from, to);
	Map<String, Object[]> partnerCredits = new HashMap<String, Object[]>();
	for (Object[] obj : credits) {
		if (partnerCredits.containsKey(String.valueOf(obj[1]))) {
			if (obj[3] instanceof Double) {
				partnerCredits.get(String.valueOf(obj[1]))[3] = ((Double) partnerCredits.get(String.valueOf(obj[1]))[3])
					+ (Double) obj[3];
			}
		} else {
			partnerCredits.put(String.valueOf(obj[1]), obj);
		}
	}

	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    NumberFormat nf = new DecimalFormat("#0.00", new DecimalFormatSymbols(renderRequest.getLocale()));
    List<AnagraficheClientiFornitori> partners = new ArrayList<AnagraficheClientiFornitori>();
    if (!code.isEmpty() || !companyName.isEmpty()) {
        for (AnagraficheClientiFornitori p : tmp) {
            boolean add = true;
            if (!code.isEmpty()
                    && !p.getCodiceAnagrafica().toLowerCase().
                            contains(code.toLowerCase())) {
                add = false;
            }
            if (!companyName.isEmpty()
                    && !p.getRagioneSociale1().toLowerCase().
                            contains(companyName.toLowerCase())) {
                add = false;
            }

            if (add) {
                partners.add(p);
            }
        }
    } else {
        partners = tmp;
    }
%>
<liferay-portlet:renderURL varImpl="partnerSearchURL">
    <portlet:param name="mvcPath"
        value="/html/partnerspayment/view-generated-credits.jsp" />
    <portlet:param name="dateFrom" value="<%= String.valueOf(fromLng) %>"/>
    <portlet:param name="dateto" value="<%= String.valueOf(toLng) %>"/>
</liferay-portlet:renderURL>
<aui:form action="<%=partnerSearchURL%>" method="get" name="partnerForm">
    <liferay-portlet:renderURLParams varImpl="partnerSearchURL" />
       <aui:input label="code" name="code" value="<%=code%>" inlineField="true"/>
       <aui:input label="company-name" name="companyName" inlineField="true"
            value="<%=companyName%>" />
       <aui:button-row>
            <aui:button type="submit" value="search" />
            <aui:button type="reset" value="cancel" />
       </aui:button-row>
</aui:form>
<liferay-portlet:renderURL varImpl="iteratorURL">
    <portlet:param name="dateFrom" value="<%= String.valueOf(fromLng) %>"/>
    <portlet:param name="dateto" value="<%= String.valueOf(toLng) %>"/>
    <portlet:param name="code" value="<%= code %>"/>
    <portlet:param name="companyName" value="<%= companyName %>"/>
    <portlet:param name="mvcPath" value="/html/partnerspayment/view-generated-credits.jsp" />
</liferay-portlet:renderURL>

<liferay-ui:search-container delta="20" emptyResultsMessage="no-orders" iteratorURL="<%=iteratorURL%>">
    <liferay-ui:search-container-results>
        <%
            results = ListUtil.subList(partners,
                                    searchContainer.getStart(),
                                    searchContainer.getEnd());
                            total = partners.size();
                            pageContext.setAttribute("results", results);
                            pageContext.setAttribute("total", total);
        %>
    </liferay-ui:search-container-results>
    <liferay-ui:search-container-row className="AnagraficheClientiFornitori" modelVar="partner">
        <liferay-ui:search-container-column-text name="code" value="<%= partner.getCodiceAnagrafica() %>" />
        <%
        Object[] credit = null;
        if (partnerCredits.containsKey(partner.getCodiceAnagrafica())) {
            credit = partnerCredits.get(partner.getCodiceAnagrafica());
        } else {
            credit = new Object[]{ Utils.getYear(), partner.getCodiceAnagrafica(), 0, 0.0, -1, 0};
        }
        %>
        <liferay-ui:search-container-column-text value="<%= partner.getRagioneSociale1() + " " + partner.getRagioneSociale2() %>"
            name="company-name" />
        <liferay-ui:search-container-column-text name="board">
            <%
            if (Integer.parseInt(credit[2].toString()) > 0) {
                PaymentState state = PaymentState.valueOf(Integer.parseInt(credit[2].toString()));
            %>
                <liferay-ui:message key="<%=state.name().toLowerCase()%>" arguments="<%=state.getVal() %>" />
            <%
            }
            %>
        </liferay-ui:search-container-column-text>
        <liferay-ui:search-container-column-text value="<%=nf.format(credit[3]) %>" name="amount" />
        <liferay-ui:search-container-column-jsp
            path="/html/partnerspayment/view-credits-action.jsp" />
        <liferay-ui:search-container-row-parameter name="credit"
            value="<%= credit %>" />
        <liferay-ui:search-container-row-parameter name="backUrl"
            value="<%= iteratorURL.toString() %>" />
    </liferay-ui:search-container-row>
    <liferay-ui:search-iterator searchContainer="<%=searchContainer%>" />
</liferay-ui:search-container>