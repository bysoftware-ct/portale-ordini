<%@page import="com.liferay.portal.kernel.dao.search.DisplayTerms"%>
<%@page import="com.liferay.portal.kernel.dao.search.SearchContainer"%>
<%@ include file="../init.jsp"%>
<%
    SearchContainer searchContainer = (SearchContainer) request
            .getAttribute("liferay-ui:search:searchContainer");
    DisplayTerms displayTerms = searchContainer.getDisplayTerms();
    String itemCode = ParamUtil.getString(request, "itemCode");
    String itemVariant = ParamUtil.getString(request, "itemVariant");
    String itemDesc = ParamUtil.getString(request, "itemDesc");
    String partnerItemCode = ParamUtil.getString(request,
            "partnerItemCode");
    String partnerItemVariant = ParamUtil.getString(request,
            "partnerItemVariant");
%>

<liferay-ui:search-toggle buttonLabel="search"
    displayTerms="<%=displayTerms%>" id="toggle_id_search">
    <aui:input label="code" name="itemCode" value="<%=itemCode %>" />
    <aui:input label="variant" name="itemVariant" value="<%=itemVariant %>" />
    <aui:input label="name" name="itemDesc" value="<%=itemDesc %>" />
    <aui:input label="parnter-code" name="partnerItemCode" value="<%=partnerItemCode %>" />
    <aui:input label="parnter-variant" name="partnerItemVariant" value="<%=partnerItemVariant %>" />
</liferay-ui:search-toggle>