<%@page import="it.bysoftware.ct.model.ArticoloSocio"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@ include file="../init.jsp"%>
<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    ArticoloSocio partnerItem = (ArticoloSocio) row.getObject();
    String backUrl = (String) row.getParameter("backUrl");
    String companyName = (String) row.getParameter("companyName");
%>

<liferay-ui:icon-menu>
    <liferay-portlet:renderURL varImpl="editAssociationURL">
        <portlet:param name="mvcPath"
            value="/html/associateitems/edit-association.jsp" />
        <portlet:param name="backUrl" value="<%= backUrl %>" />
        <portlet:param name="partnerCode" value="<%=partnerItem.getAssociato()%>" />
        <portlet:param name="companyName" value="<%=companyName%>" />
        <portlet:param name="partnerItemCode" value="<%=partnerItem.getCodiceArticoloSocio()%>" />
        <portlet:param name="partnerItemVariant" value="<%=partnerItem.getCodiceVarianteSocio()%>" />
    </liferay-portlet:renderURL>
    <liferay-ui:icon image="edit" url="${editAssociationURL}" message="edit" />
    <portlet:actionURL name="delete" var="deleteAssociationURL">
        <portlet:param name="partner"
            value="<%= partnerItem.getAssociato() %>" />
        <portlet:param name="partnerItemCode"
            value="<%= partnerItem.getCodiceArticoloSocio() %>" />
        <portlet:param name="partnerItemVariant"
            value="<%= partnerItem.getCodiceVarianteSocio() %>" />
        <portlet:param name="backUrl" value="<%= backUrl %>" />
        <portlet:param name="companyName" value="<%= companyName %>" />
    </portlet:actionURL>
    <liferay-ui:icon-delete url="${deleteAssociationURL}" message="delete"  />
</liferay-ui:icon-menu>