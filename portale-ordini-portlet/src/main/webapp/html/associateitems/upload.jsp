<%@page import="it.bysoftware.ct.utils.ResourceId"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.json.JSONArray"%>
<%@ include file="../init.jsp"%>
<%
    String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
    String backUrl = ParamUtil.getString(renderRequest, "backUrl");
    String partnerCode = ParamUtil.getString(renderRequest, "partnerCode");    
//     String data = ParamUtil.getString(renderRequest, "dataJson", null);
    JSONArray remoteData = (JSONArray) renderRequest.getAttribute("dataJson");//JSONFactoryUtil.createJSONArray();
    int itemsPerPage = 20;
    int pages = 0;
//     if (data != null) {
//         remoteData = JSONFactoryUtil.createJSONArray(data);
//     }
%>
<liferay-ui:header backURL="<%=backUrl%>" title="import" >
    <portlet:param name="backUrl" value="<%=backUrl %>"/>
    <portlet:param name="partnerCode" value="<%=partnerCode %>"/>
</liferay-ui:header>

<portlet:resourceURL var="saveURL" id="<%=ResourceId.saveAssociation.name() %>"/>

<liferay-portlet:actionURL name="upload" var="uploadFileURL" >
    <portlet:param name="backUrl" value="<%=backUrl %>"/>    
</liferay-portlet:actionURL>
<liferay-ui:error key="empty-file" message="empty-data" />
<liferay-ui:error key="disk-space" message="disk-space" />
<aui:form action="<%=uploadFileURL%>" enctype="multipart/form-data"
    method="post">
    <aui:input name="partnerCode" id="partnerCode" type="hidden" value="<%=partnerCode %>"/>
    <aui:input type="file" name="fileupload" inlineField="true"
        label="file">
        <aui:validator name="acceptFiles">'csv,xls'</aui:validator>
    </aui:input>
    <aui:button name="upload" type="submit" value="import" />
</aui:form>
<c:if test="<%=remoteData != null%>">
	<aui:nav-bar>
		<aui:nav>
			<aui:nav-item id="saveBtn" iconCssClass="icon-save" label="save"
				selected='<%="save".equals(toolbarItem)%>' />
		</aui:nav>
	</aui:nav-bar>
	<div class="content" id="myDataTable"></div>
	<div id="pagination" class="aui-pagination aui-pagination-centered">
		<ul class="aui-pagination-content">
			<li><a href="#">&larr;</a></li>
			<%
			    pages = remoteData.length() / itemsPerPage;
			    if (remoteData.length() % itemsPerPage > 0) {
			        pages++;
			    }
			    for (int i = 1; i < pages + 1; i++) {
			%>
			<li><a href="#"><%=i%></a></li>
			<%
			    }
			%>
			<li><a href="#">&rarr;</a></li>
		</ul>
	</div>
</c:if>

<aui:script>
var saveURL='<%=saveURL.toString()%>';
var itemsPerPage = <%= itemsPerPage %>;
var pages = <%= pages %>;
var remoteData = <%=remoteData %>;

AUI().ready(function(A){
	if (A.one('#myDataTable')) {
		var pagedItems = chunkArray(remoteData, itemsPerPage);
		console.log(pagedItems);
		AUI().use('aui-base', 'aui-datatable', 'aui-datatype', 'datatable-sort', 'datatable-mutable', 'aui-pagination', function(A) {
		    var nestedCols = [{
		        className: 'hidden',
		        key: 'associato',
		        label: 'associato'
		    }, {
		        editor: new A.TextCellEditor({
		            inputFormatter: A.DataType.String.evaluate,
		            validator: {
		                rules: {
		                    value: {
		                        required: true
		                    }
		                }
		            }
		        }),
		        key: 'codiceArticoloSocio',
		        label: 'Articolo Socio'
		    }, {
		        editor: new A.TextCellEditor(),
		        key: 'codiceVarianteSocio',
		        label: 'Variante Socio'
		    }, {
		        editor: new A.TextCellEditor({
		            inputFormatter: A.DataType.String.evaluate,
		            validator: {
		                rules: {
		                    value: {
		                        required: true
		                    }
		                }
		            }
		        }),
		        className: 'text-success',
		        key: 'codiceArticolo',
		        label: 'Articolo'
		    }, {
		        editor: new A.TextCellEditor(),
		        className: 'text-success',
		        key: 'codiceVariante',
		        label: 'Variante'
		    }];
		    
		    var dataTable = new A.DataTable({
		        columns: nestedCols,
		        data: pagedItems[0],
		        editEvent: 'click',
		        plugins: [{
		            cfg: {
		                highlightRange: false
		            },
		            fn: A.Plugin.DataTableHighlight
		        }]
		    }).render('#myDataTable');
		    dataTable.get('boundingBox').unselectable();
		    
		    var p = new A.Pagination({
		        contentBox: '#pagination .aui-pagination-content',
		        page: 1,
		        on: {
		          changeRequest: function(event) {
		        	  dataTable.set('data', pagedItems[event.state.page - 1]);
		          }
		        }
		      }).render();
		    
		    dataTable.after('*:codiceArticoloSocioChange', function (e) {
		    	pagedItems[p.lastState.page - 1] = dataTable.data.toJSON();
            });
            dataTable.after('*:codiceVarianteSocioChange', function (e) {
            	pagedItems[p.lastState.page - 1] = dataTable.data.toJSON();
            });
            dataTable.after('*:codiceArticoloChange', function (e) {
            	pagedItems[p.lastState.page - 1] = dataTable.data.toJSON();
            });
            dataTable.after('*:codiceVarianteChange', function (e) {
            	pagedItems[p.lastState.page - 1] = dataTable.data.toJSON();
            });
		});
		
		A.one('#<portlet:namespace />saveBtn').on('click', function(e) {
			sendData(pagedItems);
		});
	}
});

function sendData(data) {
	AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
        A.io.request(saveURL, {
            method: 'POST',
            data: {
            	<portlet:namespace />items: JSON.stringify([].concat.apply([], data))
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                    var data = JSON.parse(this.get('responseData'));
                    if (data.code === 0){
                        alert('Salvato correttamente.');
                        
                    }
                    modal.hide();
                },
                error: function () {
                    modal.hide();
                }
            }
        });
    });
	
}

/**
 * Returns an array with arrays of the given size.
 *
 * @param myArray {Array} array to split
 * @param chunk_size {Integer} Size of every group
 */
function chunkArray(myArray, chunk_size){
    var index = 0;
    var arrayLength = myArray.length;
    var tempArray = [];
    
    for (index = 0; index < arrayLength; index += chunk_size) {
        myChunk = myArray.slice(index, index+chunk_size);
        // Do something if you want with the group
        tempArray.push(myChunk);
    }

    return tempArray;
}

</aui:script>