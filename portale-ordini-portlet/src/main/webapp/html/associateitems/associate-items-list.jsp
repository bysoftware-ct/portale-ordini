<%@page import="it.bysoftware.ct.service.CategorieMerceologicheLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.CategorieMerceologiche"%>
<%@page import="it.bysoftware.ct.service.PianteLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Piante"%>
<%@page import="it.bysoftware.ct.utils.ItemComparator"%>
<%@page import="com.liferay.portal.kernel.dao.search.DisplayTerms"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page
	import="it.bysoftware.ct.service.persistence.DescrizioniVariantiPK"%>
<%@page
	import="it.bysoftware.ct.service.DescrizioniVariantiLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.DescrizioniVarianti"%>
<%@page import="it.bysoftware.ct.service.ArticoliLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Articoli"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.liferay.portal.service.ClassNameLocalServiceUtil"%>
<%@page
	import="com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil"%>
<%@page import="com.liferay.portlet.expando.model.ExpandoValue"%>
<%@page import="it.bysoftware.ct.model.ArticoloSocio"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.service.ArticoloSocioLocalServiceUtil"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="com.liferay.portal.model.Role"%>
<%@page import="java.util.List"%>
<%@ include file="../init.jsp"%>

<%
    String toolbarItem = ParamUtil.getString(renderRequest,
            "toolbarItem", "view-all");
	String itemCode = ParamUtil.getString(request, "itemCode");
	String itemVariant = ParamUtil.getString(request, "itemVariant");
	String itemDesc = ParamUtil.getString(request, "itemDesc");
	String partnerItemCode = ParamUtil.getString(request,
	        "partnerItemCode");
	String partnerItemVariant = ParamUtil.getString(request,
	        "partnerItemVariant");
    String backUrl = ParamUtil.getString(renderRequest, "backUrl");
    String currentURL = PortalUtil.getCurrentURL(request);
    boolean isCompany = UserLocalServiceUtil.hasRoleUser(
            themeDisplay.getCompanyId(), Constants.COMPANY,
            themeDisplay.getUserId(), false);
    String partnerCode = ParamUtil.getString(renderRequest,
            "partnerCode", null);
    if (!isCompany) {
        ExpandoValue value = ExpandoValueLocalServiceUtil.getValue(
                user.getCompanyId(),
                ClassNameLocalServiceUtil.getClassNameId(User.class),
                "CUSTOM_FIELDS", Constants.REGISTRY_CODE,
                user.getUserId());
        partnerCode = value.getString();
    }
//     List<ArticoloSocio> list = new ArrayList<ArticoloSocio>();
    String title = LanguageUtil.get(portletConfig, user.getLocale(), "partner-items-list-x");
    String companyName = "";
    if (partnerCode != null) {
//         list = ArticoloSocioLocalServiceUtil
//                 .findAllPartnerItems(partnerCode);
        AnagraficheClientiFornitori partner = AnagraficheClientiFornitoriLocalServiceUtil.
                getAnagraficheClientiFornitori(partnerCode);
        companyName = partner.getRagioneSociale1() + " " + partner.getRagioneSociale2();
        title = LanguageUtil.format(user.getLocale(), title, new Object[] { "- " + companyName });
    }
%>

<liferay-ui:header backURL="<%=backUrl%>" title="<%=title %>" />

<liferay-portlet:renderURL var="uploadURL">
	<portlet:param name="mvcPath" value="/html/associateitems/upload.jsp" />
	<portlet:param name="backUrl" value="<%=currentURL%>" />
	<portlet:param name="partnerCode" value="<%=partnerCode%>" />
</liferay-portlet:renderURL>

<liferay-portlet:renderURL var="newAssociationURL">
    <portlet:param name="mvcPath" value="/html/associateitems/edit-association.jsp" />
    <portlet:param name="backUrl" value="<%=currentURL%>" />
    <portlet:param name="partnerCode" value="<%=partnerCode%>" />
    <portlet:param name="companyName" value="<%=companyName %>" />
    <portlet:param name="new" value="<%= String.valueOf(true) %>" />
</liferay-portlet:renderURL>

<liferay-portlet:renderURL varImpl="searchURL">
    <portlet:param name="backUrl" value="<%=backUrl%>" />
    <portlet:param name="partnerCode" value="<%=partnerCode%>" />
    <portlet:param name="mvcPath" value="/html/associateitems/associate-items-list.jsp" />
</liferay-portlet:renderURL>

<aui:form action="<%=searchURL %>" method="post" name="orderForm">
    <liferay-portlet:renderURLParams varImpl="searchURL" />
	<liferay-portlet:renderURL varImpl="iteratorURL">
	    <portlet:param name="backUrl" value="<%=backUrl%>" />
		<portlet:param name="partnerCode" value="<%=partnerCode%>" />
		<portlet:param name="itemCode" value="<%=itemCode%>" />
		<portlet:param name="itemVariant" value="<%=itemVariant%>" />
		<portlet:param name="partnerItemCode" value="<%=partnerItemCode%>" />
		<portlet:param name="partnerItemVariant" value="<%=partnerItemVariant%>" />
		<portlet:param name="mvcPath"
			value="/html/associateitems/associate-items-list.jsp" />
	</liferay-portlet:renderURL>
	<liferay-ui:search-container delta="20" displayTerms="<%= new DisplayTerms(renderRequest) %>" 
	    emptyResultsMessage="no-partner-items" headerNames="code,name,code-variant,variant,parnter-code, parnter-variant" iteratorURL="<%=iteratorURL%>">
	    <aui:nav-bar>
	        <c:if test="<%=isCompany %>">
		        <aui:nav>
		           <aui:nav-item id="addBtn" iconCssClass="icon-plus" label="add"
		               selected='<%="add".equals(toolbarItem)%>' href="<%=newAssociationURL %>"/>
		           <aui:nav-item id="importBtn" iconCssClass="icon-upload" label="import"
		               selected='<%="import".equals(toolbarItem)%>' href="<%=uploadURL %>" />
		        </aui:nav>
	        </c:if>
	        <aui:nav-bar-search cssClass="pull-right" >
	            <liferay-ui:search-form page="/html/associateitems/search.jsp" servletContext="<%= application %>"/>
	        </aui:nav-bar-search>
	    </aui:nav-bar>
		<liferay-ui:search-container-results>
			<%
			    DisplayTerms displayTerms = searchContainer.getDisplayTerms();
			    List<ArticoloSocio> list = new ArrayList<ArticoloSocio>();
	            if (displayTerms.isAdvancedSearch()) {
	                list = ArticoloSocioLocalServiceUtil.findPartnerItems(partnerCode, itemCode, itemVariant,
	                        partnerItemCode, partnerItemVariant, displayTerms.isAndOperator(), 0,
	                        ArticoloSocioLocalServiceUtil.findPartnerItemsCount(partnerCode, itemCode, itemVariant,
	                                partnerItemCode, partnerItemVariant, displayTerms.isAndOperator()), new ItemComparator());
	            } else {
	                String searchkeywords = displayTerms.getKeywords();
	                String numbesearchkeywords = String.valueOf(0);
	                if (Validator.isNumber(searchkeywords)) {
	                    numbesearchkeywords = searchkeywords;
	                }
	                list = ArticoloSocioLocalServiceUtil.findPartnerItems(partnerCode, searchkeywords, searchkeywords,
	                        searchkeywords, searchkeywords, false, 0, ArticoloSocioLocalServiceUtil.findPartnerItemsCount(partnerCode, searchkeywords, searchkeywords,
	                                searchkeywords, searchkeywords, false), new ItemComparator());  
	            }
			    results = ListUtil.subList(list,
			                    searchContainer.getStart(),
			                    searchContainer.getEnd());
			            total = list.size();
	
			            pageContext.setAttribute("results", results);
			            pageContext.setAttribute("total", total);
			%>
		</liferay-ui:search-container-results>
		<liferay-ui:search-container-row
			className="it.bysoftware.ct.model.ArticoloSocio"
			modelVar="partnerItem">
			<%--        <liferay-portlet:renderURL varImpl="rowURL"> --%>
			<%--             <portlet:param name="backURL" value="<%= currentURL %>" /> --%>
			<%--             <portlet:param name="mvcPath" value="/html/associateitems/edit-association.jsp" /> --%>
			<%--             <portlet:param name="partnerCode" value="<%= partnerItem.getAssociato() %>" /> --%>
			<%--             <portlet:param name="partnerItemCode" value="<%= partnerItem.getCodiceArticoloSocio() %>" /> --%>
			<%--             <portlet:param name="partnerVariantCode" value="<%= partnerItem.getCodiceVarianteSocio() %>" /> --%>
			<%--         </liferay-portlet:renderURL> --%>
			<liferay-ui:search-container-column-text property="codiceArticolo"
				name="code" />
			<%
			    Articoli item = ArticoliLocalServiceUtil
			                    .fetchArticoli(partnerItem.getCodiceArticolo());
			    Piante p = PianteLocalServiceUtil.findPlants(partnerItem.getCodiceArticolo());
			    String shape = "";
			    if (item != null) {
			        CategorieMerceologiche c = CategorieMerceologicheLocalServiceUtil.fetchCategorieMerceologiche(item.getCategoriaMerceologica());
                    if (c != null) {
                        shape = c.getDescrizione();
                    }
			    }
			%>
			<liferay-ui:search-container-column-text name="name"
				value="<%=item != null ? item.getDescrizione() : ""%>" />
			<liferay-ui:search-container-column-text name="shape" value="<%=shape %>"/>
            <liferay-ui:search-container-column-text name="vase" value="<%=p != null ? p.getVaso() : ""%>" />
            <liferay-ui:search-container-column-text name="H" value="<%=p != null ? "" + p.getAltezzaPianta() : ""%>"/>
			<liferay-ui:search-container-column-text property="codiceVariante"
				name="code-variant" />
			<%
			    DescrizioniVarianti variant = DescrizioniVariantiLocalServiceUtil
			                    .fetchDescrizioniVarianti(new DescrizioniVariantiPK(
			                            partnerItem.getCodiceArticolo(),
			                            partnerItem.getCodiceVariante()));
			%>
			<liferay-ui:search-container-column-text name="variant"
				value="<%=variant != null ? variant.getDescrizione() : ""%>" />
			<liferay-ui:search-container-column-text
				property="codiceArticoloSocio" name="parnter-code" />
			<liferay-ui:search-container-column-text
				property="codiceVarianteSocio" name="parnter-variant" />
			<c:if test="<%= isCompany%>">
			    <liferay-ui:search-container-row-parameter name="backUrl"
	                value="<%= iteratorURL.toString() %>" />
	            <liferay-ui:search-container-row-parameter name="companyName"
	                value="<%= companyName%>" />
			    <liferay-ui:search-container-column-jsp path="/html/associateitems/associate-items-list-action.jsp" />
			</c:if>
		</liferay-ui:search-container-row>
		<liferay-ui:search-iterator searchContainer="<%=searchContainer%>" />
	</liferay-ui:search-container>
</aui:form>
<%!private static Log _log = LogFactoryUtil
            .getLog("docroot.html.associateitems.view_jsp");%>