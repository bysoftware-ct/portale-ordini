<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.utils.ResourceId"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="it.bysoftware.ct.service.CategorieMerceologicheLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.CategorieMerceologiche"%>
<%@page import="it.bysoftware.ct.service.PianteLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Piante"%>
<%@page import="java.util.List"%>
<%@page
	import="it.bysoftware.ct.service.persistence.DescrizioniVariantiPK"%>
<%@page
	import="it.bysoftware.ct.service.DescrizioniVariantiLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.DescrizioniVarianti"%>
<%@page import="it.bysoftware.ct.service.ArticoliLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Articoli"%>
<%@page import="it.bysoftware.ct.service.persistence.ArticoloSocioPK"%>
<%@page import="it.bysoftware.ct.service.ArticoloSocioLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.ArticoloSocio"%>
<%@ include file="../init.jsp"%>
<%
    String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
    String backUrl = ParamUtil.getString(renderRequest, "backUrl");
    boolean add = ParamUtil.getBoolean(renderRequest, "new");
    String partnerCode = ParamUtil.getString(renderRequest,
            "partnerCode");
    String partnerItemCode = ParamUtil.getString(renderRequest,
            "partnerItemCode");
    String partnerItemVariant = ParamUtil.getString(renderRequest,
            "partnerItemVariant");
    String companyName = ParamUtil.getString(renderRequest,
            "companyName");
    Piante plant = null;
    Articoli item = null;
    DescrizioniVarianti variant = null;
    List<DescrizioniVarianti> variants = new ArrayList<DescrizioniVarianti>();
    List<Piante> plants = null;
    String title = "";
    if (!add) {
        ArticoloSocio partnerItem = ArticoloSocioLocalServiceUtil
                .getArticoloSocio(new ArticoloSocioPK(partnerCode,
                        partnerItemCode, partnerItemVariant));
    
        item = ArticoliLocalServiceUtil.fetchArticoli(partnerItem
                .getCodiceArticolo());
        List<CategorieMerceologiche> shapes = CategorieMerceologicheLocalServiceUtil.
                getCategorieMerceologiches(0, CategorieMerceologicheLocalServiceUtil.
                        getCategorieMerceologichesCount());
        if (item != null){
            plant = PianteLocalServiceUtil.findPlants(item.getCodiceArticolo());
        }
        variant = DescrizioniVariantiLocalServiceUtil .fetchDescrizioniVarianti(new DescrizioniVariantiPK(
                        partnerItem.getCodiceArticolo(), partnerItem
                                .getCodiceVariante()));
    
        variants = DescrizioniVariantiLocalServiceUtil.findVariants(item.getCodiceArticolo());
        title= LanguageUtil.get(portletConfig, user.getLocale(), "edit-item-association-x");
    } else {
        title= LanguageUtil.get(portletConfig, user.getLocale(), "new-item-association-x");
    }
    title = LanguageUtil.format(user.getLocale(), title, new Object[] { "- " + companyName });
%>

<liferay-portlet:renderURL var="itemURL"
	windowState="<%=LiferayWindowState.POP_UP.toString()%>">
	<liferay-portlet:param name="search" value="true" />
	<liferay-portlet:param name="vase" value="<%=plant != null ? plant.getVaso() : "" %>" />
    <liferay-portlet:param name="height" value="<%=plant != null ? String.valueOf(plant.getAltezzaPianta()) : "" %>" />
	<liferay-portlet:param name="suppliers" value="<%=plant != null ? String.valueOf(plant.getIdFornitore()) : partnerCode %>" />
	<liferay-portlet:param name="disableSuppliers" value="true" />
	<liferay-portlet:param name="mvcPath" value="/html/items/view.jsp" />
</liferay-portlet:renderURL>
<portlet:resourceURL id="<%=ResourceId.autoComplete.toString() %>" var="getPlants" />
<portlet:resourceURL id="<%=ResourceId.findAssociation.toString() %>" var="findAssociation" />
<portlet:resourceURL id="<%=ResourceId.editAssociation.toString() %>" var="editAssociation" />
<liferay-ui:header backURL="<%=backUrl%>" title="<%=title %>" />
<span id="group-error-block" ></span>
<span id="group-success-block" ></span>
<aui:layout>
	<aui:column columnWidth="30" first="true">
	   <c:if test="<%=add %>">
		   <aui:fieldset label="partner-item">
			  <aui:input name="partnerItemCode" label="parnter-code"
			     value="<%=partnerItemCode%>" />
	          <aui:input name="partnerItemVariant" label="parnter-variant"
	             value="<%=partnerItemVariant%>" />
		   </aui:fieldset>
	   </c:if>
	   <c:if test="<%=!add %>">
           <aui:fieldset label="partner-item">
              <aui:input name="partnerItemCode" label="parnter-code" disabled="true"
                 value="<%=partnerItemCode%>" />
              <aui:input name="partnerItemVariant" label="parnter-variant" disabled="true"
                 value="<%=partnerItemVariant%>" />
           </aui:fieldset>
       </c:if>
	</aui:column>
	<aui:column columnWidth="70" last="true">
	   <aui:fieldset label="new-item">
            <aui:nav-bar>
                <aui:nav>
                    <c:if test="<%=add %>">
                        <aui:nav-item id="saveBtn" cssClass="hidden" iconCssClass="icon-save" label="save" disabled="true"
                            selected='<%="save".equals(toolbarItem)%>' />
                    </c:if>
                    <c:if test="<%=!add %>">
                        <aui:nav-item id="saveBtn" iconCssClass="icon-save" label="save" disabled="true"
                            selected='<%="save".equals(toolbarItem)%>' />
                    </c:if>
                    <aui:nav-item id="searchBtn" iconCssClass="icon-search" label="search"
                        selected='<%="search".equals(toolbarItem)%>' />
                </aui:nav>
            </aui:nav-bar>
            <span id="group-error-block" ></span>
            <aui:input type="text" name="code" label="code" value="<%=item != null ? item.getCodiceArticolo() : ""%>" inlineField="true" cssClass="input-small" disabled="true"/>
            <aui:input name="description" label="description" value="<%=item != null ? item.getDescrizione() : ""%>" inlineField="true" cssClass="input-xlarge" disabled="true"/>
            <aui:input name="oldVariantCode" label="" value="<%=!add && variant != null ? variant.getCodiceVariante() : "NULL"%>" type="hidden"/>
            <aui:select id="variants" name="variants" label="variant" >
                <aui:option label="select-variant" value="" />
                <%
                    if (Validator.isNotNull(variants)) {
                        for (DescrizioniVarianti v : variants) {
                            if (variant != null
                                    && variant.getCodiceVariante().equals(v.getCodiceVariante())
                                    && variant.getCodiceArticolo().equals(item.getCodiceArticolo())) {
                %>
                                    <aui:option label="<%=v.getDescrizione()%>"
                                        value="<%=v.getCodiceVariante()%>" selected="true" />
                <%
                            } else {
                %>
                                    <aui:option label="<%=v.getDescrizione()%>"
                                        value="<%=v.getCodiceVariante()%>" />
                <%
                            }
                        }
                    }
                %>
            </aui:select>
	        <aui:fieldset label="item-details">
	            <%
	                CategorieMerceologiche shape = null;
	                if (item != null) {
	                    shape = CategorieMerceologicheLocalServiceUtil.fetchCategorieMerceologiche(item.getCategoriaMerceologica());
	                }
	            %>
	            <aui:input name="shape" value="<%=shape != null ? shape.getDescrizione() : ""%>" disabled="true" inlineField="true"/>
	            <aui:input name="vase"
	                value="<%=plant != null ? plant.getVaso() : ""%>" inlineField="true" cssClass="input-small" disabled="true"/>
	            <aui:input name="height"
	                value="<%=plant != null ? plant.getAltezzaPianta() : ""%>" inlineField="true" cssClass="input-small" disabled="true"/>
	        </aui:fieldset>
			<aui:script>
			AUI().use('aui-base', 'aui-io-request', 'aui-modal', function(A) {
				var errorBlock=A.one('#group-error-block');
	            var successBlock=A.one('#group-success-block');
			    A.one('#<portlet:namespace/>searchBtn').on('click', function() {
			        Liferay.Util.openWindow({
			            dialog : {
			                centered : true,
			                modal : true
			            },
			            id : '<portlet:namespace/>itemDialog',
			            title : 'Cerca Prodotto',
			            uri : '<%=itemURL%>'
			        });
			    });
			    A.one('#<portlet:namespace/>variants').on('change', function() {
			        if (<%= !add%>) {
			        	A.one('#<portlet:namespace/>saveBtn').removeClass('hidden');
			        }
			    });
			    A.one('#<portlet:namespace/>saveBtn').on('click', function() {
			    	var partnerItemCode = A.one('#<portlet:namespace/>partnerItemCode').get('value');
			    	var partnerItemVariant = A.one('#<portlet:namespace/>partnerItemVariant').get('value');
			    	console.log(partnerItemCode);
			    	console.log(partnerItemVariant);
			    	var modal;
                    A.io.request('<%=editAssociation %>', {
                    	method: 'POST',
                        data : {
                            <portlet:namespace />add : '<%=add%>',
                            <portlet:namespace />partnerCode : '<%=partnerCode%>',
                            <portlet:namespace />itemCode : A.one('#<portlet:namespace/>code').get('value'),
                            <portlet:namespace />variants : A.one('#<portlet:namespace/>variants').get('value'),
                            <portlet:namespace />oldVariantCode : A.one('#<portlet:namespace/>oldVariantCode').get('value'),
                            <portlet:namespace />partnerItemCode : partnerItemCode,//A.one('#<portlet:namespace/>partnerItemCode').get('value'),
                            <portlet:namespace />partnerItemVariant : partnerItemVariant //A.one('#<portlet:namespace/>partnerItemVariant').get('value'),
                        },
                        on: {
                            start: function() {
                                modal = new A.Modal({
                                    bodyContent : '<div class="loading-animation"/>',
                                    centered : true,
                                    headerContent : '<h3>Loading...</h3>',
                                    modal : true,
                                    render : '#modal',
                                    close: false,
                                    zIndex: 99999,
                                    width : 450
                                }).render();
                            },
                            success: function () {
                                var data = JSON.parse(this.get('responseData'));
                                console.log(data);
                                switch (data.code) {
	                                case 0:
		                                if (successBlock != null) {
		                                    successBlock.empty();
		                                }
		                                var obj = JSON.parse(data.data);
		                                var messageNode = A.Node.create('<div id="test" class="portlet-msg-success">' 
		                                		+ 'Associazione dell\'articolo: ' + obj.codiceArticolo + ' modificata con successo</div>');
		                                messageNode.appendTo(successBlock);
		                                setTimeout(function(){
		                                    A.one("#test").toggle();
		                                }, 5000);
		                                break;
	                                case 6:
	                                	var errorMessageNode = A.Node.create('<div class="portlet-msg-error">' 
	                                            + 'Si e\' verificato un errore nel salvataggio dei dati: ' + data.message + '</div>');
	                                	errorMessageNode.appendTo(errorBlock);
	                                	break;
                                }
                                modal.hide();
                            },
                            error: function name() {
                                modal.hide();
                            }
                        }
                    });
                });
                Liferay.provide(window, 'closePopup', function(data, dialogId) {
		        console.log(data);
		        var dialog = Liferay.Util.Window.getById(dialogId);
		        dialog.hide();
		        var modal;
		        switch (dialogId) {
		            case '<portlet:namespace/>itemDialog':
		                A.one('#<portlet:namespace/>code').set('value', data);
		                A.io.request('<%=getPlants %>', {
		                    method: 'POST',
		                    data : {
		                        <portlet:namespace />itemCode : A.one('#<portlet:namespace/>code').get('value'),
		//                       <portlet:namespace />itemName : itemName,
		                        <portlet:namespace />supplier : '',
		                        <portlet:namespace />category : '-1',
		                        <portlet:namespace />onlyAvailable : false
		                    },
		                    on: {
		                        start: function() {
		                            modal = new A.Modal({
		                                bodyContent : '<div class="loading-animation"/>',
		                                centered : true,
		                                headerContent : '<h3>Loading...</h3>',
		                                modal : true,
		                                render : '#modal',
		                                close: false,
		                                zIndex: 99999,
		                                width : 450
		                            }).render();
		                        },
		                        success: function () {
		                            var data = JSON.parse(this.get('responseData'));
		                            console.log(data);
		                            if (data.length > 0) {
		                            	updateFields(data[0]);
		                            }
		                            A.one('#<portlet:namespace/>saveBtn').removeClass('hidden');
		                            modal.hide();
		                        },
		                        error: function name() {
		                            modal.hide();
		                        }
		                    }
		                });
		                break;
			        }
			    });
			});
			function updateFields (result) {
			    AUI().use('aui-base', 'aui-io-request', 'aui-modal', function(A) {
			        A.one('#<portlet:namespace/>description').val(result.nome);
			        A.one('#<portlet:namespace/>height').val(result.altezza);
			        A.one('#<portlet:namespace/>vase').val(result.vaso);
			        A.one('#<portlet:namespace/>shape').val(result.dettagliPianta.categoriaMerceologica);
			        var oldVariant = A.one('#<portlet:namespace/>variants').get('value');
			        var options = '<option value="" selected>Seleziona variante...</option>';
			        var modal;
			        for (var i = 0; i < result.varianti.length; i++) {
			            var variant = result.varianti[i];
			            options += '<option value="' + variant.codiceVariante + '"';
			            if (variant.codiceVariante === oldVariant) {
			                options += 'selected="true"';
			            }
			            options += '>' + variant.descrizione + '</option>';
			        }
			        A.one('#<portlet:namespace/>variants').setHTML(options);
			        A.one('#<portlet:namespace/>variants').set('value', oldVariant);
			        if (<%=add%>) {
			        	A.io.request('<%=findAssociation %>', {
	                        method: 'POST',
	                        data : {
	                            <portlet:namespace />itemCode : A.one('#<portlet:namespace/>code').get('value'),
	                            <portlet:namespace />itemVariant : A.one('#<portlet:namespace/>variants').get('value'),
	                            <portlet:namespace />partnercode : '<%=partnerCode%>',
	                        },
	                        on: {
	                            start: function() {
	                                modal = new A.Modal({
	                                    bodyContent : '<div class="loading-animation"/>',
	                                    centered : true,
	                                    headerContent : '<h3>Loading...</h3>',
	                                    modal : true,
	                                    render : '#modal',
	                                    close: false,
	                                    zIndex: 99999,
	                                    width : 450
	                                }).render();
	                            },
	                            success: function () {
	                                var data = JSON.parse(this.get('responseData'));
	                                console.log(data);
	                                modal.hide();
	                                if (data.data === null) {
	                                    A.one('#<portlet:namespace/>saveBtn').removeClass('not-exist');
	                                    A.one('#<portlet:namespace/>description').addClass('not-exist');
	                                    A.one('#<portlet:namespace/>height').addClass('not-exist');
	                                    A.one('#<portlet:namespace/>vase').addClass('not-exist');
	                                    A.one('#<portlet:namespace/>shape').addClass('not-exist')
	                                } else {
	                                	var errorBlock=A.one('#group-error-block');
	                                	if (errorBlock != null) {
	                                		errorBlock.empty();
	                                    }
	                                    var messageNode = A.Node.create('<div id="test" class="portlet-msg-error">' 
	                                            + data.message + '</div>');
	                                    messageNode.appendTo(errorBlock);
	                                    setTimeout(function(){
	                                        A.one("#test").toggle();
	                                    }, 5000);
	                                    A.one('#<portlet:namespace/>description').addClass('exist');
	                                    A.one('#<portlet:namespace/>height').addClass('exist');
	                                    A.one('#<portlet:namespace/>vase').addClass('exist');
	                                    A.one('#<portlet:namespace/>shape').addClass('exist');
	                                }
	                            },
	                            error: function name() {
	                                modal.hide();
	                            }
	                        }
	                    });
			        }
			    });
			}
			</aui:script>
	   </aui:fieldset>
	</aui:column>
</aui:layout>
