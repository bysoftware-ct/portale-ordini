<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="it.bysoftware.ct.model.MovimentoVuoto"%>
<%@page import="it.bysoftware.ct.service.MovimentoVuotoLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="it.bysoftware.ct.service.VuotoLocalServiceUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.model.Vuoto"%>
<%@page import="java.util.List"%>
<%@include file="../init.jsp"%>

<%
    String toolbarItem = ParamUtil.getString(renderRequest,
            "toolbarItem", "view-all");
    String backUrl = ParamUtil.getString(renderRequest, "backUrl");
    String prevUrl = ParamUtil.getString(renderRequest, "prevUrl");
    String partnerCode = ParamUtil.getString(renderRequest,
            "partnerCode");
    AnagraficheClientiFornitori supplier = AnagraficheClientiFornitoriLocalServiceUtil
            .getAnagraficheClientiFornitori(partnerCode);
    long dateLng = ParamUtil.getLong(renderRequest, "date", 0);
    Calendar calendar = Calendar.getInstance();
    if (dateLng > 0) {
        calendar.setTimeInMillis(dateLng);
    } else {
        calendar = Utils.today();
    }

    Date date = calendar.getTime();

    String title = LanguageUtil.get(portletConfig, user.getLocale(),
            "partner-movements-list-x");
//     SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    title = LanguageUtil.format(user.getLocale(), title,
            new Object[] { " " + supplier.getRagioneSociale1() });
    
    List<Vuoto> returnables = VuotoLocalServiceUtil.getVuotos(0,
            VuotoLocalServiceUtil.getVuotosCount());
    Map<String, List<MovimentoVuoto>> gave = new HashMap<String, List<MovimentoVuoto>>();
    Map<String, List<MovimentoVuoto>> got = new HashMap<String, List<MovimentoVuoto>>();
    for (Vuoto returnable : returnables) {
        gave.put(returnable.getCodiceVuoto(), MovimentoVuotoLocalServiceUtil.findMovementiByVuotoSoggettoDataTipo(
                returnable.getCodiceVuoto(), partnerCode, calendar.getTime(), 1));
        got.put(returnable.getCodiceVuoto(), MovimentoVuotoLocalServiceUtil.findMovementiByVuotoSoggettoDataTipo(
                returnable.getCodiceVuoto(), partnerCode, calendar.getTime(), -1));
    }
    int balance = 0;
%>

<liferay-portlet:renderURL varImpl="reloadURL">
    <portlet:param name="backUrl" value="<%=backUrl %>"/>
    <portlet:param name="prevUrl" value="<%=prevUrl %>"/>
    <portlet:param name="partnerCode" value="<%=partnerCode %>"/>
    <portlet:param name="mvcPath" value="/html/returnablemanagement/search-movement.jsp" />
</liferay-portlet:renderURL>
<liferay-portlet:renderURL varImpl="back">
    <portlet:param name="backUrl" value="<%=prevUrl %>"/>
    <portlet:param name="partnerCode" value="<%=partnerCode %>"/>
    <portlet:param name="mvcPath" value="/html/returnablemanagement/view-movements-summary.jsp" />
</liferay-portlet:renderURL>
<liferay-ui:header backURL="<%=back.toString()%>" title="<%=title%>" />
<liferay-portlet:renderURL varImpl="iteratorURL">
<%--     <portlet:param name="code" value="<%=code %>"/> --%>
    <portlet:param name="partnerCode" value="<%=partnerCode%>" />
    <portlet:param name="mvcPath"
        value="/html/returnablemanagement/search-movements.jsp" />
</liferay-portlet:renderURL>

<aui:nav-bar>
    <aui:nav-bar-search cssClass="pull-right" >
        <liferay-ui:input-date name="date" cssClass="input-small"
            dayValue="<%= calendar.get(Calendar.DAY_OF_MONTH) %>" dayParam="day"
            monthValue="<%= calendar.get(Calendar.MONTH) %>" monthParam="month"
            yearValue="<%= calendar.get(Calendar.YEAR) %>" yearParam="year" />
            <aui:button name="searchBtn" type="button" value="search" last="true" />
    </aui:nav-bar-search>
</aui:nav-bar>

<aui:layout>
    <aui:column columnWidth="50" first="true">
        <liferay-ui:search-container delta="20" emptyResultsMessage="no-movements-x" iteratorURL="<%=iteratorURL%>">
		    <liferay-ui:search-container-results>
		        <%
		            results = ListUtil.subList(returnables,
		                            searchContainer.getStart(),
		                            searchContainer.getEnd());
		                    total = returnables.size();
		                    pageContext.setAttribute("results", results);
		                    pageContext.setAttribute("total", total);
		        %>
		    </liferay-ui:search-container-results>
		    <liferay-ui:search-container-row
		        className="it.bysoftware.ct.model.Vuoto" modelVar="returnable">
		        <%
		            double q = 0;
		            List<MovimentoVuoto> moves = gave.get(returnable.getCodiceVuoto());
		            for (MovimentoVuoto move : moves) {
		                q += move.getQuantita();
		            }
		        %>
		        <%
		            String incLbl = LanguageUtil.get(portletConfig, user.getLocale(),
		                        "incoming-x");
		            String incName = LanguageUtil.format(user.getLocale(), incLbl,
		                        new Object[] { "", supplier.getRagioneSociale1() });
		        %>
		        <liferay-ui:search-container-column-text name="item" property="nome"/>
	            <liferay-ui:search-container-column-text name="<%= incName %>" cssClass="text-success"
	                value="<%=(q != 0) ? String.valueOf((int)q) : "///"%>" />
<%-- 	            <c:choose> --%>
<%--                     <c:when test="<%= moves.size() > 0 %>"> --%>
<%--                         <liferay-ui:search-container-column-text > --%>
<%--                             <liferay-ui:icon id="<%= "gave_" + returnable.getCodiceVuoto() %>" cssClass="details-gave" image="add" toolTip="false" /> --%>
<%--                         </liferay-ui:search-container-column-text> --%>
<%--                     </c:when> --%>
<%--                     <c:otherwise> --%>
<%--                        <liferay-ui:search-container-column-text value=""/> --%>
<%--                     </c:otherwise> --%>
<%--                 </c:choose> --%>
		    </liferay-ui:search-container-row>
		    <liferay-ui:search-iterator searchContainer="<%=searchContainer%>" />
		</liferay-ui:search-container>
    </aui:column>
    <aui:column columnWidth="50" last="true">
        <liferay-ui:search-container delta="20" emptyResultsMessage="no-movements-x" iteratorURL="<%=iteratorURL%>">
            <liferay-ui:search-container-results>
                <%
                    results = ListUtil.subList(returnables,
                                    searchContainer.getStart(),
                                    searchContainer.getEnd());
                            total = returnables.size();
                            pageContext.setAttribute("results", results);
                            pageContext.setAttribute("total", total);
                %>
            </liferay-ui:search-container-results>
            <liferay-ui:search-container-row
                className="it.bysoftware.ct.model.Vuoto" modelVar="returnable">
                <%
                    double q = 0;
                    int i = 0;
                    List<MovimentoVuoto> moves = got.get(returnable.getCodiceVuoto());
                    double[] quants = new double[moves.size()];
                    for (MovimentoVuoto move : moves) {
                        q += move.getQuantita();
                        quants[i] = move.getQuantita();
                        i++;
                    }
                    
                %>
                <%
                    String outLbl = LanguageUtil.get(portletConfig, user.getLocale(),
                                "outgoing-x");
                    String outName = LanguageUtil.format(user.getLocale(), outLbl,
                                new Object[] { "", supplier.getRagioneSociale1() });
                %>
                <liferay-ui:search-container-column-text name="item" property="nome"/>
                <liferay-ui:search-container-column-text name="<%= outName %>" cssClass="text-error"
                    value="<%=(q != 0) ? String.valueOf((int)q) : "///"%>" />
<%--                 <c:choose> --%>
<%-- 	                <c:when test="<%= moves.size() > 0 %>"> --%>
<%-- 	                    <liferay-ui:search-container-column-text > --%>
<%-- 	                        <liferay-ui:icon image="add" toolTip="false" onClick="showDetails(<%=quants%>)"/> --%>
<%-- 	                    </liferay-ui:search-container-column-text> --%>
<%-- 	                </c:when> --%>
<%-- 	                <c:otherwise> --%>
<%-- 	                   <liferay-ui:search-container-column-text value=""/> --%>
<%-- 	                </c:otherwise> --%>
<%--                 </c:choose> --%>
            </liferay-ui:search-container-row>
            <liferay-ui:search-iterator searchContainer="<%=searchContainer%>" />
        </liferay-ui:search-container>
    </aui:column>
</aui:layout>

<aui:script>

AUI().use('aui-base', 'datatype-number', function(A) {
	A.all('.details_gave').on('click', function(e) {
		console.log(e);
	});
    A.all('.details_got').on('click', function(e) {
    	console.log(e);
    });
});
	
AUI().use('aui-base', 'datatype-number', function(A) {
    A.one('#<portlet:namespace/>searchBtn').on('click', function() {
        var dateStr = A.one('#<portlet:namespace />date').get('value');
        var date = parseDate(dateStr);
        window.location.href = '<%= reloadURL %>&<portlet:namespace/>date=' + date.getTime();
    });
});
function parseDate(string) {
    var tmp = string.split('/');
    return new Date(tmp[2] + '-' + tmp[1] + '-' + tmp[0]);
}
function showDialog(data) {
	console.log(data);
}
</aui:script>