<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.liferay.portal.service.ClassNameLocalServiceUtil"%>
<%@page
	import="com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil"%>
<%@page import="com.liferay.portlet.expando.model.ExpandoValue"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@include file="../init.jsp"%>

<%
    String currentURL = PortalUtil.getCurrentURL(request);
    boolean isCompany = UserLocalServiceUtil.hasRoleUser(
            themeDisplay.getCompanyId(), Constants.COMPANY,
            themeDisplay.getUserId(), false);

    _log.debug("[/html/associateitems/view.jsp] isCompany: "
            + isCompany + "...");
    ExpandoValue value = ExpandoValueLocalServiceUtil.getValue(
            user.getCompanyId(),
            ClassNameLocalServiceUtil.getClassNameId(User.class),
            "CUSTOM_FIELDS", Constants.REGISTRY_CODE, user.getUserId());
    String partnerCode = value.getString();
%>

<c:if test="<%=isCompany %>">
    <jsp:include page="/html/returnablemanagement/view-suppliers.jsp" />
</c:if>

<c:if test="<%=!isCompany %>">
    <jsp:include page="/html/returnablemanagement/returnable-list.jsp" />
</c:if>

<%!private static Log _log = LogFactoryUtil
            .getLog("docroot.html.returnablemanagement.view_jsp");%>