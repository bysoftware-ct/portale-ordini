<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@include file="../init.jsp"%>
<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    Object[] obj = (Object[]) row.getObject();
    String code = String.valueOf(obj[0].toString());
    String backUrl = (String) row.getParameter("backUrl");
    String partnerCode = (String) row.getParameter("partnerCode");
%>
<liferay-ui:icon-menu>
	<liferay-portlet:renderURL varImpl="viewMovementsURL">
        <portlet:param name="mvcPath"
            value="/html/returnablemanagement/view-movements.jsp" />
        <portlet:param name="backUrl" value="<%=backUrl%>" />
        <portlet:param name="code" value="<%=code%>" />
        <portlet:param name="partnerCode" value="<%=partnerCode%>" />
    </liferay-portlet:renderURL>
    <liferay-ui:icon image="view" url="${viewMovementsURL}" message="view" />
</liferay-ui:icon-menu>