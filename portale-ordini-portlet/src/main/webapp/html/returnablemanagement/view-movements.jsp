<%@page import="it.bysoftware.ct.utils.ResourceId"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="java.util.Date"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="it.bysoftware.ct.service.VuotoLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Vuoto"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="it.bysoftware.ct.service.MovimentoVuotoLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.MovimentoVuoto"%>
<%@page import="java.util.List"%>
<%@include file="../init.jsp"%>

<%
    String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
    String backUrl = ParamUtil.getString(renderRequest, "backUrl");
    String prevUrl = ParamUtil.getString(renderRequest, "prevUrl");
    String partnerCode = ParamUtil.getString(renderRequest,
            "partnerCode");
    String code = ParamUtil.getString(renderRequest, "code", null);
    AnagraficheClientiFornitori supplier = AnagraficheClientiFornitoriLocalServiceUtil
            .getAnagraficheClientiFornitori(partnerCode);
    long fromLng = ParamUtil.getLong(renderRequest, "dateFrom", 0);
    Calendar calendarFrom = Calendar.getInstance();
    if (fromLng > 0) {
        calendarFrom.setTimeInMillis(fromLng);
    } else {
        calendarFrom.set(Calendar.DATE, 1);
        calendarFrom.set(Calendar.HOUR_OF_DAY, 0);
        calendarFrom.set(Calendar.MINUTE, 0);
        calendarFrom.set(Calendar.SECOND, 0);
        calendarFrom.set(Calendar.MILLISECOND, 0);
    }
    
    Date from = calendarFrom.getTime();
    
    Calendar calendarTo = Calendar.getInstance();
    long toLng = ParamUtil.getLong(renderRequest, "dateTo", 0);
    if (toLng > 0) {
        calendarTo.setTimeInMillis(toLng);
    } else {
        calendarTo.set(Calendar.DATE, calendarTo.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendarTo.set(Calendar.HOUR_OF_DAY, 0);
        calendarTo.set(Calendar.MINUTE, 0);
        calendarTo.set(Calendar.SECOND, 0);
        calendarTo.set(Calendar.MILLISECOND, 0);
    }
    Date to = calendarTo.getTime();
//     List<Object[]> summary = MovimentoVuotoLocalServiceUtil.
//             getPartnerReturnablesStock(partnerCode, from, code);
    List<Object[]> summary1 = MovimentoVuotoLocalServiceUtil.
            getPartnerReturnablesStock(partnerCode, from, code);
    Map<String, List<MovimentoVuoto>> map = new HashMap<String, List<MovimentoVuoto>>();
    if (summary1.size() > 0) {
	    for (Object[] obj : summary1) {
	        map.put(obj[0].toString(), supplier.getMovimentiVuoti(from, to, obj[0].toString(),
	                0, MovimentoVuotoLocalServiceUtil.getMovimentoVuotosCount()));
	    }
    } else {
        List<Vuoto> returnables = new ArrayList<Vuoto>();
        if (code == null || code.isEmpty()) {
            returnables = VuotoLocalServiceUtil.getVuotos(0, VuotoLocalServiceUtil.getVuotosCount());
        } else {
            returnables.add(VuotoLocalServiceUtil.getVuoto(code));
        }
        
        for (Vuoto r : returnables) {
            map.put(r.getCodiceVuoto(), supplier.getMovimentiVuoti(from, to, r.getCodiceVuoto(),
                    0, MovimentoVuotoLocalServiceUtil.getMovimentoVuotosCount()));
        }
    }
    String title = LanguageUtil.get(portletConfig, user.getLocale(),
            "partner-movements-list-x");
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    title = LanguageUtil.format(user.getLocale(), title,
            new Object[] { " " + supplier.getRagioneSociale1() });
    int x = 0;

    String msg = LanguageUtil.get(portletConfig, user.getLocale(),
            "no-movements-x");
    if (code != null && !code.isEmpty()) {
        msg = LanguageUtil.format(user.getLocale(), msg,
                new Object[] { VuotoLocalServiceUtil.getVuoto(code).getNome()});
    } else {
        msg = LanguageUtil.format(user.getLocale(), msg,
                new Object[] { "" });
    }
        
    List<Object[]> objs = new ArrayList<Object[]>();
%>
<liferay-portlet:renderURL varImpl="reloadURL">
    <portlet:param name="backUrl" value="<%=backUrl %>"/>
    <portlet:param name="code" value="<%=code %>"/>
    <portlet:param name="partnerCode" value="<%=partnerCode %>"/>
    <portlet:param name="mvcPath" value="/html/returnablemanagement/view-movements.jsp" />
</liferay-portlet:renderURL>
<liferay-portlet:renderURL varImpl="back">
    <portlet:param name="backUrl" value="<%=prevUrl %>"/>
    <portlet:param name="partnerCode" value="<%=partnerCode %>"/>
    <portlet:param name="mvcPath" value="/html/returnablemanagement/view-movements-summary.jsp" />
</liferay-portlet:renderURL>
<liferay-ui:header backURL="<%=back.toString()%>" title="<%=title%>" />
<liferay-portlet:renderURL varImpl="iteratorURL">
    <portlet:param name="code" value="<%=code %>"/>
    <portlet:param name="partnerCode" value="<%=partnerCode%>" />
    <portlet:param name="mvcPath"
        value="/html/returnablemanagement/view-movements.jsp" />
</liferay-portlet:renderURL>
<portlet:resourceURL var="printMovementsURL" 
    id="<%=ResourceId.downloadMovements.name() %>" />
<aui:nav-bar>
    <aui:nav>
        <aui:nav-item id="printBtn" iconCssClass="icon-cloud-download" label="download-recap"
            selected='<%="print".equals(toolbarItem)%>' />
<%--         <aui:nav-item id="sendBtn" iconCssClass="icon-envelope" label="send" --%>
<%--            selected='<%="send".equals(toolbarItem)%>' /> --%>
    </aui:nav>
    <aui:nav-bar-search cssClass="pull-right" >
        <liferay-ui:input-date name="fromDate" cssClass="input-small"
            dayValue="<%= calendarFrom.get(Calendar.DAY_OF_MONTH) %>" dayParam="fromDay"
            monthValue="<%= calendarFrom.get(Calendar.MONTH) %>" monthParam="fromMonth"
            yearValue="<%= calendarFrom.get(Calendar.YEAR) %>" yearParam="fromYear" />
        <liferay-ui:input-date name="toDate" cssClass="input-small"
            dayValue="<%= calendarTo.get(Calendar.DAY_OF_MONTH) %>" dayParam="toDay"
            monthValue="<%= calendarTo.get(Calendar.MONTH) %>" monthParam="toMonth"
            yearValue="<%= calendarTo.get(Calendar.YEAR) %>" yearParam="toYear" />
       <aui:button name="searchBtn" type="button" value="search" last="true" />
    </aui:nav-bar-search>
</aui:nav-bar>
<aui:layout>
    <aui:column columnWidth="50" first="true">
        <%
			int i = 0;
			for (String c : map.keySet()) {
			    Object[] obj = new Object[5]; 
			    Vuoto returnable = VuotoLocalServiceUtil.getVuoto(c);
			    obj[0] = returnable.getNome();
			    List<MovimentoVuoto> listMovs = map.get(c);
			    List<Object[]> list1 = MovimentoVuotoLocalServiceUtil.
			            getPartnerReturnablesStock(partnerCode, from, c);
			    int balance = 0;
			    if (list1.size() > 0) {
			        balance = (int)Double.parseDouble(list1.get(0)[1].toString());
			    }
			    obj[1] = balance;
			%>
			<liferay-ui:panel title="<%= returnable.getNome()%>" state="collapsed">
			<aui:input name="stock" value="<%=balance %>" label="prev-balance" cssClass="input-mini"
			    disabled="true"/>
			<liferay-ui:search-container delta="20"
			    emptyResultsMessage="<%=msg %>" iteratorURL="<%=iteratorURL%>">
			    <liferay-ui:search-container-results>
			        <%
			            results = ListUtil.subList(listMovs,
			                            searchContainer.getStart(),
			                            searchContainer.getEnd());
			                    total = listMovs.size();
			                    pageContext.setAttribute("results", results);
			                    pageContext.setAttribute("total", total);
			        %>
			    </liferay-ui:search-container-results>
			    <liferay-ui:search-container-row
			        className="it.bysoftware.ct.model.MovimentoVuoto" modelVar="movement1">
			        <%
			            balance += movement1.getQuantita() * movement1.getTipoMovimento(); 
			        %>
			        <liferay-ui:search-container-column-text name="date"
			            value="<%=sdf.format(movement1.getDataMovimento())%>" />
			<%--        <liferay-ui:search-container-column-text name="item" --%>
			<%--            value="<%=returnable.getNome()%>" /> --%>
			        <%
			            String incLbl = LanguageUtil.get(portletConfig, user.getLocale(),
			                        "incoming-x");
			            String outLbl = LanguageUtil.get(portletConfig, user.getLocale(),
			                            "outgoing-x");
			            String incName = LanguageUtil.format(user.getLocale(), incLbl,
			                        new Object[] { returnable.getNome(), supplier.getRagioneSociale1() });
			            String outName = LanguageUtil.format(user.getLocale(), outLbl,
			                        new Object[] { returnable.getNome(), supplier.getRagioneSociale1() });
			        %>
			        <c:if test="<%=movement1.getTipoMovimento() > 0%>">
			            <liferay-ui:search-container-column-text name="<%= incName %>"
			                cssClass="text-success"
			                value="<%=String.valueOf((int)movement1.getQuantita())%>" />
			            <liferay-ui:search-container-column-text name="<%= outName %>" value="" />
			            <%
			                if (obj[2] != null) {
			                    obj[2] = (int)(Double.parseDouble(obj[2].toString()) + movement1.getQuantita());
			                } else {
			                    obj[2] = (int) movement1.getQuantita();
			                }
			            %>
			        </c:if>
			        <c:if test="<%=movement1.getTipoMovimento() < 0%>">
			            <liferay-ui:search-container-column-text name="<%=incName %>" value="" />
			            <liferay-ui:search-container-column-text name="<%=outName %>"
			                cssClass="text-error"
			                value="<%=String.valueOf((int)movement1.getQuantita())%>" />
			            <%
                            if (obj[3] != null) {
                                obj[3] =(int)(Double.parseDouble(obj[3].toString()) + movement1.getQuantita());
                            } else {
                                obj[3] = (int) movement1.getQuantita();
                            }
                        %>
			        </c:if>
			    </liferay-ui:search-container-row>
			    <liferay-ui:search-iterator searchContainer="<%=searchContainer%>" />
			</liferay-ui:search-container>
			<aui:input name="new-stock" value="<%=balance %>" label="new-balance" cssClass="input-mini"
			    disabled="true"/>
			</liferay-ui:panel>
			<%
			    obj[4] = (int)balance;
			    objs.add(obj);
			    i++;
			}
			%>
    </aui:column>
    <aui:column columnWidth="50" last="true">
        <liferay-ui:search-container delta="20"
                emptyResultsMessage="<%=msg %>" iteratorURL="<%=iteratorURL%>">
                <liferay-ui:search-container-results>
                    <%
                        results = ListUtil.subList(objs,
                                        searchContainer.getStart(),
                                        searchContainer.getEnd());
                                total = objs.size();
                                pageContext.setAttribute("results", results);
                                pageContext.setAttribute("total", total);
                    %>
                </liferay-ui:search-container-results>
                <liferay-ui:search-container-row className="Object[]" modelVar="obj">
                    <liferay-ui:search-container-column-text name="item" value="<%=String.valueOf(obj[0])%>" />
                    <liferay-ui:search-container-column-text name="Saldo prec." value="<%=String.valueOf(obj[1])%>" />
                    <liferay-ui:search-container-column-text name="Consegnati" value="<%=String.valueOf(obj[2]!=null?obj[2]:0)%>" />
                    <liferay-ui:search-container-column-text name="Ricevuti" value="<%=String.valueOf(obj[3]!=null?obj[3]:0)%>" />
                    <liferay-ui:search-container-column-text name="Saldo corr." value="<%=String.valueOf(obj[4])%>" />
                </liferay-ui:search-container-row>
                <liferay-ui:search-iterator searchContainer="<%=searchContainer%>" />
            </liferay-ui:search-container>
    </aui:column>
</aui:layout>

<aui:script>
var printMovements = '<%= printMovementsURL %>';
AUI().use('aui-base', 'datatype-number', function(A) {
    A.one('#<portlet:namespace/>searchBtn').on('click', function() {
        var fromDateStr = A.one('#<portlet:namespace />fromDate').get('value');
        var fromDate = parseDate(fromDateStr);
        var toDateStr = A.one('#<portlet:namespace/>toDate').get('value');
        var toDate = parseDate(toDateStr);
        window.location.href = '<%= reloadURL %>&<portlet:namespace/>dateFrom=' + fromDate.getTime() + '&<portlet:namespace/>dateTo=' + toDate.getTime();
    });
});
AUI().use('aui-base', 'datatype-number', 'liferay-portlet-url', function(A) {
    A.one('#<portlet:namespace/>printBtn').on('click', function() {
        var fromDateStr = A.one('#<portlet:namespace />fromDate').get('value');
        var fromDate = parseDate(fromDateStr).getTime();
        console.log("FROM DATE: " + fromDate);
        var toDateStr = A.one('#<portlet:namespace/>toDate').get('value');
        var toDate = parseDate(toDateStr).getTime();
        console.log("TO DATE: " + toDate);
        var resourceURL = Liferay.PortletURL.createResourceURL();
        resourceURL.setResourceId('<%=ResourceId.downloadMovements.name()%>');
        resourceURL.setPortletId('<%= PortalUtil.getPortletId(request) %>');
        resourceURL.setParameter('dateFrom', fromDate);
        resourceURL.setParameter('dateTo', toDate);
        resourceURL.setParameter('code', '<%=partnerCode%>');
        window.open(resourceURL,'Download');
//         A.io.request(printMovements, {
//             method: 'POST',
//             data: {
//             	<portlet:namespace />dateFrom: fromDate,
//             	<portlet:namespace />dateTo: toDate,
//             	<portlet:namespace />code: '<%=partnerCode%>'
//             },
//             on: {
//                 start: function() {
//                     modal = new A.Modal({
//                         bodyContent : '<div class="loading-animation"/>',
//                         centered : true,
//                         headerContent : '<h3>Loading...</h3>',
//                         modal : true,
//                         render : '#modal',
//                         close: false,
//                         zIndex: 99999,
//                         width : 450
//                     }).render();
//                 },
//                 success: function () {
//                     var data = JSON.parse(this.get('responseData'));
//                     console.log(data);
//                     modal.hide();
//                     if (data.code === 0) {
//                         var win = window.open(data.message.toLocaleString());
//                         if (win) {
//                             window.focus();
//                         } else {
//                             var timer = window.setTimeout(function(){
//                                 if (win) {
//                                     win.focus();
//                                 }
//                             }, 200 );
//                         }
//                     } else {
//                         alert('Si e\' verificato un errore durante la stampa del'
//                                 + ' buono.\n\n' + data.message);
//                     }
                    
//                 },
//                 error: function () {
//                     modal.hide();
//                 }
//             }
//         });
    });
});
function parseDate(string) {
    var tmp = string.split('/');
    return new Date(tmp[2] + '-' + tmp[1] + '-' + tmp[0]);
}
</aui:script>