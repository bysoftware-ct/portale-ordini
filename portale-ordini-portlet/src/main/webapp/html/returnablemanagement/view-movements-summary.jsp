<%@page import="java.util.Calendar"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="it.bysoftware.ct.service.VuotoLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Vuoto"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.util.Date"%>
<%@page import="it.bysoftware.ct.service.MovimentoVuotoLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="it.bysoftware.ct.model.MovimentoVuoto"%>
<%@page import="java.util.List"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@include file="../init.jsp"%>

<%
    String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
    String backUrl = ParamUtil.getString(renderRequest, "backUrl");
    String partnerCode = ParamUtil.getString(renderRequest,
            "partnerCode");
    AnagraficheClientiFornitori supplier = AnagraficheClientiFornitoriLocalServiceUtil
            .getAnagraficheClientiFornitori(partnerCode);
    String title = LanguageUtil.get(portletConfig, user.getLocale(),
            "partner-movements-list-x");
    title = LanguageUtil.format(user.getLocale(), title, new Object[] { " " + supplier.getRagioneSociale1() });
    Calendar tomorrow = Calendar.getInstance();
    tomorrow = Utils.today();
    tomorrow.add(Calendar.DATE, 1);
    List<Object[]> stocks = MovimentoVuotoLocalServiceUtil.getPartnerReturnablesStock(partnerCode, tomorrow.getTime(), null);
    String msg = LanguageUtil.get(portletConfig, user.getLocale(), "no-sp-movements-x");
    msg = LanguageUtil.format(user.getLocale(), msg, new Object[] { supplier.getRagioneSociale1() });
%>

<liferay-ui:header backURL="<%=backUrl%>" title="<%=title %>" />
<liferay-portlet:renderURL varImpl="iteratorURL">
	<portlet:param name="partnerCode" value="<%=partnerCode %>" />
	<portlet:param name="mvcPath"
		value="/html/returnablemanagement/view-movements-summary.jsp" />
</liferay-portlet:renderURL>
<liferay-portlet:renderURL varImpl="newMovementURL">
    <portlet:param name="backUrl" value="<%=themeDisplay.getURLCurrent() %>" />
    <portlet:param name="partnerCode" value="<%=partnerCode %>" />
    <portlet:param name="mvcPath"
        value="/html/returnablemanagement/new-movements.jsp" />
</liferay-portlet:renderURL>
<liferay-portlet:renderURL varImpl="searchMovementURL">
    <portlet:param name="mvcPath"
        value="/html/returnablemanagement/search-movement.jsp" />
    <portlet:param name="backUrl" value="<%=iteratorURL.toString()%>" />
    <portlet:param name="prevUrl" value="<%=backUrl%>" />
    <portlet:param name="partnerCode" value="<%=partnerCode%>" />
</liferay-portlet:renderURL>
<liferay-portlet:renderURL varImpl="allMovementsURL">
    <portlet:param name="mvcPath"
        value="/html/returnablemanagement/view-movements.jsp" />
    <portlet:param name="backUrl" value="<%=iteratorURL.toString()%>" />
    <portlet:param name="prevUrl" value="<%=backUrl%>" />
    <portlet:param name="partnerCode" value="<%=partnerCode%>" />
</liferay-portlet:renderURL>
<aui:nav-bar>
    <aui:nav>
        <aui:nav-item id="addBtn" iconCssClass="icon-plus" label="add"
           selected='<%="add".equals(toolbarItem)%>' href="<%= newMovementURL.toString() %>" />
        <aui:nav-item id="searchBtn" iconCssClass="icon-search" label="search"
           selected='<%="search".equals(toolbarItem)%>' href="<%= searchMovementURL.toString() %>" />
        <aui:nav-item id="listBtn" iconCssClass="icon-list" label="view-all"
           selected='<%="list".equals(toolbarItem)%>' href="<%= allMovementsURL.toString() %>" />
    </aui:nav>
</aui:nav-bar>
<liferay-ui:search-container delta="20"
	emptyResultsMessage="<%=msg %>" iteratorURL="<%=iteratorURL%>">
	<liferay-ui:search-container-results>
		<%
            results = ListUtil.subList(stocks,
                    searchContainer.getStart(),
                    searchContainer.getEnd());
            total = stocks.size();
            pageContext.setAttribute("results", results);
            pageContext.setAttribute("total", total);
        %>
	</liferay-ui:search-container-results>
	<liferay-ui:search-container-row className="Object[]" modelVar="stock">
		<c:if test="<%=stock.length == 2 %>">
			<%
            double value = Double.parseDouble(stock[1].toString());
            Vuoto v = VuotoLocalServiceUtil.getVuoto(stock[0].toString());
//             if (value > 0) {
            %>
			<liferay-ui:search-container-column-text name="code"
				value="<%= stock[0].toString()%>" />
			<liferay-ui:search-container-column-text name="item"
				value="<%=v.getNome() %>" />
			<liferay-ui:search-container-column-text name="stock"
				value="<%=String.valueOf((int)value) %>" />
			<liferay-ui:search-container-row-parameter name="partnerCode"
                value="<%= partnerCode%>" />
			<liferay-ui:search-container-row-parameter name="backUrl"
				value="<%= iteratorURL.toString() %>" />
			<liferay-ui:search-container-column-jsp
				path="/html/returnablemanagement/view-movements-summary-actions.jsp" />
			<%
// 			}
			%>
		</c:if>
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator searchContainer="<%=searchContainer%>" />
</liferay-ui:search-container>