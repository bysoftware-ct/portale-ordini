<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="it.bysoftware.ct.utils.ResourceId"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.json.JSONArray"%>
<%@page import="it.bysoftware.ct.service.VuotoLocalServiceUtil"%>
<%@page import="java.util.Calendar"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="it.bysoftware.ct.model.Vuoto"%>
<%@page import="java.util.List"%>
<%@include file="../init.jsp"%>
<%
	String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
	String backUrl = ParamUtil.getString(renderRequest, "backUrl");
	String partnerCode = ParamUtil.getString(renderRequest,
	        "partnerCode");
	String num = ParamUtil.getString(renderRequest, "num", "");
	AnagraficheClientiFornitori subject =
	        AnagraficheClientiFornitoriLocalServiceUtil.getAnagraficheClientiFornitori(partnerCode);
	
	JSONArray jsonArr = null;
	String strValues = ParamUtil.getString(renderRequest, "values", null);
	if (strValues != null && !strValues.isEmpty()) {
	    jsonArr = JSONFactoryUtil.looseDeserialize(strValues, JSONArray.class);
	}
	
	String giveToLbl = LanguageUtil.get(portletConfig, user.getLocale(),
	            "give-to-x");
	String getFromLbl = LanguageUtil.get(portletConfig, user.getLocale(),
                "get-from-x");
    giveToLbl = LanguageUtil.format(user.getLocale(), giveToLbl,
            new Object[] { user.getFirstName(), subject.getRagioneSociale1() }); 
    getFromLbl = LanguageUtil.format(user.getLocale(), getFromLbl,
            new Object[] { user.getFirstName(), subject.getRagioneSociale1() });
%>
<liferay-portlet:actionURL var="addMovements" name="addMovements">
    <liferay-portlet:param name="partnerCode" value="<%=partnerCode %>"/>
    <liferay-portlet:param name="backUrl" value="<%=backUrl %>"/>
</liferay-portlet:actionURL>
<portlet:resourceURL var="saveMovementsUrl" 
        id="<%=ResourceId.saveMovements.name() %>" />
<portlet:resourceURL var="printMovementsURL" 
    id="<%=ResourceId.printMovements.name() %>" />
<portlet:resourceURL var="sendMovementsURL" 
    id="<%=ResourceId.sendMovements.name() %>" />
<liferay-ui:header backURL="<%=backUrl%>" title="add-movements" />
<aui:nav-bar>
    <aui:nav>
<%--         <aui:nav-item id="saveBtn" iconCssClass="icon-save" label="save" --%>
<%--            selected='<%="save".equals(toolbarItem)%>' /> --%>
        <aui:nav-item id="printBtn" iconCssClass="icon-print" label="print" 
           selected='<%="print".equals(toolbarItem)%>' />
<%--         <aui:nav-item id="sendBtn" iconCssClass="icon-envelope" label="send"  --%>
<%--            selected='<%="send".equals(toolbarItem)%>' />        --%>
    </aui:nav>
    <c:if test="<%=num.isEmpty() %>">
    	<aui:nav-bar-search cssClass="pull-right" >
       		<aui:nav-item id="printDailyBtn" iconCssClass="icon-print" label="print-daily"  cssClass="btn" 
          		selected='<%="printDaily".equals(toolbarItem)%>' />
    	</aui:nav-bar-search>
    </c:if>
</aui:nav-bar>
<aui:input name="num" value="<%=num %>" type="hidden"/>
<aui:form name="fm" action="<%=addMovements %>" method="post" cssClass="form-horizontal">
    <liferay-ui:message key="movement-date" />
    <liferay-ui:input-date name="movementDate"
               dayValue="<%=Utils.today().get(Calendar.DAY_OF_MONTH)%>"
               dayParam="newDay"
               monthValue="<%=Utils.today().get(Calendar.MONTH)%>"
               monthParam="newMonth"
               yearValue="<%=Utils.today().get(Calendar.YEAR)%>"
               yearParam="newYear" />
	   <aui:layout>
	       <%
           List<Vuoto> list = VuotoLocalServiceUtil.getVuotos(
                   0, VuotoLocalServiceUtil.getVuotosCount());
           %>
	       <aui:column columnWidth="50" first="true" >
	           <aui:fieldset label="<%=giveToLbl%>">
                   <%
                   for (int i = 0; i < list.size(); i++) {
                   %>
	                   <aui:input name="<%=list.get(i).getCodiceVuoto() + "_giveTo"%>" cssClass="input-mini left-space userfields" inlineLabel="true"
	                       label="<%= list.get(i).getNome()%>">
	                           <aui:validator name="number" />
	                   </aui:input>
                       
                   <%
                   }
                   %>
               </aui:fieldset>
               <aui:input name="<%="note"%>" label="note" placeholder="Inserisci note..." type="textarea" cssClass="left-space"/>
			      <aui:button-row>
			          <aui:button name="addBtn" cssClass="btn-primary" value="add" onClick="javascript:showDialog();"/>
			          <aui:button name="resetBtn" value="cancel" type="reset" />
			      </aui:button-row>
	       </aui:column>
	       <aui:column columnWidth="50" last="true" >
               <aui:fieldset label="<%=getFromLbl%>">
	               <%
	               for (int i = 0; i < list.size(); i++) {
	               %>
                       <aui:input name="<%=list.get(i).getCodiceVuoto() + "_getFrom"%>" cssClass="input-mini left-space userfields" inlineLabel="true"
                           label="<%= list.get(i).getNome()%>">
                           <aui:validator name="number" />
                       </aui:input>
	               <%
	               }
	               %>
               </aui:fieldset>
           </aui:column>
       </aui:layout>
</aui:form>
<div class="yui3-skin-sam">
  <div id="modal"></div>
</div>
<aui:script>
var printMovements = '<%= printMovementsURL %>';
AUI().use('aui-base', 'aui-io-request', 'aui-modal', function(A) {
    A.one('#<portlet:namespace />printBtn').on('click', function(event) {
    	var date = A.one('#<portlet:namespace />movementDate').get('value');
    	date = date.split('/');
    	var orderDate = new Date(date[2] + '-' + date[1] + '-' + date[0]).getTime();
    	console.log(orderDate);
    	var fields = A.all('.userfields');
    	var isEmpty = true;
    	fields._nodes.forEach(function(field, i) {
    		var x = A.one('#' + field.id).get('value');
    		if ( x === 0 || x === '') {
    			isEmpty = isEmpty && true;
    		} else {
    			isEmpty = isEmpty && false;
    		}
    	});
        A.io.request(printMovements, {
            method: 'POST',
            data: {
            	<portlet:namespace />date: orderDate,
            	<portlet:namespace />partnerCode: '<%=partnerCode%>',
            	<portlet:namespace />num: '<%=num%>',
            	<portlet:namespace />empty: isEmpty
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                    var data = JSON.parse(this.get('responseData'));
                    console.log(data);
                    modal.hide();
                    if (data.code === 0) {
                        var win = window.open(data.message.toLocaleString());
                        if (win) {
                            window.focus();
                        } else {
                            var timer = window.setTimeout(function(){
                                if (win) {
                                    win.focus();
                                }
                            }, 200 );
                        }
                    } else {
                        alert('Si e\' verificato un errore durante la stampa del'
                                + ' buono.\n\n' + data.message);
                    }
                    
                },
                error: function () {
                    modal.hide();
                }
            }
        });
    });
    A.one('#<portlet:namespace />printDailyBtn').on('click', function(event) {
    	var date = A.one('#<portlet:namespace />movementDate').get('value');
    	date = date.split('/');
    	var orderDate = new Date(date[2] + '-' + date[1] + '-' + date[0]).getTime();
    	A.io.request(printMovements, {
            method: 'POST',
            data: {
            	<portlet:namespace />date: orderDate,
            	<portlet:namespace />partnerCode: '<%=partnerCode%>',
            	<portlet:namespace />num: '',
            	<portlet:namespace />empty: false
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                    var data = JSON.parse(this.get('responseData'));
                    console.log(data);
                    modal.hide();
                    if (data.code === 0) {
                        var win = window.open(data.message.toLocaleString());
                        if (win) {
                            window.focus();
                        } else {
                            var timer = window.setTimeout(function(){
                                if (win) {
                                    win.focus();
                                }
                            }, 200 );
                        }
                    } else {
                        alert('Si e\' verificato un errore durante la stampa del'
                                + ' buono.\n\n' + data.message);
                    }
                    
                },
                error: function () {
                    modal.hide();
                }
            }
        });
    });
});
function showDialog() {
	 AUI().use('aui-modal', function(A) {
		 var modal = new A.Modal({
			 bodyContent: '<label for="feedback/suggestions" ><liferay-ui:message key="confirm-insert"/></label>',
	         centered: true,
	         headerContent: '<h3><label for="formsofinteraction"><liferay-ui:message key="confirmation"/></label></h3>',
	         modal: true,
	         render: '#modal',
	         width: 500
         }).render();
		 modal.addToolbar([{
			 label: '<liferay-ui:message key="cancel"/>',
			 on: {
				 click: function() {
					 modal.hide();
				 }
			 }
		 }, {
			 label: '<liferay-ui:message key="submit"/>',
			 on: {
				 click: function() {
					 modal.hide();
					 document.getElementById("<portlet:namespace/>fm").submit();
				 }
			 }
		 }]);
     });
}
</aui:script>