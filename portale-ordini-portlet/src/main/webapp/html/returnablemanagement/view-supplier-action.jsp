<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@include file="../init.jsp"%>
<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    AnagraficheClientiFornitori partner = (AnagraficheClientiFornitori) row
            .getObject();
    String backUrl = (String) row.getParameter("backUrl");
%>
<liferay-ui:icon-menu>
    <liferay-portlet:renderURL varImpl="viewMovementsURL">
        <portlet:param name="mvcPath" value="/html/returnablemanagement/view-movements-summary.jsp" />
        <portlet:param name="backUrl" value="<%=backUrl%>" />
        <portlet:param name="partnerCode"
            value="<%=partner.getCodiceAnagrafica()%>" />
        <portlet:param name="partnerName"
            value="<%=partner.getRagioneSociale1() + " " + partner.getRagioneSociale2()%>" />
    </liferay-portlet:renderURL>
    <liferay-ui:icon image="search" url="${viewMovementsURL}" message="view" />
</liferay-ui:icon-menu>
