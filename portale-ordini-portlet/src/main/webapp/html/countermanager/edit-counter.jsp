<%@page
	import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@include file="../init.jsp"%>

<%
    String backUrl = ParamUtil.getString(renderRequest, "backUrl");
    boolean anyCounter = ParamUtil.getBoolean(renderRequest,
            "anyCounter", false);
    int currentNumber = ParamUtil.getInteger(renderRequest, "currentNumber", -1);
    int newNumber = ParamUtil.getInteger(renderRequest, "newNumber", currentNumber);
    String partnerCode = ParamUtil.getString(renderRequest,
            "partnerCode", "");

    AnagraficheClientiFornitori partner = AnagraficheClientiFornitoriLocalServiceUtil
            .getAnagraficheClientiFornitori(partnerCode);
%>

<liferay-ui:header backURL="<%=backUrl%>"
	title="<%=partner.getRagioneSociale1()%>" />

<liferay-portlet:actionURL name="saveDocumentNumber" var="saveDocumentNumberURL">
	<liferay-portlet:param name="anyCounter"
		value="<%=String.valueOf(anyCounter)%>" />
	<liferay-portlet:param name="partnerCode"
		value="<%=partnerCode%>" />
	<liferay-portlet:param name="backUrl"
        value="<%=backUrl%>" />
</liferay-portlet:actionURL>

<aui:form id="aForm" name="aForm" action="<%=saveDocumentNumberURL%>"
	method="post">
	<liferay-ui:message key="current-number-x" arguments="<%= new String[] {String.valueOf(currentNumber)}%>" />
	<aui:input name="currentNumber" type="hidden" cssClass="input-mini" 
		label="current-number" value="<%=currentNumber %>" />
	<aui:input name="newNumber" type="text" cssClass="input-mini" inlineLabel="true"
        label="new-number" value="<%=currentNumber %>" >
        <aui:validator name="required" />
        <aui:validator name="number" />
    </aui:input>
	<aui:button-row>
		<aui:button type="submit" value="save" />
		<aui:button type="cancel" onclick="this.form.reset()" />
	</aui:button-row>
</aui:form>