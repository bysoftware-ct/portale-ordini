<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="../init.jsp"%>

<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    AnagraficheClientiFornitori partner = (AnagraficheClientiFornitori) row.getObject(); 
    boolean anyCounter = Boolean.parseBoolean(String.valueOf(row.getParameter("anyCounter")));
    int currentNumber = Integer.parseInt(String.valueOf(row.getParameter("currentNumber")));
    int number = Integer.parseInt(String.valueOf(row.getParameter("number")));
    String backUrl = (String) row.getParameter("backUrl");
%>

<liferay-ui:icon-menu>
    <liferay-portlet:renderURL varImpl="editCounterURL">
        <portlet:param name="mvcPath" value="/html/countermanager/edit-counter.jsp" />
        <portlet:param name="backUrl" value="<%=backUrl%>" />
        <portlet:param name="anyCounter" value="<%=String.valueOf(anyCounter)%>" />
        <portlet:param name="currentNumber" value="<%=String.valueOf(currentNumber)%>" />
        <portlet:param name="newNumber" value="<%=String.valueOf(number)%>" />
        <portlet:param name="partnerCode" value="<%=partner.getCodiceAnagrafica()%>" />
    </liferay-portlet:renderURL>
    <liferay-ui:icon image="edit" url="${editCounterURL}" message="set-number" />
</liferay-ui:icon-menu>