<%@page import="it.bysoftware.ct.service.persistence.ContatoreSocioPK"%>
<%@page import="it.bysoftware.ct.service.ContatoreSocioLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.ContatoreSocioLocalService"%>
<%@page import="it.bysoftware.ct.model.ContatoreSocio"%>
<%@page import="it.bysoftware.ct.service.TestataDocumentoLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="java.util.List"%>
<%@ include file="../init.jsp"%>

<%
    List<AnagraficheClientiFornitori> tmp = AnagraficheClientiFornitoriLocalServiceUtil.
            findAssociates();
    String code = ParamUtil.getString(request, "code", "");
    String companyName = ParamUtil.getString(request, "companyName", "");
    List<AnagraficheClientiFornitori> partners = new ArrayList<AnagraficheClientiFornitori>();
    if (!code.isEmpty() || !companyName.isEmpty()) {
        for (AnagraficheClientiFornitori p : tmp) {
            boolean add = true;
            if (!code.isEmpty()
                    && !p.getCodiceAnagrafica().toLowerCase().
                            contains(code.toLowerCase())) {
                add = false;
            }
            if (!companyName.isEmpty()
                    && !p.getRagioneSociale1().toLowerCase().
                            contains(companyName.toLowerCase())) {
                add = false;
            }

            if (add) {
                partners.add(p);
            }
        }
    } else {
        partners = tmp;
    }
%>
<liferay-portlet:renderURL varImpl="iteratorURL">
    <portlet:param name="code" value="<%=code %>"/>
    <portlet:param name="companyName" value="<%=companyName %>"/>
    <portlet:param name="mvcPath"
        value="/html/countermanager/view.jsp" />
</liferay-portlet:renderURL>
<liferay-portlet:renderURL varImpl="partnerSearchURL">
    <portlet:param name="mvcPath"
        value="/html/countermanager/view.jsp" />
</liferay-portlet:renderURL>
<aui:form action="<%=partnerSearchURL%>" method="get" name="partnerForm">
    <liferay-portlet:renderURLParams varImpl="partnerSearchURL" />
       <aui:input label="code" name="code" value="<%=code%>" inlineField="true"/>
       <aui:input label="company-name" name="companyName" inlineField="true"
            value="<%=companyName%>" />
       <aui:button-row>
            <aui:button type="submit" value="search" />
            <aui:button type="reset" value="cancel" />
       </aui:button-row>
</aui:form>

<liferay-ui:search-container delta="20"
    emptyResultsMessage="no-special-partner"
    iteratorURL="<%=iteratorURL%>">
    <liferay-ui:search-container-results>
        <%
            results = ListUtil.subList(partners,
                    searchContainer.getStart(),
                    searchContainer.getEnd());
            total = partners.size();
            pageContext.setAttribute("results", results);
            pageContext.setAttribute("total", total);
        %>
    </liferay-ui:search-container-results>
    <liferay-ui:search-container-row
        className="it.bysoftware.ct.model.AnagraficheClientiFornitori"
        modelVar="partner">
        <liferay-ui:search-container-column-text property="codiceAnagrafica"
            name="code" />
        <%
            String businessName = partner.getRagioneSociale1()
                                + " " + partner.getRagioneSociale2();
            int year = Utils.getYear();
            ContatoreSocio partnerCounter = ContatoreSocioLocalServiceUtil.fetchContatoreSocio(
                    new ContatoreSocioPK(year, partner.getCodiceAnagrafica(), "FMI"));
            boolean anyCounter = false;
            int counter = 0;
            int currentNumber = 0;
            if (partnerCounter != null) {
                anyCounter = true;
                counter = partnerCounter.getNumero();
                currentNumber = TestataDocumentoLocalServiceUtil.getNumber(year, "0001", "M", "000",
                        partner.getCodiceAnagrafica(), "FMI") - 1;
            } else {
                counter = TestataDocumentoLocalServiceUtil.getNumber(year, "0001", "M", "000",
                        partner.getCodiceAnagrafica(), "FMI");
                currentNumber = counter - 1;
            }
        %>
        <liferay-ui:search-container-column-text name="company-name"
            value="<%=businessName%>" />
        <liferay-ui:search-container-row-parameter name="anyCounter" value="<%=String.valueOf(anyCounter) %>" />
        <liferay-ui:search-container-row-parameter name="currentNumber" value="<%=currentNumber %>" />
        <liferay-ui:search-container-row-parameter name="number" value="<%=counter %>" />
        <liferay-ui:search-container-column-text name="current-number" value="<%=String.valueOf(currentNumber) %>" />
        <liferay-ui:search-container-column-text name="new-number" value="<%=String.valueOf(counter) %>" />
        <liferay-ui:search-container-row-parameter name="backUrl"
            value="<%= iteratorURL.toString() %>" />
        <liferay-ui:search-container-column-jsp path="/html/countermanager/view-action.jsp" />
    </liferay-ui:search-container-row>
    <liferay-ui:search-iterator searchContainer="<%=searchContainer%>"/>
</liferay-ui:search-container>