<%@page import="it.bysoftware.ct.model.Ordine"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@include file="../init.jsp"%>
<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    Ordine order = (Ordine) row.getObject();
    String backUrl = (String) row.getParameter("backUrl");
%>

<liferay-ui:icon-menu>
	<liferay-portlet:actionURL var="createInvoice" name="createInvoice">
        <liferay-portlet:param name="orderId"
            value="<%=String.valueOf(order.getId())%>" />
    </liferay-portlet:actionURL>
    <liferay-ui:icon image="edit" url="${createInvoice}" message="generate" onClick="showDialog()"/>
</liferay-ui:icon-menu>

<aui:script>
function showDialog() {
	AUI().use('aui-base', 'aui-modal', function (A) {
		var modal = new A.Modal({
            bodyContent : '<div class="loading-animation"/>',
            centered : true,
            headerContent : '<h3>Loading...</h3>',
            modal : true,
            render : '#modal',
            close: false,
            zIndex: 99999,
            width : 450
        }).render();
	});
}
</aui:script>
