<%@page import="com.liferay.util.portlet.PortletProps"%>
<%@page import="it.bysoftware.ct.service.TestataDocumentoLocalServiceUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="com.ibm.icu.text.NumberFormat"%>
<%@page import="it.bysoftware.ct.utils.OrderState"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.util.List"%>
<%@page import="it.bysoftware.ct.service.OrdineLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../init.jsp"%>

<%
//     String CODE = PortletProps.get("foreing-center-code");
    String CODE = PortletProps.get("it-center-code");
    List<Ordine> processed = OrdineLocalServiceUtil.getTodayProcessedOrders();
    List<Ordine> openOrders = new ArrayList<Ordine>();
    for (Ordine o : processed) {
        if(!o.getCentro().equals(CODE) && TestataDocumentoLocalServiceUtil.getOpenDocuments(o.getId()) > 0) {
            openOrders.add(o);
        }
    }
    NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
%>

<liferay-portlet:renderURL varImpl="iteratorURL">
	<portlet:param name="mvcPath" value="/html/immediateinvoice/view.jsp" />
</liferay-portlet:renderURL>

<liferay-ui:search-container delta="20"
	emptyResultsMessage="no-orders" iteratorURL="<%=iteratorURL%>">
	<liferay-ui:search-container-results>
		<%
		    results = ListUtil.subList(openOrders,
		                    searchContainer.getStart(),
		                    searchContainer.getEnd());
		            total = openOrders.size();
		            pageContext.setAttribute("results", results);
		            pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	<liferay-ui:search-container-row
		className="it.bysoftware.ct.model.Ordine" modelVar="order">
		<liferay-ui:search-container-column-text name="confirm-number"
			value="<%=order.getCentro() + "/" + order.getNumero()%>" />
		<liferay-ui:search-container-column-text name="company-name"
		    value="<%= AnagraficheClientiFornitoriLocalServiceUtil.
		            fetchAnagraficheClientiFornitori(order.getIdCliente()).getRagioneSociale1() %>"/>
		<liferay-ui:search-container-column-text
			value="<%=fmt.format(order.getImporto())%>" name="amount" />
		<liferay-ui:search-container-column-jsp align="right"
			path="/html/immediateinvoice/orders-action.jsp" />
		<liferay-ui:search-container-row-parameter name="backUrl"
			value="<%=iteratorURL.toString()%>" />
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
		paginate="true" />
</liferay-ui:search-container>