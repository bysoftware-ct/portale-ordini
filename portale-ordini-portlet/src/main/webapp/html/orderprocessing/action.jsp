<%@page import="it.bysoftware.ct.model.Ordine"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="../init.jsp"%>

<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Ordine order = (Ordine) row.getObject();
	String backURL = (String) row.getParameter("backURL");
%>

<liferay-ui:icon-menu>
	<liferay-portlet:renderURL varImpl="loadOrderURL">
		<portlet:param name="mvcPath" value="/html/orderprocessing/order.jsp" />
		<portlet:param name="backURL" value="<%=backURL%>" />
		<portlet:param name="orderId" value="<%=String.valueOf(order.getId())%>" />
	</liferay-portlet:renderURL>
	   <liferay-ui:icon image="search" url="${loadOrderURL}" message="view" />
</liferay-ui:icon-menu>