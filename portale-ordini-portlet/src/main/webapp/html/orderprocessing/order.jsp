<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.exception.SystemException"%>
<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil"%>
<%@page import="java.io.File"%>
<%@page import="com.liferay.portal.kernel.repository.model.FileEntry"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="it.bysoftware.ct.service.TipoCarrelloLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.TipoCarrello"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="it.bysoftware.ct.service.PianteLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Piante"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.service.RigaLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.CarrelloLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Carrello"%>
<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="java.util.TreeMap"%>
<%@page import="it.bysoftware.ct.model.Riga"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="it.bysoftware.ct.service.OrdineLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../init.jsp"%>
<%
	String cartId = ParamUtil.getString(renderRequest, "cartId", "");
	String backUrl = ParamUtil.getString(renderRequest, "backURL");
	Ordine order = null;
	AnagraficheClientiFornitori customer = null;
	long id = ParamUtil.getLong(renderRequest, "orderId", -1);
	if (id > 0) {
		order = OrdineLocalServiceUtil.getOrdine(id);
	}
	
	String lbl = "";
	Map<Long, List<Riga>> itemToReceive = new TreeMap<Long, List<Riga>>();
	if (order != null) {
		lbl = LanguageUtil.format(pageContext, "order-x",
	            new String[] { String.valueOf(order.getNumero()),
	                    String.valueOf(order.getCentro()) });
		customer = AnagraficheClientiFornitoriLocalServiceUtil
                .getAnagraficheClientiFornitori(order.getIdCliente());
		
		List<Carrello> carts = CarrelloLocalServiceUtil.findCartsInOrder(order.getId());
		for (Carrello cart : carts) {
			List<Riga> rows = RigaLocalServiceUtil.findRowsInCart(cart.getId());
			List<Riga> rowToadd = new ArrayList<Riga>();
			for (Riga row : rows) {
				Piante p = PianteLocalServiceUtil.getPiante(row.getIdArticolo());
				rowToadd.add(row);
			}
			itemToReceive.put(cart.getId(), rowToadd);
		}
	}
	
	long currentCart = 0;
%>
<c:choose>
	<c:when test="<%= order != null %>">
		<liferay-portlet:renderURL varImpl="iteratorURL">
			<!-- TODO -->
			<portlet:param name="backUrl" value="<%= backUrl %>"/>
			<portlet:param name="mvcPath" value="/html/orderprocessing/order.jsp" />
		</liferay-portlet:renderURL>
		<liferay-ui:header title="<%=lbl %>" backURL="<%=backUrl %>" />
	    <aui:layout>
		    <aui:column columnWidth="50" first="true">
				<aui:input name="customerName" id="customerName" type="textarea"
					label="customer" disabled="true" resizable="false"
					cssClass="input-xlarge" inlineField="true"
					value="<%=customer.getRagioneSociale1() + "\n"
                                    + customer.getRagioneSociale2()%>" />
				<aui:input name="address" id="address" type="textarea"
					label="address" disabled="true" cssClass="input-xlarge"
					inlineField="true" value="<%=Utils.getFullAddress(customer)%>" />
			</aui:column>
	    </aui:layout>
	    <%
				for (long idCart : itemToReceive.keySet()) {
					Carrello cart = CarrelloLocalServiceUtil.getCarrello(idCart);
					List<Riga> rows = itemToReceive.get(cart.getId());
					TipoCarrello type = TipoCarrelloLocalServiceUtil.getTipoCarrello(order.getIdTipoCarrello());
					String cartLbl = LanguageUtil.format(pageContext, "cart-x",
				            new String[] { String.valueOf(cart.getId()) + " - " + type.getTipologia() });
					boolean enabled = true;
					for (Riga r : rows){
						enabled = enabled && r.getRicevuto();
					}
			%>
			<liferay-ui:panel title="<%= cartLbl %>" id="<%= "cart-" + String.valueOf(idCart) %>" >
				<liferay-portlet:actionURL name="lavoraCarrello"
					var="lavoraCarrelloURL">
					<liferay-portlet:param name="cartId"
						value="<%=String.valueOf(cart.getId())%>" />
					<liferay-portlet:param name="orderId"
						value="<%=String.valueOf(id)%>" />
					<liferay-portlet:param name="backURL"
						value="<%=backUrl%>" />
				</liferay-portlet:actionURL>
				<aui:form action="<%=lavoraCarrelloURL %>" method="post" name="lavoratoForm">
					<aui:button-row cssClass="center-h">
						<c:if test="<%= !cart.getLavorato() %>">
							<c:if test="<%= !enabled %>">
								<aui:button id="lavoratoBTN" name="lavoratoBTN" icon="icon-configuration" value="Lavora" type="submit" disabled="true" />
							</c:if>
							<c:if test="<%= enabled %>">
								<aui:button id="lavoratoBTN" name="lavoratoBTN" icon="icon-configuration" value="Lavora" type="submit" />
							</c:if>
						</c:if>
				        <c:if test="<%= cart.getLavorato() %>">
				        	<aui:button id="resetBTN" name="resetBTN" icon="icon-undo" cssClass="btn-warning" value="Annulla lavorato" type="submit" />
				        </c:if>
				    </aui:button-row>
				</aui:form>
				<liferay-ui:search-container delta="75" emptyResultsMessage="no-orders"
					 iteratorURL="<%=iteratorURL%>" curParam="<%=String.valueOf(idCart) %>">
					 <liferay-ui:search-container-results>
							<%
							    results = ListUtil.subList(rows,
							                            searchContainer.getStart(),
							                            searchContainer.getEnd());
							                    total = itemToReceive.size();
							                    pageContext.setAttribute("results", results);
							                    pageContext.setAttribute("total", total);
							%>
						</liferay-ui:search-container-results>
						<liferay-ui:search-container-row keyProperty="id" className="it.bysoftware.ct.model.Riga" modelVar="riga">
						<%
						Piante p = PianteLocalServiceUtil.getPiante(riga.getIdArticolo());
						if (currentCart != riga.getIdCarrello()) {
							currentCart = riga.getIdCarrello();
						}
						String descr = p.getNome() + " Vaso: "
							+ p.getVaso() + " " + p.getForma() + " H. "
								+ p.getAltezza(); 
						long imgId = -1;
						FileEntry image = null;
						String imageSrc = File.separator + "portale-ordini-portlet/icons/not-found.png";
						
						try {
						    imgId = Long.parseLong(p.getFoto1());
						    image = DLAppLocalServiceUtil.getFileEntry(imgId);
						
						    imageSrc = Constants.DOCUMENTS_FOLDER_NAME + File.separator + image.getRepositoryId()
						            + File.separator + image.getFolderId() + File.separator 
						            + image.getTitle(); 
						} catch (NumberFormatException ex) {
						    _log.debug(ex.getMessage());    
						} catch (PortalException ex) {
						    _log.debug(ex.getMessage());
						} catch (SystemException ex) {
						    _log.debug(ex.getMessage());
						}
						%>
						<liferay-ui:search-container-column-text name="code" value="<%=p.getCodice() %>" />	
						<liferay-ui:search-container-column-text name="picture" >
						    <a href="<%= imageSrc %>" target="_blank">								    	
						        <img src="<%=imageSrc %>" style='width: 80px; height: auto;' />
						    </a>
						</liferay-ui:search-container-column-text>								
						<liferay-ui:search-container-column-text name="name" value="<%=descr %>" />
						<liferay-ui:search-container-column-text name="variant" value="<%=riga.getStringa() %>" />
						<liferay-ui:search-container-column-text property="pianteRipiano" name="quantity" />
						<liferay-ui:search-container-column-text property="ean" name="barcode" />
						<liferay-ui:search-container-column-text property="prezzoEtichetta" name="label-price" />
						<liferay-ui:search-container-column-text property="stringa" name="note" />
						<liferay-ui:search-container-column-text name="received">
							<c:if test="<%=riga.getRicevuto()%>">
								<span class="badge badge-success">Si</span>
<%-- 								<liferay-ui:icon image="activate" message="yes" /> --%>
				          	</c:if>
				          	<c:if test="<%=!riga.getRicevuto()%>">
				          		<span class="badge badge-warning">No</span>
<%-- 				              	<liferay-ui:icon image="deactivate" message="no"/> --%>
				          	</c:if>
						</liferay-ui:search-container-column-text>
						<%-- <liferay-ui:search-container-row-parameter name="order_id"
							value="<%= String.valueOf(id) %>" /> 
						<liferay-ui:search-container-row-parameter name="supplier_id"
							value="<%= supplier_id %>" />
						<liferay-ui:search-container-row-parameter name="order_num"
							value="<%= order_num %>" />
						<liferay-ui:search-container-row-parameter name="cart_id"
							value="<%= "cart-" + String.valueOf(idCart) %>" />
						<liferay-ui:search-container-row-parameter name="backUrl"
							value="<%= backUrl %>" />
 --%>						<%-- <liferay-ui:search-container-column-jsp name="action"
		                	align="right"
		                	path="/html/receiveitems/row-action.jsp" /> --%>
					</liferay-ui:search-container-row>
					<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
							paginate="true" />
				</liferay-ui:search-container>	
			</liferay-ui:panel>
			<%
				}
			%>
    </c:when>
	<c:otherwise>
		<div class="portlet-msg-info">
			<liferay-ui:message key="no-orders" />
		</div>
	</c:otherwise>
</c:choose>

<script>

var cartId = '<%= cartId %>';

AUI().use('anim', 'node', function(A) {
	A.on('domready', function() {
		if (cartId !== '') {
			var target = A.one('#cart-' + cartId);
			if (target) {
				var anim = new A.Anim({
					node : 'html, body',
					to : {
						scrollTop : target.getY()
					},
					duration : 0.3
				// Animation duration in seconds
				});
				anim.run();
			}
		}
	});
});
</script>

<%!private static Log _log = LogFactoryUtil
            .getLog("docroot.html.orderprocessing.order_jsp");%>