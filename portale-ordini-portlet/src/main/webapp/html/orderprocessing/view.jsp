<%@page import="it.bysoftware.ct.service.RigaLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Riga"%>
<%@page import="it.bysoftware.ct.service.CarrelloLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Carrello"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="java.util.Date"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="it.bysoftware.ct.service.OrdineLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.dao.search.DisplayTerms"%>
<%@page import="com.liferay.portal.kernel.util.OrderByComparator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<%@page import="java.util.List"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@include file="../init.jsp"%>

<%
	String sortByCol = ParamUtil.getString(request, "orderByCol", "ship-date"); 
	String sortByType = ParamUtil.getString(request, "orderByType", "asc");
	String customerCode = ParamUtil.getString(request, "customerCode");
	String customerName = ParamUtil.getString(request, "customerName");
	String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
	
	Calendar calendar = Calendar.getInstance();
	calendar.clear();
	calendar = Utils.today();
	long defFrom = calendar.getTimeInMillis();
	calendar.add(calendar.DAY_OF_MONTH, 7);
	long defTo = calendar.getTimeInMillis();
	long orderDateLng = ParamUtil.getLong(request, "orderDateLng", defFrom);
	long shipDateLng = ParamUtil.getLong(request, "shipDateLng", defTo);	
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
%>

<liferay-portlet:renderURL varImpl="orderSearchURL">
	<portlet:param name="mvcPath" value="/html/orderprocessing/view.jsp" />
</liferay-portlet:renderURL>
<aui:form action="<%=orderSearchURL %>" method="post" name="orderForm">
    <liferay-portlet:renderURLParams varImpl="orderSearchURL" />
    <liferay-portlet:renderURL varImpl="iteratorURL1">
        <portlet:param name="customerCode" value="<%= customerCode %>" />
        <portlet:param name="customerName" value="<%= customerName %>" />
        <portlet:param name="orderDateLng" value="<%= String.valueOf(orderDateLng) %>" />
        <portlet:param name="shipDateLng" value="<%= String.valueOf(shipDateLng) %>" />
        <portlet:param name="mvcPath" value="/html/orderprocessing/view.jsp" />
    </liferay-portlet:renderURL>
    <liferay-ui:search-container displayTerms="<%= new DisplayTerms(renderRequest) %>" delta="75"
        emptyResultsMessage="no-orders" headerNames="code,company-name,order-date,ship-date"
        iteratorURL="<%= iteratorURL1 %>" orderByCol="<%= sortByCol %>" orderByType="<%= sortByType %>">
        <aui:nav-bar>
        	<aui:nav>
        		<aui:nav-bar-search cssClass="pull-right" >
        			<liferay-ui:search-form page="/html/orderprocessing/order-search.jsp"
        				servletContext="<%= application %>"/>
        		</aui:nav-bar-search>
        	</aui:nav>
        </aui:nav-bar>
        <liferay-ui:search-container-results>
            <%
            OrderByComparator orderByComparator =        
            Utils.getOrderByComparator(sortByCol, sortByType);
            DisplayTerms displayTerms = searchContainer.getDisplayTerms();
            List<Ordine> orders = new ArrayList<Ordine>();
            if (displayTerms.isAdvancedSearch()) {
                orders = OrdineLocalServiceUtil.findOrders(customerCode, new Date(orderDateLng), new Date(shipDateLng), true, false, true, true, true,
                        displayTerms.isAndOperator(), 0, OrdineLocalServiceUtil.getFindOrdersCount(customerCode, new Date(orderDateLng), new Date(shipDateLng), true, false, true, true, true, displayTerms.isAndOperator()),
                        orderByComparator); 
            } else {
                String searchkeywords = displayTerms.getKeywords();
                String numbesearchkeywords = String.valueOf(0);
                if (Validator.isNumber(searchkeywords)) {
                    numbesearchkeywords = searchkeywords;
                }
                orders = OrdineLocalServiceUtil.findOrders(searchkeywords, new Date(orderDateLng), new Date(shipDateLng), true, false, true, true, true, true,
                        0, OrdineLocalServiceUtil.getFindOrdersCount(searchkeywords, new Date(orderDateLng), new Date(shipDateLng), true, false, true, true, true, true),
                        orderByComparator);  
            }
            
            if (displayTerms.isAdvancedSearch() && customerName != null && !customerName.isEmpty()) {
                List<Ordine> tmp = new ArrayList<Ordine>();
                for (Ordine o : orders) {
                    AnagraficheClientiFornitori customer =
                            AnagraficheClientiFornitoriLocalServiceUtil.getAnagraficheClientiFornitori(o.getIdCliente());
                    if (customer.getRagioneSociale1().toLowerCase().contains(customerName.toLowerCase()) || customer.getRagioneSociale2().toLowerCase().contains(customerName.toLowerCase())) {
                        tmp.add(o);
                    }
                }
                
                orders = tmp;
            } else {
                String searchkeywords = displayTerms.getKeywords();
                List<Ordine> tmp = new ArrayList<Ordine>();
                for (Ordine o : orders) {
                    AnagraficheClientiFornitori customer =
                            AnagraficheClientiFornitoriLocalServiceUtil.getAnagraficheClientiFornitori(o.getIdCliente());
                    if (customer.getRagioneSociale1().toLowerCase().contains(searchkeywords.toLowerCase()) || customer.getRagioneSociale2().toLowerCase().contains(searchkeywords.toLowerCase())) {
                        tmp.add(o);
                    }
                }
                
                orders = tmp;
            }
            
            results = ListUtil.subList(orders,
                                    searchContainer.getStart(),
                                    searchContainer.getEnd());
                            total = orders.size();
                            pageContext.setAttribute("results", results);
                            pageContext.setAttribute("total", total);
            %>
        </liferay-ui:search-container-results>
        <liferay-ui:search-container-row className="it.bysoftware.ct.model.Ordine"
            keyProperty="id" modelVar="order">
            <liferay-portlet:renderURL varImpl="rowURL">
                <portlet:param name="orderId" value="<%= String.valueOf(order.getId()) %>" />
            </liferay-portlet:renderURL>
            <liferay-ui:search-container-column-text name="number" value="<%=order.getNumero() + " / " + order.getCentro() %>" orderable="<%= true %>"/>
            <%
            String orderDateStr = sdf.format(order.getDataInserimento());
            String shipDateStr = sdf.format(order.getDataConsegna());
            AnagraficheClientiFornitori customer =
                    AnagraficheClientiFornitoriLocalServiceUtil.fetchAnagraficheClientiFornitori(order.getIdCliente());
            String[] customerInfo = new String[] { "", "", "", "", "", "" };
            if (customer != null) {
            	customerInfo[0] = customer.getRagioneSociale1() + " " + customer.getRagioneSociale2();
            	customerInfo[1] = customer.getIndirizzo();
            	customerInfo[2] = customer.getCAP();
            	customerInfo[3] = customer.getComune();
            	customerInfo[4] = customer.getSiglaProvincia();
            	customerInfo[5] = customer.getSiglaStato();
            }
            %>
            <liferay-ui:search-container-column-text name="code" value="<%= order.getIdCliente() %>"/>
            <liferay-ui:search-container-column-text name="company-name" orderable="true">
            	<liferay-ui:message key="company-name-x" arguments="<%= customerInfo %>" />
            </liferay-ui:search-container-column-text>
            <liferay-ui:search-container-column-text name="order-date"  value="<%=orderDateStr %>" orderable="<%= true %>"/>
            <liferay-ui:search-container-column-text name="ship-date"  value="<%=shipDateStr %>" orderable="<%= true %>"/>
            <liferay-ui:search-container-column-text name="carrier" value="<%= order.getVettore() %>" orderable="<%= true %>"/>
            <liferay-ui:search-container-column-text name="note" >
            	<c:if test="<%= !order.getNote().isEmpty() %>">
            		<input id="inputNote_<%= order.getId()%>" type="hidden" value="<%=order.getNote() %>" />
            		<input id="myNote_<%= order.getId()%>" class="note-btn" type="button" value="Vedi" />
            		
            	</c:if>
            </liferay-ui:search-container-column-text>
            <%-- <c:choose>
                <c:when test="<%= order.getStato() >= 0 && order.getStato() < 7 %>">
                    <liferay-ui:search-container-column-text name="status" orderable="true">
                        <aui:workflow-status showIcon="<%= false %>" showLabel="<%= false %>"
                            status="<%= order.getStato() %>" />
                    </liferay-ui:search-container-column-text>
                </c:when>
                <c:otherwise>
                    <liferay-ui:search-container-column-text name="status" value=""  orderable="true"/>
                </c:otherwise>
            </c:choose> --%>
            <%
            boolean completo = true;
            
            List<Carrello> carts = CarrelloLocalServiceUtil.findCartsInOrder(order.getId());
            String[][] tmp = new String[carts.size()][6];
       		for (int i = 0; i < carts.size(); i++) {
            	int ric = 0;
            	Carrello cart = carts.get(i);
            	List<Riga> rows = RigaLocalServiceUtil.findRowsInCart(cart.getId());
            	for (Riga r : rows) {
            		completo = completo && r.getRicevuto();
            		if (r.getRicevuto()) {
            			ric++;
            		}
            	}
            	if (ric == 0) {
        			tmp[i][0] = "text-error";
        			tmp[i][1] = "Nessun articolo ricevuto";
        		} else if (ric > 0 && ric < rows.size()) {
        			tmp[i][0] = "text-warning";
        			tmp[i][1] = "Alcuni articoli ricevuti";
        		} else {
        			tmp[i][0] = "text-success";
        			tmp[i][1] = "Il carrello � completo";
        		}
            	if (cart.getLavorato()) {
            		tmp[i][2] = "badge-success";
        			tmp[i][3] = "Lavorato";
            	} else {
            		tmp[i][2] = "badge-warning";
        			tmp[i][3] = "Non lavorato";
            	}
            	tmp[i][4] = "" + cart.getId();
            	tmp[i][5] = "Ricevuti: " + ric + "/" + rows.size() + " pianali";
            }
            %>
            <c:if test="<%=completo%>">
           		<liferay-ui:search-container-column-text name="Stato ordine">
					<%-- <liferay-ui:icon image="activate" message="yes" /> --%>
					<span class="badge badge-success">Completo</span>
				</liferay-ui:search-container-column-text>
          	</c:if>
            <c:if test="<%=!completo%>">
          		<liferay-ui:search-container-column-text name="complete">
              		<%-- <liferay-ui:icon image="deactivate" message="no"/> --%>
              		<span class="badge badge-warning">Non completo</span>
              	</liferay-ui:search-container-column-text>
          	</c:if>
          	<liferay-ui:search-container-column-text name="Dett. carrelli">
		    <%
		    for (int i = 0; i < tmp.length; i++) {
		    %>
		    	<i class="icon-shopping-cart large-icon <%=tmp[i][0]%>" title="<%=tmp[i][1]%>"></i><span class="<%=tmp[i][0]%>"> <%=tmp[i][5]%> </span><span class="badge <%=tmp[i][2]%>"><%=tmp[i][3]%></span><br/>
		    <%
	        }
		    %>
			</liferay-ui:search-container-column-text>
            <liferay-ui:search-container-row-parameter name="recap" value="true" />
            <liferay-ui:search-container-row-parameter name="backURL" value="<%= iteratorURL1.toString() %>" />
            <liferay-ui:search-container-column-jsp path="/html/orderprocessing/action.jsp" />
        </liferay-ui:search-container-row>
        <liferay-ui:search-iterator searchContainer="<%=searchContainer %>" />
    </liferay-ui:search-container>
</aui:form>

<%!private static Log _log = LogFactoryUtil
            .getLog("docroot.html.orderprocessing.view_jsp");%>