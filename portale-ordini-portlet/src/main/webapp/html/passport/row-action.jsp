<%@page import="org.apache.xmlbeans.impl.tool.Extension.Param"%>
<%@page import="it.bysoftware.ct.model.Piante"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="../init.jsp"%>

<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    Piante pianta = (Piante) row.getObject(); 
    String orderId = String.valueOf(row.getParameter("orderId"));
    String partnerCode = String.valueOf(row.getParameter("partnerCode"));
    String customerCode = String.valueOf(row.getParameter("customerCode"));
    String traceCode =  String.valueOf(row.getParameter("traceCode"));
    boolean processed = Boolean.parseBoolean(String.valueOf(row.getParameter("processed")));
    String backUrl = (String) row.getParameter("backUrl");
    String prevUrl = (String) row.getParameter("prevUrl");
    String quantity = String.valueOf(row.getParameter("quantity"));
%>

<liferay-ui:icon-menu>
	<liferay-portlet:renderURL varImpl="setPassportURL">
		<portlet:param name="mvcPath" value="/html/passport/traceability-code.jsp" />
		<portlet:param name="backUrl" value="<%=backUrl%>" />
		<portlet:param name="prevUrl" value="<%=prevUrl%>" />
		<portlet:param name="piantaId" value="<%=String.valueOf(pianta.getId())%>" />
		<portlet:param name="orderId" value="<%=orderId%>" />
		<portlet:param name="processed" value="<%=String.valueOf(processed)%>" />
		<portlet:param name="partnerCode" value="<%=partnerCode%>" />
		<portlet:param name="customerCode" value="<%=customerCode%>" />
		<portlet:param name="quantity" value="<%=quantity%>" />
		<portlet:param name="oldPassport" value="<%=traceCode%>" />
	</liferay-portlet:renderURL>
	<c:choose>
	<c:when test="<%=traceCode.isEmpty() %>">
	   <liferay-ui:icon image="add" url="${setPassportURL}" message="set-passport" />
	</c:when>
	<c:otherwise>
	   <liferay-ui:icon image="edit" url="${setPassportURL}" message="edit" />
	</c:otherwise>
	</c:choose>
</liferay-ui:icon-menu>