<%@page import="it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK"%>
<%@page import="it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.ClientiFornitoriDatiAgg"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="it.bysoftware.ct.service.OrdineLocalServiceUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<%@page import="java.util.List"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%
    String partnerSelectedProc = ParamUtil.getString(renderRequest, "partnerSelectedProc", "");
    long fromLng = ParamUtil.getLong(renderRequest, "dateFrom", 0);
    Calendar calendarFrom = Calendar.getInstance();
    if (fromLng > 0) {
        calendarFrom.setTimeInMillis(fromLng);
    } else {
        calendarFrom = Utils.today();
    }
    Date from = calendarFrom.getTime();

    Calendar calendarTo = Calendar.getInstance();
    long toLng = ParamUtil.getLong(renderRequest, "dateTo", 0);
    if (toLng > 0) {
        calendarTo.setTimeInMillis(toLng);
    } else {
        calendarTo = Utils.today();
        calendarTo.add(Calendar.DAY_OF_MONTH, 1);
    }
    Date to = calendarTo.getTime();
    
    List<Object[]> tmp1 = new ArrayList<Object[]>();
    if (!partnerCode.isEmpty()) {
        tmp1 = OrdineLocalServiceUtil.getOrderWithPassportByPartnerAndDate(
                partnerCode, from, to);
    } else {
        if (!"".equals(partnerSelectedProc)) {
            tmp1 = OrdineLocalServiceUtil.getOrderWithPassportByPartnerAndDate(partnerSelectedProc, from, to);
        } else {
            tmp1 = OrdineLocalServiceUtil.getOrderWithPassportByDate(from, to);
        }
    }
    List<Ordine> orders1 = new ArrayList<Ordine>();
    for (Object[] object : tmp1) {
        long orderId = Long.parseLong(String.valueOf(object[0]));
        orders1.add(OrdineLocalServiceUtil.getOrdine(orderId));
    }
    
    int j = 0;
    List<AnagraficheClientiFornitori> partners = new ArrayList<AnagraficheClientiFornitori>();
%>

<liferay-portlet:renderURL varImpl="reloadURL">
    <portlet:param name="activeTab" value="passport-processed" />
    <portlet:param name="mvcPath" value="/html/passport/view.jsp" />
</liferay-portlet:renderURL>
<aui:form id="aForm" name="aForm" action="<%=reloadURL%>"
    method="post">
    <liferay-ui:input-date name="fromDate" cssClass="input-small"
        dayValue="<%=calendarFrom.get(Calendar.DAY_OF_MONTH)%>"
        dayParam="fromDay" monthValue="<%=calendarFrom.get(Calendar.MONTH)%>"
        monthParam="fromMonth"
        yearValue="<%=calendarFrom.get(Calendar.YEAR)%>" yearParam="fromYear" />
    <liferay-ui:input-date name="toDate" cssClass="input-small"
        dayValue="<%=calendarTo.get(Calendar.DAY_OF_MONTH)%>"
        dayParam="toDay" monthValue="<%=calendarTo.get(Calendar.MONTH)%>"
        monthParam="toMonth" yearValue="<%=calendarTo.get(Calendar.YEAR)%>"
        yearParam="toYear" />
    <c:if test="<%="".equals(partnerCode) %>">
    <%
     partners = AnagraficheClientiFornitoriLocalServiceUtil.findSuppliers();
     %>
     <aui:select name="partnerSelectedProc" showEmptyOption="true" label="partner">
        <%for (AnagraficheClientiFornitori partner : partners) { %>
            <aui:option value="<%=partner.getCodiceAnagrafica() %>"
                label="<%=partner.getRagioneSociale1() %>"
                selected="<%=partnerSelectedProc.equals(partner.getCodiceAnagrafica()) %>"/>
        <%} %>
     </aui:select>
    </c:if>        
    <aui:button-row>
        <aui:button name="searchProcessedBtn" type="button" value="search" />
        <aui:button type="cancel" onclick="this.form.reset()" />
    </aui:button-row>
</aui:form>

<liferay-portlet:renderURL varImpl="iteratorURL1">
    <portlet:param name="dateFrom" value="<%=String.valueOf(fromLng) %>"/>
    <portlet:param name="dateTo" value="<%=String.valueOf(toLng) %>"/>
    <portlet:param name="partnerSelectedProc" value="<%=String.valueOf(partnerSelectedProc) %>"/>
    <portlet:param name="activeTab" value="passport-processed" />
    <portlet:param name="mvcPath" value="/html/passport/view.jsp" />
</liferay-portlet:renderURL>
<liferay-ui:search-container delta="75" emptyResultsMessage="no-orders"
     iteratorURL="<%=iteratorURL1%>" >
    <liferay-ui:search-container-results>
        <%
            results = ListUtil.subList(orders1,
                                    searchContainer.getStart(),
                                    searchContainer.getEnd());
                            total = orders1.size();
                            pageContext.setAttribute("results", results);
                            pageContext.setAttribute("total", total);
        %>
    </liferay-ui:search-container-results>
    <liferay-ui:search-container-row
        className="it.bysoftware.ct.model.Ordine" modelVar="order">
        <%
        sdf.applyLocalizedPattern("dd/MM/yyyy");
        String orderDate = sdf.format(order.getDataInserimento());
        String shipDate = sdf.format(order.getDataConsegna());
        AnagraficheClientiFornitori c = AnagraficheClientiFornitoriLocalServiceUtil.
                fetchAnagraficheClientiFornitori(String.valueOf(order.getIdCliente()));
        AnagraficheClientiFornitori partner = null;
        %>
        <liferay-ui:search-container-column-text name="confirm-number"
            value="<%= order.getNumero() + "/" + order.getCentro() %>" />
        <liferay-ui:search-container-column-text value="<%= orderDate %>" name="date" />
        <liferay-ui:search-container-column-text value="<%= shipDate %>"name="ship-date" />
        <liferay-ui:search-container-column-text
            value="<%=c != null ? c.getRagioneSociale1()
                            : ""%>"
            name="customer" />
        <c:if test="<%= partnerCode.isEmpty() %>">
            <%
            partner = AnagraficheClientiFornitoriLocalServiceUtil.
                    fetchAnagraficheClientiFornitori(String.valueOf(tmp1.get(j)[1]));
            if (Validator.isNotNull(partner)) {
                ClientiFornitoriDatiAgg data = ClientiFornitoriDatiAggLocalServiceUtil.
                        getClientiFornitoriDatiAgg(new ClientiFornitoriDatiAggPK(
                                String.valueOf(tmp1.get(j)[1]), true));
            %>
                <liferay-ui:search-container-column-text name="supplier"
                    value="<%= partner.getRagioneSociale1() %>"/>
                <c:if test="<%=!data.getAssociato() %>">
                    <liferay-ui:search-container-column-text name="">
                        <liferay-ui:icon image="organization_icon" message="company" />
                    </liferay-ui:search-container-column-text>
                </c:if>
                <c:if test="<%=data.getAssociato() %>">
                    <liferay-ui:search-container-column-text name="">
                        <liferay-ui:icon image="user_icon" message="partner" />
                    </liferay-ui:search-container-column-text>
                </c:if>
            <%
            }
            j++; 
            %>
        </c:if>
<%--         <liferay-ui:search-container-column-text name="passport" value="<%= String.valueOf(tmp1.get(j)[2]) %>"/> --%>
        <liferay-ui:search-container-row-parameter name="orderId"
            value="<%=order.getId()%>" />
        <liferay-ui:search-container-row-parameter name="processed"
            value="<%=true%>" />
        <liferay-ui:search-container-row-parameter name="partnerCode"
            value="<%=(partnerCode.isEmpty()) ? partner.getCodiceAnagrafica() : partnerCode %>" />
        <liferay-ui:search-container-column-jsp path="/html/passport/passport-action.jsp" />
        <liferay-ui:search-container-row-parameter name="backUrl"
            value="<%= iteratorURL1.toString() %>" />
    </liferay-ui:search-container-row>
    <liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
        paginate="true" />
</liferay-ui:search-container>

<aui:script>
AUI().use('aui-base', 'datatype-number', function(A) {
    A.one('#<portlet:namespace/>searchProcessedBtn').on('click', function() {
        var fromDateStr = A.one('#<portlet:namespace />fromDate').get('value');
        var fromDate = parseDate(fromDateStr);
        var toDateStr = A.one('#<portlet:namespace/>toDate').get('value');
        var toDate = parseDate(toDateStr);
        if (A.one('#<portlet:namespace/>partnerSelectedProc') !== null) {
	        var partenrSelectedProc = A.one('#<portlet:namespace/>partnerSelectedProc').get('value');
	        window.location.href = '<%=reloadURL%>&<portlet:namespace/>dateFrom=' + fromDate.getTime() + '&<portlet:namespace/>dateTo=' + toDate.getTime() + '&<portlet:namespace/>partnerSelectedProc=' + partenrSelectedProc;
        } else {
        	window.location.href = '<%=reloadURL%>&<portlet:namespace/>dateFrom=' + fromDate.getTime() + '&<portlet:namespace/>dateTo=' + toDate.getTime();
        }
    });
});
function parseDate(string) {
    var tmp = string.split('/');
    return new Date(tmp[2] + '-' + tmp[1] + '-' + tmp[0]);
}
</aui:script>