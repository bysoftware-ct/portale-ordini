<%@page import="it.bysoftware.ct.service.OrdineLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="../init.jsp"%>

<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    Ordine order = (Ordine) row.getObject(); 
    String orderId = String.valueOf(row.getParameter("orderId"));
    String partnerCode = String.valueOf(row.getParameter("partnerCode"));
    boolean processed = Boolean.parseBoolean(String.valueOf(row.getParameter("processed")));
    String backUrl = (String) row.getParameter("backUrl");
%>

<liferay-ui:icon-menu>
	<liferay-portlet:renderURL varImpl="setPassportURL">
		<portlet:param name="mvcPath" value="/html/passport/order.jsp" />
		<portlet:param name="backUrl" value="<%=backUrl%>" />
		<portlet:param name="orderId" value="<%=String.valueOf(order.getId())%>" />
		<portlet:param name="customerCode" value="<%=String.valueOf(order.getIdCliente())%>" />
		<portlet:param name="processed" value="<%=String.valueOf(processed)%>" />
		<portlet:param name="partnerCode" value="<%=partnerCode%>" />
	</liferay-portlet:renderURL>
	<c:choose>
	<c:when test="<%=!processed %>">
	   <liferay-ui:icon image="edit" url="${setPassportURL}" message="set-passport" />
	</c:when>
	<c:otherwise>
	   <liferay-ui:icon image="search" url="${setPassportURL}" message="view" />
	</c:otherwise>
	</c:choose>
</liferay-ui:icon-menu>