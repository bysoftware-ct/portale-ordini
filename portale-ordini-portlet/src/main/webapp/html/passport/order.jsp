<%@page import="com.liferay.portal.model.User"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="com.liferay.portal.service.ClassNameLocalServiceUtil"%>
<%@page import="com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil"%>
<%@page import="com.liferay.portlet.expando.model.ExpandoValue"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="it.bysoftware.ct.service.CategorieMerceologicheLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.CategorieMerceologiche"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.service.PianteLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Piante"%>
<%@page import="it.bysoftware.ct.service.ArticoliLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Articoli"%>
<%@page import="it.bysoftware.ct.service.RigaLocalServiceUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="it.bysoftware.ct.model.Riga"%>
<%@page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="it.bysoftware.ct.service.CarrelloLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Carrello"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<%@page import="it.bysoftware.ct.service.OrdineLocalServiceUtil"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../init.jsp"%>

<%
    long orderId = ParamUtil.getLong(renderRequest, "orderId", 0);
    boolean processed = ParamUtil.getBoolean(renderRequest, "processed", false);
    String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
    String backUrl = ParamUtil.getString(renderRequest, "backUrl");
    String customerCode = ParamUtil.getString(renderRequest,
            "customerCode");
    String partnerCode = ParamUtil.getString(renderRequest,
            "partnerCode");
    ExpandoValue value = ExpandoValueLocalServiceUtil.getValue(
            user.getCompanyId(),
            ClassNameLocalServiceUtil.getClassNameId(User.class),
            "CUSTOM_FIELDS", Constants.REGISTRY_CODE, user.getUserId());
    
    List<Piante> items = new ArrayList<Piante>();
    List<String> passports = new ArrayList<String>();
    AnagraficheClientiFornitori customer = null;
    AnagraficheClientiFornitori supplier = null;
    Ordine order = null;
    String lbl = "";
    String oldPassport = "";
    int idx = 0;
    Map<String, Integer> itemsCounter = new HashMap<String, Integer>();
    if (orderId > 0) {
        customer = AnagraficheClientiFornitoriLocalServiceUtil
                .getAnagraficheClientiFornitori(customerCode);
        supplier = AnagraficheClientiFornitoriLocalServiceUtil
                .getAnagraficheClientiFornitori(partnerCode);
        order = OrdineLocalServiceUtil.getOrdine(orderId);
        lbl = LanguageUtil.format(pageContext, "order-x",
                new String[] { String.valueOf(order.getNumero()),
                        String.valueOf(order.getCentro()) });
        Map<String, Piante> itemsWrapper = new HashMap<String, Piante>();
        Map<String, String> passportWrapper = new HashMap<String, String>();
        List<Carrello> carts = CarrelloLocalServiceUtil
                .findCartsInOrder(orderId);
        for (Carrello cart : carts) {
            List<Riga> rows = RigaLocalServiceUtil.findRowsInCart(cart
                    .getId());
            String oldCode = "";
            for (Riga r : rows) {
                Piante p = PianteLocalServiceUtil.getPiante(r
                        .getIdArticolo());
                if (supplier.getCodiceAnagrafica().equals(p.getIdFornitore())) {
                    if (!r.getPassaporto().isEmpty()) {
                        oldPassport = r.getPassaporto();
                    }
                    if (itemsCounter.containsKey(p.getCodice())) {
                        itemsCounter.put(p.getCodice(), r.getPianteRipiano() 
                                + itemsCounter.get(p.getCodice()));
                    } else {
                        itemsCounter.put(p.getCodice(), r.getPianteRipiano());
                    }
                }
                if (partnerCode.equals(p.getIdFornitore()) && p.getPassaporto()) {
                    itemsWrapper.put(p.getCodice(), p);
                    passportWrapper.put(p.getCodice(), r.getPassaporto());
//                     oldCode = p.getCodice();
                }
//                 itemsWrapper.put(p.getCodice(), p);
            }
        }
        for (String k : itemsWrapper.keySet()) {
            items.add(itemsWrapper.get(k));
            passports.add(passportWrapper.get(k));
        }
    }
%>
<liferay-portlet:actionURL name="saveDefaultPassportNumber" var="saveDefaultPassportNumber" >
    <liferay-portlet:param name="orderId" value="<%= String.valueOf(order.getId()) %>"/>
    <liferay-portlet:param name="partnerCode" value="<%= partnerCode %>"/>
    <liferay-portlet:param name="customerCode" value="<%= customerCode %>"/>
    <liferay-portlet:param name="backUrl" value="<%= backUrl %>"/>
</liferay-portlet:actionURL>
<aui:nav-bar>
    <aui:nav>
       <aui:nav-item id="confirmBtn" cssClass="green" iconCssClass=" icon-ok green" label="confirm" 
            selected='<%="save".equals(toolbarItem)%>' />
    </aui:nav>
</aui:nav-bar>
<c:choose>
	<c:when test="<%=order != null%>">
	    <liferay-ui:header title="<%=lbl %>" backURL="<%=backUrl %>" />
		<liferay-portlet:renderURL varImpl="iteratorURL">
			<portlet:param name="processed" value="<%=String.valueOf(processed) %>"/>
			<portlet:param name="partnerCode" value="<%=partnerCode %>"/>
			<portlet:param name="backUrl" value="<%=backUrl %>"/>
			<portlet:param name="orderId" value="<%=String.valueOf(orderId) %>"/>
			<portlet:param name="customerCode" value="<%=customerCode %>"/>
			<portlet:param name="mvcPath" value="/html/passport/order.jsp" />
		</liferay-portlet:renderURL>
		
			<aui:layout>
			    <aui:column columnWidth="45" first="true">
					<aui:input name="customerName" id="customerName" type="textarea"
						label="customer" disabled="true" resizable="false"
						cssClass="input-xlarge" inlineField="true"
						value="<%=customer.getRagioneSociale1() + "\n"
	                                    + customer.getRagioneSociale2()%>" />
					<aui:input name="address" id="address" type="textarea"
						label="address" disabled="true" cssClass="input-xlarge"
						inlineField="true" value="<%=Utils.getFullAddress(customer)%>" />
				</aui:column>
				<c:if test="<%=value == null %>">
				    <aui:column columnWidth="45" last="true">
						<aui:input name="supplierName" id="supplierName" type="textarea"
		                        label="supplier" disabled="true" resizable="false"
		                        cssClass="input-xlarge" inlineField="true"
		                        value="<%=supplier.getRagioneSociale1() + "\n"
		                                        + supplier.getRagioneSociale2()%>" />
	                    <aui:input name="address" id="address" type="textarea"
	                        label="address" disabled="true" cssClass="input-xlarge"
	                        inlineField="true" value="<%=Utils.getFullAddress(supplier)%>" />
                    </aui:column>
                </c:if>
			</aui:layout>
		<aui:fieldset label="items-summary">
			<aui:form id="aForm" name="aForm" method="post">
<%--                 <aui:button-row> --%>
<%--                     <aui:button value="confirm" id="confirmBtn"/> --%>
<%--                 </aui:button-row> --%>
				<aui:input name="itemCodes" type="hidden"/>
				<liferay-ui:search-container delta="75" emptyResultsMessage="no-orders"
					 iteratorURL="<%=iteratorURL%>" rowChecker="<%=new RowChecker(renderResponse)%>">
					<liferay-ui:search-container-results>
						<%
						    results = ListUtil.subList(items,
						                            searchContainer.getStart(),
						                            searchContainer.getEnd());
						                    total = items.size();
						                    pageContext.setAttribute("results", results);
						                    pageContext.setAttribute("total", total);
						%>
					</liferay-ui:search-container-results>
					<liferay-ui:search-container-row keyProperty="codice"
						className="it.bysoftware.ct.model.Piante" modelVar="item">
						<%
						String forma = "";
			            Articoli art = ArticoliLocalServiceUtil.fetchArticoli(item.getCodice());
			            if(art != null) {
			                try {
			                    CategorieMerceologiche cat = CategorieMerceologicheLocalServiceUtil.fetchCategorieMerceologiche(art.getCategoriaMerceologica());
			                    if (cat != null) {
			                        forma = cat.getDescrizione();
			                    }
			                } catch (Exception ex) {
			                    _log.debug(ex.getMessage());
			                }
			            }
			            String traceCode = "";
			            if(idx < passports.size()){
			            	traceCode = passports.get(idx);
			            }
			            idx++;
						%>
						<liferay-ui:search-container-column-text property="codice" name="code" />
						<liferay-ui:search-container-column-text property="nome" name="name" />
						<liferay-ui:search-container-column-text name="shape" value="<%=forma %>"/>
						<liferay-ui:search-container-column-text property="vaso" name="vase" />
						<liferay-ui:search-container-column-text name="H" property="altezzaPianta"/>
						<liferay-ui:search-container-column-text name="quantity" value="<%=String.valueOf(itemsCounter.get(item.getCodice())) %>" />
						<c:choose>
							<c:when test="<%=!traceCode.isEmpty() %>">
								<liferay-ui:search-container-column-text name="passport-number" value="<%=traceCode %>"/>
							</c:when>
							<c:otherwise>
								<liferay-ui:search-container-column-text name="passport-number">
									<liferay-ui:message key="passport-default-x" 
									arguments="<%= 
									item.getPassaportoDefault() != null &&
										!item.getPassaportoDefault().isEmpty() ? 
												item.getPassaportoDefault() + "*" :
													"" %>" />
								</liferay-ui:search-container-column-text>
							</c:otherwise>
						</c:choose>
						<liferay-ui:search-container-row-parameter name="traceCode"
							value="<%=traceCode %>"/>
						<liferay-ui:search-container-row-parameter name="orderId"
	                        value="<%=String.valueOf(order.getId())%>" />
	                    <liferay-ui:search-container-row-parameter name="quantity"
	                        value="<%=String.valueOf(itemsCounter.get(item.getCodice()))%>" />
	                    <liferay-ui:search-container-row-parameter name="partnerCode"
	                        value="<%=partnerCode%>" />
	                    <liferay-ui:search-container-row-parameter name="customerCode"
	                        value="<%=customerCode%>" />
	                    <liferay-ui:search-container-row-parameter name="prevUrl"
	                        value="<%= backUrl %>" />
	                    <liferay-ui:search-container-row-parameter name="backUrl"
	                        value="<%= PortalUtil.getCurrentURL(request) %>" />
						<liferay-ui:search-container-column-jsp
	                	align="right"
	                	path="/html/passport/row-action.jsp" />
					</liferay-ui:search-container-row>
					<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
						paginate="true" />
				</liferay-ui:search-container>
				<liferay-ui:message key="to-be-confirmed" />
			</aui:form>
		</aui:fieldset>
	</c:when>
	<c:otherwise>
		<div class="portlet-msg-info">
			<liferay-ui:message key="no-order" />
		</div>
	</c:otherwise>
</c:choose>

<aui:script>
AUI().use('liferay-util-list-fields', function(A) {
    A.one('#<portlet:namespace/>confirmBtn').on('click', function(event) {
    	var selectedItems = Liferay.Util.listCheckedExcept('#<portlet:namespace/>aForm', '#<portlet:namespace/>allRowIds');
    	A.one('#<portlet:namespace/>itemCodes').set('value', selectedItems);
    	if (selectedItems.length == "" || selectedItems == null ) {
    		alert('Nessun pagamento selezionato.');
    	} else {
   			if (confirm('Procedere con l\'assegnazione di tutti i passaporti di default?') === true) {
   	            submitForm(document.<portlet:namespace />aForm, '<%=saveDefaultPassportNumber.toString() %>');
   	        }
    	}
    });
});
</aui:script>

<%!private static Log _log = LogFactoryUtil
            .getLog("docroot.html.passport.order_jsp");%>