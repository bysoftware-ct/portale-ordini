<%@page import="it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK"%>
<%@page import="it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.ClientiFornitoriDatiAgg"%>
<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<%@page import="it.bysoftware.ct.utils.OrderState"%>
<%@page import="it.bysoftware.ct.service.OrdineLocalServiceUtil"%>
<%@page import="com.liferay.portal.model.Role"%>
<%@page import="java.util.List"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.liferay.portal.service.ClassNameLocalServiceUtil"%>
<%@page
    import="com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil"%>
<%@page import="com.liferay.portlet.expando.model.ExpandoValue"%>

<%
    String partnerSelected = ParamUtil.getString(renderRequest, "partnerSelected", "");
    List<Ordine> orders = new ArrayList<Ordine>();
    List<Object[]> tmp = new ArrayList<Object[]>();
    if (!partnerCode.isEmpty()) {
        tmp = OrdineLocalServiceUtil.getOrderByCodeStatePassport(
                partnerCode, OrderState.PENDING.getValue(), "");
    } else {
    	if (!"".equals(partnerSelected)) {
    		tmp = OrdineLocalServiceUtil.getOrderByStatePassportPartner(
                    OrderState.PENDING.getValue(), "", partnerSelected);
        } else {
        	tmp = OrdineLocalServiceUtil.getOrderByStatePassport(
                    OrderState.PENDING.getValue(), "");
        }
        
    }
    
    for (Object[] object : tmp) {
        long orderId = Long.parseLong(String.valueOf(object[0]));
        orders.add(OrdineLocalServiceUtil.getOrdine(orderId));
    }
    
    int i = 0;
    List<AnagraficheClientiFornitori> listPartners = new ArrayList<AnagraficheClientiFornitori>();
%>

<liferay-portlet:renderURL varImpl="reloadURL1">
    <portlet:param name="activeTab" value="passport-pending" />
    <portlet:param name="mvcPath" value="/html/passport/view.jsp" />
</liferay-portlet:renderURL>

<aui:form id="aForm" name="aForm" action="<%=reloadURL1%>"
    method="post">
    <c:if test="<%="".equals(partnerCode) %>">
    <%
     listPartners = AnagraficheClientiFornitoriLocalServiceUtil.findSuppliers();
     %>
     <aui:select name="partnerSelected" showEmptyOption="true" label="partner">
        <%for (AnagraficheClientiFornitori partner : listPartners) { %>
            <aui:option value="<%=partner.getCodiceAnagrafica() %>"
                label="<%=partner.getRagioneSociale1() %>"
                selected="<%=partnerSelected.equals(partner.getCodiceAnagrafica()) %>"/>
        <%} %>
     </aui:select>
    </c:if>        
    <aui:button-row>
        <aui:button name="searchBtn" type="button" value="search" />
        <aui:button type="cancel" onclick="this.form.reset()" />
    </aui:button-row>
</aui:form>

<liferay-portlet:renderURL varImpl="iteratorURL">
	<portlet:param name="partnerSelected" value="<%=String.valueOf(partnerSelected) %>"/>
    <portlet:param name="activeTab" value="passport-pending" />
    <portlet:param name="mvcPath" value="/html/passport/view.jsp" />
</liferay-portlet:renderURL>

<liferay-ui:search-container delta="75" emptyResultsMessage="no-orders"
     iteratorURL="<%=iteratorURL%>" >
    <liferay-ui:search-container-results>
        <%
            results = ListUtil.subList(orders,
                                    searchContainer.getStart(),
                                    searchContainer.getEnd());
                            total = orders.size();
                            pageContext.setAttribute("results", results);
                            pageContext.setAttribute("total", total);
        %>
    </liferay-ui:search-container-results>
    <liferay-ui:search-container-row
        className="it.bysoftware.ct.model.Ordine" modelVar="order">
        <%
        sdf.applyLocalizedPattern("dd/MM/yyyy");
        String orderDate = sdf.format(order.getDataInserimento());
        String shipDate = sdf.format(order.getDataConsegna());
        AnagraficheClientiFornitori c = AnagraficheClientiFornitoriLocalServiceUtil.
                fetchAnagraficheClientiFornitori(String.valueOf(order.getIdCliente()));
        AnagraficheClientiFornitori partner = null;
        %>
        <liferay-ui:search-container-column-text name="confirm-number"
            value="<%= order.getNumero() + "/" + order.getCentro() %>" />
        <liferay-ui:search-container-column-text value="<%= orderDate %>" name="date" />
        <liferay-ui:search-container-column-text value="<%= shipDate %>"name="ship-date" />
        <liferay-ui:search-container-column-text
            value="<%=c != null ? c.getRagioneSociale1() : ""%>" name="customer" />
        <c:if test="<%= partnerCode.isEmpty() %>">
            <%
            partner = AnagraficheClientiFornitoriLocalServiceUtil.
                    fetchAnagraficheClientiFornitori(String.valueOf(tmp.get(i)[1]));
            if (Validator.isNotNull(partner)) {
                ClientiFornitoriDatiAgg data = ClientiFornitoriDatiAggLocalServiceUtil.
                        getClientiFornitoriDatiAgg(new ClientiFornitoriDatiAggPK(
                                String.valueOf(tmp.get(i)[1]), true));
            %>
                <liferay-ui:search-container-column-text name="supplier"
                    value="<%= partner.getRagioneSociale1() %>"/>
                <c:if test="<%=!data.getAssociato() %>">
                    <liferay-ui:search-container-column-text name="">
                        <liferay-ui:icon image="organization_icon" message="company" />
                    </liferay-ui:search-container-column-text>
                </c:if>
                <c:if test="<%=data.getAssociato() %>">
                    <liferay-ui:search-container-column-text name="">
                        <liferay-ui:icon image="user_icon" message="partner" />
                    </liferay-ui:search-container-column-text>
                </c:if>
            <%
            }
            i++; 
            %>
        </c:if>
        <liferay-ui:search-container-column-text name="status">
            <aui:workflow-status showIcon="<%= false %>" showLabel="<%= false %>"
                status="<%= order.getStato() %>" />
        </liferay-ui:search-container-column-text>
        <liferay-ui:search-container-row-parameter name="orderId"
                        value="<%=order.getId()%>" />
        <liferay-ui:search-container-row-parameter name="partnerCode"
                        value="<%=(partnerCode.isEmpty()) ? partner.getCodiceAnagrafica() : partnerCode %>" />
        <liferay-ui:search-container-column-jsp path="/html/passport/passport-action.jsp" />
        <liferay-ui:search-container-row-parameter name="backUrl"
            value="<%= iteratorURL.toString() %>" />
    </liferay-ui:search-container-row>
    <liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
        paginate="true" />
</liferay-ui:search-container>
<aui:script>
AUI().use('aui-base', 'datatype-number', function(A) {
    A.one('#<portlet:namespace/>searchBtn').on('click', function() {
        var fromDateStr = A.one('#<portlet:namespace />fromDate').get('value');
        var fromDate = parseDate(fromDateStr);
        var toDateStr = A.one('#<portlet:namespace/>toDate').get('value');
        var toDate = parseDate(toDateStr);
        if (A.one('#<portlet:namespace/>partnerSelected') !== null) {
	        var partenrSelected = A.one('#<portlet:namespace/>partnerSelected').get('value');
	        window.location.href = '<%=reloadURL1%>&<portlet:namespace/>dateFrom=' + fromDate.getTime() + '&<portlet:namespace/>dateTo=' + toDate.getTime() + '&<portlet:namespace/>partnerSelected=' + partenrSelected;
        } else {
        	window.location.href = '<%=reloadURL1%>&<portlet:namespace/>dateFrom=' + fromDate.getTime() + '&<portlet:namespace/>dateTo=' + toDate.getTime();
        }
    });
});
function parseDate(string) {
    var tmp = string.split('/');
    return new Date(tmp[2] + '-' + tmp[1] + '-' + tmp[0]);
}
</aui:script>