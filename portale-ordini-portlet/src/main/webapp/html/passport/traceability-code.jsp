<%@page import="it.bysoftware.ct.service.ArticoliLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Articoli"%>
<%@page import="it.bysoftware.ct.service.CategorieMerceologicheLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.CategorieMerceologiche"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="it.bysoftware.ct.service.PianteLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Piante"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="it.bysoftware.ct.service.OrdineLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="../init.jsp"%>

<%
long orderId = ParamUtil.getLong(renderRequest, "orderId", 0);
long piantaId = ParamUtil.getLong(renderRequest, "piantaId", 0);
Piante pianta = PianteLocalServiceUtil.getPiante(piantaId);
String oldPassport = ParamUtil.getString(renderRequest, "oldPassport", pianta.getPassaportoDefault());
String partnerCode = ParamUtil.getString(renderRequest,"partnerCode");
String customerCode = ParamUtil.getString(renderRequest,"customerCode");
String backUrl = ParamUtil.getString(renderRequest, "backUrl");
String prevUrl = ParamUtil.getString(renderRequest, "prevUrl");
Ordine order = OrdineLocalServiceUtil.getOrdine(orderId);
int quantitaOrdinata = ParamUtil.getInteger(renderRequest, "quantity");
String lbl = LanguageUtil.format(pageContext, "order-x",
        new String[] { String.valueOf(order.getNumero()),
        String.valueOf(order.getCentro()) });
String forma = "";
Articoli art = ArticoliLocalServiceUtil.fetchArticoli(pianta.getCodice());
if(art != null) {
    try {
        CategorieMerceologiche cat = CategorieMerceologicheLocalServiceUtil.fetchCategorieMerceologiche(art.getCategoriaMerceologica());
        if (cat != null) {
            forma = cat.getDescrizione();
        }
    } catch (Exception ex) {
        _log.debug(ex.getMessage());
    }
}
%>

<liferay-ui:header title="<%=lbl %>" backURL="<%=backUrl %>" />

<liferay-portlet:actionURL name="saveTraceabilityCode"
	var="saveTraceabilityCodeURL">
<%-- 	<portlet:param name="backUrl" value="<%=backUrl%>" /> --%>
	<portlet:param name="backUrl" value="<%=prevUrl%>" />
	<portlet:param name="piantaId" value="<%=String.valueOf(pianta.getId())%>" />
	<portlet:param name="orderId" value="<%=String.valueOf(orderId)%>" />
	<portlet:param name="partnerCode" value="<%=partnerCode%>" />
	<portlet:param name="customerCode" value="<%=customerCode%>" />
	<portlet:param name="quantity" value="<%=String.valueOf(quantitaOrdinata)%>" />
</liferay-portlet:actionURL>

<aui:input name="code" type="text" cssClass="input-small" inlineField="true"
	label="code" value="<%=pianta.getCodice()%>" disabled="true"/>
<aui:input name="nome" type="text" inlineField="true"
	label="name" value="<%=pianta.getNome()%>" disabled="true"/>
<aui:input name="forma" type="text" inlineField="true"
	label="shape" value="<%=forma%>" disabled="true"/>
<aui:input name="vaso" type="text" inlineField="true" cssClass="input-mini"
	label="vase" value="<%=pianta.getVaso()%>" disabled="true"/>
<aui:input name="altezza" type="text" inlineField="true" cssClass="input-mini"
	label="height" value="<%=pianta.getAltezza()%>" disabled="true"/>
<aui:input name="quantita" type="text" inlineField="true" cssClass="input-mini"
	label="quantity" value="<%=quantitaOrdinata%>" disabled="true"/>	
<aui:form id="aForm" name="aForm" action="<%=saveTraceabilityCodeURL%>"
	method="post" >
	<aui:input name="code" type="text"
		label="passport-number" value="<%=oldPassport%>">
		<aui:validator name="required" />
	</aui:input>
	<aui:button-row>
		<aui:button type="submit" value="save" />
		<aui:button type="cancel" onclick="this.form.reset()" />
	</aui:button-row>
</aui:form>

<%!private static Log _log = LogFactoryUtil
            .getLog("docroot.html.passport.traceability_code_jsp");%>