<%@page import="java.text.SimpleDateFormat"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../init.jsp"%>
<%
    String activeTab = ParamUtil.getString(renderRequest, "activeTab", "");
    String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
    String partnerCode = "";
    ExpandoValue value = ExpandoValueLocalServiceUtil.getValue(
        user.getCompanyId(),
        ClassNameLocalServiceUtil.getClassNameId(User.class),
        "CUSTOM_FIELDS", Constants.REGISTRY_CODE, user.getUserId());
    if (value != null) {
        if (!value.getData().equals("0")) {
		    partnerCode = value.getData();
		    List<Role> roles = user.getRoles();
		    for (Role role : roles) {
		        if (role.getName().equals(Constants.SPECIAL_PARTNER)) {
		            break;
		        }
		    }
        }
    } else {
        partnerCode = "-1";
    }
    SimpleDateFormat sdf = new SimpleDateFormat();
%>
<div id="myTab">
	<ul class="nav nav-tabs">
		<li id="passport-pending"
			class="<%="".equals(activeTab) || "passport-pending".equals(activeTab) ? "active" : ""%>">
			<a href="#passport-pending-tab"><liferay-ui:message
					key="passport-pending" /></a>
		</li>
		<li id="passport-processed"
			class="<%="passport-processed".equals(activeTab) ? "active" : ""%>">
			<a href="#passport-processed-tab"><liferay-ui:message
					key="passport-processed" /></a>
		</li>
	</ul>
	<div class="tab-content">
		<div id="passport-pending-tab" class="tab-pane">
			<%@ include file="passport-pending.jsp"%>
		</div>
		<div id="passport-processed-tab" class="tab-pane">
			<%@ include file="passport-processed.jsp"%>
		</div>
	</div>
</div>

<aui:script>
	AUI().use('aui-tabview', function(A) {
		var tab = new A.TabView({
			srcNode : '#myTab'
		}).render();
	});
</aui:script>