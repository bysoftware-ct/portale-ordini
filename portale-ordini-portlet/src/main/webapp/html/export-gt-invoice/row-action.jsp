<%@page import="com.liferay.portal.kernel.json.JSONObject"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="../init.jsp"%>

<%
	ResultRow row = (ResultRow) request
			.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	String curRowPK = String.valueOf(row.getParameter("curRowPK"));
	String customerSelected = String.valueOf(row
			.getParameter("customerSelected"));
	String dateFrom = String.valueOf(row.getParameter("dateFrom"));
	String dateTo = String.valueOf(row.getParameter("dateTo"));
	String search = String.valueOf(row.getParameter("search"));
	String numDoc = String.valueOf(row.getParameter("numDoc"));
	String center = String.valueOf(row.getParameter("center"));
	String year = String.valueOf(row.getParameter("year"));
	String checked = String.valueOf(row.getParameter("checked"));
%>



<liferay-ui:icon-menu>
	<liferay-portlet:actionURL name="export" var="exportURL">
		<portlet:param name="invoicesPKs" value="<%=curRowPK%>" />
		<portlet:param name="customerSelected" value="<%=customerSelected%>" />
		<portlet:param name="dateFrom" value="<%=dateFrom%>" />
		<portlet:param name="dateTo" value="<%=dateTo%>" />
		<portlet:param name="search" value="<%=search%>" />
		<portlet:param name="numDoc" value="<%=numDoc%>" />
		<portlet:param name="center" value="<%=center%>" />
		<portlet:param name="year" value="<%=year%>" />
		<portlet:param name="checked" value="<%=checked%>" />
	</liferay-portlet:actionURL>

	<liferay-portlet:actionURL name="export" var="sendURL">
		<portlet:param name="invoicesPKs" value="<%=curRowPK%>" />
		<portlet:param name="customerSelected" value="<%=customerSelected%>" />
		<portlet:param name="dateFrom" value="<%=dateFrom%>" />
		<portlet:param name="dateTo" value="<%=dateTo%>" />
		<portlet:param name="search" value="<%=search%>" />
		<portlet:param name="numDoc" value="<%=numDoc%>" />
		<portlet:param name="center" value="<%=center%>" />
		<portlet:param name="year" value="<%=year%>" />
		<portlet:param name="checked" value="<%=checked%>" />
		<portlet:param name="send" value="<%=String.valueOf(true)%>" />
	</liferay-portlet:actionURL>
	<liferay-ui:icon image="forward" url="${sendURL}" message="export-send" />
	<liferay-ui:icon image="export" url="${exportURL}" message="export" />
</liferay-ui:icon-menu>