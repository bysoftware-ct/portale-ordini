<%@page import="it.bysoftware.ct.utils.InvoiceComparator"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="it.bysoftware.ct.model.TestataFattureClienti"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="com.liferay.util.portlet.PortletProps"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page
	import="it.bysoftware.ct.service.TestataFattureClientiLocalServiceUtil"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../init.jsp"%>

<%
	String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem",
		"view-all");
	String GT = PortletProps.get("gt");
	List<TestataFattureClienti> exp = (ArrayList<TestataFattureClienti>) renderRequest.getAttribute("expInvoices");
	List<TestataFattureClienti> sent = (ArrayList<TestataFattureClienti>) renderRequest.getAttribute("SentInvoices");
	String customerSelected = ParamUtil.getString(renderRequest,
		"customerSelected", "");
	long fromLng = ParamUtil.getLong(renderRequest, "dateFrom", 0);

	int numDoc = ParamUtil.getInteger(renderRequest, "numDoc", 0);
	String center = ParamUtil.getString(renderRequest, "center", "");
	int year = ParamUtil.getInteger(renderRequest, "year", Utils.getYear());
	boolean checked = ParamUtil.getBoolean(renderRequest, "checked", false);
	
	Calendar calendarFrom = Calendar.getInstance();
	if (fromLng > 0) {
		calendarFrom.setTimeInMillis(fromLng);
	} else {
		calendarFrom = Utils.today();
		calendarFrom.add(Calendar.DAY_OF_MONTH, -7);
	}
	Date from = calendarFrom.getTime();

	Calendar calendarTo = Calendar.getInstance();
	long toLng = ParamUtil.getLong(renderRequest, "dateTo", 0);
	if (toLng > 0) {
		calendarTo.setTimeInMillis(toLng);
	} else {
		calendarTo = Utils.today();
	}
	Date to = calendarTo.getTime();

	boolean search = ParamUtil.getBoolean(request, "search", true);

	List<AnagraficheClientiFornitori> listGTCustomers = new ArrayList<AnagraficheClientiFornitori>();
	List<TestataFattureClienti> invoices = new ArrayList<TestataFattureClienti>();
	if (numDoc > 0 && !"".equals(center)) {
		List<TestataFattureClienti> tmp = TestataFattureClientiLocalServiceUtil.getInvoices(
				numDoc, center, year);
		for(TestataFattureClienti t : tmp){
			if(t.getCodiceCategoriaEconomica().equals(GT)){
				invoices.add(t);
			}
		}
	} else {
		List<TestataFattureClienti> tmp = TestataFattureClientiLocalServiceUtil
				.getInvoices(customerSelected, GT, from, to, search, 0,
						TestataFattureClientiLocalServiceUtil
								.getInvoicesCount(customerSelected, GT,
										from, to, search), new InvoiceComparator());
		for (TestataFattureClienti t : tmp) {
			if (t.getLibLng1() > 0)
				invoices.add(t);
		}
	}
	
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	NumberFormat nf = NumberFormat.getCurrencyInstance(renderRequest
			.getLocale());
%>

<liferay-portlet:renderURL varImpl="invoicesSearchURL">
	<portlet:param name="mvcPath" value="/html/export-gt-invoice/view.jsp" />
</liferay-portlet:renderURL>

<liferay-portlet:renderURL varImpl="iteratorURL">
	<portlet:param name="customerCode" value="<%=customerSelected%>" />
	<portlet:param name="dateFrom"
		value="<%=String.valueOf(calendarFrom.getTimeInMillis())%>" />
	<portlet:param name="dateTo"
		value="<%=String.valueOf(calendarTo.getTimeInMillis())%>" />
	<portlet:param name="search" value="<%=String.valueOf(search)%>" />
	<portlet:param name="mvcPath" value="/html/export-gt-invoice/view.jsp" />
</liferay-portlet:renderURL>

<liferay-ui:panel title="search" collapsible="true">
	<aui:form id="aForm" name="aForm" action="<%=invoicesSearchURL%>"
		method="post">
		<span class="aui-search-bar" title="search-entries"> </span>
		<aui:layout>
			<aui:column first="true" columnWidth="60">
				<aui:fieldset label="customer" helpMessage="search-by-customer">
					<aui:layout>
						<aui:column first="true" columnWidth="30">
							<%
								listGTCustomers = AnagraficheClientiFornitoriLocalServiceUtil
																	.findCustomersInCategory(GT);
							%>
							<aui:select name="customerSelected" label="customer"
								cssClass="input-xlarge">
								<aui:option label="select-customer" value=""
									selected="<%=customerSelected == null%>" />
								<%
									for (AnagraficheClientiFornitori customer : listGTCustomers) {
								%>
								<aui:option
									value="<%=customer
														.getCodiceAnagrafica()%>"
									label="<%=customer
														.getRagioneSociale1()
														+ " "
														+ customer
																.getRagioneSociale2()%>"
									selected="<%=customerSelected.equals(customer
														.getCodiceAnagrafica())%>" />
								<%
									}
								%>
							</aui:select>
						</aui:column>
						<aui:column last="true" columnWidth="30">
							<liferay-ui:message key="from" />
							<liferay-ui:input-date name="fromDate" cssClass="input-small"
								dayValue="<%=calendarFrom
												.get(Calendar.DAY_OF_MONTH)%>"
								dayParam="fromDay"
								monthValue="<%=calendarFrom
												.get(Calendar.MONTH)%>"
								monthParam="fromMonth"
								yearValue="<%=calendarFrom
												.get(Calendar.YEAR)%>"
								yearParam="fromYear" />
							<liferay-ui:message key="to" />
							<liferay-ui:input-date name="toDate" cssClass="input-small"
								dayValue="<%=calendarTo
												.get(Calendar.DAY_OF_MONTH)%>"
								dayParam="toDay"
								monthValue="<%=calendarTo
												.get(Calendar.MONTH)%>"
								monthParam="toMonth"
								yearValue="<%=calendarTo.get(Calendar.YEAR)%>"
								yearParam="toYear" />
						</aui:column>
					</aui:layout>
				</aui:fieldset>
			</aui:column>
			<aui:column last="true" columnWidth="40">
				<aui:fieldset label="number" helpMessage="search-by-number">
					<aui:layout>
						<aui:column first="true" columnWidth="30">
							<aui:input name="doc-number" type="text" cssClass="input-small"
								helpMessage="number-center" inlineField="true" value="<%= numDoc > 0 ? numDoc + "/" + center : "" %>"/>
						</aui:column>
						<aui:column last="true" columnWidth="30">
							<c:if test="<%=checked %>">
								<aui:input name="year" type="number" cssClass="input-small"
									helpMessage="year" value="<%=year %>" />
							</c:if>
							<c:if test="<%= !checked %>">
								<aui:input name="year" type="number" cssClass="input-small"
									helpMessage="year" disabled="true" value="<%=year %>" />
							</c:if>
							<aui:input name="year-chk" type="checkbox" label="edit" value="<%= checked %>"
								inlineLabel="true" />
						</aui:column>
					</aui:layout>
				</aui:fieldset>
			</aui:column>
			<aui:button-row>
				<aui:button name="searchBtn" type="button" value="search" />
				<aui:button type="cancel" onclick="this.form.reset()" />
			</aui:button-row>
		</aui:layout>
	</aui:form>
</liferay-ui:panel>

<aui:nav-bar>
	<aui:nav>
		<aui:nav-item id="sendBtn" iconCssClass="icon-envelope" cssClass="bold"
			label="export-send" selected='<%="send".equals(toolbarItem)%>' />
		<aui:nav-item id="exportBtn" iconCssClass="icon-save" label="export"
			selected='<%="export".equals(toolbarItem)%>' />
	</aui:nav>
</aui:nav-bar>
<liferay-portlet:actionURL name="export" var="exportURL" >
	<portlet:param name="customerSelected" value="<%=customerSelected%>" />
	<portlet:param name="dateFrom"
		value="<%=String.valueOf(calendarFrom.getTimeInMillis())%>" />
	<portlet:param name="dateTo"
		value="<%=String.valueOf(calendarTo.getTimeInMillis())%>" />
	<portlet:param name="search" value="<%=String.valueOf(search)%>" />
</liferay-portlet:actionURL>
<aui:fieldset label="Elenco fatture">
	<aui:form id="bForm" name="bForm" method="post">
		<aui:input name="invoicesPKs" type="hidden" />
		<liferay-ui:search-container delta="75"
			emptyResultsMessage="no-orders" iteratorURL="<%=iteratorURL%>"
			rowChecker="<%=new RowChecker(renderResponse)%>">
			<liferay-ui:search-container-results>
				<%
					results = ListUtil.subList(invoices,
											searchContainer.getStart(),
											searchContainer.getEnd());
									total = invoices.size();
									pageContext.setAttribute("results", results);
									pageContext.setAttribute("total", total);
				%>
			</liferay-ui:search-container-results>
			<liferay-ui:search-container-row
				className="it.bysoftware.ct.model.TestataFattureClienti"
				modelVar="invoice" rowVar="curRow">
				<liferay-ui:search-container-column-text name="number"
					value="<%=invoice.getNumeroDocumento() + "/"
									+ invoice.getCodiceCentro()%>" />

				<%
					curRow.setPrimaryKey(JSONFactoryUtil
											.looseSerialize(invoice.getPrimaryKeyObj()));
									AnagraficheClientiFornitori customer = customerSelected
											.isEmpty() ? AnagraficheClientiFornitoriLocalServiceUtil
											.getAnagraficheClientiFornitori(invoice
													.getCodiceCliente())
											: AnagraficheClientiFornitoriLocalServiceUtil
													.getAnagraficheClientiFornitori(customerSelected);
				%>
				<liferay-ui:search-container-column-text name="customer"
					value="<%=customer.getRagioneSociale1() + " "
									+ customer.getRagioneSociale2()%>" />
				<liferay-ui:search-container-column-text name="date"
					value="<%=sdf.format(invoice.getDataDocumento())%>" />
				<liferay-ui:search-container-column-text name="amount"
					value="<%=nf.format(invoice.getImportoFattura())%>" />
				<liferay-ui:search-container-row-parameter name="customerSelected" value="<%=customerSelected%>" />
				<liferay-ui:search-container-row-parameter name="dateFrom"
					value="<%=String.valueOf(calendarFrom.getTimeInMillis())%>" />
				<liferay-ui:search-container-row-parameter name="dateTo"
					value="<%=String.valueOf(calendarTo.getTimeInMillis())%>" />
				<liferay-ui:search-container-row-parameter name="search" value="<%=String.valueOf(search)%>" />
				<liferay-ui:search-container-row-parameter name="numDoc" value="<%=String.valueOf(numDoc)%>" />
				<liferay-ui:search-container-row-parameter name="center" value="<%=center%>" />
				<liferay-ui:search-container-row-parameter name="year" value="<%=String.valueOf(year)%>" />
				<liferay-ui:search-container-row-parameter name="checked" value="<%=String.valueOf(checked)%>" />
				<liferay-ui:search-container-row-parameter name="curRowPK" value="<%="[" + curRow.getPrimaryKey() + "]" %>" />
				<liferay-ui:search-container-column-jsp align="right"
					path="/html/export-gt-invoice/row-action.jsp" />
			</liferay-ui:search-container-row>
			<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
				paginate="true" />
		</liferay-ui:search-container>
	</aui:form>
</aui:fieldset>



<aui:script>

var exportURL = '<%= exportURL.toString() %>';

AUI().use('aui-base', 'datatype-number', function(A) {
    A.one('#<portlet:namespace/>searchBtn').on('click', function() {
    	var numDocStr = A.one('#<portlet:namespace />doc-number').get('value');
    	if (numDocStr !== "") {
    		if (numDocStr.indexOf('/') > 0){
    			var num = numDocStr.split('/')[0];
        		var center = numDocStr.split('/')[1];
        		var year = A.one('#<portlet:namespace />year').get('value');
        		var checked = A.one('#<portlet:namespace />year-chk').get('value')
        		window.location.href = '<%=invoicesSearchURL%>&<portlet:namespace/>numDoc=' + num + '&<portlet:namespace/>center=' + center + '&<portlet:namespace/>year=' + year + '&<portlet:namespace/>checked=' + checked;
    		} else {
    			alert('Specifica il numero ed il centro della fattura da cercare separati dal carattere "/"');
    		}
    	} else {
    		var fromDateStr = A.one('#<portlet:namespace />fromDate').get('value');
            var fromDate = parseDate(fromDateStr);
            var toDateStr = A.one('#<portlet:namespace/>toDate').get('value');
            var toDate = parseDate(toDateStr);
            if (A.one('#<portlet:namespace/>customerSelected') !== null) {
    	        var customerSelected = A.one('#<portlet:namespace/>customerSelected').get('value');
    	        window.location.href = '<%=invoicesSearchURL%>&<portlet:namespace/>dateFrom=' + fromDate.getTime() + '&<portlet:namespace/>dateTo=' + toDate.getTime() + '&<portlet:namespace/>customerSelected=' + customerSelected;
            } else {
            	window.location.href = '<%=invoicesSearchURL%>&<portlet:namespace/>dateFrom=' + fromDate.getTime() + '&<portlet:namespace/>dateTo=' + toDate.getTime();
            }
    	}
    });    
});

AUI().use('aui-base', 'datatype-number', function(A) {
	A.one('#<portlet:namespace/>year-chkCheckbox').on('click', function() {
		var checked = A.one('#<portlet:namespace />year-chk').get('value');
		if (checked === 'true'){
			A.one('#<portlet:namespace />year').removeAttribute('disabled');
		} else {
			A.one('#<portlet:namespace />year').setAttribute('disabled', true);
		}
	});
});

AUI().use('liferay-util-list-fields', function(A) {
    A.one('#<portlet:namespace/>sendBtn').on('click', function(event) {
    	callAction(A, true);
    });
    A.one('#<portlet:namespace/>exportBtn').on('click', function(event) {
    	callAction(A, false)
    });
});


function callAction(A, send) {
	var selectedItems = Liferay.Util.listCheckedExcept(document.<portlet:namespace />bForm, '<portlet:namespace />allRowIds');
	A.one('#<portlet:namespace/>invoicesPKs').set('value', '['+ selectedItems + ']');
	console.log(exportURL + '&<portlet:namespace/>send=' + send);
	if (selectedItems.length == "" || selectedItems == null ) {
		alert('Nessun fattura selezionata.');
	} else {    		
		var numDocStr = A.one('#<portlet:namespace />doc-number').get('value');
		var num, center;
   		if (numDocStr.indexOf('/') > 0){
   			num = numDocStr.split('/')[0];
       		center = numDocStr.split('/')[1];
   		}
   		var year = A.one('#<portlet:namespace />year').get('value');
   		var checked = A.one('#<portlet:namespace />year-chk').get('value')
		submitForm(document.<portlet:namespace />bForm,  exportURL + '&<portlet:namespace/>send=' + send
				+ '&<portlet:namespace/>numDoc=' + num + '&<portlet:namespace/>center=' + center + '&<portlet:namespace/>year=' + year + '&<portlet:namespace/>checked=' + checked);
	}
}

function parseDate(string) {
    var tmp = string.split('/');
    return new Date(tmp[2] + '-' + tmp[1] + '-' + tmp[0]);
}
</aui:script>