<%@page import="com.liferay.portal.kernel.util.OrderByComparator"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="it.bysoftware.ct.service.PianoPagamentiLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.PianoPagamenti"%>
<%@page import="it.bysoftware.ct.service.VettoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Vettori"%>
<%@page import="java.util.Arrays"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowConstants"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="it.bysoftware.ct.utils.OrderState"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="it.bysoftware.ct.utils.OrderComparator"%>
<%@page import="it.bysoftware.ct.service.OrdineLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<%@page import="it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK"%>
<%@page import="it.bysoftware.ct.service.StatiLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Stati"%>
<%@page import="it.bysoftware.ct.utils.SubjectComparator"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.ClientiFornitoriDatiAgg"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.kernel.dao.search.DisplayTerms"%>
<%@page import="java.util.List"%>
<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8" />
<%@include file="../init.jsp"%>

<%
    String exceptionArgs = ParamUtil.getString(renderRequest, "exceptionArgs", "");
    Object[] args = exceptionArgs.split(StringPool.COMMA);
    String sortByCol = ParamUtil.getString(request, "orderByCol", "ship-date"); 
    String sortByType = ParamUtil.getString(request, "orderByType", "asc"); 
    String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
    String currentURL = PortalUtil.getCurrentURL(request);
    String customerCode = ParamUtil.getString(request, "customerCode");
    String customerName = ParamUtil.getString(request, "customerName");
    boolean showsInactive = ParamUtil.getBoolean(renderRequest, "show-inactive", true);
    boolean showsDraft = ParamUtil.getBoolean(renderRequest, "show-draft", true);
    boolean showsPending = ParamUtil.getBoolean(renderRequest, "show-pending", true);
    boolean showsApproved = ParamUtil.getBoolean(renderRequest, "show-approved", true);
    boolean showsProcessed = ParamUtil.getBoolean(renderRequest, "show-processed");
    Calendar calendar = Calendar.getInstance();
    calendar.clear();
    calendar.set(Calendar.MONTH, 0);
    calendar.set(Calendar.YEAR, Utils.getYear());
    calendar.set(Calendar.DAY_OF_MONTH, 1);
    long firstYearDay = calendar.getTimeInMillis();//Utils.today().getTimeInMillis();
    calendar.set(Calendar.MONTH, 11);
    calendar.set(Calendar.DAY_OF_MONTH, 31);
    long lastYearDay = calendar.getTimeInMillis(); //Utils.today().getTimeInMillis();
    long orderDateLng = ParamUtil.getLong(request, "orderDateLng", firstYearDay);
    long shipDateLng = ParamUtil.getLong(request, "shipDateLng", lastYearDay);
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    NumberFormat nf = NumberFormat.getCurrencyInstance(renderRequest.getLocale());
    
%>
<liferay-ui:error key="error-retrieving-order-x">
    <liferay-ui:message key="error-retrieving-order-x" arguments="<%=args %>" />
</liferay-ui:error>
<liferay-ui:error key="delete-order-error" message="delete-order-error"/>
<liferay-ui:success key="delete-order-success" message="delete-order-success"/>
<liferay-ui:header title="select-order" />
<liferay-portlet:renderURL varImpl="newOrderURL">
    <portlet:param name="mvcPath" value="/html/ordini/new-order.jsp" />
</liferay-portlet:renderURL>
<liferay-portlet:renderURL varImpl="orderSearchURL">
<%-- 	<portlet:param name="customerCode" value="<%= customerCode %>" /> --%>
<%-- 	<portlet:param name="showsInactive" value="<%= String.valueOf(showsInactive) %>" /> --%>
<%-- 	<portlet:param name="showsPending" value="<%= String.valueOf(showsPending) %>" /> --%>
<%-- 	<portlet:param name="showsDraft" value="<%= String.valueOf(showsDraft) %>" /> --%>
<%-- 	<portlet:param name="showsApproved" value="<%= String.valueOf(showsApproved) %>" /> --%>
<%-- 	<portlet:param name="showsProcessed" value="<%= String.valueOf(showsProcessed) %>" /> --%>
<%-- 	<portlet:param name="orderDateLng" value="<%= String.valueOf(orderDateLng) %>" /> --%>
<%-- 	<portlet:param name="shipDateLng" value="<%= String.valueOf(shipDateLng) %>" /> --%>
	<portlet:param name="mvcPath" value="/html/ordini/view.jsp" />
</liferay-portlet:renderURL>
<aui:form action="<%=orderSearchURL %>" method="post" name="orderForm">
    <liferay-portlet:renderURLParams varImpl="orderSearchURL" />
    <liferay-portlet:renderURL varImpl="iteratorURL1">
        <portlet:param name="customerCode" value="<%= customerCode %>" />
        <portlet:param name="customerName" value="<%= customerName %>" />
        <portlet:param name="show-inactive" value="<%= String.valueOf(showsInactive) %>" />
        <portlet:param name="show-pending" value="<%= String.valueOf(showsPending) %>" />
        <portlet:param name="show-draft" value="<%= String.valueOf(showsDraft) %>" />
        <portlet:param name="show-approved" value="<%= String.valueOf(showsApproved) %>" />
        <portlet:param name="show-processed" value="<%= String.valueOf(showsProcessed) %>" />
        <portlet:param name="orderDateLng" value="<%= String.valueOf(orderDateLng) %>" />
        <portlet:param name="shipDateLng" value="<%= String.valueOf(shipDateLng) %>" />
        <portlet:param name="mvcPath" value="/html/ordini/view.jsp" />
    </liferay-portlet:renderURL>
    <liferay-ui:search-container displayTerms="<%= new DisplayTerms(renderRequest) %>" delta="75"
        emptyResultsMessage="no-orders" headerNames="code,company-name,order-date,ship-date,amount"
        iteratorURL="<%= iteratorURL1 %>" orderByCol="<%= sortByCol %>" orderByType="<%= sortByType %>">
        <aui:nav-bar>
            <aui:nav>
                <aui:nav-item id="newBtn" iconCssClass="icon-plus" label="new"
                    selected='<%="new".equals(toolbarItem)%>' href="<%= newOrderURL.toString()%>"/>
            </aui:nav>
            <aui:nav-bar-search cssClass="pull-right" >
                <liferay-ui:search-form page="/html/ordini/order-search.jsp" 
                     servletContext="<%= application %>"/>
            </aui:nav-bar-search>
        </aui:nav-bar>
        <liferay-ui:search-container-results>
            <%
            OrderByComparator orderByComparator =        
            Utils.getOrderByComparator(sortByCol, sortByType);
            DisplayTerms displayTerms = searchContainer.getDisplayTerms();
            List<Ordine> orders = new ArrayList<Ordine>();
            if (displayTerms.isAdvancedSearch()) {
                orders = OrdineLocalServiceUtil.findOrders(customerCode, new Date(orderDateLng), new Date(shipDateLng), showsInactive, showsDraft, showsPending, showsApproved, showsProcessed,
                        displayTerms.isAndOperator(), 0, OrdineLocalServiceUtil.getFindOrdersCount(customerCode, new Date(orderDateLng), new Date(shipDateLng), showsInactive, showsDraft, showsPending, showsApproved, showsProcessed, displayTerms.isAndOperator()),
                        orderByComparator); 
            } else {
                String searchkeywords = displayTerms.getKeywords();
                String numbesearchkeywords = String.valueOf(0);
                if (Validator.isNumber(searchkeywords)) {
                    numbesearchkeywords = searchkeywords;
                }
                orders = OrdineLocalServiceUtil.findOrders(searchkeywords, new Date(orderDateLng), new Date(shipDateLng), showsInactive, showsDraft, showsPending, showsApproved, showsProcessed, true,
                        0, OrdineLocalServiceUtil.getFindOrdersCount(searchkeywords, new Date(orderDateLng), new Date(shipDateLng), showsInactive, showsDraft, showsPending, showsApproved, showsProcessed, true),
                        orderByComparator);  
            }
            
            if (displayTerms.isAdvancedSearch() && customerName != null && !customerName.isEmpty()) {
                List<Ordine> tmp = new ArrayList<Ordine>();
                for (Ordine o : orders) {
                    AnagraficheClientiFornitori customer =
                            AnagraficheClientiFornitoriLocalServiceUtil.getAnagraficheClientiFornitori(o.getIdCliente());
                    if (customer.getRagioneSociale1().toLowerCase().contains(customerName.toLowerCase()) || customer.getRagioneSociale2().toLowerCase().contains(customerName.toLowerCase())) {
                        tmp.add(o);
                    }
                }
                
                orders = tmp;
            } else {
                String searchkeywords = displayTerms.getKeywords();
                List<Ordine> tmp = new ArrayList<Ordine>();
                for (Ordine o : orders) {
                    AnagraficheClientiFornitori customer =
                            AnagraficheClientiFornitoriLocalServiceUtil.getAnagraficheClientiFornitori(o.getIdCliente());
                    if (customer.getRagioneSociale1().toLowerCase().contains(searchkeywords.toLowerCase()) || customer.getRagioneSociale2().toLowerCase().contains(searchkeywords.toLowerCase())) {
                        tmp.add(o);
                    }
                }
                
                orders = tmp;
            }
            
            results = ListUtil.subList(orders,
                                    searchContainer.getStart(),
                                    searchContainer.getEnd());
                            total = orders.size();
                            pageContext.setAttribute("results", results);
                            pageContext.setAttribute("total", total);
            %>
        </liferay-ui:search-container-results>
        <liferay-ui:search-container-row className="it.bysoftware.ct.model.Ordine"
            keyProperty="id" modelVar="order">
            <liferay-portlet:renderURL varImpl="rowURL">
                <portlet:param name="backURL" value="<%= currentURL %>" />
                <portlet:param name="redirect" value="<%= currentURL %>" />
                <portlet:param name="orderId" value="<%= String.valueOf(order.getId()) %>" />
            </liferay-portlet:renderURL>
            <liferay-ui:search-container-column-text name="number" value="<%=order.getNumero() + " / " + order.getCentro() %>" orderable="<%= true %>"/>
<%--             <liferay-ui:search-container-column-text name="customer-code" property="idCliente"  /> --%>
            <%
            String orderDateStr = sdf.format(order.getDataInserimento());
            String shipDateStr = sdf.format(order.getDataConsegna());
            AnagraficheClientiFornitori customer =
                    AnagraficheClientiFornitoriLocalServiceUtil.fetchAnagraficheClientiFornitori(order.getIdCliente());
            String[] customerInfo = new String[] { "", "", "", "", "", "" };
            if (customer != null) {
            	customerInfo[0] = customer.getRagioneSociale1() + " " + customer.getRagioneSociale2();
            	customerInfo[1] = customer.getIndirizzo();
            	customerInfo[2] = customer.getCAP();
            	customerInfo[3] = customer.getComune();
            	customerInfo[4] = customer.getSiglaProvincia();
            	customerInfo[5] = customer.getSiglaStato();
            }
            %>
            <liferay-ui:search-container-column-text name="code" value="<%= order.getIdCliente() %>"/>
            <liferay-ui:search-container-column-text name="company-name" orderable="true">
            	<liferay-ui:message key="company-name-x" arguments="<%= customerInfo %>" />
            </liferay-ui:search-container-column-text>
            <liferay-ui:search-container-column-text name="order-date"  value="<%=orderDateStr %>" orderable="<%= true %>"/>
            <liferay-ui:search-container-column-text name="ship-date"  value="<%=shipDateStr %>" orderable="<%= true %>"/>
            <liferay-ui:search-container-column-text name="carrier" value="<%= order.getVettore() %>" orderable="<%= true %>"/>
            <liferay-ui:search-container-column-text name="amount" value="<%= nf.format(order.getImporto()) %>" orderable="true" />
            <liferay-ui:search-container-column-text name="payment" value="<%= order.getPagamento() %>" orderable="<%= true %>"/>
            <liferay-ui:search-container-column-text name="note" >
            	<c:if test="<%= !order.getNote().isEmpty() %>">
            		<input id="inputNote_<%= order.getId()%>" type="hidden" value="<%=order.getNote() %>" />
            		<input id="myNote_<%= order.getId()%>" class="note-btn" type="button" value="Vedi" />
            		
            	</c:if>
            </liferay-ui:search-container-column-text>
            <c:choose>
                <c:when test="<%= order.getStato() >= 0 && order.getStato() < 7 %>">
                    <liferay-ui:search-container-column-text name="status" orderable="true">
                        <aui:workflow-status showIcon="<%= false %>" showLabel="<%= false %>"
                            status="<%= order.getStato() %>" />
                    </liferay-ui:search-container-column-text>
                </c:when>
                <c:otherwise>
                    <liferay-ui:search-container-column-text name="status" value=""  orderable="true"/>
                </c:otherwise>
            </c:choose>
            <liferay-ui:search-container-row-parameter name="recap" value="true" />
            <liferay-ui:search-container-row-parameter name="backURL" value="<%= iteratorURL1.toString() %>" />
            <liferay-ui:search-container-column-jsp path="/html/ordini/order-view/view-orders-action.jsp" />
        </liferay-ui:search-container-row>
        <liferay-ui:search-iterator searchContainer="<%=searchContainer %>" />
    </liferay-ui:search-container>
</aui:form>


<%!private static Log _log = LogFactoryUtil
            .getLog("docroot.html.ordini.view_jsp");%>

<aui:script>
	AUI().use('aui-base', function(A) {
		var btns = A.all('.note-btn');
		btns.on('click', function(e) {
			console.log(e);
			console.log(A.one('#inputNote_'+e.target._node.id.split('_')[1]).get('value'));
			var index = '_' + Math.random().toString(36).substr(2, 9);
			var popover = new A.Popover({
				id: index,
				align: {
					node: e.target,
				    points:[A.WidgetPositionAlign.BC, A.WidgetPositionAlign.TC]
				},
				bodyContent: unescape(A.one('#inputNote_'+e.target._node.id.split('_')[1]).get('value')),
				headerContent: 'Nota',
				position: 'top',
				plugins: [A.Plugin.WidgetAnim]
			}).render();
			console.log(popover);
			A.one('#' + index).on('click', function(e) {
				A.one('#' + index).toggleView();
			});
		});
	});
</aui:script>