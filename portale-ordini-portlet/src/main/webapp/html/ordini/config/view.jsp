<%@page import="com.liferay.portal.kernel.util.StringUtil"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../../init.jsp"%>


<%
    int cartHeight = GetterUtil.getInteger(portletPreferences.getValue(
            Constants.CART_DEFAULT_HEIGHT, "260"));
	double addPrintLabel = GetterUtil.getDouble(portletPreferences.getValue(
        Constants.ADD_PRINT_LABEL, "0.1"));
%>

<liferay-portlet:actionURL portletConfiguration="true"
	var="configurationURL" />

<aui:form action="<%=configurationURL%>" method="post" name="fm">
	<aui:input name="<%=Constants.CMD%>" type="hidden"
		value="<%=Constants.UPDATE%>" />

	<aui:input name="preferences--cartHeight--" type="text"
		label="cart-height" value="<%=cartHeight%>">
		<aui:validator name="required" />
		<aui:validator name="digits" />
	</aui:input>
	
	<aui:input name="preferences--labelPrice--" type="text" suffix="€"
		label="add-label-price" value="<%= addPrintLabel %>">
		<aui:validator name="number" />
	</aui:input>
	    
	<aui:button-row>
		<aui:button type="submit" />
	</aui:button-row>
</aui:form>