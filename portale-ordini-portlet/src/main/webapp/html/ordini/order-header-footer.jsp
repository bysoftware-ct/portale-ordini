<%@page import="com.liferay.portal.kernel.util.StringUtil"%>
<%@page import="it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK"%>
<%@page import="com.liferay.util.portlet.PortletProps"%>
<%@page import="it.bysoftware.ct.service.VociIvaLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.VociIva"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="it.bysoftware.ct.service.CentroLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Centro"%>
<%@page import="com.liferay.portal.kernel.util.UnicodeFormatter"%>
<%@page import="it.bysoftware.ct.service.TipoCarrelloLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.TipoCarrello"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page
	import="it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="it.bysoftware.ct.model.ClientiFornitoriDatiAgg"%>
<%@page import="java.util.List"%>
<%@page import="it.bysoftware.ct.service.VettoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Vettori"%>
<%@page import="java.net.URLEncoder"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../init.jsp"%>

<%
	String GT = PortletProps.get("gt");
    AnagraficheClientiFornitori customer = AnagraficheClientiFornitoriLocalServiceUtil.
            getAnagraficheClientiFornitori(customerCode);
	ClientiFornitoriDatiAgg customerData = ClientiFornitoriDatiAggLocalServiceUtil.
			getClientiFornitoriDatiAgg(new ClientiFornitoriDatiAggPK(customerCode, false));
	boolean gtCustomer = !customerData.getCategoriaEconomica().isEmpty() &&
			customerData.getCategoriaEconomica().equals(GT);
    String carrierCode = ParamUtil.getString(renderRequest, "carriers", "");
    Vettori carrier = null;
    if (carrierCode != null) {
        carrier = VettoriLocalServiceUtil.fetchVettori(carrierCode);
    }
    List<Vettori> carriers = VettoriLocalServiceUtil.getVettoris(0,
            VettoriLocalServiceUtil.getVettorisCount());
    long cartId = ParamUtil.getLong(renderRequest, "cartsType", 0);
    TipoCarrello cartType = null;
    if (cartId > 0) {
        cartType = TipoCarrelloLocalServiceUtil.getTipoCarrello(cartId);
    }
    List<TipoCarrello> cartsType = TipoCarrelloLocalServiceUtil.
            getTipoCarrellos(0, TipoCarrelloLocalServiceUtil.getTipoCarrellosCount());
    String billingCenter = ParamUtil.getString(renderRequest, "centers", "");
    String vatSelected = ParamUtil.getString(renderRequest, "vats", "10");
    if (billingCenter.isEmpty()) {
        String state = customer.getSiglaStato();
        if (state.equals("I")) {
            billingCenter = PortletProps.get("it-center-code");
            vatSelected = PortletProps.get("it-vat-code");
        } else if (state.equals("SUI")) {
            billingCenter = PortletProps.get("suisse-center-code");
            vatSelected = PortletProps.get("foreign-vat-code");
        } else {
            billingCenter = PortletProps.get("foreing-center-code");
            vatSelected = PortletProps.get("foreign-vat-code");
        }
    }
    
    String loadPlace = ParamUtil.getString(renderRequest, "loadPlace", "");
//     List<Centro> centers = CentroLocalServiceUtil.getCentros(0,
//             CentroLocalServiceUtil.getCentrosCount());
    
    long dateInMs = ParamUtil.getLong(renderRequest, "orderDate", new Date().getTime()); 
    Date orderDate = new Date();
    orderDate.setTime(dateInMs);
    dateInMs = ParamUtil.getLong(renderRequest, "shipDate");
    Date shipDate = null;
    if (dateInMs > 0) {
        shipDate = new Date();
        shipDate.setTime(dateInMs);
    } else {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(orderDate);
        calendar.add(Calendar.DAY_OF_YEAR, 7);
        shipDate = calendar.getTime();
    }
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    String orderDateStr = sdf.format(orderDate);
    String shipDateStr = sdf.format(shipDate);
    
    String note = orderJSON.getString("note");
    
    List<VociIva> vatList = VociIvaLocalServiceUtil.getVociIvas(0, VociIvaLocalServiceUtil.getVociIvasCount());
    
    String GTorderNum = ParamUtil.getString(renderRequest, "GTorderNum", "");
    String GTorderDateStr = "";
 	dateInMs = ParamUtil.getLong(renderRequest, "GTorderDate", new Date().getTime());
    Date GTorderDate = new Date();
    GTorderDate.setTime(dateInMs);
    GTorderDateStr = sdf.format(GTorderDate);
    
    
%>
<liferay-portlet:renderURL varImpl="searchCustomerURL" windowState="<%=LiferayWindowState.POP_UP.toString()%>">
    <portlet:param name="mvcPath" value="/html/ordini/new-order.jsp" />
    <portlet:param name="search" value="true" />
</liferay-portlet:renderURL>
<aui:input name="orderId" id="orderId" label="" disabled="true" type="hidden"
    cssClass="input-small" inlineField="true" value="<%= orderId%>"/>
<aui:layout>
	<aui:column columnWidth="70" first="true">
		<aui:fieldset label="customer">
			<aui:input name="customerCode" id="customerCode" type="hidden"
				label="code" disabled="true" cssClass="input-small"
				inlineField="true" value="<%=customer.getCodiceAnagrafica()%>" />
			<aui:input name="companyName" id="companyName" type="textarea"
				label="company-name" disabled="true" resizable="false"
				cssClass="input-xlarge" inlineField="true"
				value="<%=customer.getRagioneSociale1() + "\n"
                                + customer.getRagioneSociale2()%>" />
			<aui:input name="address" id="address" type="textarea"
				label="address" disabled="true" cssClass="input-xlarge"
				inlineField="true" value="<%=Utils.getFullAddress(customer)%>" />
			<aui:button id="changeAddressBTN" name="changeAddressBTN" icon="icon-refresh"
			    cssClass="btn-warning" value="edit" />
			<aui:input name="email" id="email" type="text" label="email"
				value="<%=customer.getPreferredEmail(String.valueOf(customerCode))%>" />
            <aui:input id="orderDate" type="text" name="orderDate" label="order-date" inlineField="true"
                value="<%= orderDateStr%>" cssClass="input-small"/>
            <aui:input id="shipDate" type="text" name="shipDate" label="ship-date" inlineField="true"
                value="<%= shipDateStr%>" cssClass="input-small"/>
            <aui:input id="loadPlace" type="text" name="loadPlace" label="load-place" inlineField="true"
                value="<%= loadPlace%>" cssClass="input-xlarge"/>
            <aui:input name="centers" id="centers" type="text" label="center"
                disabled="true" cssClass="input-mini" inlineField="true"
                value="<%=billingCenter %>" />
            <aui:input name="vatSelected" id="vatSelected" label="" type="hidden"
                disabled="true" cssClass="input-mini" inlineField="true"
                value="<%=vatSelected %>" />
            <aui:select id="vats" name="selectVat" inlineField="true" label="vat" disabled="true">
                <%
                    if (Validator.isNotNull(vatList)) {
                        for (VociIva vat : vatList) {
                            boolean selected = false;
                            if (!PortletProps.get("it-center-code").equals(billingCenter)) {
                                selected = vatSelected.equals(vat.getCodiceIva());
                            } else {
                                selected = vatSelected.equals(vat.getCodiceIva());
                            }
                %>
                <aui:option label="<%= vat.getDescrizione()%>"
                    value="<%= vat.getCodiceIva()%>" selected="<%= selected%>" />
                <%
                        }
                    }
                %>
            </aui:select>
            <aui:input name="enableVATSelect" type="checkbox" inlineField="true" label="enable" />
		</aui:fieldset>
	</aui:column>
	<c:if test="<%= gtCustomer %>">
		<aui:column columnWidth="20">
			<aui:fieldset label="gt-order-details">
				<aui:input id="GTorderNum" type="text" name="GTorderNum" label="gt-order-num" inlineField="true"
		                cssClass="input-small"/>
				<aui:input id="GTorderDate" type="text" name="GTorderDate" label="gt-order-date" inlineField="true"
		                value="<%=GTorderDateStr %>" cssClass="input-small"/>
	        </aui:fieldset>
	    </aui:column>
    </c:if>
	<aui:column columnWidth="20">
		<aui:fieldset label="carrier">
			<aui:input name="carrierCode" id="carrierCode" type="hidden"
				label="code" disabled="true" cssClass="input-small"
				inlineField="true"
				value="<%=carrier != null ? carrier.getCodiceVettore()
                                : ""%>" />
			<aui:select id="carriers" name="selectCarrier">
				<%
				    if (Validator.isNotNull(carriers)) {
		                for (Vettori c : carriers) {
		                    boolean selected = false;
		                    if (carrierCode
		                            .equals(c.getCodiceVettore())) {
		                        selected = true;
		                    }
				%>
				<aui:option label="<%=c.getRagioneSociale()%>"
					value="<%=c.getCodiceVettore()%>" selected="<%=selected%>" />
				<%
				        }
		            }
				%>
				<aui:option label="select-carrier" value=""
					selected="<%=carrierCode.isEmpty()%>" />
			</aui:select>
		</aui:fieldset>
		<aui:fieldset label="cart">
            <aui:input name="cartId" id="cartId" type="hidden" label="cart-id"
                disabled="true" cssClass="input-small" inlineField="true"
                value="<%=cartType != null ? cartType.getId() : 0%>" />
            <aui:select id="cartsType" name="selectCartType" inlineField="true">
                <%
                    if (Validator.isNotNull(cartsType)) {
                        for (TipoCarrello cT : cartsType) {
                            boolean selected = false;
                            if (cartId == cT.getId()) {
                                selected = true;
                            }
                %>
                <aui:option label="<%= cT.getTipologia()%>"
                    value="<%= cT.getId()%>" selected="<%= selected%>" />
                <%
                        }
                    }
                %>
                <aui:option label="select-cart-type" value="0"
                    selected="<%= cartId == 0%>" />
            </aui:select>
        </aui:fieldset>
	</aui:column>
</aui:layout>
<aui:layout>
    <aui:column columnWidth="45" first="true">
<%--         <liferay-ui:message key="note" /> --%>
<%--         <liferay-ui:input-editor toolbarSet="simple" initMethod="initEditor" onBlurMethod="saveData" /> --%>
        <aui:input id="note" name="note" label="note" type="textarea" cssClass="maximize" />
    </aui:column>
    <aui:column columnWidth="45" last="true">
        <aui:input id="customerNote" name="customerNote" label="customer-note"
            type="hidden" cssClass="input-large"/>
    </aui:column>
</aui:layout>
<%-- <aui:input name="shipTime" label="ship-time" type="text" value="12:00 AM" placeholder="hh:mm"/> --%>
<%-- <input class="input-large" type="text" placeholder="hh:mm" value="12:00 AM" /> --%>

<aui:script>
	var orderDate = new Date('<%=orderDate %>');
	var shipDate = new Date('<%=shipDate %>');
	var gtCustomer = '<%= gtCustomer %>';
	var gtOrderDate = new Date('<%=GTorderDate %>'); 
	var gtOrderNum = '<%= GTorderNum %>';
	AUI().ready(function(A) {
        drawDatePickers();
        AUI().use('aui-base', function(A) {
        	A.one("#<portlet:namespace/>orderDate").set('value', '<%=orderDateStr %>');
        	A.one("#<portlet:namespace/>shipDate").set('value', '<%=shipDateStr %>');
        	var decodedNote = unescape('<%= note%>'); 
        	A.one("#<portlet:namespace/>note").set('value', decodedNote);
//         	window.<portlet:namespace />editor.setHTML(decodedNote);
			if (gtCustomer === 'true') {
		       	A.one("#<portlet:namespace/>GTorderDate").set('value', '<%=GTorderDateStr %>');
		       	A.one("#<portlet:namespace/>GTorderNum").set('value', gtOrderNum);
			}
        });
    });
	
	AUI().use('aui-base', 'datatype-number', function(A) {
        A.one("#<portlet:namespace/>sendBtn").on('click', function() {
        	var email = A.one('#<portlet:namespace/>email').get('value');
        	var orderId = A.one('#<portlet:namespace/>orderId').get('value');
        	var carrier = A.one('#<portlet:namespace/>carriers').get('value');
            var cartType = A.one('#<portlet:namespace/>cartsType').get('value');
            var stringDeliveryDate =  A.one('#<portlet:namespace/>shipDate').get('value');
            var deliveryDateSplitted = stringDeliveryDate.split("/");
            var deliveryDate = new Date(deliveryDateSplitted[2], deliveryDateSplitted[1] - 1, deliveryDateSplitted[0]);
            var gtNum = '';
            var stringGtDate = '';
            if (gtCustomer === 'true') {
	            gtNum = A.one('#<portlet:namespace/>GTorderNum').get('value');
	            stringGtDate = A.one('#<portlet:namespace/>GTorderDate').get('value');
            }
            var ok = false;
            var today = new Date();
            today.setHours(0, 0, 0, 0);
            var msg = 'Prima di procedere all\'invio della conferma d\'ordine,\n';
            if (orderId !== '') {
            	if (email !== '') {
            		if (carrier !== '') {
                        if (cartType !== '0') {
                        	if (deliveryDate >= today) {
                        		ok = true;	
                        	} else {
                        		msg += 'modificare la data di consegna.\nLa data' +
                        		  ' di conegna deve essere settata almeno oggi';
                        	}
                        } else {
                            msg += 'selezionare un tipo di carrello.';
                        }
                    } else {
                        msg += 'selezionare un vettore.';
                    }
            	} else {
            		msg += 'inserire l\'email del cliente.';
            	}
            } else {
            	msg += 'salvare l\'ordine.';
            }
            if (ok && gtCustomer === 'true') {
            	if (gtNum === '') {
            		ok = false;
            		msg += 'inserire il numero dell\'ordine del cliente Garden Team.';
            		if (stringGtDate === '') {
            			ok = false;
                		msg += 'inserire la data dell\'ordine del cliente Garden Team.\nLa data' +
              		  		' di conegna deve essere settata almeno oggi'; 
            		}
            	}
            }
            if (ok) {
            	sendConfirm();
            } else {
                alert(msg);
            }
        });
    });
	
	AUI().use('aui-base', 'datatype-number', function(A) {
        A.one("#<portlet:namespace/>printBtn").on('click', function() {
            var orderId = A.one('#<portlet:namespace/>orderId').get('value');
            var ok = false;
            var msg = 'Prima di procedere alla stampa delle etichette carrello,\n';
            if (orderId !== '') {
            	ok = true;
            } else {
                msg += 'salvare l\'ordine.';
            }
            
            if (ok) {
                printCartLabel();
            } else {
                alert(msg);
            }
        });
    });
	
	AUI().use('aui-base', function(A) {
        A.one("#<portlet:namespace/>vats").on('change', function() {
            A.one('#<portlet:namespace/>vatSelected').set('value',
            		A.one("#<portlet:namespace/>vats").get('value'));
        });
    });	
	
	AUI().use('aui-base', function(A) {
        A.one("#<portlet:namespace/>enableVATSelectCheckbox").on('click', function(e) {
        	if (A.one('#<portlet:namespace/>enableVATSelect').get('value') === 'true') {
        		A.one('#<portlet:namespace/>vats').removeAttribute('disabled');
        		A.one('#<portlet:namespace/>vats').removeClass('disabled');
        	} else {
        		A.one('#<portlet:namespace/>vats').setAttribute('disabled', 'true');
                A.one('#<portlet:namespace/>vats').addClass('disabled');
        	}
        });
    });
	
	AUI().use('aui-base', 'aui-io-request', 'aui-modal', function(A) {
	    A.one('#<portlet:namespace/>changeAddressBTN').on('click', function() {
	    	if (!orderJSON.centro) {
	    		Liferay.Util.openWindow({
	                dialog : {
	                    centered : true,
	                    modal : true
	                },
	                id : '<portlet:namespace/>cutomerDialog',
	                title : 'Cerca Cliente',
	                uri : '<%=searchCustomerURL%>'
	            });
	    	} else {
	    		alert('Non puoi modificare il cliente per un ordine salvato.');
	    	}
	    });
	    Liferay.provide(window, 'closePopup', function(response, dialogId) {
	    	console.log('CICCIO');
	        var dialog = Liferay.Util.Window.getById(dialogId);
	        
	        switch (dialogId) {
	            case '<portlet:namespace/>cutomerDialog':
	            	if (response.code === 0) {
	            		var object = JSON.parse(response.data)
	            		var customer = JSON.parse(object.customer);
	                    var email = JSON.parse(object.contact);
	                    var curCustomer = "<%=customer.getRagioneSociale1()%>";
	                    if (confirm("Confermi la modifica dal cliente:\n\t" + curCustomer + "\nal cliente:\n\t" + customer.ragioneSociale1)) {
	                    	var ok = false;
	                    	if (customer.siglaStato === 'I' || customer.siglaStato === 'SUI') {
	                    		ok = customer.siglaStato === '<%=customer.getSiglaStato()%>';
	                    	} else {
	                    		ok = '<%=customer.getSiglaStato()%>' !== 'I' && '<%=customer.getSiglaStato()%>' !== 'SUI';
	                    	} 
	                    	if (ok) {
	                    		A.one('#<portlet:namespace/>customerCode').set('value', customer.codiceAnagrafica);
	                            A.one('#<portlet:namespace/>companyName').set('value', customer.ragioneSociale1);
	                            A.one('#<portlet:namespace/>address').set('value', customer.indirizzo + " " + customer.CAP + " - "
	                                    + customer.comune + " (" + customer.siglaProvincia + ") - " + customer.siglaStato);
	                            A.one('#<portlet:namespace/>email').set('value', email);
	                            customerCode = customer.codiceAnagrafica;
	                            dialog.hide();
	                    	} else {
	                    		alert('Non è possibile modificare il cliente con quello selezionato.');
	                    	}
	                    }
	            	}
	                break;
	            case '<portlet:namespace/>itemDialog':
//	              A.one('#<portlet:namespace/>code').set('value', data);
	            	var modal;
	            	dialog.hide();
	                A.io.request('<%=getPlants%>', {
	                    method: 'POST',
	                    data : {
	                        <portlet:namespace />itemCode : response,
	                         <portlet:namespace />itemName : '',
	                        <portlet:namespace />supplier : '',
	                        <portlet:namespace />category : '-1',
	                        <portlet:namespace />onlyAvailable : false,
	                        <portlet:namespace />onlyActive : true,
	                        <portlet:namespace />onlyInactive : true
	                        
	                    },
	                    on: {
	                        start: function() {
	                            modal = new A.Modal({
	                                bodyContent : '<div class="loading-animation"/>',
	                                centered : true,
	                                headerContent : '<h3>Loading...</h3>',
	                                modal : true,
	                                render : '#modal',
	                                close: false,
	                                zIndex: 99999,
	                                width : 450
	                            }).render();
	                        },
	                        success: function () {
	                            var res = JSON.parse(this.get('responseData'));
	                            console.log(res);
	                            res.forEach(function(item) {
	                                if (item.codice === response){
	                                    updateFields(item);
	                                }
	                            });
	                            modal.hide();
	                        },
	                        error: function name() {
	                            modal.hide();
	                        }
	                    }
	                });
	                break;
	        }
	    });
	});
	
// 	function <portlet:namespace />initEditor() {
// //         return 'Note ...';
//         var A = AUI();
//         console.log(A.one('#<portlet:namespace/>note').get('value'));
//         return A.one('#<portlet:namespace/>note').get('value');
//     }
	
// 	function <portlet:namespace />save() {
// 		AUI().use('aui-base', function(A) {
//             A.one('#<portlet:namespace/>note').val(window.<portlet:namespace />editor.getHTML());
// 		});
//     }
</aui:script>
