<%@page import="java.util.Date"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.dao.search.DisplayTerms"%>
<%@page import="com.liferay.portal.kernel.dao.search.SearchContainer"%>
<%@ include file="../init.jsp"%>
<%
    SearchContainer searchContainer = (SearchContainer) request
		.getAttribute("liferay-ui:search:searchContainer");
    DisplayTerms displayTerms = searchContainer.getDisplayTerms();
    String customerCode = ParamUtil.getString(request, "customerCode");
    String customerName = ParamUtil.getString(request, "customerName");
    boolean showsInactive = ParamUtil.getBoolean(renderRequest, "show-inactive", true);
    boolean showsDraft = ParamUtil.getBoolean(renderRequest, "show-draft", true);
    boolean showsPending = ParamUtil.getBoolean(renderRequest, "show-pending", true);
    boolean showsApproved = ParamUtil.getBoolean(renderRequest, "show-approved", true);
    boolean showsProcessed = ParamUtil.getBoolean(renderRequest, "show-processed");
//     Calendar calendar = Calendar.getInstance();
//     calendar.clear();
//     calendar.set(Calendar.MONTH, 0);
//     calendar.set(Calendar.YEAR, Utils.getYear());
//     calendar.set(Calendar.DAY_OF_MONTH, 1);
    long firstYearDay = Utils.today().getTimeInMillis();//calendar.getTimeInMillis();
//     calendar.set(Calendar.MONTH, 11);
//     calendar.set(Calendar.DAY_OF_MONTH, 31);
    long lastYearDay = Utils.today().getTimeInMillis();//calendar.getTimeInMillis();
    
    long orderDate = ParamUtil.getLong(request, "orderDateLng", firstYearDay);
    long shipDate = ParamUtil.getLong(request, "shipDateLng", lastYearDay);
%>
<liferay-ui:search-toggle buttonLabel="search"
	displayTerms="<%=displayTerms%>" id="toggle_id_order_search">
<%-- 	<aui:input label="shows-processed" name="showsProcessed" type="checkbox" checked="<%= showsProcessed%>" /> --%>
	<aui:input label="customer-code" name="customerCode" value="<%=customerCode%>" />
	<aui:input label="company-name" name="customerName" value="<%=customerName%>" />
	<aui:input id="orderDate" type="text" name="orderDate" cssClass="input-small"
		label="ship-date-from" inlineField="true" />
	<aui:input id="orderDateLng" type="hidden" name="orderDateLng"
        label="order-date" inlineField="true" value="<%=orderDate%>" />
	<aui:input id="shipDate" type="text" name="shipDate" label="date-to" cssClass="input-small"
		inlineField="true" />
	<aui:input id="shipDateLng" type="hidden" name="shipDateLng" label="ship-date"
        inlineField="true" value="<%=shipDate%>" />
    <aui:field-wrapper label="shows-orders">
        <aui:input id="show-inactive" name="show-inactive" type="checkbox" label="inactive" checked="<%=showsInactive %>"/>
        <aui:input id="show-draft" name="show-draft" type="checkbox" label="draft" checked="<%= showsDraft %>"/>
        <aui:input id="show-pending" name="show-pending" type="checkbox" label="pending" checked="<%=showsPending %>"/>
        <aui:input id="show-approved" name="show-approved" type="checkbox" label="approved" checked="<%=showsApproved %>"/>
        <aui:input id="show-processed" name="show-processed" type="checkbox" label="shows-processed" checked="<%= showsProcessed%>" />
    </aui:field-wrapper>
    <aui:button-row cssClass="center-h">
        <aui:button id="searchBTN" name="searchBTN" icon="icon-search" value="search" type="submit" />
        <aui:button id="resetBTN" name="resetBTN" icon="icon-trash" cssClass="btn-warning" value="reset" type="cancel" />
    </aui:button-row>
</liferay-ui:search-toggle>

<%!private static Log _log = LogFactoryUtil
            .getLog("docroot.html.orderrecap.order_search_jsp");%>

<aui:script>
	var orderDate = new Date(<%=orderDate %>);
	var shipDate = new Date(<%=shipDate %>);
	var firstYearDay = new Date(<%=firstYearDay %>);
    var lastYearDay = new Date(<%=lastYearDay %>);

	AUI().use('aui-datepicker', function(A) {
        var orderDatePicker = new A.DatePicker({
            trigger : '#<portlet:namespace/>orderDate',
            mask : '%d/%m/%Y',
            popover : {
                position : 'right',
                toolbars : {
                    header : [ {
                        icon : 'icon-trash',
                        label : 'Cancella',
                        on : {
                            click : function() {
                            	orderDatePicker.clearSelection();
                            }
                        }
                    } ]
                },
                zIndex : 1000
            },
            on : {
                selectionChange : function(event) {
                	A.one("#<portlet:namespace/>orderDateLng").set('value', new Date(event.newSelection).getTime());
                }
            }
        });
        var shipDatePicker = new A.DatePicker({
            trigger : '#<portlet:namespace/>shipDate',
            mask : '%d/%m/%Y',
            popover : {
                position : 'right',
                toolbars : {
                    header : [ {
                        icon : 'icon-trash',
                        label : 'Cancella',
                        on : {
                        	click : function() {
                                shipDatePicker.clearSelection();
                            }
                        }
                    } ]
                },
                zIndex : 1000
            },
            on : {
            	selectionChange : function(event) {
           		    A.one("#<portlet:namespace/>shipDateLng").set('value', new Date(event.newSelection).getTime());
                }
            }
        });
    });
	
	AUI().use('aui-base', function(A) {
        A.one("#<portlet:namespace/>resetBTN").on('click', function(e) {
        	A.one('#<portlet:namespace/>customerCode').set('value', '');
        	A.one('#<portlet:namespace/>customerName').set('value', '');
            var myDateString = ('0' + firstYearDay.getDate()).slice(-2) + '/'
             + ('0' + (firstYearDay.getMonth()+1)).slice(-2) + '/'
             + firstYearDay.getFullYear();
            A.one('#<portlet:namespace/>orderDate').set('value',
                    myDateString);
            A.one('#<portlet:namespace/>orderDateLng').set('value',
            		firstYearDay);
            myDateString = ('0' + lastYearDay.getDate()).slice(-2) + '/'
            + ('0' + (lastYearDay.getMonth()+1)).slice(-2) + '/'
            + lastYearDay.getFullYear();
            A.one('#<portlet:namespace/>shipDate').set('value',
                    myDateString);
            A.one('#<portlet:namespace/>shipDateLng').set('value',
            		lastYearDay);
            
            A.one('#<portlet:namespace/>customerCode').set('value','');
            A.one('#<portlet:namespace/>show-inactiveCheckbox').set('checked', true);
            A.one('#<portlet:namespace/>show-draftCheckbox').set('checked', true);
            A.one('#<portlet:namespace/>show-pendingCheckbox').set('checked', true);
            A.one('#<portlet:namespace/>show-approvedCheckbox').set('checked', true);
            A.one('#<portlet:namespace/>show-processedCheckbox').set('checked', false);
            
            A.one('#<portlet:namespace/>show-inactive').set('value', true);
            A.one('#<portlet:namespace/>show-draft').set('value', true);
            A.one('#<portlet:namespace/>show-pending').set('value', true);
            A.one('#<portlet:namespace/>show-approved').set('value', true);
            A.one('#<portlet:namespace/>show-processed').set('value', false);
        });
    });
	
	AUI().ready(function(A) {
        AUI().use('aui-base', function(A) {
            A.one("#<portlet:namespace/>orderDate").set('value', convertDate(orderDate));
            A.one("#<portlet:namespace/>shipDate").set('value', convertDate(shipDate));
        });
    });
	
	function convertDate(inputFormat) {
		function pad(s) {
			return (s < 10) ? '0' + s : s;
		}
		var d = new Date(inputFormat);
		return [ pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear() ]
				.join('/');
	}
</aui:script>