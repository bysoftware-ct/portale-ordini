<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../init.jsp"%>

<%
    String customerCode = ParamUtil.getString(renderRequest, "customerId", null);
    String backURL = ParamUtil.getString(renderRequest, "backURL");
    _log.debug(customerCode);

    SimpleDateFormat sdf = new SimpleDateFormat();
    NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
%>
<liferay-ui:header backURL="<%= backURL%>" title="order" />
<div id="myTab">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#pending-order-tab"><liferay-ui:message
                    key="pending-order" /></a></li>
        <li><a href="#ready-order-tab"><liferay-ui:message
                    key="ready-order" /></a></li>
        <li><a href="#processed-order-tab"><liferay-ui:message
                    key="processed-order" /></a></li>
    </ul>
    <div class="tab-content">
        <div id="pending-order-tab" class="tab-pane">
            <%@ include file="order-view/pending-order.jsp"%>
        </div>
        <div id="ready-order-tab" class="tab-pane">
            <%@ include file="order-view/ready-order.jsp"%>
        </div>
        <div id="ready-order-tab" class="tab-pane">
            <%@ include file="order-view/processed-order.jsp"%>
        </div>
    </div>
</div>

<aui:script>
    AUI().use('aui-tabview', function(A) {
        var tab = new A.TabView({
            srcNode : '#myTab'
        }).render();
    });
</aui:script>

<%!private static Log _log = LogFactoryUtil
            .getLog("docroot.html.ordini.view-orders_jsp");%>