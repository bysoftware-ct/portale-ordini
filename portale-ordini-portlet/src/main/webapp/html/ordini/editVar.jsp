<%@page import="it.bysoftware.ct.utils.ResourceId"%>
<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../init.jsp"%>
<%
long rowId = ParamUtil.getLong(renderRequest, "rowId", 0);
long cartId = ParamUtil.getLong(renderRequest, "cartId", 0);
String itemCode = ParamUtil.getString(renderRequest, "itemCode");
String itemName = ParamUtil.getString(renderRequest, "itemName");
String varCode = ParamUtil.getString(renderRequest, "varCode");
%>
<portlet:resourceURL var="getPlants"
    id="<%=ResourceId.autoComplete.toString() %>"/>

<aui:input id="itemCode" name="itemCode" cssClass="input-large"
	inlineField="true" label="code" value="<%=itemCode %>" type="hidden"/>
<aui:input id="itemName" name="itemName" ilabel="name" cssClass="input-xxlarge"
	value="<%=itemCode %>" disabled="true"/>
<aui:input id="varCode" name="varCode" label="variant" cssClass="input-xlarge"
	value="<%=varCode %>" type="hidden"/>
<aui:select id="itemVariants" name="itemVariants" label="variants" />

<aui:button name="closeDialog" type="submit" icon="save" value="apply" disabled="true"/>
 
<aui:script use="aui-base">
var rowId = '<%=rowId%>';
var cartId = '<%=cartId%>';
var itemCode = '<%=itemCode%>';
var itemName = '<%=itemName%>';
var varCode = '<%=varCode%>';
var getPlantsURL = '<%=getPlants %>';
var dettagliPianta;
var selected;

AUI().ready(function(A){
	AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
        var errorBlock=A.one('#group-error-block');
        A.io.request(getPlantsURL, {
            dataType: 'json',
        	method: 'POST',
            data: {
            	<portlet:namespace />itemCode : itemCode,
             	<portlet:namespace />supplier : '',
             	<portlet:namespace />category : '',
             	<portlet:namespace />onlyActive : true,
             	<portlet:namespace />onlyInactive : true,
             	<portlet:namespace />onlyAvailable : false
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                    var data = this.get('responseData');
                    var varianti = [];
                    if (data.length > 0){
                    	varianti = data[0].varianti;
                    	dettagliPianta = data[0].dettagliPianta;
                    }
                    var options = '<option value="" selected>Seleziona variante...</option>';
            	    for (var i = 0; i < varianti.length; i++) {
            	        var variant = varianti[i];
            	        if (variant.codiceVariante.toString() === varCode) {
            	        	options += '<option value="' + variant.codiceVariante + '" label="' + variant.descrizione + '" selected></option>';
            	        } else {
            	        	options += '<option value="' + variant.codiceVariante + '" label="' + variant.descrizione + '"></option>';
            	        }
            	    }
            	    A.one('#<portlet:namespace/>itemVariants').setHTML(options);
            	    A.one('#<portlet:namespace/>itemVariants').set('value', varCode);
                    modal.hide();
                },
                error: function name() {
                	console.error("ERRORE!!!");
                    modal.hide();
                }
            }
        });
    });
});
A.one('#<portlet:namespace/>itemVariants').on('change', function(event){
	A.one('#<portlet:namespace/>varCode').val(A.one('#<portlet:namespace/>itemVariants').get('value'));
	A.one('#<portlet:namespace/>closeDialog').removeAttribute('disabled');
    A.one('#<portlet:namespace/>closeDialog').removeClass('disabled');
});
A.one('#<portlet:namespace/>closeDialog').on('click', function(event) {
	var selectedCode = A.one('#<portlet:namespace/>varCode').get('value');
	var selectedVar;
	A.one('#<portlet:namespace/>itemVariants').get('childNodes')._nodes.forEach(function(elt, i) {
		if (elt.value === selectedCode) {
			selectedVar = elt.label;
		}
	});
	var data = {
		rowId: Number(rowId),
		cartId: Number(cartId),
		dettagliPianta: dettagliPianta,
		varCode: selectedCode,
		stringa: selectedVar
	};
	Liferay.Util.getOpener().<portlet:namespace/>closePopup(data, '<portlet:namespace/>varDialog');
});
</aui:script>