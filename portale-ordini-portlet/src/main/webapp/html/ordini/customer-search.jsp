<%@page import="it.bysoftware.ct.service.StatiLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Stati"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.dao.search.DisplayTerms"%>
<%@page import="com.liferay.portal.kernel.dao.search.SearchContainer"%>
<%@ include file="../init.jsp"%>
<%
    SearchContainer searchContainer = (SearchContainer) request
            .getAttribute("liferay-ui:search:searchContainer");
    DisplayTerms displayTerms = searchContainer.getDisplayTerms();
    String customerCode = ParamUtil.getString(request, "customerCode");
    String businessName = ParamUtil.getString(request, "businessName");
    String vatCode = ParamUtil.getString(request, "vatCode");
    String country = ParamUtil.getString(request, "country");
    String city = ParamUtil.getString(request, "city");
    boolean search = ParamUtil.getBoolean(request, "search", false);
    List<Stati> countries = StatiLocalServiceUtil.getStatis(0, StatiLocalServiceUtil.getStatisCount());
%>
<liferay-ui:search-toggle buttonLabel="search"
	displayTerms="<%=displayTerms%>" id="toggle_id_customer_search">
	<aui:input label="code" name="customerCode" value="<%=customerCode%>" />
	<aui:input label="company-name" name="businessName" value="<%=businessName%>" />
	<aui:input label="country" name="country" value="<%=country%>" type="hidden" />
	<aui:input label="city" name="city" value="<%=city%>" />
	<aui:input label="search" name="search" value="<%=String.valueOf(search)%>" />
	<aui:select id="countries" name="countries" label="country">
	   <aui:option label="all" value="" selected="<%=country.isEmpty()%>" />
        <%
            if (Validator.isNotNull(countries)) {
                for (Stati s : countries) {
                    boolean selected = false;
                    if (country.equals(s.getPrimaryKey())) {
                        selected = true;
                    }
        %>
        <aui:option label="<%=s.getStato()%>"
            value="<%=s.getPrimaryKey()%>" selected="<%=selected%>" />
        <%
                }
            }
        %>
     </aui:select>
     <aui:input label="vat-code" name="vatCode" value="<%=vatCode%>" />
</liferay-ui:search-toggle>

<aui:script>
AUI().use('aui-base', function(A) {
	A.one("#<portlet:namespace/>countries").on('change', function() {
		A.one("#<portlet:namespace/>country").set('value',
				A.one("#<portlet:namespace/>countries").get('value'));
	})
})
</aui:script>