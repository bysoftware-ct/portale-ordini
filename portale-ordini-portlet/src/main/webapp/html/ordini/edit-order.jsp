<%@page import="it.bysoftware.ct.service.ArticoliLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Articoli"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowConstants"%>
<%@page import="it.bysoftware.ct.utils.OrderState"%>
<%@page import="com.liferay.portlet.documentlibrary.util.DLUtil"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactory"%>
<%@page import="it.bysoftware.ct.service.RigaLocalServiceUtil"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../init.jsp"%>

<%
    String activeTab = ParamUtil.getString(renderRequest, "activeTab", "");
    boolean view = ParamUtil.getBoolean(renderRequest, "view");
    String customerCode = ParamUtil.getString(renderRequest, "customerId");
    String backURL = ParamUtil.getString(renderRequest, "backURL");
    long orderId = ParamUtil.getLong(renderRequest, "orderId", -1);
    String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
    String tmp = ParamUtil.getString(renderRequest, "orderJSON", "");
    JSONObject orderJSON = JSONFactoryUtil.createJSONObject();
    Ordine order = null;
    if (tmp.equals("")) {
        if (orderId == -1) {
            orderJSON = JSONFactoryUtil.createJSONObject();
        } else {
            _log.info("[edit-order.jsp] Retrieving order: " + orderId + "...");
            order = OrdineLocalServiceUtil.getOrdine(orderId);
            orderJSON = JSONFactoryUtil.createJSONObject(
                    JSONFactoryUtil.looseSerialize(order));
            List<Carrello> carts = CarrelloLocalServiceUtil.findCartsInOrder(orderId);
            orderJSON.put("carts", JSONFactoryUtil.createJSONArray());
            for (int i = 0; i < carts.size(); i++) {
                JSONArray jsonCarts = orderJSON.getJSONArray(Constants.CARTS);
                jsonCarts.put(JSONFactoryUtil.createJSONObject(JSONFactoryUtil.looseSerialize(carts.get(i))));
                List<Riga> rows = RigaLocalServiceUtil.findRowsInCart(carts.get(i).getId());
                jsonCarts.getJSONObject(i).put(Constants.ROWS, JSONFactoryUtil.createJSONArray());
                int plants = 0;
                int usedHeight = 0;
                if (carts.get(i).getGenerico()) {
                    usedHeight = -1;
                }
                boolean closed = false;
                for (int j = 0; j < rows.size(); j++) {
                    JSONArray jsonRows = jsonCarts.getJSONObject(i).getJSONArray(Constants.ROWS);
                    jsonRows.put(JSONFactoryUtil.createJSONObject(JSONFactoryUtil.looseSerialize(rows.get(j))));
                    Piante p = PianteLocalServiceUtil.getPiante(rows.get(j).getIdArticolo());
                    Articoli item = ArticoliLocalServiceUtil.fetchArticoli(p.getCodice());
                    jsonRows.getJSONObject(j).put("ean", p.getEan());
                    String pictureUrl = "/portale-ordini-portlet/icons/not-found.png";
                    try {
                        FileEntry picture = DLAppLocalServiceUtil.getFileEntry(Long.parseLong(p.getFoto1()));
                        pictureUrl = DLUtil.getPreviewURL(picture, picture.getFileVersion(), themeDisplay, "");
                    } catch (Exception ex) {
                        _log.warn("[edit-order.jsp] Exception retrieving picture: " + ex.getMessage());
                    }
                    String forma = p.getForma();
                    if (item != null && !item.getCategoriaMerceologica().isEmpty()) {
                    	CategorieMerceologiche shape = CategorieMerceologicheLocalServiceUtil.fetchCategorieMerceologiche(item.getCategoriaMerceologica());
                    	if (shape != null) {
                    		forma = shape.getDescrizione();
                    	}
                    }
                    jsonRows.getJSONObject(j).put("img", "<a href='" + pictureUrl +"' title='" + p.getNome() + "' target='_blank'><img src='" + pictureUrl + "' class='center-h' /></a>");
                    jsonRows.getJSONObject(j).put("itemCode", p.getCodice());
                    jsonRows.getJSONObject(j).put("name", p.getNome() + "\nVaso: " + p.getVaso() + "\n" + forma + "\nH. " + p.getAltezzaPianta());
                    double prezzoSc = rows.get(j).getPrezzo() - rows.get(j).getImportoVarianti();
                    jsonRows.getJSONObject(j).put("prezzoSc", prezzoSc);
                    jsonRows.getJSONObject(j).put("prezzoUn", prezzoSc / (1 - rows.get(j).getSconto() / 100));
                    jsonRows.getJSONObject(j).put("prezzoEtichetta", rows.get(j).getPrezzoEtichetta());
                    jsonRows.getJSONObject(j).put("ean", rows.get(j).getEan());
                    plants += rows.get(j).getPianteRipiano();
                    if (usedHeight >=0) {
                        usedHeight += p.getAltezza();
                        if (rows.get(j).getS2() > 0) {
                            usedHeight += p.getAggiuntaRipiano();
                        }
                        if (rows.get(j).getS3() > 0) {
                            usedHeight += p.getAggiuntaRipiano();
                        }
                        if (rows.get(j).getS4() > 0) {
                            usedHeight += p.getAggiuntaRipiano();
                        }
                    }
                }
                jsonCarts.getJSONObject(i).put("plants", plants);
                jsonCarts.getJSONObject(i).put("usedHeight", usedHeight);
            }
            _log.debug("[edit-order.jsp] Retrieved order: " + orderJSON);
        }
    } else {
        orderJSON = JSONFactoryUtil.createJSONObject(tmp);
    }
%>

<portlet:resourceURL var="saveOrderURL" 
    id="<%=ResourceId.saveOrder.name() %>" />
<portlet:resourceURL var="sendConfirmURL" 
    id="<%=ResourceId.sendConfirm.name() %>" />
<portlet:resourceURL var="printCartLabelURL" 
    id="<%=ResourceId.printCartLabel.name() %>" />
<portlet:resourceURL var="getPlants"
    id="<%=ResourceId.autoComplete.toString() %>"/>
        
<liferay-portlet:renderURL var="refreshURL">
    <portlet:param name="mvcPath" value="/html/ordini/edit-order.jsp" />
    <portlet:param name="backURL" value="<%=backURL%>" />
</liferay-portlet:renderURL>
                
<liferay-ui:header backURL="<%= backURL%>" title="fill-order" />
<aui:nav-bar>
    <aui:nav>
        <c:if test="<%=view %>">
	        <aui:nav-item id="saveBtn" iconCssClass="icon-save" label="save"
	           selected='<%="save".equals(toolbarItem)%>' cssClass="hidden" />
        </c:if>
        <c:if test="<%=!view %>">
            <aui:nav-item id="saveBtn" iconCssClass="icon-save" label="save"
               selected='<%="save".equals(toolbarItem)%>'/>
        </c:if>
        <aui:nav-item id="printBtn" iconCssClass="icon-print" label="print"
            selected='<%="print".equals(toolbarItem)%>' />
        <c:if test="<%=view %>">
            <aui:nav-item id="sendBtn" iconCssClass="icon-envelope" label="send" 
                selected='<%="send".equals(toolbarItem)%>' cssClass="hidden" />
        </c:if>
        <c:if test="<%=!view %>">
	        <aui:nav-item id="sendBtn" iconCssClass="icon-envelope" label="send" 
	            selected='<%="send".equals(toolbarItem)%>' />
        </c:if>
    </aui:nav>
    <aui:nav-bar-search cssClass="pull-right" >
        <c:choose>
            <c:when test="<%= order == null %>">
                <aui:workflow-status showIcon="<%= false %>" showLabel="<%= false %>"
                    status="<%= WorkflowConstants.STATUS_INCOMPLETE %>" />
            </c:when>
            <c:otherwise>
                <liferay-ui:message key="order-x"
                    arguments="<%=new String[] { String.valueOf(order.getNumero()), order.getCentro() } %>" />
                <aui:workflow-status showIcon="<%= false %>" showLabel="<%= false %>"
                    status="<%= order.getStato() %>" />
            </c:otherwise>
        </c:choose>
    </aui:nav-bar-search>
</aui:nav-bar>
<div id="myTab">
	<ul class="nav nav-tabs">
	   <c:if test="<%=!view %>">
		   <li id="order-items" class="<%= "".equals(activeTab) || "order-items".equals(activeTab) ? "active" : ""%>">
		       <a href="#order-items-tab"><liferay-ui:message key="order-items" /></a>
	       </li>
       </c:if>
       <li id="order-body" class="<%= "order-body".equals(activeTab) ? "active" : ""%>">
           <a href="#order-body-tab"><liferay-ui:message key="order-body" /></a>
       </li>
       <li id="order-footer" class="<%= "order-footer".equals(activeTab) ? "active" : ""%>">
           <a href="#header-footer-tab"><liferay-ui:message key="header-footer-fields" /></a>
       </li>
	</ul>
	<div class="tab-content">
	    <c:if test="<%=!view %>">
			<div id="order-items-tab" class="tab-pane">
				<%@ include file="order-items.jsp"%>
			</div>
		</c:if>
		<div id="order-body-tab" class="tab-pane">
            <%@ include file="order.jsp"%>
        </div>
		<div id="header-footer-tab" class="tab-pane">
            <%@ include file="order-header-footer.jsp"%>
        </div>
	</div>
</div>

<aui:script>
    var customerCode = '<%= customerCode%>';
    var saveOrderURL = '<%= saveOrderURL %>';
    var sendConfirmURL = '<%= sendConfirmURL %>';
    var printCartLabelURL = '<%= printCartLabelURL %>';
    var refreshURL = '<%= refreshURL %>';
    function <portlet:namespace />initEditor() {
        return '';
    }
    
	AUI().use('aui-tabview', function(A) {
		var tab = new A.TabView({
			srcNode : '#myTab'
		}).render();
	});
	AUI().ready(function(A){
        A.one('body').on('keydown', function(e) {
            var tmp = A.one('#<portlet:namespace />orderJSON').val();
            if (tmp !=='{}' && (e.which || e.keyCode) == 116) {
                if (!confirm('Vuoi abbandonare le modifiche non salvate?')) {
                    e.preventDefault();
                }
            }
        });
        
        AUI().use('aui-base', function(A) {
           A.all('.lfr-nav-item').on('click', function(e) {
               if (!confirm('Vuoi abbandonare le modifiche non salvate?')) {
                   e.preventDefault();
               }
           });
        });
    });
    
    AUI().use('aui-base', 'datatype-number', function(A) {
        A.one("#<portlet:namespace/>saveBtn").on('click', function() {
            var stringOrderDate = A.one('#<portlet:namespace/>orderDate').get('value');
            var orderDateSplitted = stringOrderDate.split("/");
            var orderDate = new Date(orderDateSplitted[2], orderDateSplitted[1] - 1, orderDateSplitted[0]);

            var stringDeliveryDate =  A.one('#<portlet:namespace/>shipDate').get('value')
            var deliveryDateSplitted = stringDeliveryDate.split("/");
            var deliveryDate = new Date(deliveryDateSplitted[2], deliveryDateSplitted[1] - 1, deliveryDateSplitted[0]);
            
            var msg;
            var ok = false;
            if (deliveryDate >= orderDate) {
                if (!orderJSON.carts) {
                    msg = 'Inserire almeno un carrello nell\'ordine.';
                } else {
                    ok = true;
                }
            } else {
                msg = 'La data di consegna deve essere successiva alla data dell\'ordine.';
            }
            if (confirm('Confermi il salvataggio delle modifiche effettuate?')) {
                if (ok) {
                    saveOrder();
                } else {
                    alert(msg);
                }
            }
        });
    });
</aui:script>