<%@page import="it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK"%>
<%@page import="it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.ClientiFornitoriDatiAgg"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="it.bysoftware.ct.service.CategorieMerceologicheLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.CategorieMerceologiche"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.liferay.portal.kernel.repository.model.FileEntry"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="java.io.File"%>
<%@page import="it.bysoftware.ct.utils.ResourceId"%>
<%@page import="it.bysoftware.ct.service.VariantiPiantaLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.VariantiPianta"%>
<%@page import="it.bysoftware.ct.service.VarianteLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Variante"%>
<%@page import="com.liferay.portal.kernel.exception.SystemException"%>
<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.service.CategorieLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Categorie"%>
<%@page import="java.util.Locale"%>
<%@page import="it.bysoftware.ct.service.PianteLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Piante"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@include file="../init.jsp"%>

<%    
    boolean active = ParamUtil.getBoolean(renderRequest, "active", true);
	boolean inactive = ParamUtil.getBoolean(renderRequest, "inactive", false);
    boolean available = ParamUtil.getBoolean(renderRequest, "available", false);
    long category = ParamUtil.getLong(renderRequest, "categories", -1);
    String supplierId = ParamUtil.getString(renderRequest, "suppliers", "");
    ClientiFornitoriDatiAgg customerData = ClientiFornitoriDatiAggLocalServiceUtil.
            getClientiFornitoriDatiAgg(new ClientiFornitoriDatiAggPK(customerCode, false));
    String code = ParamUtil.getString(renderRequest, "code", "");
    String name = ParamUtil.getString(renderRequest, "name", "");
    List<CategorieMerceologiche> shapes = CategorieMerceologicheLocalServiceUtil.
            getCategorieMerceologiches(0, CategorieMerceologicheLocalServiceUtil.
                    getCategorieMerceologichesCount());
    List<AnagraficheClientiFornitori> suppliers =
            AnagraficheClientiFornitoriLocalServiceUtil.findSuppliers();
    List<Categorie> categories = CategorieLocalServiceUtil.findAll();
    List<Piante> piante = new ArrayList<Piante>();
    if (category != -1) {
	    for(Categorie c : categories) {
	        if(category > 0){
	            if(category == c.getId()){
	                piante = PianteLocalServiceUtil.findPlants(category, active, inactive,
	                        supplierId, name, code, false);
	            }
	        } else if(c.getPredefinita()){
	            piante = PianteLocalServiceUtil.findPlants(c.getId(), active, inactive,
	                    supplierId, name, code, false);
	        }
	    }
    } else {
        piante = PianteLocalServiceUtil.findPlants(active, inactive, supplierId, name, code, false);
    }
%>
<liferay-portlet:renderURL var="editVarUrl"
    windowState="<%=LiferayWindowState.POP_UP.toString()%>">
    <liferay-portlet:param name="search" value="true"/>
    <liferay-portlet:param name="rowId" value="placeholder"/>
    <liferay-portlet:param name="mvcPath"
        value="/html/ordini/editVar.jsp" />
</liferay-portlet:renderURL>

<liferay-portlet:renderURL var="itemURL"
    windowState="<%=LiferayWindowState.POP_UP.toString()%>">
    <liferay-portlet:param name="search" value="true"/>
    <liferay-portlet:param name="mvcPath"
        value="/html/items/view.jsp" />
</liferay-portlet:renderURL>
<liferay-portlet:renderURL varImpl="searchURL">
    <portlet:param name="mvcPath" value="/html/ordini/edit-order.jsp" />
    <portlet:param name="customerId" value="<%=customerCode %>" />
</liferay-portlet:renderURL>
<portlet:resourceURL var="addQuantityURL" 
        id="<%=ResourceId.addQuantity.name() %>" />
<portlet:resourceURL var="addPlatformURL" 
        id="<%=ResourceId.addPlatform.name() %>" />
<portlet:resourceURL var="addCartURL" 
        id="<%=ResourceId.addCart.name() %>" />
        
<aui:layout>
    <aui:column first="true" columnWidth="30">
        <div class="search-form">
            <aui:fieldset label="search-entries" >
                <aui:input name="active" label="active" type="checkbox"
                    checked="<%=active%>" />
                <aui:input name="inactive" label="inactive" type="checkbox"
                   		checked="<%=inactive%>" />
                <aui:input name="available" label="active-plants" type="checkbox"
                    checked="<%=available%>" />
                <aui:select id="categories" name="categories" >
                <aui:option label="all" value="-1" selected="<%= category == -1%>" />
                <%
                    if (Validator.isNotNull(categories)) {
                        for (Categorie c : categories) {
                            boolean selected = false;
                            if (category > 0) {
                                selected = c.getId() == category;
//                             } else {
//                                 selected = c.getPredefinita();
                            }
                %>
                   <aui:option label="<%=c.getNomeIta()%>" value="<%=c.getId()%>"
                       selected="<%= selected%>" />
                <%
                        }
                    }
                %>
               </aui:select>
               <aui:select id="suppliers" name="suppliers">
                  <%
                       if (Validator.isNotNull(suppliers)) {
                           for (AnagraficheClientiFornitori s : suppliers) {
                           %>
                               <aui:option label="<%=s.getRagioneSociale1()%>"
                                   value="<%=s.getCodiceAnagrafica()%>"
                                   selected="<%= s.getCodiceAnagrafica().equals(String.valueOf(supplierId))%>" />
                           <%
                           }
                       }
                   %>
                   <aui:option label="all" value="" selected="<%= supplierId.isEmpty()%>" />
               </aui:select>
               <aui:input id="code" name="code" inlineField="true" label="code"
                   helpMessage="help-search"/>
               <aui:button id="searchBtn" icon="icon-search" value="advanced-search" />
           </aui:fieldset>
       </div>
   </aui:column>
   <aui:column columnWidth="70">
        <aui:fieldset label="item-details">
            <aui:form>
                <aui:input id="itemId" name="itemId" type="hidden"/>
	            <aui:input id="itemCode" name="itemCode" cssClass="input-small"
	                inlineField="true" disabled="true" label="code"/>
	            <aui:input id="itemName" name="itemName" cssClass="input-large"
	                inlineField="true" disabled="true" label="name"/>
	            <aui:input id="itemVase" name="itemVase" cssClass="input-small"
	                inlineField="true" disabled="true" label="vase"/>
	                <aui:input id="itemHeight" name="itemHeight" cssClass="input-mini"
                    inlineField="true" disabled="true" label="plant-height"/>                
	            <aui:input id="platformHeight" name="platformHeight" cssClass="input-mini"
	                inlineField="true" disabled="true" label="Alt. ripiano"/>
	            <aui:select id="itemShapes" name="itemShapes" label="shape" disabled="true"
	                inlineField="true">
	                <aui:option label="" value="0" />
	            <%
	                if (Validator.isNotNull(shapes)) {
	                    for (CategorieMerceologiche s : shapes) {
	            %>
	               <aui:option label="<%=s.getDescrizione()%>" value="<%=s.getCodiceCategoria()%>" />
	            <%
	                    }
	                }
	            %>
	           </aui:select>
	           <aui:select id="itemVariants" name="itemVariants" label="variants"
                    inlineField="true" />
	           <aui:input id="itemAvailability" name="itemAvailability" cssClass="input-small"
	                disabled="true" inlineField="true" label="availability"/>
	           <aui:button-row>
	               <aui:input id="itemPrice1" name="itemPrice1" cssClass="input-small"
                        inlineField="true" label="price" prefix="€">
                        <aui:validator name="required" />
                        <aui:validator name="number" />
                   </aui:input>
                   <aui:input id="itemPrice2" name="itemPrice2" cssClass="input-small"
                        inlineField="true" label="con v. col." prefix="€">
                        <aui:validator name="required" />
                        <aui:validator name="number" />
                   </aui:input>
                   <aui:input id="variantPrice" name="variantPrice" cssClass="input-small"
                        inlineField="true" label="variant-price" prefix="€" disabled="true">
                        <aui:validator name="number" />
                   </aui:input>
                   <aui:input id="discount" name="discount" cssClass="input-small"
                        inlineField="true" label="discount" prefix="%" value="<%=customerData.getSconto() %>">
                        <aui:validator name="number" />
                   </aui:input>
                   <aui:input id="itemPrice3" name="itemPrice3" cssClass="input-small"
                        inlineField="true" label="per pianale" prefix="€" disabled="true">
                        <aui:validator name="number" />
                   </aui:input>
	           </aui:button-row>
	           <aui:button-row>
                   <aui:input id="supplierPrice" name="supplierPrice" cssClass="input-small" disabled="true"
                         inlineField="true" label="supplier-price" prefix="€" >
                         <aui:validator name="required" /> 
                         <aui:validator name="number" /> 
                    </aui:input>
                    <aui:input name="supplierVariant" type="hidden" value="0"/>
                    <aui:input name="enableSupplierPrice" type="checkbox" inlineField="true" label="enable" /> 
               </aui:button-row> 
	           <aui:input id="itemPerPlat" name="itemPerPlat" cssClass="input-small"
	                disabled="true" inlineField="true" label="plant-per-platform"/>
	           <aui:input id="item2Over" name="item2Over" cssClass="input-small"
	                disabled="true" inlineField="true" label="item-2-over"/>
	           <aui:input id="item3Over" name="item3Over" cssClass="input-small"
	                disabled="true" inlineField="true" label="item-3-over"/>
	           <aui:input id="item4Over" name="item3Over" cssClass="input-small"
	                disabled="true" inlineField="true" label="item-4-over"/>
	           <aui:input id="itemPerCart" name="itemPerCart" cssClass="input-small"
	                disabled="true" inlineField="true" label="plant-per-cart"/>
	           <aui:layout>
	               <aui:column columnWidth="30" first="true">
	                   <aui:input id="itemQuantity" name="itemQuantity" cssClass="input-small" label="quantity" value="0">
	                       <aui:validator name="digits" />
	                   </aui:input>
	                   <aui:button id="addQuantBTN" name="addQuantBTN" icon="icon-plus" cssClass="btn-warning" value="add" />
	               </aui:column>
	               <aui:column columnWidth="30">
	                   <aui:input id="platQuantity" name="platQuantity" cssClass="input-small" label="platforms" value="1">
	                       <aui:validator name="digits" />
	                   </aui:input>
	                   <aui:button id="addPlatBTN" name="addPlatBTN" icon="icon-inbox" cssClass="btn-info"  value="add-platforms" />
	               </aui:column>
	               <aui:column columnWidth="30">
	                   <aui:input id="cartQuantity" name="cartQuantity" cssClass="input-small" label="carts" value="1">
	                       <aui:validator name="digits" />
	                   </aui:input>
	                   <aui:button id="addCartBTN" name="addCartBTN" icon="icon-shopping-cart" cssClass="btn-success" value="add-carts" />                   
	               </aui:column>
	           </aui:layout>
           </aui:form>
	    </aui:fieldset>
    </aui:column>
</aui:layout>

<%-- <liferay-portlet:renderURL varImpl="iteratorURL"> --%>
<%--     <portlet:param name="mvcPath" value="/html/ordini/edit-order.jsp" /> --%>
<%--     <portlet:param name="categories" value="<%= String.valueOf(category) %>"/> --%>
<%--     <portlet:param name="active" value="<%= String.valueOf(active) %>"/> --%>
<%--     <portlet:param name="name" value="<%= name %>"/> --%>
<%--     <portlet:param name="code" value="<%= code %>"/> --%>
<%--     <portlet:param name="customerId" value="<%=String.valueOf(customerCode) %>" /> --%>
<%--     <portlet:param name="orderJSON" value="<%=String.valueOf(orderJSON) %>" /> --%>
<%-- </liferay-portlet:renderURL> --%>

<%--<liferay-ui:search-container delta="5" emptyResultsMessage="no-plant"
     iteratorURL="<%=iteratorURL%>" >
	<liferay-ui:search-container-results>
		<%
		    results = ListUtil.subList(piante,
				                    searchContainer.getStart(),
				                    searchContainer.getEnd());
				            total = piante.size();
				            pageContext.setAttribute("results", results);
				            pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	<liferay-ui:search-container-row
		className="it.bysoftware.ct.model.Piante" modelVar="pianta">
		<liferay-ui:search-container-column-text property="codice" name="code" />
		<%
		    String forma = "";
			if(pianta != null) {
			    try {
			        forma = FormaLocalServiceUtil.getForma(pianta.getIdForma()).getForma();
			    } catch (Exception ex) {
			        _log.debug(ex.getMessage());
			    }
			}
			String nome = "<b>" + pianta.getNome() + "</b></br>&Oslash; " + pianta.getVaso()
			    + " " + forma + "</br>H. " + pianta.getAltezzaPianta();
		%>
		<liferay-ui:search-container-column-text name="Nome" value="<%= nome %>"/>
		<%
		    String supplier = "";
            if (pianta.getIdFornitore() > 0){
                Fornitori fornitore = FornitoriLocalServiceUtil.fetchFornitori(pianta.getIdFornitore());
                if (fornitore != null) {
                    supplier = fornitore.getDitta();
                }
            }
        %>
        <liferay-ui:search-container-column-text name="supplier" value="<%=supplier %>"/>
        <liferay-ui:search-container-column-text name="Varianti" >
            <aui:select id="<%= "variants_" + pianta.getId() %>" label=""
                name="<%= "variants_" + pianta.getId() %>" cssClass="input-small">
                <%
                    List<VariantiPianta> plantVariants =
                        VariantiPiantaLocalServiceUtil.findByIdPianta(pianta.getId());
                    if (plantVariants.size() > 0) {
                        %>
                        <aui:option label="select-variant" value="-1" />
                        <%
                        for (VariantiPianta vp : plantVariants) {
                            Variante v = VarianteLocalServiceUtil.fetchVariante(vp.getIdVariante());
                            if (Validator.isNotNull(v)) {
                                %>
                                <aui:option label="<%=v.getVariante()%>" value="<%=v.getId()%>" />
                                <%
                            }
                        }
                    }
                %>
            </aui:select>
        </liferay-ui:search-container-column-text>
        <%
           String imageSrc = Constants.PICTURE_URL_PREFIX + "not-found.png";
           
           try {
               imageSrc = Utils.retrievePictureURL(pianta); 
           } catch (NumberFormatException ex) {
               _log.debug(ex.getMessage());
           } catch (PortalException ex) {
               _log.debug(ex.getMessage());
           } catch (SystemException ex) {
               _log.debug(ex.getMessage());
           }
       %>
       <liferay-ui:search-container-column-text name="picture" >
           <a href="<%=imageSrc %>" title="<%=pianta.getNome() %>" target="_blank">
               <img src="<%=imageSrc %>" class="small center-h" />
           </a>
       </liferay-ui:search-container-column-text>
        <c:if test="<%=!active%>">
			<liferay-ui:search-container-column-text name="active" >
	            <c:if test="<%=pianta.isAttiva()%>">
	                <liferay-ui:icon image="activate" />
	            </c:if>
	            <c:if test="<%=!pianta.isAttiva()%>">
	                <liferay-ui:icon image="deactivate" />
	            </c:if>
	       </liferay-ui:search-container-column-text>
       </c:if>
       <liferay-ui:search-container-row-parameter name="backUrl"
            value="<%= iteratorURL.toString() %>" />
       <liferay-ui:search-container-column-text name="platforms" colspan="2">
           <aui:button-row>
	           <aui:input name="<%="platformsQuant_" + pianta.getId()%>" label=""
	               cssClass="custom-input-small" value="1" inlineField="true" />
	           <aui:button name="add-platforms" onClick="addPlatform(${pianta.id})"
                   value="add-platforms" />
           </aui:button-row>
       </liferay-ui:search-container-column-text>
       <liferay-ui:search-container-column-text name="carts" colspan="2">
           <aui:button-row>
               <aui:input name="<%= "cartsQuant_" + pianta.getId()%>" label=""
                   cssClass="custom-input-small" value="1" inlineField="true" />
               <aui:button name="add-carts" onClick="addPlatform(${pianta.id})"
                   value="add-carts" />
           </aui:button-row>
       </liferay-ui:search-container-column-text>
	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
		paginate="true" />
</liferay-ui:search-container>  --%>
<aui:input name="orderJSON"label="" type="hidden"
        value="<%=orderJSON.toString() %>"/>
            
<aui:script>
var addQuantUrl = '<%=addQuantityURL %>';
var addPlatformUrl = '<%=addPlatformURL %>';
var addCartUrl = '<%=addCartURL %>';
var editVarUrl = '<%=editVarUrl %>';
var modal;
AUI().use('autocomplete-list', 'aui-base', 'aui-io-request',
        'autocomplete-filters', 'autocomplete-highlighters', function(A) {
	var testData;
    var itemCode = new A.AutoCompleteList({
        allowBrowserAutocomplete : true,
        activateFirstItem : true,
        inputNode : '#<portlet:namespace />code',
        resultTextLocator : function (result) {
            return result.codice + ' --> ' + result.nome + ' Vaso: ' + result.vaso;
        },
        render : true,
        resultHighlighter : 'startsWith',
        resultFilters : [ 'startsWith', 'phraseMatch' ],
        source : function() {
        	var itemCode = A.one("#<portlet:namespace />code").get('value');
//         	var itemName = A.one("#<portlet:namespace />name").get('value');
        	var supplier = A.one("#<portlet:namespace />suppliers").get('value');
        	var category = A.one("#<portlet:namespace />categories").get('value');
        	var onlyActive = A.one("#<portlet:namespace />activeCheckbox").get('checked');
        	var onlyInactive = A.one("#<portlet:namespace />inactiveCheckbox").get('checked');
        	var onlyAvailable = A.one("#<portlet:namespace />availableCheckbox").get('checked');
            var myAjaxRequest = A.io.request('<%=getPlants%>', {
            	dataType : 'json',
                method : 'POST',
                data : {
                    <portlet:namespace />itemCode : itemCode,
//                     <portlet:namespace />itemName : itemName,
                    <portlet:namespace />supplier : supplier,
                    <portlet:namespace />category : category,
                    <portlet:namespace />onlyActive : onlyActive,
                    <portlet:namespace />onlyInactive : onlyInactive,
                    <portlet:namespace />onlyAvailable : onlyAvailable
                },
                autoLoad : false,
                sync : false,
                on : {
                    success : function() {
                        var data = this.get('responseData');
                        testData = data;
                    },
                    error: function() {
                    	alert('Si è verificato un errore durante la ricerca');
                    }
                }
            });
            myAjaxRequest.start();
            return testData;
        },
//         maxResults: 10,
        width: 400
    });
    itemCode.on('select', function(e) {
    	var selected_node = e.itemNode, selected_data = e.result;
    	var result = e.result.raw;
    	console.log(result);
    	updateFields(result);
    });
});

AUI().use('aui-base', function(A) {
    A.one('#<portlet:namespace/>code').on('click', function(){
        AUI().use('aui-base', function(A) {
              A.one('#<portlet:namespace/>code').val('');
        });
    });
});

AUI().use('aui-base', function(A) {
    A.one('#<portlet:namespace/>itemPrice1').on('blur', function(){
        AUI().use('aui-base', function(A) {
              var p1 = A.one('#<portlet:namespace/>itemPrice1').val();
              var p2 = A.one('#<portlet:namespace/>itemPrice2').val();
              A.one('#<portlet:namespace/>variantPrice').val(currencyFormatter(p2 - p1));
              if (A.one('#<portlet:namespace/>variantPrice').get('value') !== ''
      		 	&& parseFloat(A.one('#<portlet:namespace/>variantPrice').get('value')) > 0) {
    		  A.one('#<portlet:namespace/>supplierVariant').val(currencyFormatter(
    					A.one('#<portlet:namespace/>variantPrice').get('value')));
		   	  } else {
		   		  A.one('#<portlet:namespace/>supplierVariant').val(0);
		   	  }
        });
    });
});

AUI().use('aui-base', function(A) {
    A.one('#<portlet:namespace/>itemPrice2').on('blur', function(){
        AUI().use('aui-base', function(A) {
              var p1 = A.one('#<portlet:namespace/>itemPrice1').val();
              var p2 = A.one('#<portlet:namespace/>itemPrice2').val();
              A.one('#<portlet:namespace/>variantPrice').val(currencyFormatter(p2 - p1));
              if (A.one('#<portlet:namespace/>variantPrice').get('value') !== ''
            		 	&& parseFloat(A.one('#<portlet:namespace/>variantPrice').get('value')) > 0) {
          		  A.one('#<portlet:namespace/>supplierVariant').val(currencyFormatter(
          					A.one('#<portlet:namespace/>variantPrice').get('value')));
         	  } else {
         		  A.one('#<portlet:namespace/>supplierVariant').val(0);
         	  }
        });
    });
});

AUI().use('aui-base', function(A) {
    A.one('#<portlet:namespace/>code').on('click', function(){
        AUI().use('aui-base', function(A) {
              A.one('#<portlet:namespace/>itemId').val('');
        });
    });
});

AUI().use('aui-base', function(A) {
    A.one('#<portlet:namespace/>addQuantBTN').on('click', function(){
        AUI().use('aui-base', function(A) {
              var id = A.one('#<portlet:namespace/>itemId').val();
              addQuant(id);
        });
    });
});

AUI().use('aui-base', function(A) {
	A.one('#<portlet:namespace/>addPlatBTN').on('click', function(){
		AUI().use('aui-base', function(A) {
			  var id = A.one('#<portlet:namespace/>itemId').val();
			  addPlatform(id);
		});
	});
});

AUI().use('aui-base', function(A) {
    A.one('#<portlet:namespace/>addCartBTN').on('click', function(){
        AUI().use('aui-base', function(A) {
              var id = A.one('#<portlet:namespace/>itemId').val();
              addCart(id);
        });
    });
});

AUI().use('aui-base', 'aui-io-request', 'aui-modal', function(A) {
	A.one('#searchBtn').on('click', function() {
	    Liferay.Util.openWindow({
	        dialog : {
	            centered : true,
	            modal : true
	        },
	        id : '<portlet:namespace/>itemDialog',
	        title : 'Cerca Prodotto',
	        uri : '<%=itemURL%>'
	    });
	});
	Liferay.provide(window,'<portlet:namespace/>closePopup',
		function(data, dialogId) {
			var A = AUI();
			updateVariant(data);
			var dialog = Liferay.Util.Window.getById(dialogId);
			dialog.destroy();
		},
		['liferay-util-window']
	);
// 	Liferay.provide(window, 'closePopup', function(data, dialogId) {
	    
// 	    console.log(data);
// 	    var dialog = Liferay.Util.Window.getById(dialogId);
// 	    dialog.hide();
// 	    console.log(data);
// 	    switch (dialogId) {
	       
// 	    }
// 	});
});

AUI().use('aui-base', function(A) {
    A.one("#<portlet:namespace/>enableSupplierPriceCheckbox").on('click', function(e) {
        if (A.one('#<portlet:namespace/>enableSupplierPrice').get('value') === 'true') {
            A.one('#<portlet:namespace/>supplierPrice').removeAttribute('disabled');
            A.one('#<portlet:namespace/>supplierPrice').removeClass('disabled');
        } else {
            A.one('#<portlet:namespace/>supplierPrice').setAttribute('disabled', 'true');
            A.one('#<portlet:namespace/>supplierPrice').addClass('disabled');
        }
    });
});

AUI().use('aui-base', function(A) {
    A.one("#<portlet:namespace/>itemVariants").on('change', function(e) {
    	if (e.target._node.value !== '') {
    		A.one('#<portlet:namespace/>supplierVariant').val(currencyFormatter(
    				A.one('#<portlet:namespace/>variantPrice').get('value')));
    	} else {
    		A.one('#<portlet:namespace/>supplierVariant').val(0);
    	}
    });
});

function updateFields (result) {
	AUI().use('aui-base', function(A) {
	    A.one('#<portlet:namespace/>itemId').val(result.id);
	    A.one('#<portlet:namespace/>itemCode').val(result.codice);
	    A.one('#<portlet:namespace/>itemName').val(result.nome);
	    A.one('#<portlet:namespace/>itemHeight').val(result.altezzaPianta);
	    A.one('#<portlet:namespace/>platformHeight').val(result.altezza);
	    A.one('#<portlet:namespace/>itemVase').val(result.vaso);
	    A.one('#<portlet:namespace/>itemShapes').val(result.dettagliPianta.categoriaMerceologica);
	    A.one('#<portlet:namespace/>itemPrice1').val(currencyFormatter(result.dettagliPianta.prezzo1));
	    A.one('#<portlet:namespace/>itemPrice2').val(currencyFormatter(result.dettagliPianta.prezzo2));
	    A.one('#<portlet:namespace/>itemPrice3').val(currencyFormatter(result.dettagliPianta.prezzo3));
	    A.one('#<portlet:namespace/>supplierPrice').val(currencyFormatter(result.prezzoFornitore));
	    A.one('#<portlet:namespace/>variantPrice').val(currencyFormatter(result.dettagliPianta.prezzo2 - result.dettagliPianta.prezzo1));
	    A.one('#<portlet:namespace/>itemAvailability').val(result.disponibilita);
	    A.one('#<portlet:namespace/>itemPerPlat').val(result.piantePianale);
	    A.one('#<portlet:namespace/>item2Over').val(result.sormonto2Fila);
	    A.one('#<portlet:namespace/>item3Over').val(result.sormonto3Fila);
	    A.one('#<portlet:namespace/>item4Over').val(result.sormonto4Fila);
	    A.one('#<portlet:namespace/>itemPerCart').val(result.pianteCarrello);
	    var options = '<option value="" selected>Seleziona variante...</option>';
	    for (var i = 0; i < result.varianti.length; i++) {
	        var variant = result.varianti[i];
	        options += '<option value="' + variant.codiceVariante + '">' + variant.descrizione + '</option>';
	    }
	    A.one('#<portlet:namespace/>itemVariants').setHTML(options);
	    A.one('#<portlet:namespace/>itemVariants').set('value', '');
	    A.one('#<portlet:namespace/>itemQuantity').set('value', 0);
	    A.one('#<portlet:namespace/>platQuantity').set('value', 1);
	    A.one('#<portlet:namespace/>cartQuantity').set('value', 1);
	    A.one('#<portlet:namespace/>supplierPrice').setAttribute('disabled', 'true');
        A.one('#<portlet:namespace/>supplierPrice').addClass('disabled');
        A.one('#<portlet:namespace/>enableSupplierPrice').set('value', false);
        A.one('#<portlet:namespace/>enableSupplierPriceCheckbox').set('checked', false); 
	});
}

</aui:script>