<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.liferay.portal.kernel.json.JSONArray"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.json.JSONObject"%>
<%@page import="it.bysoftware.ct.model.Riga"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.service.CarrelloLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Carrello"%>
<%@page import="java.util.List"%>
<%@page import="it.bysoftware.ct.service.OrdineLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<%@page import="it.bysoftware.ct.utils.ResourceId"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../init.jsp"%>

<%
//     boolean printBarcode = ParamUtil.getBoolean(renderRequest, "printBarcode", false);

    List<Carrello> carts = new ArrayList<Carrello>();
    List<Riga> rows = new ArrayList<Riga>();
//     if (orderJSON.length() == 0) {
// //         _log.info("[order.jsp] orderJSON: " + orderJSON);
//     } else {
// //         orderJSON = JSONFactoryUtil.createJSONObject(tmp);
//     }
    int index = 0;
    
    DecimalFormat df = new DecimalFormat("€0.00");
    int height = GetterUtil.getInteger(portletPreferences.getValue(Constants.
            CART_DEFAULT_HEIGHT, "260"));
    double labelPrintCost = GetterUtil.getDouble(portletPreferences.getValue(
    		Constants.ADD_PRINT_LABEL, "0.1"));
%>
<liferay-portlet:resourceURL var="removePlatformUrl"
    id="<%=ResourceId.removePlatform.name()%>" />
<liferay-portlet:resourceURL var="deleteCartUrl"
    id="<%=ResourceId.deleteCart.name()%>"/>
<liferay-portlet:resourceURL var="updatePlatItemsUrl"
    id="<%=ResourceId.updatePlatItems.name()%>"/>
<liferay-portlet:resourceURL var="lockUnlockCartUrl"
    id="<%=ResourceId.lockCart.name()%>" />
<liferay-portlet:resourceURL var="loadItemUrl"
    id="<%=ResourceId.loadItem.name()%>"/>
<span id="group-error-block" ></span>
<span id="group-success-block" ></span>
<liferay-ui:panel-container cssClass="hidden cart-container" id="ciao">
    <liferay-ui:panel collapsible="true" cssClass=""
        id="cart_{0}" title="Carrello {0}">
        <aui:nav-bar>
            <aui:nav>
                <aui:nav-item id="deleteCart_{0}"
                    iconCssClass="icon-trash" label="delete" cssClass="delete-cart"
                    selected='<%="delete".equals(toolbarItem)%>' href="#" />
                <aui:nav-item id="lockCart_{0}"
                    iconCssClass="icon-unlock" label="lock" cssClass="lock-unlock-cart"
                    selected='<%="lock".equals(toolbarItem)%>'
                    href="#" />
            </aui:nav>
        </aui:nav-bar>
        <div id="myDataTable_{0}"></div>
        <div id="myProgressBar_{0}"></div>
        <aui:button-row>
	        <aui:input id="cartTotal_{0}" name="cartTotal_{0}" label="cart-total" inlineField="true"
	            cssClass="input-small" value="{1}" prefix="€"/>
	        <aui:input id="cartPlantTotal_{0}" name="cartPlantTotal_{0}" label="cart-plants-total"
	            cssClass="input-small" inlineField="true" value="{2}" prefix="#"/>
        </aui:button-row>
    </liferay-ui:panel>
</liferay-ui:panel-container>

<aui:input id="printBarcode" type="checkbox" name="printBarcode"
    label="print-barcode" inlineLabel="true" checked="<%= orderJSON.getBoolean("stampaEtichette")%>"/>
<%-- <aui:layout > --%>
<%--     <aui:column columnWidth="70" first="true"  > --%>
    <c:if test="<%=orderJSON.has(Constants.CARTS) %>">
        <c:forEach begin="0" end="<%=orderJSON.getJSONArray(Constants.CARTS).length() - 1%>" var="i">
<%
    JSONObject cartJSON = orderJSON.getJSONArray(Constants.CARTS).getJSONObject(index);
    index++; 
%>
<liferay-ui:panel-container cssClass="cart-container smaller-font">
    <liferay-ui:panel collapsible="true" cssClass=""
        id="<%="cart_" + cartJSON.getInt("id")%>" title="<%="Carrello " + cartJSON.getInt("id")%>">
                    <aui:nav-bar>
            <aui:nav>
                <aui:nav-item id="<%="deleteCart_" + cartJSON.getInt("id")%>"
                    iconCssClass="icon-trash" label="delete" cssClass="delete-cart"
                    selected='<%="delete".equals(toolbarItem)%>' href="#"/>
                <c:choose>
	                <c:when test="<%= !cartJSON.getBoolean(Constants.CLOSED)%>" >
		                <aui:nav-item id="<%="lockCart_" + cartJSON.getInt("id")%>"
		                    iconCssClass="icon-unlock" label="lock" cssClass="lock-unlock-cart"
		                    selected='<%="lock".equals(toolbarItem)%>'
		                    href="#" />
                    </c:when>
                    <c:otherwise>
                        <aui:nav-item id="<%="lockCart_" + cartJSON.getInt("id")%>"
                            iconCssClass="icon-lock" label="unlock" cssClass="lock-unlock-cart"
                            selected='<%="lock".equals(toolbarItem)%>'
                            href="#" />
                    </c:otherwise>
               </c:choose>
            </aui:nav>
        </aui:nav-bar>
        <div id="<%="myDataTable_" + cartJSON.getInt("id")%>"></div>
        <div id="<%="myProgressBar_" + cartJSON.getInt("id")%>"></div>
                    <aui:input id="<%="cartTotal_" + cartJSON.getInt("id") %>"
                        name="<%="cartTotal_" + cartJSON.getInt("id") %>"
                        label="cart-total" prefix="€" cssClass="input-small"  inlineField="true"
                        value="<%=df.format(cartJSON.getDouble("importo")) %>" />
                     <aui:input id="<%="cartPlantTotal_" + cartJSON.getInt("id")%>"
                        name="<%="cartPlantTotal_" + cartJSON.getInt("id")%>" label="cart-plants-total"
                        cssClass="input-small" inlineField="true"
                        value="<%=cartJSON.getInt("plants") %>" prefix="#"/>
                </liferay-ui:panel>
            </liferay-ui:panel-container>
        </c:forEach>
    </c:if>
<div id="contentHolder" class="smaller-font"></div>
<%!private static Log _log = LogFactoryUtil.getLog("docroot.html.ordini.order_jsp");%>
<%--     </aui:column> --%>
<%-- 	<aui:column columnWidth="30"> --%>
<%--     </aui:column> --%>
<%-- </aui:layout> --%>
<aui:a href="#main-content" cssClass="top-btn btn icon-chevron-up" title="top" />
<aui:script>
	var portletNamespace = '<portlet:namespace />';
	var defPrintLabelCost = <%= labelPrintCost %>;
	var defCartHeight = <%= height %>;
	var customerCode = <%=customerCode %>
	var orderJSON = <%=orderJSON %>
	var removePlatformUrl = '<%=removePlatformUrl %>';
	var deleteCartUrl = '<%=deleteCartUrl %>';
	var updatePlatItemsUrl = '<%=updatePlatItemsUrl%>';
	var lockUnlockCartUrl = '<%=lockUnlockCartUrl %>';
	var loadItemUrl = '<%=loadItemUrl%>';

	AUI().ready(function(A) {
		drawProgressBars();
		drawTables();
		
		AUI().use('aui-base', function(A){
			A.all(".delete-cart").on('click', function(e){
				var tmp = e._currentTarget.id.split("_");
				var r = confirm('Confermi l\'eliminazione del carrello: ' + tmp[tmp.length - 1]);
				if (r == true) {
	                deleteCart(tmp[tmp.length - 1]);
				}
			});
		});
		
		AUI().use('aui-base', function(A){
            A.all(".lock-unlock-cart").on('click', function(e){
                var tmp = e._currentTarget.id.split("_");
//                 var r = confirm('Confermi l\'eliminazione del carrello: ' + tmp[tmp.length - 1]);
//                 if (r == true) {
                lockUnlockCart(tmp[tmp.length - 1]);
//                 }
            });
        });
		
		AUI().use('aui-base', function(A){
            A.one("#<portlet:namespace/>printBarcodeCheckbox").on('click', function(e){
            	var test = A.one('#<portlet:namespace/>printBarcode').get('value');
            	if(test === "true"){
            		console.log("A");
            		addPrintLabelCost(defPrintLabelCost, true);
            	} else {
            		console.log("B");
            		addPrintLabelCost(-1 * defPrintLabelCost, false);
            	}
            });
        });
	});
</aui:script>