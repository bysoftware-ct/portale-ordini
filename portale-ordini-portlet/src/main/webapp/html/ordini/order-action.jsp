<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@include file="../init.jsp"%>
<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    AnagraficheClientiFornitori customer = (AnagraficheClientiFornitori) row
            .getObject();
    String backURL = (String) row.getParameter("backURL");
%>

<liferay-ui:icon-menu>
	<liferay-portlet:renderURL varImpl="editOrderURL">
		<portlet:param name="mvcPath" value="/html/ordini/edit-order.jsp" />
		<portlet:param name="backURL" value="<%=backURL%>" />
		<portlet:param name="customerId"
			value="<%=String.valueOf(customer.getCodiceAnagrafica())%>" />
	</liferay-portlet:renderURL>
	<liferay-ui:icon image="add" url="${editOrderURL}" message="new-order" />
	<liferay-portlet:renderURL varImpl="viewOrderURL">
        <portlet:param name="mvcPath" value="/html/ordini/view-orders.jsp" />
        <portlet:param name="backURL" value="<%=backURL%>" />
        <portlet:param name="customerId"
            value="<%=customer.getCodiceAnagrafica()%>" />
    </liferay-portlet:renderURL>
    <liferay-ui:icon image="search" url="${viewOrderURL}" message="view" />
</liferay-ui:icon-menu>