<%@page import="it.bysoftware.ct.utils.OrderState"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="it.bysoftware.ct.service.OrdineLocalServiceUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<%@page import="java.util.List"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../init.jsp"%>
<%
List<Ordine> pendingOrders = new ArrayList<Ordine>();
if (Validator.isNotNull(customerCode) && !customerCode.isEmpty()) {
    pendingOrders.addAll(OrdineLocalServiceUtil.getCustomerPendingOrders(customerCode));
    pendingOrders.addAll(OrdineLocalServiceUtil.getSavedCustomerOrders(customerCode));
}
%>

<liferay-portlet:renderURL varImpl="iteratorURL1">
	<portlet:param name="mvcPath" value="/html/ordini/view-order.jsp" />
</liferay-portlet:renderURL>

<liferay-ui:search-container delta="20"
	emptyResultsMessage="no-orders" iteratorURL="<%=iteratorURL1%>">
	<liferay-ui:search-container-results>
		<%
            results = ListUtil.subList(pendingOrders,
                                    searchContainer.getStart(),
                                    searchContainer.getEnd());
                            total = pendingOrders.size();
                            pageContext.setAttribute("results", results);
                            pageContext.setAttribute("total", total);
        %>
	</liferay-ui:search-container-results>
	<liferay-ui:search-container-row
		className="it.bysoftware.ct.model.Ordine" modelVar="order">
		<%
        sdf.applyLocalizedPattern("dd/MM/yyyy");
        String orderDate = sdf.format(order.getDataInserimento());
        String shipDate = sdf.format(order.getDataConsegna());
        %>
		<liferay-ui:search-container-column-text name="confirm-number"
			value="<%= order.getCentro() + "/" + order.getNumero() %>" />
		<liferay-ui:search-container-column-text value="<%= orderDate %>"
			name="date" />
		<liferay-ui:search-container-column-text value="<%= shipDate %>"
			name="ship-date" />
		<liferay-ui:search-container-column-text
			value="<%=fmt.format(order.getImporto()) %>" name="amount" />
		<liferay-ui:search-container-column-text name="status">
			<aui:workflow-status showIcon="<%= false %>" showLabel="<%= false %>"
				status="<%= order.getStato()%>" />
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-jsp
			path="/html/ordini/order-view/view-orders-action.jsp" />
		<liferay-ui:search-container-row-parameter name="backUrl"
			value="<%= iteratorURL1.toString() %>" />
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
		paginate="true" />
</liferay-ui:search-container>