<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="it.bysoftware.ct.utils.OrderState"%>
<%@page import="it.bysoftware.ct.model.Ordine"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@include file="../../init.jsp"%>
<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    Ordine order = (Ordine) row.getObject();
    String backURL = (String) row.getParameter("backURL");//ParamUtil.getString(renderRequest, "backURL");
%>

<liferay-ui:icon-menu>
    <liferay-portlet:renderURL varImpl="editOrderURL">
		<portlet:param name="mvcPath" value="/html/ordini/edit-order.jsp" />
		<portlet:param name="backURL" value="<%=backURL%>" />
		<portlet:param name="orderId" value="<%=String.valueOf(order.getId())%>" />
        <portlet:param name="customerId" value="<%=order.getIdCliente()%>" />
        <portlet:param name="carriers" value="<%=order.getIdTrasportatore()%>" />
        <portlet:param name="centers" value="<%=order.getCentro()%>" />
        <portlet:param name="vats" value="<%=order.getCodiceIva()%>" />
        <portlet:param name="cartsType" value="<%=String.valueOf(order.getIdTipoCarrello())%>" />
        <portlet:param name="orderDate" value="<%=String.valueOf(order.getDataInserimento().getTime())%>" />
        <portlet:param name="shipDate" value="<%=String.valueOf(order.getDataConsegna().getTime())%>" />
        <portlet:param name="loadPlace" value="<%=String.valueOf(order.getLuogoCarico())%>" />
        <portlet:param name="view" value="false" />
        <c:if test="<%= order.getDataOrdineGT() != null %>">
        	<portlet:param name="GTorderDate" value="<%=String.valueOf(order.getDataOrdineGT().getTime())%>" />
        </c:if>
        <c:if test="<%= order.getNumOrdineGT() != null || !order.getNumOrdineGT().isEmpty() %>">
        	<portlet:param name="GTorderNum" value="<%=order.getNumOrdineGT()%>" />
        </c:if>
	</liferay-portlet:renderURL>
	<liferay-ui:icon image="edit" url="${editOrderURL}" />
	<liferay-portlet:renderURL varImpl="viewOrderURL">
        <portlet:param name="mvcPath" value="/html/ordini/edit-order.jsp" />
        <portlet:param name="backURL" value="<%=backURL%>" />
        <portlet:param name="orderId" value="<%=String.valueOf(order.getId())%>" />
        <portlet:param name="customerId" value="<%=order.getIdCliente()%>" />
        <portlet:param name="carriers" value="<%=order.getIdTrasportatore()%>" />
        <portlet:param name="centers" value="<%=order.getCentro()%>" />
        <portlet:param name="vats" value="<%=order.getCodiceIva()%>" />
        <portlet:param name="cartsType" value="<%=String.valueOf(order.getIdTipoCarrello())%>" />
        <portlet:param name="orderDate" value="<%=String.valueOf(order.getDataInserimento().getTime())%>" />
        <portlet:param name="shipDate" value="<%=String.valueOf(order.getDataConsegna().getTime())%>" />
        <portlet:param name="loadPlace" value="<%=String.valueOf(order.getLuogoCarico())%>" />
        <portlet:param name="view" value="true" />
		<c:if test="<%= order.getDataOrdineGT() != null %>">
        	<portlet:param name="GTorderDate" value="<%=String.valueOf(order.getDataOrdineGT().getTime())%>" />
        </c:if>
        <c:if test="<%= order.getNumOrdineGT() != null || !order.getNumOrdineGT().isEmpty() %>">
        	<portlet:param name="GTorderNum" value="<%=order.getNumOrdineGT()%>" />
        </c:if>
    </liferay-portlet:renderURL>
	<liferay-ui:icon image="view" url="${viewOrderURL}" />
	<c:if test="<%= order.getStato() >= OrderState.CONFIRMED.getValue() 
	        && order.getStato() < OrderState.SCHEDULED.getValue()%>">
       <c:if test="<%= order.getStato() == OrderState.INACTIVE.getValue()%>">
	      <liferay-portlet:actionURL name="unlockOrder" var="unlockOrderURL">
	          <portlet:param name="orderId" value="<%=String.valueOf(order.getId()) %>"/>
<%-- 	          <portlet:param name="currentURL" value="<%= backURL %>"/> --%>
	      </liferay-portlet:actionURL>
	      <liferay-ui:icon image="unlock" message="unlock-order" url="${unlockOrderURL}" />
	   </c:if>
	   <liferay-portlet:actionURL name="deleteOrder" var="deleteOrderURL">
	       <portlet:param name="orderId" value="<%=String.valueOf(order.getId()) %>"/>
<%-- 	       <portlet:param name="currentURL" value="<%= backURL %>"/> --%>
	   </liferay-portlet:actionURL>
	   <liferay-ui:icon-delete message="delete" url="${deleteOrderURL}" />
	</c:if>
	<c:if test="<%= order.getStato() == OrderState.CONFIRMED.getValue() 
            &&  order.getDataConsegna().getTime() == Utils.today().getTimeInMillis()%>">
       <liferay-portlet:actionURL name="exportOrder" var="exportOrderURL">
           <portlet:param name="orderId" value="<%=String.valueOf(order.getId()) %>"/>
<%--            <portlet:param name="currentURL" value="<%= backURL %>"/> --%>
       </liferay-portlet:actionURL>
       <liferay-ui:icon image="check" message="consolidate" url="${exportOrderURL}" />
    </c:if>
</liferay-ui:icon-menu>