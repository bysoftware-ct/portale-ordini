<%@page import="it.bysoftware.ct.utils.ResourceId"%>
<%@page import="it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK"%>
<%@page import="it.bysoftware.ct.service.StatiLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Stati"%>
<%@page import="it.bysoftware.ct.utils.SubjectComparator"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.ClientiFornitoriDatiAgg"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="com.liferay.portal.kernel.dao.search.DisplayTerms"%>
<%@page import="java.util.List"%>
<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8" />
<%@include file="../init.jsp"%>

<%
//     boolean active = ParamUtil.getBoolean(renderRequest, "active", true);
//     String name = ParamUtil.getString(renderRequest, "name", "");
//     List<AnagraficheClientiFornitori> customers =
//             AnagraficheClientiFornitoriLocalServiceUtil.findCustomers();
    
    String currentURL = PortalUtil.getCurrentURL(request);
    String customerCode = ParamUtil.getString(request, "customerCode");
    String businessName = ParamUtil.getString(request, "businessName");
    String vatCode = ParamUtil.getString(request, "vatCode");
    String country = ParamUtil.getString(request, "country");
    String city = ParamUtil.getString(request, "city");
    boolean search = ParamUtil.getBoolean(request, "search", false);
//     int studentAge = ParamUtil.getInteger(request, "studentAge");
//     int studentGender = ParamUtil.getInteger(request, "studentGender");
//     String studentAddress = ParamUtil.getString(request, "studentAddress");
    
%>
<c:if test="<%=!search %>">
    <liferay-ui:header title="new-order" />
</c:if>
<portlet:resourceURL var="findCustomerUrl" 
        id="<%=ResourceId.findCustomer.name() %>" />
<liferay-portlet:renderURL varImpl="iteratorURL">
    <portlet:param name="mvcPath" value="/html/ordini/new-order.jsp" />
<%--     <portlet:param name="mvcPath" value="/html/ordini/view.jsp" /> --%>
</liferay-portlet:renderURL>
<liferay-portlet:renderURL varImpl="customerSearchURL">
    <portlet:param name="mvcPath" value="/html/ordini/new-order.jsp" />
<%--     <portlet:param name="mvcPath" value="/html/ordini/view.jsp" /> --%>
</liferay-portlet:renderURL>
<aui:form action="<%=customerSearchURL %>" method="get" name="customerForm">
    <liferay-portlet:renderURLParams varImpl="customerSearchURL" />
    <liferay-portlet:renderURL varImpl="iteratorURL1">
        <portlet:param name="customerCode" value="<%= customerCode %>" />
        <portlet:param name="businessName" value="<%= businessName %>" />
        <portlet:param name="vatCode" value="<%= vatCode %>" />
        <portlet:param name="country" value="<%= country %>" />
        <portlet:param name="city" value="<%= city %>" />
        <portlet:param name="search" value="<%= String.valueOf(search) %>" />
        <portlet:param name="mvcPath" value="/html/ordini/new-order.jsp" />
<%--         <portlet:param name="mvcPath" value="/html/ordini/view.jsp" /> --%>
    </liferay-portlet:renderURL>
    <liferay-ui:search-container displayTerms="<%= new DisplayTerms(renderRequest) %>"
        emptyResultsMessage="no-customers" headerNames="code,company-name,city,country,vat-code"
        iteratorURL="<%= iteratorURL1 %>">
        <liferay-ui:search-form page="/html/ordini/customer-search.jsp"
            servletContext="<%= application %>"/>
        <liferay-ui:search-container-results>
            <%
            DisplayTerms displayTerms = searchContainer.getDisplayTerms();
            List<AnagraficheClientiFornitori> subjects = new ArrayList<AnagraficheClientiFornitori>();
            if (displayTerms.isAdvancedSearch()) {
                subjects = AnagraficheClientiFornitoriLocalServiceUtil.
                        getCustomers(customerCode, businessName, vatCode, country, city, displayTerms.isAndOperator(),
                                0, AnagraficheClientiFornitoriLocalServiceUtil.findCustomers().size(),
                                new SubjectComparator());
            } else { 
                String searchkeywords = displayTerms.getKeywords();
                String numbesearchkeywords = String.valueOf(0);
                if (Validator.isNumber(searchkeywords)) {
                    numbesearchkeywords = searchkeywords;
                }
                subjects = AnagraficheClientiFornitoriLocalServiceUtil.
                        getCustomers(searchkeywords, searchkeywords, searchkeywords, searchkeywords, searchkeywords, false,
                                0, AnagraficheClientiFornitoriLocalServiceUtil.findCustomers().size(),
                                new SubjectComparator());
            }
            
            List<AnagraficheClientiFornitori> customers = new ArrayList<AnagraficheClientiFornitori>();
            for (AnagraficheClientiFornitori s : subjects) {
                ClientiFornitoriDatiAgg tmp = ClientiFornitoriDatiAggLocalServiceUtil.fetchClientiFornitoriDatiAgg(new ClientiFornitoriDatiAggPK(s.getCodiceAnagrafica(), false));
                if (tmp != null) {
                    customers.add(s);
                }
            }
//             searchContainer.setTotal(customers.size());
//             searchContainer.setResults(customers);
            results = ListUtil.subList(customers,
                                    searchContainer.getStart(),
                                    searchContainer.getEnd());
                            total = customers.size();
                            pageContext.setAttribute("results", results);
                            pageContext.setAttribute("total", total);
            %>
        </liferay-ui:search-container-results>
        <liferay-ui:search-container-row className="it.bysoftware.ct.model.AnagraficheClientiFornitori"
            keyProperty="codiceAnagrafica" modelVar="customer">
            <liferay-portlet:renderURL varImpl="rowURL">
                <portlet:param name="backURL" value="<%= currentURL %>" />
                <portlet:param name="redirect" value="<%= currentURL %>" />
                <portlet:param name="customerCode" value="<%= customer.getCodiceAnagrafica() %>" />
            </liferay-portlet:renderURL>
            <liferay-ui:search-container-column-text property="codiceAnagrafica" name="code" />
            <liferay-ui:search-container-column-text name="company-name"
                value="<%=customer.getRagioneSociale1() + " " + customer.getRagioneSociale2() %>"/>
            <liferay-ui:search-container-column-text property="comune" name="city" />
            <%
			     Stati s = StatiLocalServiceUtil.fetchStati(customer.getSiglaStato());
			     String coutry = "";
			     if (s != null){
			         coutry = s.getStato(); 
			     }
			%>  
           <liferay-ui:search-container-column-text value="<%=coutry %>" name="country" />
           <liferay-ui:search-container-column-text property="partitaIVA" name="vat-code" />
           <liferay-ui:search-container-row-parameter name="backURL"
               value="<%= currentURL %>" />
           <c:choose>
               <c:when test="<%=!search %>">
                   <liferay-ui:search-container-column-jsp path="/html/ordini/order-action.jsp" />
               </c:when>
               <c:otherwise>
                   <liferay-ui:search-container-column-button name="select"
                        href="set('${customer.codiceAnagrafica}')" />
               </c:otherwise>
           </c:choose>
           <c:if test="">
           </c:if>
        </liferay-ui:search-container-row>
        <liferay-ui:search-iterator searchContainer="<%=searchContainer %>" />
    </liferay-ui:search-container>
</aui:form>


<%!private static Log _log = LogFactoryUtil
            .getLog("docroot.html.ordini.view_jsp");%>

<aui:script>
function set(customerCode) {
	var modal;
	var opener = Liferay.Util.getOpener();
	AUI().use('aui-base', 'aui-io-request', 'aui-modal', function(A) {
        A.io.request('<%=findCustomerUrl %>', {
            method: 'POST',
            data: {
            	customerCode: customerCode,
            },
            on: {
                start: function() {
                    modal = new A.Modal({
                        bodyContent : '<div class="loading-animation"/>',
                        centered : true,
                        headerContent : '<h3>Loading...</h3>',
                        modal : true,
                        render : '#modal',
                        close: false,
                        zIndex: 99999,
                        width : 450
                    }).render();
                },
                success: function () {
                    var data = JSON.parse(this.get('responseData'));
                    console.log(data);
                    modal.hide();
                    if (data.code !== 0) {
                        alert('Si e\' verificato un errore durante la modifica del'
                                + ' cliente.\n\n' + data.message);
                    }
                    opener.closePopup(data, '<portlet:namespace/>cutomerDialog');
                    
                },
                error: function () {
                    modal.hide();
                }
            }
        });
    });
}
</aui:script>