<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../../init.jsp"%>

<liferay-portlet:actionURL portletConfiguration="true"
    var="generateLinkURL" />
<liferay-ui:header title="generate-pics-link" />

<aui:form action="<%=generateLinkURL%>" method="post" name="fm">
	<aui:button type="submit" icon="configuration" value="generate" />
</aui:form>