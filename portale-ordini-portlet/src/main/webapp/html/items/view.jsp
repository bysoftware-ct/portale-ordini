<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="it.bysoftware.ct.utils.ResourceId"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="it.bysoftware.ct.service.ArticoliLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Articoli"%>
<%@page import="it.bysoftware.ct.service.CategorieMerceologicheLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.CategorieMerceologiche"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portal.model.Role"%>
<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.liferay.portlet.documentlibrary.util.DLUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.util.ImageProcessorUtil"%>
<%@page import="java.io.File"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="com.liferay.portal.kernel.exception.SystemException"%>
<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.repository.model.FileEntry"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowConstants"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="it.bysoftware.ct.model.Categorie"%>
<%@page import="it.bysoftware.ct.model.Piante"%>
<%@page import="it.bysoftware.ct.model.Variante"%>
<%@page import="it.bysoftware.ct.model.VariantiPianta"%>
<%@page import="it.bysoftware.ct.service.CategorieLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.PianteLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.VarianteLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.VariantiPiantaLocalServiceUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@include file="../init.jsp"%>

<%
    String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
    boolean search = ParamUtil.getBoolean(renderRequest, "search", false);    
    boolean disableSuppliers = ParamUtil.getBoolean(renderRequest, "disableSuppliers", false);
    boolean signedIn = themeDisplay.isSignedIn();
    boolean active = ParamUtil.getBoolean(renderRequest, "active", true);
    boolean inactive = ParamUtil.getBoolean(renderRequest, "inactive", false);
    boolean obsolete = ParamUtil.getBoolean(renderRequest, "obsolete", false);
    boolean available = ParamUtil.getBoolean(renderRequest, "available", false);
    long category = ParamUtil.getLong(renderRequest, "categories", -1);
    String supplierId = ParamUtil.getString(renderRequest, "suppliers", "");
    String vase = ParamUtil.getString(renderRequest, "vase", "");
    int height = ParamUtil.getInteger(renderRequest, "height", 0);
    String shapeSelected = ParamUtil.getString(renderRequest, "shapes", "");
    String code = ParamUtil.getString(renderRequest, "code", "");
    String name = ParamUtil.getString(renderRequest, "name", "");
    
    List<AnagraficheClientiFornitori> suppliers =
            AnagraficheClientiFornitoriLocalServiceUtil.findSuppliers();
    List<Categorie> categories = CategorieLocalServiceUtil.findAll();
    List<Piante> tmp = new ArrayList<Piante>();
    if (category != -1) {
	    for(Categorie c : categories) {
	        if(category > 0){
		        if(category == c.getId()){
		            tmp = PianteLocalServiceUtil.findPlants(category, active, inactive,
		                    supplierId, name, code, obsolete);
		        }
	        } else if(c.getPredefinita()){
	            tmp = PianteLocalServiceUtil.findPlants(c.getId(), active, inactive,
	                    supplierId, name, code, obsolete);
	        }
	    }
    } else {
        tmp = PianteLocalServiceUtil.findPlants(active, inactive, supplierId, name, code, obsolete);
    }
    
    List<Piante> piante = new ArrayList<Piante>();
    for (Piante p : tmp) {
        boolean add = true;
        Articoli i;
        try {
            i = ArticoliLocalServiceUtil.fetchArticoli(p.getCodice());
            if (i != null && !shapeSelected.isEmpty() && !i.getCategoriaMerceologica().
                    equals(shapeSelected)) {
                add = false;
            }
        } catch (SystemException e) {
            add = false;
        }
        
        if (available) {
            add = p.getDisponibilita() > 0;
        }
        
        if (!vase.isEmpty() && !p.getVaso().equals(vase)) {
            add = false;
        }
        
        if (height > 0 && p.getAltezzaPianta() != height) {
            add = false;
        }
        
        if (add) {
            piante.add(p);
        }
    }
    
    List<CategorieMerceologiche> shapes = CategorieMerceologicheLocalServiceUtil.
            getCategorieMerceologiches(0, CategorieMerceologicheLocalServiceUtil.getCategorieMerceologichesCount());
    
    boolean isCompany = false;
    List<Role> roles = user.getRoles();
    for (Role role : roles) {
        if (role.getName().equals(Constants.COMPANY)) {
            isCompany = true;
            break;
        }
    }

    NumberFormat nf = new DecimalFormat("€0.00", new DecimalFormatSymbols(renderRequest.getLocale()));
%>
<liferay-portlet:renderURL varImpl="searchURL">
    <portlet:param name="search" value="<%= String.valueOf(search) %>"/>
    <portlet:param name="mvcPath" value="/html/items/view.jsp" />
</liferay-portlet:renderURL>
<c:if test="<%=signedIn && isCompany && !search%>">
    <liferay-ui:header title="items-management" />
    <liferay-portlet:renderURL varImpl="newItemURL">
    	<portlet:param name="title" value="new-item" />
        <portlet:param name="backUrl" value="<%= searchURL.toString() %>" />
        <portlet:param name="mvcPath" value="/html/items/edit-picture.jsp" />
    </liferay-portlet:renderURL>
    <aui:nav-bar>
        <aui:nav>
            <aui:nav-item id="addBtn" iconCssClass="icon-plus" label="new"
               selected='<%="add".equals(toolbarItem)%>' href="<%= newItemURL.toString()%>"/>
            <aui:nav-item id="printBtn" iconCssClass="icon-print" label="Excel"
               selected='<%="print".equals(toolbarItem)%>' />
            <aui:nav-item id="downloadBtn" iconCssClass="icon-download" label="Pdf"
               selected='<%="download".equals(toolbarItem)%>' />
            <aui:nav-item id="downloadBtn1" iconCssClass="icon-download" label="Pdf (solo link)"
               selected='<%="download1".equals(toolbarItem)%>' />
        </aui:nav>
    </aui:nav-bar>
</c:if>
<aui:form action="<%=searchURL%>" method="get" name="fm">
    <liferay-portlet:renderURLParams varImpl="searchURL" />
    <div class="search-form">
        <span class="aui-search-bar" title="search-entries"> 
            <aui:layout>
                <aui:column first="true" columnWidth="25">
                    <aui:select id="categories" name="categories">
                        <aui:option label="all" value="-1" selected="<%= category == -1%>" />
                        <%
                            if (Validator.isNotNull(categories)) {
                                for (Categorie c : categories) {
                                    boolean selected = false;
                                    if (category > 0) {
                                        selected = c.getId() == category;
//                                     } else {
//                                         selected = c.getPredefinita();
                                    }
			                        %>
			                            <aui:option label="<%=c.getNomeIta()%>"
			                                value="<%=c.getId()%>"
			                                selected="<%= selected%>" />
			                        <%
                                }
                            }
                        %>
                    </aui:select>
                    <aui:input name="active" label="active" type="checkbox" checked="<%=active%>" />
                    <aui:input name="available" label="active-plants" type="checkbox"
                        checked="<%=available%>" />
                    <c:if test="<%= signedIn && !search %>">    
	                    <aui:input name="obsolete" label="obsolete" type="checkbox"
	                        checked="<%=obsolete%>" />
                    </c:if>
                    <aui:button inlineField="true" type="submit" value="search" />
                </aui:column>
                <c:if test="<%= signedIn && isCompany %>">
                    <aui:column columnWidth="15">
                        <aui:input name="code" inlineField="true" label="code"
                            cssClass="input-small"/>
                        <aui:input name="inactive" label="inactive" type="checkbox"
                        	checked="<%=inactive%>" />
                    </aui:column>
                </c:if>
                <aui:column columnWidth="35">
                    <aui:input name="name" label="name" cssClass="input-xlarge"/>
                    <aui:select id="shapes" name="shapes" label="shape">
                       <aui:option label="all" value="" selected="<%= shapeSelected.isEmpty()%>" />
                       <%
                            if (Validator.isNotNull(shapes)) {
                                for (CategorieMerceologiche s : shapes) {
                                %>
                                    <aui:option label="<%=s.getDescrizione()%>"
                                        value="<%=s.getCodiceCategoria()%>"
                                        selected="<%= s.getCodiceCategoria().equals(String.valueOf(shapeSelected))%>" />
                                <%
                                }
                            }
                        %>
                    </aui:select>
                    <aui:input name="vase" label="vase" inlineField="true" cssClass="input-small" />
                    <aui:input name="height" label="height" inlineField="true" cssClass="input-small">
                        <aui:validator name="number" />
                    </aui:input>
                </aui:column>
                <c:if test="<%= signedIn && isCompany %>">
	                <aui:column columnWidth="25">
		                <aui:select id="suppliers" name="suppliers" cssClass="<%=disableSuppliers ? "hidden" : "" %>"
		                    label="<%=disableSuppliers ? "" : "suppliers" %>">
		                    <%
	                            if (Validator.isNotNull(suppliers)) {
	                                for (AnagraficheClientiFornitori f : suppliers) {
	                                %>
	                                    <aui:option label="<%=f.getRagioneSociale1()%>"
	                                        value="<%=f.getCodiceAnagrafica()%>"
	                                        selected="<%= f.getCodiceAnagrafica().equals(String.valueOf(supplierId))%>" />
	                                <%
	                                }
	                            }
	                        %>
	                        <aui:option label="all" value="" selected="<%= supplierId.isEmpty()%>" />
	                    </aui:select>
	                </aui:column>
                </c:if>
                <c:if test="<%= !signedIn && !isCompany %>">
                    <aui:column columnWidth="35" last="true">
                        <liferay-ui:panel title="legend">
	                        <div class="portlet-msg-info">
	                            <liferay-ui:message key="active-help" />
	                        </div>
                        </liferay-ui:panel>
                    </aui:column>
                </c:if>
            </aui:layout>
        </span>
    </div>
</aui:form>

<liferay-portlet:renderURL varImpl="iteratorURL">
    <portlet:param name="mvcPath" value="/html/items/view.jsp" />
    <portlet:param name="categories" value="<%= String.valueOf(category) %>"/>
    <portlet:param name="active" value="<%= String.valueOf(active) %>"/>
    <portlet:param name="inactive" value="<%= String.valueOf(inactive) %>"/>
    <portlet:param name="obsolete" value="<%= String.valueOf(obsolete) %>"/>
    <portlet:param name="available" value="<%= String.valueOf(available) %>"/>
    <portlet:param name="suppliers" value="<%= String.valueOf(supplierId) %>"/>
    <portlet:param name="shapes" value="<%= shapeSelected %>"/>
    <portlet:param name="height" value="<%= String.valueOf(height) %>"/>
    <portlet:param name="vase" value="<%= vase %>"/>
    <portlet:param name="name" value="<%= name %>"/>
    <portlet:param name="code" value="<%= code %>"/>
    <portlet:param name="search" value="<%= String.valueOf(search) %>"/>
</liferay-portlet:renderURL>

<liferay-ui:search-container delta="75" emptyResultsMessage="no-plant"
     iteratorURL="<%=iteratorURL%>" >
    <liferay-ui:search-container-results>
        <%
            results = ListUtil.subList(piante,
                                    searchContainer.getStart(),
                                    searchContainer.getEnd());
                      total = piante.size();
                      pageContext.setAttribute("results", results);
                      pageContext.setAttribute("total", total);
        %>
    </liferay-ui:search-container-results>
    <liferay-ui:search-container-row
        className="it.bysoftware.ct.model.Piante" modelVar="pianta">
<%--         <c:if test="<%= signedIn && isCompany %>"> --%>
        <%
            String[] label = new String[] { pianta.getCodice(), ""};
            AnagraficheClientiFornitori s = AnagraficheClientiFornitoriLocalServiceUtil.
                    fetchAnagraficheClientiFornitori(pianta.getIdFornitore());
            if (signedIn && isCompany && s != null ) {
                label[1] = s.getRagioneSociale1();
            }
        %>
            <liferay-ui:search-container-column-text name="code" >
                <liferay-ui:message key="code-x" arguments="<%= label %>" />
            </liferay-ui:search-container-column-text>
<%--         </c:if> --%>
        <%
            String forma = "";
            boolean passport = false;
            String price = "";
            Articoli i = ArticoliLocalServiceUtil.fetchArticoli(pianta.getCodice());
            if(i != null) {
                try {
                    CategorieMerceologiche c = CategorieMerceologicheLocalServiceUtil.fetchCategorieMerceologiche(i.getCategoriaMerceologica());
                    if (c != null) {
                        forma = c.getDescrizione();
                    }
                } catch (Exception ex) {
                    _log.debug(ex.getMessage());
                }
                passport = i.getLibLng1() > 0;
                price = nf.format(i.getPrezzo1());
            }
        %>
        <liferay-ui:search-container-column-text name="name" property="nome"/>
        <liferay-ui:search-container-column-text name="shape" value="<%=forma %>"/>
        <liferay-ui:search-container-column-text name="vase" property="vaso" />
        <liferay-ui:search-container-column-text name="H" property="altezzaPianta"/>
        <liferay-ui:search-container-column-text name="plant-per-platform" property="piantePianale"/>
        <liferay-ui:search-container-column-text name="plant-per-cart" property="pianteCarrello" />
        <liferay-ui:search-container-column-text name="Pass." >
            <c:if test="<%=passport %>">
                <liferay-ui:icon image="check" message="yes" />
            </c:if>
            <c:if test="<%=!passport %>">
                <liferay-ui:icon image="close" message="no"/>
            </c:if>
        </liferay-ui:search-container-column-text>
        <c:if test="<%=signedIn %>">
            <liferay-ui:search-container-column-text name="price" value="<%=price %>" />
        </c:if>
        <c:if test="<%=signedIn && isCompany%>">
            <liferay-ui:search-container-column-text name="Pr. for." value="<%=nf.format(pianta.getPrezzoFornitore()) %>" />
        </c:if>
        <liferay-portlet:renderURL varImpl="editRowImageURL">
	        <portlet:param name="mvcPath"
	            value="/html/items/edit-picture.jsp" />
	        <portlet:param name="backUrl" value="<%= iteratorURL.toString() %>" />
	        <portlet:param name="itemId"
	            value="<%=String.valueOf(pianta.getId())%>" />
        </liferay-portlet:renderURL>
       <%
           long imgId = -1;
           FileEntry image = null;
           String imageSrc = File.separator + "portale-ordini-portlet/icons/not-found.png";
           
           try {
               imgId = Long.parseLong(pianta.getFoto1());
               image = DLAppLocalServiceUtil.getFileEntry(imgId);
          
               imageSrc = Constants.DOCUMENTS_FOLDER_NAME + File.separator + image.getRepositoryId()
                       + File.separator + image.getFolderId() + File.separator 
                       + image.getTitle(); 
           } catch (NumberFormatException ex) {
               _log.debug(ex.getMessage());    
           } catch (PortalException ex) {
               _log.debug(ex.getMessage());
           } catch (SystemException ex) {
               _log.debug(ex.getMessage());
           }
       %>
       <liferay-ui:search-container-column-text name="picture" >
<%--            <a href="<%= signedIn && isCompany ? editRowImageURL : imageSrc %>" target="<%= !isCompany ? "_blank" : ""%>"> --%>
           <a href="<%= imageSrc %>" target="_blank">
               <img src="<%=imageSrc %>" class="small center-h" />
           </a>
       </liferay-ui:search-container-column-text>
      <liferay-ui:search-container-column-text name="active" >
          <c:if test="<%=pianta.getAttiva()%>">
              <liferay-ui:icon image="activate" message="yes" />
          </c:if>
          <c:if test="<%=!pianta.getAttiva()%>">
              <liferay-ui:icon image="deactivate" message="no"/>
          </c:if>
     </liferay-ui:search-container-column-text>
       <c:if test="<%= signedIn && isCompany%>">
	       <liferay-ui:search-container-column-text property="disponibilita"
	           name="availability"/>
	       <c:choose>
	           <c:when test="<%=search %>">
	                <liferay-ui:search-container-column-button name="select"
                        href="set('${pianta.codice}')" />
	           </c:when>
	           <c:otherwise>    
                    <liferay-ui:search-container-column-jsp path="/html/items/item-action.jsp" />
               </c:otherwise>
           </c:choose>
       </c:if>
       <liferay-ui:search-container-row-parameter name="backUrl"
            value="<%= iteratorURL.toString() %>" />
    </liferay-ui:search-container-row>
    <liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
        paginate="true" />
</liferay-ui:search-container>

<%!private static Log _log = LogFactoryUtil
            .getLog("docroot.html.items.view_jsp");%>
            
<aui:script>
function set(data) {
    Liferay.Util.getOpener().closePopup(data,
        '<portlet:namespace/>itemDialog');
}

AUI().use('aui-base', 'aui-io-request', 'aui-modal', 'liferay-portlet-url', function (A) {
	if (A.one("#<portlet:namespace/>printBtn") !== null) {
		A.one("#<portlet:namespace/>printBtn").on('click', function() {
			var active = A.one('#<portlet:namespace />active').get('value');
			var inactive = A.one('#<portlet:namespace />inactive').get('value');
	        if (active !== 'false' || inactive !== 'false') {
	        	callResourceURL(A, 'xls');
			} else {
				alert('Seleziona almeno un filtro tra \'Attiva\' e \'Inattiva\'.');
			}        
	    });
	}
});

AUI().use('aui-base', 'aui-io-request', 'aui-modal', 'liferay-portlet-url', function (A) {
	if (A.one("#<portlet:namespace/>downloadBtn") !== null) {
		A.one("#<portlet:namespace/>downloadBtn").on('click', function() {
			var active = A.one('#<portlet:namespace />active').get('value');
			var inactive = A.one('#<portlet:namespace />inactive').get('value');
	        if (active !== 'false' || inactive !== 'false') {
	        	callResourceURL(A, 'pdf');
			} else {
				alert('Seleziona almeno un filtro tra \'Attiva\' e \'Inattiva\'.');
			}        
	    });
	}
});

AUI().use('aui-base', 'aui-io-request', 'aui-modal', 'liferay-portlet-url', function (A) {
	if (A.one("#<portlet:namespace/>downloadBtn1") !== null) {
		A.one("#<portlet:namespace/>downloadBtn1").on('click', function() {
			var active = A.one('#<portlet:namespace />active').get('value');
			var inactive = A.one('#<portlet:namespace />inactive').get('value');
	        if (active !== 'false' || inactive !== 'false') {
	        	callResourceURL(A, 'pdf-l');
			} else {
				alert('Seleziona almeno un filtro tra \'Attiva\' e \'Inattiva\'.');
			}        
	    });
	}
});

function callResourceURL(A, type) {
	var resourceURL = Liferay.PortletURL.createResourceURL();
    resourceURL.setResourceId('<%=ResourceId.printListItem.name()%>');
    resourceURL.setPortletId('<%= PortalUtil.getPortletId(request) %>');
    resourceURL.setParameter('category', A.one('#<portlet:namespace />categories').get('value'));
    resourceURL.setParameter('active', A.one('#<portlet:namespace />active').get('value'));
    resourceURL.setParameter('inactive', A.one('#<portlet:namespace />inactive').get('value'));
    resourceURL.setParameter('obsolete', A.one('#<portlet:namespace />obsolete').get('value'));
    resourceURL.setParameter('available', A.one('#<portlet:namespace />available').get('value'));
    resourceURL.setParameter('code', A.one('#<portlet:namespace />code').get('value'));
    resourceURL.setParameter('name', A.one('#<portlet:namespace />name').get('value'));
    resourceURL.setParameter('shapes', A.one('#<portlet:namespace />shapes').get('value'));
    resourceURL.setParameter('vase', A.one('#<portlet:namespace />vase').get('value'));
    resourceURL.setParameter('height', A.one('#<portlet:namespace />height').get('value'));
    resourceURL.setParameter('supplier', A.one('#<portlet:namespace />suppliers').get('value'));
    resourceURL.setParameter('type', type);
    window.open(resourceURL,'Download');
}

</aui:script>
