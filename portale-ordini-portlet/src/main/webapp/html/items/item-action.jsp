<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@page import="it.bysoftware.ct.model.Piante"%>
<%@include file="../init.jsp"%>
<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    Piante pianta = (Piante) row.getObject();
    String backUrl = (String) row.getParameter("backUrl");
%>

<liferay-ui:icon-menu>
	<liferay-portlet:renderURL varImpl="editImageURL">
		<portlet:param name="mvcPath"
			value="/html/items/edit-picture.jsp" />
		<portlet:param name="backUrl" value="<%= backUrl %>" />
		<portlet:param name="itemId"
			value="<%=String.valueOf(pianta.getId())%>" />
	</liferay-portlet:renderURL>
	<liferay-ui:icon image="edit" url="${editImageURL}" message="edit" />
</liferay-ui:icon-menu>