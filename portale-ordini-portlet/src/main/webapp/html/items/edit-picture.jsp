<%@page import="com.liferay.portal.kernel.json.JSONArray"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.service.VariantiLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Varianti"%>
<%@page import="it.bysoftware.ct.service.persistence.DescrizioniVariantiPK"%>
<%@page import="it.bysoftware.ct.service.DescrizioniVariantiLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.DescrizioniVarianti"%>
<%@page import="it.bysoftware.ct.utils.ResourceId"%>
<%@page import="it.bysoftware.ct.service.CategorieLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Categorie"%>
<%@page import="com.liferay.portal.kernel.exception.SystemException"%>
<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="it.bysoftware.ct.service.CategorieMerceologicheLocalServiceUtil"%>
<%@page import="java.util.List"%>
<%@page import="it.bysoftware.ct.model.CategorieMerceologiche"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="it.bysoftware.ct.service.ArticoliLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Articoli"%>
<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLAppServiceUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="it.bysoftware.ct.service.PianteLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Piante"%>
<%@page import="com.liferay.portlet.documentlibrary.util.DLUtil"%>
<%@page import="com.liferay.portal.kernel.repository.model.FileVersion"%>
<%@page import="com.liferay.portal.kernel.repository.model.FileEntry"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLAppHelperLocalServiceUtil"%>
<%@ include file="../init.jsp"%>

<%
	String title = ParamUtil.getString(renderRequest, "title", "edit-item");
	String variantsIds = ParamUtil.getString(renderRequest, "variantValues", "");
    String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
    String exceptionArgs = ParamUtil.getString(renderRequest, "exceptionArgs", "");    
    Object[] args = exceptionArgs.split(StringPool.COMMA);
    List<Categorie> categories = CategorieLocalServiceUtil.getCategories(0,
            CategorieLocalServiceUtil.getCategoriesCount());
    List<CategorieMerceologiche> shapes = CategorieMerceologicheLocalServiceUtil.
            getCategorieMerceologiches(0, CategorieMerceologicheLocalServiceUtil.
                    getCategorieMerceologichesCount());
    List<AnagraficheClientiFornitori> suppliers = AnagraficheClientiFornitoriLocalServiceUtil.findSuppliers();
    long itemId = ParamUtil.getLong(renderRequest, "itemId", -1);
    boolean continua = itemId == -1 || ParamUtil.getBoolean(renderRequest, "continua");
//     long fileEntryId = ParamUtil.getLong(renderRequest, "fileEntryId", -1);
    String backUrl = ParamUtil.getString(renderRequest, "backUrl");
    List<Varianti> variants = VariantiLocalServiceUtil.findByVariantType("VASOCOLORE");
    JSONArray jsnArr = JSONFactoryUtil.createJSONArray(variantsIds);
    
    Piante p = null;
    Articoli i = null;
    FileEntry fileEntry = null;
    if (itemId > 0) {
        p = PianteLocalServiceUtil.getPiante(itemId);
        try {
            i = ArticoliLocalServiceUtil.getArticoli(p.getCodice());
            fileEntry = DLAppServiceUtil.getFileEntry(Long.parseLong(p.getFoto1()));
        } catch (NumberFormatException ex) {
            _log.debug(ex.getMessage());
        } catch (PortalException e) {
            _log.debug(e.getMessage());
        } catch (SystemException e) {
            _log.debug(e.getMessage());
        }
    } else {
    	for (Varianti v : variants) {
        	jsnArr.put(v.getCodiceVariante());
        }
    }
    
//     NumberFormat nf = new DecimalFormat("#0.00");
    Calendar c = Calendar.getInstance();
    c.setTime(new Date());
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
%>
<liferay-ui:header backURL="<%=backUrl%>" title="<%= title%>" />
<liferay-portlet:renderURL varImpl="viewPictureURL">
    <portlet:param name="mvcPath" value="/html/items/edit-picture.jsp" />
    <portlet:param name="itemId" value="<%=String.valueOf(itemId)%>" />
    <portlet:param name="backUrl" value="<%=String.valueOf(backUrl)%>" />
</liferay-portlet:renderURL>
<liferay-portlet:resourceURL var="deletePictureUrl" id="<%=String.valueOf(ResourceId.deletePicture) %>" />
<liferay-ui:error key="error-inserting-item-x">
    <liferay-ui:message key="error-inserting-item-x" arguments="<%=args %>" />
</liferay-ui:error>
<liferay-ui:error key="error-inserting-empty-code-item">
    <liferay-ui:message key="empty-code" />
</liferay-ui:error>
<aui:layout>
    <liferay-portlet:actionURL name="editItem" var="editItem">
    	<c:if test="<%=!continua %>">
    		<portlet:param name="itemId" value="<%=String.valueOf(itemId)%>" />
    	</c:if>
    	<c:if test="<%=continua %>">
    		<portlet:param name="itemId" value="<%="-1"%>" />
    	</c:if>
    		<portlet:param name="continua" value="<%=String.valueOf(continua)%>" />
            <portlet:param name="backUrl" value="<%=String.valueOf(backUrl)%>" />
    </liferay-portlet:actionURL>
    <aui:form action="<%=editItem%>" method="post" enctype="multipart/form-data">
        <aui:nav-bar>
            <aui:nav>
                <aui:nav-item id="saveBtn" iconCssClass="icon-save" label="save"
                   selected='<%="save".equals(toolbarItem)%>'/>
            </aui:nav>
        </aui:nav-bar>
        <aui:column columnWidth="40">
        <aui:fieldset label="item-details">
            <c:choose>
                <c:when test="<%=p != null %>">
                    <aui:input type="text" name="code" value="<%=p.getCodice()%>" cssClass="input-small" readonly="true" inlineField="true"/>
                    <aui:input type="hidden" name="id" value="<%=p.getId()%>" />
                    <aui:input type="text" name="name" value="<%=p.getNome()%>" inlineField="true"/>
                </c:when>
                <c:otherwise>
                    <aui:input type="text" name="code" cssClass="input-small" inlineField="true" />
                    <aui:input type="hidden" name="id" value="<%=itemId%>" />
                    <aui:input type="text" name="name" inlineField="true"/>
                </c:otherwise>
            </c:choose>
            <aui:select id="itemShapes" name="itemShapes" label="shape"
                    inlineField="true">
                    <aui:option label="select-shape" value="0" />
                <%
                    if (Validator.isNotNull(shapes)) {
                        for (CategorieMerceologiche s : shapes) {
                            if (i != null && i.getCategoriaMerceologica().equals(s.getCodiceCategoria())) {
                %>
                   <aui:option label="<%=s.getDescrizione()%>" value="<%=s.getCodiceCategoria()%>" selected="true" />
                <%
                            } else {
                %>
                   <aui:option label="<%=s.getDescrizione()%>" value="<%=s.getCodiceCategoria()%>" />
                <%
                            }
                        }
                    }
                %>
            </aui:select>
            <aui:select id="itemCategories" name="itemCategories" label="categories"
                    inlineField="true">
<%--                     <aui:option label="select-shape" value="0" /> --%>
                <%
                    if (Validator.isNotNull(categories)) {
                        for (Categorie category : categories) {
                            if (p != null) { 
                                if (p.getIdCategoria() == category.getId()) {
			                    %>
			                         <aui:option label="<%=category.getNomeIta()%>" value="<%=category.getId()%>" selected="true" />
			                    <%
			                    } else {
			                    %>
				                     <aui:option label="<%=category.getNomeIta()%>" value="<%=category.getId()%>" />
			                    <%
			                    }
                            } else if (17 == category.getId()) {
                                %>
                                    <aui:option label="<%=category.getNomeIta()%>" value="<%=category.getId()%>" selected="true" />
                                <%
                            } else {
                                %>
                                    <aui:option label="<%=category.getNomeIta()%>" value="<%=category.getId()%>" />
                                <%
                            }
                        }
                    }
                %>
            </aui:select>
            <aui:layout>
                <aui:column columnWidth="30" first="true">
                    <aui:input type="text" name="plant-per-platform" cssClass="input-small" label="plant-per-platform"
                        value="<%=p != null ? p.getPiantePianale() : ""%>" >
                        <aui:validator name="number" />
                        <aui:validator name="min">1</aui:validator>
                    </aui:input>
                    <aui:input type="text" name="vase" cssClass="input-small"
                        value="<%=p != null ? p.getVaso() : ""%>" />
                </aui:column>
                <aui:column columnWidth="30">
                    <aui:input type="text" name="plant-per-cart" cssClass="input-small" label="plant-per-cart"
                        value="<%=p != null ? p.getPianteCarrello() : ""%>" >
                        <aui:validator name="number" />
                        <aui:validator name="min">1</aui:validator>
                    </aui:input>
                    <aui:input type="text" name="plant-height" cssClass="input-small"
                        value="<%=p != null ? p.getAltezzaPianta() : ""%>" />
                </aui:column>
                <aui:column columnWidth="30" last="true">
                    <aui:input type="text" name="foliage-height" cssClass="input-small" label="Circ.chioma"
                        value="<%=p != null ? p.getAltezzaChioma() : ""%>" />
                    <aui:input type="text" name="platform-height" cssClass="input-small" label="Alt.ripiano"
                        value="<%=p != null ? p.getAltezza() : 0%>" >
                        <aui:validator name="number" />
                        <aui:validator name="min" errorMessage="positive-number">1</aui:validator>
                    </aui:input>
                </aui:column>
            </aui:layout>
            <aui:layout>
                <aui:column columnWidth="25" first="true">
                    <aui:input type="text" name="item-2-over" cssClass="input-mini" label="item-2-over"
                        value="<%=p != null ? p.getSormonto2Fila() : ""%>" >
                        <aui:validator name="number" />
                    </aui:input>
                    <aui:input id="passport" name="passport" label="passport"
                        type="checkbox" value="<%= i != null ? i.getLibLng1() > 0 : true%>" />
                    <aui:input id="gross-weight" name="gross-weight" cssClass="input-small"
                        label="gross-weight" suffix="Kg"
                        value="<%=i != null ? i.getPesoLordo() : 0%>" >
                            <aui:validator name="number" />
                            <aui:validator name="min">-1</aui:validator>
                    </aui:input>
                    <aui:input id="item-note" type="textarea" name="item-note" label="note"
                        value="<%=p != null ? p.getTempoConsegna() : ""%>" />                    
                </aui:column>
                <aui:column columnWidth="25">
                    <aui:input type="text" name="item-3-over" cssClass="input-mini" label="item-3-over"
                        value="<%=p != null ? p.getSormonto3Fila() : ""%>" >
                        <aui:validator name="number" />
                    </aui:input>
                    <aui:input id="active" name="active" label="active" type="checkbox"
                        value="<%= p != null ? p.getAttiva() : true %>" /> 
<%--                         value="<%= i != null ? !i.getObsoleto() : true %>" /> --%>
                </aui:column>
                <aui:column columnWidth="25">
                    <aui:input type="text" name="item-4-over" cssClass="input-mini" label="item-4-over"
                        value="<%=p != null ? p.getSormonto4Fila() : ""%>" >
                        <aui:validator name="number" />
                    </aui:input>
                    <aui:input type="text" name="default-passport"
                    	value="<%= p != null ? p.getPassaportoDefault() : ""%>" />
                </aui:column>
                <aui:column columnWidth="25" last="true">
                    <aui:input type="text" name="add-platform" cssClass="input-mini" label="Agg.sormonto"
                        value="<%=p != null ? p.getAggiuntaRipiano() : 0%>" >
                        <aui:validator name="number" />
                        <aui:validator name="min">-1</aui:validator>
                    </aui:input>
                </aui:column>
            </aui:layout>
            <aui:layout>
                <aui:column columnWidth="100">
                    <aui:input id="ean" type="text" name="ean" label="barcode"
                        value="<%=p != null ? p.getEan() : ""%>" >
                        <aui:validator name="custom" errorMessage="invalid-ean" >
                            function (barcode, fieldNode, ruleValue) {
                              // check length
                              if(barcode === '') {
                                return true;
                              }
                              if (barcode.length < 8 || barcode.length > 18 ||
                                  (barcode.length != 8 && barcode.length != 12 &&
                                  barcode.length != 13 && barcode.length != 14 &&
                                  barcode.length != 18)) {
                                return false;
                              }
                            
                              var lastDigit = Number(barcode.substring(barcode.length - 1));
                              var checkSum = 0;
                              if (isNaN(lastDigit)) { return false; } // not a valid upc/ean
                            
                              var arr = barcode.substring(0,barcode.length - 1).split("").reverse();
                              var oddTotal = 0, evenTotal = 0;
                            
                              for (var i=0; i < arr.length; i++) {
                                if (isNaN(arr[i])) { return false; } // can't be a valid upc/ean we're checking for
                            
                                if (i % 2 == 0) { oddTotal += Number(arr[i]) * 3; }
                                else { evenTotal += Number(arr[i]); }
                              }
                              checkSum = (10 - ((evenTotal + oddTotal) % 10)) % 10;
                            
                              // true if they are equal
                              return checkSum == lastDigit;
                            }
                        </aui:validator>
                    </aui:input>
                </aui:column>
            </aui:layout>
            <aui:layout>
                <aui:column columnWidth="30" first="true">
                    <aui:input id="price1" name="price1" cssClass="input-small"
                        inlineField="true" label="price" prefix="€" 
                        value="<%=i != null ? i.getPrezzo1() : 0%>" >
                        <aui:validator name="number" />
                        <aui:validator name="min">-1</aui:validator>
                    </aui:input>
                </aui:column>
                <aui:column columnWidth="30" >
                    <aui:input id="price2" name="price2" cssClass="input-small"
                        label="con vaso colorato" prefix="€"
                        value="<%=i != null ? i.getPrezzo2() : 0%>" >
                            <aui:validator name="number" />
                            <aui:validator name="min">-1</aui:validator>
                    </aui:input>
                </aui:column>
                <aui:column columnWidth="30" last="true">
                    <aui:input id="price3" name="price3" cssClass="input-small"
                        label="per pianale" prefix="€"
                        value="<%=i != null ? i.getPrezzo3() : 0%>" >
                            <aui:validator name="number" />
                            <aui:validator name="min">-1</aui:validator>
                    </aui:input>
                </aui:column>
            </aui:layout>
            </aui:fieldset>
            <aui:fieldset label="supplier-details">
                <aui:select id="supplier" name="supplier" label="supplier" showEmptyOption="true" showRequiredLabel="true" required="true" errorMessage="select-supplier">
<%--                         <aui:option label="select-supplier" value="0" /> --%>
                    <%
                        if (Validator.isNotNull(suppliers)) {
                            for (AnagraficheClientiFornitori s : suppliers) {
                                if (p != null && p.getIdFornitore().equals(s.getCodiceAnagrafica())) {
                    %>
                       <aui:option label="<%=s.getRagioneSociale1() + " " + s.getRagioneSociale2()%>"
                            value="<%=s.getCodiceAnagrafica()%>" selected="true" />
                    <%
                                } else {
                    %>
                       <aui:option label="<%=s.getRagioneSociale1() + " " + s.getRagioneSociale2()%>"
                            value="<%=s.getCodiceAnagrafica()%>" />
                    <%
                                }
                            }
                        }
                    %>
                </aui:select>
                <aui:layout>
                <aui:column columnWidth="50">
                    <aui:input id="suppPrice" name="suppPrice" cssClass="input-small"
                        inlineField="true" label="supplier-price" prefix="€"
                        value="<%=p != null && p.getPrezzoFornitore() != 0 ? p.getPrezzoFornitore() : 0%>" >
                            <aui:validator name="number" />
                    </aui:input>
                    <aui:input id="obsolete" name="obsolete" label="obsolete" type="checkbox"
                        value="<%= p != null ? p.getObsoleto() : false %>" /> 
                </aui:column>
                <aui:column columnWidth="50">
                    <aui:input id="availability" name="availability" cssClass="input-small"
                        inlineField="true" label="availability" prefix="#"
                        value="<%=p != null ? p.getDisponibilita() : 0%>" >
                            <aui:validator name="required" />
                            <aui:validator name="number" />
                            <aui:validator name="min">0</aui:validator>
                    </aui:input>
                </aui:column>
            </aui:layout>
            </aui:fieldset>
        </aui:column>
<%--    </aui:form> --%>
    <aui:column columnWidth="60">
<%--        <liferay-portlet:actionURL name="upload" var="uploadFileURL"> --%>
<%--             <portlet:param name="itemId" value="<%=String.valueOf(itemId)%>" /> --%>
<%--         </liferay-portlet:actionURL> --%>
<%--         <aui:form action="<%=uploadFileURL%>" enctype="multipart/form-data" --%>
<%--             method="post"> --%>
			
            <aui:fieldset label="variants">
            <aui:input name="variantsIds" type="hidden" value="<%= "[" +  jsnArr.join(",") + "]" %>" /> 
            <aui:button-row>
            	<aui:button id="all" name="select-all" inlineField="true" value="all"
            		cssClass="btn-success" icon="icon-check" />
            	<aui:button id="none" name="select-none" inlineField="true" value="none"
            		cssClass="btn-warning" icon="icon-unchecked" />
            </aui:button-row>
            
            <%
            List<Varianti> var1 = new ArrayList<Varianti>();
            List<Varianti> var2 = new ArrayList<Varianti>();
            for (int idx = 0; idx < variants.size(); idx++) {
            	if (idx < variants.size() / 2) {
            		var1.add(variants.get(idx));
            	} else {
            		var2.add(variants.get(idx));
            	}
            	
            }
            %>
            <aui:layout>
            	<aui:column columnWidth="40">
            	<%
		            for(Varianti variant : var1) {
		            	boolean checked = true;
		            	if (i != null) {
		            		DescrizioniVarianti v = DescrizioniVariantiLocalServiceUtil.fetchDescrizioniVarianti(
		            				new DescrizioniVariantiPK(i.getCodiceArticolo(), variant.getCodiceVariante()));
		            		checked = v != null;
		            	}
		            %>
		            	<aui:input id="<%="variant_" + variant.getCodiceVariante() %>"
		            		name="variants" cssClass="variants"	type="checkbox"
		            		checked="<%=checked%>" value="<%=variant.getCodiceVariante() %>"
		            		label="<%=variant.getDescrizione() %>"/>
		            <%
		            }
	            %>
            	</aui:column>
            	<aui:column columnWidth="40">
            		<%
		            for(Varianti variant : var2) {
		            	boolean checked = true;
		            	if (i != null) {
		            		DescrizioniVarianti v = DescrizioniVariantiLocalServiceUtil.fetchDescrizioniVarianti(new DescrizioniVariantiPK(i.getCodiceArticolo(), variant.getCodiceVariante()));
		            		checked = v != null;
		            	}
		            %>
		            	<aui:input id="<%="variant_" + variant.getCodiceVariante() %>" name="variants" cssClass="variants"
		            		type="checkbox" checked="<%=checked%>" value="<%=variant.getCodiceVariante() %>" label="<%=variant.getDescrizione() %>"/>
		            <%
		            }
	            %>
            	</aui:column>
            </aui:layout>
            </aui:fieldset>
            <aui:fieldset label="picture">
            	<aui:input id="link-drive" name="link-drive" type="text" cssClass="input-xxlarge" label="Link foto"
                    value="<%=p != null ? p.getFoto3() : "" %>" />
                <aui:input id="lastUpdate" name="lastUpdate" type="text" inlineField="true" cssClass="input-small"
                    value="<%=p != null ? sdf.format(p.getUltimoAggiornamentoFoto() != null ? p.getUltimoAggiornamentoFoto() : c.getTime()) : c.getTime() %>" disabled="true"/>
                <aui:input id="expiration" name="expiration" type="text" inlineField="true" cssClass="input-small"
                    value="<%=p != null ? sdf.format(p.getScadenzaFoto() != null ? p.getScadenzaFoto() : c.getTime()) : c.getTime() %>" disabled="true"/>
                <aui:input id="expirationDays" name="expirationDays" type="text" inlineField="true" cssClass="input-small"
                    value="<%=p != null ? p.getGiorniScadenzaFoto() : Constants.HUNDRED%>" prefix="#">
                    <aui:validator name="number" />
                    <aui:validator name="min" errorMessage="positive-number">1</aui:validator>
                </aui:input>
                <liferay-ui:error key="empty-file" message="empty-image" />
                <liferay-ui:error key="disk-space" message="disk-space" />
                <aui:input name="fileEntryId" type="hidden"
                    value="<%=(fileEntry != null) ? String.valueOf(fileEntry.getFileEntryId()) : ""%>" />
                <aui:input type="file" name="fileupload" inlineField="true"
                    label="file">
                    <aui:validator name="acceptFiles">'jpg,png,bmp'</aui:validator>
                </aui:input>
            </aui:fieldset>
<%--         </aui:form> --%>
        <%
            int previewFileCount = 0;
            String previewFileURL = null;
            String[] previewFileURLs = null;
            String videoThumbnailURL = null;
            String previewQueryString = null;
        %>
        <c:choose>
            <c:when test="<%=fileEntry != null%>">
                <%
                    FileVersion fileVersion = fileEntry.getFileVersion();
                            //boolean hasPDFImages = PDFProcessorUtil.hasImages(fileVersion);
                            previewQueryString = "&imagePreview=";
                            previewFileURL = DLUtil.getPreviewURL(fileEntry, fileVersion,
                                    themeDisplay, previewQueryString);
                %>
                <button id="btnDeletePicture" class="btn btn-warning" type="button">
                    <i class="icon-trash"></i> Elimina foto
                </button>
                <div class="lfr-preview-file" id="<portlet:namespace />previewFile">
                    <div class="lfr-preview-file-content"
                        id="<portlet:namespace />previewFileContent">
                        <div class="lfr-preview-file-image-current-column">
                            <div class="lfr-preview-file-image-container">
                                <img class="lfr-preview-file-image-current"
                                    id="<portlet:namespace />previewFileImage"
                                    src="<%=previewFileURL + "1"%>" />
                            </div>
                            <span class="lfr-preview-file-actions aui-helper-hidden"
                                id="<portlet:namespace />previewFileActions"> <span
                                class="lfr-preview-file-toolbar"
                                id="<portlet:namespace />previewToolbar"></span> <span
                                class="lfr-preview-file-info"> <span
                                    class="lfr-preview-file-index"
                                    id="<portlet:namespace />previewFileIndex">1</span> of <span
                                    class="lfr-preview-file-count"><%=previewFileCount%></span>
                            </span>
                            </span>
                        </div>

                        <div class="lfr-preview-file-images"
                            id="<portlet:namespace />previewImagesContent">
                            <div class="lfr-preview-file-images-content"></div>
                        </div>
                    </div>
                </div>
                <aui:script use="aui-base,liferay-preview">
                    new Liferay.Preview(
                            {
                                actionContent : '#<portlet:namespace />previewFileActions',
                                baseImageURL : '<%= previewFileURL %>',
                                boundingBox : '#<portlet:namespace />previewFile',
                                contentBox : '#<portlet:namespace />previewFileContent',
                                currentPreviewImage : '#<portlet:namespace />previewFileImage',
                                imageListContent : '#<portlet:namespace />previewImagesContent',
                                maxIndex : <%= previewFileCount %>,
                                previewFileIndexNode : '#<portlet:namespace />previewFileIndex',
                                toolbar : '#<portlet:namespace />previewToolbar'
                            }).render();
                </aui:script>
            </c:when>
            <c:otherwise>
                <h2>Anteprima non disponibile.</h2>
                <button id="btnRefresh" class="btn" type="button">
                    <i class="icon-refresh"></i> Aggiorna
                </button>
            </c:otherwise>
        </c:choose>
    </aui:column>
<%--    <aui:button id="btnSave" name="Save" value="save" type="submit" /> --%>
    </aui:form>
</aui:layout>
<div class="yui3-skin-sam">
    <div id="modal"></div>
</div>
<aui:script>
	var variantsIds; // = '<%= jsnArr.toString()%>';
	var continua = '<%= continua %>';
	AUI().ready(function(A) {
		variantsIds = JSON.parse(A.one('#<portlet:namespace />variantsIds').get('value'));	
		if (A.one('#<portlet:namespace />id').get('value') != -1 && continua==="true") {
			if (confirm('Vuoi continuare con gli inserimenti?')){
				A.one('#<portlet:namespace />id').set('value', -1);
				A.one('#<portlet:namespace />code').set('value', '');
				A.one('#<portlet:namespace />code').removeAttribute('readonly');
				A.one('#<portlet:namespace />fileEntryId').set('value', '');
			}	
		}
    	/* if (variantsIds !== '{}') {
	    	console.log(JSON.parse(variantsIds));
    	} */
    });
	AUI().use("aui-base", function(A) {
		var variants = A.all('.variants');
		A.one('#<portlet:namespace />select-all').on('click', function(event) {
			variantsIds = <%= jsnArr.toString()%>;
			A.one('#<portlet:namespace />variantsIds').set('value', JSON.stringify(variantsIds));
			variants._nodes.forEach(function(elt, i) {
				A.one('#' + elt.id).set('checked', true);
// 				A.one('#' + elt.id).set('value', true);
			});
		});
		A.one('#<portlet:namespace />select-none').on('click', function(event) {
			variantsIds = [];
			A.one('#<portlet:namespace />variantsIds').set('value', '[]');
			variants._nodes.forEach(function(elt, i) {
				A.one('#' + elt.id).set('checked', false);
// 				A.one('#' + elt.id).set('value', false);
			});
		});
		variants._nodes.forEach(function(elt, i) {
			A.one('#' + elt.id).on('change', function(event){
				console.log(event);
				if (event.currentTarget._node.checked) {
					variantsIds.push(event.currentTarget._node.value);
				}
				else {
					var index = variantsIds.indexOf(event.currentTarget._node.value);
					 
				    if (index > -1) {
				       variantsIds.splice(index, 1);
				    }
				}
				A.one('#<portlet:namespace />variantsIds').set('value', JSON.stringify(variantsIds));
			});
		});
	});
	
    AUI().use("aui-base", "liferay-util-list-fields", function(A) {
        if (A.one('#<portlet:namespace />saveBtn') !== null) {
            A.one('#<portlet:namespace />saveBtn').on('click', function(event) {
                var validator = Liferay.Form._INSTANCES.<portlet:namespace/>fm.formValidator;
                validator.validate();
                if (!validator.hasErrors()) {
                    AUI().use("aui-modal", function(A) {
                        var modal = new A.Modal({
                            bodyContent : '<div class="loading-animation"/>',
                            centered : true,
                            headerContent : '<h3>Loading...</h3>',
                            modal : true,
                            render : '#modal',
                            close : false,
                            width : 450
                        }).render();
                        modal.after("render", function() {
                            submitForm(document.<portlet:namespace/>fm);
                        });
                    });
                } else {
                    validator.focusInvalidField();
                    alert('Verifica i campi inseiriti.');
                    return;
                }
            });
        }
    });

    AUI().use('node', function(A) {
        if (A.one('#btnRefresh') !== null) {
            A.one('#btnRefresh').on('click', function() {
                window.location.href = '<%=viewPictureURL%>';
            });
        }
    });
    
    AUI().use('node', 'aui-base', 'aui-io-request', 'aui-modal', function(A) {
        if (A.one('#btnDeletePicture') !== null) {
            A.one('#btnDeletePicture').on('click', function() {
            	if (confirm('Confermi l\'eliminazione dell\'immagine?')) {
	                var modal;
	                A.io.request('<%=deletePictureUrl %>', {
	                    method: 'POST',
	                    data: {
	                        <portlet:namespace/>fileEntryId: A.one('#<portlet:namespace />fileEntryId').get('value')
	                    },
	                    on: {
	                        start: function () {
	                            modal = new A.Modal({
	                                bodyContent : '<div class="loading-animation"/>',
	                                centered : true,
	                                headerContent : '<h3>Loading...</h3>',
	                                modal : true,
	                                render : '#modal',
	                                close: false,
	                                zIndex: 99999,
	                                width : 450
	                            }).render();
	                        },
	                        success: function () {
	                            var data = JSON.parse(this.get('responseData'));
	                            modal.hide();
	                            if (data.code === 0) {
	                            	alert('Eliminazione completata con succeso.');
	                            	window.location.href = '<%=viewPictureURL%>';
	                            }
	                        },
	                        failure: function () {
	                            alert("Si e' verificato un errore nella cancellazione dell'immagine.");
	                            modal.hide();
	                        }
	                    }
	                });
                }
            });
        }
    });
</aui:script>
    


<%!private static Log _log = LogFactoryUtil
            .getLog("docroot.html.items.edit_item_jsp");%>