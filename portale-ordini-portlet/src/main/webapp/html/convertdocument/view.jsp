<%@page import="it.bysoftware.ct.utils.ResourceId"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="it.bysoftware.ct.service.ArticoloSocioLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.ArticoloSocio"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="it.bysoftware.ct.convertdocument.Row"%>
<%@page import="com.liferay.portal.kernel.util.StringUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.bysoftware.ct.convertdocument.Invoice"%>
<%@page import="it.bysoftware.ct.scheduler.InvoiceExporterScheduler"%>
<%@page import="java.util.List"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.liferay.portal.service.ClassNameLocalServiceUtil"%>
<%@page
	import="com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil"%>
<%@page import="com.liferay.portlet.expando.model.ExpandoValue"%>
<%@ include file="../init.jsp"%>
<%
    String toolbarItem = ParamUtil.getString(renderRequest,
        "toolbarItem", "view-all");
    String backUrl = ParamUtil.getString(renderRequest, "backUrl");
    String args = ParamUtil.getString(renderRequest, "exceptionArgs");
    ExpandoValue value = ExpandoValueLocalServiceUtil.getValue(
            user.getCompanyId(),
            ClassNameLocalServiceUtil.getClassNameId(User.class),
            "CUSTOM_FIELDS", Constants.REGISTRY_CODE, user.getUserId());
    String partnerCode = value.getString();
    List<Invoice> invoices = (ArrayList<Invoice>) renderRequest.getAttribute("invoices");
    List<String> names = new ArrayList<String>();
    if (Validator.isNotNull(invoices)) {
        for (int i = 0; i < invoices.size(); i++) {
            names.add("Fattura " + invoices.get(i).getProtoc());
        }
    }
    DecimalFormat nf = (DecimalFormat) NumberFormat.getInstance(Locale.ITALIAN);
%>
<c:if test="<%=invoices != null %>">
    <portlet:resourceURL var="saveInvoiceURL" id="<%=ResourceId.saveInvoice.name() %>" />
    <liferay-ui:tabs names="<%=StringUtil.merge(names, ",") %>" refresh="false" tabsValues="<%=StringUtil.merge(names, ",") %>">
        <%
        int i = 0;
        for (String name : names) {
            Invoice invoice = invoices.get(i);
            List<Row> list = invoice.getRows();
            double amount = 0;
            String tagId = "saveBtn_" + i;
            String tagRef = "orderRef_" + i;
            String successId = "group-success-block_" + i;
            String errorId = "group-error-block_" + i;
            String invoiceId = "invoice_" + i;
        %>
            <liferay-ui:section>
                <span id="<%=successId %>" ></span>
                <span id="<%=errorId %>" ></span>
                <button id="<%=tagId %>" class="btn saveBtn"><i class="icon-save"></i>Salva</button>
	            <liferay-portlet:renderURL varImpl="iteratorURL">
				    <portlet:param name="mvcPath"
				        value="/html/convertdocument/view.jsp" />
			        <portlet:param name="tabName" value="<%=name %>" />
				</liferay-portlet:renderURL>
				<aui:input id="<%=tagRef %>" name="<%=tagRef %>" label="order-ref" value="<%=invoice.getNudaor()%>" disabled="true" />
				<liferay-ui:search-container delta="100" emptyResultsMessage="no-invoice" 
				    iteratorURL="<%=iteratorURL %>">
				    <liferay-ui:search-container-results>
				        <%
				            results = ListUtil.subList(list, searchContainer.getStart(),
				                    searchContainer.getEnd());
				            total = list.size();
				            
				            pageContext.setAttribute("results", results);
				            pageContext.setAttribute("total", total);
				        %>
				    </liferay-ui:search-container-results>
				    <liferay-ui:search-container-row className="it.bysoftware.ct.convertdocument.Row"
				        modelVar="invoiceRow" >
				        <c:choose>
				            <c:when test="<%=invoiceRow.getTiprig() != 0 %>">
					            <liferay-ui:search-container-column-text name="check" >
					                <liferay-ui:icon image="check" />
					            </liferay-ui:search-container-column-text>
				            </c:when>
				            <c:otherwise>
				                <%
			                        ArticoloSocio item = ArticoloSocioLocalServiceUtil.findCompanyItem(
			                                partnerCode, invoiceRow.getCodart(), invoiceRow.getCodvar());
			                        %>
			                        <liferay-ui:search-container-column-text name="check" >
			                             <c:if test="<%=item != null%>">
			                                 <liferay-ui:icon image="check" />
			                             </c:if>
			                             <c:if test="<%=item == null%>">
			                                 <liferay-ui:icon image="close" />
			                             </c:if>
			                        </liferay-ui:search-container-column-text>
				            </c:otherwise>
				        </c:choose>
				        <liferay-ui:search-container-column-text property="codart" name="code" />
				        <liferay-ui:search-container-column-text property="codvar" name="variant" />
				        <liferay-ui:search-container-column-text property="descri" name="description" />
				        <liferay-ui:search-container-column-text property="unimis" name="unit-mis" />
				        <liferay-ui:search-container-column-text name="quantity" value="<%=nf.format(invoiceRow.getQuanet()) %>"/>
				        <liferay-ui:search-container-column-text name="price" value="<%=nf.format(invoiceRow.getPrezzo()) %>"/>
				        <liferay-ui:search-container-column-text name="amount" value="<%=nf.format(invoiceRow.getImpnet()) %>"/>
				        <liferay-ui:search-container-column-text property="codiva" name="IVA" />
				        <%amount += invoiceRow.getImpnet(); %>
				    </liferay-ui:search-container-row>
				    <liferay-ui:search-iterator searchContainer="<%=searchContainer %>" paginate="false"/>
				</liferay-ui:search-container>
				<aui:input id="taxable" name="taxable" prefix="&euro;" value="<%=nf.format(amount) %>" disabled="true" />
				<aui:input id="<%=invoiceId %>" name="<%=invoiceId %>" value="<%=JSONFactoryUtil.looseSerialize(invoice, "rows") %>" disabled="true" type="hidden"/>
		    </liferay-ui:section>
        <%
        i++;
        }
        %>
    </liferay-ui:tabs>
    <aui:script>
        var saveURL = '<%=saveInvoiceURL %>';
        var partnerCode = '<%=partnerCode%>';
	    AUI().use("aui-base", "liferay-util-list-fields", function(A) {
            A.all(".saveBtn").on('click', function(e){
            	var tmp = e._currentTarget.id.split("_");
                var r = confirm('Confermi l\'importazione della fattura?');
                if (r == true) {
                	var successBlock = A.one('#group-success-block_' + tmp[tmp.length - 1]);
                    var errorBlock = A.one('#group-error-block_' + tmp[tmp.length - 1]);
                	AUI().use('aui-base', 'aui-io-request', 'aui-modal', function (A) {
                        A.io.request(saveURL, {
                            method: 'POST',
                            data: {
                            	<portlet:namespace />partner: partnerCode,
                            	<portlet:namespace />invoice: A.one('#<portlet:namespace />invoice_' + tmp[tmp.length - 1]).get('value')
                            },
                            on: {
                                start: function() {
                                    modal = new A.Modal({
                                        bodyContent : '<div class="loading-animation"/>',
                                        centered : true,
                                        headerContent : '<h3>Loading...</h3>',
                                        modal : true,
                                        render : '#modal',
                                        close: false,
                                        zIndex: 99999,
                                        width : 450
                                    }).render();
                                },
                                success: function () {
                                    var data = JSON.parse(this.get('responseData'));
                                    console.log(data);
                                    if (successBlock != null) {
                                        successBlock.empty();
                                    }
                                    if (errorBlock != null) {
                                    	errorBlock.empty();
                                    }
                                    if (data.code === 0) {
                                        A.one('#saveBtn_' + tmp[tmp.length - 1]).toggle();
                                        var messageNode = A.Node.create('<div id="test" class="portlet-msg-success">Salvata Correttamente.</div>');
                                        messageNode.appendTo(successBlock);
                                    } else {
                                    	alert('Impossibile effettuare il savataggio.\nErrore: ' + data.message);
                                    	var messageNode = A.Node.create('<div id="test" class="portlet-msg-error">Fattura non salvata. Errore:  ' + data.message + '</div>');
                                    	messageNode.appendTo(errorBlock);
                                    }
                                    modal.hide();
                                },
                                error: function () {
                                	console.log("ERRORE");
                                    modal.hide();
                                }
                            }
                        });
                    });
                }
            });
	    });
	</aui:script>
</c:if>
<aui:fieldset label="import">
	<liferay-ui:error key="error-index-x">
	    <liferay-ui:message key="error-index-x" arguments="<%=args %>" />
	</liferay-ui:error>
	<liferay-portlet:actionURL name="uploadInvoice" var="uploadFileURL">
	    <portlet:param name="backUrl" value="<%=backUrl%>" />
	</liferay-portlet:actionURL>
	<liferay-ui:error key="empty-file" message="empty-data" />
	<liferay-ui:error key="disk-space" message="disk-space" />
	<aui:form action="<%=uploadFileURL%>" enctype="multipart/form-data"
	    method="post">
	    <aui:input name="partnerCode" id="partnerCode" type="hidden"
	        value="<%=partnerCode%>" />
	    <aui:input type="file" name="fileupload" inlineField="true"
	        label="file">
	        <aui:validator name="acceptFiles">'txt,csv'</aui:validator>
	    </aui:input>
	    <aui:button name="upload" type="submit" value="import" />
	</aui:form>
</aui:fieldset>