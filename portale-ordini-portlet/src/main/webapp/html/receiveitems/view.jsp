<%@page import="it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil"%>
<%@page import="java.util.List"%>
<%@page import="it.bysoftware.ct.model.AnagraficheClientiFornitori"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../init.jsp"%>

<liferay-portlet:renderURL varImpl="loadSupplierOrderURL">
	<portlet:param name="mvcPath" value="/html/receiveitems/order.jsp" />
</liferay-portlet:renderURL>
<liferay-ui:header title="receive-items" />
<aui:form action="<%=loadSupplierOrderURL%>" method="get" name="fm">
	<audio id="eventSound" src="/portale-ordini-portlet/sounds/beep.mp3" preload="auto"></audio>
	<liferay-portlet:renderURLParams varImpl="loadSupplierOrderURL" />
	<aui:layout>
		<aui:column first="true" columnWidth="50">
			<div style="position: relative; width: 100%; max-width: 400px;">
				<video id="video" style="width: 100%; max-width: 400px;"
					playsinline="true"></video>
				<canvas id="canvas" style="display: none;" height="480" width="640"></canvas>
				<canvas id="overlay"
					style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; z-index: 2; pointer-events: none;"
					height="480" width="640"></canvas>
			</div>
			<aui:button-row>
				<button id="pause" class="btn btn-warning" type="button">
					<i class="icon-pause"></i> Pause
				</button>
				<button id="resume" class="btn btn-success" type="button" disabled>
					<i class="icon-play"></i> Play
				</button>
			</aui:button-row>
		</aui:column>
		<aui:column columnWidth="50">
			<aui:input id="order_id" name="order_id" label="id" type="hidden" />
            <aui:input id="supplier_id" name="supplier_id" label="supplier-id" type="hidden"/>
            <aui:input id="order_num" name="order_num" label="order" inlineField="true" cssClass="input-small" />
<%--             <aui:input id="supplier" name="supplier" label="supplier" inlineField="true" cssClass="input-small"/> --%>
            <aui:select name="supplier" showEmptyOption="true"
				label="partner">
				<%
				List<AnagraficheClientiFornitori> partners = AnagraficheClientiFornitoriLocalServiceUtil.findSuppliers();
					for (AnagraficheClientiFornitori partner : partners) {
				%>
				<aui:option value="<%=partner.getCodiceAnagrafica()%>"
					label="<%=partner.getRagioneSociale1()%>" />
				<%
					}
				%>
			</aui:select>
            <aui:button-row>
				<aui:button name="searchBtn" type="submit" value="search" />
				<aui:button type="cancel" onclick="this.form.reset()" />
			</aui:button-row>
		</aui:column>
	</aui:layout> 
</aui:form>
<div>
	<p id="output">Scanning...</p>
</div>

<script src="https://cdn.jsdelivr.net/npm/jsqr@1.4.0/dist/jsQR.js"></script>
<script>
AUI().use('node', 'event', function(A) {
	const form = A.one('#<portlet:namespace/>fm');
	const orderId = A.one('#<portlet:namespace/>order_id');
	const supplierId = A.one('#<portlet:namespace/>supplier_id');
	const orderNum = A.one('#<portlet:namespace/>order_num');
	const supplier = A.one('#<portlet:namespace/>supplier');
	const beep = A.one('#eventSound').getDOMNode();
    const video = A.one('#video').getDOMNode();
    const canvas = A.one('#canvas').getDOMNode();
    const overlay = A.one('#overlay').getDOMNode();
    const canvasContext = canvas.getContext('2d');
    const overlayContext = overlay.getContext('2d');
    const output = A.one('#output');
    const pauseButton = A.one('#pause');
    const resumeButton = A.one('#resume');
    var scanning = true;

    if (navigator && navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia({ video: { facingMode: 'environment' } })
            .then(function(stream) {
                video.srcObject = stream;
                video.setAttribute('playsinline', true); // Required for iPhone compatibility
                video.play();
                requestAnimationFrame(tick);
            }, function(err) {
            	output.setHTML('Error accessing camera: ' + err.message);
            });
    } else {
        output.setHTML('getUserMedia is not supported by this browser.');
    }

    pauseButton.on('click', function() {
        scanning = false;
        video.pause();
        pauseButton.set('disabled', true);
        resumeButton.set('disabled', false);
    });

    resumeButton.on('click', function() {
        scanning = true;
        video.play();
        requestAnimationFrame(tick);
        pauseButton.set('disabled', false);
        resumeButton.set('disabled', true);
    });

    A.on('windowresize', function() {
        if (video.readyState === video.HAVE_ENOUGH_DATA) {
            canvas.height = video.videoHeight;
            canvas.width = video.videoWidth;
            overlay.height = video.videoHeight;
            overlay.width = video.videoWidth;
        }
    });
    
    function tick() {
    	if (video.readyState === video.HAVE_ENOUGH_DATA && scanning) {
    		canvas.height = video.videoHeight;
    		canvas.width = video.videoWidth;
    		overlay.height = video.videoHeight;
    		overlay.width = video.videoWidth;

    		canvasContext.drawImage(video, 0, 0, canvas.width, canvas.height);
    		const
    		imageData = canvasContext.getImageData(0, 0, canvas.width,
    				canvas.height);
    		const
    		code = jsQR(imageData.data, imageData.width, imageData.height, {
    			inversionAttempts : 'dontInvert',
    		});

    		overlayContext.clearRect(0, 0, overlay.width, overlay.height);

    		if (code && isJsonString(code.data)) {
    			beep.play();
    			output.setHTML('');

    			// Draw bounding box
    			drawLine(overlayContext, code.location.topLeftCorner,
    					code.location.topRightCorner);
    			drawLine(overlayContext, code.location.topRightCorner,
    					code.location.bottomRightCorner);
    			drawLine(overlayContext, code.location.bottomRightCorner,
    					code.location.bottomLeftCorner);
    			drawLine(overlayContext, code.location.bottomLeftCorner,
    					code.location.topLeftCorner);
    			scanning = false;
    	        video.pause();
    	        pauseButton.set('disabled', true);
    	        resumeButton.set('disabled', false);
    	        var obj = JSON.parse(code.data);
    	        orderId.set('value', obj.id_ordine);
    	        if (obj.num_ordine) {
    	        	orderNum.set('value', obj.num_ordine);
				}
    	        supplierId.set('value', obj.id_fornitore);
    	        if (obj.fornitore) {
    	        	supplier.set('value', obj.fornitore);
				}
    	        submitForm(form);
    		} else {
    			output.setHTML('Scanning...');
    			orderId.set('value', '');
    	        orderNum.set('value', '');
    	        supplierId.set('value', '');
    	        supplier.set('value', '');
    		}
    	}
    	if (scanning) {
    		requestAnimationFrame(tick);
    	}
    }

    function isJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
        	console.log(e);
            return false;
        }
        return true;
    }
    
    function drawLine(context, begin, end) {
    	context.beginPath();
    	context.moveTo(begin.x, begin.y);
    	context.lineTo(end.x, end.y);
    	context.lineWidth = 4;
    	context.strokeStyle = 'red';
    	context.stroke();
    }
    
});
</script>