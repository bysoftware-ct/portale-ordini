<%@page import="it.bysoftware.ct.model.Riga"%>
<%@page import="it.bysoftware.ct.model.Piante"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="../init.jsp"%>

<%
	ResultRow row = (ResultRow) request
			.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Riga riga = (Riga) row.getObject();
	String id = String.valueOf(row.getParameter("order_id"));
	String supplier_id = String
			.valueOf(row.getParameter("supplier_id"));
	String order_num = String.valueOf(row.getParameter("order_num"));
	String backUrl = String.valueOf(row.getParameter("backUrl"));
	String cart_id = String.valueOf(row.getParameter("cart_id"));
%>
<liferay-portlet:actionURL name="saveReceivedItems"
	var="saveReceivedItems">
	<liferay-portlet:param name="rowId"
		value="<%=String.valueOf(riga.getId())%>" />
	<liferay-portlet:param name="order_id" value="<%=id%>" />
	<liferay-portlet:param name="supplier_id" value="<%=supplier_id%>" />
	<liferay-portlet:param name="order_num" value="<%=order_num%>" />
	<liferay-portlet:param name="cart_id" value="<%=cart_id%>" />
	<liferay-portlet:param name="backUrl" value="<%=backUrl%>" />
</liferay-portlet:actionURL>
<liferay-ui:icon-menu>
	<c:choose>
		<c:when test="<%=riga.getRicevuto()%>">
			<liferay-ui:icon image="undo" url="${saveReceivedItems}"
				message="cancel" />
		</c:when>
		<c:otherwise>
			<liferay-ui:icon image="check" url="${saveReceivedItems}"
				message="receive-items" />
		</c:otherwise>
	</c:choose>
</liferay-ui:icon-menu>