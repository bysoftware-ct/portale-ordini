package it.bysoftware.ct;

import it.bysoftware.ct.model.Piante;
import it.bysoftware.ct.service.PianteLocalServiceUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class DefaultPassportPortlet
 */
public class DefaultPassportPortlet extends MVCPortlet {

	/**
	 * Exception arguments constant.
	 */
	private static final String EXCEPTION_ARGS = "exceptionArgs";

	/**
	 * Logger object. Based on Liferay logger
	 * {@link com.liferay.portal.kernel.log.Log} commons logging.
	 */
	private Log logger = LogFactoryUtil.getLog(DefaultPassportPortlet.class);

	/**
	 * Delete a default passport.
	 * 
	 * @param areq
	 *            {@link ActionRequest}
	 * @param ares
	 *            {@link ActionResponse}
	 * @throws SystemException
	 *             exception
	 */
	public final void deleteDefaultPassport(final ActionRequest areq,
			final ActionResponse ares) throws SystemException {
		if (Validator.isNotNull(ParamUtil.getLong(areq, "piantaId"))) {
			long itemId = ParamUtil.getLong(areq, "piantaId");
			this.logger.info("Deleting item id: " + itemId);
			try {
				this.setDefaultPassport(itemId, "");
			} catch (PortalException e) {
				e.printStackTrace();
				SessionMessages.add(areq, "delete-default-passport");
				ares.setRenderParameter(DefaultPassportPortlet.EXCEPTION_ARGS,
						e.getMessage());
			}
		} else {
			SessionMessages.add(areq, "delete-default-passport");
			ares.setRenderParameter(DefaultPassportPortlet.EXCEPTION_ARGS,
					"Invalid id");
		}
		PortalUtil.copyRequestParameters(areq, ares);
		ares.setRenderParameter("jspPage",
				"/html/defaultpassport/default-passport-list.jsp");
	}

	/**
	 * Delete a default passport.
	 * 
	 * @param areq
	 *            {@link ActionRequest}
	 * @param ares
	 *            {@link ActionResponse}
	 * @throws SystemException
	 *             exception
	 */
	public final void saveDefaultPassport(final ActionRequest areq,
			final ActionResponse ares) throws SystemException {
		if (Validator.isNotNull(ParamUtil.getLong(areq, "piantaId"))) {
			long itemId = ParamUtil.getLong(areq, "piantaId");
			String defaultPasport = ParamUtil.getString(areq,
					"default-passport");
			this.logger.info("Saving default passport for item id: " + itemId);
			try {
				this.setDefaultPassport(itemId, defaultPasport);
			} catch (PortalException e) {
				e.printStackTrace();
				SessionMessages.add(areq, "delete-default-passport");
				ares.setRenderParameter(DefaultPassportPortlet.EXCEPTION_ARGS,
						e.getMessage());
			}
		} else {
			SessionMessages.add(areq, "delete-default-passport");
			ares.setRenderParameter(DefaultPassportPortlet.EXCEPTION_ARGS,
					"Invalid id");
		}
        PortalUtil.copyRequestParameters(areq, ares);
		ares.setRenderParameter("jspPage",
				"/html/defaultpassport/set-default-passport.jsp");
	}

	private void setDefaultPassport(long itemId, String defaultPasport)
			throws SystemException, PortalException {
		Piante p = PianteLocalServiceUtil.getPiante(itemId);
		p.setPassaportoDefault(defaultPasport);
		PianteLocalServiceUtil.updatePiante(p);
	}
}
