package it.bysoftware.ct;

import it.bysoftware.ct.utils.Report;
import it.bysoftware.ct.utils.ResourceId;
import it.bysoftware.ct.utils.Response;
import it.bysoftware.ct.utils.Response.Code;
import it.bysoftware.ct.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.portlet.documentlibrary.util.DLUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.liferay.util.portlet.PortletProps;

/**
 * Portlet implementation class DailyReport
 */
public class DailyReport extends MVCPortlet {

	/**
	 * Logger object. Based on Liferay logger
	 * {@link com.liferay.portal.kernel.log.Log} commons logging.
	 */
	private Log logger = LogFactoryUtil.getLog(DailyReport.class);

	/**
	 * Give document type constant.
	 */
	public static final String DDM = PortletProps.get("give-document");

	/**
	 * Data source connection.
	 */
	private DataSource ds;

	@Override
	public final void init(final PortletConfig config) throws PortletException {
		Context ctx;
		try {
			ctx = new InitialContext();
			this.ds = (DataSource) ctx.lookup("jdbc/OrdiniPool");
		} catch (NamingException e) {
			this.logger.fatal(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
		}
		super.init(config);
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		String resourceID = resourceRequest.getResourceID();
		PrintWriter out;
		Response response;
		switch (ResourceId.valueOf(resourceID)) {
		case printDailyReport:
			this.logger.debug("Printing daily report label.");
			response = this.printDailyReport(resourceRequest);
			if (response.getCode() == Code.OK.ordinal()) {
				File f = new File(response.getMessage());
				ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest
						.getAttribute(WebKeys.THEME_DISPLAY);
				User company = themeDisplay.getUser();
				long groupId = themeDisplay.getLayout().getGroupId();
				long repoId = themeDisplay.getScopeGroupId();
				DLFolder companyFolder;
				try {
					companyFolder = Utils.getAziendaFolder(groupId, company);
					DLFolder itemSheetFolder = Utils.getPicturesFolder(groupId,
							companyFolder);
					FileEntry fileEntry = DLAppServiceUtil.addFileEntry(repoId,
							itemSheetFolder.getFolderId(), f.getName(),
							MimeTypesUtil.getContentType(f), f.getName(), "",
							"", f, new ServiceContext());
					this.logger.debug("Added: " + fileEntry.getTitle()
							+ " to: /" + companyFolder.getName());
					response.setMessage(DLUtil.getPreviewURL(fileEntry,
							fileEntry.getFileVersion(), themeDisplay, ""));
				} catch (PortalException e) {
					this.logger.error(e.getMessage());
					if (this.logger.isDebugEnabled()) {
						e.printStackTrace();
					}
				} catch (SystemException e) {
					this.logger.error(e.getMessage());
					if (this.logger.isDebugEnabled()) {
						e.printStackTrace();
					}
				}
			}
			out = resourceResponse.getWriter();
			out.print(JSONFactoryUtil.looseSerialize(response));
			out.flush();
			out.close();
			break;
		default:
			break;
		}
	}

	private Response printDailyReport(ResourceRequest resourceRequest) {
		long dateLng = ParamUtil.getLong(resourceRequest, "date", 0);
		this.logger.info(dateLng);
		String partnerCode = ParamUtil.getString(resourceRequest,
				"partnerCode", "");
		this.logger.info(partnerCode);
		String message = null;
		Code c = null;
		if (dateLng > 0 && !partnerCode.isEmpty()) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(dateLng);
			Map<String, Object> parametersMap = new HashMap<String, Object>();
	        parametersMap.put("RfiAnno", calendar.get(Calendar.YEAR));
	        parametersMap.put("RfiCodatt", PortletProps.get("activity-code"));
	        parametersMap.put("RfiCodcen", PortletProps.get("center-code"));
	        parametersMap.put("RfiCoddep", PortletProps.get("storage-code"));
	        parametersMap.put("RfiTipdoc", DailyReport.DDM);
	        parametersMap.put("RfiCodfor", partnerCode);
	        parametersMap.put("RfiDatreg", calendar.getTime());
	        parametersMap.put(JRParameter.REPORT_LOCALE, new Locale("it", "IT"));
			try {
				Report report = new Report(this.ds.getConnection());
				User user = UserLocalServiceUtil.getUser(Long
						.parseLong(resourceRequest.getRemoteUser()));
				String filePath = report.print(user.getUserId(), "",
						parametersMap, "partner_daily");
				report.closeConnection();

				c = Response.Code.OK;
				message = filePath;
			} catch (SQLException e) {
				this.logger.error(e.getMessage());
//				if (this.logger.isDebugEnabled()) {
					e.printStackTrace();
//				}
				c = Response.Code.GENERIC_ERROR;
				message = e.getMessage();
			} catch (NumberFormatException e) {
				this.logger.error(e.getMessage());
//				if (this.logger.isDebugEnabled()) {
					e.printStackTrace();
//				}
				c = Response.Code.GENERIC_ERROR;
				message = e.getMessage();
			} catch (PortalException e) {
				this.logger.error(e.getMessage());
//				if (this.logger.isDebugEnabled()) {
					e.printStackTrace();
//				}
				c = Response.Code.GENERIC_ERROR;
				message = e.getMessage();
			} catch (SystemException e) {
				this.logger.error(e.getMessage());
//				if (this.logger.isDebugEnabled()) {
					e.printStackTrace();
//				}
				c = Response.Code.GENERIC_ERROR;
				message = e.getMessage();
			} catch (JRException e) {
				this.logger.error(e.getMessage());
//				if (this.logger.isDebugEnabled()) {
					e.printStackTrace();
//				}
				c = Response.Code.GENERIC_ERROR;
				message = e.getMessage();
			}
		} else {
			c = Response.Code.GENERIC_ERROR;
			message = "Data o socio non validi";
		}

		return new Response(c, message);
	}

}
