package it.bysoftware.ct;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.portlet.documentlibrary.util.DLUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.liferay.util.portlet.PortletProps;

import it.bysoftware.ct.model.AnagraficheClientiFornitori;
import it.bysoftware.ct.model.ClientiFornitoriDatiAgg;
import it.bysoftware.ct.model.Pagamento;
import it.bysoftware.ct.model.RigoDocumento;
import it.bysoftware.ct.model.ScadenzePartite;
import it.bysoftware.ct.model.SchedaPagamento;
import it.bysoftware.ct.model.SpettanzeSoci;
import it.bysoftware.ct.model.TestataDocumento;
import it.bysoftware.ct.model.TestataFattureClienti;
import it.bysoftware.ct.model.VociIva;
import it.bysoftware.ct.orderexport.EntryExporter;
import it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil;
import it.bysoftware.ct.service.PagamentoLocalServiceUtil;
import it.bysoftware.ct.service.RigoDocumentoLocalServiceUtil;
import it.bysoftware.ct.service.ScadenzePartiteLocalServiceUtil;
import it.bysoftware.ct.service.SchedaPagamentoLocalServiceUtil;
import it.bysoftware.ct.service.SpettanzeSociLocalServiceUtil;
import it.bysoftware.ct.service.TestataDocumentoLocalServiceUtil;
import it.bysoftware.ct.service.TestataFattureClientiLocalServiceUtil;
import it.bysoftware.ct.service.VociIvaLocalServiceUtil;
import it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK;
import it.bysoftware.ct.service.persistence.PagamentoPK;
import it.bysoftware.ct.service.persistence.ScadenzePartitePK;
import it.bysoftware.ct.service.persistence.SchedaPagamentoPK;
import it.bysoftware.ct.service.persistence.TestataFattureClientiPK;
import it.bysoftware.ct.utils.Constants;
import it.bysoftware.ct.utils.EntryComparator;
import it.bysoftware.ct.utils.PaymentState;
import it.bysoftware.ct.utils.Report;
import it.bysoftware.ct.utils.ResourceId;
import it.bysoftware.ct.utils.Response;
import it.bysoftware.ct.utils.Utils;
import it.bysoftware.ct.utils.Response.Code;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;

/**
 * Portlet implementation class PartnersPaymentPortlet.
 */
public class PartnersPaymentPortlet extends MVCPortlet {

    /**
     * Properties file path.
     */
    private static String propertiesFile;
    
    /**
     * Exception arguments constant.
     */
    private static final String EXCEPTION_ARGS = "exceptionArgs";
    
    /**
     * Export folder path.
     */
    private static final String EXPORT_FOLDER = PortletProps.get(
            "export-folder");
    
    /**
     * Exported order file name.
     */
    private static final String EXPORTED_ENTRIES = PortletProps.get(
            "exported-entries");
    
    /**
     * Exported order file name.
     */
    private static final String REWARDED_ENTRIES = PortletProps.get(
            "rewarded-entries");
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(PartnersPaymentPortlet.class);

    /**
     * Data source connection.
     */
    private DataSource ds;
    
    /**
     * Initialises portlet's configuration with pilot-script file path.
     * @throws PortletException exception
     * 
     * @see com.liferay.util.bridges.mvc.MVCPortlet#init()
     */
    @Override
    public final void init() throws PortletException {
    	Context ctx;
        try {
            ctx = new InitialContext();
            this.ds = (DataSource) ctx.lookup("jdbc/OrdiniPool");
        } catch (NamingException e) {
            this.logger.fatal(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        super.init();
        PartnersPaymentPortlet.setPropertiesFile(getPortletContext().
                getRealPath(File.separator) + "WEB-INF/classes/"
                + getInitParameter("properties-file"));
    }
    
    @Override
    public final void serveResource(final ResourceRequest resourceRequest,
            final ResourceResponse resourceResponse) throws IOException,
            PortletException {
        String resourceID = resourceRequest.getResourceID();
        PrintWriter out;
        Response response;
        switch (ResourceId.valueOf(resourceID)) {
            case selectEntries:
                this.logger.debug("Select Entries called...");
                response = this.selectEntries(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case selectInvoices:
                this.logger.debug("Select invoices called...");
                response = this.selectInvoices(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case printCredits:
            	this.logger.info("Printing credits.");
                response = this.printCredits(resourceRequest);
                if (response.getCode() == Code.OK.ordinal()) {
                    File f = new File(response.getMessage());
                    ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.
                            getAttribute(WebKeys.THEME_DISPLAY);
                    User company = themeDisplay.getUser();
                    long groupId = themeDisplay.getLayout().getGroupId();
                    long repoId = themeDisplay.getScopeGroupId();
                    DLFolder companyFolder;
                    try {
                        companyFolder = Utils.getAziendaFolder(groupId,
                                company);
                        DLFolder itemSheetFolder = Utils.getPicturesFolder(
                                groupId, companyFolder);
                        FileEntry fileEntry = DLAppServiceUtil.addFileEntry(
                                repoId, itemSheetFolder.getFolderId(),
                                f.getName(), MimeTypesUtil.getContentType(f),
                                f.getName(), "", "", f, new ServiceContext());
                        this.logger.debug("Added: " + fileEntry.getTitle()
                                + " to: /" + companyFolder.getName());
                        response.setMessage(DLUtil.getPreviewURL(fileEntry,
                                fileEntry.getFileVersion(), themeDisplay, ""));
                    } catch (PortalException e) {
                        this.logger.error(e.getMessage());
                        if (this.logger.isDebugEnabled()) {
                            e.printStackTrace();                        
                        }
                    } catch (SystemException e) {
                        this.logger.error(e.getMessage());
                        if (this.logger.isDebugEnabled()) {
                            e.printStackTrace();                        
                        }
                    }
                }
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            default:
                break;
        }
    }

	private Response printCredits(ResourceRequest resourceRequest) {
		String partnerCode = ParamUtil.getString(resourceRequest,
				"partnerCode", null);
		this.logger.info("partnerCode: " + partnerCode);
		Date from = new Date(ParamUtil.getLong(resourceRequest, "from"));
		this.logger.info("FROM: " + from);
		Date to = new Date(ParamUtil.getLong(resourceRequest, "to"));
		this.logger.info("to: " + to);
		String message = null;
		Code c = null;
		if (partnerCode != null) {
			try {
				AnagraficheClientiFornitori partner =
						AnagraficheClientiFornitoriLocalServiceUtil.getAnagraficheClientiFornitori(
								partnerCode);
				Report report = new Report(this.ds.getConnection());
				User user = UserLocalServiceUtil.getUser(Long
						.parseLong(resourceRequest.getRemoteUser()));
				String filePath = report.print(user.getUserId(),
						user.getScreenName(), "partner-credits", partner.getCodiceAnagrafica(),
						from, to, partner.getRagioneSociale1() + " "
						+ partner.getRagioneSociale2());
				report.closeConnection();
				c = Response.Code.OK;
				message = filePath;
			} catch (SQLException e) {
				this.logger.error(e.getMessage());
				if (this.logger.isDebugEnabled()) {
					e.printStackTrace();
				}
				c = Response.Code.GENERIC_ERROR;
				message = e.getMessage();
			} catch (NumberFormatException e) {
				this.logger.error(e.getMessage());
				if (this.logger.isDebugEnabled()) {
					e.printStackTrace();
				}
				c = Response.Code.GENERIC_ERROR;
				message = e.getMessage();
			} catch (PortalException e) {
				this.logger.error(e.getMessage());
				if (this.logger.isDebugEnabled()) {
					e.printStackTrace();
				}
				c = Response.Code.GENERIC_ERROR;
				message = e.getMessage();
			} catch (SystemException e) {
				this.logger.error(e.getMessage());
				if (this.logger.isDebugEnabled()) {
					e.printStackTrace();
				}
				c = Response.Code.GENERIC_ERROR;
				message = e.getMessage();
			} catch (JRException e) {
				this.logger.error(e.getMessage());
				if (this.logger.isDebugEnabled()) {
					e.printStackTrace();
				}
				c = Response.Code.GENERIC_ERROR;
				message = e.getMessage();
			}
		} else {
            c = Response.Code.GENERIC_ERROR;
            message = "NO_SUBJECT_SPECIFIED";
		}
		return new Response(c, message);
	}

	/**
     * Generate payments.
     * 
     * @param areq
     *            {@link ActionRequest}
     * @param ares
     *            {@link ActionResponse}
     */
    public final void generate(final ActionRequest areq,
            final ActionResponse ares) {
        String fromDateString = ParamUtil.getString(areq, "fromDate");
        String toDateString = ParamUtil.getString(areq, "toDate");
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date fromDate = sdf.parse(fromDateString);
            this.logger.debug("FROM DATE " + sdf.format(fromDate));
            Date toDate = sdf.parse(toDateString);
            this.logger.debug("TO DATE " + sdf.format(toDate));
            String entriesStr = ParamUtil.getString(areq, "documentIds", "{}");
            
            List<ScadenzePartite> entries =
                    this.retrieveSelectedEntries(entriesStr);
            Map<TestataFattureClienti, Map<String, Double>> partnersCredits =
                    this.computeCredits(entries);
            
            this.storeCredits(partnersCredits);
            
            ares.setRenderParameter("jspPage",
                    "/html/partnerspayment/view-generated-credits.jsp");
        } catch (ParseException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            SessionErrors.add(areq, "error-generating-payments",
                    e.getMessage());
            ares.setRenderParameter(PartnersPaymentPortlet.EXCEPTION_ARGS,
                    e.getMessage());
            ares.setRenderParameter("jspPage",
                    "/html/partnerspayment/view.jsp");
        } catch (PortalException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            SessionErrors.add(areq, "error-generating-payments",
                    e.getMessage());
            ares.setRenderParameter(PartnersPaymentPortlet.EXCEPTION_ARGS,
                    e.getMessage());
            ares.setRenderParameter("jspPage",
                    "/html/partnerspayment/view.jsp");
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            SessionErrors.add(areq, "error-generating-payments");
            ares.setRenderParameter(PartnersPaymentPortlet.EXCEPTION_ARGS,
                    e.getMessage());
            ares.setRenderParameter("jspPage",
                    "/html/partnerspayment/view.jsp");
        }

    }
    
    /**
     * Save payments.
     * 
     * @param areq
     *            {@link ActionRequest}
     * @param ares
     *            {@link ActionResponse}
     */
    public final void save(final ActionRequest areq,
            final ActionResponse ares) {
        
        try {
            JSONObject selectedEntries = JSONFactoryUtil.createJSONObject(
                    ParamUtil.getString(areq, "entryIds", "{}"));
            String partnercode = ParamUtil.getString(areq, "partnerCode");
            this.logger.info("Selected entries: " + selectedEntries);
            if (selectedEntries.length() > 0) {
                JSONArray entriesJson = selectedEntries.getJSONArray("entries");
                try {
                    int idPayment = PagamentoLocalServiceUtil.getNumber(
                            Utils.getYear(), partnercode);
                    List<SchedaPagamento> creditBoards =
                            new ArrayList<SchedaPagamento>();
                    for (int i = 0; i < entriesJson.length(); i++) {
                        JSONObject entryJson = entriesJson.getJSONObject(i);
                        ScadenzePartitePK entryPK = JSONFactoryUtil.
                                looseDeserialize(entryJson.
                                        getJSONObject("entryPK").toString(),
                                        ScadenzePartitePK.class);
                        SchedaPagamento creditBoard =
                                SchedaPagamentoLocalServiceUtil.
                                    fetchSchedaPagamento(new SchedaPagamentoPK(
                                        entryPK.getEsercizioRegistrazione(),
                                        entryPK.getCodiceSoggetto(),
                                        entryPK.getNumeroPartita(),
                                        entryPK.getNumeroScadenza(),
                                        entryPK.getTipoSoggetto()));
                        if (creditBoard == null) {
                            creditBoard = SchedaPagamentoLocalServiceUtil.
                                    createSchedaPagamento(new SchedaPagamentoPK(
                                            entryPK.getEsercizioRegistrazione(),
                                            entryPK.getCodiceSoggetto(), 
                                            entryPK.getNumeroPartita(),
                                            entryPK.getNumeroScadenza(),
                                            entryPK.getTipoSoggetto()));
                        }
                        if (entryJson.has("ref")) {
                            int ref = entryJson.getInt("ref");
                            for (int j = 0; j < entriesJson.length(); j++) {
                                JSONObject entryJson1 = entriesJson.
                                        getJSONObject(j);
                                if (entryJson1.getInt("id") == ref) {
                                    ScadenzePartitePK entryPK1 =
                                            JSONFactoryUtil.looseDeserialize(
                                                    entryJson1.
                                                    getJSONObject("entryPK").
                                                        toString(),
                                                    ScadenzePartitePK.class);
                                    creditBoard.setEsercizioRegistrazioneComp(
                                            entryPK1.
                                                getEsercizioRegistrazione());
                                    creditBoard.setCodiceSoggettoComp(
                                            entryPK1.getCodiceSoggetto());
                                    creditBoard.setNumeroPartitaComp(
                                            entryPK1.getNumeroPartita());
                                    creditBoard.setNumeroScadenzaComp(
                                            entryPK1.getNumeroScadenza());
                                    creditBoard.setTipoSoggettoComp(
                                            entryPK1.getTipoSoggetto());
                                }
                            }
                        } else {
                            creditBoard.setEsercizioRegistrazioneComp(0);
                            creditBoard.setCodiceSoggettoComp("");
                            creditBoard.setNumeroPartitaComp(0);
                            creditBoard.setNumeroScadenzaComp(0);
                            creditBoard.setTipoSoggettoComp(false);
                        }
                        if (entryJson.has("diff") && !entryJson.getString(
                                "diff").isEmpty() && Double.parseDouble(
                                        entryJson.getString("diff")) > 0) {
                            creditBoard.setDifferenza(Double.parseDouble(
                                    entryJson.getString("diff")));
                        }
                        creditBoard.setIdPagamento(idPayment);
                        creditBoard.setAnnoPagamento(Utils.getYear());
                        creditBoards.add(creditBoard);
                    }
                    for (SchedaPagamento creditBoard : creditBoards) {
                        SchedaPagamentoLocalServiceUtil.updateSchedaPagamento(
                              creditBoard);
                    }
                    
                    Pagamento payment = PagamentoLocalServiceUtil.
                            createPagamento(new PagamentoPK(idPayment, Utils.
                                    getYear(), partnercode));
                    payment.setDataCreazione(new Date());
                    payment.setCredito(ParamUtil.getDouble(areq, "credit"));
                    payment.setImporto(ParamUtil.getDouble(areq,
                            "payedAmount"));
                    payment.setSaldo(ParamUtil.getDouble(areq, "newBalance"));
                    PagamentoLocalServiceUtil.updatePagamento(payment);
                    
                    List<SpettanzeSoci> list = SpettanzeSociLocalServiceUtil.
                            findByPartnerState(partnercode, 0);
                    for (SpettanzeSoci partnerCredit : list) {
                        partnerCredit.setStato(1);
                        partnerCredit.setIdPagamento(payment.getId());
                        partnerCredit.setAnnoPagamento(payment.getAnno());
                        SpettanzeSociLocalServiceUtil.updateSpettanzeSoci(
                                partnerCredit);
                    }
                    ares.setRenderParameter("year",
                            String.valueOf(payment.getAnno()));
                    ares.setRenderParameter("partnerCode",
                            payment.getCodiceSoggetto());
                    ares.setRenderParameter("paymentId",
                            String.valueOf(payment.getId()));
                    ares.setRenderParameter("paymentYear",
                            String.valueOf(payment.getAnno()));
                } catch (SystemException e) {
                    this.logger.error(e.getMessage());
                    if (this.logger.isDebugEnabled()) {
                        e.printStackTrace();
                    }
                    SessionErrors.add(areq, "error-saving", e.getMessage());
                }
            } else {
                this.logger.warn("Empty selection!!!");
                SessionErrors.add(areq, "empty-selection");
            }
        } catch (JSONException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            SessionErrors.add(areq, "parse-error");
        }
        
        PortalUtil.copyRequestParameters(areq, ares);
        
        ares.setRenderParameter("jspPage",
                "/html/partnerspayment/view-payment-board.jsp");
    }
    
    /**
     * Saves the payments selected by the user as payed.
     * 
     * @param areq
     *            {@link ActionRequest}
     * @param ares
     *            {@link ActionResponse}
     */
    public final void payPartner(final ActionRequest areq,
            final ActionResponse ares) {
        
        try {
            int year = ParamUtil.getInteger(areq, "year", 0);
            int paymentId = ParamUtil.getInteger(areq, "paymentId", 0);
            String partnerCode = ParamUtil.getString(areq, "partnerCode");
            Pagamento payment = PagamentoLocalServiceUtil.getPagamento(
                    new PagamentoPK(paymentId, year, partnerCode));
            JSONArray selectedBoards = JSONFactoryUtil.createJSONArray(
                    ParamUtil.getString(areq, "selectedBoards"));
            this.logger.debug(payment);
            this.logger.debug(selectedBoards);
            String movements = "";
            String rewards = "";
            for (int i = 0; i < selectedBoards.length(); i++) {
                JSONObject selectedBoard = selectedBoards.getJSONObject(i);
                if (selectedBoard.getBoolean("payed")) {
                    SchedaPagamentoPK boardPK = JSONFactoryUtil.
                            looseDeserializeSafe(selectedBoard.getJSONObject(
                                    "paymentBoardPK").toString(),
                                    SchedaPagamentoPK.class);
                    SchedaPagamento board = SchedaPagamentoLocalServiceUtil.
                            getSchedaPagamento(boardPK);
                    board.setStato(PaymentState.PAYED.getVal());
                    board.setNota(selectedBoard.getString("note"));
                    JSONObject boardCompPKJson = selectedBoard.getJSONObject(
                            "paymentBoardCompPK");
                    if (boardCompPKJson.length() > 0) {
                        this.logger.info(boardCompPKJson.toString());
                        SchedaPagamentoPK boardCompPK = JSONFactoryUtil.
                                looseDeserializeSafe(boardCompPKJson.toString(),
                                        SchedaPagamentoPK.class);
                        SchedaPagamento comp = SchedaPagamentoLocalServiceUtil.
                                getSchedaPagamento(boardCompPK);
                        comp.setStato(PaymentState.REWARDED.getVal());
                        comp.setNota(selectedBoard.getString("note"));
                        SchedaPagamentoLocalServiceUtil.updateSchedaPagamento(
                                comp);
                    }
                    
                    String[] tmp = this.exportEntry(board,
                            selectedBoard.getString("subs", ""),
                            selectedBoard.getString("note", "").
                            replace(";", ""));
                    
                    movements += tmp[0];
                    if (tmp.length > 1) {
                        rewards += tmp[1];
                    }
                    SchedaPagamentoLocalServiceUtil.updateSchedaPagamento(
                            board);
                }
            }
            payment.setSaldo(ParamUtil.getDouble(areq, "newBalance"));
            payment.setImporto(ParamUtil.getDouble(areq, "totalPayed"));
            payment.setDataPagamento(new Date());
            payment.setStato(PaymentState.PAYED.getVal());
            PagamentoLocalServiceUtil.updatePagamento(payment);
            List<SpettanzeSoci> entries = SpettanzeSociLocalServiceUtil.
                    findByPartnerState(partnerCode, PaymentState.SENT.getVal());
            for (SpettanzeSoci entry : entries) {
                entry.setStato(PaymentState.PAYED.getVal());
                SpettanzeSociLocalServiceUtil.updateSpettanzeSoci(entry);
            }
            this.logger.debug(movements);
            this.logger.debug(rewards);
            this.writeFiles(movements, rewards);
            ares.setRenderParameter("jspPage",
                    "/html/partnerspayment/view-generated-credits.jsp");
        } catch (JSONException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            SessionErrors.add(areq, "parse-error");
            ares.setRenderParameter("jspPage",
                    "/html/partnerspayment/view-payment-board.jsp");
        } catch (PortalException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            SessionErrors.add(areq, "parse-error");
            ares.setRenderParameter("jspPage",
                    "/html/partnerspayment/view-payment-board.jsp");
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            SessionErrors.add(areq, "parse-error");
            ares.setRenderParameter("jspPage",
                    "/html/partnerspayment/view-payment-board.jsp");
        }
        
        PortalUtil.copyRequestParameters(areq, ares);
        
    }
    
    /**
     * Stores the passport number for the current order.
     * 
     * @param areq
     *            the request {@link ActionRequest}
     * @param ares
     *            the response {@link ActionResponse}
     */
    public final void addPartnerCredit(final ActionRequest areq,
            final ActionResponse ares) {
        String partnerCode = ParamUtil.getString(areq, "partnerCode");
        double creditAmount = ParamUtil.getDouble(areq, "credit");
        String invoiceSelected = ParamUtil.getString(areq, "invoiceSelected",
                null);
        this.logger.info("Adding credit to partner: " + partnerCode);
        SpettanzeSoci credit = SpettanzeSociLocalServiceUtil.
                createSpettanzeSoci(0);
        try {
            if (invoiceSelected != null && !invoiceSelected.isEmpty()) {
                ScadenzePartitePK entryPK = JSONFactoryUtil.looseDeserialize(
                        URLDecoder.decode(invoiceSelected, "UTF-8"),
                        ScadenzePartitePK.class);
                ScadenzePartite entry = ScadenzePartiteLocalServiceUtil.
                        getScadenzePartite(entryPK);
                TestataFattureClientiPK headerPK = new TestataFattureClientiPK(
                        entry.getEsercizioDocumento(),
                        entry.getCodiceAttivita(), entry.getCodiceCentro(),
                        entry.getNumeroDocumento());
                TestataFattureClienti header =
                        TestataFattureClientiLocalServiceUtil.
                        getTestataFattureClienti(headerPK);
                SpettanzeSoci s = SpettanzeSociLocalServiceUtil.
                        findByInvoice(headerPK, partnerCode);
                if (s == null) {
                    credit.setAnno(header.getAnno());
                    credit.setCodiceAttivita(header.getCodiceAttivita());
                    credit.setCodiceCentro(header.getCodiceCentro());
                    credit.setNumeroProtocollo(header.getNumeroProtocollo());
                } else {
                    throw new Exception("used");
                }
            } else {
                credit.setAnno(Utils.getYear());
            }
            
            credit.setCodiceFornitore(partnerCode);
            credit.setImporto(creditAmount);
            credit.setDataCalcolo(new Date());
            credit.setStato(0);
            credit.setIdPagamento(0);
            credit.setAnnoPagamento(Utils.getYear());
        
            SpettanzeSociLocalServiceUtil.updateSpettanzeSoci(credit);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            SessionErrors.add(areq, "error", e.getMessage());
        } catch (PortalException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            SessionErrors.add(areq, "error", e.getMessage());
        } catch (Exception e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            SessionErrors.add(areq, "error", e.getMessage());
        }
        ares.setRenderParameter("jspPage",
                "/html/partnerspayment/credit-details.jsp");
        PortalUtil.copyRequestParameters(areq, ares);
    }

    /**
     * Writes the file containing the exported entries.
     * 
     * @param movements
     *            movements file content
     * @param rewards
     *            rewards file content
     */
    private void writeFiles(final String movements, final String rewards) {
        File exportFolder = new File(EXPORT_FOLDER);
        File exportedEntries = new File(exportFolder + File.separator
                + EXPORTED_ENTRIES);
        FileWriter fw;
        BufferedWriter bw;
        PrintWriter out;
        
        try {
            fw = new FileWriter(exportedEntries.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);
            out = new PrintWriter(bw);
            out.print(movements);
            out.close();
            bw.close();
            fw.close();
            if (!rewards.isEmpty()) {
                File rewardedEntries = new File(exportFolder + File.separator
                        + REWARDED_ENTRIES);
                fw = new FileWriter(rewardedEntries.getAbsoluteFile(), true);
                bw = new BufferedWriter(fw);
                out = new PrintWriter(bw);
                out.print(rewards);
                out.close();
                bw.close();
                fw.close();
            }
        } catch (IOException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        
    }

    /**
     * Writes a file with the payed entry.
     * 
     * @param board
     *            the payment board.
     * @param subAccount
     *            the sub account selected
     * @param note
     *            payment note
     * @return the string representing the exported entry
     * @throws SystemException
     *             exception
     * @throws PortalException
     *             exception
     */
    private String[] exportEntry(final SchedaPagamento board,
            final String subAccount, final String note) throws PortalException,
            SystemException {
        
        ScadenzePartite payingEntry = ScadenzePartiteLocalServiceUtil
                .getScadenzePartite(new ScadenzePartitePK(board
                        .getTipoSoggetto(), board.getCodiceSoggetto(), board
                        .getEsercizioRegistrazione(), board.getNumeroPartita(),
                        board.getNumeroScadenza()));
        ScadenzePartite rewardingEntry = null;
        boolean exportDocument = true;
        if (board.getEsercizioRegistrazioneComp() > 0) {
            exportDocument = board.getDifferenza() > 0;
            rewardingEntry = ScadenzePartiteLocalServiceUtil.getScadenzePartite(
                    new ScadenzePartitePK(board.getTipoSoggettoComp(),
                            board.getCodiceSoggettoComp(),
                            board.getEsercizioRegistrazioneComp(),
                            board.getNumeroPartitaComp(),
                            board.getNumeroScadenzaComp()));
        }
        
        EntryExporter exporter = new EntryExporter(payingEntry, rewardingEntry,
                subAccount, note);
        
        String movs = "";
        
        if (exportDocument) {
            movs = exporter.exportDocument();
        }
        return new String[] {movs, exporter.exportComp()};
    }

    /**
     * Retrieves the user's selected entries.
     * 
     * @param entriesStr
     *            entries selected
     * @return the list of {@link ScadenzePartite} selected
     * @throws SystemException
     *             exception
     * @throws PortalException
     *             exception
     */
    private List<ScadenzePartite> retrieveSelectedEntries(
            final String entriesStr) throws PortalException, SystemException {
        this.logger.debug("GENERATING PAYMENTS FROM INVOICE: " + entriesStr);
        List<ScadenzePartite> entries = new ArrayList<ScadenzePartite>();
        JSONObject tmp = JSONFactoryUtil.createJSONObject(entriesStr);
        JSONArray entriesJson = tmp.getJSONArray("entries");
        for (int i = 0; i < entriesJson.length(); i++) {
            JSONObject entryJson = entriesJson.getJSONObject(i);
            ScadenzePartitePK pk = JSONFactoryUtil.looseDeserializeSafe(
                    entryJson.toString(), ScadenzePartitePK.class);
            entries.add(ScadenzePartiteLocalServiceUtil.getScadenzePartite(pk));
        }
        return entries;
    }

    /**
     * Persists credits into the database.
     * 
     * @param partnersCredits
     *            the computed credits per partner
     * @throws SystemException
     *             exception
     */
    private void storeCredits(final Map<TestataFattureClienti,
            Map<String, Double>> partnersCredits) throws SystemException {
        for (TestataFattureClienti invoice : partnersCredits.keySet()) {
            this.logger.debug("INVOICE: " + invoice);
            Map<String, Double> credits = partnersCredits.get(invoice);

            for (String partner : credits.keySet()) {
                SpettanzeSoci credit = SpettanzeSociLocalServiceUtil.
                        findByInvoice(invoice.getPrimaryKey(), partner);

                if (credit == null) {
                    credit = SpettanzeSociLocalServiceUtil.
                            createSpettanzeSoci(0);
                    credit.setAnno(invoice.getAnno());
                    credit.setCodiceAttivita(invoice.getCodiceAttivita());
                    credit.setCodiceCentro(invoice.getCodiceCentro());
                    credit.setNumeroProtocollo(invoice.getNumeroProtocollo());
                    credit.setCodiceFornitore(partner);
                    credit.setDataCalcolo(new Date());
                    credit.setStato(0);
                    credit.setAnnoPagamento(Utils.getYear());
                }

                credit.setImporto(credits.get(partner));

                SpettanzeSociLocalServiceUtil.updateSpettanzeSoci(credit);
                this.logger.debug("PARTNER: " + partner + " DUE AMOUNT: "
                        + credits.get(partner));
            }
        }
    }

    /**
     * Computes partner credits based on the customer payed invoices.
     * 
     * @param entries
     *            customer payed invoices
     * @return partner credits
     * @throws SystemException
     *             exception
     * @throws PortalException 
     */
    private Map<TestataFattureClienti, Map<String, Double>> computeCredits(
            final List<ScadenzePartite> entries) throws SystemException,
            PortalException {
        Map<TestataFattureClienti, Map<String, Double>> credits;
        credits = new HashMap<TestataFattureClienti, Map<String, Double>>();
        for (ScadenzePartite entry : entries) {
                TestataFattureClienti header =
                        TestataFattureClientiLocalServiceUtil.getInvoice(
                                entry.getEsercizioDocumento(),
                                entry.getCodiceAttivita(),
                                entry.getCodiceCentro(),
                                entry.getNumeroDocumento(),
                                entry.getDataDocumento(), false,
                                entry.getCodiceSoggetto());

                HashMap<String, Double> credit = new HashMap<String, Double>();
                List<TestataDocumento> docHeaders =
                        TestataDocumentoLocalServiceUtil.getDocumentsByOrderId(
                                header.getLibLng1());
                String partnerCode = "";
                double amount = 0.0;
                for (TestataDocumento docHeader : docHeaders) {
                    ClientiFornitoriDatiAgg customerData =
                            ClientiFornitoriDatiAggLocalServiceUtil.
                                getClientiFornitoriDatiAgg(
                                        new ClientiFornitoriDatiAggPK(docHeader.
                                                getCodiceFornitore(), true));
                    if (partnerCode.isEmpty()
                            || !customerData.getCodiceAnagrafica().equals(
                                    partnerCode)) {
                        amount = 0.0;
                        partnerCode = customerData.getCodiceAnagrafica();
                    }
                    VociIva partnerVat = null;
                    if (!customerData.getCodiceEsenzione().isEmpty()) {
                        partnerVat = VociIvaLocalServiceUtil
                                .getVociIva(customerData.getCodiceEsenzione());
                    }

                    List<RigoDocumento> rows = RigoDocumentoLocalServiceUtil
                            .findByHeaderItem(docHeader.getPrimaryKey());
                    for (RigoDocumento row : rows) {
                        if (partnerVat != null) {
                            amount += row.getImportoNetto()
                                    * (1 + partnerVat.getAliquota()
                                            / Constants.HUNDRED);
                        } else {
                            VociIva itemVat = VociIvaLocalServiceUtil
                                    .fetchVociIva(row.getCodiceIVA());
                            if (itemVat == null) {
                                itemVat = VociIvaLocalServiceUtil
                                        .fetchVociIva("1");
                            }
                            amount += row.getImportoNetto()
                                    * (1 + itemVat.getAliquota()
                                            / Constants.HUNDRED);
                        }
                    }
                    if (credit.containsKey(docHeader
                            .getCodiceFornitore())) {
                        amount += credit.get(docHeader
                                .getCodiceFornitore());
                    }
                    credit.put(docHeader.getCodiceFornitore(), amount);

                }
                credits.put(header, credit);
        }
        return credits;
    }

    /**
     * Returns the entries selected by the user.
     * 
     * @param rreq
     *            {@link}
     * @return entries selected by the user
     */
    private Response selectEntries(final ResourceRequest rreq) {
        String data = ParamUtil.getString(rreq, "data");
        this.logger.debug(data);
        JSONArray resJson = JSONFactoryUtil.createJSONArray();
        Response res = null;
        try {
            JSONArray entriesJson = JSONFactoryUtil.createJSONObject(data).
                    getJSONArray("entries");
            for (int i = 0; i < entriesJson.length(); i++) {
                ScadenzePartitePK entryPK = JSONFactoryUtil.
                        looseDeserializeSafe(entriesJson.getJSONObject(i)
                                .toString(), ScadenzePartitePK.class);
                resJson.put(JSONFactoryUtil.looseSerialize(
                        ScadenzePartiteLocalServiceUtil.getScadenzePartite(
                                entryPK)));
            }
            res = new Response(Response.Code.OK, resJson.toString());
        } catch (JSONException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            res = new Response(Response.Code.GENERIC_ERROR, e.getMessage());
        } catch (PortalException e) {
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            res = new Response(Response.Code.GENERIC_ERROR, e.getMessage());
        } catch (SystemException e) {
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            res = new Response(Response.Code.GENERIC_ERROR, e.getMessage());
        }
        return res;
    }

    /**
     * Returns the closed invoices for selected customer.
     * 
     * @param rreq
     *            {@link}
     * @return the closed invoices for selected customer
     */
    private Response selectInvoices(final ResourceRequest rreq) {
        Response res = null;
        String customerSelected = ParamUtil.getString(rreq, "customerSelected",
                null);
        this.logger.info(customerSelected);
        if (customerSelected != null && !customerSelected.isEmpty()) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, -1);
            calendar.set(Calendar.DATE, 1);
            Date from = calendar.getTime();
            calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, -1);
            calendar.set(Calendar.DATE, calendar.getActualMaximum(
                    Calendar.DAY_OF_MONTH));
            Date to = calendar.getTime();
            List<ScadenzePartite> tmp;
            try {
                tmp = ScadenzePartiteLocalServiceUtil.
                        getCustomerClosedEntries(customerSelected, from, to, 0,
                                ScadenzePartiteLocalServiceUtil.
                                getCustomerClosedEntriesCount(customerSelected,
                                        from, to), new EntryComparator());
                List<ScadenzePartite> list = new ArrayList<ScadenzePartite>();
                for (ScadenzePartite entry : tmp) {
                    ClientiFornitoriDatiAgg c =
                            ClientiFornitoriDatiAggLocalServiceUtil.
                                fetchClientiFornitoriDatiAgg(
                                        new ClientiFornitoriDatiAggPK(entry.
                                                getCodiceSoggetto(), true));
                    if (c == null) {
                        list.add(entry);
                    }
                }
                res = new Response(Response.Code.OK, JSONFactoryUtil.
                        looseSerialize(list));
            } catch (SystemException e) {
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                res = new Response(Response.Code.GENERIC_ERROR, e.getMessage());
            }
        } else {
            res = new Response(Response.Code.GENERIC_ERROR, "empty-customer");
        }
        return res;
    }
    
    /**
     * Returns the properties file path.
     * 
     * @return the propertiesFile
     */
    public static String getPropertiesFile() {
        return propertiesFile;
    }

    /**
     * Sets properties file path.
     * 
     * @param filepath the propertiesFile to set
     */
    private static void setPropertiesFile(final String filepath) {
        PartnersPaymentPortlet.propertiesFile = filepath;
    }
}
