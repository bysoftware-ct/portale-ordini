package it.bysoftware.ct;


import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.scheduler.SchedulerException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.liferay.util.portlet.PortletProps;

import it.bysoftware.ct.scheduler.service.ForeignInvoiceExporterService;
import it.bysoftware.ct.service.persistence.TestataDocumentoPK;
import it.bysoftware.ct.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.sql.DataSource;

/**
 * Portlet implementation class ImmediateInvoice.
 */
public class ImmediateInvoice extends MVCPortlet {

    /**
     * Export folder path.
     */
    private static final String EXPORT_FOLDER = PortletProps.get(
            "export-folder");
    
    /**
     * Date formatter.
     */
    private static final SimpleDateFormat SDF = new SimpleDateFormat(
            "dd/MM/yyyy");
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(ImmediateInvoice.class);

    /**
     * Triggers invoice creation.
     * 
     * @param areq
     *            {@link ActionRequest}
     * @param ares
     *            {@link ActionResponse}
     * @throws SchedulerException
     *             exception
     */
    public final void createInvoice(final ActionRequest areq,
            final ActionResponse ares) throws SchedulerException {

        long orderId = ParamUtil.getLong(areq, "orderId");
        this.logger.info("order id: " + orderId);
        if (orderId > 0) {
            this.logger.info("#### FOREIGN INVOICE EXPORTER EXECUTED AT: "
                    + SDF.format(new Date()) + "####");

            Calendar today = Utils.today();
            ForeignInvoiceExporterService exporterService =
                    ForeignInvoiceExporterService.getInstance();
            
            exporterService.convertDocument2Invoice(today, orderId);
            Context ctx;
            try {
                ctx = new InitialContext();
                DataSource ds = (DataSource) ctx.lookup("jdbc/OrdiniPool");
                Connection c = ds.getConnection();
                File exportFolder = new File(EXPORT_FOLDER);
                TestataDocumentoPK[] exportedInvoices =
                        exporterService.export(c, exportFolder, today);
                c.close();
                Utils.updateCheckFile(exportFolder, exportedInvoices,
                        "FOREIGN INVOICES");
            } catch (NamingException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
            } catch (SQLException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
            }
                
        }

    }
}
