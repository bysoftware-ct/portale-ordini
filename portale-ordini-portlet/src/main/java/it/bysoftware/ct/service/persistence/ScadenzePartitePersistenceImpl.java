package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchScadenzePartiteException;
import it.bysoftware.ct.model.ScadenzePartite;
import it.bysoftware.ct.model.impl.ScadenzePartiteImpl;
import it.bysoftware.ct.model.impl.ScadenzePartiteModelImpl;
import it.bysoftware.ct.service.persistence.ScadenzePartitePersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the scadenze partite service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see ScadenzePartitePersistence
 * @see ScadenzePartiteUtil
 * @generated
 */
public class ScadenzePartitePersistenceImpl extends BasePersistenceImpl<ScadenzePartite>
    implements ScadenzePartitePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link ScadenzePartiteUtil} to access the scadenze partite persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = ScadenzePartiteImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ScadenzePartiteModelImpl.ENTITY_CACHE_ENABLED,
            ScadenzePartiteModelImpl.FINDER_CACHE_ENABLED,
            ScadenzePartiteImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ScadenzePartiteModelImpl.ENTITY_CACHE_ENABLED,
            ScadenzePartiteModelImpl.FINDER_CACHE_ENABLED,
            ScadenzePartiteImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ScadenzePartiteModelImpl.ENTITY_CACHE_ENABLED,
            ScadenzePartiteModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_SCADENZEPARTITE = "SELECT scadenzePartite FROM ScadenzePartite scadenzePartite";
    private static final String _SQL_COUNT_SCADENZEPARTITE = "SELECT COUNT(scadenzePartite) FROM ScadenzePartite scadenzePartite";
    private static final String _ORDER_BY_ENTITY_ALIAS = "scadenzePartite.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ScadenzePartite exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(ScadenzePartitePersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "tipoSoggetto", "codiceSoggetto", "esercizioRegistrazione",
                "numeroPartita", "numeroScadenza", "dataScadenza",
                "codiceTipoPagam", "stato", "codiceBanca", "codiceContoCorrente",
                "codiceAgente", "tipoCausale", "esercizioDocumento",
                "protocolloDocumento", "codiceAttivita", "codiceCentro",
                "dataRegistrazione", "dataOperazione", "dataAggiornamento",
                "dataChiusura", "codiceCausale", "dataDocumento",
                "numeroDocumento", "codiceTipoPagamOrig", "codiceTipoPagamScad",
                "causaleEstrattoContoOrig", "codiceDivisaOrig",
                "importoTotaleInt", "importoPagatoInt", "importoApertoInt",
                "importoEspostoInt", "importoScadutoInt", "importoInsolutoInt",
                "importoInContenziosoInt", "importoTotale", "importoPagato",
                "importoAperto", "importoEsposto", "importoScaduto",
                "importoInsoluto", "importoInContenzioso",
                "numeroLettereSollecito", "codiceDivisaInterna",
                "codiceEsercizioScadenzaConversione",
                "numeroPartitaScadenzaCoversione", "numeroScadenzaCoversione",
                "tipoCoversione", "codiceEsercizioScritturaConversione",
                "numeroRegistrazioneScritturaConversione",
                "dataRegistrazioneScritturaConversione",
                "codiceDivisaIntScadenzaConversione",
                "codiceDivisaScadenzaConversione", "cambioScadenzaConversione"
            });
    private static ScadenzePartite _nullScadenzePartite = new ScadenzePartiteImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<ScadenzePartite> toCacheModel() {
                return _nullScadenzePartiteCacheModel;
            }
        };

    private static CacheModel<ScadenzePartite> _nullScadenzePartiteCacheModel = new CacheModel<ScadenzePartite>() {
            @Override
            public ScadenzePartite toEntityModel() {
                return _nullScadenzePartite;
            }
        };

    public ScadenzePartitePersistenceImpl() {
        setModelClass(ScadenzePartite.class);
    }

    /**
     * Caches the scadenze partite in the entity cache if it is enabled.
     *
     * @param scadenzePartite the scadenze partite
     */
    @Override
    public void cacheResult(ScadenzePartite scadenzePartite) {
        EntityCacheUtil.putResult(ScadenzePartiteModelImpl.ENTITY_CACHE_ENABLED,
            ScadenzePartiteImpl.class, scadenzePartite.getPrimaryKey(),
            scadenzePartite);

        scadenzePartite.resetOriginalValues();
    }

    /**
     * Caches the scadenze partites in the entity cache if it is enabled.
     *
     * @param scadenzePartites the scadenze partites
     */
    @Override
    public void cacheResult(List<ScadenzePartite> scadenzePartites) {
        for (ScadenzePartite scadenzePartite : scadenzePartites) {
            if (EntityCacheUtil.getResult(
                        ScadenzePartiteModelImpl.ENTITY_CACHE_ENABLED,
                        ScadenzePartiteImpl.class,
                        scadenzePartite.getPrimaryKey()) == null) {
                cacheResult(scadenzePartite);
            } else {
                scadenzePartite.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all scadenze partites.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(ScadenzePartiteImpl.class.getName());
        }

        EntityCacheUtil.clearCache(ScadenzePartiteImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the scadenze partite.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(ScadenzePartite scadenzePartite) {
        EntityCacheUtil.removeResult(ScadenzePartiteModelImpl.ENTITY_CACHE_ENABLED,
            ScadenzePartiteImpl.class, scadenzePartite.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<ScadenzePartite> scadenzePartites) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (ScadenzePartite scadenzePartite : scadenzePartites) {
            EntityCacheUtil.removeResult(ScadenzePartiteModelImpl.ENTITY_CACHE_ENABLED,
                ScadenzePartiteImpl.class, scadenzePartite.getPrimaryKey());
        }
    }

    /**
     * Creates a new scadenze partite with the primary key. Does not add the scadenze partite to the database.
     *
     * @param scadenzePartitePK the primary key for the new scadenze partite
     * @return the new scadenze partite
     */
    @Override
    public ScadenzePartite create(ScadenzePartitePK scadenzePartitePK) {
        ScadenzePartite scadenzePartite = new ScadenzePartiteImpl();

        scadenzePartite.setNew(true);
        scadenzePartite.setPrimaryKey(scadenzePartitePK);

        return scadenzePartite;
    }

    /**
     * Removes the scadenze partite with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param scadenzePartitePK the primary key of the scadenze partite
     * @return the scadenze partite that was removed
     * @throws it.bysoftware.ct.NoSuchScadenzePartiteException if a scadenze partite with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ScadenzePartite remove(ScadenzePartitePK scadenzePartitePK)
        throws NoSuchScadenzePartiteException, SystemException {
        return remove((Serializable) scadenzePartitePK);
    }

    /**
     * Removes the scadenze partite with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the scadenze partite
     * @return the scadenze partite that was removed
     * @throws it.bysoftware.ct.NoSuchScadenzePartiteException if a scadenze partite with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ScadenzePartite remove(Serializable primaryKey)
        throws NoSuchScadenzePartiteException, SystemException {
        Session session = null;

        try {
            session = openSession();

            ScadenzePartite scadenzePartite = (ScadenzePartite) session.get(ScadenzePartiteImpl.class,
                    primaryKey);

            if (scadenzePartite == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchScadenzePartiteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(scadenzePartite);
        } catch (NoSuchScadenzePartiteException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected ScadenzePartite removeImpl(ScadenzePartite scadenzePartite)
        throws SystemException {
        scadenzePartite = toUnwrappedModel(scadenzePartite);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(scadenzePartite)) {
                scadenzePartite = (ScadenzePartite) session.get(ScadenzePartiteImpl.class,
                        scadenzePartite.getPrimaryKeyObj());
            }

            if (scadenzePartite != null) {
                session.delete(scadenzePartite);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (scadenzePartite != null) {
            clearCache(scadenzePartite);
        }

        return scadenzePartite;
    }

    @Override
    public ScadenzePartite updateImpl(
        it.bysoftware.ct.model.ScadenzePartite scadenzePartite)
        throws SystemException {
        scadenzePartite = toUnwrappedModel(scadenzePartite);

        boolean isNew = scadenzePartite.isNew();

        Session session = null;

        try {
            session = openSession();

            if (scadenzePartite.isNew()) {
                session.save(scadenzePartite);

                scadenzePartite.setNew(false);
            } else {
                session.merge(scadenzePartite);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(ScadenzePartiteModelImpl.ENTITY_CACHE_ENABLED,
            ScadenzePartiteImpl.class, scadenzePartite.getPrimaryKey(),
            scadenzePartite);

        return scadenzePartite;
    }

    protected ScadenzePartite toUnwrappedModel(ScadenzePartite scadenzePartite) {
        if (scadenzePartite instanceof ScadenzePartiteImpl) {
            return scadenzePartite;
        }

        ScadenzePartiteImpl scadenzePartiteImpl = new ScadenzePartiteImpl();

        scadenzePartiteImpl.setNew(scadenzePartite.isNew());
        scadenzePartiteImpl.setPrimaryKey(scadenzePartite.getPrimaryKey());

        scadenzePartiteImpl.setTipoSoggetto(scadenzePartite.isTipoSoggetto());
        scadenzePartiteImpl.setCodiceSoggetto(scadenzePartite.getCodiceSoggetto());
        scadenzePartiteImpl.setEsercizioRegistrazione(scadenzePartite.getEsercizioRegistrazione());
        scadenzePartiteImpl.setNumeroPartita(scadenzePartite.getNumeroPartita());
        scadenzePartiteImpl.setNumeroScadenza(scadenzePartite.getNumeroScadenza());
        scadenzePartiteImpl.setDataScadenza(scadenzePartite.getDataScadenza());
        scadenzePartiteImpl.setCodiceTipoPagam(scadenzePartite.getCodiceTipoPagam());
        scadenzePartiteImpl.setStato(scadenzePartite.getStato());
        scadenzePartiteImpl.setCodiceBanca(scadenzePartite.getCodiceBanca());
        scadenzePartiteImpl.setCodiceContoCorrente(scadenzePartite.getCodiceContoCorrente());
        scadenzePartiteImpl.setCodiceAgente(scadenzePartite.getCodiceAgente());
        scadenzePartiteImpl.setTipoCausale(scadenzePartite.getTipoCausale());
        scadenzePartiteImpl.setEsercizioDocumento(scadenzePartite.getEsercizioDocumento());
        scadenzePartiteImpl.setProtocolloDocumento(scadenzePartite.getProtocolloDocumento());
        scadenzePartiteImpl.setCodiceAttivita(scadenzePartite.getCodiceAttivita());
        scadenzePartiteImpl.setCodiceCentro(scadenzePartite.getCodiceCentro());
        scadenzePartiteImpl.setDataRegistrazione(scadenzePartite.getDataRegistrazione());
        scadenzePartiteImpl.setDataOperazione(scadenzePartite.getDataOperazione());
        scadenzePartiteImpl.setDataAggiornamento(scadenzePartite.getDataAggiornamento());
        scadenzePartiteImpl.setDataChiusura(scadenzePartite.getDataChiusura());
        scadenzePartiteImpl.setCodiceCausale(scadenzePartite.getCodiceCausale());
        scadenzePartiteImpl.setDataDocumento(scadenzePartite.getDataDocumento());
        scadenzePartiteImpl.setNumeroDocumento(scadenzePartite.getNumeroDocumento());
        scadenzePartiteImpl.setCodiceTipoPagamOrig(scadenzePartite.getCodiceTipoPagamOrig());
        scadenzePartiteImpl.setCodiceTipoPagamScad(scadenzePartite.getCodiceTipoPagamScad());
        scadenzePartiteImpl.setCausaleEstrattoContoOrig(scadenzePartite.getCausaleEstrattoContoOrig());
        scadenzePartiteImpl.setCodiceDivisaOrig(scadenzePartite.getCodiceDivisaOrig());
        scadenzePartiteImpl.setImportoTotaleInt(scadenzePartite.getImportoTotaleInt());
        scadenzePartiteImpl.setImportoPagatoInt(scadenzePartite.getImportoPagatoInt());
        scadenzePartiteImpl.setImportoApertoInt(scadenzePartite.getImportoApertoInt());
        scadenzePartiteImpl.setImportoEspostoInt(scadenzePartite.getImportoEspostoInt());
        scadenzePartiteImpl.setImportoScadutoInt(scadenzePartite.getImportoScadutoInt());
        scadenzePartiteImpl.setImportoInsolutoInt(scadenzePartite.getImportoInsolutoInt());
        scadenzePartiteImpl.setImportoInContenziosoInt(scadenzePartite.getImportoInContenziosoInt());
        scadenzePartiteImpl.setImportoTotale(scadenzePartite.getImportoTotale());
        scadenzePartiteImpl.setImportoPagato(scadenzePartite.getImportoPagato());
        scadenzePartiteImpl.setImportoAperto(scadenzePartite.getImportoAperto());
        scadenzePartiteImpl.setImportoEsposto(scadenzePartite.getImportoEsposto());
        scadenzePartiteImpl.setImportoScaduto(scadenzePartite.getImportoScaduto());
        scadenzePartiteImpl.setImportoInsoluto(scadenzePartite.getImportoInsoluto());
        scadenzePartiteImpl.setImportoInContenzioso(scadenzePartite.getImportoInContenzioso());
        scadenzePartiteImpl.setNumeroLettereSollecito(scadenzePartite.getNumeroLettereSollecito());
        scadenzePartiteImpl.setCodiceDivisaInterna(scadenzePartite.getCodiceDivisaInterna());
        scadenzePartiteImpl.setCodiceEsercizioScadenzaConversione(scadenzePartite.getCodiceEsercizioScadenzaConversione());
        scadenzePartiteImpl.setNumeroPartitaScadenzaCoversione(scadenzePartite.getNumeroPartitaScadenzaCoversione());
        scadenzePartiteImpl.setNumeroScadenzaCoversione(scadenzePartite.getNumeroScadenzaCoversione());
        scadenzePartiteImpl.setTipoCoversione(scadenzePartite.getTipoCoversione());
        scadenzePartiteImpl.setCodiceEsercizioScritturaConversione(scadenzePartite.getCodiceEsercizioScritturaConversione());
        scadenzePartiteImpl.setNumeroRegistrazioneScritturaConversione(scadenzePartite.getNumeroRegistrazioneScritturaConversione());
        scadenzePartiteImpl.setDataRegistrazioneScritturaConversione(scadenzePartite.getDataRegistrazioneScritturaConversione());
        scadenzePartiteImpl.setCodiceDivisaIntScadenzaConversione(scadenzePartite.getCodiceDivisaIntScadenzaConversione());
        scadenzePartiteImpl.setCodiceDivisaScadenzaConversione(scadenzePartite.getCodiceDivisaScadenzaConversione());
        scadenzePartiteImpl.setCambioScadenzaConversione(scadenzePartite.getCambioScadenzaConversione());

        return scadenzePartiteImpl;
    }

    /**
     * Returns the scadenze partite with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the scadenze partite
     * @return the scadenze partite
     * @throws it.bysoftware.ct.NoSuchScadenzePartiteException if a scadenze partite with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ScadenzePartite findByPrimaryKey(Serializable primaryKey)
        throws NoSuchScadenzePartiteException, SystemException {
        ScadenzePartite scadenzePartite = fetchByPrimaryKey(primaryKey);

        if (scadenzePartite == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchScadenzePartiteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return scadenzePartite;
    }

    /**
     * Returns the scadenze partite with the primary key or throws a {@link it.bysoftware.ct.NoSuchScadenzePartiteException} if it could not be found.
     *
     * @param scadenzePartitePK the primary key of the scadenze partite
     * @return the scadenze partite
     * @throws it.bysoftware.ct.NoSuchScadenzePartiteException if a scadenze partite with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ScadenzePartite findByPrimaryKey(ScadenzePartitePK scadenzePartitePK)
        throws NoSuchScadenzePartiteException, SystemException {
        return findByPrimaryKey((Serializable) scadenzePartitePK);
    }

    /**
     * Returns the scadenze partite with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the scadenze partite
     * @return the scadenze partite, or <code>null</code> if a scadenze partite with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ScadenzePartite fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        ScadenzePartite scadenzePartite = (ScadenzePartite) EntityCacheUtil.getResult(ScadenzePartiteModelImpl.ENTITY_CACHE_ENABLED,
                ScadenzePartiteImpl.class, primaryKey);

        if (scadenzePartite == _nullScadenzePartite) {
            return null;
        }

        if (scadenzePartite == null) {
            Session session = null;

            try {
                session = openSession();

                scadenzePartite = (ScadenzePartite) session.get(ScadenzePartiteImpl.class,
                        primaryKey);

                if (scadenzePartite != null) {
                    cacheResult(scadenzePartite);
                } else {
                    EntityCacheUtil.putResult(ScadenzePartiteModelImpl.ENTITY_CACHE_ENABLED,
                        ScadenzePartiteImpl.class, primaryKey,
                        _nullScadenzePartite);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(ScadenzePartiteModelImpl.ENTITY_CACHE_ENABLED,
                    ScadenzePartiteImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return scadenzePartite;
    }

    /**
     * Returns the scadenze partite with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param scadenzePartitePK the primary key of the scadenze partite
     * @return the scadenze partite, or <code>null</code> if a scadenze partite with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ScadenzePartite fetchByPrimaryKey(
        ScadenzePartitePK scadenzePartitePK) throws SystemException {
        return fetchByPrimaryKey((Serializable) scadenzePartitePK);
    }

    /**
     * Returns all the scadenze partites.
     *
     * @return the scadenze partites
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ScadenzePartite> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the scadenze partites.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ScadenzePartiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of scadenze partites
     * @param end the upper bound of the range of scadenze partites (not inclusive)
     * @return the range of scadenze partites
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ScadenzePartite> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the scadenze partites.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ScadenzePartiteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of scadenze partites
     * @param end the upper bound of the range of scadenze partites (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of scadenze partites
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ScadenzePartite> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<ScadenzePartite> list = (List<ScadenzePartite>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_SCADENZEPARTITE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_SCADENZEPARTITE;

                if (pagination) {
                    sql = sql.concat(ScadenzePartiteModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<ScadenzePartite>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<ScadenzePartite>(list);
                } else {
                    list = (List<ScadenzePartite>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the scadenze partites from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (ScadenzePartite scadenzePartite : findAll()) {
            remove(scadenzePartite);
        }
    }

    /**
     * Returns the number of scadenze partites.
     *
     * @return the number of scadenze partites
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_SCADENZEPARTITE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the scadenze partite persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.ScadenzePartite")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<ScadenzePartite>> listenersList = new ArrayList<ModelListener<ScadenzePartite>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<ScadenzePartite>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(ScadenzePartiteImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
