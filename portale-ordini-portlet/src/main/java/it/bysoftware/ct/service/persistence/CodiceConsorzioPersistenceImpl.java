package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchCodiceConsorzioException;
import it.bysoftware.ct.model.CodiceConsorzio;
import it.bysoftware.ct.model.impl.CodiceConsorzioImpl;
import it.bysoftware.ct.model.impl.CodiceConsorzioModelImpl;
import it.bysoftware.ct.service.persistence.CodiceConsorzioPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the codice consorzio service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CodiceConsorzioPersistence
 * @see CodiceConsorzioUtil
 * @generated
 */
public class CodiceConsorzioPersistenceImpl extends BasePersistenceImpl<CodiceConsorzio>
    implements CodiceConsorzioPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link CodiceConsorzioUtil} to access the codice consorzio persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = CodiceConsorzioImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CodiceConsorzioModelImpl.ENTITY_CACHE_ENABLED,
            CodiceConsorzioModelImpl.FINDER_CACHE_ENABLED,
            CodiceConsorzioImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CodiceConsorzioModelImpl.ENTITY_CACHE_ENABLED,
            CodiceConsorzioModelImpl.FINDER_CACHE_ENABLED,
            CodiceConsorzioImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CodiceConsorzioModelImpl.ENTITY_CACHE_ENABLED,
            CodiceConsorzioModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_CODICECONSORZIO = "SELECT codiceConsorzio FROM CodiceConsorzio codiceConsorzio";
    private static final String _SQL_COUNT_CODICECONSORZIO = "SELECT COUNT(codiceConsorzio) FROM CodiceConsorzio codiceConsorzio";
    private static final String _ORDER_BY_ENTITY_ALIAS = "codiceConsorzio.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CodiceConsorzio exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(CodiceConsorzioPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "codiceSocio", "idLiferay", "codiceConsorzio"
            });
    private static CodiceConsorzio _nullCodiceConsorzio = new CodiceConsorzioImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<CodiceConsorzio> toCacheModel() {
                return _nullCodiceConsorzioCacheModel;
            }
        };

    private static CacheModel<CodiceConsorzio> _nullCodiceConsorzioCacheModel = new CacheModel<CodiceConsorzio>() {
            @Override
            public CodiceConsorzio toEntityModel() {
                return _nullCodiceConsorzio;
            }
        };

    public CodiceConsorzioPersistenceImpl() {
        setModelClass(CodiceConsorzio.class);
    }

    /**
     * Caches the codice consorzio in the entity cache if it is enabled.
     *
     * @param codiceConsorzio the codice consorzio
     */
    @Override
    public void cacheResult(CodiceConsorzio codiceConsorzio) {
        EntityCacheUtil.putResult(CodiceConsorzioModelImpl.ENTITY_CACHE_ENABLED,
            CodiceConsorzioImpl.class, codiceConsorzio.getPrimaryKey(),
            codiceConsorzio);

        codiceConsorzio.resetOriginalValues();
    }

    /**
     * Caches the codice consorzios in the entity cache if it is enabled.
     *
     * @param codiceConsorzios the codice consorzios
     */
    @Override
    public void cacheResult(List<CodiceConsorzio> codiceConsorzios) {
        for (CodiceConsorzio codiceConsorzio : codiceConsorzios) {
            if (EntityCacheUtil.getResult(
                        CodiceConsorzioModelImpl.ENTITY_CACHE_ENABLED,
                        CodiceConsorzioImpl.class,
                        codiceConsorzio.getPrimaryKey()) == null) {
                cacheResult(codiceConsorzio);
            } else {
                codiceConsorzio.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all codice consorzios.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(CodiceConsorzioImpl.class.getName());
        }

        EntityCacheUtil.clearCache(CodiceConsorzioImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the codice consorzio.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(CodiceConsorzio codiceConsorzio) {
        EntityCacheUtil.removeResult(CodiceConsorzioModelImpl.ENTITY_CACHE_ENABLED,
            CodiceConsorzioImpl.class, codiceConsorzio.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<CodiceConsorzio> codiceConsorzios) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (CodiceConsorzio codiceConsorzio : codiceConsorzios) {
            EntityCacheUtil.removeResult(CodiceConsorzioModelImpl.ENTITY_CACHE_ENABLED,
                CodiceConsorzioImpl.class, codiceConsorzio.getPrimaryKey());
        }
    }

    /**
     * Creates a new codice consorzio with the primary key. Does not add the codice consorzio to the database.
     *
     * @param codiceSocio the primary key for the new codice consorzio
     * @return the new codice consorzio
     */
    @Override
    public CodiceConsorzio create(String codiceSocio) {
        CodiceConsorzio codiceConsorzio = new CodiceConsorzioImpl();

        codiceConsorzio.setNew(true);
        codiceConsorzio.setPrimaryKey(codiceSocio);

        return codiceConsorzio;
    }

    /**
     * Removes the codice consorzio with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param codiceSocio the primary key of the codice consorzio
     * @return the codice consorzio that was removed
     * @throws it.bysoftware.ct.NoSuchCodiceConsorzioException if a codice consorzio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CodiceConsorzio remove(String codiceSocio)
        throws NoSuchCodiceConsorzioException, SystemException {
        return remove((Serializable) codiceSocio);
    }

    /**
     * Removes the codice consorzio with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the codice consorzio
     * @return the codice consorzio that was removed
     * @throws it.bysoftware.ct.NoSuchCodiceConsorzioException if a codice consorzio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CodiceConsorzio remove(Serializable primaryKey)
        throws NoSuchCodiceConsorzioException, SystemException {
        Session session = null;

        try {
            session = openSession();

            CodiceConsorzio codiceConsorzio = (CodiceConsorzio) session.get(CodiceConsorzioImpl.class,
                    primaryKey);

            if (codiceConsorzio == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchCodiceConsorzioException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(codiceConsorzio);
        } catch (NoSuchCodiceConsorzioException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected CodiceConsorzio removeImpl(CodiceConsorzio codiceConsorzio)
        throws SystemException {
        codiceConsorzio = toUnwrappedModel(codiceConsorzio);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(codiceConsorzio)) {
                codiceConsorzio = (CodiceConsorzio) session.get(CodiceConsorzioImpl.class,
                        codiceConsorzio.getPrimaryKeyObj());
            }

            if (codiceConsorzio != null) {
                session.delete(codiceConsorzio);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (codiceConsorzio != null) {
            clearCache(codiceConsorzio);
        }

        return codiceConsorzio;
    }

    @Override
    public CodiceConsorzio updateImpl(
        it.bysoftware.ct.model.CodiceConsorzio codiceConsorzio)
        throws SystemException {
        codiceConsorzio = toUnwrappedModel(codiceConsorzio);

        boolean isNew = codiceConsorzio.isNew();

        Session session = null;

        try {
            session = openSession();

            if (codiceConsorzio.isNew()) {
                session.save(codiceConsorzio);

                codiceConsorzio.setNew(false);
            } else {
                session.merge(codiceConsorzio);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(CodiceConsorzioModelImpl.ENTITY_CACHE_ENABLED,
            CodiceConsorzioImpl.class, codiceConsorzio.getPrimaryKey(),
            codiceConsorzio);

        return codiceConsorzio;
    }

    protected CodiceConsorzio toUnwrappedModel(CodiceConsorzio codiceConsorzio) {
        if (codiceConsorzio instanceof CodiceConsorzioImpl) {
            return codiceConsorzio;
        }

        CodiceConsorzioImpl codiceConsorzioImpl = new CodiceConsorzioImpl();

        codiceConsorzioImpl.setNew(codiceConsorzio.isNew());
        codiceConsorzioImpl.setPrimaryKey(codiceConsorzio.getPrimaryKey());

        codiceConsorzioImpl.setCodiceSocio(codiceConsorzio.getCodiceSocio());
        codiceConsorzioImpl.setIdLiferay(codiceConsorzio.getIdLiferay());
        codiceConsorzioImpl.setCodiceConsorzio(codiceConsorzio.getCodiceConsorzio());

        return codiceConsorzioImpl;
    }

    /**
     * Returns the codice consorzio with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the codice consorzio
     * @return the codice consorzio
     * @throws it.bysoftware.ct.NoSuchCodiceConsorzioException if a codice consorzio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CodiceConsorzio findByPrimaryKey(Serializable primaryKey)
        throws NoSuchCodiceConsorzioException, SystemException {
        CodiceConsorzio codiceConsorzio = fetchByPrimaryKey(primaryKey);

        if (codiceConsorzio == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchCodiceConsorzioException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return codiceConsorzio;
    }

    /**
     * Returns the codice consorzio with the primary key or throws a {@link it.bysoftware.ct.NoSuchCodiceConsorzioException} if it could not be found.
     *
     * @param codiceSocio the primary key of the codice consorzio
     * @return the codice consorzio
     * @throws it.bysoftware.ct.NoSuchCodiceConsorzioException if a codice consorzio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CodiceConsorzio findByPrimaryKey(String codiceSocio)
        throws NoSuchCodiceConsorzioException, SystemException {
        return findByPrimaryKey((Serializable) codiceSocio);
    }

    /**
     * Returns the codice consorzio with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the codice consorzio
     * @return the codice consorzio, or <code>null</code> if a codice consorzio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CodiceConsorzio fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        CodiceConsorzio codiceConsorzio = (CodiceConsorzio) EntityCacheUtil.getResult(CodiceConsorzioModelImpl.ENTITY_CACHE_ENABLED,
                CodiceConsorzioImpl.class, primaryKey);

        if (codiceConsorzio == _nullCodiceConsorzio) {
            return null;
        }

        if (codiceConsorzio == null) {
            Session session = null;

            try {
                session = openSession();

                codiceConsorzio = (CodiceConsorzio) session.get(CodiceConsorzioImpl.class,
                        primaryKey);

                if (codiceConsorzio != null) {
                    cacheResult(codiceConsorzio);
                } else {
                    EntityCacheUtil.putResult(CodiceConsorzioModelImpl.ENTITY_CACHE_ENABLED,
                        CodiceConsorzioImpl.class, primaryKey,
                        _nullCodiceConsorzio);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(CodiceConsorzioModelImpl.ENTITY_CACHE_ENABLED,
                    CodiceConsorzioImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return codiceConsorzio;
    }

    /**
     * Returns the codice consorzio with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param codiceSocio the primary key of the codice consorzio
     * @return the codice consorzio, or <code>null</code> if a codice consorzio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CodiceConsorzio fetchByPrimaryKey(String codiceSocio)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) codiceSocio);
    }

    /**
     * Returns all the codice consorzios.
     *
     * @return the codice consorzios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<CodiceConsorzio> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the codice consorzios.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CodiceConsorzioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of codice consorzios
     * @param end the upper bound of the range of codice consorzios (not inclusive)
     * @return the range of codice consorzios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<CodiceConsorzio> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the codice consorzios.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CodiceConsorzioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of codice consorzios
     * @param end the upper bound of the range of codice consorzios (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of codice consorzios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<CodiceConsorzio> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<CodiceConsorzio> list = (List<CodiceConsorzio>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_CODICECONSORZIO);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_CODICECONSORZIO;

                if (pagination) {
                    sql = sql.concat(CodiceConsorzioModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<CodiceConsorzio>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<CodiceConsorzio>(list);
                } else {
                    list = (List<CodiceConsorzio>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the codice consorzios from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (CodiceConsorzio codiceConsorzio : findAll()) {
            remove(codiceConsorzio);
        }
    }

    /**
     * Returns the number of codice consorzios.
     *
     * @return the number of codice consorzios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_CODICECONSORZIO);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the codice consorzio persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.CodiceConsorzio")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<CodiceConsorzio>> listenersList = new ArrayList<ModelListener<CodiceConsorzio>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<CodiceConsorzio>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(CodiceConsorzioImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
