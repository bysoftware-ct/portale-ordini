package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.SottocontiServiceBaseImpl;

/**
 * The implementation of the sottoconti remote service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.SottocontiService} interface.
 *
 * <p>This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.SottocontiServiceBaseImpl
 * @see it.bysoftware.ct.service.SottocontiServiceUtil
 */
public class SottocontiServiceImpl extends SottocontiServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.SottocontiServiceUtil} to access the sottoconti
     * remote service.
     */
}
