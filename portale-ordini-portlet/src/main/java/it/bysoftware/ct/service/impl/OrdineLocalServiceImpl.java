package it.bysoftware.ct.service.impl;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Junction;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import it.bysoftware.ct.NoSuchOrdineException;
import it.bysoftware.ct.model.Ordine;
import it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.OrdineLocalServiceUtil;
import it.bysoftware.ct.service.base.OrdineLocalServiceBaseImpl;
import it.bysoftware.ct.utils.OrderState;
import it.bysoftware.ct.utils.Utils;

/**
 * The implementation of the ordine local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.OrdineLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.OrdineLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.OrdineLocalServiceUtil
 */
public class OrdineLocalServiceImpl extends OrdineLocalServiceBaseImpl {

    /**
     * 31 constants.    
     */
    private static final int THERTY_ONE = 31;

    /**
     * December constants.
     */
    private static final int DECEMBER = 11;

    /**
     * Query to retrieve the order of a specified partner having the specified
     * state and passport number. 
     */
    private static final String QUERY1 = "SELECT DISTINCT o.id, p.id_fornitore"
        + " FROM _ordini AS o INNER JOIN _carrelli AS c"
        + " ON o.id = c.id_ordine INNER JOIN _righe AS r"
        + " ON c.id = r.id_carrello INNER JOIN _piante AS p"
        + " ON r.id_articolo = p.id"
        + " WHERE (o.stato = ?) AND (p.id_fornitore = ?)"
        + " AND (r.codice_regionale <> '') AND (r.numero_passaporto = ?)";
    
    /**
     * Query to retrieve the order of all partners having the specified state
     * and passport number. 
     */
    private static final String QUERY2 = "SELECT DISTINCT o.id, p.id_fornitore"
        + " FROM _ordini AS o INNER JOIN _carrelli AS c"
        + " ON o.id = c.id_ordine INNER JOIN _righe AS r"
        + " ON c.id = r.id_carrello INNER JOIN _piante AS p"
        + " ON r.id_articolo = p.id"
        + " WHERE (o.stato = ?) AND (r.codice_regionale <> '')"
        + " AND (r.numero_passaporto = ?)";
    
    /**
     * Query to retrieve the order of all partners having the specified state
     * and passport number. 
     */
    private static final String QUERY5 = "SELECT DISTINCT o.id, p.id_fornitore"
        + " FROM _ordini AS o INNER JOIN _carrelli AS c"
        + " ON o.id = c.id_ordine INNER JOIN _righe AS r"
        + " ON c.id = r.id_carrello INNER JOIN _piante AS p"
        + " ON r.id_articolo = p.id"
        + " WHERE (o.stato = ?) AND (r.codice_regionale <> '')"
        + " AND (p.id_fornitore = ?) AND (r.numero_passaporto = ?)";
    
    /**
     * Query to retrieve the order of a specified partner having the specified
     * state and passport number. 
     */
    private static final String QUERY3 = "SELECT DISTINCT o.id, p.id_fornitore"
            + " FROM _ordini AS o INNER JOIN _carrelli AS c"
            + " ON o.id = c.id_ordine INNER JOIN _righe AS r"
            + " ON c.id = r.id_carrello INNER JOIN _piante AS p"
            + " ON r.id_articolo = p.id WHERE (o.stato <> 2 AND o.stato <> 5)"
            + " AND (p.id_fornitore = ?) AND (r.codice_regionale <> '')"
            + " AND (r.numero_passaporto <> '')"
            + " AND (o.data_consegna_merce BETWEEN ? AND ?)";
    
    /**
     * Query to retrieve the order of a specified partner having the specified
     * state and passport number. 
     */
    private static final String QUERY4 = "SELECT DISTINCT o.id, p.id_fornitore"
            + " FROM _ordini AS o INNER JOIN _carrelli AS c"
            + " ON o.id = c.id_ordine INNER JOIN _righe AS r"
            + " ON c.id = r.id_carrello INNER JOIN _piante AS p"
            + " ON r.id_articolo = p.id WHERE (o.stato <> 2 AND o.stato <> 5)"
            + " AND (r.codice_regionale <> '') AND (r.numero_passaporto <> '')"
            + " AND (o.data_consegna_merce BETWEEN ? AND ?)";
    
    /**
     * Query to retrieve the orders of a specified partner in the specified date
     * interval. 
     */
    private static final String QUERY6 = "SELECT DISTINCT o.id, p.id_fornitore"
    		+ " FROM _ordini AS o INNER JOIN _carrelli AS c"
    		+ " ON o.id = c.id_ordine INNER JOIN _righe AS r" 
    		+ " ON c.id = r.id_carrello INNER JOIN _piante AS p"
    		+ " ON r.id_articolo = p.id WHERE (p.id_fornitore = ?)"
    		+ " AND (o.data_consegna_merce BETWEEN ? AND ?)";
    
    /**
     * Query to retrieve the orders of all partners in the specified date
     * interval. 
     */
    private static final String QUERY7 = "SELECT DISTINCT o.id, p.id_fornitore"
    		+ " FROM _ordini AS o INNER JOIN _carrelli AS c"
    		+ " ON o.id = c.id_ordine INNER JOIN _righe AS r" 
    		+ " ON c.id = r.id_carrello INNER JOIN _piante AS p"
    		+ " ON r.id_articolo = p.id"
    		+ " AND (o.data_consegna_merce BETWEEN ? AND ?)";
    
    /**
     * Query to retrieve the orders having specified item in the specified date
     * interval. 
     */
    private static final String QUERY8 = "SELECT DISTINCT o.id, p.cod"
    		+ " FROM _ordini AS o INNER JOIN _carrelli AS c"
    		+ " ON o.id = c.id_ordine INNER JOIN _righe AS r"
    		+ " ON c.id = r.id_carrello INNER JOIN _piante AS p"
    		+ " ON r.id_articolo = p.id WHERE (p.cod = ?) AND"
    		+ " (o.data_consegna_merce BETWEEN ? AND ?)";
    
    /**
     * Query to retrieve the orders having specified item in the specified date
     * interval. 
     */
    private static final String QUERY9 = "SELECT DISTINCT o.id, p.cod"
    		+ " FROM _ordini AS o INNER JOIN _carrelli AS c"
    		+ " ON o.id = c.id_ordine INNER JOIN _righe AS r"
    		+ " ON c.id = r.id_carrello INNER JOIN _piante AS p"
    		+ " ON r.id_articolo = p.id WHERE"
    		+ " (o.data_consegna_merce BETWEEN ? AND ?)";
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(OrdineLocalServiceImpl.class);
    
    @SuppressWarnings("unchecked")
    @Override
    public final int getNumber(final int year, final String center) {
        int result = 0;
        DynamicQuery userQuery = DynamicQueryFactoryUtil.forClass(Ordine.class,
                getClassLoader());
        userQuery.add(PropertyFactoryUtil.forName("anno").eq(year));
        userQuery.add(PropertyFactoryUtil.forName("centro").eq(center));
        Projection projection = PropertyFactoryUtil.forName("numero").max();
        userQuery.setProjection(projection);
        try {
            List<Object> list = OrdineLocalServiceUtil.dynamicQuery(userQuery);
            if (Validator.isNumber(String.valueOf(list.get(0)))) {
                result = Integer.parseInt(String.valueOf(list.get(0)));
            } else {
                result = 0;
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        return result + 1;
    }

    /**
     * Returns the customer's orders in the specified state.
     * 
     * @param code
     *            customer code
     * @param state
     *            state
     * @return list of orders
     */
    private List<Ordine> getOrderByCodeState(final String code,
            final int state) {
        List<Ordine> result = new ArrayList<Ordine>();
        try {
            result = this.ordinePersistence
                    .findByCodiceClienteStato(code, state);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * Returns an array of object that contains the order IDs in which the
     * provided partner is involved having the specified state.
     * 
     * @param partnerCode
     *            the partner code
     * @param state
     *            the state
     * @param passport
     *            the passport number
     * @return the array of order IDs
     */
    @SuppressWarnings("unchecked")
    public final List<Object[]> getOrderByCodeStatePassport(
            final String partnerCode, final int state, final String passport) {
        Session session = this.ordinePersistence.openSession();
        SQLQuery query = session.createSQLQuery(OrdineLocalServiceImpl.QUERY1);
        QueryPos pos = QueryPos.getInstance(query);
        pos.add(state);
        pos.add(partnerCode);
        pos.add(passport);
        return query.list();
    }
    
    @Override
    public final List<Ordine> getOrderByCustomer(final String code) {
        List<Ordine> result = null;
        try {
            result = this.ordinePersistence.findByCodiceCliente(code);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            result = new ArrayList<Ordine>();
        }
        return result;
    }
    
    @Override
    public final List<Ordine> getOrderByState(final int state) {
        List<Ordine> result = null;
        try {
            result = this.ordinePersistence.findByStato(state);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            result = new ArrayList<Ordine>();
        }
        return result;
    }
    
    /**
     * Returns an array of object that contains the order IDs in which the
     * provided partner is involved having the specified state.
     * @param state
     *            the state
     * @param passport
     *            the passport number
     * 
     * @return the array of order IDs
     */
    @SuppressWarnings("unchecked")
    public final List<Object[]> getOrderByStatePassport(
            final int state, final String passport) {
        Session session = this.ordinePersistence.openSession();
        SQLQuery query = session.createSQLQuery(OrdineLocalServiceImpl.QUERY2);
        QueryPos pos = QueryPos.getInstance(query);
        pos.add(state);
        pos.add(passport);
        return query.list();
    }
    
    /**
     * Returns an array of object that contains the order IDs in which the
     * provided partner is involved having the specified state.
     * @param state
     *            the state
     * @param passport
     *            the passport number
     * 
     * @return the array of order IDs
     */
    @SuppressWarnings("unchecked")
    public final List<Object[]> getOrderByStatePassportPartner(
            final int state, final String passport, final String partnerCode) {
        Session session = this.ordinePersistence.openSession();
        SQLQuery query = session.createSQLQuery(OrdineLocalServiceImpl.QUERY5);
        QueryPos pos = QueryPos.getInstance(query);
        pos.add(state);
        pos.add(partnerCode);
        pos.add(passport);
        return query.list();
    }
    
    /**
     * Returns an array of object that contains the order IDs in which the
     * provided partner is involved having the specified state.
     * @param partnerCode
     *            the partner code
     * @param from
     *            from date
     * @param to
     *            to date
     * @return the array of order IDs
     */
    @SuppressWarnings("unchecked")
    public final List<Object[]> getOrderBetweenDatePerPartner(
    		final String partnerCode, final Date from, final Date to) {
        Session session = this.ordinePersistence.openSession();
        SQLQuery query = session.createSQLQuery(OrdineLocalServiceImpl.QUERY6);
        QueryPos pos = QueryPos.getInstance(query);
        pos.add(partnerCode);
        pos.add(from);
        pos.add(to);
        return query.list();
    }
    
    /**
     * Returns an array of object that contains the order IDs in which the
     * provided partner is involved having the specified state.
     * @param from
     *            from date
     * @param to
     *            to date
     * @return the array of order IDs
     */
    @SuppressWarnings("unchecked")
    public final List<Object[]> getOrderBetweenDate(final Date from,
    		final Date to) {
        Session session = this.ordinePersistence.openSession();
        SQLQuery query = session.createSQLQuery(OrdineLocalServiceImpl.QUERY7);
        QueryPos pos = QueryPos.getInstance(query);
        pos.add(from);
        pos.add(to);
        return query.list();
    }
    
    @SuppressWarnings("unchecked")
    public final List<Object[]> getOrderBetweenDatePerItem(
    		final String cod, final Date from, final Date to) {
        Session session = this.ordinePersistence.openSession();
        SQLQuery query = session.createSQLQuery(OrdineLocalServiceImpl.QUERY8);
        QueryPos pos = QueryPos.getInstance(query);
        pos.add(cod);
        pos.add(from);
        pos.add(to);
        return query.list();
    }
    
    @SuppressWarnings("unchecked")
    public final List<Object[]> getItemsInOrderBetweenDate(
    		final Date from, final Date to) {
        Session session = this.ordinePersistence.openSession();
        SQLQuery query = session.createSQLQuery(OrdineLocalServiceImpl.QUERY9);
        QueryPos pos = QueryPos.getInstance(query);
        pos.add(from);
        pos.add(to);
        return query.list();
    }
    
    /**
     * Returns an array of object that contains the order IDs in which the
     * provided partner is involved having the specified state.
     * 
     * @param partnerCode
     *            the partner code
     * @param from
     *            from date
     * @param to
     *            to date
     * @return the array of order IDs
     */
    @SuppressWarnings("unchecked")
    public final List<Object[]> getOrderWithPassportByPartnerAndDate(
            final String partnerCode, final Date from, final Date to) {
        Session session = this.ordinePersistence.openSession();
        SQLQuery query = session.createSQLQuery(OrdineLocalServiceImpl.QUERY3);
        QueryPos pos = QueryPos.getInstance(query);
        pos.add(partnerCode);
        pos.add(from);
        pos.add(to);
        return query.list();
    }
    
    /**
     * Returns an array of object that contains the order IDs in which the
     * provided partner is involved having the specified state.
     * 
     * @param from
     *            from date
     * @param to
     *            to date
     * @return the array of order IDs
     */
    @SuppressWarnings("unchecked")
    public final List<Object[]> getOrderWithPassportByDate(
            final Date from, final Date to) {
        Session session = this.ordinePersistence.openSession();
        SQLQuery query = session.createSQLQuery(OrdineLocalServiceImpl.QUERY4);
        QueryPos pos = QueryPos.getInstance(query);
        pos.add(from);
        pos.add(to);
        return query.list();
    }
    
    @Override
    public final List<Ordine> getCustomerConfirmedOrders(final String code) {
        return this.getOrderByCodeState(code, OrderState.CONFIRMED.getValue());
    }
    
    @Override
    public final List<Ordine> getCustomerPendingOrders(final String code) {
        return this.getOrderByCodeState(code, OrderState.PENDING.getValue());
    }
    
    @Override
    public final List<Ordine> getSavedCustomerOrders(final String code) {
        return this.getOrderByCodeState(code, OrderState.SAVED.getValue());
    }
    
    @Override
    public final List<Ordine> getScheduledCustomerOrders(final String code) {
        return this.getOrderByCodeState(code, OrderState.SCHEDULED.getValue());
    }
    
    @Override
    public final List<Ordine> getProcessedCustomerOrders(final String code) {
        return this.getOrderByCodeState(code, OrderState.PROCESSED.getValue());
    }
    
    @Override
    public final List<Ordine> getUnScheduledOrderByCustomer(final String code) {
        List<Ordine> result = new ArrayList<Ordine>();
        result.addAll(this.getOrderByCodeState(code,
                OrderState.PENDING.getValue()));
        result.addAll(this.getOrderByCodeState(code,
                OrderState.SAVED.getValue()));
        result.addAll(this.getOrderByCodeState(code,
                OrderState.CONFIRMED.getValue()));
        return result;
    }
    
    @Override
    public final List<Ordine> getTodayReadyOrders() {
        List<Ordine> result = new ArrayList<Ordine>();
        
        try {
            result = this.ordinePersistence.findByConsegnaStato(Utils.today().
                    getTime(), new int[] { OrderState.CONFIRMED.getValue(),
                    OrderState.PENDING.getValue() });
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        
        return result;
    }
    
    @Override
    public final List<Ordine> getTodayConfirmedOrders() {
        List<Ordine> result = new ArrayList<Ordine>();
        
        try {
            result = this.ordinePersistence.findByConsegnaStato(Utils.today().
                    getTime(), new int[] { OrderState.CONFIRMED.getValue() });
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        
        return result;
    }
    
    @Override
    public final List<Ordine> getTodayProcessedOrders() {
        List<Ordine> result = new ArrayList<Ordine>();
        
        try {
            result = this.ordinePersistence.findByInserimentoConsegnaStato(
                    Utils.today().getTime(), Utils.today().getTime(),
                    OrderState.SCHEDULED.getValue());
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<Ordine> findOrders(final String customerCode,
            final Date orderDate, final Date shipDate,
            final boolean showsInactive, final boolean showsDraft,
            final boolean showsPending, final boolean showsApproved,
            final boolean showsProcessed, final boolean andSearch,
            final int start, final int end,
            final OrderByComparator orderByComparator) throws SystemException {
        DynamicQuery dynamicQuery = this.buildDynamicQuery(customerCode,
                orderDate, shipDate, showsInactive, showsDraft, showsPending,
                showsApproved, showsProcessed, andSearch);
        return OrdineLocalServiceUtil.dynamicQuery(dynamicQuery, start, end,
                orderByComparator);
    }
    
    @Override
    public final int getFindOrdersCount(final String customerCode,
            final Date orderDate, final Date shipDate,
            final boolean showsInactive, final boolean showsDraft,
            final boolean showsPending, final boolean showsApproved,
            final boolean showsProcessed, final boolean andSearch)
                    throws SystemException {
        DynamicQuery dynamicQuery = this.buildDynamicQuery(customerCode,
                orderDate, shipDate, showsInactive, showsDraft, showsPending,
                showsApproved, showsProcessed, andSearch);
        return (int) AnagraficheClientiFornitoriLocalServiceUtil
                .dynamicQueryCount(dynamicQuery);
    }
    
    @Override
    public final Ordine getByYearNumberCenter(final int n, final String c) {
        Ordine result = null;
        
        try {
            result = this.ordinePersistence.findByAnnoNumeroCentro(
                    Utils.getYear(), n, c);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (NoSuchOrdineException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        
        return result;
    }
    
    /**
     * Build {@link DynamicQuery} based on the filter specified.
     * 
     * @param customerCode
     *            customer code
     * @param from
     *            order date
     * @param to
     *            ship date
     * @param showsInactive
     *            true to show inactive orders false otherwise
     * @param showsDraft
     *            true to show draft orders false otherwise
     * @param showsPending
     *            true to show pending orders false otherwise
     * @param showsApproved
     *            true to show approved orders false otherwise
     * @param showsProcessed
     *            true to show processed orders false otherwise
     * @param andSearch
     *            true if is and search false otherwise
     * @return return a {@link DynamicQuery}
     */
    private DynamicQuery buildDynamicQuery(final String customerCode,
            final Date from, final Date to,
            final boolean showsInactive, final boolean showsDraft,
            final boolean showsPending, final boolean showsApproved,
            final boolean showsProcessed, final boolean andSearch) {
        Junction junction = null;
        if (andSearch) {
            junction = RestrictionsFactoryUtil.conjunction();
        } else {
            junction = RestrictionsFactoryUtil.disjunction();
        }
        if (Validator.isNotNull(customerCode)) {
            Property property = PropertyFactoryUtil.forName("idCliente");
            String value = (new StringBuilder(StringPool.PERCENT)).
                    append(customerCode).append(StringPool.PERCENT).toString();
            junction.add(property.like(value));
        }
        
        this.logger.debug("CUSTOMER: " + customerCode);
        
        Calendar calFrom = Calendar.getInstance();
        calFrom.setTime(from);
        calFrom.set(Calendar.HOUR_OF_DAY, 0);
        calFrom.set(Calendar.MINUTE, 0);
        calFrom.set(Calendar.SECOND, 0);
        calFrom.set(Calendar.MILLISECOND, 0);
        Calendar calTo = Calendar.getInstance();
        calTo.setTime(to);
        calTo.set(Calendar.HOUR_OF_DAY, 0);
        calTo.set(Calendar.MINUTE, 0);
        calTo.set(Calendar.SECOND, 0);
        calTo.set(Calendar.MILLISECOND, 0);
        if (calTo.after(calFrom) || calTo.equals(calFrom)) {
            Property property = PropertyFactoryUtil.forName("dataConsegna");
            junction.add(property.ge(calFrom.getTime()));
            junction.add(property.le(calTo.getTime()));
        } else {
            Property property = PropertyFactoryUtil.forName("dataConsegna");
            junction.add(property.ge(calFrom.getTime()));
            calTo.set(Calendar.YEAR, Utils.getYear());
            calTo.set(Calendar.MONTH, OrdineLocalServiceImpl.DECEMBER);
            calTo.set(Calendar.DAY_OF_MONTH, OrdineLocalServiceImpl.THERTY_ONE);
            junction.add(property.le(calTo.getTime()));
        }
        
        this.logger.debug("ORDER DATE: " + calFrom.getTime());
        this.logger.debug("SHIP DATE: " + calTo.getTime());
        
        
        Junction junction1 = RestrictionsFactoryUtil.disjunction();
        Property property = PropertyFactoryUtil.forName("stato");
        
        if (showsInactive) {
            this.logger.debug("showsInactive: " + showsInactive);
            junction1.add(property.eq(OrderState.INACTIVE.getValue()));
        }
        
        if (showsDraft) {
            this.logger.debug("showsDrafts: " + showsDraft);
            junction1.add(property.eq(OrderState.SAVED.getValue()));
        }
        
        if (showsPending) {
            this.logger.debug("showsPending: " + showsPending);
            junction1.add(property.eq(OrderState.PENDING.getValue()));
        }
        
        if (showsApproved) {
            this.logger.debug("showsApproved: " + showsApproved);
            junction1.add(property.eq(OrderState.CONFIRMED.getValue()));
        }
        
        if (showsProcessed) {
            this.logger.debug("showsProcessed: " + showsProcessed);
            junction1.add(property.eq(OrderState.PROCESSED.getValue()));
            junction1.add(property.eq(OrderState.SCHEDULED.getValue()));
        }
        
        junction.add(junction1);
        
        DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
                Ordine.class, getClassLoader());
        return dynamicQuery.add(junction);
    }
    
}
