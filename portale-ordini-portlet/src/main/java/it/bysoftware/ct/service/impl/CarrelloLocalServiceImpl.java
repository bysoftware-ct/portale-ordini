package it.bysoftware.ct.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.util.ArrayList;
import java.util.List;

import it.bysoftware.ct.model.Carrello;
import it.bysoftware.ct.service.base.CarrelloLocalServiceBaseImpl;

/**
 * The implementation of the carrello local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.CarrelloLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.CarrelloLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.CarrelloLocalServiceUtil
 */
public class CarrelloLocalServiceImpl extends CarrelloLocalServiceBaseImpl {
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(CarrelloLocalServiceImpl.class);

    @Override
    public final List<Carrello> findCartsInOrder(final long orderId) {
        List<Carrello> result = null;
        try {
            result = this.carrelloPersistence.findByIdOrdine(orderId);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            result = new ArrayList<Carrello>();
        }
        return result;
    }
}
