package it.bysoftware.ct.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.bysoftware.ct.model.DettagliOperazioniContabili;
import it.bysoftware.ct.service.base.
    DettagliOperazioniContabiliLocalServiceBaseImpl;

import java.util.ArrayList;
import java.util.List;


/**
 * The implementation of the dettagli operazioni contabili local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.DettagliOperazioniContabiliLocalService}
 * interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.
 *      DettagliOperazioniContabiliLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.DettagliOperazioniContabiliLocalServiceUtil
 */
public class DettagliOperazioniContabiliLocalServiceImpl extends
        DettagliOperazioniContabiliLocalServiceBaseImpl {
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(
            DettagliOperazioniContabiliServiceImpl.class);

    @Override
    public final List<DettagliOperazioniContabili> findByAccountingOperation(
            final String code) {
        List<DettagliOperazioniContabili> result = null;
        
        try {
            result = this.dettagliOperazioniContabiliPersistence.
                    findByCodiceOperazione(code);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            result = new ArrayList<DettagliOperazioniContabili>();
        }
        return result;
    }
}
