package it.bysoftware.ct.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.bysoftware.ct.model.MovimentoVuoto;
import it.bysoftware.ct.service.base.VuotoServiceBaseImpl;

/**
 * The implementation of the vuoto remote service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.VuotoService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.VuotoServiceBaseImpl
 * @see it.bysoftware.ct.service.VuotoServiceUtil
 */
public class VuotoServiceImpl extends VuotoServiceBaseImpl {
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(VuotoLocalServiceImpl.class);
    
}
