package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchRigaException;
import it.bysoftware.ct.model.Riga;
import it.bysoftware.ct.model.impl.RigaImpl;
import it.bysoftware.ct.model.impl.RigaModelImpl;
import it.bysoftware.ct.service.persistence.RigaPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the riga service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see RigaPersistence
 * @see RigaUtil
 * @generated
 */
public class RigaPersistenceImpl extends BasePersistenceImpl<Riga>
    implements RigaPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link RigaUtil} to access the riga persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = RigaImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(RigaModelImpl.ENTITY_CACHE_ENABLED,
            RigaModelImpl.FINDER_CACHE_ENABLED, RigaImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(RigaModelImpl.ENTITY_CACHE_ENABLED,
            RigaModelImpl.FINDER_CACHE_ENABLED, RigaImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(RigaModelImpl.ENTITY_CACHE_ENABLED,
            RigaModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDCARELLO =
        new FinderPath(RigaModelImpl.ENTITY_CACHE_ENABLED,
            RigaModelImpl.FINDER_CACHE_ENABLED, RigaImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByIdCarello",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDCARELLO =
        new FinderPath(RigaModelImpl.ENTITY_CACHE_ENABLED,
            RigaModelImpl.FINDER_CACHE_ENABLED, RigaImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByIdCarello",
            new String[] { Long.class.getName() },
            RigaModelImpl.IDCARRELLO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_IDCARELLO = new FinderPath(RigaModelImpl.ENTITY_CACHE_ENABLED,
            RigaModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByIdCarello",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_IDCARELLO_IDCARRELLO_2 = "riga.idCarrello = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FORNITOREPASSAPORTO =
        new FinderPath(RigaModelImpl.ENTITY_CACHE_ENABLED,
            RigaModelImpl.FINDER_CACHE_ENABLED, RigaImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByfornitorePassaporto",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITOREPASSAPORTO =
        new FinderPath(RigaModelImpl.ENTITY_CACHE_ENABLED,
            RigaModelImpl.FINDER_CACHE_ENABLED, RigaImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByfornitorePassaporto",
            new String[] { String.class.getName() },
            RigaModelImpl.PASSAPORTO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_FORNITOREPASSAPORTO = new FinderPath(RigaModelImpl.ENTITY_CACHE_ENABLED,
            RigaModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByfornitorePassaporto",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_FORNITOREPASSAPORTO_PASSAPORTO_1 = "riga.passaporto IS NULL";
    private static final String _FINDER_COLUMN_FORNITOREPASSAPORTO_PASSAPORTO_2 = "riga.passaporto = ?";
    private static final String _FINDER_COLUMN_FORNITOREPASSAPORTO_PASSAPORTO_3 = "(riga.passaporto IS NULL OR riga.passaporto = '')";
    private static final String _SQL_SELECT_RIGA = "SELECT riga FROM Riga riga";
    private static final String _SQL_SELECT_RIGA_WHERE = "SELECT riga FROM Riga riga WHERE ";
    private static final String _SQL_COUNT_RIGA = "SELECT COUNT(riga) FROM Riga riga";
    private static final String _SQL_COUNT_RIGA_WHERE = "SELECT COUNT(riga) FROM Riga riga WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "riga.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Riga exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Riga exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(RigaPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "idCarrello", "idArticolo", "progressivo", "pianteRipiano",
                "importoVarianti", "codiceVariante", "codiceRegionale",
                "passaporto", "prezzoFornitore", "prezzoEtichetta"
            });
    private static Riga _nullRiga = new RigaImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Riga> toCacheModel() {
                return _nullRigaCacheModel;
            }
        };

    private static CacheModel<Riga> _nullRigaCacheModel = new CacheModel<Riga>() {
            @Override
            public Riga toEntityModel() {
                return _nullRiga;
            }
        };

    public RigaPersistenceImpl() {
        setModelClass(Riga.class);
    }

    /**
     * Returns all the rigas where idCarrello = &#63;.
     *
     * @param idCarrello the id carrello
     * @return the matching rigas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Riga> findByIdCarello(long idCarrello)
        throws SystemException {
        return findByIdCarello(idCarrello, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the rigas where idCarrello = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param idCarrello the id carrello
     * @param start the lower bound of the range of rigas
     * @param end the upper bound of the range of rigas (not inclusive)
     * @return the range of matching rigas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Riga> findByIdCarello(long idCarrello, int start, int end)
        throws SystemException {
        return findByIdCarello(idCarrello, start, end, null);
    }

    /**
     * Returns an ordered range of all the rigas where idCarrello = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param idCarrello the id carrello
     * @param start the lower bound of the range of rigas
     * @param end the upper bound of the range of rigas (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching rigas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Riga> findByIdCarello(long idCarrello, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDCARELLO;
            finderArgs = new Object[] { idCarrello };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDCARELLO;
            finderArgs = new Object[] { idCarrello, start, end, orderByComparator };
        }

        List<Riga> list = (List<Riga>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Riga riga : list) {
                if ((idCarrello != riga.getIdCarrello())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_RIGA_WHERE);

            query.append(_FINDER_COLUMN_IDCARELLO_IDCARRELLO_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(RigaModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(idCarrello);

                if (!pagination) {
                    list = (List<Riga>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Riga>(list);
                } else {
                    list = (List<Riga>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first riga in the ordered set where idCarrello = &#63;.
     *
     * @param idCarrello the id carrello
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching riga
     * @throws it.bysoftware.ct.NoSuchRigaException if a matching riga could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Riga findByIdCarello_First(long idCarrello,
        OrderByComparator orderByComparator)
        throws NoSuchRigaException, SystemException {
        Riga riga = fetchByIdCarello_First(idCarrello, orderByComparator);

        if (riga != null) {
            return riga;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("idCarrello=");
        msg.append(idCarrello);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchRigaException(msg.toString());
    }

    /**
     * Returns the first riga in the ordered set where idCarrello = &#63;.
     *
     * @param idCarrello the id carrello
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching riga, or <code>null</code> if a matching riga could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Riga fetchByIdCarello_First(long idCarrello,
        OrderByComparator orderByComparator) throws SystemException {
        List<Riga> list = findByIdCarello(idCarrello, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last riga in the ordered set where idCarrello = &#63;.
     *
     * @param idCarrello the id carrello
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching riga
     * @throws it.bysoftware.ct.NoSuchRigaException if a matching riga could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Riga findByIdCarello_Last(long idCarrello,
        OrderByComparator orderByComparator)
        throws NoSuchRigaException, SystemException {
        Riga riga = fetchByIdCarello_Last(idCarrello, orderByComparator);

        if (riga != null) {
            return riga;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("idCarrello=");
        msg.append(idCarrello);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchRigaException(msg.toString());
    }

    /**
     * Returns the last riga in the ordered set where idCarrello = &#63;.
     *
     * @param idCarrello the id carrello
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching riga, or <code>null</code> if a matching riga could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Riga fetchByIdCarello_Last(long idCarrello,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByIdCarello(idCarrello);

        if (count == 0) {
            return null;
        }

        List<Riga> list = findByIdCarello(idCarrello, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the rigas before and after the current riga in the ordered set where idCarrello = &#63;.
     *
     * @param id the primary key of the current riga
     * @param idCarrello the id carrello
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next riga
     * @throws it.bysoftware.ct.NoSuchRigaException if a riga with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Riga[] findByIdCarello_PrevAndNext(long id, long idCarrello,
        OrderByComparator orderByComparator)
        throws NoSuchRigaException, SystemException {
        Riga riga = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Riga[] array = new RigaImpl[3];

            array[0] = getByIdCarello_PrevAndNext(session, riga, idCarrello,
                    orderByComparator, true);

            array[1] = riga;

            array[2] = getByIdCarello_PrevAndNext(session, riga, idCarrello,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Riga getByIdCarello_PrevAndNext(Session session, Riga riga,
        long idCarrello, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_RIGA_WHERE);

        query.append(_FINDER_COLUMN_IDCARELLO_IDCARRELLO_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(RigaModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(idCarrello);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(riga);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Riga> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the rigas where idCarrello = &#63; from the database.
     *
     * @param idCarrello the id carrello
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByIdCarello(long idCarrello) throws SystemException {
        for (Riga riga : findByIdCarello(idCarrello, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(riga);
        }
    }

    /**
     * Returns the number of rigas where idCarrello = &#63;.
     *
     * @param idCarrello the id carrello
     * @return the number of matching rigas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByIdCarello(long idCarrello) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_IDCARELLO;

        Object[] finderArgs = new Object[] { idCarrello };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_RIGA_WHERE);

            query.append(_FINDER_COLUMN_IDCARELLO_IDCARRELLO_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(idCarrello);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the rigas where passaporto = &#63;.
     *
     * @param passaporto the passaporto
     * @return the matching rigas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Riga> findByfornitorePassaporto(String passaporto)
        throws SystemException {
        return findByfornitorePassaporto(passaporto, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the rigas where passaporto = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param passaporto the passaporto
     * @param start the lower bound of the range of rigas
     * @param end the upper bound of the range of rigas (not inclusive)
     * @return the range of matching rigas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Riga> findByfornitorePassaporto(String passaporto, int start,
        int end) throws SystemException {
        return findByfornitorePassaporto(passaporto, start, end, null);
    }

    /**
     * Returns an ordered range of all the rigas where passaporto = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param passaporto the passaporto
     * @param start the lower bound of the range of rigas
     * @param end the upper bound of the range of rigas (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching rigas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Riga> findByfornitorePassaporto(String passaporto, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITOREPASSAPORTO;
            finderArgs = new Object[] { passaporto };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FORNITOREPASSAPORTO;
            finderArgs = new Object[] { passaporto, start, end, orderByComparator };
        }

        List<Riga> list = (List<Riga>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Riga riga : list) {
                if (!Validator.equals(passaporto, riga.getPassaporto())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_RIGA_WHERE);

            boolean bindPassaporto = false;

            if (passaporto == null) {
                query.append(_FINDER_COLUMN_FORNITOREPASSAPORTO_PASSAPORTO_1);
            } else if (passaporto.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITOREPASSAPORTO_PASSAPORTO_3);
            } else {
                bindPassaporto = true;

                query.append(_FINDER_COLUMN_FORNITOREPASSAPORTO_PASSAPORTO_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(RigaModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindPassaporto) {
                    qPos.add(passaporto);
                }

                if (!pagination) {
                    list = (List<Riga>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Riga>(list);
                } else {
                    list = (List<Riga>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first riga in the ordered set where passaporto = &#63;.
     *
     * @param passaporto the passaporto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching riga
     * @throws it.bysoftware.ct.NoSuchRigaException if a matching riga could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Riga findByfornitorePassaporto_First(String passaporto,
        OrderByComparator orderByComparator)
        throws NoSuchRigaException, SystemException {
        Riga riga = fetchByfornitorePassaporto_First(passaporto,
                orderByComparator);

        if (riga != null) {
            return riga;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("passaporto=");
        msg.append(passaporto);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchRigaException(msg.toString());
    }

    /**
     * Returns the first riga in the ordered set where passaporto = &#63;.
     *
     * @param passaporto the passaporto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching riga, or <code>null</code> if a matching riga could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Riga fetchByfornitorePassaporto_First(String passaporto,
        OrderByComparator orderByComparator) throws SystemException {
        List<Riga> list = findByfornitorePassaporto(passaporto, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last riga in the ordered set where passaporto = &#63;.
     *
     * @param passaporto the passaporto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching riga
     * @throws it.bysoftware.ct.NoSuchRigaException if a matching riga could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Riga findByfornitorePassaporto_Last(String passaporto,
        OrderByComparator orderByComparator)
        throws NoSuchRigaException, SystemException {
        Riga riga = fetchByfornitorePassaporto_Last(passaporto,
                orderByComparator);

        if (riga != null) {
            return riga;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("passaporto=");
        msg.append(passaporto);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchRigaException(msg.toString());
    }

    /**
     * Returns the last riga in the ordered set where passaporto = &#63;.
     *
     * @param passaporto the passaporto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching riga, or <code>null</code> if a matching riga could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Riga fetchByfornitorePassaporto_Last(String passaporto,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByfornitorePassaporto(passaporto);

        if (count == 0) {
            return null;
        }

        List<Riga> list = findByfornitorePassaporto(passaporto, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the rigas before and after the current riga in the ordered set where passaporto = &#63;.
     *
     * @param id the primary key of the current riga
     * @param passaporto the passaporto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next riga
     * @throws it.bysoftware.ct.NoSuchRigaException if a riga with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Riga[] findByfornitorePassaporto_PrevAndNext(long id,
        String passaporto, OrderByComparator orderByComparator)
        throws NoSuchRigaException, SystemException {
        Riga riga = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Riga[] array = new RigaImpl[3];

            array[0] = getByfornitorePassaporto_PrevAndNext(session, riga,
                    passaporto, orderByComparator, true);

            array[1] = riga;

            array[2] = getByfornitorePassaporto_PrevAndNext(session, riga,
                    passaporto, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Riga getByfornitorePassaporto_PrevAndNext(Session session,
        Riga riga, String passaporto, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_RIGA_WHERE);

        boolean bindPassaporto = false;

        if (passaporto == null) {
            query.append(_FINDER_COLUMN_FORNITOREPASSAPORTO_PASSAPORTO_1);
        } else if (passaporto.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_FORNITOREPASSAPORTO_PASSAPORTO_3);
        } else {
            bindPassaporto = true;

            query.append(_FINDER_COLUMN_FORNITOREPASSAPORTO_PASSAPORTO_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(RigaModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindPassaporto) {
            qPos.add(passaporto);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(riga);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Riga> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the rigas where passaporto = &#63; from the database.
     *
     * @param passaporto the passaporto
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByfornitorePassaporto(String passaporto)
        throws SystemException {
        for (Riga riga : findByfornitorePassaporto(passaporto,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(riga);
        }
    }

    /**
     * Returns the number of rigas where passaporto = &#63;.
     *
     * @param passaporto the passaporto
     * @return the number of matching rigas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByfornitorePassaporto(String passaporto)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_FORNITOREPASSAPORTO;

        Object[] finderArgs = new Object[] { passaporto };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_RIGA_WHERE);

            boolean bindPassaporto = false;

            if (passaporto == null) {
                query.append(_FINDER_COLUMN_FORNITOREPASSAPORTO_PASSAPORTO_1);
            } else if (passaporto.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITOREPASSAPORTO_PASSAPORTO_3);
            } else {
                bindPassaporto = true;

                query.append(_FINDER_COLUMN_FORNITOREPASSAPORTO_PASSAPORTO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindPassaporto) {
                    qPos.add(passaporto);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the riga in the entity cache if it is enabled.
     *
     * @param riga the riga
     */
    @Override
    public void cacheResult(Riga riga) {
        EntityCacheUtil.putResult(RigaModelImpl.ENTITY_CACHE_ENABLED,
            RigaImpl.class, riga.getPrimaryKey(), riga);

        riga.resetOriginalValues();
    }

    /**
     * Caches the rigas in the entity cache if it is enabled.
     *
     * @param rigas the rigas
     */
    @Override
    public void cacheResult(List<Riga> rigas) {
        for (Riga riga : rigas) {
            if (EntityCacheUtil.getResult(RigaModelImpl.ENTITY_CACHE_ENABLED,
                        RigaImpl.class, riga.getPrimaryKey()) == null) {
                cacheResult(riga);
            } else {
                riga.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all rigas.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(RigaImpl.class.getName());
        }

        EntityCacheUtil.clearCache(RigaImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the riga.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Riga riga) {
        EntityCacheUtil.removeResult(RigaModelImpl.ENTITY_CACHE_ENABLED,
            RigaImpl.class, riga.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Riga> rigas) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Riga riga : rigas) {
            EntityCacheUtil.removeResult(RigaModelImpl.ENTITY_CACHE_ENABLED,
                RigaImpl.class, riga.getPrimaryKey());
        }
    }

    /**
     * Creates a new riga with the primary key. Does not add the riga to the database.
     *
     * @param id the primary key for the new riga
     * @return the new riga
     */
    @Override
    public Riga create(long id) {
        Riga riga = new RigaImpl();

        riga.setNew(true);
        riga.setPrimaryKey(id);

        return riga;
    }

    /**
     * Removes the riga with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the riga
     * @return the riga that was removed
     * @throws it.bysoftware.ct.NoSuchRigaException if a riga with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Riga remove(long id) throws NoSuchRigaException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the riga with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the riga
     * @return the riga that was removed
     * @throws it.bysoftware.ct.NoSuchRigaException if a riga with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Riga remove(Serializable primaryKey)
        throws NoSuchRigaException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Riga riga = (Riga) session.get(RigaImpl.class, primaryKey);

            if (riga == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchRigaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(riga);
        } catch (NoSuchRigaException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Riga removeImpl(Riga riga) throws SystemException {
        riga = toUnwrappedModel(riga);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(riga)) {
                riga = (Riga) session.get(RigaImpl.class,
                        riga.getPrimaryKeyObj());
            }

            if (riga != null) {
                session.delete(riga);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (riga != null) {
            clearCache(riga);
        }

        return riga;
    }

    @Override
    public Riga updateImpl(it.bysoftware.ct.model.Riga riga)
        throws SystemException {
        riga = toUnwrappedModel(riga);

        boolean isNew = riga.isNew();

        RigaModelImpl rigaModelImpl = (RigaModelImpl) riga;

        Session session = null;

        try {
            session = openSession();

            if (riga.isNew()) {
                session.save(riga);

                riga.setNew(false);
            } else {
                session.merge(riga);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !RigaModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((rigaModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDCARELLO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        rigaModelImpl.getOriginalIdCarrello()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDCARELLO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDCARELLO,
                    args);

                args = new Object[] { rigaModelImpl.getIdCarrello() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDCARELLO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDCARELLO,
                    args);
            }

            if ((rigaModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITOREPASSAPORTO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        rigaModelImpl.getOriginalPassaporto()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FORNITOREPASSAPORTO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITOREPASSAPORTO,
                    args);

                args = new Object[] { rigaModelImpl.getPassaporto() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FORNITOREPASSAPORTO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITOREPASSAPORTO,
                    args);
            }
        }

        EntityCacheUtil.putResult(RigaModelImpl.ENTITY_CACHE_ENABLED,
            RigaImpl.class, riga.getPrimaryKey(), riga);

        return riga;
    }

    protected Riga toUnwrappedModel(Riga riga) {
        if (riga instanceof RigaImpl) {
            return riga;
        }

        RigaImpl rigaImpl = new RigaImpl();

        rigaImpl.setNew(riga.isNew());
        rigaImpl.setPrimaryKey(riga.getPrimaryKey());

        rigaImpl.setId(riga.getId());
        rigaImpl.setIdCarrello(riga.getIdCarrello());
        rigaImpl.setIdArticolo(riga.getIdArticolo());
        rigaImpl.setProgressivo(riga.getProgressivo());
        rigaImpl.setPrezzo(riga.getPrezzo());
        rigaImpl.setImporto(riga.getImporto());
        rigaImpl.setPianteRipiano(riga.getPianteRipiano());
        rigaImpl.setSormontate(riga.isSormontate());
        rigaImpl.setStringa(riga.getStringa());
        rigaImpl.setOpzioni(riga.getOpzioni());
        rigaImpl.setImportoVarianti(riga.getImportoVarianti());
        rigaImpl.setCodiceVariante(riga.getCodiceVariante());
        rigaImpl.setCodiceRegionale(riga.getCodiceRegionale());
        rigaImpl.setPassaporto(riga.getPassaporto());
        rigaImpl.setS2(riga.getS2());
        rigaImpl.setS3(riga.getS3());
        rigaImpl.setS4(riga.getS4());
        rigaImpl.setSconto(riga.getSconto());
        rigaImpl.setPrezzoFornitore(riga.getPrezzoFornitore());
        rigaImpl.setPrezzoEtichetta(riga.getPrezzoEtichetta());
        rigaImpl.setEan(riga.getEan());
        rigaImpl.setModificato(riga.isModificato());
        rigaImpl.setRicevuto(riga.isRicevuto());

        return rigaImpl;
    }

    /**
     * Returns the riga with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the riga
     * @return the riga
     * @throws it.bysoftware.ct.NoSuchRigaException if a riga with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Riga findByPrimaryKey(Serializable primaryKey)
        throws NoSuchRigaException, SystemException {
        Riga riga = fetchByPrimaryKey(primaryKey);

        if (riga == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchRigaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return riga;
    }

    /**
     * Returns the riga with the primary key or throws a {@link it.bysoftware.ct.NoSuchRigaException} if it could not be found.
     *
     * @param id the primary key of the riga
     * @return the riga
     * @throws it.bysoftware.ct.NoSuchRigaException if a riga with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Riga findByPrimaryKey(long id)
        throws NoSuchRigaException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the riga with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the riga
     * @return the riga, or <code>null</code> if a riga with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Riga fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Riga riga = (Riga) EntityCacheUtil.getResult(RigaModelImpl.ENTITY_CACHE_ENABLED,
                RigaImpl.class, primaryKey);

        if (riga == _nullRiga) {
            return null;
        }

        if (riga == null) {
            Session session = null;

            try {
                session = openSession();

                riga = (Riga) session.get(RigaImpl.class, primaryKey);

                if (riga != null) {
                    cacheResult(riga);
                } else {
                    EntityCacheUtil.putResult(RigaModelImpl.ENTITY_CACHE_ENABLED,
                        RigaImpl.class, primaryKey, _nullRiga);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(RigaModelImpl.ENTITY_CACHE_ENABLED,
                    RigaImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return riga;
    }

    /**
     * Returns the riga with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the riga
     * @return the riga, or <code>null</code> if a riga with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Riga fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the rigas.
     *
     * @return the rigas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Riga> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the rigas.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of rigas
     * @param end the upper bound of the range of rigas (not inclusive)
     * @return the range of rigas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Riga> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the rigas.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of rigas
     * @param end the upper bound of the range of rigas (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of rigas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Riga> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Riga> list = (List<Riga>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_RIGA);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_RIGA;

                if (pagination) {
                    sql = sql.concat(RigaModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Riga>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Riga>(list);
                } else {
                    list = (List<Riga>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the rigas from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Riga riga : findAll()) {
            remove(riga);
        }
    }

    /**
     * Returns the number of rigas.
     *
     * @return the number of rigas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_RIGA);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the riga persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.Riga")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Riga>> listenersList = new ArrayList<ModelListener<Riga>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Riga>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(RigaImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
