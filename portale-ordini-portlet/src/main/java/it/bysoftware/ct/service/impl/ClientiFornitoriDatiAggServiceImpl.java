package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.ClientiFornitoriDatiAggServiceBaseImpl;

/**
 * The implementation of the clienti fornitori dati agg remote service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.ClientiFornitoriDatiAggService} interface.
 *
 * <p>This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.ClientiFornitoriDatiAggServiceBaseImpl
 * @see it.bysoftware.ct.service.ClientiFornitoriDatiAggServiceUtil
 */
public class ClientiFornitoriDatiAggServiceImpl extends
        ClientiFornitoriDatiAggServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.ClientiFornitoriDatiAggServiceUtil} to access
     * the clienti fornitori dati agg remote service.
     */
}
