package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchCarrelloException;
import it.bysoftware.ct.model.Carrello;
import it.bysoftware.ct.model.impl.CarrelloImpl;
import it.bysoftware.ct.model.impl.CarrelloModelImpl;
import it.bysoftware.ct.service.persistence.CarrelloPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the carrello service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CarrelloPersistence
 * @see CarrelloUtil
 * @generated
 */
public class CarrelloPersistenceImpl extends BasePersistenceImpl<Carrello>
    implements CarrelloPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link CarrelloUtil} to access the carrello persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = CarrelloImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CarrelloModelImpl.ENTITY_CACHE_ENABLED,
            CarrelloModelImpl.FINDER_CACHE_ENABLED, CarrelloImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CarrelloModelImpl.ENTITY_CACHE_ENABLED,
            CarrelloModelImpl.FINDER_CACHE_ENABLED, CarrelloImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CarrelloModelImpl.ENTITY_CACHE_ENABLED,
            CarrelloModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDORDINE = new FinderPath(CarrelloModelImpl.ENTITY_CACHE_ENABLED,
            CarrelloModelImpl.FINDER_CACHE_ENABLED, CarrelloImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByIdOrdine",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDORDINE =
        new FinderPath(CarrelloModelImpl.ENTITY_CACHE_ENABLED,
            CarrelloModelImpl.FINDER_CACHE_ENABLED, CarrelloImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByIdOrdine",
            new String[] { Long.class.getName() },
            CarrelloModelImpl.IDORDINE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_IDORDINE = new FinderPath(CarrelloModelImpl.ENTITY_CACHE_ENABLED,
            CarrelloModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByIdOrdine",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_IDORDINE_IDORDINE_2 = "carrello.idOrdine = ?";
    private static final String _SQL_SELECT_CARRELLO = "SELECT carrello FROM Carrello carrello";
    private static final String _SQL_SELECT_CARRELLO_WHERE = "SELECT carrello FROM Carrello carrello WHERE ";
    private static final String _SQL_COUNT_CARRELLO = "SELECT COUNT(carrello) FROM Carrello carrello";
    private static final String _SQL_COUNT_CARRELLO_WHERE = "SELECT COUNT(carrello) FROM Carrello carrello WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "carrello.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Carrello exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Carrello exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(CarrelloPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "idOrdine", "progressivo"
            });
    private static Carrello _nullCarrello = new CarrelloImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Carrello> toCacheModel() {
                return _nullCarrelloCacheModel;
            }
        };

    private static CacheModel<Carrello> _nullCarrelloCacheModel = new CacheModel<Carrello>() {
            @Override
            public Carrello toEntityModel() {
                return _nullCarrello;
            }
        };

    public CarrelloPersistenceImpl() {
        setModelClass(Carrello.class);
    }

    /**
     * Returns all the carrellos where idOrdine = &#63;.
     *
     * @param idOrdine the id ordine
     * @return the matching carrellos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Carrello> findByIdOrdine(long idOrdine)
        throws SystemException {
        return findByIdOrdine(idOrdine, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the carrellos where idOrdine = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CarrelloModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param idOrdine the id ordine
     * @param start the lower bound of the range of carrellos
     * @param end the upper bound of the range of carrellos (not inclusive)
     * @return the range of matching carrellos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Carrello> findByIdOrdine(long idOrdine, int start, int end)
        throws SystemException {
        return findByIdOrdine(idOrdine, start, end, null);
    }

    /**
     * Returns an ordered range of all the carrellos where idOrdine = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CarrelloModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param idOrdine the id ordine
     * @param start the lower bound of the range of carrellos
     * @param end the upper bound of the range of carrellos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching carrellos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Carrello> findByIdOrdine(long idOrdine, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDORDINE;
            finderArgs = new Object[] { idOrdine };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDORDINE;
            finderArgs = new Object[] { idOrdine, start, end, orderByComparator };
        }

        List<Carrello> list = (List<Carrello>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Carrello carrello : list) {
                if ((idOrdine != carrello.getIdOrdine())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_CARRELLO_WHERE);

            query.append(_FINDER_COLUMN_IDORDINE_IDORDINE_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(CarrelloModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(idOrdine);

                if (!pagination) {
                    list = (List<Carrello>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Carrello>(list);
                } else {
                    list = (List<Carrello>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first carrello in the ordered set where idOrdine = &#63;.
     *
     * @param idOrdine the id ordine
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching carrello
     * @throws it.bysoftware.ct.NoSuchCarrelloException if a matching carrello could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Carrello findByIdOrdine_First(long idOrdine,
        OrderByComparator orderByComparator)
        throws NoSuchCarrelloException, SystemException {
        Carrello carrello = fetchByIdOrdine_First(idOrdine, orderByComparator);

        if (carrello != null) {
            return carrello;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("idOrdine=");
        msg.append(idOrdine);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchCarrelloException(msg.toString());
    }

    /**
     * Returns the first carrello in the ordered set where idOrdine = &#63;.
     *
     * @param idOrdine the id ordine
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching carrello, or <code>null</code> if a matching carrello could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Carrello fetchByIdOrdine_First(long idOrdine,
        OrderByComparator orderByComparator) throws SystemException {
        List<Carrello> list = findByIdOrdine(idOrdine, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last carrello in the ordered set where idOrdine = &#63;.
     *
     * @param idOrdine the id ordine
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching carrello
     * @throws it.bysoftware.ct.NoSuchCarrelloException if a matching carrello could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Carrello findByIdOrdine_Last(long idOrdine,
        OrderByComparator orderByComparator)
        throws NoSuchCarrelloException, SystemException {
        Carrello carrello = fetchByIdOrdine_Last(idOrdine, orderByComparator);

        if (carrello != null) {
            return carrello;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("idOrdine=");
        msg.append(idOrdine);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchCarrelloException(msg.toString());
    }

    /**
     * Returns the last carrello in the ordered set where idOrdine = &#63;.
     *
     * @param idOrdine the id ordine
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching carrello, or <code>null</code> if a matching carrello could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Carrello fetchByIdOrdine_Last(long idOrdine,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByIdOrdine(idOrdine);

        if (count == 0) {
            return null;
        }

        List<Carrello> list = findByIdOrdine(idOrdine, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the carrellos before and after the current carrello in the ordered set where idOrdine = &#63;.
     *
     * @param id the primary key of the current carrello
     * @param idOrdine the id ordine
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next carrello
     * @throws it.bysoftware.ct.NoSuchCarrelloException if a carrello with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Carrello[] findByIdOrdine_PrevAndNext(long id, long idOrdine,
        OrderByComparator orderByComparator)
        throws NoSuchCarrelloException, SystemException {
        Carrello carrello = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Carrello[] array = new CarrelloImpl[3];

            array[0] = getByIdOrdine_PrevAndNext(session, carrello, idOrdine,
                    orderByComparator, true);

            array[1] = carrello;

            array[2] = getByIdOrdine_PrevAndNext(session, carrello, idOrdine,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Carrello getByIdOrdine_PrevAndNext(Session session,
        Carrello carrello, long idOrdine, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_CARRELLO_WHERE);

        query.append(_FINDER_COLUMN_IDORDINE_IDORDINE_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(CarrelloModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(idOrdine);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(carrello);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Carrello> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the carrellos where idOrdine = &#63; from the database.
     *
     * @param idOrdine the id ordine
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByIdOrdine(long idOrdine) throws SystemException {
        for (Carrello carrello : findByIdOrdine(idOrdine, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(carrello);
        }
    }

    /**
     * Returns the number of carrellos where idOrdine = &#63;.
     *
     * @param idOrdine the id ordine
     * @return the number of matching carrellos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByIdOrdine(long idOrdine) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_IDORDINE;

        Object[] finderArgs = new Object[] { idOrdine };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_CARRELLO_WHERE);

            query.append(_FINDER_COLUMN_IDORDINE_IDORDINE_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(idOrdine);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the carrello in the entity cache if it is enabled.
     *
     * @param carrello the carrello
     */
    @Override
    public void cacheResult(Carrello carrello) {
        EntityCacheUtil.putResult(CarrelloModelImpl.ENTITY_CACHE_ENABLED,
            CarrelloImpl.class, carrello.getPrimaryKey(), carrello);

        carrello.resetOriginalValues();
    }

    /**
     * Caches the carrellos in the entity cache if it is enabled.
     *
     * @param carrellos the carrellos
     */
    @Override
    public void cacheResult(List<Carrello> carrellos) {
        for (Carrello carrello : carrellos) {
            if (EntityCacheUtil.getResult(
                        CarrelloModelImpl.ENTITY_CACHE_ENABLED,
                        CarrelloImpl.class, carrello.getPrimaryKey()) == null) {
                cacheResult(carrello);
            } else {
                carrello.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all carrellos.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(CarrelloImpl.class.getName());
        }

        EntityCacheUtil.clearCache(CarrelloImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the carrello.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Carrello carrello) {
        EntityCacheUtil.removeResult(CarrelloModelImpl.ENTITY_CACHE_ENABLED,
            CarrelloImpl.class, carrello.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Carrello> carrellos) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Carrello carrello : carrellos) {
            EntityCacheUtil.removeResult(CarrelloModelImpl.ENTITY_CACHE_ENABLED,
                CarrelloImpl.class, carrello.getPrimaryKey());
        }
    }

    /**
     * Creates a new carrello with the primary key. Does not add the carrello to the database.
     *
     * @param id the primary key for the new carrello
     * @return the new carrello
     */
    @Override
    public Carrello create(long id) {
        Carrello carrello = new CarrelloImpl();

        carrello.setNew(true);
        carrello.setPrimaryKey(id);

        return carrello;
    }

    /**
     * Removes the carrello with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the carrello
     * @return the carrello that was removed
     * @throws it.bysoftware.ct.NoSuchCarrelloException if a carrello with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Carrello remove(long id)
        throws NoSuchCarrelloException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the carrello with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the carrello
     * @return the carrello that was removed
     * @throws it.bysoftware.ct.NoSuchCarrelloException if a carrello with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Carrello remove(Serializable primaryKey)
        throws NoSuchCarrelloException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Carrello carrello = (Carrello) session.get(CarrelloImpl.class,
                    primaryKey);

            if (carrello == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchCarrelloException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(carrello);
        } catch (NoSuchCarrelloException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Carrello removeImpl(Carrello carrello) throws SystemException {
        carrello = toUnwrappedModel(carrello);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(carrello)) {
                carrello = (Carrello) session.get(CarrelloImpl.class,
                        carrello.getPrimaryKeyObj());
            }

            if (carrello != null) {
                session.delete(carrello);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (carrello != null) {
            clearCache(carrello);
        }

        return carrello;
    }

    @Override
    public Carrello updateImpl(it.bysoftware.ct.model.Carrello carrello)
        throws SystemException {
        carrello = toUnwrappedModel(carrello);

        boolean isNew = carrello.isNew();

        CarrelloModelImpl carrelloModelImpl = (CarrelloModelImpl) carrello;

        Session session = null;

        try {
            session = openSession();

            if (carrello.isNew()) {
                session.save(carrello);

                carrello.setNew(false);
            } else {
                session.merge(carrello);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !CarrelloModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((carrelloModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDORDINE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        carrelloModelImpl.getOriginalIdOrdine()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDORDINE, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDORDINE,
                    args);

                args = new Object[] { carrelloModelImpl.getIdOrdine() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDORDINE, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDORDINE,
                    args);
            }
        }

        EntityCacheUtil.putResult(CarrelloModelImpl.ENTITY_CACHE_ENABLED,
            CarrelloImpl.class, carrello.getPrimaryKey(), carrello);

        return carrello;
    }

    protected Carrello toUnwrappedModel(Carrello carrello) {
        if (carrello instanceof CarrelloImpl) {
            return carrello;
        }

        CarrelloImpl carrelloImpl = new CarrelloImpl();

        carrelloImpl.setNew(carrello.isNew());
        carrelloImpl.setPrimaryKey(carrello.getPrimaryKey());

        carrelloImpl.setId(carrello.getId());
        carrelloImpl.setIdOrdine(carrello.getIdOrdine());
        carrelloImpl.setProgressivo(carrello.getProgressivo());
        carrelloImpl.setImporto(carrello.getImporto());
        carrelloImpl.setChiuso(carrello.isChiuso());
        carrelloImpl.setGenerico(carrello.isGenerico());
        carrelloImpl.setLavorato(carrello.isLavorato());

        return carrelloImpl;
    }

    /**
     * Returns the carrello with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the carrello
     * @return the carrello
     * @throws it.bysoftware.ct.NoSuchCarrelloException if a carrello with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Carrello findByPrimaryKey(Serializable primaryKey)
        throws NoSuchCarrelloException, SystemException {
        Carrello carrello = fetchByPrimaryKey(primaryKey);

        if (carrello == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchCarrelloException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return carrello;
    }

    /**
     * Returns the carrello with the primary key or throws a {@link it.bysoftware.ct.NoSuchCarrelloException} if it could not be found.
     *
     * @param id the primary key of the carrello
     * @return the carrello
     * @throws it.bysoftware.ct.NoSuchCarrelloException if a carrello with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Carrello findByPrimaryKey(long id)
        throws NoSuchCarrelloException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the carrello with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the carrello
     * @return the carrello, or <code>null</code> if a carrello with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Carrello fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Carrello carrello = (Carrello) EntityCacheUtil.getResult(CarrelloModelImpl.ENTITY_CACHE_ENABLED,
                CarrelloImpl.class, primaryKey);

        if (carrello == _nullCarrello) {
            return null;
        }

        if (carrello == null) {
            Session session = null;

            try {
                session = openSession();

                carrello = (Carrello) session.get(CarrelloImpl.class, primaryKey);

                if (carrello != null) {
                    cacheResult(carrello);
                } else {
                    EntityCacheUtil.putResult(CarrelloModelImpl.ENTITY_CACHE_ENABLED,
                        CarrelloImpl.class, primaryKey, _nullCarrello);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(CarrelloModelImpl.ENTITY_CACHE_ENABLED,
                    CarrelloImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return carrello;
    }

    /**
     * Returns the carrello with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the carrello
     * @return the carrello, or <code>null</code> if a carrello with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Carrello fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the carrellos.
     *
     * @return the carrellos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Carrello> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the carrellos.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CarrelloModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of carrellos
     * @param end the upper bound of the range of carrellos (not inclusive)
     * @return the range of carrellos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Carrello> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the carrellos.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CarrelloModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of carrellos
     * @param end the upper bound of the range of carrellos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of carrellos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Carrello> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Carrello> list = (List<Carrello>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_CARRELLO);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_CARRELLO;

                if (pagination) {
                    sql = sql.concat(CarrelloModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Carrello>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Carrello>(list);
                } else {
                    list = (List<Carrello>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the carrellos from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Carrello carrello : findAll()) {
            remove(carrello);
        }
    }

    /**
     * Returns the number of carrellos.
     *
     * @return the number of carrellos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_CARRELLO);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the carrello persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.Carrello")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Carrello>> listenersList = new ArrayList<ModelListener<Carrello>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Carrello>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(CarrelloImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
