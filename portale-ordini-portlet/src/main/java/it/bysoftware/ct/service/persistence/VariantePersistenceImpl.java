package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchVarianteException;
import it.bysoftware.ct.model.Variante;
import it.bysoftware.ct.model.impl.VarianteImpl;
import it.bysoftware.ct.model.impl.VarianteModelImpl;
import it.bysoftware.ct.service.persistence.VariantePersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the variante service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see VariantePersistence
 * @see VarianteUtil
 * @generated
 */
public class VariantePersistenceImpl extends BasePersistenceImpl<Variante>
    implements VariantePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link VarianteUtil} to access the variante persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = VarianteImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VarianteModelImpl.ENTITY_CACHE_ENABLED,
            VarianteModelImpl.FINDER_CACHE_ENABLED, VarianteImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VarianteModelImpl.ENTITY_CACHE_ENABLED,
            VarianteModelImpl.FINDER_CACHE_ENABLED, VarianteImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VarianteModelImpl.ENTITY_CACHE_ENABLED,
            VarianteModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_VARIANTE = "SELECT variante FROM Variante variante";
    private static final String _SQL_COUNT_VARIANTE = "SELECT COUNT(variante) FROM Variante variante";
    private static final String _ORDER_BY_ENTITY_ALIAS = "variante.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Variante exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(VariantePersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "idOpzione"
            });
    private static Variante _nullVariante = new VarianteImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Variante> toCacheModel() {
                return _nullVarianteCacheModel;
            }
        };

    private static CacheModel<Variante> _nullVarianteCacheModel = new CacheModel<Variante>() {
            @Override
            public Variante toEntityModel() {
                return _nullVariante;
            }
        };

    public VariantePersistenceImpl() {
        setModelClass(Variante.class);
    }

    /**
     * Caches the variante in the entity cache if it is enabled.
     *
     * @param variante the variante
     */
    @Override
    public void cacheResult(Variante variante) {
        EntityCacheUtil.putResult(VarianteModelImpl.ENTITY_CACHE_ENABLED,
            VarianteImpl.class, variante.getPrimaryKey(), variante);

        variante.resetOriginalValues();
    }

    /**
     * Caches the variantes in the entity cache if it is enabled.
     *
     * @param variantes the variantes
     */
    @Override
    public void cacheResult(List<Variante> variantes) {
        for (Variante variante : variantes) {
            if (EntityCacheUtil.getResult(
                        VarianteModelImpl.ENTITY_CACHE_ENABLED,
                        VarianteImpl.class, variante.getPrimaryKey()) == null) {
                cacheResult(variante);
            } else {
                variante.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all variantes.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(VarianteImpl.class.getName());
        }

        EntityCacheUtil.clearCache(VarianteImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the variante.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Variante variante) {
        EntityCacheUtil.removeResult(VarianteModelImpl.ENTITY_CACHE_ENABLED,
            VarianteImpl.class, variante.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Variante> variantes) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Variante variante : variantes) {
            EntityCacheUtil.removeResult(VarianteModelImpl.ENTITY_CACHE_ENABLED,
                VarianteImpl.class, variante.getPrimaryKey());
        }
    }

    /**
     * Creates a new variante with the primary key. Does not add the variante to the database.
     *
     * @param id the primary key for the new variante
     * @return the new variante
     */
    @Override
    public Variante create(long id) {
        Variante variante = new VarianteImpl();

        variante.setNew(true);
        variante.setPrimaryKey(id);

        return variante;
    }

    /**
     * Removes the variante with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the variante
     * @return the variante that was removed
     * @throws it.bysoftware.ct.NoSuchVarianteException if a variante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Variante remove(long id)
        throws NoSuchVarianteException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the variante with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the variante
     * @return the variante that was removed
     * @throws it.bysoftware.ct.NoSuchVarianteException if a variante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Variante remove(Serializable primaryKey)
        throws NoSuchVarianteException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Variante variante = (Variante) session.get(VarianteImpl.class,
                    primaryKey);

            if (variante == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchVarianteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(variante);
        } catch (NoSuchVarianteException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Variante removeImpl(Variante variante) throws SystemException {
        variante = toUnwrappedModel(variante);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(variante)) {
                variante = (Variante) session.get(VarianteImpl.class,
                        variante.getPrimaryKeyObj());
            }

            if (variante != null) {
                session.delete(variante);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (variante != null) {
            clearCache(variante);
        }

        return variante;
    }

    @Override
    public Variante updateImpl(it.bysoftware.ct.model.Variante variante)
        throws SystemException {
        variante = toUnwrappedModel(variante);

        boolean isNew = variante.isNew();

        Session session = null;

        try {
            session = openSession();

            if (variante.isNew()) {
                session.save(variante);

                variante.setNew(false);
            } else {
                session.merge(variante);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(VarianteModelImpl.ENTITY_CACHE_ENABLED,
            VarianteImpl.class, variante.getPrimaryKey(), variante);

        return variante;
    }

    protected Variante toUnwrappedModel(Variante variante) {
        if (variante instanceof VarianteImpl) {
            return variante;
        }

        VarianteImpl varianteImpl = new VarianteImpl();

        varianteImpl.setNew(variante.isNew());
        varianteImpl.setPrimaryKey(variante.getPrimaryKey());

        varianteImpl.setId(variante.getId());
        varianteImpl.setIdOpzione(variante.getIdOpzione());
        varianteImpl.setVariante(variante.getVariante());

        return varianteImpl;
    }

    /**
     * Returns the variante with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the variante
     * @return the variante
     * @throws it.bysoftware.ct.NoSuchVarianteException if a variante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Variante findByPrimaryKey(Serializable primaryKey)
        throws NoSuchVarianteException, SystemException {
        Variante variante = fetchByPrimaryKey(primaryKey);

        if (variante == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchVarianteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return variante;
    }

    /**
     * Returns the variante with the primary key or throws a {@link it.bysoftware.ct.NoSuchVarianteException} if it could not be found.
     *
     * @param id the primary key of the variante
     * @return the variante
     * @throws it.bysoftware.ct.NoSuchVarianteException if a variante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Variante findByPrimaryKey(long id)
        throws NoSuchVarianteException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the variante with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the variante
     * @return the variante, or <code>null</code> if a variante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Variante fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Variante variante = (Variante) EntityCacheUtil.getResult(VarianteModelImpl.ENTITY_CACHE_ENABLED,
                VarianteImpl.class, primaryKey);

        if (variante == _nullVariante) {
            return null;
        }

        if (variante == null) {
            Session session = null;

            try {
                session = openSession();

                variante = (Variante) session.get(VarianteImpl.class, primaryKey);

                if (variante != null) {
                    cacheResult(variante);
                } else {
                    EntityCacheUtil.putResult(VarianteModelImpl.ENTITY_CACHE_ENABLED,
                        VarianteImpl.class, primaryKey, _nullVariante);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(VarianteModelImpl.ENTITY_CACHE_ENABLED,
                    VarianteImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return variante;
    }

    /**
     * Returns the variante with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the variante
     * @return the variante, or <code>null</code> if a variante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Variante fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the variantes.
     *
     * @return the variantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Variante> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the variantes.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VarianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of variantes
     * @param end the upper bound of the range of variantes (not inclusive)
     * @return the range of variantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Variante> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the variantes.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VarianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of variantes
     * @param end the upper bound of the range of variantes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of variantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Variante> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Variante> list = (List<Variante>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_VARIANTE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_VARIANTE;

                if (pagination) {
                    sql = sql.concat(VarianteModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Variante>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Variante>(list);
                } else {
                    list = (List<Variante>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the variantes from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Variante variante : findAll()) {
            remove(variante);
        }
    }

    /**
     * Returns the number of variantes.
     *
     * @return the number of variantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_VARIANTE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the variante persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.Variante")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Variante>> listenersList = new ArrayList<ModelListener<Variante>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Variante>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(VarianteImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
