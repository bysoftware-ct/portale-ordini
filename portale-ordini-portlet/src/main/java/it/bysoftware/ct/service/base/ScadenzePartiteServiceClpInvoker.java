package it.bysoftware.ct.service.base;

import it.bysoftware.ct.service.ScadenzePartiteServiceUtil;

import java.util.Arrays;

/**
 * @author Mario Torrisi
 * @generated
 */
public class ScadenzePartiteServiceClpInvoker {
    private String _methodName222;
    private String[] _methodParameterTypes222;
    private String _methodName223;
    private String[] _methodParameterTypes223;

    public ScadenzePartiteServiceClpInvoker() {
        _methodName222 = "getBeanIdentifier";

        _methodParameterTypes222 = new String[] {  };

        _methodName223 = "setBeanIdentifier";

        _methodParameterTypes223 = new String[] { "java.lang.String" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName222.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes222, parameterTypes)) {
            return ScadenzePartiteServiceUtil.getBeanIdentifier();
        }

        if (_methodName223.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes223, parameterTypes)) {
            ScadenzePartiteServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        throw new UnsupportedOperationException();
    }
}
