package it.bysoftware.ct.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.util.ArrayList;
import java.util.List;

import it.bysoftware.ct.model.SchedaPagamento;
import it.bysoftware.ct.service.base.SchedaPagamentoLocalServiceBaseImpl;

/**
 * The implementation of the scheda pagamento local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.SchedaPagamentoLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.SchedaPagamentoLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.SchedaPagamentoLocalServiceUtil
 */
public class SchedaPagamentoLocalServiceImpl extends
        SchedaPagamentoLocalServiceBaseImpl {
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil
            .getLog(SchedaPagamentoLocalServiceImpl.class);
    
    @Override
    public final List<SchedaPagamento> findByCustomerYearAndIdPayment(
            final String partnerCode, final int year, final int idPagamento) {
        return this.findByCustomerYearIdPaymentAndState(partnerCode, year,
                idPagamento, null);
    }
    
    @Override
    public final List<SchedaPagamento> findByCustomerYearIdPaymentAndState(
            final String partnerCode, final int year, final int idPagamento,
            final Integer state) {

        List<SchedaPagamento> result = new ArrayList<SchedaPagamento>();

        try {
            if (state != null) {
                result = this.schedaPagamentoPersistence.
                        findByCodiceFornitoreAnnoIdPagamentoStato(partnerCode,
                                year, idPagamento, state);
            } else {
                result = this.schedaPagamentoPersistence.
                        findByCodiceFornitoreAnnoIdPagamento(partnerCode, year,
                                idPagamento);
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        return result;
    }
    
}
