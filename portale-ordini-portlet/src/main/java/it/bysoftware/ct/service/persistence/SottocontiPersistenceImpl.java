package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchSottocontiException;
import it.bysoftware.ct.model.Sottoconti;
import it.bysoftware.ct.model.impl.SottocontiImpl;
import it.bysoftware.ct.model.impl.SottocontiModelImpl;
import it.bysoftware.ct.service.persistence.SottocontiPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the sottoconti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see SottocontiPersistence
 * @see SottocontiUtil
 * @generated
 */
public class SottocontiPersistenceImpl extends BasePersistenceImpl<Sottoconti>
    implements SottocontiPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link SottocontiUtil} to access the sottoconti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = SottocontiImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(SottocontiModelImpl.ENTITY_CACHE_ENABLED,
            SottocontiModelImpl.FINDER_CACHE_ENABLED, SottocontiImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(SottocontiModelImpl.ENTITY_CACHE_ENABLED,
            SottocontiModelImpl.FINDER_CACHE_ENABLED, SottocontiImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(SottocontiModelImpl.ENTITY_CACHE_ENABLED,
            SottocontiModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_SOTTOCONTI = "SELECT sottoconti FROM Sottoconti sottoconti";
    private static final String _SQL_COUNT_SOTTOCONTI = "SELECT COUNT(sottoconti) FROM Sottoconti sottoconti";
    private static final String _ORDER_BY_ENTITY_ALIAS = "sottoconti.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Sottoconti exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(SottocontiPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "codice", "descrizione"
            });
    private static Sottoconti _nullSottoconti = new SottocontiImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Sottoconti> toCacheModel() {
                return _nullSottocontiCacheModel;
            }
        };

    private static CacheModel<Sottoconti> _nullSottocontiCacheModel = new CacheModel<Sottoconti>() {
            @Override
            public Sottoconti toEntityModel() {
                return _nullSottoconti;
            }
        };

    public SottocontiPersistenceImpl() {
        setModelClass(Sottoconti.class);
    }

    /**
     * Caches the sottoconti in the entity cache if it is enabled.
     *
     * @param sottoconti the sottoconti
     */
    @Override
    public void cacheResult(Sottoconti sottoconti) {
        EntityCacheUtil.putResult(SottocontiModelImpl.ENTITY_CACHE_ENABLED,
            SottocontiImpl.class, sottoconti.getPrimaryKey(), sottoconti);

        sottoconti.resetOriginalValues();
    }

    /**
     * Caches the sottocontis in the entity cache if it is enabled.
     *
     * @param sottocontis the sottocontis
     */
    @Override
    public void cacheResult(List<Sottoconti> sottocontis) {
        for (Sottoconti sottoconti : sottocontis) {
            if (EntityCacheUtil.getResult(
                        SottocontiModelImpl.ENTITY_CACHE_ENABLED,
                        SottocontiImpl.class, sottoconti.getPrimaryKey()) == null) {
                cacheResult(sottoconti);
            } else {
                sottoconti.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all sottocontis.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(SottocontiImpl.class.getName());
        }

        EntityCacheUtil.clearCache(SottocontiImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the sottoconti.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Sottoconti sottoconti) {
        EntityCacheUtil.removeResult(SottocontiModelImpl.ENTITY_CACHE_ENABLED,
            SottocontiImpl.class, sottoconti.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Sottoconti> sottocontis) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Sottoconti sottoconti : sottocontis) {
            EntityCacheUtil.removeResult(SottocontiModelImpl.ENTITY_CACHE_ENABLED,
                SottocontiImpl.class, sottoconti.getPrimaryKey());
        }
    }

    /**
     * Creates a new sottoconti with the primary key. Does not add the sottoconti to the database.
     *
     * @param codice the primary key for the new sottoconti
     * @return the new sottoconti
     */
    @Override
    public Sottoconti create(String codice) {
        Sottoconti sottoconti = new SottocontiImpl();

        sottoconti.setNew(true);
        sottoconti.setPrimaryKey(codice);

        return sottoconti;
    }

    /**
     * Removes the sottoconti with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param codice the primary key of the sottoconti
     * @return the sottoconti that was removed
     * @throws it.bysoftware.ct.NoSuchSottocontiException if a sottoconti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Sottoconti remove(String codice)
        throws NoSuchSottocontiException, SystemException {
        return remove((Serializable) codice);
    }

    /**
     * Removes the sottoconti with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the sottoconti
     * @return the sottoconti that was removed
     * @throws it.bysoftware.ct.NoSuchSottocontiException if a sottoconti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Sottoconti remove(Serializable primaryKey)
        throws NoSuchSottocontiException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Sottoconti sottoconti = (Sottoconti) session.get(SottocontiImpl.class,
                    primaryKey);

            if (sottoconti == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchSottocontiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(sottoconti);
        } catch (NoSuchSottocontiException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Sottoconti removeImpl(Sottoconti sottoconti)
        throws SystemException {
        sottoconti = toUnwrappedModel(sottoconti);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(sottoconti)) {
                sottoconti = (Sottoconti) session.get(SottocontiImpl.class,
                        sottoconti.getPrimaryKeyObj());
            }

            if (sottoconti != null) {
                session.delete(sottoconti);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (sottoconti != null) {
            clearCache(sottoconti);
        }

        return sottoconti;
    }

    @Override
    public Sottoconti updateImpl(it.bysoftware.ct.model.Sottoconti sottoconti)
        throws SystemException {
        sottoconti = toUnwrappedModel(sottoconti);

        boolean isNew = sottoconti.isNew();

        Session session = null;

        try {
            session = openSession();

            if (sottoconti.isNew()) {
                session.save(sottoconti);

                sottoconti.setNew(false);
            } else {
                session.merge(sottoconti);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(SottocontiModelImpl.ENTITY_CACHE_ENABLED,
            SottocontiImpl.class, sottoconti.getPrimaryKey(), sottoconti);

        return sottoconti;
    }

    protected Sottoconti toUnwrappedModel(Sottoconti sottoconti) {
        if (sottoconti instanceof SottocontiImpl) {
            return sottoconti;
        }

        SottocontiImpl sottocontiImpl = new SottocontiImpl();

        sottocontiImpl.setNew(sottoconti.isNew());
        sottocontiImpl.setPrimaryKey(sottoconti.getPrimaryKey());

        sottocontiImpl.setCodice(sottoconti.getCodice());
        sottocontiImpl.setDescrizione(sottoconti.getDescrizione());

        return sottocontiImpl;
    }

    /**
     * Returns the sottoconti with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the sottoconti
     * @return the sottoconti
     * @throws it.bysoftware.ct.NoSuchSottocontiException if a sottoconti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Sottoconti findByPrimaryKey(Serializable primaryKey)
        throws NoSuchSottocontiException, SystemException {
        Sottoconti sottoconti = fetchByPrimaryKey(primaryKey);

        if (sottoconti == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchSottocontiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return sottoconti;
    }

    /**
     * Returns the sottoconti with the primary key or throws a {@link it.bysoftware.ct.NoSuchSottocontiException} if it could not be found.
     *
     * @param codice the primary key of the sottoconti
     * @return the sottoconti
     * @throws it.bysoftware.ct.NoSuchSottocontiException if a sottoconti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Sottoconti findByPrimaryKey(String codice)
        throws NoSuchSottocontiException, SystemException {
        return findByPrimaryKey((Serializable) codice);
    }

    /**
     * Returns the sottoconti with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the sottoconti
     * @return the sottoconti, or <code>null</code> if a sottoconti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Sottoconti fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Sottoconti sottoconti = (Sottoconti) EntityCacheUtil.getResult(SottocontiModelImpl.ENTITY_CACHE_ENABLED,
                SottocontiImpl.class, primaryKey);

        if (sottoconti == _nullSottoconti) {
            return null;
        }

        if (sottoconti == null) {
            Session session = null;

            try {
                session = openSession();

                sottoconti = (Sottoconti) session.get(SottocontiImpl.class,
                        primaryKey);

                if (sottoconti != null) {
                    cacheResult(sottoconti);
                } else {
                    EntityCacheUtil.putResult(SottocontiModelImpl.ENTITY_CACHE_ENABLED,
                        SottocontiImpl.class, primaryKey, _nullSottoconti);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(SottocontiModelImpl.ENTITY_CACHE_ENABLED,
                    SottocontiImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return sottoconti;
    }

    /**
     * Returns the sottoconti with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param codice the primary key of the sottoconti
     * @return the sottoconti, or <code>null</code> if a sottoconti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Sottoconti fetchByPrimaryKey(String codice)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) codice);
    }

    /**
     * Returns all the sottocontis.
     *
     * @return the sottocontis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Sottoconti> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the sottocontis.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SottocontiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of sottocontis
     * @param end the upper bound of the range of sottocontis (not inclusive)
     * @return the range of sottocontis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Sottoconti> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the sottocontis.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SottocontiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of sottocontis
     * @param end the upper bound of the range of sottocontis (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of sottocontis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Sottoconti> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Sottoconti> list = (List<Sottoconti>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_SOTTOCONTI);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_SOTTOCONTI;

                if (pagination) {
                    sql = sql.concat(SottocontiModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Sottoconti>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Sottoconti>(list);
                } else {
                    list = (List<Sottoconti>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the sottocontis from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Sottoconti sottoconti : findAll()) {
            remove(sottoconti);
        }
    }

    /**
     * Returns the number of sottocontis.
     *
     * @return the number of sottocontis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_SOTTOCONTI);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the sottoconti persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.Sottoconti")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Sottoconti>> listenersList = new ArrayList<ModelListener<Sottoconti>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Sottoconti>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(SottocontiImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
