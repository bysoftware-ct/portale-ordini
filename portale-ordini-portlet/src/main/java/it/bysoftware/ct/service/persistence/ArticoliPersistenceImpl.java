package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchArticoliException;
import it.bysoftware.ct.model.Articoli;
import it.bysoftware.ct.model.impl.ArticoliImpl;
import it.bysoftware.ct.model.impl.ArticoliModelImpl;
import it.bysoftware.ct.service.persistence.ArticoliPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the articoli service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see ArticoliPersistence
 * @see ArticoliUtil
 * @generated
 */
public class ArticoliPersistenceImpl extends BasePersistenceImpl<Articoli>
    implements ArticoliPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link ArticoliUtil} to access the articoli persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = ArticoliImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ArticoliModelImpl.ENTITY_CACHE_ENABLED,
            ArticoliModelImpl.FINDER_CACHE_ENABLED, ArticoliImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ArticoliModelImpl.ENTITY_CACHE_ENABLED,
            ArticoliModelImpl.FINDER_CACHE_ENABLED, ArticoliImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ArticoliModelImpl.ENTITY_CACHE_ENABLED,
            ArticoliModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICE = new FinderPath(ArticoliModelImpl.ENTITY_CACHE_ENABLED,
            ArticoliModelImpl.FINDER_CACHE_ENABLED, ArticoliImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCodice",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_CODICE = new FinderPath(ArticoliModelImpl.ENTITY_CACHE_ENABLED,
            ArticoliModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByCodice",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_CODICE_CODICEARTICOLO_1 = "articoli.codiceArticolo LIKE NULL";
    private static final String _FINDER_COLUMN_CODICE_CODICEARTICOLO_2 = "articoli.codiceArticolo LIKE ?";
    private static final String _FINDER_COLUMN_CODICE_CODICEARTICOLO_3 = "(articoli.codiceArticolo IS NULL OR articoli.codiceArticolo LIKE '')";
    private static final String _SQL_SELECT_ARTICOLI = "SELECT articoli FROM Articoli articoli";
    private static final String _SQL_SELECT_ARTICOLI_WHERE = "SELECT articoli FROM Articoli articoli WHERE ";
    private static final String _SQL_COUNT_ARTICOLI = "SELECT COUNT(articoli) FROM Articoli articoli";
    private static final String _SQL_COUNT_ARTICOLI_WHERE = "SELECT COUNT(articoli) FROM Articoli articoli WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "articoli.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Articoli exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Articoli exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(ArticoliPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "codiceArticolo", "categoriaMerceologica", "categoriaInventario",
                "descrizione", "descrizioneDocumento", "descrizioneFiscale",
                "unitaMisura", "tara", "codiceIVA", "prezzo1", "prezzo2",
                "prezzo3", "libLng1", "libStr1", "obsoleto", "tipoArticolo",
                "codiceProvvigione", "pesoLordo"
            });
    private static Articoli _nullArticoli = new ArticoliImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Articoli> toCacheModel() {
                return _nullArticoliCacheModel;
            }
        };

    private static CacheModel<Articoli> _nullArticoliCacheModel = new CacheModel<Articoli>() {
            @Override
            public Articoli toEntityModel() {
                return _nullArticoli;
            }
        };

    public ArticoliPersistenceImpl() {
        setModelClass(Articoli.class);
    }

    /**
     * Returns all the articolis where codiceArticolo LIKE &#63;.
     *
     * @param codiceArticolo the codice articolo
     * @return the matching articolis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Articoli> findByCodice(String codiceArticolo)
        throws SystemException {
        return findByCodice(codiceArticolo, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the articolis where codiceArticolo LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceArticolo the codice articolo
     * @param start the lower bound of the range of articolis
     * @param end the upper bound of the range of articolis (not inclusive)
     * @return the range of matching articolis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Articoli> findByCodice(String codiceArticolo, int start, int end)
        throws SystemException {
        return findByCodice(codiceArticolo, start, end, null);
    }

    /**
     * Returns an ordered range of all the articolis where codiceArticolo LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceArticolo the codice articolo
     * @param start the lower bound of the range of articolis
     * @param end the upper bound of the range of articolis (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching articolis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Articoli> findByCodice(String codiceArticolo, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICE;
        finderArgs = new Object[] { codiceArticolo, start, end, orderByComparator };

        List<Articoli> list = (List<Articoli>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Articoli articoli : list) {
                if (!StringUtil.wildcardMatches(articoli.getCodiceArticolo(),
                            codiceArticolo, CharPool.UNDERLINE,
                            CharPool.PERCENT, CharPool.BACK_SLASH, true)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_ARTICOLI_WHERE);

            boolean bindCodiceArticolo = false;

            if (codiceArticolo == null) {
                query.append(_FINDER_COLUMN_CODICE_CODICEARTICOLO_1);
            } else if (codiceArticolo.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICE_CODICEARTICOLO_3);
            } else {
                bindCodiceArticolo = true;

                query.append(_FINDER_COLUMN_CODICE_CODICEARTICOLO_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(ArticoliModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceArticolo) {
                    qPos.add(codiceArticolo);
                }

                if (!pagination) {
                    list = (List<Articoli>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Articoli>(list);
                } else {
                    list = (List<Articoli>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first articoli in the ordered set where codiceArticolo LIKE &#63;.
     *
     * @param codiceArticolo the codice articolo
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching articoli
     * @throws it.bysoftware.ct.NoSuchArticoliException if a matching articoli could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Articoli findByCodice_First(String codiceArticolo,
        OrderByComparator orderByComparator)
        throws NoSuchArticoliException, SystemException {
        Articoli articoli = fetchByCodice_First(codiceArticolo,
                orderByComparator);

        if (articoli != null) {
            return articoli;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceArticolo=");
        msg.append(codiceArticolo);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchArticoliException(msg.toString());
    }

    /**
     * Returns the first articoli in the ordered set where codiceArticolo LIKE &#63;.
     *
     * @param codiceArticolo the codice articolo
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching articoli, or <code>null</code> if a matching articoli could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Articoli fetchByCodice_First(String codiceArticolo,
        OrderByComparator orderByComparator) throws SystemException {
        List<Articoli> list = findByCodice(codiceArticolo, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last articoli in the ordered set where codiceArticolo LIKE &#63;.
     *
     * @param codiceArticolo the codice articolo
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching articoli
     * @throws it.bysoftware.ct.NoSuchArticoliException if a matching articoli could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Articoli findByCodice_Last(String codiceArticolo,
        OrderByComparator orderByComparator)
        throws NoSuchArticoliException, SystemException {
        Articoli articoli = fetchByCodice_Last(codiceArticolo, orderByComparator);

        if (articoli != null) {
            return articoli;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceArticolo=");
        msg.append(codiceArticolo);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchArticoliException(msg.toString());
    }

    /**
     * Returns the last articoli in the ordered set where codiceArticolo LIKE &#63;.
     *
     * @param codiceArticolo the codice articolo
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching articoli, or <code>null</code> if a matching articoli could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Articoli fetchByCodice_Last(String codiceArticolo,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByCodice(codiceArticolo);

        if (count == 0) {
            return null;
        }

        List<Articoli> list = findByCodice(codiceArticolo, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Removes all the articolis where codiceArticolo LIKE &#63; from the database.
     *
     * @param codiceArticolo the codice articolo
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByCodice(String codiceArticolo) throws SystemException {
        for (Articoli articoli : findByCodice(codiceArticolo,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(articoli);
        }
    }

    /**
     * Returns the number of articolis where codiceArticolo LIKE &#63;.
     *
     * @param codiceArticolo the codice articolo
     * @return the number of matching articolis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByCodice(String codiceArticolo) throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_CODICE;

        Object[] finderArgs = new Object[] { codiceArticolo };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_ARTICOLI_WHERE);

            boolean bindCodiceArticolo = false;

            if (codiceArticolo == null) {
                query.append(_FINDER_COLUMN_CODICE_CODICEARTICOLO_1);
            } else if (codiceArticolo.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICE_CODICEARTICOLO_3);
            } else {
                bindCodiceArticolo = true;

                query.append(_FINDER_COLUMN_CODICE_CODICEARTICOLO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceArticolo) {
                    qPos.add(codiceArticolo);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the articoli in the entity cache if it is enabled.
     *
     * @param articoli the articoli
     */
    @Override
    public void cacheResult(Articoli articoli) {
        EntityCacheUtil.putResult(ArticoliModelImpl.ENTITY_CACHE_ENABLED,
            ArticoliImpl.class, articoli.getPrimaryKey(), articoli);

        articoli.resetOriginalValues();
    }

    /**
     * Caches the articolis in the entity cache if it is enabled.
     *
     * @param articolis the articolis
     */
    @Override
    public void cacheResult(List<Articoli> articolis) {
        for (Articoli articoli : articolis) {
            if (EntityCacheUtil.getResult(
                        ArticoliModelImpl.ENTITY_CACHE_ENABLED,
                        ArticoliImpl.class, articoli.getPrimaryKey()) == null) {
                cacheResult(articoli);
            } else {
                articoli.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all articolis.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(ArticoliImpl.class.getName());
        }

        EntityCacheUtil.clearCache(ArticoliImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the articoli.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Articoli articoli) {
        EntityCacheUtil.removeResult(ArticoliModelImpl.ENTITY_CACHE_ENABLED,
            ArticoliImpl.class, articoli.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Articoli> articolis) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Articoli articoli : articolis) {
            EntityCacheUtil.removeResult(ArticoliModelImpl.ENTITY_CACHE_ENABLED,
                ArticoliImpl.class, articoli.getPrimaryKey());
        }
    }

    /**
     * Creates a new articoli with the primary key. Does not add the articoli to the database.
     *
     * @param codiceArticolo the primary key for the new articoli
     * @return the new articoli
     */
    @Override
    public Articoli create(String codiceArticolo) {
        Articoli articoli = new ArticoliImpl();

        articoli.setNew(true);
        articoli.setPrimaryKey(codiceArticolo);

        return articoli;
    }

    /**
     * Removes the articoli with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param codiceArticolo the primary key of the articoli
     * @return the articoli that was removed
     * @throws it.bysoftware.ct.NoSuchArticoliException if a articoli with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Articoli remove(String codiceArticolo)
        throws NoSuchArticoliException, SystemException {
        return remove((Serializable) codiceArticolo);
    }

    /**
     * Removes the articoli with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the articoli
     * @return the articoli that was removed
     * @throws it.bysoftware.ct.NoSuchArticoliException if a articoli with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Articoli remove(Serializable primaryKey)
        throws NoSuchArticoliException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Articoli articoli = (Articoli) session.get(ArticoliImpl.class,
                    primaryKey);

            if (articoli == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchArticoliException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(articoli);
        } catch (NoSuchArticoliException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Articoli removeImpl(Articoli articoli) throws SystemException {
        articoli = toUnwrappedModel(articoli);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(articoli)) {
                articoli = (Articoli) session.get(ArticoliImpl.class,
                        articoli.getPrimaryKeyObj());
            }

            if (articoli != null) {
                session.delete(articoli);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (articoli != null) {
            clearCache(articoli);
        }

        return articoli;
    }

    @Override
    public Articoli updateImpl(it.bysoftware.ct.model.Articoli articoli)
        throws SystemException {
        articoli = toUnwrappedModel(articoli);

        boolean isNew = articoli.isNew();

        Session session = null;

        try {
            session = openSession();

            if (articoli.isNew()) {
                session.save(articoli);

                articoli.setNew(false);
            } else {
                session.merge(articoli);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !ArticoliModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(ArticoliModelImpl.ENTITY_CACHE_ENABLED,
            ArticoliImpl.class, articoli.getPrimaryKey(), articoli);

        return articoli;
    }

    protected Articoli toUnwrappedModel(Articoli articoli) {
        if (articoli instanceof ArticoliImpl) {
            return articoli;
        }

        ArticoliImpl articoliImpl = new ArticoliImpl();

        articoliImpl.setNew(articoli.isNew());
        articoliImpl.setPrimaryKey(articoli.getPrimaryKey());

        articoliImpl.setCodiceArticolo(articoli.getCodiceArticolo());
        articoliImpl.setCategoriaMerceologica(articoli.getCategoriaMerceologica());
        articoliImpl.setCategoriaInventario(articoli.getCategoriaInventario());
        articoliImpl.setDescrizione(articoli.getDescrizione());
        articoliImpl.setDescrizioneDocumento(articoli.getDescrizioneDocumento());
        articoliImpl.setDescrizioneFiscale(articoli.getDescrizioneFiscale());
        articoliImpl.setUnitaMisura(articoli.getUnitaMisura());
        articoliImpl.setTara(articoli.getTara());
        articoliImpl.setCodiceIVA(articoli.getCodiceIVA());
        articoliImpl.setPrezzo1(articoli.getPrezzo1());
        articoliImpl.setPrezzo2(articoli.getPrezzo2());
        articoliImpl.setPrezzo3(articoli.getPrezzo3());
        articoliImpl.setLibLng1(articoli.getLibLng1());
        articoliImpl.setLibStr1(articoli.getLibStr1());
        articoliImpl.setObsoleto(articoli.isObsoleto());
        articoliImpl.setTipoArticolo(articoli.getTipoArticolo());
        articoliImpl.setCodiceProvvigione(articoli.getCodiceProvvigione());
        articoliImpl.setPesoLordo(articoli.getPesoLordo());

        return articoliImpl;
    }

    /**
     * Returns the articoli with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the articoli
     * @return the articoli
     * @throws it.bysoftware.ct.NoSuchArticoliException if a articoli with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Articoli findByPrimaryKey(Serializable primaryKey)
        throws NoSuchArticoliException, SystemException {
        Articoli articoli = fetchByPrimaryKey(primaryKey);

        if (articoli == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchArticoliException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return articoli;
    }

    /**
     * Returns the articoli with the primary key or throws a {@link it.bysoftware.ct.NoSuchArticoliException} if it could not be found.
     *
     * @param codiceArticolo the primary key of the articoli
     * @return the articoli
     * @throws it.bysoftware.ct.NoSuchArticoliException if a articoli with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Articoli findByPrimaryKey(String codiceArticolo)
        throws NoSuchArticoliException, SystemException {
        return findByPrimaryKey((Serializable) codiceArticolo);
    }

    /**
     * Returns the articoli with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the articoli
     * @return the articoli, or <code>null</code> if a articoli with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Articoli fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Articoli articoli = (Articoli) EntityCacheUtil.getResult(ArticoliModelImpl.ENTITY_CACHE_ENABLED,
                ArticoliImpl.class, primaryKey);

        if (articoli == _nullArticoli) {
            return null;
        }

        if (articoli == null) {
            Session session = null;

            try {
                session = openSession();

                articoli = (Articoli) session.get(ArticoliImpl.class, primaryKey);

                if (articoli != null) {
                    cacheResult(articoli);
                } else {
                    EntityCacheUtil.putResult(ArticoliModelImpl.ENTITY_CACHE_ENABLED,
                        ArticoliImpl.class, primaryKey, _nullArticoli);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(ArticoliModelImpl.ENTITY_CACHE_ENABLED,
                    ArticoliImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return articoli;
    }

    /**
     * Returns the articoli with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param codiceArticolo the primary key of the articoli
     * @return the articoli, or <code>null</code> if a articoli with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Articoli fetchByPrimaryKey(String codiceArticolo)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) codiceArticolo);
    }

    /**
     * Returns all the articolis.
     *
     * @return the articolis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Articoli> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the articolis.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of articolis
     * @param end the upper bound of the range of articolis (not inclusive)
     * @return the range of articolis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Articoli> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the articolis.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of articolis
     * @param end the upper bound of the range of articolis (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of articolis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Articoli> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Articoli> list = (List<Articoli>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_ARTICOLI);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_ARTICOLI;

                if (pagination) {
                    sql = sql.concat(ArticoliModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Articoli>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Articoli>(list);
                } else {
                    list = (List<Articoli>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the articolis from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Articoli articoli : findAll()) {
            remove(articoli);
        }
    }

    /**
     * Returns the number of articolis.
     *
     * @return the number of articolis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_ARTICOLI);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the articoli persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.Articoli")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Articoli>> listenersList = new ArrayList<ModelListener<Articoli>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Articoli>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(ArticoliImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
