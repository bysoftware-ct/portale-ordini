package it.bysoftware.ct.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.util.ArrayList;
import java.util.List;

import it.bysoftware.ct.model.RigheFattureVedita;
import it.bysoftware.ct.model.TestataFattureClienti;
import it.bysoftware.ct.service.base.RigheFattureVeditaLocalServiceBaseImpl;

/**
 * The implementation of the righe fatture vedita local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.RigheFattureVeditaLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.RigheFattureVeditaLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.RigheFattureVeditaLocalServiceUtil
 */
public class RigheFattureVeditaLocalServiceImpl extends
        RigheFattureVeditaLocalServiceBaseImpl {
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(RigheFattureVedita.class);

    @Override
    public final List<RigheFattureVedita> getRigheFatturaByTestata(
            final TestataFattureClienti testata) {
        try {
            return this.righeFattureVeditaPersistence.findByTestataFattura(
                    testata.getAnno(), testata.getCodiceAttivita(),
                    testata.getCodiceCentro(), testata.getNumeroProtocollo());
        } catch (SystemException e) {
            e.printStackTrace();
            this.logger.error(e.getMessage());
            return new ArrayList<RigheFattureVedita>();
        }
    }
}
