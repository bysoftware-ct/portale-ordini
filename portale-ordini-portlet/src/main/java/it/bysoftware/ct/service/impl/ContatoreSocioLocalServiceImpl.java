package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.ContatoreSocioLocalServiceBaseImpl;

/**
 * The implementation of the contatore socio local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.bysoftware.ct.service.ContatoreSocioLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.ContatoreSocioLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.ContatoreSocioLocalServiceUtil
 */
public class ContatoreSocioLocalServiceImpl
    extends ContatoreSocioLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link it.bysoftware.ct.service.ContatoreSocioLocalServiceUtil} to access the contatore socio local service.
     */
}
