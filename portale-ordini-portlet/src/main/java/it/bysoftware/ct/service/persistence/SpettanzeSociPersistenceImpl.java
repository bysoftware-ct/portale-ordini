package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchSpettanzeSociException;
import it.bysoftware.ct.model.SpettanzeSoci;
import it.bysoftware.ct.model.impl.SpettanzeSociImpl;
import it.bysoftware.ct.model.impl.SpettanzeSociModelImpl;
import it.bysoftware.ct.service.persistence.SpettanzeSociPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the spettanze soci service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see SpettanzeSociPersistence
 * @see SpettanzeSociUtil
 * @generated
 */
public class SpettanzeSociPersistenceImpl extends BasePersistenceImpl<SpettanzeSoci>
    implements SpettanzeSociPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link SpettanzeSociUtil} to access the spettanze soci persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = SpettanzeSociImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociModelImpl.FINDER_CACHE_ENABLED,
            SpettanzeSociImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociModelImpl.FINDER_CACHE_ENABLED,
            SpettanzeSociImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_FETCH_BY_FATTURAVENDITASTATO = new FinderPath(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociModelImpl.FINDER_CACHE_ENABLED,
            SpettanzeSociImpl.class, FINDER_CLASS_NAME_ENTITY,
            "fetchByFatturaVenditaStato",
            new String[] {
                Integer.class.getName(), String.class.getName(),
                String.class.getName(), Integer.class.getName(),
                String.class.getName(), Integer.class.getName()
            },
            SpettanzeSociModelImpl.ANNO_COLUMN_BITMASK |
            SpettanzeSociModelImpl.CODICEATTIVITA_COLUMN_BITMASK |
            SpettanzeSociModelImpl.CODICECENTRO_COLUMN_BITMASK |
            SpettanzeSociModelImpl.NUMEROPROTOCOLLO_COLUMN_BITMASK |
            SpettanzeSociModelImpl.CODICEFORNITORE_COLUMN_BITMASK |
            SpettanzeSociModelImpl.STATO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_FATTURAVENDITASTATO = new FinderPath(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByFatturaVenditaStato",
            new String[] {
                Integer.class.getName(), String.class.getName(),
                String.class.getName(), Integer.class.getName(),
                String.class.getName(), Integer.class.getName()
            });
    private static final String _FINDER_COLUMN_FATTURAVENDITASTATO_ANNO_2 = "spettanzeSoci.anno = ? AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITASTATO_CODICEATTIVITA_1 =
        "spettanzeSoci.codiceAttivita IS NULL AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITASTATO_CODICEATTIVITA_2 =
        "spettanzeSoci.codiceAttivita = ? AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITASTATO_CODICEATTIVITA_3 =
        "(spettanzeSoci.codiceAttivita IS NULL OR spettanzeSoci.codiceAttivita = '') AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITASTATO_CODICECENTRO_1 =
        "spettanzeSoci.codiceCentro IS NULL AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITASTATO_CODICECENTRO_2 =
        "spettanzeSoci.codiceCentro = ? AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITASTATO_CODICECENTRO_3 =
        "(spettanzeSoci.codiceCentro IS NULL OR spettanzeSoci.codiceCentro = '') AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITASTATO_NUMEROPROTOCOLLO_2 =
        "spettanzeSoci.numeroProtocollo = ? AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITASTATO_CODICEFORNITORE_1 =
        "spettanzeSoci.codiceFornitore IS NULL AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITASTATO_CODICEFORNITORE_2 =
        "spettanzeSoci.codiceFornitore = ? AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITASTATO_CODICEFORNITORE_3 =
        "(spettanzeSoci.codiceFornitore IS NULL OR spettanzeSoci.codiceFornitore = '') AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITASTATO_STATO_2 = "spettanzeSoci.stato = ?";
    public static final FinderPath FINDER_PATH_FETCH_BY_FATTURAVENDITA = new FinderPath(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociModelImpl.FINDER_CACHE_ENABLED,
            SpettanzeSociImpl.class, FINDER_CLASS_NAME_ENTITY,
            "fetchByFatturaVendita",
            new String[] {
                Integer.class.getName(), String.class.getName(),
                String.class.getName(), Integer.class.getName(),
                String.class.getName()
            },
            SpettanzeSociModelImpl.ANNO_COLUMN_BITMASK |
            SpettanzeSociModelImpl.CODICEATTIVITA_COLUMN_BITMASK |
            SpettanzeSociModelImpl.CODICECENTRO_COLUMN_BITMASK |
            SpettanzeSociModelImpl.NUMEROPROTOCOLLO_COLUMN_BITMASK |
            SpettanzeSociModelImpl.CODICEFORNITORE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_FATTURAVENDITA = new FinderPath(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByFatturaVendita",
            new String[] {
                Integer.class.getName(), String.class.getName(),
                String.class.getName(), Integer.class.getName(),
                String.class.getName()
            });
    private static final String _FINDER_COLUMN_FATTURAVENDITA_ANNO_2 = "spettanzeSoci.anno = ? AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITA_CODICEATTIVITA_1 = "spettanzeSoci.codiceAttivita IS NULL AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITA_CODICEATTIVITA_2 = "spettanzeSoci.codiceAttivita = ? AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITA_CODICEATTIVITA_3 = "(spettanzeSoci.codiceAttivita IS NULL OR spettanzeSoci.codiceAttivita = '') AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITA_CODICECENTRO_1 = "spettanzeSoci.codiceCentro IS NULL AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITA_CODICECENTRO_2 = "spettanzeSoci.codiceCentro = ? AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITA_CODICECENTRO_3 = "(spettanzeSoci.codiceCentro IS NULL OR spettanzeSoci.codiceCentro = '') AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITA_NUMEROPROTOCOLLO_2 =
        "spettanzeSoci.numeroProtocollo = ? AND ";
    private static final String _FINDER_COLUMN_FATTURAVENDITA_CODICEFORNITORE_1 = "spettanzeSoci.codiceFornitore IS NULL";
    private static final String _FINDER_COLUMN_FATTURAVENDITA_CODICEFORNITORE_2 = "spettanzeSoci.codiceFornitore = ?";
    private static final String _FINDER_COLUMN_FATTURAVENDITA_CODICEFORNITORE_3 = "(spettanzeSoci.codiceFornitore IS NULL OR spettanzeSoci.codiceFornitore = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FORNITORESTATO =
        new FinderPath(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociModelImpl.FINDER_CACHE_ENABLED,
            SpettanzeSociImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByFornitoreStato",
            new String[] {
                String.class.getName(), Integer.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITORESTATO =
        new FinderPath(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociModelImpl.FINDER_CACHE_ENABLED,
            SpettanzeSociImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByFornitoreStato",
            new String[] { String.class.getName(), Integer.class.getName() },
            SpettanzeSociModelImpl.CODICEFORNITORE_COLUMN_BITMASK |
            SpettanzeSociModelImpl.STATO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_FORNITORESTATO = new FinderPath(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByFornitoreStato",
            new String[] { String.class.getName(), Integer.class.getName() });
    private static final String _FINDER_COLUMN_FORNITORESTATO_CODICEFORNITORE_1 = "spettanzeSoci.codiceFornitore IS NULL AND ";
    private static final String _FINDER_COLUMN_FORNITORESTATO_CODICEFORNITORE_2 = "spettanzeSoci.codiceFornitore = ? AND ";
    private static final String _FINDER_COLUMN_FORNITORESTATO_CODICEFORNITORE_3 = "(spettanzeSoci.codiceFornitore IS NULL OR spettanzeSoci.codiceFornitore = '') AND ";
    private static final String _FINDER_COLUMN_FORNITORESTATO_STATO_2 = "spettanzeSoci.stato = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FORNITORE =
        new FinderPath(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociModelImpl.FINDER_CACHE_ENABLED,
            SpettanzeSociImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByFornitore",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITORE =
        new FinderPath(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociModelImpl.FINDER_CACHE_ENABLED,
            SpettanzeSociImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByFornitore", new String[] { String.class.getName() },
            SpettanzeSociModelImpl.CODICEFORNITORE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_FORNITORE = new FinderPath(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByFornitore",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_FORNITORE_CODICEFORNITORE_1 = "spettanzeSoci.codiceFornitore IS NULL";
    private static final String _FINDER_COLUMN_FORNITORE_CODICEFORNITORE_2 = "spettanzeSoci.codiceFornitore = ?";
    private static final String _FINDER_COLUMN_FORNITORE_CODICEFORNITORE_3 = "(spettanzeSoci.codiceFornitore IS NULL OR spettanzeSoci.codiceFornitore = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FORNITOREANNOPAGAMENTOIDPAGAMENTO =
        new FinderPath(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociModelImpl.FINDER_CACHE_ENABLED,
            SpettanzeSociImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByFornitoreAnnoPagamentoIdPagamento",
            new String[] {
                String.class.getName(), Integer.class.getName(),
                Integer.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITOREANNOPAGAMENTOIDPAGAMENTO =
        new FinderPath(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociModelImpl.FINDER_CACHE_ENABLED,
            SpettanzeSociImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByFornitoreAnnoPagamentoIdPagamento",
            new String[] {
                String.class.getName(), Integer.class.getName(),
                Integer.class.getName()
            },
            SpettanzeSociModelImpl.CODICEFORNITORE_COLUMN_BITMASK |
            SpettanzeSociModelImpl.ANNOPAGAMENTO_COLUMN_BITMASK |
            SpettanzeSociModelImpl.IDPAGAMENTO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_FORNITOREANNOPAGAMENTOIDPAGAMENTO =
        new FinderPath(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByFornitoreAnnoPagamentoIdPagamento",
            new String[] {
                String.class.getName(), Integer.class.getName(),
                Integer.class.getName()
            });
    private static final String _FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_CODICEFORNITORE_1 =
        "spettanzeSoci.codiceFornitore IS NULL AND ";
    private static final String _FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_CODICEFORNITORE_2 =
        "spettanzeSoci.codiceFornitore = ? AND ";
    private static final String _FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_CODICEFORNITORE_3 =
        "(spettanzeSoci.codiceFornitore IS NULL OR spettanzeSoci.codiceFornitore = '') AND ";
    private static final String _FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_ANNOPAGAMENTO_2 =
        "spettanzeSoci.annoPagamento = ? AND ";
    private static final String _FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_IDPAGAMENTO_2 =
        "spettanzeSoci.idPagamento = ?";
    private static final String _SQL_SELECT_SPETTANZESOCI = "SELECT spettanzeSoci FROM SpettanzeSoci spettanzeSoci";
    private static final String _SQL_SELECT_SPETTANZESOCI_WHERE = "SELECT spettanzeSoci FROM SpettanzeSoci spettanzeSoci WHERE ";
    private static final String _SQL_COUNT_SPETTANZESOCI = "SELECT COUNT(spettanzeSoci) FROM SpettanzeSoci spettanzeSoci";
    private static final String _SQL_COUNT_SPETTANZESOCI_WHERE = "SELECT COUNT(spettanzeSoci) FROM SpettanzeSoci spettanzeSoci WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "spettanzeSoci.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No SpettanzeSoci exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No SpettanzeSoci exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(SpettanzeSociPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "anno", "codiceAttivita", "codiceCentro", "numeroProtocollo",
                "codiceFornitore", "dataCalcolo", "idPagamento", "annoPagamento"
            });
    private static SpettanzeSoci _nullSpettanzeSoci = new SpettanzeSociImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<SpettanzeSoci> toCacheModel() {
                return _nullSpettanzeSociCacheModel;
            }
        };

    private static CacheModel<SpettanzeSoci> _nullSpettanzeSociCacheModel = new CacheModel<SpettanzeSoci>() {
            @Override
            public SpettanzeSoci toEntityModel() {
                return _nullSpettanzeSoci;
            }
        };

    public SpettanzeSociPersistenceImpl() {
        setModelClass(SpettanzeSoci.class);
    }

    /**
     * Returns the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; and stato = &#63; or throws a {@link it.bysoftware.ct.NoSuchSpettanzeSociException} if it could not be found.
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param numeroProtocollo the numero protocollo
     * @param codiceFornitore the codice fornitore
     * @param stato the stato
     * @return the matching spettanze soci
     * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci findByFatturaVenditaStato(int anno,
        String codiceAttivita, String codiceCentro, int numeroProtocollo,
        String codiceFornitore, int stato)
        throws NoSuchSpettanzeSociException, SystemException {
        SpettanzeSoci spettanzeSoci = fetchByFatturaVenditaStato(anno,
                codiceAttivita, codiceCentro, numeroProtocollo,
                codiceFornitore, stato);

        if (spettanzeSoci == null) {
            StringBundler msg = new StringBundler(14);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("anno=");
            msg.append(anno);

            msg.append(", codiceAttivita=");
            msg.append(codiceAttivita);

            msg.append(", codiceCentro=");
            msg.append(codiceCentro);

            msg.append(", numeroProtocollo=");
            msg.append(numeroProtocollo);

            msg.append(", codiceFornitore=");
            msg.append(codiceFornitore);

            msg.append(", stato=");
            msg.append(stato);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchSpettanzeSociException(msg.toString());
        }

        return spettanzeSoci;
    }

    /**
     * Returns the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; and stato = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param numeroProtocollo the numero protocollo
     * @param codiceFornitore the codice fornitore
     * @param stato the stato
     * @return the matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci fetchByFatturaVenditaStato(int anno,
        String codiceAttivita, String codiceCentro, int numeroProtocollo,
        String codiceFornitore, int stato) throws SystemException {
        return fetchByFatturaVenditaStato(anno, codiceAttivita, codiceCentro,
            numeroProtocollo, codiceFornitore, stato, true);
    }

    /**
     * Returns the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; and stato = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param numeroProtocollo the numero protocollo
     * @param codiceFornitore the codice fornitore
     * @param stato the stato
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci fetchByFatturaVenditaStato(int anno,
        String codiceAttivita, String codiceCentro, int numeroProtocollo,
        String codiceFornitore, int stato, boolean retrieveFromCache)
        throws SystemException {
        Object[] finderArgs = new Object[] {
                anno, codiceAttivita, codiceCentro, numeroProtocollo,
                codiceFornitore, stato
            };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_FATTURAVENDITASTATO,
                    finderArgs, this);
        }

        if (result instanceof SpettanzeSoci) {
            SpettanzeSoci spettanzeSoci = (SpettanzeSoci) result;

            if ((anno != spettanzeSoci.getAnno()) ||
                    !Validator.equals(codiceAttivita,
                        spettanzeSoci.getCodiceAttivita()) ||
                    !Validator.equals(codiceCentro,
                        spettanzeSoci.getCodiceCentro()) ||
                    (numeroProtocollo != spettanzeSoci.getNumeroProtocollo()) ||
                    !Validator.equals(codiceFornitore,
                        spettanzeSoci.getCodiceFornitore()) ||
                    (stato != spettanzeSoci.getStato())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(8);

            query.append(_SQL_SELECT_SPETTANZESOCI_WHERE);

            query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_ANNO_2);

            boolean bindCodiceAttivita = false;

            if (codiceAttivita == null) {
                query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_CODICEATTIVITA_1);
            } else if (codiceAttivita.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_CODICEATTIVITA_3);
            } else {
                bindCodiceAttivita = true;

                query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_CODICEATTIVITA_2);
            }

            boolean bindCodiceCentro = false;

            if (codiceCentro == null) {
                query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_CODICECENTRO_1);
            } else if (codiceCentro.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_CODICECENTRO_3);
            } else {
                bindCodiceCentro = true;

                query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_CODICECENTRO_2);
            }

            query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_NUMEROPROTOCOLLO_2);

            boolean bindCodiceFornitore = false;

            if (codiceFornitore == null) {
                query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_CODICEFORNITORE_1);
            } else if (codiceFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_CODICEFORNITORE_3);
            } else {
                bindCodiceFornitore = true;

                query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_CODICEFORNITORE_2);
            }

            query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_STATO_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(anno);

                if (bindCodiceAttivita) {
                    qPos.add(codiceAttivita);
                }

                if (bindCodiceCentro) {
                    qPos.add(codiceCentro);
                }

                qPos.add(numeroProtocollo);

                if (bindCodiceFornitore) {
                    qPos.add(codiceFornitore);
                }

                qPos.add(stato);

                List<SpettanzeSoci> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FATTURAVENDITASTATO,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "SpettanzeSociPersistenceImpl.fetchByFatturaVenditaStato(int, String, String, int, String, int, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    SpettanzeSoci spettanzeSoci = list.get(0);

                    result = spettanzeSoci;

                    cacheResult(spettanzeSoci);

                    if ((spettanzeSoci.getAnno() != anno) ||
                            (spettanzeSoci.getCodiceAttivita() == null) ||
                            !spettanzeSoci.getCodiceAttivita()
                                              .equals(codiceAttivita) ||
                            (spettanzeSoci.getCodiceCentro() == null) ||
                            !spettanzeSoci.getCodiceCentro().equals(codiceCentro) ||
                            (spettanzeSoci.getNumeroProtocollo() != numeroProtocollo) ||
                            (spettanzeSoci.getCodiceFornitore() == null) ||
                            !spettanzeSoci.getCodiceFornitore()
                                              .equals(codiceFornitore) ||
                            (spettanzeSoci.getStato() != stato)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FATTURAVENDITASTATO,
                            finderArgs, spettanzeSoci);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_FATTURAVENDITASTATO,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (SpettanzeSoci) result;
        }
    }

    /**
     * Removes the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; and stato = &#63; from the database.
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param numeroProtocollo the numero protocollo
     * @param codiceFornitore the codice fornitore
     * @param stato the stato
     * @return the spettanze soci that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci removeByFatturaVenditaStato(int anno,
        String codiceAttivita, String codiceCentro, int numeroProtocollo,
        String codiceFornitore, int stato)
        throws NoSuchSpettanzeSociException, SystemException {
        SpettanzeSoci spettanzeSoci = findByFatturaVenditaStato(anno,
                codiceAttivita, codiceCentro, numeroProtocollo,
                codiceFornitore, stato);

        return remove(spettanzeSoci);
    }

    /**
     * Returns the number of spettanze socis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; and stato = &#63;.
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param numeroProtocollo the numero protocollo
     * @param codiceFornitore the codice fornitore
     * @param stato the stato
     * @return the number of matching spettanze socis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByFatturaVenditaStato(int anno, String codiceAttivita,
        String codiceCentro, int numeroProtocollo, String codiceFornitore,
        int stato) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_FATTURAVENDITASTATO;

        Object[] finderArgs = new Object[] {
                anno, codiceAttivita, codiceCentro, numeroProtocollo,
                codiceFornitore, stato
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(7);

            query.append(_SQL_COUNT_SPETTANZESOCI_WHERE);

            query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_ANNO_2);

            boolean bindCodiceAttivita = false;

            if (codiceAttivita == null) {
                query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_CODICEATTIVITA_1);
            } else if (codiceAttivita.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_CODICEATTIVITA_3);
            } else {
                bindCodiceAttivita = true;

                query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_CODICEATTIVITA_2);
            }

            boolean bindCodiceCentro = false;

            if (codiceCentro == null) {
                query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_CODICECENTRO_1);
            } else if (codiceCentro.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_CODICECENTRO_3);
            } else {
                bindCodiceCentro = true;

                query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_CODICECENTRO_2);
            }

            query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_NUMEROPROTOCOLLO_2);

            boolean bindCodiceFornitore = false;

            if (codiceFornitore == null) {
                query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_CODICEFORNITORE_1);
            } else if (codiceFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_CODICEFORNITORE_3);
            } else {
                bindCodiceFornitore = true;

                query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_CODICEFORNITORE_2);
            }

            query.append(_FINDER_COLUMN_FATTURAVENDITASTATO_STATO_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(anno);

                if (bindCodiceAttivita) {
                    qPos.add(codiceAttivita);
                }

                if (bindCodiceCentro) {
                    qPos.add(codiceCentro);
                }

                qPos.add(numeroProtocollo);

                if (bindCodiceFornitore) {
                    qPos.add(codiceFornitore);
                }

                qPos.add(stato);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; or throws a {@link it.bysoftware.ct.NoSuchSpettanzeSociException} if it could not be found.
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param numeroProtocollo the numero protocollo
     * @param codiceFornitore the codice fornitore
     * @return the matching spettanze soci
     * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci findByFatturaVendita(int anno, String codiceAttivita,
        String codiceCentro, int numeroProtocollo, String codiceFornitore)
        throws NoSuchSpettanzeSociException, SystemException {
        SpettanzeSoci spettanzeSoci = fetchByFatturaVendita(anno,
                codiceAttivita, codiceCentro, numeroProtocollo, codiceFornitore);

        if (spettanzeSoci == null) {
            StringBundler msg = new StringBundler(12);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("anno=");
            msg.append(anno);

            msg.append(", codiceAttivita=");
            msg.append(codiceAttivita);

            msg.append(", codiceCentro=");
            msg.append(codiceCentro);

            msg.append(", numeroProtocollo=");
            msg.append(numeroProtocollo);

            msg.append(", codiceFornitore=");
            msg.append(codiceFornitore);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchSpettanzeSociException(msg.toString());
        }

        return spettanzeSoci;
    }

    /**
     * Returns the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param numeroProtocollo the numero protocollo
     * @param codiceFornitore the codice fornitore
     * @return the matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci fetchByFatturaVendita(int anno, String codiceAttivita,
        String codiceCentro, int numeroProtocollo, String codiceFornitore)
        throws SystemException {
        return fetchByFatturaVendita(anno, codiceAttivita, codiceCentro,
            numeroProtocollo, codiceFornitore, true);
    }

    /**
     * Returns the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param numeroProtocollo the numero protocollo
     * @param codiceFornitore the codice fornitore
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci fetchByFatturaVendita(int anno, String codiceAttivita,
        String codiceCentro, int numeroProtocollo, String codiceFornitore,
        boolean retrieveFromCache) throws SystemException {
        Object[] finderArgs = new Object[] {
                anno, codiceAttivita, codiceCentro, numeroProtocollo,
                codiceFornitore
            };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_FATTURAVENDITA,
                    finderArgs, this);
        }

        if (result instanceof SpettanzeSoci) {
            SpettanzeSoci spettanzeSoci = (SpettanzeSoci) result;

            if ((anno != spettanzeSoci.getAnno()) ||
                    !Validator.equals(codiceAttivita,
                        spettanzeSoci.getCodiceAttivita()) ||
                    !Validator.equals(codiceCentro,
                        spettanzeSoci.getCodiceCentro()) ||
                    (numeroProtocollo != spettanzeSoci.getNumeroProtocollo()) ||
                    !Validator.equals(codiceFornitore,
                        spettanzeSoci.getCodiceFornitore())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(7);

            query.append(_SQL_SELECT_SPETTANZESOCI_WHERE);

            query.append(_FINDER_COLUMN_FATTURAVENDITA_ANNO_2);

            boolean bindCodiceAttivita = false;

            if (codiceAttivita == null) {
                query.append(_FINDER_COLUMN_FATTURAVENDITA_CODICEATTIVITA_1);
            } else if (codiceAttivita.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FATTURAVENDITA_CODICEATTIVITA_3);
            } else {
                bindCodiceAttivita = true;

                query.append(_FINDER_COLUMN_FATTURAVENDITA_CODICEATTIVITA_2);
            }

            boolean bindCodiceCentro = false;

            if (codiceCentro == null) {
                query.append(_FINDER_COLUMN_FATTURAVENDITA_CODICECENTRO_1);
            } else if (codiceCentro.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FATTURAVENDITA_CODICECENTRO_3);
            } else {
                bindCodiceCentro = true;

                query.append(_FINDER_COLUMN_FATTURAVENDITA_CODICECENTRO_2);
            }

            query.append(_FINDER_COLUMN_FATTURAVENDITA_NUMEROPROTOCOLLO_2);

            boolean bindCodiceFornitore = false;

            if (codiceFornitore == null) {
                query.append(_FINDER_COLUMN_FATTURAVENDITA_CODICEFORNITORE_1);
            } else if (codiceFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FATTURAVENDITA_CODICEFORNITORE_3);
            } else {
                bindCodiceFornitore = true;

                query.append(_FINDER_COLUMN_FATTURAVENDITA_CODICEFORNITORE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(anno);

                if (bindCodiceAttivita) {
                    qPos.add(codiceAttivita);
                }

                if (bindCodiceCentro) {
                    qPos.add(codiceCentro);
                }

                qPos.add(numeroProtocollo);

                if (bindCodiceFornitore) {
                    qPos.add(codiceFornitore);
                }

                List<SpettanzeSoci> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FATTURAVENDITA,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "SpettanzeSociPersistenceImpl.fetchByFatturaVendita(int, String, String, int, String, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    SpettanzeSoci spettanzeSoci = list.get(0);

                    result = spettanzeSoci;

                    cacheResult(spettanzeSoci);

                    if ((spettanzeSoci.getAnno() != anno) ||
                            (spettanzeSoci.getCodiceAttivita() == null) ||
                            !spettanzeSoci.getCodiceAttivita()
                                              .equals(codiceAttivita) ||
                            (spettanzeSoci.getCodiceCentro() == null) ||
                            !spettanzeSoci.getCodiceCentro().equals(codiceCentro) ||
                            (spettanzeSoci.getNumeroProtocollo() != numeroProtocollo) ||
                            (spettanzeSoci.getCodiceFornitore() == null) ||
                            !spettanzeSoci.getCodiceFornitore()
                                              .equals(codiceFornitore)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FATTURAVENDITA,
                            finderArgs, spettanzeSoci);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_FATTURAVENDITA,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (SpettanzeSoci) result;
        }
    }

    /**
     * Removes the spettanze soci where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63; from the database.
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param numeroProtocollo the numero protocollo
     * @param codiceFornitore the codice fornitore
     * @return the spettanze soci that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci removeByFatturaVendita(int anno,
        String codiceAttivita, String codiceCentro, int numeroProtocollo,
        String codiceFornitore)
        throws NoSuchSpettanzeSociException, SystemException {
        SpettanzeSoci spettanzeSoci = findByFatturaVendita(anno,
                codiceAttivita, codiceCentro, numeroProtocollo, codiceFornitore);

        return remove(spettanzeSoci);
    }

    /**
     * Returns the number of spettanze socis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; and codiceFornitore = &#63;.
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param numeroProtocollo the numero protocollo
     * @param codiceFornitore the codice fornitore
     * @return the number of matching spettanze socis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByFatturaVendita(int anno, String codiceAttivita,
        String codiceCentro, int numeroProtocollo, String codiceFornitore)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_FATTURAVENDITA;

        Object[] finderArgs = new Object[] {
                anno, codiceAttivita, codiceCentro, numeroProtocollo,
                codiceFornitore
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(6);

            query.append(_SQL_COUNT_SPETTANZESOCI_WHERE);

            query.append(_FINDER_COLUMN_FATTURAVENDITA_ANNO_2);

            boolean bindCodiceAttivita = false;

            if (codiceAttivita == null) {
                query.append(_FINDER_COLUMN_FATTURAVENDITA_CODICEATTIVITA_1);
            } else if (codiceAttivita.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FATTURAVENDITA_CODICEATTIVITA_3);
            } else {
                bindCodiceAttivita = true;

                query.append(_FINDER_COLUMN_FATTURAVENDITA_CODICEATTIVITA_2);
            }

            boolean bindCodiceCentro = false;

            if (codiceCentro == null) {
                query.append(_FINDER_COLUMN_FATTURAVENDITA_CODICECENTRO_1);
            } else if (codiceCentro.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FATTURAVENDITA_CODICECENTRO_3);
            } else {
                bindCodiceCentro = true;

                query.append(_FINDER_COLUMN_FATTURAVENDITA_CODICECENTRO_2);
            }

            query.append(_FINDER_COLUMN_FATTURAVENDITA_NUMEROPROTOCOLLO_2);

            boolean bindCodiceFornitore = false;

            if (codiceFornitore == null) {
                query.append(_FINDER_COLUMN_FATTURAVENDITA_CODICEFORNITORE_1);
            } else if (codiceFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FATTURAVENDITA_CODICEFORNITORE_3);
            } else {
                bindCodiceFornitore = true;

                query.append(_FINDER_COLUMN_FATTURAVENDITA_CODICEFORNITORE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(anno);

                if (bindCodiceAttivita) {
                    qPos.add(codiceAttivita);
                }

                if (bindCodiceCentro) {
                    qPos.add(codiceCentro);
                }

                qPos.add(numeroProtocollo);

                if (bindCodiceFornitore) {
                    qPos.add(codiceFornitore);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the spettanze socis where codiceFornitore = &#63; and stato = &#63;.
     *
     * @param codiceFornitore the codice fornitore
     * @param stato the stato
     * @return the matching spettanze socis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SpettanzeSoci> findByFornitoreStato(String codiceFornitore,
        int stato) throws SystemException {
        return findByFornitoreStato(codiceFornitore, stato, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the spettanze socis where codiceFornitore = &#63; and stato = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceFornitore the codice fornitore
     * @param stato the stato
     * @param start the lower bound of the range of spettanze socis
     * @param end the upper bound of the range of spettanze socis (not inclusive)
     * @return the range of matching spettanze socis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SpettanzeSoci> findByFornitoreStato(String codiceFornitore,
        int stato, int start, int end) throws SystemException {
        return findByFornitoreStato(codiceFornitore, stato, start, end, null);
    }

    /**
     * Returns an ordered range of all the spettanze socis where codiceFornitore = &#63; and stato = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceFornitore the codice fornitore
     * @param stato the stato
     * @param start the lower bound of the range of spettanze socis
     * @param end the upper bound of the range of spettanze socis (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching spettanze socis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SpettanzeSoci> findByFornitoreStato(String codiceFornitore,
        int stato, int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITORESTATO;
            finderArgs = new Object[] { codiceFornitore, stato };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FORNITORESTATO;
            finderArgs = new Object[] {
                    codiceFornitore, stato,
                    
                    start, end, orderByComparator
                };
        }

        List<SpettanzeSoci> list = (List<SpettanzeSoci>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (SpettanzeSoci spettanzeSoci : list) {
                if (!Validator.equals(codiceFornitore,
                            spettanzeSoci.getCodiceFornitore()) ||
                        (stato != spettanzeSoci.getStato())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_SPETTANZESOCI_WHERE);

            boolean bindCodiceFornitore = false;

            if (codiceFornitore == null) {
                query.append(_FINDER_COLUMN_FORNITORESTATO_CODICEFORNITORE_1);
            } else if (codiceFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITORESTATO_CODICEFORNITORE_3);
            } else {
                bindCodiceFornitore = true;

                query.append(_FINDER_COLUMN_FORNITORESTATO_CODICEFORNITORE_2);
            }

            query.append(_FINDER_COLUMN_FORNITORESTATO_STATO_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(SpettanzeSociModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceFornitore) {
                    qPos.add(codiceFornitore);
                }

                qPos.add(stato);

                if (!pagination) {
                    list = (List<SpettanzeSoci>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<SpettanzeSoci>(list);
                } else {
                    list = (List<SpettanzeSoci>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first spettanze soci in the ordered set where codiceFornitore = &#63; and stato = &#63;.
     *
     * @param codiceFornitore the codice fornitore
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching spettanze soci
     * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci findByFornitoreStato_First(String codiceFornitore,
        int stato, OrderByComparator orderByComparator)
        throws NoSuchSpettanzeSociException, SystemException {
        SpettanzeSoci spettanzeSoci = fetchByFornitoreStato_First(codiceFornitore,
                stato, orderByComparator);

        if (spettanzeSoci != null) {
            return spettanzeSoci;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceFornitore=");
        msg.append(codiceFornitore);

        msg.append(", stato=");
        msg.append(stato);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchSpettanzeSociException(msg.toString());
    }

    /**
     * Returns the first spettanze soci in the ordered set where codiceFornitore = &#63; and stato = &#63;.
     *
     * @param codiceFornitore the codice fornitore
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci fetchByFornitoreStato_First(String codiceFornitore,
        int stato, OrderByComparator orderByComparator)
        throws SystemException {
        List<SpettanzeSoci> list = findByFornitoreStato(codiceFornitore, stato,
                0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last spettanze soci in the ordered set where codiceFornitore = &#63; and stato = &#63;.
     *
     * @param codiceFornitore the codice fornitore
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching spettanze soci
     * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci findByFornitoreStato_Last(String codiceFornitore,
        int stato, OrderByComparator orderByComparator)
        throws NoSuchSpettanzeSociException, SystemException {
        SpettanzeSoci spettanzeSoci = fetchByFornitoreStato_Last(codiceFornitore,
                stato, orderByComparator);

        if (spettanzeSoci != null) {
            return spettanzeSoci;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceFornitore=");
        msg.append(codiceFornitore);

        msg.append(", stato=");
        msg.append(stato);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchSpettanzeSociException(msg.toString());
    }

    /**
     * Returns the last spettanze soci in the ordered set where codiceFornitore = &#63; and stato = &#63;.
     *
     * @param codiceFornitore the codice fornitore
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci fetchByFornitoreStato_Last(String codiceFornitore,
        int stato, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByFornitoreStato(codiceFornitore, stato);

        if (count == 0) {
            return null;
        }

        List<SpettanzeSoci> list = findByFornitoreStato(codiceFornitore, stato,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the spettanze socis before and after the current spettanze soci in the ordered set where codiceFornitore = &#63; and stato = &#63;.
     *
     * @param id the primary key of the current spettanze soci
     * @param codiceFornitore the codice fornitore
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next spettanze soci
     * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a spettanze soci with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci[] findByFornitoreStato_PrevAndNext(long id,
        String codiceFornitore, int stato, OrderByComparator orderByComparator)
        throws NoSuchSpettanzeSociException, SystemException {
        SpettanzeSoci spettanzeSoci = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            SpettanzeSoci[] array = new SpettanzeSociImpl[3];

            array[0] = getByFornitoreStato_PrevAndNext(session, spettanzeSoci,
                    codiceFornitore, stato, orderByComparator, true);

            array[1] = spettanzeSoci;

            array[2] = getByFornitoreStato_PrevAndNext(session, spettanzeSoci,
                    codiceFornitore, stato, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected SpettanzeSoci getByFornitoreStato_PrevAndNext(Session session,
        SpettanzeSoci spettanzeSoci, String codiceFornitore, int stato,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_SPETTANZESOCI_WHERE);

        boolean bindCodiceFornitore = false;

        if (codiceFornitore == null) {
            query.append(_FINDER_COLUMN_FORNITORESTATO_CODICEFORNITORE_1);
        } else if (codiceFornitore.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_FORNITORESTATO_CODICEFORNITORE_3);
        } else {
            bindCodiceFornitore = true;

            query.append(_FINDER_COLUMN_FORNITORESTATO_CODICEFORNITORE_2);
        }

        query.append(_FINDER_COLUMN_FORNITORESTATO_STATO_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(SpettanzeSociModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindCodiceFornitore) {
            qPos.add(codiceFornitore);
        }

        qPos.add(stato);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(spettanzeSoci);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<SpettanzeSoci> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the spettanze socis where codiceFornitore = &#63; and stato = &#63; from the database.
     *
     * @param codiceFornitore the codice fornitore
     * @param stato the stato
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByFornitoreStato(String codiceFornitore, int stato)
        throws SystemException {
        for (SpettanzeSoci spettanzeSoci : findByFornitoreStato(
                codiceFornitore, stato, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
                null)) {
            remove(spettanzeSoci);
        }
    }

    /**
     * Returns the number of spettanze socis where codiceFornitore = &#63; and stato = &#63;.
     *
     * @param codiceFornitore the codice fornitore
     * @param stato the stato
     * @return the number of matching spettanze socis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByFornitoreStato(String codiceFornitore, int stato)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_FORNITORESTATO;

        Object[] finderArgs = new Object[] { codiceFornitore, stato };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_SPETTANZESOCI_WHERE);

            boolean bindCodiceFornitore = false;

            if (codiceFornitore == null) {
                query.append(_FINDER_COLUMN_FORNITORESTATO_CODICEFORNITORE_1);
            } else if (codiceFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITORESTATO_CODICEFORNITORE_3);
            } else {
                bindCodiceFornitore = true;

                query.append(_FINDER_COLUMN_FORNITORESTATO_CODICEFORNITORE_2);
            }

            query.append(_FINDER_COLUMN_FORNITORESTATO_STATO_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceFornitore) {
                    qPos.add(codiceFornitore);
                }

                qPos.add(stato);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the spettanze socis where codiceFornitore = &#63;.
     *
     * @param codiceFornitore the codice fornitore
     * @return the matching spettanze socis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SpettanzeSoci> findByFornitore(String codiceFornitore)
        throws SystemException {
        return findByFornitore(codiceFornitore, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the spettanze socis where codiceFornitore = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceFornitore the codice fornitore
     * @param start the lower bound of the range of spettanze socis
     * @param end the upper bound of the range of spettanze socis (not inclusive)
     * @return the range of matching spettanze socis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SpettanzeSoci> findByFornitore(String codiceFornitore,
        int start, int end) throws SystemException {
        return findByFornitore(codiceFornitore, start, end, null);
    }

    /**
     * Returns an ordered range of all the spettanze socis where codiceFornitore = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceFornitore the codice fornitore
     * @param start the lower bound of the range of spettanze socis
     * @param end the upper bound of the range of spettanze socis (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching spettanze socis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SpettanzeSoci> findByFornitore(String codiceFornitore,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITORE;
            finderArgs = new Object[] { codiceFornitore };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FORNITORE;
            finderArgs = new Object[] {
                    codiceFornitore,
                    
                    start, end, orderByComparator
                };
        }

        List<SpettanzeSoci> list = (List<SpettanzeSoci>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (SpettanzeSoci spettanzeSoci : list) {
                if (!Validator.equals(codiceFornitore,
                            spettanzeSoci.getCodiceFornitore())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_SPETTANZESOCI_WHERE);

            boolean bindCodiceFornitore = false;

            if (codiceFornitore == null) {
                query.append(_FINDER_COLUMN_FORNITORE_CODICEFORNITORE_1);
            } else if (codiceFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITORE_CODICEFORNITORE_3);
            } else {
                bindCodiceFornitore = true;

                query.append(_FINDER_COLUMN_FORNITORE_CODICEFORNITORE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(SpettanzeSociModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceFornitore) {
                    qPos.add(codiceFornitore);
                }

                if (!pagination) {
                    list = (List<SpettanzeSoci>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<SpettanzeSoci>(list);
                } else {
                    list = (List<SpettanzeSoci>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first spettanze soci in the ordered set where codiceFornitore = &#63;.
     *
     * @param codiceFornitore the codice fornitore
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching spettanze soci
     * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci findByFornitore_First(String codiceFornitore,
        OrderByComparator orderByComparator)
        throws NoSuchSpettanzeSociException, SystemException {
        SpettanzeSoci spettanzeSoci = fetchByFornitore_First(codiceFornitore,
                orderByComparator);

        if (spettanzeSoci != null) {
            return spettanzeSoci;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceFornitore=");
        msg.append(codiceFornitore);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchSpettanzeSociException(msg.toString());
    }

    /**
     * Returns the first spettanze soci in the ordered set where codiceFornitore = &#63;.
     *
     * @param codiceFornitore the codice fornitore
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci fetchByFornitore_First(String codiceFornitore,
        OrderByComparator orderByComparator) throws SystemException {
        List<SpettanzeSoci> list = findByFornitore(codiceFornitore, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last spettanze soci in the ordered set where codiceFornitore = &#63;.
     *
     * @param codiceFornitore the codice fornitore
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching spettanze soci
     * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci findByFornitore_Last(String codiceFornitore,
        OrderByComparator orderByComparator)
        throws NoSuchSpettanzeSociException, SystemException {
        SpettanzeSoci spettanzeSoci = fetchByFornitore_Last(codiceFornitore,
                orderByComparator);

        if (spettanzeSoci != null) {
            return spettanzeSoci;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceFornitore=");
        msg.append(codiceFornitore);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchSpettanzeSociException(msg.toString());
    }

    /**
     * Returns the last spettanze soci in the ordered set where codiceFornitore = &#63;.
     *
     * @param codiceFornitore the codice fornitore
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci fetchByFornitore_Last(String codiceFornitore,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByFornitore(codiceFornitore);

        if (count == 0) {
            return null;
        }

        List<SpettanzeSoci> list = findByFornitore(codiceFornitore, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the spettanze socis before and after the current spettanze soci in the ordered set where codiceFornitore = &#63;.
     *
     * @param id the primary key of the current spettanze soci
     * @param codiceFornitore the codice fornitore
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next spettanze soci
     * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a spettanze soci with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci[] findByFornitore_PrevAndNext(long id,
        String codiceFornitore, OrderByComparator orderByComparator)
        throws NoSuchSpettanzeSociException, SystemException {
        SpettanzeSoci spettanzeSoci = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            SpettanzeSoci[] array = new SpettanzeSociImpl[3];

            array[0] = getByFornitore_PrevAndNext(session, spettanzeSoci,
                    codiceFornitore, orderByComparator, true);

            array[1] = spettanzeSoci;

            array[2] = getByFornitore_PrevAndNext(session, spettanzeSoci,
                    codiceFornitore, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected SpettanzeSoci getByFornitore_PrevAndNext(Session session,
        SpettanzeSoci spettanzeSoci, String codiceFornitore,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_SPETTANZESOCI_WHERE);

        boolean bindCodiceFornitore = false;

        if (codiceFornitore == null) {
            query.append(_FINDER_COLUMN_FORNITORE_CODICEFORNITORE_1);
        } else if (codiceFornitore.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_FORNITORE_CODICEFORNITORE_3);
        } else {
            bindCodiceFornitore = true;

            query.append(_FINDER_COLUMN_FORNITORE_CODICEFORNITORE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(SpettanzeSociModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindCodiceFornitore) {
            qPos.add(codiceFornitore);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(spettanzeSoci);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<SpettanzeSoci> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the spettanze socis where codiceFornitore = &#63; from the database.
     *
     * @param codiceFornitore the codice fornitore
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByFornitore(String codiceFornitore)
        throws SystemException {
        for (SpettanzeSoci spettanzeSoci : findByFornitore(codiceFornitore,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(spettanzeSoci);
        }
    }

    /**
     * Returns the number of spettanze socis where codiceFornitore = &#63;.
     *
     * @param codiceFornitore the codice fornitore
     * @return the number of matching spettanze socis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByFornitore(String codiceFornitore)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_FORNITORE;

        Object[] finderArgs = new Object[] { codiceFornitore };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_SPETTANZESOCI_WHERE);

            boolean bindCodiceFornitore = false;

            if (codiceFornitore == null) {
                query.append(_FINDER_COLUMN_FORNITORE_CODICEFORNITORE_1);
            } else if (codiceFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITORE_CODICEFORNITORE_3);
            } else {
                bindCodiceFornitore = true;

                query.append(_FINDER_COLUMN_FORNITORE_CODICEFORNITORE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceFornitore) {
                    qPos.add(codiceFornitore);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the spettanze socis where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
     *
     * @param codiceFornitore the codice fornitore
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @return the matching spettanze socis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SpettanzeSoci> findByFornitoreAnnoPagamentoIdPagamento(
        String codiceFornitore, int annoPagamento, int idPagamento)
        throws SystemException {
        return findByFornitoreAnnoPagamentoIdPagamento(codiceFornitore,
            annoPagamento, idPagamento, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the spettanze socis where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceFornitore the codice fornitore
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param start the lower bound of the range of spettanze socis
     * @param end the upper bound of the range of spettanze socis (not inclusive)
     * @return the range of matching spettanze socis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SpettanzeSoci> findByFornitoreAnnoPagamentoIdPagamento(
        String codiceFornitore, int annoPagamento, int idPagamento, int start,
        int end) throws SystemException {
        return findByFornitoreAnnoPagamentoIdPagamento(codiceFornitore,
            annoPagamento, idPagamento, start, end, null);
    }

    /**
     * Returns an ordered range of all the spettanze socis where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceFornitore the codice fornitore
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param start the lower bound of the range of spettanze socis
     * @param end the upper bound of the range of spettanze socis (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching spettanze socis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SpettanzeSoci> findByFornitoreAnnoPagamentoIdPagamento(
        String codiceFornitore, int annoPagamento, int idPagamento, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITOREANNOPAGAMENTOIDPAGAMENTO;
            finderArgs = new Object[] {
                    codiceFornitore, annoPagamento, idPagamento
                };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FORNITOREANNOPAGAMENTOIDPAGAMENTO;
            finderArgs = new Object[] {
                    codiceFornitore, annoPagamento, idPagamento,
                    
                    start, end, orderByComparator
                };
        }

        List<SpettanzeSoci> list = (List<SpettanzeSoci>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (SpettanzeSoci spettanzeSoci : list) {
                if (!Validator.equals(codiceFornitore,
                            spettanzeSoci.getCodiceFornitore()) ||
                        (annoPagamento != spettanzeSoci.getAnnoPagamento()) ||
                        (idPagamento != spettanzeSoci.getIdPagamento())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(5 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(5);
            }

            query.append(_SQL_SELECT_SPETTANZESOCI_WHERE);

            boolean bindCodiceFornitore = false;

            if (codiceFornitore == null) {
                query.append(_FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_CODICEFORNITORE_1);
            } else if (codiceFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_CODICEFORNITORE_3);
            } else {
                bindCodiceFornitore = true;

                query.append(_FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_CODICEFORNITORE_2);
            }

            query.append(_FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_ANNOPAGAMENTO_2);

            query.append(_FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_IDPAGAMENTO_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(SpettanzeSociModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceFornitore) {
                    qPos.add(codiceFornitore);
                }

                qPos.add(annoPagamento);

                qPos.add(idPagamento);

                if (!pagination) {
                    list = (List<SpettanzeSoci>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<SpettanzeSoci>(list);
                } else {
                    list = (List<SpettanzeSoci>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first spettanze soci in the ordered set where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
     *
     * @param codiceFornitore the codice fornitore
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching spettanze soci
     * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci findByFornitoreAnnoPagamentoIdPagamento_First(
        String codiceFornitore, int annoPagamento, int idPagamento,
        OrderByComparator orderByComparator)
        throws NoSuchSpettanzeSociException, SystemException {
        SpettanzeSoci spettanzeSoci = fetchByFornitoreAnnoPagamentoIdPagamento_First(codiceFornitore,
                annoPagamento, idPagamento, orderByComparator);

        if (spettanzeSoci != null) {
            return spettanzeSoci;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceFornitore=");
        msg.append(codiceFornitore);

        msg.append(", annoPagamento=");
        msg.append(annoPagamento);

        msg.append(", idPagamento=");
        msg.append(idPagamento);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchSpettanzeSociException(msg.toString());
    }

    /**
     * Returns the first spettanze soci in the ordered set where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
     *
     * @param codiceFornitore the codice fornitore
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci fetchByFornitoreAnnoPagamentoIdPagamento_First(
        String codiceFornitore, int annoPagamento, int idPagamento,
        OrderByComparator orderByComparator) throws SystemException {
        List<SpettanzeSoci> list = findByFornitoreAnnoPagamentoIdPagamento(codiceFornitore,
                annoPagamento, idPagamento, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last spettanze soci in the ordered set where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
     *
     * @param codiceFornitore the codice fornitore
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching spettanze soci
     * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a matching spettanze soci could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci findByFornitoreAnnoPagamentoIdPagamento_Last(
        String codiceFornitore, int annoPagamento, int idPagamento,
        OrderByComparator orderByComparator)
        throws NoSuchSpettanzeSociException, SystemException {
        SpettanzeSoci spettanzeSoci = fetchByFornitoreAnnoPagamentoIdPagamento_Last(codiceFornitore,
                annoPagamento, idPagamento, orderByComparator);

        if (spettanzeSoci != null) {
            return spettanzeSoci;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceFornitore=");
        msg.append(codiceFornitore);

        msg.append(", annoPagamento=");
        msg.append(annoPagamento);

        msg.append(", idPagamento=");
        msg.append(idPagamento);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchSpettanzeSociException(msg.toString());
    }

    /**
     * Returns the last spettanze soci in the ordered set where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
     *
     * @param codiceFornitore the codice fornitore
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching spettanze soci, or <code>null</code> if a matching spettanze soci could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci fetchByFornitoreAnnoPagamentoIdPagamento_Last(
        String codiceFornitore, int annoPagamento, int idPagamento,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByFornitoreAnnoPagamentoIdPagamento(codiceFornitore,
                annoPagamento, idPagamento);

        if (count == 0) {
            return null;
        }

        List<SpettanzeSoci> list = findByFornitoreAnnoPagamentoIdPagamento(codiceFornitore,
                annoPagamento, idPagamento, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the spettanze socis before and after the current spettanze soci in the ordered set where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
     *
     * @param id the primary key of the current spettanze soci
     * @param codiceFornitore the codice fornitore
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next spettanze soci
     * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a spettanze soci with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci[] findByFornitoreAnnoPagamentoIdPagamento_PrevAndNext(
        long id, String codiceFornitore, int annoPagamento, int idPagamento,
        OrderByComparator orderByComparator)
        throws NoSuchSpettanzeSociException, SystemException {
        SpettanzeSoci spettanzeSoci = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            SpettanzeSoci[] array = new SpettanzeSociImpl[3];

            array[0] = getByFornitoreAnnoPagamentoIdPagamento_PrevAndNext(session,
                    spettanzeSoci, codiceFornitore, annoPagamento, idPagamento,
                    orderByComparator, true);

            array[1] = spettanzeSoci;

            array[2] = getByFornitoreAnnoPagamentoIdPagamento_PrevAndNext(session,
                    spettanzeSoci, codiceFornitore, annoPagamento, idPagamento,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected SpettanzeSoci getByFornitoreAnnoPagamentoIdPagamento_PrevAndNext(
        Session session, SpettanzeSoci spettanzeSoci, String codiceFornitore,
        int annoPagamento, int idPagamento,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_SPETTANZESOCI_WHERE);

        boolean bindCodiceFornitore = false;

        if (codiceFornitore == null) {
            query.append(_FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_CODICEFORNITORE_1);
        } else if (codiceFornitore.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_CODICEFORNITORE_3);
        } else {
            bindCodiceFornitore = true;

            query.append(_FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_CODICEFORNITORE_2);
        }

        query.append(_FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_ANNOPAGAMENTO_2);

        query.append(_FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_IDPAGAMENTO_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(SpettanzeSociModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindCodiceFornitore) {
            qPos.add(codiceFornitore);
        }

        qPos.add(annoPagamento);

        qPos.add(idPagamento);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(spettanzeSoci);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<SpettanzeSoci> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the spettanze socis where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63; from the database.
     *
     * @param codiceFornitore the codice fornitore
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByFornitoreAnnoPagamentoIdPagamento(
        String codiceFornitore, int annoPagamento, int idPagamento)
        throws SystemException {
        for (SpettanzeSoci spettanzeSoci : findByFornitoreAnnoPagamentoIdPagamento(
                codiceFornitore, annoPagamento, idPagamento, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(spettanzeSoci);
        }
    }

    /**
     * Returns the number of spettanze socis where codiceFornitore = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
     *
     * @param codiceFornitore the codice fornitore
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @return the number of matching spettanze socis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByFornitoreAnnoPagamentoIdPagamento(
        String codiceFornitore, int annoPagamento, int idPagamento)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_FORNITOREANNOPAGAMENTOIDPAGAMENTO;

        Object[] finderArgs = new Object[] {
                codiceFornitore, annoPagamento, idPagamento
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(4);

            query.append(_SQL_COUNT_SPETTANZESOCI_WHERE);

            boolean bindCodiceFornitore = false;

            if (codiceFornitore == null) {
                query.append(_FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_CODICEFORNITORE_1);
            } else if (codiceFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_CODICEFORNITORE_3);
            } else {
                bindCodiceFornitore = true;

                query.append(_FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_CODICEFORNITORE_2);
            }

            query.append(_FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_ANNOPAGAMENTO_2);

            query.append(_FINDER_COLUMN_FORNITOREANNOPAGAMENTOIDPAGAMENTO_IDPAGAMENTO_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceFornitore) {
                    qPos.add(codiceFornitore);
                }

                qPos.add(annoPagamento);

                qPos.add(idPagamento);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the spettanze soci in the entity cache if it is enabled.
     *
     * @param spettanzeSoci the spettanze soci
     */
    @Override
    public void cacheResult(SpettanzeSoci spettanzeSoci) {
        EntityCacheUtil.putResult(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociImpl.class, spettanzeSoci.getPrimaryKey(),
            spettanzeSoci);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FATTURAVENDITASTATO,
            new Object[] {
                spettanzeSoci.getAnno(), spettanzeSoci.getCodiceAttivita(),
                spettanzeSoci.getCodiceCentro(),
                spettanzeSoci.getNumeroProtocollo(),
                spettanzeSoci.getCodiceFornitore(), spettanzeSoci.getStato()
            }, spettanzeSoci);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FATTURAVENDITA,
            new Object[] {
                spettanzeSoci.getAnno(), spettanzeSoci.getCodiceAttivita(),
                spettanzeSoci.getCodiceCentro(),
                spettanzeSoci.getNumeroProtocollo(),
                spettanzeSoci.getCodiceFornitore()
            }, spettanzeSoci);

        spettanzeSoci.resetOriginalValues();
    }

    /**
     * Caches the spettanze socis in the entity cache if it is enabled.
     *
     * @param spettanzeSocis the spettanze socis
     */
    @Override
    public void cacheResult(List<SpettanzeSoci> spettanzeSocis) {
        for (SpettanzeSoci spettanzeSoci : spettanzeSocis) {
            if (EntityCacheUtil.getResult(
                        SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
                        SpettanzeSociImpl.class, spettanzeSoci.getPrimaryKey()) == null) {
                cacheResult(spettanzeSoci);
            } else {
                spettanzeSoci.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all spettanze socis.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(SpettanzeSociImpl.class.getName());
        }

        EntityCacheUtil.clearCache(SpettanzeSociImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the spettanze soci.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(SpettanzeSoci spettanzeSoci) {
        EntityCacheUtil.removeResult(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociImpl.class, spettanzeSoci.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        clearUniqueFindersCache(spettanzeSoci);
    }

    @Override
    public void clearCache(List<SpettanzeSoci> spettanzeSocis) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (SpettanzeSoci spettanzeSoci : spettanzeSocis) {
            EntityCacheUtil.removeResult(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
                SpettanzeSociImpl.class, spettanzeSoci.getPrimaryKey());

            clearUniqueFindersCache(spettanzeSoci);
        }
    }

    protected void cacheUniqueFindersCache(SpettanzeSoci spettanzeSoci) {
        if (spettanzeSoci.isNew()) {
            Object[] args = new Object[] {
                    spettanzeSoci.getAnno(), spettanzeSoci.getCodiceAttivita(),
                    spettanzeSoci.getCodiceCentro(),
                    spettanzeSoci.getNumeroProtocollo(),
                    spettanzeSoci.getCodiceFornitore(), spettanzeSoci.getStato()
                };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_FATTURAVENDITASTATO,
                args, Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FATTURAVENDITASTATO,
                args, spettanzeSoci);

            args = new Object[] {
                    spettanzeSoci.getAnno(), spettanzeSoci.getCodiceAttivita(),
                    spettanzeSoci.getCodiceCentro(),
                    spettanzeSoci.getNumeroProtocollo(),
                    spettanzeSoci.getCodiceFornitore()
                };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_FATTURAVENDITA,
                args, Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FATTURAVENDITA,
                args, spettanzeSoci);
        } else {
            SpettanzeSociModelImpl spettanzeSociModelImpl = (SpettanzeSociModelImpl) spettanzeSoci;

            if ((spettanzeSociModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_FATTURAVENDITASTATO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        spettanzeSoci.getAnno(),
                        spettanzeSoci.getCodiceAttivita(),
                        spettanzeSoci.getCodiceCentro(),
                        spettanzeSoci.getNumeroProtocollo(),
                        spettanzeSoci.getCodiceFornitore(),
                        spettanzeSoci.getStato()
                    };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_FATTURAVENDITASTATO,
                    args, Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FATTURAVENDITASTATO,
                    args, spettanzeSoci);
            }

            if ((spettanzeSociModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_FATTURAVENDITA.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        spettanzeSoci.getAnno(),
                        spettanzeSoci.getCodiceAttivita(),
                        spettanzeSoci.getCodiceCentro(),
                        spettanzeSoci.getNumeroProtocollo(),
                        spettanzeSoci.getCodiceFornitore()
                    };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_FATTURAVENDITA,
                    args, Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FATTURAVENDITA,
                    args, spettanzeSoci);
            }
        }
    }

    protected void clearUniqueFindersCache(SpettanzeSoci spettanzeSoci) {
        SpettanzeSociModelImpl spettanzeSociModelImpl = (SpettanzeSociModelImpl) spettanzeSoci;

        Object[] args = new Object[] {
                spettanzeSoci.getAnno(), spettanzeSoci.getCodiceAttivita(),
                spettanzeSoci.getCodiceCentro(),
                spettanzeSoci.getNumeroProtocollo(),
                spettanzeSoci.getCodiceFornitore(), spettanzeSoci.getStato()
            };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FATTURAVENDITASTATO,
            args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_FATTURAVENDITASTATO,
            args);

        if ((spettanzeSociModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_FATTURAVENDITASTATO.getColumnBitmask()) != 0) {
            args = new Object[] {
                    spettanzeSociModelImpl.getOriginalAnno(),
                    spettanzeSociModelImpl.getOriginalCodiceAttivita(),
                    spettanzeSociModelImpl.getOriginalCodiceCentro(),
                    spettanzeSociModelImpl.getOriginalNumeroProtocollo(),
                    spettanzeSociModelImpl.getOriginalCodiceFornitore(),
                    spettanzeSociModelImpl.getOriginalStato()
                };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FATTURAVENDITASTATO,
                args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_FATTURAVENDITASTATO,
                args);
        }

        args = new Object[] {
                spettanzeSoci.getAnno(), spettanzeSoci.getCodiceAttivita(),
                spettanzeSoci.getCodiceCentro(),
                spettanzeSoci.getNumeroProtocollo(),
                spettanzeSoci.getCodiceFornitore()
            };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FATTURAVENDITA, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_FATTURAVENDITA, args);

        if ((spettanzeSociModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_FATTURAVENDITA.getColumnBitmask()) != 0) {
            args = new Object[] {
                    spettanzeSociModelImpl.getOriginalAnno(),
                    spettanzeSociModelImpl.getOriginalCodiceAttivita(),
                    spettanzeSociModelImpl.getOriginalCodiceCentro(),
                    spettanzeSociModelImpl.getOriginalNumeroProtocollo(),
                    spettanzeSociModelImpl.getOriginalCodiceFornitore()
                };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FATTURAVENDITA,
                args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_FATTURAVENDITA,
                args);
        }
    }

    /**
     * Creates a new spettanze soci with the primary key. Does not add the spettanze soci to the database.
     *
     * @param id the primary key for the new spettanze soci
     * @return the new spettanze soci
     */
    @Override
    public SpettanzeSoci create(long id) {
        SpettanzeSoci spettanzeSoci = new SpettanzeSociImpl();

        spettanzeSoci.setNew(true);
        spettanzeSoci.setPrimaryKey(id);

        return spettanzeSoci;
    }

    /**
     * Removes the spettanze soci with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the spettanze soci
     * @return the spettanze soci that was removed
     * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a spettanze soci with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci remove(long id)
        throws NoSuchSpettanzeSociException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the spettanze soci with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the spettanze soci
     * @return the spettanze soci that was removed
     * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a spettanze soci with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci remove(Serializable primaryKey)
        throws NoSuchSpettanzeSociException, SystemException {
        Session session = null;

        try {
            session = openSession();

            SpettanzeSoci spettanzeSoci = (SpettanzeSoci) session.get(SpettanzeSociImpl.class,
                    primaryKey);

            if (spettanzeSoci == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchSpettanzeSociException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(spettanzeSoci);
        } catch (NoSuchSpettanzeSociException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected SpettanzeSoci removeImpl(SpettanzeSoci spettanzeSoci)
        throws SystemException {
        spettanzeSoci = toUnwrappedModel(spettanzeSoci);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(spettanzeSoci)) {
                spettanzeSoci = (SpettanzeSoci) session.get(SpettanzeSociImpl.class,
                        spettanzeSoci.getPrimaryKeyObj());
            }

            if (spettanzeSoci != null) {
                session.delete(spettanzeSoci);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (spettanzeSoci != null) {
            clearCache(spettanzeSoci);
        }

        return spettanzeSoci;
    }

    @Override
    public SpettanzeSoci updateImpl(
        it.bysoftware.ct.model.SpettanzeSoci spettanzeSoci)
        throws SystemException {
        spettanzeSoci = toUnwrappedModel(spettanzeSoci);

        boolean isNew = spettanzeSoci.isNew();

        SpettanzeSociModelImpl spettanzeSociModelImpl = (SpettanzeSociModelImpl) spettanzeSoci;

        Session session = null;

        try {
            session = openSession();

            if (spettanzeSoci.isNew()) {
                session.save(spettanzeSoci);

                spettanzeSoci.setNew(false);
            } else {
                session.merge(spettanzeSoci);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !SpettanzeSociModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((spettanzeSociModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITORESTATO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        spettanzeSociModelImpl.getOriginalCodiceFornitore(),
                        spettanzeSociModelImpl.getOriginalStato()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FORNITORESTATO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITORESTATO,
                    args);

                args = new Object[] {
                        spettanzeSociModelImpl.getCodiceFornitore(),
                        spettanzeSociModelImpl.getStato()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FORNITORESTATO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITORESTATO,
                    args);
            }

            if ((spettanzeSociModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITORE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        spettanzeSociModelImpl.getOriginalCodiceFornitore()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FORNITORE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITORE,
                    args);

                args = new Object[] { spettanzeSociModelImpl.getCodiceFornitore() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FORNITORE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITORE,
                    args);
            }

            if ((spettanzeSociModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITOREANNOPAGAMENTOIDPAGAMENTO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        spettanzeSociModelImpl.getOriginalCodiceFornitore(),
                        spettanzeSociModelImpl.getOriginalAnnoPagamento(),
                        spettanzeSociModelImpl.getOriginalIdPagamento()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FORNITOREANNOPAGAMENTOIDPAGAMENTO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITOREANNOPAGAMENTOIDPAGAMENTO,
                    args);

                args = new Object[] {
                        spettanzeSociModelImpl.getCodiceFornitore(),
                        spettanzeSociModelImpl.getAnnoPagamento(),
                        spettanzeSociModelImpl.getIdPagamento()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FORNITOREANNOPAGAMENTOIDPAGAMENTO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITOREANNOPAGAMENTOIDPAGAMENTO,
                    args);
            }
        }

        EntityCacheUtil.putResult(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
            SpettanzeSociImpl.class, spettanzeSoci.getPrimaryKey(),
            spettanzeSoci);

        clearUniqueFindersCache(spettanzeSoci);
        cacheUniqueFindersCache(spettanzeSoci);

        return spettanzeSoci;
    }

    protected SpettanzeSoci toUnwrappedModel(SpettanzeSoci spettanzeSoci) {
        if (spettanzeSoci instanceof SpettanzeSociImpl) {
            return spettanzeSoci;
        }

        SpettanzeSociImpl spettanzeSociImpl = new SpettanzeSociImpl();

        spettanzeSociImpl.setNew(spettanzeSoci.isNew());
        spettanzeSociImpl.setPrimaryKey(spettanzeSoci.getPrimaryKey());

        spettanzeSociImpl.setId(spettanzeSoci.getId());
        spettanzeSociImpl.setAnno(spettanzeSoci.getAnno());
        spettanzeSociImpl.setCodiceAttivita(spettanzeSoci.getCodiceAttivita());
        spettanzeSociImpl.setCodiceCentro(spettanzeSoci.getCodiceCentro());
        spettanzeSociImpl.setNumeroProtocollo(spettanzeSoci.getNumeroProtocollo());
        spettanzeSociImpl.setCodiceFornitore(spettanzeSoci.getCodiceFornitore());
        spettanzeSociImpl.setImporto(spettanzeSoci.getImporto());
        spettanzeSociImpl.setDataCalcolo(spettanzeSoci.getDataCalcolo());
        spettanzeSociImpl.setStato(spettanzeSoci.getStato());
        spettanzeSociImpl.setIdPagamento(spettanzeSoci.getIdPagamento());
        spettanzeSociImpl.setAnnoPagamento(spettanzeSoci.getAnnoPagamento());

        return spettanzeSociImpl;
    }

    /**
     * Returns the spettanze soci with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the spettanze soci
     * @return the spettanze soci
     * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a spettanze soci with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci findByPrimaryKey(Serializable primaryKey)
        throws NoSuchSpettanzeSociException, SystemException {
        SpettanzeSoci spettanzeSoci = fetchByPrimaryKey(primaryKey);

        if (spettanzeSoci == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchSpettanzeSociException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return spettanzeSoci;
    }

    /**
     * Returns the spettanze soci with the primary key or throws a {@link it.bysoftware.ct.NoSuchSpettanzeSociException} if it could not be found.
     *
     * @param id the primary key of the spettanze soci
     * @return the spettanze soci
     * @throws it.bysoftware.ct.NoSuchSpettanzeSociException if a spettanze soci with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci findByPrimaryKey(long id)
        throws NoSuchSpettanzeSociException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the spettanze soci with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the spettanze soci
     * @return the spettanze soci, or <code>null</code> if a spettanze soci with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        SpettanzeSoci spettanzeSoci = (SpettanzeSoci) EntityCacheUtil.getResult(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
                SpettanzeSociImpl.class, primaryKey);

        if (spettanzeSoci == _nullSpettanzeSoci) {
            return null;
        }

        if (spettanzeSoci == null) {
            Session session = null;

            try {
                session = openSession();

                spettanzeSoci = (SpettanzeSoci) session.get(SpettanzeSociImpl.class,
                        primaryKey);

                if (spettanzeSoci != null) {
                    cacheResult(spettanzeSoci);
                } else {
                    EntityCacheUtil.putResult(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
                        SpettanzeSociImpl.class, primaryKey, _nullSpettanzeSoci);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(SpettanzeSociModelImpl.ENTITY_CACHE_ENABLED,
                    SpettanzeSociImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return spettanzeSoci;
    }

    /**
     * Returns the spettanze soci with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the spettanze soci
     * @return the spettanze soci, or <code>null</code> if a spettanze soci with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SpettanzeSoci fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the spettanze socis.
     *
     * @return the spettanze socis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SpettanzeSoci> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the spettanze socis.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of spettanze socis
     * @param end the upper bound of the range of spettanze socis (not inclusive)
     * @return the range of spettanze socis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SpettanzeSoci> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the spettanze socis.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SpettanzeSociModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of spettanze socis
     * @param end the upper bound of the range of spettanze socis (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of spettanze socis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SpettanzeSoci> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<SpettanzeSoci> list = (List<SpettanzeSoci>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_SPETTANZESOCI);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_SPETTANZESOCI;

                if (pagination) {
                    sql = sql.concat(SpettanzeSociModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<SpettanzeSoci>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<SpettanzeSoci>(list);
                } else {
                    list = (List<SpettanzeSoci>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the spettanze socis from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (SpettanzeSoci spettanzeSoci : findAll()) {
            remove(spettanzeSoci);
        }
    }

    /**
     * Returns the number of spettanze socis.
     *
     * @return the number of spettanze socis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_SPETTANZESOCI);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the spettanze soci persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.SpettanzeSoci")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<SpettanzeSoci>> listenersList = new ArrayList<ModelListener<SpettanzeSoci>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<SpettanzeSoci>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(SpettanzeSociImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
