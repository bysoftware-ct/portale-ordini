package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchPianteException;
import it.bysoftware.ct.model.Piante;
import it.bysoftware.ct.model.impl.PianteImpl;
import it.bysoftware.ct.model.impl.PianteModelImpl;
import it.bysoftware.ct.service.persistence.PiantePersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the piante service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see PiantePersistence
 * @see PianteUtil
 * @generated
 */
public class PiantePersistenceImpl extends BasePersistenceImpl<Piante>
    implements PiantePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link PianteUtil} to access the piante persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = PianteImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, PianteImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, PianteImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_FETCH_BY_CODICE = new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, PianteImpl.class,
            FINDER_CLASS_NAME_ENTITY, "fetchByCodice",
            new String[] { String.class.getName() },
            PianteModelImpl.CODICE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_CODICE = new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCodice",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_CODICE_CODICE_1 = "piante.codice IS NULL";
    private static final String _FINDER_COLUMN_CODICE_CODICE_2 = "piante.codice = ?";
    private static final String _FINDER_COLUMN_CODICE_CODICE_3 = "(piante.codice IS NULL OR piante.codice = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NOMECODICELIKE =
        new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, PianteImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByNomeCodiceLike",
            new String[] {
                Boolean.class.getName(), String.class.getName(),
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_NOMECODICELIKE =
        new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByNomeCodiceLike",
            new String[] {
                Boolean.class.getName(), String.class.getName(),
                String.class.getName()
            });
    private static final String _FINDER_COLUMN_NOMECODICELIKE_OBSOLETO_2 = "piante.obsoleto = ? AND ";
    private static final String _FINDER_COLUMN_NOMECODICELIKE_NOME_1 = "piante.nome LIKE NULL AND ";
    private static final String _FINDER_COLUMN_NOMECODICELIKE_NOME_2 = "piante.nome LIKE ? AND ";
    private static final String _FINDER_COLUMN_NOMECODICELIKE_NOME_3 = "(piante.nome IS NULL OR piante.nome LIKE '') AND ";
    private static final String _FINDER_COLUMN_NOMECODICELIKE_CODICE_1 = "piante.codice LIKE NULL";
    private static final String _FINDER_COLUMN_NOMECODICELIKE_CODICE_2 = "piante.codice LIKE ?";
    private static final String _FINDER_COLUMN_NOMECODICELIKE_CODICE_3 = "(piante.codice IS NULL OR piante.codice LIKE '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CATEGORIA =
        new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, PianteImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCategoria",
            new String[] {
                Boolean.class.getName(), Long.class.getName(),
                String.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_CATEGORIA =
        new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByCategoria",
            new String[] {
                Boolean.class.getName(), Long.class.getName(),
                String.class.getName(), String.class.getName()
            });
    private static final String _FINDER_COLUMN_CATEGORIA_OBSOLETO_2 = "piante.obsoleto = ? AND ";
    private static final String _FINDER_COLUMN_CATEGORIA_IDCATEGORIA_2 = "piante.idCategoria = ? AND ";
    private static final String _FINDER_COLUMN_CATEGORIA_NOME_1 = "piante.nome LIKE NULL AND ";
    private static final String _FINDER_COLUMN_CATEGORIA_NOME_2 = "piante.nome LIKE ? AND ";
    private static final String _FINDER_COLUMN_CATEGORIA_NOME_3 = "(piante.nome IS NULL OR piante.nome LIKE '') AND ";
    private static final String _FINDER_COLUMN_CATEGORIA_CODICE_1 = "piante.codice LIKE NULL";
    private static final String _FINDER_COLUMN_CATEGORIA_CODICE_2 = "piante.codice LIKE ?";
    private static final String _FINDER_COLUMN_CATEGORIA_CODICE_3 = "(piante.codice IS NULL OR piante.codice LIKE '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ATTIVANOMECODICELIKE =
        new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, PianteImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByAttivaNomeCodiceLike",
            new String[] {
                Boolean.class.getName(), Boolean.class.getName(),
                String.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_ATTIVANOMECODICELIKE =
        new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "countByAttivaNomeCodiceLike",
            new String[] {
                Boolean.class.getName(), Boolean.class.getName(),
                String.class.getName(), String.class.getName()
            });
    private static final String _FINDER_COLUMN_ATTIVANOMECODICELIKE_OBSOLETO_2 = "piante.obsoleto = ? AND ";
    private static final String _FINDER_COLUMN_ATTIVANOMECODICELIKE_ATTIVA_2 = "piante.attiva = ? AND ";
    private static final String _FINDER_COLUMN_ATTIVANOMECODICELIKE_NOME_1 = "piante.nome LIKE NULL AND ";
    private static final String _FINDER_COLUMN_ATTIVANOMECODICELIKE_NOME_2 = "piante.nome LIKE ? AND ";
    private static final String _FINDER_COLUMN_ATTIVANOMECODICELIKE_NOME_3 = "(piante.nome IS NULL OR piante.nome LIKE '') AND ";
    private static final String _FINDER_COLUMN_ATTIVANOMECODICELIKE_CODICE_1 = "piante.codice LIKE NULL";
    private static final String _FINDER_COLUMN_ATTIVANOMECODICELIKE_CODICE_2 = "piante.codice LIKE ?";
    private static final String _FINDER_COLUMN_ATTIVANOMECODICELIKE_CODICE_3 = "(piante.codice IS NULL OR piante.codice LIKE '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CATEGORIAATTIVA =
        new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, PianteImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCategoriaAttiva",
            new String[] {
                Boolean.class.getName(), Long.class.getName(),
                Boolean.class.getName(), String.class.getName(),
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_CATEGORIAATTIVA =
        new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByCategoriaAttiva",
            new String[] {
                Boolean.class.getName(), Long.class.getName(),
                Boolean.class.getName(), String.class.getName(),
                String.class.getName()
            });
    private static final String _FINDER_COLUMN_CATEGORIAATTIVA_OBSOLETO_2 = "piante.obsoleto = ? AND ";
    private static final String _FINDER_COLUMN_CATEGORIAATTIVA_IDCATEGORIA_2 = "piante.idCategoria = ? AND ";
    private static final String _FINDER_COLUMN_CATEGORIAATTIVA_ATTIVA_2 = "piante.attiva = ? AND ";
    private static final String _FINDER_COLUMN_CATEGORIAATTIVA_NOME_1 = "piante.nome LIKE NULL AND ";
    private static final String _FINDER_COLUMN_CATEGORIAATTIVA_NOME_2 = "piante.nome LIKE ? AND ";
    private static final String _FINDER_COLUMN_CATEGORIAATTIVA_NOME_3 = "(piante.nome IS NULL OR piante.nome LIKE '') AND ";
    private static final String _FINDER_COLUMN_CATEGORIAATTIVA_CODICE_1 = "piante.codice LIKE NULL";
    private static final String _FINDER_COLUMN_CATEGORIAATTIVA_CODICE_2 = "piante.codice LIKE ?";
    private static final String _FINDER_COLUMN_CATEGORIAATTIVA_CODICE_3 = "(piante.codice IS NULL OR piante.codice LIKE '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FORNITORENOMECODICELIKE =
        new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, PianteImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByFornitoreNomeCodiceLike",
            new String[] {
                Boolean.class.getName(), String.class.getName(),
                String.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_FORNITORENOMECODICELIKE =
        new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "countByFornitoreNomeCodiceLike",
            new String[] {
                Boolean.class.getName(), String.class.getName(),
                String.class.getName(), String.class.getName()
            });
    private static final String _FINDER_COLUMN_FORNITORENOMECODICELIKE_OBSOLETO_2 =
        "piante.obsoleto = ? AND ";
    private static final String _FINDER_COLUMN_FORNITORENOMECODICELIKE_IDFORNITORE_1 =
        "piante.idFornitore IS NULL AND ";
    private static final String _FINDER_COLUMN_FORNITORENOMECODICELIKE_IDFORNITORE_2 =
        "piante.idFornitore = ? AND ";
    private static final String _FINDER_COLUMN_FORNITORENOMECODICELIKE_IDFORNITORE_3 =
        "(piante.idFornitore IS NULL OR piante.idFornitore = '') AND ";
    private static final String _FINDER_COLUMN_FORNITORENOMECODICELIKE_NOME_1 = "piante.nome LIKE NULL AND ";
    private static final String _FINDER_COLUMN_FORNITORENOMECODICELIKE_NOME_2 = "piante.nome LIKE ? AND ";
    private static final String _FINDER_COLUMN_FORNITORENOMECODICELIKE_NOME_3 = "(piante.nome IS NULL OR piante.nome LIKE '') AND ";
    private static final String _FINDER_COLUMN_FORNITORENOMECODICELIKE_CODICE_1 = "piante.codice LIKE NULL";
    private static final String _FINDER_COLUMN_FORNITORENOMECODICELIKE_CODICE_2 = "piante.codice LIKE ?";
    private static final String _FINDER_COLUMN_FORNITORENOMECODICELIKE_CODICE_3 = "(piante.codice IS NULL OR piante.codice LIKE '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CATEGORIAFORNITORE =
        new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, PianteImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCategoriaFornitore",
            new String[] {
                Boolean.class.getName(), Long.class.getName(),
                String.class.getName(), String.class.getName(),
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_CATEGORIAFORNITORE =
        new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "countByCategoriaFornitore",
            new String[] {
                Boolean.class.getName(), Long.class.getName(),
                String.class.getName(), String.class.getName(),
                String.class.getName()
            });
    private static final String _FINDER_COLUMN_CATEGORIAFORNITORE_OBSOLETO_2 = "piante.obsoleto = ? AND ";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITORE_IDCATEGORIA_2 = "piante.idCategoria = ? AND ";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITORE_IDFORNITORE_1 = "piante.idFornitore IS NULL AND ";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITORE_IDFORNITORE_2 = "piante.idFornitore = ? AND ";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITORE_IDFORNITORE_3 = "(piante.idFornitore IS NULL OR piante.idFornitore = '') AND ";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITORE_NOME_1 = "piante.nome LIKE NULL AND ";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITORE_NOME_2 = "piante.nome LIKE ? AND ";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITORE_NOME_3 = "(piante.nome IS NULL OR piante.nome LIKE '') AND ";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITORE_CODICE_1 = "piante.codice LIKE NULL";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITORE_CODICE_2 = "piante.codice LIKE ?";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITORE_CODICE_3 = "(piante.codice IS NULL OR piante.codice LIKE '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FORNITOREATTIVANOMECODICELIKE =
        new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, PianteImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByFornitoreAttivaNomeCodiceLike",
            new String[] {
                Boolean.class.getName(), String.class.getName(),
                Boolean.class.getName(), String.class.getName(),
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_FORNITOREATTIVANOMECODICELIKE =
        new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "countByFornitoreAttivaNomeCodiceLike",
            new String[] {
                Boolean.class.getName(), String.class.getName(),
                Boolean.class.getName(), String.class.getName(),
                String.class.getName()
            });
    private static final String _FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_OBSOLETO_2 =
        "piante.obsoleto = ? AND ";
    private static final String _FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_IDFORNITORE_1 =
        "piante.idFornitore IS NULL AND ";
    private static final String _FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_IDFORNITORE_2 =
        "piante.idFornitore = ? AND ";
    private static final String _FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_IDFORNITORE_3 =
        "(piante.idFornitore IS NULL OR piante.idFornitore = '') AND ";
    private static final String _FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_ATTIVA_2 =
        "piante.attiva = ? AND ";
    private static final String _FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_NOME_1 =
        "piante.nome LIKE NULL AND ";
    private static final String _FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_NOME_2 =
        "piante.nome LIKE ? AND ";
    private static final String _FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_NOME_3 =
        "(piante.nome IS NULL OR piante.nome LIKE '') AND ";
    private static final String _FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_CODICE_1 =
        "piante.codice LIKE NULL";
    private static final String _FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_CODICE_2 =
        "piante.codice LIKE ?";
    private static final String _FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_CODICE_3 =
        "(piante.codice IS NULL OR piante.codice LIKE '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CATEGORIAFORNITOREATTIVA =
        new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, PianteImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByCategoriaFornitoreAttiva",
            new String[] {
                Boolean.class.getName(), Long.class.getName(),
                String.class.getName(), Boolean.class.getName(),
                String.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_CATEGORIAFORNITOREATTIVA =
        new FinderPath(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "countByCategoriaFornitoreAttiva",
            new String[] {
                Boolean.class.getName(), Long.class.getName(),
                String.class.getName(), Boolean.class.getName(),
                String.class.getName(), String.class.getName()
            });
    private static final String _FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_OBSOLETO_2 =
        "piante.obsoleto = ? AND ";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_IDCATEGORIA_2 =
        "piante.idCategoria = ? AND ";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_IDFORNITORE_1 =
        "piante.idFornitore IS NULL AND ";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_IDFORNITORE_2 =
        "piante.idFornitore = ? AND ";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_IDFORNITORE_3 =
        "(piante.idFornitore IS NULL OR piante.idFornitore = '') AND ";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_ATTIVA_2 =
        "piante.attiva = ? AND ";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_NOME_1 = "piante.nome LIKE NULL AND ";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_NOME_2 = "piante.nome LIKE ? AND ";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_NOME_3 = "(piante.nome IS NULL OR piante.nome LIKE '') AND ";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_CODICE_1 =
        "piante.codice LIKE NULL";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_CODICE_2 =
        "piante.codice LIKE ?";
    private static final String _FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_CODICE_3 =
        "(piante.codice IS NULL OR piante.codice LIKE '')";
    private static final String _SQL_SELECT_PIANTE = "SELECT piante FROM Piante piante";
    private static final String _SQL_SELECT_PIANTE_WHERE = "SELECT piante FROM Piante piante WHERE ";
    private static final String _SQL_COUNT_PIANTE = "SELECT COUNT(piante) FROM Piante piante";
    private static final String _SQL_COUNT_PIANTE_WHERE = "SELECT COUNT(piante) FROM Piante piante WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "piante.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Piante exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Piante exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(PiantePersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "piantePianale", "pianteCarrello", "altezzaPianta",
                "idCategoria", "idFornitore", "prezzoFornitore", "codice",
                "altezzaChioma", "tempoConsegna", "prezzoB",
                "percScontoMistoAziendale", "prezzoACarrello", "prezzoBCarrello",
                "idForma", "costoRipiano", "ultimoAggiornamentoFoto",
                "scadenzaFoto", "giorniScadenzaFoto", "sormonto2Fila",
                "sormonto3Fila", "sormonto4Fila", "aggiuntaRipiano",
                "bloccoDisponibilita", "pathFile", "prezzoEtichetta",
                "passaportoDefault"
            });
    private static Piante _nullPiante = new PianteImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Piante> toCacheModel() {
                return _nullPianteCacheModel;
            }
        };

    private static CacheModel<Piante> _nullPianteCacheModel = new CacheModel<Piante>() {
            @Override
            public Piante toEntityModel() {
                return _nullPiante;
            }
        };

    public PiantePersistenceImpl() {
        setModelClass(Piante.class);
    }

    /**
     * Returns the piante where codice = &#63; or throws a {@link it.bysoftware.ct.NoSuchPianteException} if it could not be found.
     *
     * @param codice the codice
     * @return the matching piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByCodice(String codice)
        throws NoSuchPianteException, SystemException {
        Piante piante = fetchByCodice(codice);

        if (piante == null) {
            StringBundler msg = new StringBundler(4);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("codice=");
            msg.append(codice);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchPianteException(msg.toString());
        }

        return piante;
    }

    /**
     * Returns the piante where codice = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param codice the codice
     * @return the matching piante, or <code>null</code> if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByCodice(String codice) throws SystemException {
        return fetchByCodice(codice, true);
    }

    /**
     * Returns the piante where codice = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param codice the codice
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching piante, or <code>null</code> if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByCodice(String codice, boolean retrieveFromCache)
        throws SystemException {
        Object[] finderArgs = new Object[] { codice };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_CODICE,
                    finderArgs, this);
        }

        if (result instanceof Piante) {
            Piante piante = (Piante) result;

            if (!Validator.equals(codice, piante.getCodice())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_SELECT_PIANTE_WHERE);

            boolean bindCodice = false;

            if (codice == null) {
                query.append(_FINDER_COLUMN_CODICE_CODICE_1);
            } else if (codice.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICE_CODICE_3);
            } else {
                bindCodice = true;

                query.append(_FINDER_COLUMN_CODICE_CODICE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodice) {
                    qPos.add(codice);
                }

                List<Piante> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODICE,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "PiantePersistenceImpl.fetchByCodice(String, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    Piante piante = list.get(0);

                    result = piante;

                    cacheResult(piante);

                    if ((piante.getCodice() == null) ||
                            !piante.getCodice().equals(codice)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODICE,
                            finderArgs, piante);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CODICE,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (Piante) result;
        }
    }

    /**
     * Removes the piante where codice = &#63; from the database.
     *
     * @param codice the codice
     * @return the piante that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante removeByCodice(String codice)
        throws NoSuchPianteException, SystemException {
        Piante piante = findByCodice(codice);

        return remove(piante);
    }

    /**
     * Returns the number of piantes where codice = &#63;.
     *
     * @param codice the codice
     * @return the number of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByCodice(String codice) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_CODICE;

        Object[] finderArgs = new Object[] { codice };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_PIANTE_WHERE);

            boolean bindCodice = false;

            if (codice == null) {
                query.append(_FINDER_COLUMN_CODICE_CODICE_1);
            } else if (codice.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICE_CODICE_3);
            } else {
                bindCodice = true;

                query.append(_FINDER_COLUMN_CODICE_CODICE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodice) {
                    qPos.add(codice);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the piantes where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param nome the nome
     * @param codice the codice
     * @return the matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByNomeCodiceLike(boolean obsoleto, String nome,
        String codice) throws SystemException {
        return findByNomeCodiceLike(obsoleto, nome, codice, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the piantes where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param obsoleto the obsoleto
     * @param nome the nome
     * @param codice the codice
     * @param start the lower bound of the range of piantes
     * @param end the upper bound of the range of piantes (not inclusive)
     * @return the range of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByNomeCodiceLike(boolean obsoleto, String nome,
        String codice, int start, int end) throws SystemException {
        return findByNomeCodiceLike(obsoleto, nome, codice, start, end, null);
    }

    /**
     * Returns an ordered range of all the piantes where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param obsoleto the obsoleto
     * @param nome the nome
     * @param codice the codice
     * @param start the lower bound of the range of piantes
     * @param end the upper bound of the range of piantes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByNomeCodiceLike(boolean obsoleto, String nome,
        String codice, int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NOMECODICELIKE;
        finderArgs = new Object[] {
                obsoleto, nome, codice,
                
                start, end, orderByComparator
            };

        List<Piante> list = (List<Piante>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Piante piante : list) {
                if ((obsoleto != piante.getObsoleto()) ||
                        !StringUtil.wildcardMatches(piante.getNome(), nome,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, true) ||
                        !StringUtil.wildcardMatches(piante.getCodice(), codice,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, true)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(5 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(5);
            }

            query.append(_SQL_SELECT_PIANTE_WHERE);

            query.append(_FINDER_COLUMN_NOMECODICELIKE_OBSOLETO_2);

            boolean bindNome = false;

            if (nome == null) {
                query.append(_FINDER_COLUMN_NOMECODICELIKE_NOME_1);
            } else if (nome.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NOMECODICELIKE_NOME_3);
            } else {
                bindNome = true;

                query.append(_FINDER_COLUMN_NOMECODICELIKE_NOME_2);
            }

            boolean bindCodice = false;

            if (codice == null) {
                query.append(_FINDER_COLUMN_NOMECODICELIKE_CODICE_1);
            } else if (codice.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NOMECODICELIKE_CODICE_3);
            } else {
                bindCodice = true;

                query.append(_FINDER_COLUMN_NOMECODICELIKE_CODICE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PianteModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(obsoleto);

                if (bindNome) {
                    qPos.add(nome);
                }

                if (bindCodice) {
                    qPos.add(codice);
                }

                if (!pagination) {
                    list = (List<Piante>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Piante>(list);
                } else {
                    list = (List<Piante>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first piante in the ordered set where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByNomeCodiceLike_First(boolean obsoleto, String nome,
        String codice, OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = fetchByNomeCodiceLike_First(obsoleto, nome, codice,
                orderByComparator);

        if (piante != null) {
            return piante;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("obsoleto=");
        msg.append(obsoleto);

        msg.append(", nome=");
        msg.append(nome);

        msg.append(", codice=");
        msg.append(codice);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPianteException(msg.toString());
    }

    /**
     * Returns the first piante in the ordered set where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching piante, or <code>null</code> if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByNomeCodiceLike_First(boolean obsoleto, String nome,
        String codice, OrderByComparator orderByComparator)
        throws SystemException {
        List<Piante> list = findByNomeCodiceLike(obsoleto, nome, codice, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last piante in the ordered set where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByNomeCodiceLike_Last(boolean obsoleto, String nome,
        String codice, OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = fetchByNomeCodiceLike_Last(obsoleto, nome, codice,
                orderByComparator);

        if (piante != null) {
            return piante;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("obsoleto=");
        msg.append(obsoleto);

        msg.append(", nome=");
        msg.append(nome);

        msg.append(", codice=");
        msg.append(codice);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPianteException(msg.toString());
    }

    /**
     * Returns the last piante in the ordered set where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching piante, or <code>null</code> if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByNomeCodiceLike_Last(boolean obsoleto, String nome,
        String codice, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByNomeCodiceLike(obsoleto, nome, codice);

        if (count == 0) {
            return null;
        }

        List<Piante> list = findByNomeCodiceLike(obsoleto, nome, codice,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param id the primary key of the current piante
     * @param obsoleto the obsoleto
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante[] findByNomeCodiceLike_PrevAndNext(long id, boolean obsoleto,
        String nome, String codice, OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Piante[] array = new PianteImpl[3];

            array[0] = getByNomeCodiceLike_PrevAndNext(session, piante,
                    obsoleto, nome, codice, orderByComparator, true);

            array[1] = piante;

            array[2] = getByNomeCodiceLike_PrevAndNext(session, piante,
                    obsoleto, nome, codice, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Piante getByNomeCodiceLike_PrevAndNext(Session session,
        Piante piante, boolean obsoleto, String nome, String codice,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PIANTE_WHERE);

        query.append(_FINDER_COLUMN_NOMECODICELIKE_OBSOLETO_2);

        boolean bindNome = false;

        if (nome == null) {
            query.append(_FINDER_COLUMN_NOMECODICELIKE_NOME_1);
        } else if (nome.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_NOMECODICELIKE_NOME_3);
        } else {
            bindNome = true;

            query.append(_FINDER_COLUMN_NOMECODICELIKE_NOME_2);
        }

        boolean bindCodice = false;

        if (codice == null) {
            query.append(_FINDER_COLUMN_NOMECODICELIKE_CODICE_1);
        } else if (codice.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_NOMECODICELIKE_CODICE_3);
        } else {
            bindCodice = true;

            query.append(_FINDER_COLUMN_NOMECODICELIKE_CODICE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(PianteModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(obsoleto);

        if (bindNome) {
            qPos.add(nome);
        }

        if (bindCodice) {
            qPos.add(codice);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(piante);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Piante> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the piantes where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
     *
     * @param obsoleto the obsoleto
     * @param nome the nome
     * @param codice the codice
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByNomeCodiceLike(boolean obsoleto, String nome,
        String codice) throws SystemException {
        for (Piante piante : findByNomeCodiceLike(obsoleto, nome, codice,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(piante);
        }
    }

    /**
     * Returns the number of piantes where obsoleto = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param nome the nome
     * @param codice the codice
     * @return the number of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByNomeCodiceLike(boolean obsoleto, String nome,
        String codice) throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_NOMECODICELIKE;

        Object[] finderArgs = new Object[] { obsoleto, nome, codice };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(4);

            query.append(_SQL_COUNT_PIANTE_WHERE);

            query.append(_FINDER_COLUMN_NOMECODICELIKE_OBSOLETO_2);

            boolean bindNome = false;

            if (nome == null) {
                query.append(_FINDER_COLUMN_NOMECODICELIKE_NOME_1);
            } else if (nome.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NOMECODICELIKE_NOME_3);
            } else {
                bindNome = true;

                query.append(_FINDER_COLUMN_NOMECODICELIKE_NOME_2);
            }

            boolean bindCodice = false;

            if (codice == null) {
                query.append(_FINDER_COLUMN_NOMECODICELIKE_CODICE_1);
            } else if (codice.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NOMECODICELIKE_CODICE_3);
            } else {
                bindCodice = true;

                query.append(_FINDER_COLUMN_NOMECODICELIKE_CODICE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(obsoleto);

                if (bindNome) {
                    qPos.add(nome);
                }

                if (bindCodice) {
                    qPos.add(codice);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the piantes where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param nome the nome
     * @param codice the codice
     * @return the matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByCategoria(boolean obsoleto, long idCategoria,
        String nome, String codice) throws SystemException {
        return findByCategoria(obsoleto, idCategoria, nome, codice,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param nome the nome
     * @param codice the codice
     * @param start the lower bound of the range of piantes
     * @param end the upper bound of the range of piantes (not inclusive)
     * @return the range of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByCategoria(boolean obsoleto, long idCategoria,
        String nome, String codice, int start, int end)
        throws SystemException {
        return findByCategoria(obsoleto, idCategoria, nome, codice, start, end,
            null);
    }

    /**
     * Returns an ordered range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param nome the nome
     * @param codice the codice
     * @param start the lower bound of the range of piantes
     * @param end the upper bound of the range of piantes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByCategoria(boolean obsoleto, long idCategoria,
        String nome, String codice, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CATEGORIA;
        finderArgs = new Object[] {
                obsoleto, idCategoria, nome, codice,
                
                start, end, orderByComparator
            };

        List<Piante> list = (List<Piante>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Piante piante : list) {
                if ((obsoleto != piante.getObsoleto()) ||
                        (idCategoria != piante.getIdCategoria()) ||
                        !StringUtil.wildcardMatches(piante.getNome(), nome,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, true) ||
                        !StringUtil.wildcardMatches(piante.getCodice(), codice,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, true)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(6 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(6);
            }

            query.append(_SQL_SELECT_PIANTE_WHERE);

            query.append(_FINDER_COLUMN_CATEGORIA_OBSOLETO_2);

            query.append(_FINDER_COLUMN_CATEGORIA_IDCATEGORIA_2);

            boolean bindNome = false;

            if (nome == null) {
                query.append(_FINDER_COLUMN_CATEGORIA_NOME_1);
            } else if (nome.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIA_NOME_3);
            } else {
                bindNome = true;

                query.append(_FINDER_COLUMN_CATEGORIA_NOME_2);
            }

            boolean bindCodice = false;

            if (codice == null) {
                query.append(_FINDER_COLUMN_CATEGORIA_CODICE_1);
            } else if (codice.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIA_CODICE_3);
            } else {
                bindCodice = true;

                query.append(_FINDER_COLUMN_CATEGORIA_CODICE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PianteModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(obsoleto);

                qPos.add(idCategoria);

                if (bindNome) {
                    qPos.add(nome);
                }

                if (bindCodice) {
                    qPos.add(codice);
                }

                if (!pagination) {
                    list = (List<Piante>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Piante>(list);
                } else {
                    list = (List<Piante>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByCategoria_First(boolean obsoleto, long idCategoria,
        String nome, String codice, OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = fetchByCategoria_First(obsoleto, idCategoria, nome,
                codice, orderByComparator);

        if (piante != null) {
            return piante;
        }

        StringBundler msg = new StringBundler(10);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("obsoleto=");
        msg.append(obsoleto);

        msg.append(", idCategoria=");
        msg.append(idCategoria);

        msg.append(", nome=");
        msg.append(nome);

        msg.append(", codice=");
        msg.append(codice);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPianteException(msg.toString());
    }

    /**
     * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching piante, or <code>null</code> if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByCategoria_First(boolean obsoleto, long idCategoria,
        String nome, String codice, OrderByComparator orderByComparator)
        throws SystemException {
        List<Piante> list = findByCategoria(obsoleto, idCategoria, nome,
                codice, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByCategoria_Last(boolean obsoleto, long idCategoria,
        String nome, String codice, OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = fetchByCategoria_Last(obsoleto, idCategoria, nome,
                codice, orderByComparator);

        if (piante != null) {
            return piante;
        }

        StringBundler msg = new StringBundler(10);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("obsoleto=");
        msg.append(obsoleto);

        msg.append(", idCategoria=");
        msg.append(idCategoria);

        msg.append(", nome=");
        msg.append(nome);

        msg.append(", codice=");
        msg.append(codice);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPianteException(msg.toString());
    }

    /**
     * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching piante, or <code>null</code> if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByCategoria_Last(boolean obsoleto, long idCategoria,
        String nome, String codice, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByCategoria(obsoleto, idCategoria, nome, codice);

        if (count == 0) {
            return null;
        }

        List<Piante> list = findByCategoria(obsoleto, idCategoria, nome,
                codice, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param id the primary key of the current piante
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante[] findByCategoria_PrevAndNext(long id, boolean obsoleto,
        long idCategoria, String nome, String codice,
        OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Piante[] array = new PianteImpl[3];

            array[0] = getByCategoria_PrevAndNext(session, piante, obsoleto,
                    idCategoria, nome, codice, orderByComparator, true);

            array[1] = piante;

            array[2] = getByCategoria_PrevAndNext(session, piante, obsoleto,
                    idCategoria, nome, codice, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Piante getByCategoria_PrevAndNext(Session session, Piante piante,
        boolean obsoleto, long idCategoria, String nome, String codice,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PIANTE_WHERE);

        query.append(_FINDER_COLUMN_CATEGORIA_OBSOLETO_2);

        query.append(_FINDER_COLUMN_CATEGORIA_IDCATEGORIA_2);

        boolean bindNome = false;

        if (nome == null) {
            query.append(_FINDER_COLUMN_CATEGORIA_NOME_1);
        } else if (nome.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CATEGORIA_NOME_3);
        } else {
            bindNome = true;

            query.append(_FINDER_COLUMN_CATEGORIA_NOME_2);
        }

        boolean bindCodice = false;

        if (codice == null) {
            query.append(_FINDER_COLUMN_CATEGORIA_CODICE_1);
        } else if (codice.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CATEGORIA_CODICE_3);
        } else {
            bindCodice = true;

            query.append(_FINDER_COLUMN_CATEGORIA_CODICE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(PianteModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(obsoleto);

        qPos.add(idCategoria);

        if (bindNome) {
            qPos.add(nome);
        }

        if (bindCodice) {
            qPos.add(codice);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(piante);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Piante> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the piantes where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param nome the nome
     * @param codice the codice
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByCategoria(boolean obsoleto, long idCategoria,
        String nome, String codice) throws SystemException {
        for (Piante piante : findByCategoria(obsoleto, idCategoria, nome,
                codice, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(piante);
        }
    }

    /**
     * Returns the number of piantes where obsoleto = &#63; and idCategoria = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param nome the nome
     * @param codice the codice
     * @return the number of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByCategoria(boolean obsoleto, long idCategoria,
        String nome, String codice) throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_CATEGORIA;

        Object[] finderArgs = new Object[] { obsoleto, idCategoria, nome, codice };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(5);

            query.append(_SQL_COUNT_PIANTE_WHERE);

            query.append(_FINDER_COLUMN_CATEGORIA_OBSOLETO_2);

            query.append(_FINDER_COLUMN_CATEGORIA_IDCATEGORIA_2);

            boolean bindNome = false;

            if (nome == null) {
                query.append(_FINDER_COLUMN_CATEGORIA_NOME_1);
            } else if (nome.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIA_NOME_3);
            } else {
                bindNome = true;

                query.append(_FINDER_COLUMN_CATEGORIA_NOME_2);
            }

            boolean bindCodice = false;

            if (codice == null) {
                query.append(_FINDER_COLUMN_CATEGORIA_CODICE_1);
            } else if (codice.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIA_CODICE_3);
            } else {
                bindCodice = true;

                query.append(_FINDER_COLUMN_CATEGORIA_CODICE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(obsoleto);

                qPos.add(idCategoria);

                if (bindNome) {
                    qPos.add(nome);
                }

                if (bindCodice) {
                    qPos.add(codice);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the piantes where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @return the matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByAttivaNomeCodiceLike(boolean obsoleto,
        boolean attiva, String nome, String codice) throws SystemException {
        return findByAttivaNomeCodiceLike(obsoleto, attiva, nome, codice,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the piantes where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param obsoleto the obsoleto
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param start the lower bound of the range of piantes
     * @param end the upper bound of the range of piantes (not inclusive)
     * @return the range of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByAttivaNomeCodiceLike(boolean obsoleto,
        boolean attiva, String nome, String codice, int start, int end)
        throws SystemException {
        return findByAttivaNomeCodiceLike(obsoleto, attiva, nome, codice,
            start, end, null);
    }

    /**
     * Returns an ordered range of all the piantes where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param obsoleto the obsoleto
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param start the lower bound of the range of piantes
     * @param end the upper bound of the range of piantes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByAttivaNomeCodiceLike(boolean obsoleto,
        boolean attiva, String nome, String codice, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ATTIVANOMECODICELIKE;
        finderArgs = new Object[] {
                obsoleto, attiva, nome, codice,
                
                start, end, orderByComparator
            };

        List<Piante> list = (List<Piante>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Piante piante : list) {
                if ((obsoleto != piante.getObsoleto()) ||
                        (attiva != piante.getAttiva()) ||
                        !StringUtil.wildcardMatches(piante.getNome(), nome,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, true) ||
                        !StringUtil.wildcardMatches(piante.getCodice(), codice,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, true)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(6 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(6);
            }

            query.append(_SQL_SELECT_PIANTE_WHERE);

            query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_OBSOLETO_2);

            query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_ATTIVA_2);

            boolean bindNome = false;

            if (nome == null) {
                query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_NOME_1);
            } else if (nome.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_NOME_3);
            } else {
                bindNome = true;

                query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_NOME_2);
            }

            boolean bindCodice = false;

            if (codice == null) {
                query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_CODICE_1);
            } else if (codice.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_CODICE_3);
            } else {
                bindCodice = true;

                query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_CODICE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PianteModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(obsoleto);

                qPos.add(attiva);

                if (bindNome) {
                    qPos.add(nome);
                }

                if (bindCodice) {
                    qPos.add(codice);
                }

                if (!pagination) {
                    list = (List<Piante>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Piante>(list);
                } else {
                    list = (List<Piante>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first piante in the ordered set where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByAttivaNomeCodiceLike_First(boolean obsoleto,
        boolean attiva, String nome, String codice,
        OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = fetchByAttivaNomeCodiceLike_First(obsoleto, attiva,
                nome, codice, orderByComparator);

        if (piante != null) {
            return piante;
        }

        StringBundler msg = new StringBundler(10);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("obsoleto=");
        msg.append(obsoleto);

        msg.append(", attiva=");
        msg.append(attiva);

        msg.append(", nome=");
        msg.append(nome);

        msg.append(", codice=");
        msg.append(codice);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPianteException(msg.toString());
    }

    /**
     * Returns the first piante in the ordered set where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching piante, or <code>null</code> if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByAttivaNomeCodiceLike_First(boolean obsoleto,
        boolean attiva, String nome, String codice,
        OrderByComparator orderByComparator) throws SystemException {
        List<Piante> list = findByAttivaNomeCodiceLike(obsoleto, attiva, nome,
                codice, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last piante in the ordered set where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByAttivaNomeCodiceLike_Last(boolean obsoleto,
        boolean attiva, String nome, String codice,
        OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = fetchByAttivaNomeCodiceLike_Last(obsoleto, attiva,
                nome, codice, orderByComparator);

        if (piante != null) {
            return piante;
        }

        StringBundler msg = new StringBundler(10);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("obsoleto=");
        msg.append(obsoleto);

        msg.append(", attiva=");
        msg.append(attiva);

        msg.append(", nome=");
        msg.append(nome);

        msg.append(", codice=");
        msg.append(codice);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPianteException(msg.toString());
    }

    /**
     * Returns the last piante in the ordered set where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching piante, or <code>null</code> if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByAttivaNomeCodiceLike_Last(boolean obsoleto,
        boolean attiva, String nome, String codice,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByAttivaNomeCodiceLike(obsoleto, attiva, nome, codice);

        if (count == 0) {
            return null;
        }

        List<Piante> list = findByAttivaNomeCodiceLike(obsoleto, attiva, nome,
                codice, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param id the primary key of the current piante
     * @param obsoleto the obsoleto
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante[] findByAttivaNomeCodiceLike_PrevAndNext(long id,
        boolean obsoleto, boolean attiva, String nome, String codice,
        OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Piante[] array = new PianteImpl[3];

            array[0] = getByAttivaNomeCodiceLike_PrevAndNext(session, piante,
                    obsoleto, attiva, nome, codice, orderByComparator, true);

            array[1] = piante;

            array[2] = getByAttivaNomeCodiceLike_PrevAndNext(session, piante,
                    obsoleto, attiva, nome, codice, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Piante getByAttivaNomeCodiceLike_PrevAndNext(Session session,
        Piante piante, boolean obsoleto, boolean attiva, String nome,
        String codice, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PIANTE_WHERE);

        query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_OBSOLETO_2);

        query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_ATTIVA_2);

        boolean bindNome = false;

        if (nome == null) {
            query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_NOME_1);
        } else if (nome.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_NOME_3);
        } else {
            bindNome = true;

            query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_NOME_2);
        }

        boolean bindCodice = false;

        if (codice == null) {
            query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_CODICE_1);
        } else if (codice.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_CODICE_3);
        } else {
            bindCodice = true;

            query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_CODICE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(PianteModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(obsoleto);

        qPos.add(attiva);

        if (bindNome) {
            qPos.add(nome);
        }

        if (bindCodice) {
            qPos.add(codice);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(piante);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Piante> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the piantes where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
     *
     * @param obsoleto the obsoleto
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByAttivaNomeCodiceLike(boolean obsoleto, boolean attiva,
        String nome, String codice) throws SystemException {
        for (Piante piante : findByAttivaNomeCodiceLike(obsoleto, attiva, nome,
                codice, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(piante);
        }
    }

    /**
     * Returns the number of piantes where obsoleto = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @return the number of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByAttivaNomeCodiceLike(boolean obsoleto, boolean attiva,
        String nome, String codice) throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_ATTIVANOMECODICELIKE;

        Object[] finderArgs = new Object[] { obsoleto, attiva, nome, codice };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(5);

            query.append(_SQL_COUNT_PIANTE_WHERE);

            query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_OBSOLETO_2);

            query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_ATTIVA_2);

            boolean bindNome = false;

            if (nome == null) {
                query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_NOME_1);
            } else if (nome.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_NOME_3);
            } else {
                bindNome = true;

                query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_NOME_2);
            }

            boolean bindCodice = false;

            if (codice == null) {
                query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_CODICE_1);
            } else if (codice.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_CODICE_3);
            } else {
                bindCodice = true;

                query.append(_FINDER_COLUMN_ATTIVANOMECODICELIKE_CODICE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(obsoleto);

                qPos.add(attiva);

                if (bindNome) {
                    qPos.add(nome);
                }

                if (bindCodice) {
                    qPos.add(codice);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the piantes where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @return the matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByCategoriaAttiva(boolean obsoleto,
        long idCategoria, boolean attiva, String nome, String codice)
        throws SystemException {
        return findByCategoriaAttiva(obsoleto, idCategoria, attiva, nome,
            codice, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param start the lower bound of the range of piantes
     * @param end the upper bound of the range of piantes (not inclusive)
     * @return the range of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByCategoriaAttiva(boolean obsoleto,
        long idCategoria, boolean attiva, String nome, String codice,
        int start, int end) throws SystemException {
        return findByCategoriaAttiva(obsoleto, idCategoria, attiva, nome,
            codice, start, end, null);
    }

    /**
     * Returns an ordered range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param start the lower bound of the range of piantes
     * @param end the upper bound of the range of piantes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByCategoriaAttiva(boolean obsoleto,
        long idCategoria, boolean attiva, String nome, String codice,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CATEGORIAATTIVA;
        finderArgs = new Object[] {
                obsoleto, idCategoria, attiva, nome, codice,
                
                start, end, orderByComparator
            };

        List<Piante> list = (List<Piante>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Piante piante : list) {
                if ((obsoleto != piante.getObsoleto()) ||
                        (idCategoria != piante.getIdCategoria()) ||
                        (attiva != piante.getAttiva()) ||
                        !StringUtil.wildcardMatches(piante.getNome(), nome,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, true) ||
                        !StringUtil.wildcardMatches(piante.getCodice(), codice,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, true)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(7 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(7);
            }

            query.append(_SQL_SELECT_PIANTE_WHERE);

            query.append(_FINDER_COLUMN_CATEGORIAATTIVA_OBSOLETO_2);

            query.append(_FINDER_COLUMN_CATEGORIAATTIVA_IDCATEGORIA_2);

            query.append(_FINDER_COLUMN_CATEGORIAATTIVA_ATTIVA_2);

            boolean bindNome = false;

            if (nome == null) {
                query.append(_FINDER_COLUMN_CATEGORIAATTIVA_NOME_1);
            } else if (nome.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIAATTIVA_NOME_3);
            } else {
                bindNome = true;

                query.append(_FINDER_COLUMN_CATEGORIAATTIVA_NOME_2);
            }

            boolean bindCodice = false;

            if (codice == null) {
                query.append(_FINDER_COLUMN_CATEGORIAATTIVA_CODICE_1);
            } else if (codice.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIAATTIVA_CODICE_3);
            } else {
                bindCodice = true;

                query.append(_FINDER_COLUMN_CATEGORIAATTIVA_CODICE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PianteModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(obsoleto);

                qPos.add(idCategoria);

                qPos.add(attiva);

                if (bindNome) {
                    qPos.add(nome);
                }

                if (bindCodice) {
                    qPos.add(codice);
                }

                if (!pagination) {
                    list = (List<Piante>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Piante>(list);
                } else {
                    list = (List<Piante>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByCategoriaAttiva_First(boolean obsoleto,
        long idCategoria, boolean attiva, String nome, String codice,
        OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = fetchByCategoriaAttiva_First(obsoleto, idCategoria,
                attiva, nome, codice, orderByComparator);

        if (piante != null) {
            return piante;
        }

        StringBundler msg = new StringBundler(12);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("obsoleto=");
        msg.append(obsoleto);

        msg.append(", idCategoria=");
        msg.append(idCategoria);

        msg.append(", attiva=");
        msg.append(attiva);

        msg.append(", nome=");
        msg.append(nome);

        msg.append(", codice=");
        msg.append(codice);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPianteException(msg.toString());
    }

    /**
     * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching piante, or <code>null</code> if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByCategoriaAttiva_First(boolean obsoleto,
        long idCategoria, boolean attiva, String nome, String codice,
        OrderByComparator orderByComparator) throws SystemException {
        List<Piante> list = findByCategoriaAttiva(obsoleto, idCategoria,
                attiva, nome, codice, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByCategoriaAttiva_Last(boolean obsoleto,
        long idCategoria, boolean attiva, String nome, String codice,
        OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = fetchByCategoriaAttiva_Last(obsoleto, idCategoria,
                attiva, nome, codice, orderByComparator);

        if (piante != null) {
            return piante;
        }

        StringBundler msg = new StringBundler(12);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("obsoleto=");
        msg.append(obsoleto);

        msg.append(", idCategoria=");
        msg.append(idCategoria);

        msg.append(", attiva=");
        msg.append(attiva);

        msg.append(", nome=");
        msg.append(nome);

        msg.append(", codice=");
        msg.append(codice);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPianteException(msg.toString());
    }

    /**
     * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching piante, or <code>null</code> if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByCategoriaAttiva_Last(boolean obsoleto,
        long idCategoria, boolean attiva, String nome, String codice,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByCategoriaAttiva(obsoleto, idCategoria, attiva, nome,
                codice);

        if (count == 0) {
            return null;
        }

        List<Piante> list = findByCategoriaAttiva(obsoleto, idCategoria,
                attiva, nome, codice, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param id the primary key of the current piante
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante[] findByCategoriaAttiva_PrevAndNext(long id,
        boolean obsoleto, long idCategoria, boolean attiva, String nome,
        String codice, OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Piante[] array = new PianteImpl[3];

            array[0] = getByCategoriaAttiva_PrevAndNext(session, piante,
                    obsoleto, idCategoria, attiva, nome, codice,
                    orderByComparator, true);

            array[1] = piante;

            array[2] = getByCategoriaAttiva_PrevAndNext(session, piante,
                    obsoleto, idCategoria, attiva, nome, codice,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Piante getByCategoriaAttiva_PrevAndNext(Session session,
        Piante piante, boolean obsoleto, long idCategoria, boolean attiva,
        String nome, String codice, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PIANTE_WHERE);

        query.append(_FINDER_COLUMN_CATEGORIAATTIVA_OBSOLETO_2);

        query.append(_FINDER_COLUMN_CATEGORIAATTIVA_IDCATEGORIA_2);

        query.append(_FINDER_COLUMN_CATEGORIAATTIVA_ATTIVA_2);

        boolean bindNome = false;

        if (nome == null) {
            query.append(_FINDER_COLUMN_CATEGORIAATTIVA_NOME_1);
        } else if (nome.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CATEGORIAATTIVA_NOME_3);
        } else {
            bindNome = true;

            query.append(_FINDER_COLUMN_CATEGORIAATTIVA_NOME_2);
        }

        boolean bindCodice = false;

        if (codice == null) {
            query.append(_FINDER_COLUMN_CATEGORIAATTIVA_CODICE_1);
        } else if (codice.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CATEGORIAATTIVA_CODICE_3);
        } else {
            bindCodice = true;

            query.append(_FINDER_COLUMN_CATEGORIAATTIVA_CODICE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(PianteModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(obsoleto);

        qPos.add(idCategoria);

        qPos.add(attiva);

        if (bindNome) {
            qPos.add(nome);
        }

        if (bindCodice) {
            qPos.add(codice);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(piante);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Piante> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the piantes where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByCategoriaAttiva(boolean obsoleto, long idCategoria,
        boolean attiva, String nome, String codice) throws SystemException {
        for (Piante piante : findByCategoriaAttiva(obsoleto, idCategoria,
                attiva, nome, codice, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(piante);
        }
    }

    /**
     * Returns the number of piantes where obsoleto = &#63; and idCategoria = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @return the number of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByCategoriaAttiva(boolean obsoleto, long idCategoria,
        boolean attiva, String nome, String codice) throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_CATEGORIAATTIVA;

        Object[] finderArgs = new Object[] {
                obsoleto, idCategoria, attiva, nome, codice
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(6);

            query.append(_SQL_COUNT_PIANTE_WHERE);

            query.append(_FINDER_COLUMN_CATEGORIAATTIVA_OBSOLETO_2);

            query.append(_FINDER_COLUMN_CATEGORIAATTIVA_IDCATEGORIA_2);

            query.append(_FINDER_COLUMN_CATEGORIAATTIVA_ATTIVA_2);

            boolean bindNome = false;

            if (nome == null) {
                query.append(_FINDER_COLUMN_CATEGORIAATTIVA_NOME_1);
            } else if (nome.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIAATTIVA_NOME_3);
            } else {
                bindNome = true;

                query.append(_FINDER_COLUMN_CATEGORIAATTIVA_NOME_2);
            }

            boolean bindCodice = false;

            if (codice == null) {
                query.append(_FINDER_COLUMN_CATEGORIAATTIVA_CODICE_1);
            } else if (codice.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIAATTIVA_CODICE_3);
            } else {
                bindCodice = true;

                query.append(_FINDER_COLUMN_CATEGORIAATTIVA_CODICE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(obsoleto);

                qPos.add(idCategoria);

                qPos.add(attiva);

                if (bindNome) {
                    qPos.add(nome);
                }

                if (bindCodice) {
                    qPos.add(codice);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the piantes where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @return the matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByFornitoreNomeCodiceLike(boolean obsoleto,
        String idFornitore, String nome, String codice)
        throws SystemException {
        return findByFornitoreNomeCodiceLike(obsoleto, idFornitore, nome,
            codice, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the piantes where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @param start the lower bound of the range of piantes
     * @param end the upper bound of the range of piantes (not inclusive)
     * @return the range of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByFornitoreNomeCodiceLike(boolean obsoleto,
        String idFornitore, String nome, String codice, int start, int end)
        throws SystemException {
        return findByFornitoreNomeCodiceLike(obsoleto, idFornitore, nome,
            codice, start, end, null);
    }

    /**
     * Returns an ordered range of all the piantes where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @param start the lower bound of the range of piantes
     * @param end the upper bound of the range of piantes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByFornitoreNomeCodiceLike(boolean obsoleto,
        String idFornitore, String nome, String codice, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FORNITORENOMECODICELIKE;
        finderArgs = new Object[] {
                obsoleto, idFornitore, nome, codice,
                
                start, end, orderByComparator
            };

        List<Piante> list = (List<Piante>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Piante piante : list) {
                if ((obsoleto != piante.getObsoleto()) ||
                        !Validator.equals(idFornitore, piante.getIdFornitore()) ||
                        !StringUtil.wildcardMatches(piante.getNome(), nome,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, true) ||
                        !StringUtil.wildcardMatches(piante.getCodice(), codice,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, true)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(6 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(6);
            }

            query.append(_SQL_SELECT_PIANTE_WHERE);

            query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_OBSOLETO_2);

            boolean bindIdFornitore = false;

            if (idFornitore == null) {
                query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_IDFORNITORE_1);
            } else if (idFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_IDFORNITORE_3);
            } else {
                bindIdFornitore = true;

                query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_IDFORNITORE_2);
            }

            boolean bindNome = false;

            if (nome == null) {
                query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_NOME_1);
            } else if (nome.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_NOME_3);
            } else {
                bindNome = true;

                query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_NOME_2);
            }

            boolean bindCodice = false;

            if (codice == null) {
                query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_CODICE_1);
            } else if (codice.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_CODICE_3);
            } else {
                bindCodice = true;

                query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_CODICE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PianteModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(obsoleto);

                if (bindIdFornitore) {
                    qPos.add(idFornitore);
                }

                if (bindNome) {
                    qPos.add(nome);
                }

                if (bindCodice) {
                    qPos.add(codice);
                }

                if (!pagination) {
                    list = (List<Piante>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Piante>(list);
                } else {
                    list = (List<Piante>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByFornitoreNomeCodiceLike_First(boolean obsoleto,
        String idFornitore, String nome, String codice,
        OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = fetchByFornitoreNomeCodiceLike_First(obsoleto,
                idFornitore, nome, codice, orderByComparator);

        if (piante != null) {
            return piante;
        }

        StringBundler msg = new StringBundler(10);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("obsoleto=");
        msg.append(obsoleto);

        msg.append(", idFornitore=");
        msg.append(idFornitore);

        msg.append(", nome=");
        msg.append(nome);

        msg.append(", codice=");
        msg.append(codice);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPianteException(msg.toString());
    }

    /**
     * Returns the first piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching piante, or <code>null</code> if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByFornitoreNomeCodiceLike_First(boolean obsoleto,
        String idFornitore, String nome, String codice,
        OrderByComparator orderByComparator) throws SystemException {
        List<Piante> list = findByFornitoreNomeCodiceLike(obsoleto,
                idFornitore, nome, codice, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByFornitoreNomeCodiceLike_Last(boolean obsoleto,
        String idFornitore, String nome, String codice,
        OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = fetchByFornitoreNomeCodiceLike_Last(obsoleto,
                idFornitore, nome, codice, orderByComparator);

        if (piante != null) {
            return piante;
        }

        StringBundler msg = new StringBundler(10);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("obsoleto=");
        msg.append(obsoleto);

        msg.append(", idFornitore=");
        msg.append(idFornitore);

        msg.append(", nome=");
        msg.append(nome);

        msg.append(", codice=");
        msg.append(codice);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPianteException(msg.toString());
    }

    /**
     * Returns the last piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching piante, or <code>null</code> if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByFornitoreNomeCodiceLike_Last(boolean obsoleto,
        String idFornitore, String nome, String codice,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByFornitoreNomeCodiceLike(obsoleto, idFornitore, nome,
                codice);

        if (count == 0) {
            return null;
        }

        List<Piante> list = findByFornitoreNomeCodiceLike(obsoleto,
                idFornitore, nome, codice, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param id the primary key of the current piante
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante[] findByFornitoreNomeCodiceLike_PrevAndNext(long id,
        boolean obsoleto, String idFornitore, String nome, String codice,
        OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Piante[] array = new PianteImpl[3];

            array[0] = getByFornitoreNomeCodiceLike_PrevAndNext(session,
                    piante, obsoleto, idFornitore, nome, codice,
                    orderByComparator, true);

            array[1] = piante;

            array[2] = getByFornitoreNomeCodiceLike_PrevAndNext(session,
                    piante, obsoleto, idFornitore, nome, codice,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Piante getByFornitoreNomeCodiceLike_PrevAndNext(Session session,
        Piante piante, boolean obsoleto, String idFornitore, String nome,
        String codice, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PIANTE_WHERE);

        query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_OBSOLETO_2);

        boolean bindIdFornitore = false;

        if (idFornitore == null) {
            query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_IDFORNITORE_1);
        } else if (idFornitore.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_IDFORNITORE_3);
        } else {
            bindIdFornitore = true;

            query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_IDFORNITORE_2);
        }

        boolean bindNome = false;

        if (nome == null) {
            query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_NOME_1);
        } else if (nome.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_NOME_3);
        } else {
            bindNome = true;

            query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_NOME_2);
        }

        boolean bindCodice = false;

        if (codice == null) {
            query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_CODICE_1);
        } else if (codice.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_CODICE_3);
        } else {
            bindCodice = true;

            query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_CODICE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(PianteModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(obsoleto);

        if (bindIdFornitore) {
            qPos.add(idFornitore);
        }

        if (bindNome) {
            qPos.add(nome);
        }

        if (bindCodice) {
            qPos.add(codice);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(piante);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Piante> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the piantes where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
     *
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByFornitoreNomeCodiceLike(boolean obsoleto,
        String idFornitore, String nome, String codice)
        throws SystemException {
        for (Piante piante : findByFornitoreNomeCodiceLike(obsoleto,
                idFornitore, nome, codice, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(piante);
        }
    }

    /**
     * Returns the number of piantes where obsoleto = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @return the number of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByFornitoreNomeCodiceLike(boolean obsoleto,
        String idFornitore, String nome, String codice)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_FORNITORENOMECODICELIKE;

        Object[] finderArgs = new Object[] { obsoleto, idFornitore, nome, codice };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(5);

            query.append(_SQL_COUNT_PIANTE_WHERE);

            query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_OBSOLETO_2);

            boolean bindIdFornitore = false;

            if (idFornitore == null) {
                query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_IDFORNITORE_1);
            } else if (idFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_IDFORNITORE_3);
            } else {
                bindIdFornitore = true;

                query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_IDFORNITORE_2);
            }

            boolean bindNome = false;

            if (nome == null) {
                query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_NOME_1);
            } else if (nome.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_NOME_3);
            } else {
                bindNome = true;

                query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_NOME_2);
            }

            boolean bindCodice = false;

            if (codice == null) {
                query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_CODICE_1);
            } else if (codice.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_CODICE_3);
            } else {
                bindCodice = true;

                query.append(_FINDER_COLUMN_FORNITORENOMECODICELIKE_CODICE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(obsoleto);

                if (bindIdFornitore) {
                    qPos.add(idFornitore);
                }

                if (bindNome) {
                    qPos.add(nome);
                }

                if (bindCodice) {
                    qPos.add(codice);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @return the matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByCategoriaFornitore(boolean obsoleto,
        long idCategoria, String idFornitore, String nome, String codice)
        throws SystemException {
        return findByCategoriaFornitore(obsoleto, idCategoria, idFornitore,
            nome, codice, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @param start the lower bound of the range of piantes
     * @param end the upper bound of the range of piantes (not inclusive)
     * @return the range of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByCategoriaFornitore(boolean obsoleto,
        long idCategoria, String idFornitore, String nome, String codice,
        int start, int end) throws SystemException {
        return findByCategoriaFornitore(obsoleto, idCategoria, idFornitore,
            nome, codice, start, end, null);
    }

    /**
     * Returns an ordered range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @param start the lower bound of the range of piantes
     * @param end the upper bound of the range of piantes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByCategoriaFornitore(boolean obsoleto,
        long idCategoria, String idFornitore, String nome, String codice,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CATEGORIAFORNITORE;
        finderArgs = new Object[] {
                obsoleto, idCategoria, idFornitore, nome, codice,
                
                start, end, orderByComparator
            };

        List<Piante> list = (List<Piante>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Piante piante : list) {
                if ((obsoleto != piante.getObsoleto()) ||
                        (idCategoria != piante.getIdCategoria()) ||
                        !Validator.equals(idFornitore, piante.getIdFornitore()) ||
                        !StringUtil.wildcardMatches(piante.getNome(), nome,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, true) ||
                        !StringUtil.wildcardMatches(piante.getCodice(), codice,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, true)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(7 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(7);
            }

            query.append(_SQL_SELECT_PIANTE_WHERE);

            query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_OBSOLETO_2);

            query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_IDCATEGORIA_2);

            boolean bindIdFornitore = false;

            if (idFornitore == null) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_IDFORNITORE_1);
            } else if (idFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_IDFORNITORE_3);
            } else {
                bindIdFornitore = true;

                query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_IDFORNITORE_2);
            }

            boolean bindNome = false;

            if (nome == null) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_NOME_1);
            } else if (nome.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_NOME_3);
            } else {
                bindNome = true;

                query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_NOME_2);
            }

            boolean bindCodice = false;

            if (codice == null) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_CODICE_1);
            } else if (codice.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_CODICE_3);
            } else {
                bindCodice = true;

                query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_CODICE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PianteModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(obsoleto);

                qPos.add(idCategoria);

                if (bindIdFornitore) {
                    qPos.add(idFornitore);
                }

                if (bindNome) {
                    qPos.add(nome);
                }

                if (bindCodice) {
                    qPos.add(codice);
                }

                if (!pagination) {
                    list = (List<Piante>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Piante>(list);
                } else {
                    list = (List<Piante>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByCategoriaFornitore_First(boolean obsoleto,
        long idCategoria, String idFornitore, String nome, String codice,
        OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = fetchByCategoriaFornitore_First(obsoleto, idCategoria,
                idFornitore, nome, codice, orderByComparator);

        if (piante != null) {
            return piante;
        }

        StringBundler msg = new StringBundler(12);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("obsoleto=");
        msg.append(obsoleto);

        msg.append(", idCategoria=");
        msg.append(idCategoria);

        msg.append(", idFornitore=");
        msg.append(idFornitore);

        msg.append(", nome=");
        msg.append(nome);

        msg.append(", codice=");
        msg.append(codice);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPianteException(msg.toString());
    }

    /**
     * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching piante, or <code>null</code> if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByCategoriaFornitore_First(boolean obsoleto,
        long idCategoria, String idFornitore, String nome, String codice,
        OrderByComparator orderByComparator) throws SystemException {
        List<Piante> list = findByCategoriaFornitore(obsoleto, idCategoria,
                idFornitore, nome, codice, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByCategoriaFornitore_Last(boolean obsoleto,
        long idCategoria, String idFornitore, String nome, String codice,
        OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = fetchByCategoriaFornitore_Last(obsoleto, idCategoria,
                idFornitore, nome, codice, orderByComparator);

        if (piante != null) {
            return piante;
        }

        StringBundler msg = new StringBundler(12);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("obsoleto=");
        msg.append(obsoleto);

        msg.append(", idCategoria=");
        msg.append(idCategoria);

        msg.append(", idFornitore=");
        msg.append(idFornitore);

        msg.append(", nome=");
        msg.append(nome);

        msg.append(", codice=");
        msg.append(codice);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPianteException(msg.toString());
    }

    /**
     * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching piante, or <code>null</code> if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByCategoriaFornitore_Last(boolean obsoleto,
        long idCategoria, String idFornitore, String nome, String codice,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByCategoriaFornitore(obsoleto, idCategoria,
                idFornitore, nome, codice);

        if (count == 0) {
            return null;
        }

        List<Piante> list = findByCategoriaFornitore(obsoleto, idCategoria,
                idFornitore, nome, codice, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param id the primary key of the current piante
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante[] findByCategoriaFornitore_PrevAndNext(long id,
        boolean obsoleto, long idCategoria, String idFornitore, String nome,
        String codice, OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Piante[] array = new PianteImpl[3];

            array[0] = getByCategoriaFornitore_PrevAndNext(session, piante,
                    obsoleto, idCategoria, idFornitore, nome, codice,
                    orderByComparator, true);

            array[1] = piante;

            array[2] = getByCategoriaFornitore_PrevAndNext(session, piante,
                    obsoleto, idCategoria, idFornitore, nome, codice,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Piante getByCategoriaFornitore_PrevAndNext(Session session,
        Piante piante, boolean obsoleto, long idCategoria, String idFornitore,
        String nome, String codice, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PIANTE_WHERE);

        query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_OBSOLETO_2);

        query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_IDCATEGORIA_2);

        boolean bindIdFornitore = false;

        if (idFornitore == null) {
            query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_IDFORNITORE_1);
        } else if (idFornitore.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_IDFORNITORE_3);
        } else {
            bindIdFornitore = true;

            query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_IDFORNITORE_2);
        }

        boolean bindNome = false;

        if (nome == null) {
            query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_NOME_1);
        } else if (nome.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_NOME_3);
        } else {
            bindNome = true;

            query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_NOME_2);
        }

        boolean bindCodice = false;

        if (codice == null) {
            query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_CODICE_1);
        } else if (codice.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_CODICE_3);
        } else {
            bindCodice = true;

            query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_CODICE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(PianteModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(obsoleto);

        qPos.add(idCategoria);

        if (bindIdFornitore) {
            qPos.add(idFornitore);
        }

        if (bindNome) {
            qPos.add(nome);
        }

        if (bindCodice) {
            qPos.add(codice);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(piante);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Piante> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByCategoriaFornitore(boolean obsoleto, long idCategoria,
        String idFornitore, String nome, String codice)
        throws SystemException {
        for (Piante piante : findByCategoriaFornitore(obsoleto, idCategoria,
                idFornitore, nome, codice, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(piante);
        }
    }

    /**
     * Returns the number of piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param nome the nome
     * @param codice the codice
     * @return the number of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByCategoriaFornitore(boolean obsoleto, long idCategoria,
        String idFornitore, String nome, String codice)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_CATEGORIAFORNITORE;

        Object[] finderArgs = new Object[] {
                obsoleto, idCategoria, idFornitore, nome, codice
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(6);

            query.append(_SQL_COUNT_PIANTE_WHERE);

            query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_OBSOLETO_2);

            query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_IDCATEGORIA_2);

            boolean bindIdFornitore = false;

            if (idFornitore == null) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_IDFORNITORE_1);
            } else if (idFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_IDFORNITORE_3);
            } else {
                bindIdFornitore = true;

                query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_IDFORNITORE_2);
            }

            boolean bindNome = false;

            if (nome == null) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_NOME_1);
            } else if (nome.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_NOME_3);
            } else {
                bindNome = true;

                query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_NOME_2);
            }

            boolean bindCodice = false;

            if (codice == null) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_CODICE_1);
            } else if (codice.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_CODICE_3);
            } else {
                bindCodice = true;

                query.append(_FINDER_COLUMN_CATEGORIAFORNITORE_CODICE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(obsoleto);

                qPos.add(idCategoria);

                if (bindIdFornitore) {
                    qPos.add(idFornitore);
                }

                if (bindNome) {
                    qPos.add(nome);
                }

                if (bindCodice) {
                    qPos.add(codice);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the piantes where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @return the matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByFornitoreAttivaNomeCodiceLike(boolean obsoleto,
        String idFornitore, boolean attiva, String nome, String codice)
        throws SystemException {
        return findByFornitoreAttivaNomeCodiceLike(obsoleto, idFornitore,
            attiva, nome, codice, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the piantes where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param start the lower bound of the range of piantes
     * @param end the upper bound of the range of piantes (not inclusive)
     * @return the range of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByFornitoreAttivaNomeCodiceLike(boolean obsoleto,
        String idFornitore, boolean attiva, String nome, String codice,
        int start, int end) throws SystemException {
        return findByFornitoreAttivaNomeCodiceLike(obsoleto, idFornitore,
            attiva, nome, codice, start, end, null);
    }

    /**
     * Returns an ordered range of all the piantes where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param start the lower bound of the range of piantes
     * @param end the upper bound of the range of piantes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByFornitoreAttivaNomeCodiceLike(boolean obsoleto,
        String idFornitore, boolean attiva, String nome, String codice,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FORNITOREATTIVANOMECODICELIKE;
        finderArgs = new Object[] {
                obsoleto, idFornitore, attiva, nome, codice,
                
                start, end, orderByComparator
            };

        List<Piante> list = (List<Piante>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Piante piante : list) {
                if ((obsoleto != piante.getObsoleto()) ||
                        !Validator.equals(idFornitore, piante.getIdFornitore()) ||
                        (attiva != piante.getAttiva()) ||
                        !StringUtil.wildcardMatches(piante.getNome(), nome,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, true) ||
                        !StringUtil.wildcardMatches(piante.getCodice(), codice,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, true)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(7 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(7);
            }

            query.append(_SQL_SELECT_PIANTE_WHERE);

            query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_OBSOLETO_2);

            boolean bindIdFornitore = false;

            if (idFornitore == null) {
                query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_IDFORNITORE_1);
            } else if (idFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_IDFORNITORE_3);
            } else {
                bindIdFornitore = true;

                query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_IDFORNITORE_2);
            }

            query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_ATTIVA_2);

            boolean bindNome = false;

            if (nome == null) {
                query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_NOME_1);
            } else if (nome.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_NOME_3);
            } else {
                bindNome = true;

                query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_NOME_2);
            }

            boolean bindCodice = false;

            if (codice == null) {
                query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_CODICE_1);
            } else if (codice.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_CODICE_3);
            } else {
                bindCodice = true;

                query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_CODICE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PianteModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(obsoleto);

                if (bindIdFornitore) {
                    qPos.add(idFornitore);
                }

                qPos.add(attiva);

                if (bindNome) {
                    qPos.add(nome);
                }

                if (bindCodice) {
                    qPos.add(codice);
                }

                if (!pagination) {
                    list = (List<Piante>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Piante>(list);
                } else {
                    list = (List<Piante>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByFornitoreAttivaNomeCodiceLike_First(boolean obsoleto,
        String idFornitore, boolean attiva, String nome, String codice,
        OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = fetchByFornitoreAttivaNomeCodiceLike_First(obsoleto,
                idFornitore, attiva, nome, codice, orderByComparator);

        if (piante != null) {
            return piante;
        }

        StringBundler msg = new StringBundler(12);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("obsoleto=");
        msg.append(obsoleto);

        msg.append(", idFornitore=");
        msg.append(idFornitore);

        msg.append(", attiva=");
        msg.append(attiva);

        msg.append(", nome=");
        msg.append(nome);

        msg.append(", codice=");
        msg.append(codice);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPianteException(msg.toString());
    }

    /**
     * Returns the first piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching piante, or <code>null</code> if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByFornitoreAttivaNomeCodiceLike_First(boolean obsoleto,
        String idFornitore, boolean attiva, String nome, String codice,
        OrderByComparator orderByComparator) throws SystemException {
        List<Piante> list = findByFornitoreAttivaNomeCodiceLike(obsoleto,
                idFornitore, attiva, nome, codice, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByFornitoreAttivaNomeCodiceLike_Last(boolean obsoleto,
        String idFornitore, boolean attiva, String nome, String codice,
        OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = fetchByFornitoreAttivaNomeCodiceLike_Last(obsoleto,
                idFornitore, attiva, nome, codice, orderByComparator);

        if (piante != null) {
            return piante;
        }

        StringBundler msg = new StringBundler(12);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("obsoleto=");
        msg.append(obsoleto);

        msg.append(", idFornitore=");
        msg.append(idFornitore);

        msg.append(", attiva=");
        msg.append(attiva);

        msg.append(", nome=");
        msg.append(nome);

        msg.append(", codice=");
        msg.append(codice);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPianteException(msg.toString());
    }

    /**
     * Returns the last piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching piante, or <code>null</code> if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByFornitoreAttivaNomeCodiceLike_Last(boolean obsoleto,
        String idFornitore, boolean attiva, String nome, String codice,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByFornitoreAttivaNomeCodiceLike(obsoleto, idFornitore,
                attiva, nome, codice);

        if (count == 0) {
            return null;
        }

        List<Piante> list = findByFornitoreAttivaNomeCodiceLike(obsoleto,
                idFornitore, attiva, nome, codice, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param id the primary key of the current piante
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante[] findByFornitoreAttivaNomeCodiceLike_PrevAndNext(long id,
        boolean obsoleto, String idFornitore, boolean attiva, String nome,
        String codice, OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Piante[] array = new PianteImpl[3];

            array[0] = getByFornitoreAttivaNomeCodiceLike_PrevAndNext(session,
                    piante, obsoleto, idFornitore, attiva, nome, codice,
                    orderByComparator, true);

            array[1] = piante;

            array[2] = getByFornitoreAttivaNomeCodiceLike_PrevAndNext(session,
                    piante, obsoleto, idFornitore, attiva, nome, codice,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Piante getByFornitoreAttivaNomeCodiceLike_PrevAndNext(
        Session session, Piante piante, boolean obsoleto, String idFornitore,
        boolean attiva, String nome, String codice,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PIANTE_WHERE);

        query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_OBSOLETO_2);

        boolean bindIdFornitore = false;

        if (idFornitore == null) {
            query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_IDFORNITORE_1);
        } else if (idFornitore.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_IDFORNITORE_3);
        } else {
            bindIdFornitore = true;

            query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_IDFORNITORE_2);
        }

        query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_ATTIVA_2);

        boolean bindNome = false;

        if (nome == null) {
            query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_NOME_1);
        } else if (nome.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_NOME_3);
        } else {
            bindNome = true;

            query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_NOME_2);
        }

        boolean bindCodice = false;

        if (codice == null) {
            query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_CODICE_1);
        } else if (codice.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_CODICE_3);
        } else {
            bindCodice = true;

            query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_CODICE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(PianteModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(obsoleto);

        if (bindIdFornitore) {
            qPos.add(idFornitore);
        }

        qPos.add(attiva);

        if (bindNome) {
            qPos.add(nome);
        }

        if (bindCodice) {
            qPos.add(codice);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(piante);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Piante> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the piantes where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
     *
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByFornitoreAttivaNomeCodiceLike(boolean obsoleto,
        String idFornitore, boolean attiva, String nome, String codice)
        throws SystemException {
        for (Piante piante : findByFornitoreAttivaNomeCodiceLike(obsoleto,
                idFornitore, attiva, nome, codice, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(piante);
        }
    }

    /**
     * Returns the number of piantes where obsoleto = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @return the number of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByFornitoreAttivaNomeCodiceLike(boolean obsoleto,
        String idFornitore, boolean attiva, String nome, String codice)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_FORNITOREATTIVANOMECODICELIKE;

        Object[] finderArgs = new Object[] {
                obsoleto, idFornitore, attiva, nome, codice
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(6);

            query.append(_SQL_COUNT_PIANTE_WHERE);

            query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_OBSOLETO_2);

            boolean bindIdFornitore = false;

            if (idFornitore == null) {
                query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_IDFORNITORE_1);
            } else if (idFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_IDFORNITORE_3);
            } else {
                bindIdFornitore = true;

                query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_IDFORNITORE_2);
            }

            query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_ATTIVA_2);

            boolean bindNome = false;

            if (nome == null) {
                query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_NOME_1);
            } else if (nome.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_NOME_3);
            } else {
                bindNome = true;

                query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_NOME_2);
            }

            boolean bindCodice = false;

            if (codice == null) {
                query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_CODICE_1);
            } else if (codice.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_CODICE_3);
            } else {
                bindCodice = true;

                query.append(_FINDER_COLUMN_FORNITOREATTIVANOMECODICELIKE_CODICE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(obsoleto);

                if (bindIdFornitore) {
                    qPos.add(idFornitore);
                }

                qPos.add(attiva);

                if (bindNome) {
                    qPos.add(nome);
                }

                if (bindCodice) {
                    qPos.add(codice);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @return the matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByCategoriaFornitoreAttiva(boolean obsoleto,
        long idCategoria, String idFornitore, boolean attiva, String nome,
        String codice) throws SystemException {
        return findByCategoriaFornitoreAttiva(obsoleto, idCategoria,
            idFornitore, attiva, nome, codice, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param start the lower bound of the range of piantes
     * @param end the upper bound of the range of piantes (not inclusive)
     * @return the range of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByCategoriaFornitoreAttiva(boolean obsoleto,
        long idCategoria, String idFornitore, boolean attiva, String nome,
        String codice, int start, int end) throws SystemException {
        return findByCategoriaFornitoreAttiva(obsoleto, idCategoria,
            idFornitore, attiva, nome, codice, start, end, null);
    }

    /**
     * Returns an ordered range of all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param start the lower bound of the range of piantes
     * @param end the upper bound of the range of piantes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findByCategoriaFornitoreAttiva(boolean obsoleto,
        long idCategoria, String idFornitore, boolean attiva, String nome,
        String codice, int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CATEGORIAFORNITOREATTIVA;
        finderArgs = new Object[] {
                obsoleto, idCategoria, idFornitore, attiva, nome, codice,
                
                start, end, orderByComparator
            };

        List<Piante> list = (List<Piante>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Piante piante : list) {
                if ((obsoleto != piante.getObsoleto()) ||
                        (idCategoria != piante.getIdCategoria()) ||
                        !Validator.equals(idFornitore, piante.getIdFornitore()) ||
                        (attiva != piante.getAttiva()) ||
                        !StringUtil.wildcardMatches(piante.getNome(), nome,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, true) ||
                        !StringUtil.wildcardMatches(piante.getCodice(), codice,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, true)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(8 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(8);
            }

            query.append(_SQL_SELECT_PIANTE_WHERE);

            query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_OBSOLETO_2);

            query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_IDCATEGORIA_2);

            boolean bindIdFornitore = false;

            if (idFornitore == null) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_IDFORNITORE_1);
            } else if (idFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_IDFORNITORE_3);
            } else {
                bindIdFornitore = true;

                query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_IDFORNITORE_2);
            }

            query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_ATTIVA_2);

            boolean bindNome = false;

            if (nome == null) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_NOME_1);
            } else if (nome.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_NOME_3);
            } else {
                bindNome = true;

                query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_NOME_2);
            }

            boolean bindCodice = false;

            if (codice == null) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_CODICE_1);
            } else if (codice.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_CODICE_3);
            } else {
                bindCodice = true;

                query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_CODICE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PianteModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(obsoleto);

                qPos.add(idCategoria);

                if (bindIdFornitore) {
                    qPos.add(idFornitore);
                }

                qPos.add(attiva);

                if (bindNome) {
                    qPos.add(nome);
                }

                if (bindCodice) {
                    qPos.add(codice);
                }

                if (!pagination) {
                    list = (List<Piante>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Piante>(list);
                } else {
                    list = (List<Piante>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByCategoriaFornitoreAttiva_First(boolean obsoleto,
        long idCategoria, String idFornitore, boolean attiva, String nome,
        String codice, OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = fetchByCategoriaFornitoreAttiva_First(obsoleto,
                idCategoria, idFornitore, attiva, nome, codice,
                orderByComparator);

        if (piante != null) {
            return piante;
        }

        StringBundler msg = new StringBundler(14);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("obsoleto=");
        msg.append(obsoleto);

        msg.append(", idCategoria=");
        msg.append(idCategoria);

        msg.append(", idFornitore=");
        msg.append(idFornitore);

        msg.append(", attiva=");
        msg.append(attiva);

        msg.append(", nome=");
        msg.append(nome);

        msg.append(", codice=");
        msg.append(codice);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPianteException(msg.toString());
    }

    /**
     * Returns the first piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching piante, or <code>null</code> if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByCategoriaFornitoreAttiva_First(boolean obsoleto,
        long idCategoria, String idFornitore, boolean attiva, String nome,
        String codice, OrderByComparator orderByComparator)
        throws SystemException {
        List<Piante> list = findByCategoriaFornitoreAttiva(obsoleto,
                idCategoria, idFornitore, attiva, nome, codice, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByCategoriaFornitoreAttiva_Last(boolean obsoleto,
        long idCategoria, String idFornitore, boolean attiva, String nome,
        String codice, OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = fetchByCategoriaFornitoreAttiva_Last(obsoleto,
                idCategoria, idFornitore, attiva, nome, codice,
                orderByComparator);

        if (piante != null) {
            return piante;
        }

        StringBundler msg = new StringBundler(14);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("obsoleto=");
        msg.append(obsoleto);

        msg.append(", idCategoria=");
        msg.append(idCategoria);

        msg.append(", idFornitore=");
        msg.append(idFornitore);

        msg.append(", attiva=");
        msg.append(attiva);

        msg.append(", nome=");
        msg.append(nome);

        msg.append(", codice=");
        msg.append(codice);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPianteException(msg.toString());
    }

    /**
     * Returns the last piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching piante, or <code>null</code> if a matching piante could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByCategoriaFornitoreAttiva_Last(boolean obsoleto,
        long idCategoria, String idFornitore, boolean attiva, String nome,
        String codice, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByCategoriaFornitoreAttiva(obsoleto, idCategoria,
                idFornitore, attiva, nome, codice);

        if (count == 0) {
            return null;
        }

        List<Piante> list = findByCategoriaFornitoreAttiva(obsoleto,
                idCategoria, idFornitore, attiva, nome, codice, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the piantes before and after the current piante in the ordered set where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param id the primary key of the current piante
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante[] findByCategoriaFornitoreAttiva_PrevAndNext(long id,
        boolean obsoleto, long idCategoria, String idFornitore, boolean attiva,
        String nome, String codice, OrderByComparator orderByComparator)
        throws NoSuchPianteException, SystemException {
        Piante piante = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Piante[] array = new PianteImpl[3];

            array[0] = getByCategoriaFornitoreAttiva_PrevAndNext(session,
                    piante, obsoleto, idCategoria, idFornitore, attiva, nome,
                    codice, orderByComparator, true);

            array[1] = piante;

            array[2] = getByCategoriaFornitoreAttiva_PrevAndNext(session,
                    piante, obsoleto, idCategoria, idFornitore, attiva, nome,
                    codice, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Piante getByCategoriaFornitoreAttiva_PrevAndNext(
        Session session, Piante piante, boolean obsoleto, long idCategoria,
        String idFornitore, boolean attiva, String nome, String codice,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PIANTE_WHERE);

        query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_OBSOLETO_2);

        query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_IDCATEGORIA_2);

        boolean bindIdFornitore = false;

        if (idFornitore == null) {
            query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_IDFORNITORE_1);
        } else if (idFornitore.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_IDFORNITORE_3);
        } else {
            bindIdFornitore = true;

            query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_IDFORNITORE_2);
        }

        query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_ATTIVA_2);

        boolean bindNome = false;

        if (nome == null) {
            query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_NOME_1);
        } else if (nome.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_NOME_3);
        } else {
            bindNome = true;

            query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_NOME_2);
        }

        boolean bindCodice = false;

        if (codice == null) {
            query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_CODICE_1);
        } else if (codice.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_CODICE_3);
        } else {
            bindCodice = true;

            query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_CODICE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(PianteModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(obsoleto);

        qPos.add(idCategoria);

        if (bindIdFornitore) {
            qPos.add(idFornitore);
        }

        qPos.add(attiva);

        if (bindNome) {
            qPos.add(nome);
        }

        if (bindCodice) {
            qPos.add(codice);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(piante);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Piante> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63; from the database.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByCategoriaFornitoreAttiva(boolean obsoleto,
        long idCategoria, String idFornitore, boolean attiva, String nome,
        String codice) throws SystemException {
        for (Piante piante : findByCategoriaFornitoreAttiva(obsoleto,
                idCategoria, idFornitore, attiva, nome, codice,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(piante);
        }
    }

    /**
     * Returns the number of piantes where obsoleto = &#63; and idCategoria = &#63; and idFornitore = &#63; and attiva = &#63; and nome LIKE &#63; and codice LIKE &#63;.
     *
     * @param obsoleto the obsoleto
     * @param idCategoria the id categoria
     * @param idFornitore the id fornitore
     * @param attiva the attiva
     * @param nome the nome
     * @param codice the codice
     * @return the number of matching piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByCategoriaFornitoreAttiva(boolean obsoleto,
        long idCategoria, String idFornitore, boolean attiva, String nome,
        String codice) throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_CATEGORIAFORNITOREATTIVA;

        Object[] finderArgs = new Object[] {
                obsoleto, idCategoria, idFornitore, attiva, nome, codice
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(7);

            query.append(_SQL_COUNT_PIANTE_WHERE);

            query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_OBSOLETO_2);

            query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_IDCATEGORIA_2);

            boolean bindIdFornitore = false;

            if (idFornitore == null) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_IDFORNITORE_1);
            } else if (idFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_IDFORNITORE_3);
            } else {
                bindIdFornitore = true;

                query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_IDFORNITORE_2);
            }

            query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_ATTIVA_2);

            boolean bindNome = false;

            if (nome == null) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_NOME_1);
            } else if (nome.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_NOME_3);
            } else {
                bindNome = true;

                query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_NOME_2);
            }

            boolean bindCodice = false;

            if (codice == null) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_CODICE_1);
            } else if (codice.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_CODICE_3);
            } else {
                bindCodice = true;

                query.append(_FINDER_COLUMN_CATEGORIAFORNITOREATTIVA_CODICE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(obsoleto);

                qPos.add(idCategoria);

                if (bindIdFornitore) {
                    qPos.add(idFornitore);
                }

                qPos.add(attiva);

                if (bindNome) {
                    qPos.add(nome);
                }

                if (bindCodice) {
                    qPos.add(codice);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the piante in the entity cache if it is enabled.
     *
     * @param piante the piante
     */
    @Override
    public void cacheResult(Piante piante) {
        EntityCacheUtil.putResult(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteImpl.class, piante.getPrimaryKey(), piante);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODICE,
            new Object[] { piante.getCodice() }, piante);

        piante.resetOriginalValues();
    }

    /**
     * Caches the piantes in the entity cache if it is enabled.
     *
     * @param piantes the piantes
     */
    @Override
    public void cacheResult(List<Piante> piantes) {
        for (Piante piante : piantes) {
            if (EntityCacheUtil.getResult(
                        PianteModelImpl.ENTITY_CACHE_ENABLED, PianteImpl.class,
                        piante.getPrimaryKey()) == null) {
                cacheResult(piante);
            } else {
                piante.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all piantes.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(PianteImpl.class.getName());
        }

        EntityCacheUtil.clearCache(PianteImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the piante.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Piante piante) {
        EntityCacheUtil.removeResult(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteImpl.class, piante.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        clearUniqueFindersCache(piante);
    }

    @Override
    public void clearCache(List<Piante> piantes) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Piante piante : piantes) {
            EntityCacheUtil.removeResult(PianteModelImpl.ENTITY_CACHE_ENABLED,
                PianteImpl.class, piante.getPrimaryKey());

            clearUniqueFindersCache(piante);
        }
    }

    protected void cacheUniqueFindersCache(Piante piante) {
        if (piante.isNew()) {
            Object[] args = new Object[] { piante.getCodice() };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CODICE, args,
                Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODICE, args, piante);
        } else {
            PianteModelImpl pianteModelImpl = (PianteModelImpl) piante;

            if ((pianteModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_CODICE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { piante.getCodice() };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CODICE, args,
                    Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODICE, args,
                    piante);
            }
        }
    }

    protected void clearUniqueFindersCache(Piante piante) {
        PianteModelImpl pianteModelImpl = (PianteModelImpl) piante;

        Object[] args = new Object[] { piante.getCodice() };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICE, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CODICE, args);

        if ((pianteModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_CODICE.getColumnBitmask()) != 0) {
            args = new Object[] { pianteModelImpl.getOriginalCodice() };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICE, args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CODICE, args);
        }
    }

    /**
     * Creates a new piante with the primary key. Does not add the piante to the database.
     *
     * @param id the primary key for the new piante
     * @return the new piante
     */
    @Override
    public Piante create(long id) {
        Piante piante = new PianteImpl();

        piante.setNew(true);
        piante.setPrimaryKey(id);

        return piante;
    }

    /**
     * Removes the piante with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the piante
     * @return the piante that was removed
     * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante remove(long id) throws NoSuchPianteException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the piante with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the piante
     * @return the piante that was removed
     * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante remove(Serializable primaryKey)
        throws NoSuchPianteException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Piante piante = (Piante) session.get(PianteImpl.class, primaryKey);

            if (piante == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchPianteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(piante);
        } catch (NoSuchPianteException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Piante removeImpl(Piante piante) throws SystemException {
        piante = toUnwrappedModel(piante);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(piante)) {
                piante = (Piante) session.get(PianteImpl.class,
                        piante.getPrimaryKeyObj());
            }

            if (piante != null) {
                session.delete(piante);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (piante != null) {
            clearCache(piante);
        }

        return piante;
    }

    @Override
    public Piante updateImpl(it.bysoftware.ct.model.Piante piante)
        throws SystemException {
        piante = toUnwrappedModel(piante);

        boolean isNew = piante.isNew();

        Session session = null;

        try {
            session = openSession();

            if (piante.isNew()) {
                session.save(piante);

                piante.setNew(false);
            } else {
                session.merge(piante);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !PianteModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(PianteModelImpl.ENTITY_CACHE_ENABLED,
            PianteImpl.class, piante.getPrimaryKey(), piante);

        clearUniqueFindersCache(piante);
        cacheUniqueFindersCache(piante);

        return piante;
    }

    protected Piante toUnwrappedModel(Piante piante) {
        if (piante instanceof PianteImpl) {
            return piante;
        }

        PianteImpl pianteImpl = new PianteImpl();

        pianteImpl.setNew(piante.isNew());
        pianteImpl.setPrimaryKey(piante.getPrimaryKey());

        pianteImpl.setId(piante.getId());
        pianteImpl.setNome(piante.getNome());
        pianteImpl.setVaso(piante.getVaso());
        pianteImpl.setPrezzo(piante.getPrezzo());
        pianteImpl.setAltezza(piante.getAltezza());
        pianteImpl.setPiantePianale(piante.getPiantePianale());
        pianteImpl.setPianteCarrello(piante.getPianteCarrello());
        pianteImpl.setAttiva(piante.isAttiva());
        pianteImpl.setFoto1(piante.getFoto1());
        pianteImpl.setFoto2(piante.getFoto2());
        pianteImpl.setFoto3(piante.getFoto3());
        pianteImpl.setAltezzaPianta(piante.getAltezzaPianta());
        pianteImpl.setIdCategoria(piante.getIdCategoria());
        pianteImpl.setIdFornitore(piante.getIdFornitore());
        pianteImpl.setPrezzoFornitore(piante.getPrezzoFornitore());
        pianteImpl.setDisponibilita(piante.getDisponibilita());
        pianteImpl.setCodice(piante.getCodice());
        pianteImpl.setForma(piante.getForma());
        pianteImpl.setAltezzaChioma(piante.getAltezzaChioma());
        pianteImpl.setTempoConsegna(piante.getTempoConsegna());
        pianteImpl.setPrezzoB(piante.getPrezzoB());
        pianteImpl.setPercScontoMistoAziendale(piante.getPercScontoMistoAziendale());
        pianteImpl.setPrezzoACarrello(piante.getPrezzoACarrello());
        pianteImpl.setPrezzoBCarrello(piante.getPrezzoBCarrello());
        pianteImpl.setIdForma(piante.getIdForma());
        pianteImpl.setCostoRipiano(piante.getCostoRipiano());
        pianteImpl.setUltimoAggiornamentoFoto(piante.getUltimoAggiornamentoFoto());
        pianteImpl.setScadenzaFoto(piante.getScadenzaFoto());
        pianteImpl.setGiorniScadenzaFoto(piante.getGiorniScadenzaFoto());
        pianteImpl.setSormonto2Fila(piante.getSormonto2Fila());
        pianteImpl.setSormonto3Fila(piante.getSormonto3Fila());
        pianteImpl.setSormonto4Fila(piante.getSormonto4Fila());
        pianteImpl.setAggiuntaRipiano(piante.getAggiuntaRipiano());
        pianteImpl.setEan(piante.getEan());
        pianteImpl.setPassaporto(piante.isPassaporto());
        pianteImpl.setBloccoDisponibilita(piante.isBloccoDisponibilita());
        pianteImpl.setPathFile(piante.getPathFile());
        pianteImpl.setObsoleto(piante.isObsoleto());
        pianteImpl.setPrezzoEtichetta(piante.getPrezzoEtichetta());
        pianteImpl.setPassaportoDefault(piante.getPassaportoDefault());

        return pianteImpl;
    }

    /**
     * Returns the piante with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the piante
     * @return the piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByPrimaryKey(Serializable primaryKey)
        throws NoSuchPianteException, SystemException {
        Piante piante = fetchByPrimaryKey(primaryKey);

        if (piante == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchPianteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return piante;
    }

    /**
     * Returns the piante with the primary key or throws a {@link it.bysoftware.ct.NoSuchPianteException} if it could not be found.
     *
     * @param id the primary key of the piante
     * @return the piante
     * @throws it.bysoftware.ct.NoSuchPianteException if a piante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante findByPrimaryKey(long id)
        throws NoSuchPianteException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the piante with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the piante
     * @return the piante, or <code>null</code> if a piante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Piante piante = (Piante) EntityCacheUtil.getResult(PianteModelImpl.ENTITY_CACHE_ENABLED,
                PianteImpl.class, primaryKey);

        if (piante == _nullPiante) {
            return null;
        }

        if (piante == null) {
            Session session = null;

            try {
                session = openSession();

                piante = (Piante) session.get(PianteImpl.class, primaryKey);

                if (piante != null) {
                    cacheResult(piante);
                } else {
                    EntityCacheUtil.putResult(PianteModelImpl.ENTITY_CACHE_ENABLED,
                        PianteImpl.class, primaryKey, _nullPiante);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(PianteModelImpl.ENTITY_CACHE_ENABLED,
                    PianteImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return piante;
    }

    /**
     * Returns the piante with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the piante
     * @return the piante, or <code>null</code> if a piante with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Piante fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the piantes.
     *
     * @return the piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the piantes.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of piantes
     * @param end the upper bound of the range of piantes (not inclusive)
     * @return the range of piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the piantes.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of piantes
     * @param end the upper bound of the range of piantes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Piante> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Piante> list = (List<Piante>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_PIANTE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_PIANTE;

                if (pagination) {
                    sql = sql.concat(PianteModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Piante>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Piante>(list);
                } else {
                    list = (List<Piante>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the piantes from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Piante piante : findAll()) {
            remove(piante);
        }
    }

    /**
     * Returns the number of piantes.
     *
     * @return the number of piantes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_PIANTE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the piante persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.Piante")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Piante>> listenersList = new ArrayList<ModelListener<Piante>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Piante>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(PianteImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
