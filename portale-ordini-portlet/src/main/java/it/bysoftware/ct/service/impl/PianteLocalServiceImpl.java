package it.bysoftware.ct.service.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringPool;

import it.bysoftware.ct.NoSuchPianteException;
import it.bysoftware.ct.model.Piante;
import it.bysoftware.ct.service.PianteLocalServiceUtil;
import it.bysoftware.ct.service.base.PianteLocalServiceBaseImpl;

/**
 * The implementation of the piante local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.PianteLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.PianteLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.PianteLocalServiceUtil
 */
public class PianteLocalServiceImpl extends PianteLocalServiceBaseImpl {

    /**
     * Five constant.
     */
    private static final int FIVE = 5;
    
    /**
     * Four constant.
     */
    private static final int FOUR = 4;

    /**
     * Seven constant.
     */
    private static final int SEVEN = 7;
    
    /**
     * Six constant.
     */
    private static final int SIX = 6;
    
    /**
     * Three constant.
     */
    private static final int THREE = 3;


    
    /**
     * Query to retrieve the order of a specified partner having the specified
     * state and passport number.
     */
    private static final String QUERY1 = "SELECT p.cod, p.ean, p.nome,"
            + " f.RdtDescri, p.vaso, p.altezza_pianta, p.piante_pianale,"
            + " p.piante_carrello, a.RanPrezz1, a.RanPrezz3, p.disponibilita,"
            + " p.tempo_consegna, p.foto1, p.foto3, p.obsoleto FROM"
            + " _piante AS p LEFT OUTER JOIN Articoli AS a"
            + " ON p.cod = a.RanCodart LEFT OUTER JOIN"
            + " CategorieMerceologiche AS f ON a.RanCatmer = f.RdtCodtot LEFT"
            + " OUTER JOIN _categorie AS c ON p.id_categoria = c.id WHERE"
            + " p.cod LIKE ? AND p.nome LIKE ? {0} {1} {2} {3}"
            + " {4} {5} {6} {7} ORDER BY p.nome, p.forma, p.vaso, p.altezza_pianta ";
    
    /**
     * Query to retrieve the order of a specified partner having the specified
     * state and passport number.
     */
    private static final String QUERY2 = "SELECT p.id FROM _piante AS p LEFT"
            + " OUTER JOIN _articoli_socio_consorzio AS a ON"
            + " p.cod = a.codice_articolo AND p.id_fornitore = a.associato"
            + " WHERE p.id_fornitore = ? {0} ORDER BY p.id";
    

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(PianteLocalServiceImpl.class);

    @Override
    public final List<Piante> findPlants(final long idCategory,
            final boolean active, final boolean inactive,
            final String idSupplier, final String name,
            final String code, final boolean obsolete) {
        List<Piante> results = null;
        try {
            this.logger.debug(idCategory);
            this.logger.debug(idSupplier);
            this.logger.debug(active);
            this.logger.debug(inactive);
            this.logger.debug(name);
            this.logger.debug(code);
            this.logger.debug(obsolete);
			if (active) {
				if (inactive) {
					if (!idSupplier.isEmpty()) {
						results = this.piantePersistence
								.findByCategoriaFornitore(obsolete, idCategory,
										idSupplier, StringPool.PERCENT + name
												+ StringPool.PERCENT,
										StringPool.PERCENT + code
												+ StringPool.PERCENT);
					} else {
						results = this.piantePersistence.findByCategoria(
								obsolete, idCategory, StringPool.PERCENT + name
										+ StringPool.PERCENT,
								StringPool.PERCENT + code + StringPool.PERCENT);
					}
				} else {
					if (!idSupplier.isEmpty()) {
						results = this.piantePersistence
								.findByCategoriaFornitoreAttiva(obsolete,
										idCategory, idSupplier, active,
										StringPool.PERCENT + name
												+ StringPool.PERCENT,
										StringPool.PERCENT + code
												+ StringPool.PERCENT);
					} else {
						results = this.piantePersistence.findByCategoriaAttiva(
								obsolete, idCategory, active,
								StringPool.PERCENT + name + StringPool.PERCENT,
								StringPool.PERCENT + code + StringPool.PERCENT);
					}
				}
			} else {
				if (inactive) {
					if (!idSupplier.isEmpty()) {
						results = this.piantePersistence
								.findByCategoriaFornitoreAttiva(obsolete,
										idCategory, idSupplier, false,
										StringPool.PERCENT + name
												+ StringPool.PERCENT,
										StringPool.PERCENT + code
												+ StringPool.PERCENT);
					} else {
						results = this.piantePersistence.findByCategoriaAttiva(
								obsolete, idCategory, false, StringPool.PERCENT
										+ name + StringPool.PERCENT,
								StringPool.PERCENT + code + StringPool.PERCENT);
					}
				}
			}
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            // e.printStackTrace();
            results = new ArrayList<Piante>();
        }

        return results;
    }

	@Override
    public final List<Piante> findPlants(final boolean active,
    		final boolean inactive, final String idSupplier, final String name,
    		final String code, final boolean obsolete) {
		return this.findPlants(active, inactive, idSupplier, name, code, obsolete,
				null);
		
    }
    
    @SuppressWarnings("unchecked")
	@Override
    public final List<Piante> findPlants(final boolean active,
    		final boolean inactive, final String idSupplier, final String name,
    		final String code, final boolean obsolete,
    		final OrderByComparator orderByComparator) {
        List<Piante> results = new ArrayList<Piante>(); // = null;
        try {
            this.logger.debug(idSupplier);
            this.logger.debug(active);
            this.logger.debug(inactive);
            this.logger.debug(name);
            this.logger.debug(code);
            this.logger.debug(obsolete);
            if (active) {
				if (inactive) {
					if (!idSupplier.isEmpty()) {
		                results = this.piantePersistence
		                		.findByFornitoreNomeCodiceLike(obsolete,
		                				idSupplier, StringPool.PERCENT + name
		                				+ StringPool.PERCENT, StringPool.PERCENT
		                				+ code + StringPool.PERCENT);
		            } else {
		                results = this.piantePersistence.findByNomeCodiceLike(
		                		obsolete, StringPool.PERCENT + name
		                		+ StringPool.PERCENT, StringPool.PERCENT + code
		                		+ StringPool.PERCENT);
		            }
				} else {
					if (!idSupplier.isEmpty()) {
	                    results = this.piantePersistence
	                            .findByFornitoreAttivaNomeCodiceLike(obsolete,
	                                    idSupplier, active, StringPool.PERCENT
	                                    + name + StringPool.PERCENT,
	                                    StringPool.PERCENT + code
	                                    + StringPool.PERCENT);
	                } else {
	                    results = this.piantePersistence
	                            .findByAttivaNomeCodiceLike(obsolete, active,
	                                    StringPool.PERCENT + name
	                                            + StringPool.PERCENT,
	                                    StringPool.PERCENT + code
	                                            + StringPool.PERCENT);
	                }
				}
			} else {
				if (inactive) {
					if (!idSupplier.isEmpty()) {
	                    results = this.piantePersistence
	                            .findByFornitoreAttivaNomeCodiceLike(obsolete,
	                                    idSupplier, false, StringPool.PERCENT
	                                    + name + StringPool.PERCENT,
	                                    StringPool.PERCENT + code
	                                    + StringPool.PERCENT);
	                } else {
	                    results = this.piantePersistence
	                            .findByAttivaNomeCodiceLike(obsolete, false,
	                                    StringPool.PERCENT + name
	                                    + StringPool.PERCENT, StringPool.PERCENT
	                                    + code + StringPool.PERCENT);
	                }
	            }
			}
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            results = new ArrayList<Piante>();
        }
        
        List<Piante> newList = new ArrayList<Piante>(results);
        if (orderByComparator != null) {
        	Collections.sort(newList, orderByComparator);
		}
        return newList;

    }

    @Override
    public final Piante findPlants(final String code) {
        Piante p = null;
        try {
            p = this.piantePersistence.findByCodice(code);
        } catch (NoSuchPianteException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        return p;
    }

    /**
     * Returns a {@link List} of {@link Object} array representing the items
     * found.
     * 
     * @param category
     *            the select category
     * @param code
     *            item code
     * @param name
     *            item name
     * @param vase
     *            vase
     * @param shape
     *            item shape
     * @param height
     *            item height
     * @param supplier
     *            supplier
     * @param active
     *            true for only active items, false otherwise
     * @param available
     *            true for only available items, false otherwise
     * @param obsolete 
     * @return the item list
     */
    @SuppressWarnings("unchecked")
    public final List<Object[]> getItemsByFilters(final int category,
            final String code, final String name, final String vase,
            final String shape, final int height, final String supplier,
            final boolean active, final boolean inactive,
            final boolean available, final boolean obsolete) {
        Object[] filters = new String[] { "", "", "", "", "", "", "", "" };
        if (category > 0) {
            filters[0] = "AND p.id_categoria = ?";
        }
        if (!vase.isEmpty()) {
            filters[1] = "AND p.vaso = ?";
        }
        if (!shape.isEmpty()) {
            filters[2] = "AND p.forma = ?";
        }
        if (height > 0) {
            filters[PianteLocalServiceImpl.THREE] = "AND p.altezza_pianta = ?";
        }
        if (supplier != null && !supplier.isEmpty()) {
            filters[PianteLocalServiceImpl.FOUR] = " AND p.id_fornitore = ?";
        }
        if (active && inactive) {
        	filters[PianteLocalServiceImpl.FIVE] = " ";
		} else if (active && !inactive) {
            filters[PianteLocalServiceImpl.FIVE] = " AND p.attiva = 1";
        } else {
        	filters[PianteLocalServiceImpl.FIVE] = " AND p.attiva = 0";
        }
        if (available) {
            filters[PianteLocalServiceImpl.SIX] = " AND p.disponibilita > 0";
        }
        if (obsolete) {
			filters[PianteLocalServiceImpl.SEVEN] = " AND obsoleto = 1";
		} else {
			filters[PianteLocalServiceImpl.SEVEN] = " AND obsoleto = 0";
		}

        String q = MessageFormat.format(PianteLocalServiceImpl.QUERY1, filters);
        this.logger.debug(q);
        
        Session session = this.ordinePersistence.openSession();
        SQLQuery query = session.createSQLQuery(q);
        QueryPos pos = QueryPos.getInstance(query);
        pos.add(CharPool.PERCENT + code + CharPool.PERCENT);
        pos.add(CharPool.PERCENT + name + CharPool.PERCENT);
        if (category > 0) {
            pos.add(category);
        }
        if (!vase.isEmpty()) {
            pos.add(vase);
        }
        if (!shape.isEmpty()) {
            pos.add(shape);
        }
        if (height > 0) {
            pos.add(height);
        }
        if (supplier != null && !supplier.isEmpty()) {
            pos.add(supplier);
        }
        return query.list();
    }

    /**
     * Returns the list of not associated partner items.
     * 
     * @param partner
     *            the partner code
     * @return list of not associated partner items
     */
    public final List<Piante> getUnassociatedItems(final String partner) {
        return this.getPartnerItems(partner, false);
    }
    
    /**
     * Returns the list of associated partner items.
     * 
     * @param partner
     *            the partner code
     * @return list of associated partner items
     */
    public final List<Piante> getAssociatedItems(final String partner) {
        return this.getPartnerItems(partner, true);
    }
    
    /**
     * Returns the list of partner items.
     * 
     * @param partner
     *            the partner code
     * @param associated
     *            true for associated items false otherwise
     * @return list of associated partner items
     */
    private List<Piante> getPartnerItems(final String partner,
            final boolean associated) {
        List<Piante> result = new ArrayList<Piante>();
        try {
            long partnerId = Long.parseLong(partner);
            Session session = this.ordinePersistence.openSession();
            String q = QUERY2;
            if (associated) {
                q = MessageFormat.format(PianteLocalServiceImpl.QUERY2,
                        new Object[] { "" });
            } else {
                q = MessageFormat.format(PianteLocalServiceImpl.QUERY2,
                        new Object[] { "AND a.associato IS NULL" });
            }
            SQLQuery query = session.createSQLQuery(q);
            QueryPos pos = QueryPos.getInstance(query);
            pos.add(partnerId);
            @SuppressWarnings("unchecked")
            List<Integer> ids = query.list();
            
            for (Integer id : ids) {
                try {
                    result.add(PianteLocalServiceUtil.getPiante(id));
                } catch (PortalException e) {
                    this.logger.error(e.getMessage());
                    if (this.logger.isDebugEnabled()) {
                        e.printStackTrace();
                    }
                } catch (SystemException e) {
                    this.logger.error(e.getMessage());
                    if (this.logger.isDebugEnabled()) {
                        e.printStackTrace();
                    }
                }
            }
            session.close();
        } catch (NumberFormatException e) {
            this.logger.warn(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        
        return result;
    }
}
