package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.VociIvaServiceBaseImpl;

/**
 * The implementation of the voci iva remote service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.VociIvaService} interface.
 *
 * <p>This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.VociIvaServiceBaseImpl
 * @see it.bysoftware.ct.service.VociIvaServiceUtil
 */
public class VociIvaServiceImpl extends VociIvaServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.VociIvaServiceUtil} to access the voci iva
     * remote service.
     */
}
