package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.RubricaServiceBaseImpl;

/**
 * The implementation of the rubrica remote service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods 
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.RubricaService} interface.
 *
 * <p>This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.RubricaServiceBaseImpl
 * @see it.bysoftware.ct.service.RubricaServiceUtil
 */
public class RubricaServiceImpl extends RubricaServiceBaseImpl {
    
}
