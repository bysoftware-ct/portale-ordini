package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchDescrizioniVariantiException;
import it.bysoftware.ct.model.DescrizioniVarianti;
import it.bysoftware.ct.model.impl.DescrizioniVariantiImpl;
import it.bysoftware.ct.model.impl.DescrizioniVariantiModelImpl;
import it.bysoftware.ct.service.persistence.DescrizioniVariantiPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the descrizioni varianti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see DescrizioniVariantiPersistence
 * @see DescrizioniVariantiUtil
 * @generated
 */
public class DescrizioniVariantiPersistenceImpl extends BasePersistenceImpl<DescrizioniVarianti>
    implements DescrizioniVariantiPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link DescrizioniVariantiUtil} to access the descrizioni varianti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = DescrizioniVariantiImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(DescrizioniVariantiModelImpl.ENTITY_CACHE_ENABLED,
            DescrizioniVariantiModelImpl.FINDER_CACHE_ENABLED,
            DescrizioniVariantiImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(DescrizioniVariantiModelImpl.ENTITY_CACHE_ENABLED,
            DescrizioniVariantiModelImpl.FINDER_CACHE_ENABLED,
            DescrizioniVariantiImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(DescrizioniVariantiModelImpl.ENTITY_CACHE_ENABLED,
            DescrizioniVariantiModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICEARTICOLO =
        new FinderPath(DescrizioniVariantiModelImpl.ENTITY_CACHE_ENABLED,
            DescrizioniVariantiModelImpl.FINDER_CACHE_ENABLED,
            DescrizioniVariantiImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCodiceArticolo",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEARTICOLO =
        new FinderPath(DescrizioniVariantiModelImpl.ENTITY_CACHE_ENABLED,
            DescrizioniVariantiModelImpl.FINDER_CACHE_ENABLED,
            DescrizioniVariantiImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCodiceArticolo",
            new String[] { String.class.getName() },
            DescrizioniVariantiModelImpl.CODICEARTICOLO_COLUMN_BITMASK |
            DescrizioniVariantiModelImpl.DESCRIZIONE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_CODICEARTICOLO = new FinderPath(DescrizioniVariantiModelImpl.ENTITY_CACHE_ENABLED,
            DescrizioniVariantiModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCodiceArticolo",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_CODICEARTICOLO_CODICEARTICOLO_1 = "descrizioniVarianti.id.codiceArticolo IS NULL";
    private static final String _FINDER_COLUMN_CODICEARTICOLO_CODICEARTICOLO_2 = "descrizioniVarianti.id.codiceArticolo = ?";
    private static final String _FINDER_COLUMN_CODICEARTICOLO_CODICEARTICOLO_3 = "(descrizioniVarianti.id.codiceArticolo IS NULL OR descrizioniVarianti.id.codiceArticolo = '')";
    private static final String _SQL_SELECT_DESCRIZIONIVARIANTI = "SELECT descrizioniVarianti FROM DescrizioniVarianti descrizioniVarianti";
    private static final String _SQL_SELECT_DESCRIZIONIVARIANTI_WHERE = "SELECT descrizioniVarianti FROM DescrizioniVarianti descrizioniVarianti WHERE ";
    private static final String _SQL_COUNT_DESCRIZIONIVARIANTI = "SELECT COUNT(descrizioniVarianti) FROM DescrizioniVarianti descrizioniVarianti";
    private static final String _SQL_COUNT_DESCRIZIONIVARIANTI_WHERE = "SELECT COUNT(descrizioniVarianti) FROM DescrizioniVarianti descrizioniVarianti WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "descrizioniVarianti.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No DescrizioniVarianti exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No DescrizioniVarianti exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(DescrizioniVariantiPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "codiceArticolo", "codiceVariante", "descrizione"
            });
    private static DescrizioniVarianti _nullDescrizioniVarianti = new DescrizioniVariantiImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<DescrizioniVarianti> toCacheModel() {
                return _nullDescrizioniVariantiCacheModel;
            }
        };

    private static CacheModel<DescrizioniVarianti> _nullDescrizioniVariantiCacheModel =
        new CacheModel<DescrizioniVarianti>() {
            @Override
            public DescrizioniVarianti toEntityModel() {
                return _nullDescrizioniVarianti;
            }
        };

    public DescrizioniVariantiPersistenceImpl() {
        setModelClass(DescrizioniVarianti.class);
    }

    /**
     * Returns all the descrizioni variantis where codiceArticolo = &#63;.
     *
     * @param codiceArticolo the codice articolo
     * @return the matching descrizioni variantis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<DescrizioniVarianti> findByCodiceArticolo(String codiceArticolo)
        throws SystemException {
        return findByCodiceArticolo(codiceArticolo, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the descrizioni variantis where codiceArticolo = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DescrizioniVariantiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceArticolo the codice articolo
     * @param start the lower bound of the range of descrizioni variantis
     * @param end the upper bound of the range of descrizioni variantis (not inclusive)
     * @return the range of matching descrizioni variantis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<DescrizioniVarianti> findByCodiceArticolo(
        String codiceArticolo, int start, int end) throws SystemException {
        return findByCodiceArticolo(codiceArticolo, start, end, null);
    }

    /**
     * Returns an ordered range of all the descrizioni variantis where codiceArticolo = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DescrizioniVariantiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceArticolo the codice articolo
     * @param start the lower bound of the range of descrizioni variantis
     * @param end the upper bound of the range of descrizioni variantis (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching descrizioni variantis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<DescrizioniVarianti> findByCodiceArticolo(
        String codiceArticolo, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEARTICOLO;
            finderArgs = new Object[] { codiceArticolo };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICEARTICOLO;
            finderArgs = new Object[] {
                    codiceArticolo,
                    
                    start, end, orderByComparator
                };
        }

        List<DescrizioniVarianti> list = (List<DescrizioniVarianti>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (DescrizioniVarianti descrizioniVarianti : list) {
                if (!Validator.equals(codiceArticolo,
                            descrizioniVarianti.getCodiceArticolo())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_DESCRIZIONIVARIANTI_WHERE);

            boolean bindCodiceArticolo = false;

            if (codiceArticolo == null) {
                query.append(_FINDER_COLUMN_CODICEARTICOLO_CODICEARTICOLO_1);
            } else if (codiceArticolo.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICEARTICOLO_CODICEARTICOLO_3);
            } else {
                bindCodiceArticolo = true;

                query.append(_FINDER_COLUMN_CODICEARTICOLO_CODICEARTICOLO_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(DescrizioniVariantiModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceArticolo) {
                    qPos.add(codiceArticolo);
                }

                if (!pagination) {
                    list = (List<DescrizioniVarianti>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<DescrizioniVarianti>(list);
                } else {
                    list = (List<DescrizioniVarianti>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first descrizioni varianti in the ordered set where codiceArticolo = &#63;.
     *
     * @param codiceArticolo the codice articolo
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching descrizioni varianti
     * @throws it.bysoftware.ct.NoSuchDescrizioniVariantiException if a matching descrizioni varianti could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DescrizioniVarianti findByCodiceArticolo_First(
        String codiceArticolo, OrderByComparator orderByComparator)
        throws NoSuchDescrizioniVariantiException, SystemException {
        DescrizioniVarianti descrizioniVarianti = fetchByCodiceArticolo_First(codiceArticolo,
                orderByComparator);

        if (descrizioniVarianti != null) {
            return descrizioniVarianti;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceArticolo=");
        msg.append(codiceArticolo);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchDescrizioniVariantiException(msg.toString());
    }

    /**
     * Returns the first descrizioni varianti in the ordered set where codiceArticolo = &#63;.
     *
     * @param codiceArticolo the codice articolo
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching descrizioni varianti, or <code>null</code> if a matching descrizioni varianti could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DescrizioniVarianti fetchByCodiceArticolo_First(
        String codiceArticolo, OrderByComparator orderByComparator)
        throws SystemException {
        List<DescrizioniVarianti> list = findByCodiceArticolo(codiceArticolo,
                0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last descrizioni varianti in the ordered set where codiceArticolo = &#63;.
     *
     * @param codiceArticolo the codice articolo
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching descrizioni varianti
     * @throws it.bysoftware.ct.NoSuchDescrizioniVariantiException if a matching descrizioni varianti could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DescrizioniVarianti findByCodiceArticolo_Last(
        String codiceArticolo, OrderByComparator orderByComparator)
        throws NoSuchDescrizioniVariantiException, SystemException {
        DescrizioniVarianti descrizioniVarianti = fetchByCodiceArticolo_Last(codiceArticolo,
                orderByComparator);

        if (descrizioniVarianti != null) {
            return descrizioniVarianti;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceArticolo=");
        msg.append(codiceArticolo);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchDescrizioniVariantiException(msg.toString());
    }

    /**
     * Returns the last descrizioni varianti in the ordered set where codiceArticolo = &#63;.
     *
     * @param codiceArticolo the codice articolo
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching descrizioni varianti, or <code>null</code> if a matching descrizioni varianti could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DescrizioniVarianti fetchByCodiceArticolo_Last(
        String codiceArticolo, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByCodiceArticolo(codiceArticolo);

        if (count == 0) {
            return null;
        }

        List<DescrizioniVarianti> list = findByCodiceArticolo(codiceArticolo,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the descrizioni variantis before and after the current descrizioni varianti in the ordered set where codiceArticolo = &#63;.
     *
     * @param descrizioniVariantiPK the primary key of the current descrizioni varianti
     * @param codiceArticolo the codice articolo
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next descrizioni varianti
     * @throws it.bysoftware.ct.NoSuchDescrizioniVariantiException if a descrizioni varianti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DescrizioniVarianti[] findByCodiceArticolo_PrevAndNext(
        DescrizioniVariantiPK descrizioniVariantiPK, String codiceArticolo,
        OrderByComparator orderByComparator)
        throws NoSuchDescrizioniVariantiException, SystemException {
        DescrizioniVarianti descrizioniVarianti = findByPrimaryKey(descrizioniVariantiPK);

        Session session = null;

        try {
            session = openSession();

            DescrizioniVarianti[] array = new DescrizioniVariantiImpl[3];

            array[0] = getByCodiceArticolo_PrevAndNext(session,
                    descrizioniVarianti, codiceArticolo, orderByComparator, true);

            array[1] = descrizioniVarianti;

            array[2] = getByCodiceArticolo_PrevAndNext(session,
                    descrizioniVarianti, codiceArticolo, orderByComparator,
                    false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected DescrizioniVarianti getByCodiceArticolo_PrevAndNext(
        Session session, DescrizioniVarianti descrizioniVarianti,
        String codiceArticolo, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_DESCRIZIONIVARIANTI_WHERE);

        boolean bindCodiceArticolo = false;

        if (codiceArticolo == null) {
            query.append(_FINDER_COLUMN_CODICEARTICOLO_CODICEARTICOLO_1);
        } else if (codiceArticolo.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CODICEARTICOLO_CODICEARTICOLO_3);
        } else {
            bindCodiceArticolo = true;

            query.append(_FINDER_COLUMN_CODICEARTICOLO_CODICEARTICOLO_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(DescrizioniVariantiModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindCodiceArticolo) {
            qPos.add(codiceArticolo);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(descrizioniVarianti);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<DescrizioniVarianti> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the descrizioni variantis where codiceArticolo = &#63; from the database.
     *
     * @param codiceArticolo the codice articolo
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByCodiceArticolo(String codiceArticolo)
        throws SystemException {
        for (DescrizioniVarianti descrizioniVarianti : findByCodiceArticolo(
                codiceArticolo, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(descrizioniVarianti);
        }
    }

    /**
     * Returns the number of descrizioni variantis where codiceArticolo = &#63;.
     *
     * @param codiceArticolo the codice articolo
     * @return the number of matching descrizioni variantis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByCodiceArticolo(String codiceArticolo)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_CODICEARTICOLO;

        Object[] finderArgs = new Object[] { codiceArticolo };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_DESCRIZIONIVARIANTI_WHERE);

            boolean bindCodiceArticolo = false;

            if (codiceArticolo == null) {
                query.append(_FINDER_COLUMN_CODICEARTICOLO_CODICEARTICOLO_1);
            } else if (codiceArticolo.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICEARTICOLO_CODICEARTICOLO_3);
            } else {
                bindCodiceArticolo = true;

                query.append(_FINDER_COLUMN_CODICEARTICOLO_CODICEARTICOLO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceArticolo) {
                    qPos.add(codiceArticolo);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the descrizioni varianti in the entity cache if it is enabled.
     *
     * @param descrizioniVarianti the descrizioni varianti
     */
    @Override
    public void cacheResult(DescrizioniVarianti descrizioniVarianti) {
        EntityCacheUtil.putResult(DescrizioniVariantiModelImpl.ENTITY_CACHE_ENABLED,
            DescrizioniVariantiImpl.class, descrizioniVarianti.getPrimaryKey(),
            descrizioniVarianti);

        descrizioniVarianti.resetOriginalValues();
    }

    /**
     * Caches the descrizioni variantis in the entity cache if it is enabled.
     *
     * @param descrizioniVariantis the descrizioni variantis
     */
    @Override
    public void cacheResult(List<DescrizioniVarianti> descrizioniVariantis) {
        for (DescrizioniVarianti descrizioniVarianti : descrizioniVariantis) {
            if (EntityCacheUtil.getResult(
                        DescrizioniVariantiModelImpl.ENTITY_CACHE_ENABLED,
                        DescrizioniVariantiImpl.class,
                        descrizioniVarianti.getPrimaryKey()) == null) {
                cacheResult(descrizioniVarianti);
            } else {
                descrizioniVarianti.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all descrizioni variantis.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(DescrizioniVariantiImpl.class.getName());
        }

        EntityCacheUtil.clearCache(DescrizioniVariantiImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the descrizioni varianti.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(DescrizioniVarianti descrizioniVarianti) {
        EntityCacheUtil.removeResult(DescrizioniVariantiModelImpl.ENTITY_CACHE_ENABLED,
            DescrizioniVariantiImpl.class, descrizioniVarianti.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<DescrizioniVarianti> descrizioniVariantis) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (DescrizioniVarianti descrizioniVarianti : descrizioniVariantis) {
            EntityCacheUtil.removeResult(DescrizioniVariantiModelImpl.ENTITY_CACHE_ENABLED,
                DescrizioniVariantiImpl.class,
                descrizioniVarianti.getPrimaryKey());
        }
    }

    /**
     * Creates a new descrizioni varianti with the primary key. Does not add the descrizioni varianti to the database.
     *
     * @param descrizioniVariantiPK the primary key for the new descrizioni varianti
     * @return the new descrizioni varianti
     */
    @Override
    public DescrizioniVarianti create(
        DescrizioniVariantiPK descrizioniVariantiPK) {
        DescrizioniVarianti descrizioniVarianti = new DescrizioniVariantiImpl();

        descrizioniVarianti.setNew(true);
        descrizioniVarianti.setPrimaryKey(descrizioniVariantiPK);

        return descrizioniVarianti;
    }

    /**
     * Removes the descrizioni varianti with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param descrizioniVariantiPK the primary key of the descrizioni varianti
     * @return the descrizioni varianti that was removed
     * @throws it.bysoftware.ct.NoSuchDescrizioniVariantiException if a descrizioni varianti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DescrizioniVarianti remove(
        DescrizioniVariantiPK descrizioniVariantiPK)
        throws NoSuchDescrizioniVariantiException, SystemException {
        return remove((Serializable) descrizioniVariantiPK);
    }

    /**
     * Removes the descrizioni varianti with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the descrizioni varianti
     * @return the descrizioni varianti that was removed
     * @throws it.bysoftware.ct.NoSuchDescrizioniVariantiException if a descrizioni varianti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DescrizioniVarianti remove(Serializable primaryKey)
        throws NoSuchDescrizioniVariantiException, SystemException {
        Session session = null;

        try {
            session = openSession();

            DescrizioniVarianti descrizioniVarianti = (DescrizioniVarianti) session.get(DescrizioniVariantiImpl.class,
                    primaryKey);

            if (descrizioniVarianti == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchDescrizioniVariantiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(descrizioniVarianti);
        } catch (NoSuchDescrizioniVariantiException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected DescrizioniVarianti removeImpl(
        DescrizioniVarianti descrizioniVarianti) throws SystemException {
        descrizioniVarianti = toUnwrappedModel(descrizioniVarianti);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(descrizioniVarianti)) {
                descrizioniVarianti = (DescrizioniVarianti) session.get(DescrizioniVariantiImpl.class,
                        descrizioniVarianti.getPrimaryKeyObj());
            }

            if (descrizioniVarianti != null) {
                session.delete(descrizioniVarianti);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (descrizioniVarianti != null) {
            clearCache(descrizioniVarianti);
        }

        return descrizioniVarianti;
    }

    @Override
    public DescrizioniVarianti updateImpl(
        it.bysoftware.ct.model.DescrizioniVarianti descrizioniVarianti)
        throws SystemException {
        descrizioniVarianti = toUnwrappedModel(descrizioniVarianti);

        boolean isNew = descrizioniVarianti.isNew();

        DescrizioniVariantiModelImpl descrizioniVariantiModelImpl = (DescrizioniVariantiModelImpl) descrizioniVarianti;

        Session session = null;

        try {
            session = openSession();

            if (descrizioniVarianti.isNew()) {
                session.save(descrizioniVarianti);

                descrizioniVarianti.setNew(false);
            } else {
                session.merge(descrizioniVarianti);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !DescrizioniVariantiModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((descrizioniVariantiModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEARTICOLO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        descrizioniVariantiModelImpl.getOriginalCodiceArticolo()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICEARTICOLO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEARTICOLO,
                    args);

                args = new Object[] {
                        descrizioniVariantiModelImpl.getCodiceArticolo()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICEARTICOLO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEARTICOLO,
                    args);
            }
        }

        EntityCacheUtil.putResult(DescrizioniVariantiModelImpl.ENTITY_CACHE_ENABLED,
            DescrizioniVariantiImpl.class, descrizioniVarianti.getPrimaryKey(),
            descrizioniVarianti);

        return descrizioniVarianti;
    }

    protected DescrizioniVarianti toUnwrappedModel(
        DescrizioniVarianti descrizioniVarianti) {
        if (descrizioniVarianti instanceof DescrizioniVariantiImpl) {
            return descrizioniVarianti;
        }

        DescrizioniVariantiImpl descrizioniVariantiImpl = new DescrizioniVariantiImpl();

        descrizioniVariantiImpl.setNew(descrizioniVarianti.isNew());
        descrizioniVariantiImpl.setPrimaryKey(descrizioniVarianti.getPrimaryKey());

        descrizioniVariantiImpl.setCodiceArticolo(descrizioniVarianti.getCodiceArticolo());
        descrizioniVariantiImpl.setCodiceVariante(descrizioniVarianti.getCodiceVariante());
        descrizioniVariantiImpl.setDescrizione(descrizioniVarianti.getDescrizione());

        return descrizioniVariantiImpl;
    }

    /**
     * Returns the descrizioni varianti with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the descrizioni varianti
     * @return the descrizioni varianti
     * @throws it.bysoftware.ct.NoSuchDescrizioniVariantiException if a descrizioni varianti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DescrizioniVarianti findByPrimaryKey(Serializable primaryKey)
        throws NoSuchDescrizioniVariantiException, SystemException {
        DescrizioniVarianti descrizioniVarianti = fetchByPrimaryKey(primaryKey);

        if (descrizioniVarianti == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchDescrizioniVariantiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return descrizioniVarianti;
    }

    /**
     * Returns the descrizioni varianti with the primary key or throws a {@link it.bysoftware.ct.NoSuchDescrizioniVariantiException} if it could not be found.
     *
     * @param descrizioniVariantiPK the primary key of the descrizioni varianti
     * @return the descrizioni varianti
     * @throws it.bysoftware.ct.NoSuchDescrizioniVariantiException if a descrizioni varianti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DescrizioniVarianti findByPrimaryKey(
        DescrizioniVariantiPK descrizioniVariantiPK)
        throws NoSuchDescrizioniVariantiException, SystemException {
        return findByPrimaryKey((Serializable) descrizioniVariantiPK);
    }

    /**
     * Returns the descrizioni varianti with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the descrizioni varianti
     * @return the descrizioni varianti, or <code>null</code> if a descrizioni varianti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DescrizioniVarianti fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        DescrizioniVarianti descrizioniVarianti = (DescrizioniVarianti) EntityCacheUtil.getResult(DescrizioniVariantiModelImpl.ENTITY_CACHE_ENABLED,
                DescrizioniVariantiImpl.class, primaryKey);

        if (descrizioniVarianti == _nullDescrizioniVarianti) {
            return null;
        }

        if (descrizioniVarianti == null) {
            Session session = null;

            try {
                session = openSession();

                descrizioniVarianti = (DescrizioniVarianti) session.get(DescrizioniVariantiImpl.class,
                        primaryKey);

                if (descrizioniVarianti != null) {
                    cacheResult(descrizioniVarianti);
                } else {
                    EntityCacheUtil.putResult(DescrizioniVariantiModelImpl.ENTITY_CACHE_ENABLED,
                        DescrizioniVariantiImpl.class, primaryKey,
                        _nullDescrizioniVarianti);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(DescrizioniVariantiModelImpl.ENTITY_CACHE_ENABLED,
                    DescrizioniVariantiImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return descrizioniVarianti;
    }

    /**
     * Returns the descrizioni varianti with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param descrizioniVariantiPK the primary key of the descrizioni varianti
     * @return the descrizioni varianti, or <code>null</code> if a descrizioni varianti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DescrizioniVarianti fetchByPrimaryKey(
        DescrizioniVariantiPK descrizioniVariantiPK) throws SystemException {
        return fetchByPrimaryKey((Serializable) descrizioniVariantiPK);
    }

    /**
     * Returns all the descrizioni variantis.
     *
     * @return the descrizioni variantis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<DescrizioniVarianti> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the descrizioni variantis.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DescrizioniVariantiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of descrizioni variantis
     * @param end the upper bound of the range of descrizioni variantis (not inclusive)
     * @return the range of descrizioni variantis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<DescrizioniVarianti> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the descrizioni variantis.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DescrizioniVariantiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of descrizioni variantis
     * @param end the upper bound of the range of descrizioni variantis (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of descrizioni variantis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<DescrizioniVarianti> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<DescrizioniVarianti> list = (List<DescrizioniVarianti>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_DESCRIZIONIVARIANTI);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_DESCRIZIONIVARIANTI;

                if (pagination) {
                    sql = sql.concat(DescrizioniVariantiModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<DescrizioniVarianti>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<DescrizioniVarianti>(list);
                } else {
                    list = (List<DescrizioniVarianti>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the descrizioni variantis from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (DescrizioniVarianti descrizioniVarianti : findAll()) {
            remove(descrizioniVarianti);
        }
    }

    /**
     * Returns the number of descrizioni variantis.
     *
     * @return the number of descrizioni variantis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_DESCRIZIONIVARIANTI);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the descrizioni varianti persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.DescrizioniVarianti")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<DescrizioniVarianti>> listenersList = new ArrayList<ModelListener<DescrizioniVarianti>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<DescrizioniVarianti>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(DescrizioniVariantiImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
