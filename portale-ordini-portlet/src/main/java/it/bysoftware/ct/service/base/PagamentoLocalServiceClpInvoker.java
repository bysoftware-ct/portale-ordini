package it.bysoftware.ct.service.base;

import it.bysoftware.ct.service.PagamentoLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Mario Torrisi
 * @generated
 */
public class PagamentoLocalServiceClpInvoker {
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName2;
    private String[] _methodParameterTypes2;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName238;
    private String[] _methodParameterTypes238;
    private String _methodName239;
    private String[] _methodParameterTypes239;
    private String _methodName244;
    private String[] _methodParameterTypes244;
    private String _methodName245;
    private String[] _methodParameterTypes245;
    private String _methodName246;
    private String[] _methodParameterTypes246;

    public PagamentoLocalServiceClpInvoker() {
        _methodName0 = "addPagamento";

        _methodParameterTypes0 = new String[] { "it.bysoftware.ct.model.Pagamento" };

        _methodName1 = "createPagamento";

        _methodParameterTypes1 = new String[] {
                "it.bysoftware.ct.service.persistence.PagamentoPK"
            };

        _methodName2 = "deletePagamento";

        _methodParameterTypes2 = new String[] {
                "it.bysoftware.ct.service.persistence.PagamentoPK"
            };

        _methodName3 = "deletePagamento";

        _methodParameterTypes3 = new String[] { "it.bysoftware.ct.model.Pagamento" };

        _methodName4 = "dynamicQuery";

        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "dynamicQuery";

        _methodParameterTypes5 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName6 = "dynamicQuery";

        _methodParameterTypes6 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
            };

        _methodName7 = "dynamicQuery";

        _methodParameterTypes7 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName8 = "dynamicQueryCount";

        _methodParameterTypes8 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName9 = "dynamicQueryCount";

        _methodParameterTypes9 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery",
                "com.liferay.portal.kernel.dao.orm.Projection"
            };

        _methodName10 = "fetchPagamento";

        _methodParameterTypes10 = new String[] {
                "it.bysoftware.ct.service.persistence.PagamentoPK"
            };

        _methodName11 = "getPagamento";

        _methodParameterTypes11 = new String[] {
                "it.bysoftware.ct.service.persistence.PagamentoPK"
            };

        _methodName12 = "getPersistedModel";

        _methodParameterTypes12 = new String[] { "java.io.Serializable" };

        _methodName13 = "getPagamentos";

        _methodParameterTypes13 = new String[] { "int", "int" };

        _methodName14 = "getPagamentosCount";

        _methodParameterTypes14 = new String[] {  };

        _methodName15 = "updatePagamento";

        _methodParameterTypes15 = new String[] {
                "it.bysoftware.ct.model.Pagamento"
            };

        _methodName238 = "getBeanIdentifier";

        _methodParameterTypes238 = new String[] {  };

        _methodName239 = "setBeanIdentifier";

        _methodParameterTypes239 = new String[] { "java.lang.String" };

        _methodName244 = "getNumber";

        _methodParameterTypes244 = new String[] { "int", "java.lang.String" };

        _methodName245 = "findByYear";

        _methodParameterTypes245 = new String[] { "int" };

        _methodName246 = "findByYearAndState";

        _methodParameterTypes246 = new String[] { "int", "java.lang.Integer" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName0.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
            return PagamentoLocalServiceUtil.addPagamento((it.bysoftware.ct.model.Pagamento) arguments[0]);
        }

        if (_methodName1.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
            return PagamentoLocalServiceUtil.createPagamento((it.bysoftware.ct.service.persistence.PagamentoPK) arguments[0]);
        }

        if (_methodName2.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
            return PagamentoLocalServiceUtil.deletePagamento((it.bysoftware.ct.service.persistence.PagamentoPK) arguments[0]);
        }

        if (_methodName3.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
            return PagamentoLocalServiceUtil.deletePagamento((it.bysoftware.ct.model.Pagamento) arguments[0]);
        }

        if (_methodName4.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
            return PagamentoLocalServiceUtil.dynamicQuery();
        }

        if (_methodName5.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
            return PagamentoLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName6.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
            return PagamentoLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName7.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
            return PagamentoLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[3]);
        }

        if (_methodName8.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
            return PagamentoLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName9.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
            return PagamentoLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                (com.liferay.portal.kernel.dao.orm.Projection) arguments[1]);
        }

        if (_methodName10.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
            return PagamentoLocalServiceUtil.fetchPagamento((it.bysoftware.ct.service.persistence.PagamentoPK) arguments[0]);
        }

        if (_methodName11.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
            return PagamentoLocalServiceUtil.getPagamento((it.bysoftware.ct.service.persistence.PagamentoPK) arguments[0]);
        }

        if (_methodName12.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
            return PagamentoLocalServiceUtil.getPersistedModel((java.io.Serializable) arguments[0]);
        }

        if (_methodName13.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
            return PagamentoLocalServiceUtil.getPagamentos(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName14.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
            return PagamentoLocalServiceUtil.getPagamentosCount();
        }

        if (_methodName15.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
            return PagamentoLocalServiceUtil.updatePagamento((it.bysoftware.ct.model.Pagamento) arguments[0]);
        }

        if (_methodName238.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes238, parameterTypes)) {
            return PagamentoLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName239.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes239, parameterTypes)) {
            PagamentoLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName244.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes244, parameterTypes)) {
            return PagamentoLocalServiceUtil.getNumber(((Integer) arguments[0]).intValue(),
                (java.lang.String) arguments[1]);
        }

        if (_methodName245.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes245, parameterTypes)) {
            return PagamentoLocalServiceUtil.findByYear(((Integer) arguments[0]).intValue());
        }

        if (_methodName246.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes246, parameterTypes)) {
            return PagamentoLocalServiceUtil.findByYearAndState(((Integer) arguments[0]).intValue(),
                (java.lang.Integer) arguments[1]);
        }

        throw new UnsupportedOperationException();
    }
}
