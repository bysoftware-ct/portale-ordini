package it.bysoftware.ct.service.impl;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Junction;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import it.bysoftware.ct.model.ScadenzePartite;
import it.bysoftware.ct.service.ScadenzePartiteLocalServiceUtil;
import it.bysoftware.ct.service.base.ScadenzePartiteLocalServiceBaseImpl;
import it.bysoftware.ct.utils.EntryState;

/**
 * The implementation of the scadenze partite local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.ScadenzePartiteLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.ScadenzePartiteLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.ScadenzePartiteLocalServiceUtil
 */
public class ScadenzePartiteLocalServiceImpl extends
        ScadenzePartiteLocalServiceBaseImpl {

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil
            .getLog(ScadenzePartiteLocalServiceImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public final List<ScadenzePartite> getCustomersClosedEntries(
            final Date from, final Date to, final int start, final int end,
            final OrderByComparator orderByComparator) throws SystemException {

        DynamicQuery dynamicQuery = this.buildDynamicQuery(false, "", from, to,
                EntryState.CLOSED);
        return ScadenzePartiteLocalServiceUtil.dynamicQuery(dynamicQuery,
                start, end, orderByComparator);
    }
    
    @Override
    public final int getCustomersClosedEntriesCount(final Date from,
            final Date to) throws SystemException {
        DynamicQuery dynamicQuery = this.buildDynamicQuery(false, "", from, to,
                EntryState.CLOSED);
        return (int) ScadenzePartiteLocalServiceUtil.
                dynamicQueryCount(dynamicQuery);
    }
    

    @SuppressWarnings("unchecked")
    @Override
    public final List<ScadenzePartite> getCustomerClosedEntries(
            final String customer, final Date from, final Date to,
            final int start, final int end,
            final OrderByComparator orderByComparator) throws SystemException {

        DynamicQuery dynamicQuery = this.buildDynamicQuery(false, customer,
                from, to, EntryState.CLOSED);
        return ScadenzePartiteLocalServiceUtil.dynamicQuery(dynamicQuery,
                start, end, orderByComparator);
    }
    
    @Override
    public final int getCustomerClosedEntriesCount(final String customer,
            final Date from, final Date to) throws SystemException {
        DynamicQuery dynamicQuery = this.buildDynamicQuery(false, "", from, to,
                EntryState.CLOSED);
        return (int) ScadenzePartiteLocalServiceUtil.
                dynamicQueryCount(dynamicQuery);
    }

    @SuppressWarnings("unchecked")
    @Override
    public final List<ScadenzePartite> getPartnerOpenedEntries(
            final String partnerCode, final Date from, final Date to,
            final int start, final int end,
            final OrderByComparator orderByComparator) throws SystemException {

        DynamicQuery dynamicQuery = this.buildDynamicQuery(null, partnerCode,
                from, to, EntryState.OPENED);
        return ScadenzePartiteLocalServiceUtil.dynamicQuery(dynamicQuery,
                start, end, orderByComparator);
    }
    
    @Override
    public final int getPartnerOpenedEntriesCount(final String partnerCode,
            final Date from, final Date to) throws SystemException {
        DynamicQuery dynamicQuery = this.buildDynamicQuery(null, partnerCode,
                from, to, EntryState.OPENED);
        return (int) ScadenzePartiteLocalServiceUtil.
                dynamicQueryCount(dynamicQuery);
    }
    
    /**
     * Build {@link DynamicQuery} based on the filter specified.
     * 
     * @param subjectType
     *            true for partner, false otherwise
     * @param from
     *            from date
     * @param to
     *            to date
     * @param partnerCode
     *            partner code
     * @param state
     *            the entry state
     * @return {@link DynamicQuery} created
     */
    private DynamicQuery buildDynamicQuery(final Boolean subjectType,
            final String partnerCode, final Date from, final Date to,
            final EntryState state) {

        Junction junction = RestrictionsFactoryUtil.conjunction();

        this.logger.info("Looking for " + subjectType + " entries ");
        
        Property property = null;

        if (Validator.isNotNull(subjectType)) {
            property = PropertyFactoryUtil.forName("primaryKey.tipoSoggetto");
            junction.add(property.eq(subjectType));
        }
        
        if (!partnerCode.isEmpty()) {
            property = PropertyFactoryUtil.forName("primaryKey.codiceSoggetto");
            junction.add(property.eq(partnerCode));
        }
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        
        if (state.equals(EntryState.CLOSED)) {
            property = PropertyFactoryUtil.forName("dataChiusura");
        } else {
            property = PropertyFactoryUtil.forName("dataDocumento");
        }
        
        if (Validator.isNotNull(from)) {
            if (Validator.isNotNull(to)) {
                this.logger.info("between: " + sdf.format(from) + " and "
                        + sdf.format(to));
                junction.add(property.ge(from));
                junction.add(property.le(to));
            } else {
                this.logger.info("greater than: " + sdf.format(from));
                junction.add(property.ge(from));
            }
        } else if (Validator.isNotNull(to)) {
            this.logger.info("less than: " + sdf.format(to));
            junction.add(property.le(to));
        }

        if (state.equals(EntryState.CLOSED)) {
            this.logger.info(" and state equals: " + state.name() + "..."); 
            property = PropertyFactoryUtil.forName("stato");
            junction.add(property.eq(state.getVal()));
        } else {
            this.logger.info(" and state NOT equals: "
                    + EntryState.CLOSED.name() + "..."); 
            property = PropertyFactoryUtil.forName("stato");
            junction.add(property.ne(EntryState.CLOSED.getVal()));
        }
        
        DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
                ScadenzePartite.class, getClassLoader());
        return dynamicQuery.add(junction);

    }
}
