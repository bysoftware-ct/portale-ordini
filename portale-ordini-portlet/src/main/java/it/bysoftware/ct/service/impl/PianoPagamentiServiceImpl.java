package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.PianoPagamentiServiceBaseImpl;

/**
 * The implementation of the piano pagamenti remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.bysoftware.ct.service.PianoPagamentiService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.PianoPagamentiServiceBaseImpl
 * @see it.bysoftware.ct.service.PianoPagamentiServiceUtil
 */
public class PianoPagamentiServiceImpl extends PianoPagamentiServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link it.bysoftware.ct.service.PianoPagamentiServiceUtil} to access the piano pagamenti remote service.
     */
}
