package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.SchedaPagamentoServiceBaseImpl;

/**
 * The implementation of the scheda pagamento remote service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.SchedaPagamentoService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.SchedaPagamentoServiceBaseImpl
 * @see it.bysoftware.ct.service.SchedaPagamentoServiceUtil
 */
public class SchedaPagamentoServiceImpl extends SchedaPagamentoServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.SchedaPagamentoServiceUtil} to access the scheda
     * pagamento remote service.
     */
}
