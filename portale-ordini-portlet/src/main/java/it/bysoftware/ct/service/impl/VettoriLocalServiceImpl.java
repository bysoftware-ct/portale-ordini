package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.VettoriLocalServiceBaseImpl;

/**
 * The implementation of the vettori local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.VettoriLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.VettoriLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.VettoriLocalServiceUtil
 */
public class VettoriLocalServiceImpl extends VettoriLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.VettoriLocalServiceUtil} to access the vettori
     * local service.
     */
}
