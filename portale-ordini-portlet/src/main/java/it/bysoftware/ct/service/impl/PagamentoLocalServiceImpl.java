package it.bysoftware.ct.service.impl;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;

import it.bysoftware.ct.model.Pagamento;
import it.bysoftware.ct.service.PagamentoLocalServiceUtil;
import it.bysoftware.ct.service.base.PagamentoLocalServiceBaseImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * The implementation of the pagamento local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods
 * areadded, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.PagamentoLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.PagamentoLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.PagamentoLocalServiceUtil
 */
public class PagamentoLocalServiceImpl extends PagamentoLocalServiceBaseImpl {

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(PagamentoLocalServiceImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public final int getNumber(final int year, final String partner) {

        int result = 0;

        DynamicQuery userQuery = DynamicQueryFactoryUtil.forClass(
                Pagamento.class, getClassLoader());
        userQuery.add(PropertyFactoryUtil.forName("primaryKey.anno").eq(year));
        userQuery.add(PropertyFactoryUtil.forName("primaryKey.codiceSoggetto")
                .eq(partner));
        Projection projection = PropertyFactoryUtil.forName(
                "primaryKey.id").max();
        userQuery.setProjection(projection);
        try {
            List<Object> list = PagamentoLocalServiceUtil.dynamicQuery(
                    userQuery);

            if (Validator.isNumber(String.valueOf(list.get(0)))
                    && Integer.parseInt(String.valueOf(list.get(0))) > 0) {
                result = Integer.parseInt(String.valueOf(list.get(0)));
            } else {
                result = Calendar.getInstance().get(Calendar.MONTH);
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        return result + 1;
    }
    
    @Override
    public final List<Pagamento> findByYear(final int year) {
        return this.findByYearAndState(year, null);
    }
    
    @Override
    public final List<Pagamento> findByYearAndState(final int year,
            final Integer state) {
        List<Pagamento> list = new ArrayList<Pagamento>();
            try {
                if (state == null) {
                    list = this.pagamentoPersistence.findByAnno(year);
                } else {
                    list = this.pagamentoPersistence.findByAnnoStato(year,
                            state);
                }
            } catch (SystemException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
            }
        
        return list;
    }
    
}
