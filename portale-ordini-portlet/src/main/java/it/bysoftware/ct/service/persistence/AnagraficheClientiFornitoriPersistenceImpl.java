package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchAnagraficheClientiFornitoriException;
import it.bysoftware.ct.model.AnagraficheClientiFornitori;
import it.bysoftware.ct.model.impl.AnagraficheClientiFornitoriImpl;
import it.bysoftware.ct.model.impl.AnagraficheClientiFornitoriModelImpl;
import it.bysoftware.ct.service.persistence.AnagraficheClientiFornitoriPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the anagrafiche clienti fornitori service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see AnagraficheClientiFornitoriPersistence
 * @see AnagraficheClientiFornitoriUtil
 * @generated
 */
public class AnagraficheClientiFornitoriPersistenceImpl
    extends BasePersistenceImpl<AnagraficheClientiFornitori>
    implements AnagraficheClientiFornitoriPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link AnagraficheClientiFornitoriUtil} to access the anagrafiche clienti fornitori persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = AnagraficheClientiFornitoriImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AnagraficheClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
            AnagraficheClientiFornitoriModelImpl.FINDER_CACHE_ENABLED,
            AnagraficheClientiFornitoriImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AnagraficheClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
            AnagraficheClientiFornitoriModelImpl.FINDER_CACHE_ENABLED,
            AnagraficheClientiFornitoriImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AnagraficheClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
            AnagraficheClientiFornitoriModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
            new String[0]);
    private static final String _SQL_SELECT_ANAGRAFICHECLIENTIFORNITORI = "SELECT anagraficheClientiFornitori FROM AnagraficheClientiFornitori anagraficheClientiFornitori";
    private static final String _SQL_COUNT_ANAGRAFICHECLIENTIFORNITORI = "SELECT COUNT(anagraficheClientiFornitori) FROM AnagraficheClientiFornitori anagraficheClientiFornitori";
    private static final String _ORDER_BY_ENTITY_ALIAS = "anagraficheClientiFornitori.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No AnagraficheClientiFornitori exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(AnagraficheClientiFornitoriPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "attivoEC", "CAP", "codiceAnagrafica", "codiceFiscale",
                "codiceMnemonico", "comune", "indirizzo", "note", "partitaIVA",
                "ragioneSociale1", "ragioneSociale2", "siglaProvincia",
                "siglaStato", "personaFisica", "tipoSoggetto", "tipoSollecito",
                "codiceAzienda"
            });
    private static AnagraficheClientiFornitori _nullAnagraficheClientiFornitori = new AnagraficheClientiFornitoriImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<AnagraficheClientiFornitori> toCacheModel() {
                return _nullAnagraficheClientiFornitoriCacheModel;
            }
        };

    private static CacheModel<AnagraficheClientiFornitori> _nullAnagraficheClientiFornitoriCacheModel =
        new CacheModel<AnagraficheClientiFornitori>() {
            @Override
            public AnagraficheClientiFornitori toEntityModel() {
                return _nullAnagraficheClientiFornitori;
            }
        };

    public AnagraficheClientiFornitoriPersistenceImpl() {
        setModelClass(AnagraficheClientiFornitori.class);
    }

    /**
     * Caches the anagrafiche clienti fornitori in the entity cache if it is enabled.
     *
     * @param anagraficheClientiFornitori the anagrafiche clienti fornitori
     */
    @Override
    public void cacheResult(
        AnagraficheClientiFornitori anagraficheClientiFornitori) {
        EntityCacheUtil.putResult(AnagraficheClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
            AnagraficheClientiFornitoriImpl.class,
            anagraficheClientiFornitori.getPrimaryKey(),
            anagraficheClientiFornitori);

        anagraficheClientiFornitori.resetOriginalValues();
    }

    /**
     * Caches the anagrafiche clienti fornitoris in the entity cache if it is enabled.
     *
     * @param anagraficheClientiFornitoris the anagrafiche clienti fornitoris
     */
    @Override
    public void cacheResult(
        List<AnagraficheClientiFornitori> anagraficheClientiFornitoris) {
        for (AnagraficheClientiFornitori anagraficheClientiFornitori : anagraficheClientiFornitoris) {
            if (EntityCacheUtil.getResult(
                        AnagraficheClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
                        AnagraficheClientiFornitoriImpl.class,
                        anagraficheClientiFornitori.getPrimaryKey()) == null) {
                cacheResult(anagraficheClientiFornitori);
            } else {
                anagraficheClientiFornitori.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all anagrafiche clienti fornitoris.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(AnagraficheClientiFornitoriImpl.class.getName());
        }

        EntityCacheUtil.clearCache(AnagraficheClientiFornitoriImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the anagrafiche clienti fornitori.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(
        AnagraficheClientiFornitori anagraficheClientiFornitori) {
        EntityCacheUtil.removeResult(AnagraficheClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
            AnagraficheClientiFornitoriImpl.class,
            anagraficheClientiFornitori.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(
        List<AnagraficheClientiFornitori> anagraficheClientiFornitoris) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (AnagraficheClientiFornitori anagraficheClientiFornitori : anagraficheClientiFornitoris) {
            EntityCacheUtil.removeResult(AnagraficheClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
                AnagraficheClientiFornitoriImpl.class,
                anagraficheClientiFornitori.getPrimaryKey());
        }
    }

    /**
     * Creates a new anagrafiche clienti fornitori with the primary key. Does not add the anagrafiche clienti fornitori to the database.
     *
     * @param codiceAnagrafica the primary key for the new anagrafiche clienti fornitori
     * @return the new anagrafiche clienti fornitori
     */
    @Override
    public AnagraficheClientiFornitori create(String codiceAnagrafica) {
        AnagraficheClientiFornitori anagraficheClientiFornitori = new AnagraficheClientiFornitoriImpl();

        anagraficheClientiFornitori.setNew(true);
        anagraficheClientiFornitori.setPrimaryKey(codiceAnagrafica);

        return anagraficheClientiFornitori;
    }

    /**
     * Removes the anagrafiche clienti fornitori with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param codiceAnagrafica the primary key of the anagrafiche clienti fornitori
     * @return the anagrafiche clienti fornitori that was removed
     * @throws it.bysoftware.ct.NoSuchAnagraficheClientiFornitoriException if a anagrafiche clienti fornitori with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AnagraficheClientiFornitori remove(String codiceAnagrafica)
        throws NoSuchAnagraficheClientiFornitoriException, SystemException {
        return remove((Serializable) codiceAnagrafica);
    }

    /**
     * Removes the anagrafiche clienti fornitori with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the anagrafiche clienti fornitori
     * @return the anagrafiche clienti fornitori that was removed
     * @throws it.bysoftware.ct.NoSuchAnagraficheClientiFornitoriException if a anagrafiche clienti fornitori with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AnagraficheClientiFornitori remove(Serializable primaryKey)
        throws NoSuchAnagraficheClientiFornitoriException, SystemException {
        Session session = null;

        try {
            session = openSession();

            AnagraficheClientiFornitori anagraficheClientiFornitori = (AnagraficheClientiFornitori) session.get(AnagraficheClientiFornitoriImpl.class,
                    primaryKey);

            if (anagraficheClientiFornitori == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchAnagraficheClientiFornitoriException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(anagraficheClientiFornitori);
        } catch (NoSuchAnagraficheClientiFornitoriException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected AnagraficheClientiFornitori removeImpl(
        AnagraficheClientiFornitori anagraficheClientiFornitori)
        throws SystemException {
        anagraficheClientiFornitori = toUnwrappedModel(anagraficheClientiFornitori);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(anagraficheClientiFornitori)) {
                anagraficheClientiFornitori = (AnagraficheClientiFornitori) session.get(AnagraficheClientiFornitoriImpl.class,
                        anagraficheClientiFornitori.getPrimaryKeyObj());
            }

            if (anagraficheClientiFornitori != null) {
                session.delete(anagraficheClientiFornitori);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (anagraficheClientiFornitori != null) {
            clearCache(anagraficheClientiFornitori);
        }

        return anagraficheClientiFornitori;
    }

    @Override
    public AnagraficheClientiFornitori updateImpl(
        it.bysoftware.ct.model.AnagraficheClientiFornitori anagraficheClientiFornitori)
        throws SystemException {
        anagraficheClientiFornitori = toUnwrappedModel(anagraficheClientiFornitori);

        boolean isNew = anagraficheClientiFornitori.isNew();

        Session session = null;

        try {
            session = openSession();

            if (anagraficheClientiFornitori.isNew()) {
                session.save(anagraficheClientiFornitori);

                anagraficheClientiFornitori.setNew(false);
            } else {
                session.merge(anagraficheClientiFornitori);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(AnagraficheClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
            AnagraficheClientiFornitoriImpl.class,
            anagraficheClientiFornitori.getPrimaryKey(),
            anagraficheClientiFornitori);

        return anagraficheClientiFornitori;
    }

    protected AnagraficheClientiFornitori toUnwrappedModel(
        AnagraficheClientiFornitori anagraficheClientiFornitori) {
        if (anagraficheClientiFornitori instanceof AnagraficheClientiFornitoriImpl) {
            return anagraficheClientiFornitori;
        }

        AnagraficheClientiFornitoriImpl anagraficheClientiFornitoriImpl = new AnagraficheClientiFornitoriImpl();

        anagraficheClientiFornitoriImpl.setNew(anagraficheClientiFornitori.isNew());
        anagraficheClientiFornitoriImpl.setPrimaryKey(anagraficheClientiFornitori.getPrimaryKey());

        anagraficheClientiFornitoriImpl.setAttivoEC(anagraficheClientiFornitori.isAttivoEC());
        anagraficheClientiFornitoriImpl.setCAP(anagraficheClientiFornitori.getCAP());
        anagraficheClientiFornitoriImpl.setCodiceAnagrafica(anagraficheClientiFornitori.getCodiceAnagrafica());
        anagraficheClientiFornitoriImpl.setCodiceFiscale(anagraficheClientiFornitori.getCodiceFiscale());
        anagraficheClientiFornitoriImpl.setCodiceMnemonico(anagraficheClientiFornitori.getCodiceMnemonico());
        anagraficheClientiFornitoriImpl.setComune(anagraficheClientiFornitori.getComune());
        anagraficheClientiFornitoriImpl.setIndirizzo(anagraficheClientiFornitori.getIndirizzo());
        anagraficheClientiFornitoriImpl.setNote(anagraficheClientiFornitori.getNote());
        anagraficheClientiFornitoriImpl.setPartitaIVA(anagraficheClientiFornitori.getPartitaIVA());
        anagraficheClientiFornitoriImpl.setRagioneSociale1(anagraficheClientiFornitori.getRagioneSociale1());
        anagraficheClientiFornitoriImpl.setRagioneSociale2(anagraficheClientiFornitori.getRagioneSociale2());
        anagraficheClientiFornitoriImpl.setSiglaProvincia(anagraficheClientiFornitori.getSiglaProvincia());
        anagraficheClientiFornitoriImpl.setSiglaStato(anagraficheClientiFornitori.getSiglaStato());
        anagraficheClientiFornitoriImpl.setPersonaFisica(anagraficheClientiFornitori.isPersonaFisica());
        anagraficheClientiFornitoriImpl.setTipoSoggetto(anagraficheClientiFornitori.getTipoSoggetto());
        anagraficheClientiFornitoriImpl.setTipoSollecito(anagraficheClientiFornitori.getTipoSollecito());
        anagraficheClientiFornitoriImpl.setCodiceAzienda(anagraficheClientiFornitori.getCodiceAzienda());

        return anagraficheClientiFornitoriImpl;
    }

    /**
     * Returns the anagrafiche clienti fornitori with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the anagrafiche clienti fornitori
     * @return the anagrafiche clienti fornitori
     * @throws it.bysoftware.ct.NoSuchAnagraficheClientiFornitoriException if a anagrafiche clienti fornitori with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AnagraficheClientiFornitori findByPrimaryKey(Serializable primaryKey)
        throws NoSuchAnagraficheClientiFornitoriException, SystemException {
        AnagraficheClientiFornitori anagraficheClientiFornitori = fetchByPrimaryKey(primaryKey);

        if (anagraficheClientiFornitori == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchAnagraficheClientiFornitoriException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return anagraficheClientiFornitori;
    }

    /**
     * Returns the anagrafiche clienti fornitori with the primary key or throws a {@link it.bysoftware.ct.NoSuchAnagraficheClientiFornitoriException} if it could not be found.
     *
     * @param codiceAnagrafica the primary key of the anagrafiche clienti fornitori
     * @return the anagrafiche clienti fornitori
     * @throws it.bysoftware.ct.NoSuchAnagraficheClientiFornitoriException if a anagrafiche clienti fornitori with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AnagraficheClientiFornitori findByPrimaryKey(String codiceAnagrafica)
        throws NoSuchAnagraficheClientiFornitoriException, SystemException {
        return findByPrimaryKey((Serializable) codiceAnagrafica);
    }

    /**
     * Returns the anagrafiche clienti fornitori with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the anagrafiche clienti fornitori
     * @return the anagrafiche clienti fornitori, or <code>null</code> if a anagrafiche clienti fornitori with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AnagraficheClientiFornitori fetchByPrimaryKey(
        Serializable primaryKey) throws SystemException {
        AnagraficheClientiFornitori anagraficheClientiFornitori = (AnagraficheClientiFornitori) EntityCacheUtil.getResult(AnagraficheClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
                AnagraficheClientiFornitoriImpl.class, primaryKey);

        if (anagraficheClientiFornitori == _nullAnagraficheClientiFornitori) {
            return null;
        }

        if (anagraficheClientiFornitori == null) {
            Session session = null;

            try {
                session = openSession();

                anagraficheClientiFornitori = (AnagraficheClientiFornitori) session.get(AnagraficheClientiFornitoriImpl.class,
                        primaryKey);

                if (anagraficheClientiFornitori != null) {
                    cacheResult(anagraficheClientiFornitori);
                } else {
                    EntityCacheUtil.putResult(AnagraficheClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
                        AnagraficheClientiFornitoriImpl.class, primaryKey,
                        _nullAnagraficheClientiFornitori);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(AnagraficheClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
                    AnagraficheClientiFornitoriImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return anagraficheClientiFornitori;
    }

    /**
     * Returns the anagrafiche clienti fornitori with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param codiceAnagrafica the primary key of the anagrafiche clienti fornitori
     * @return the anagrafiche clienti fornitori, or <code>null</code> if a anagrafiche clienti fornitori with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AnagraficheClientiFornitori fetchByPrimaryKey(
        String codiceAnagrafica) throws SystemException {
        return fetchByPrimaryKey((Serializable) codiceAnagrafica);
    }

    /**
     * Returns all the anagrafiche clienti fornitoris.
     *
     * @return the anagrafiche clienti fornitoris
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<AnagraficheClientiFornitori> findAll()
        throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the anagrafiche clienti fornitoris.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficheClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of anagrafiche clienti fornitoris
     * @param end the upper bound of the range of anagrafiche clienti fornitoris (not inclusive)
     * @return the range of anagrafiche clienti fornitoris
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<AnagraficheClientiFornitori> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the anagrafiche clienti fornitoris.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficheClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of anagrafiche clienti fornitoris
     * @param end the upper bound of the range of anagrafiche clienti fornitoris (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of anagrafiche clienti fornitoris
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<AnagraficheClientiFornitori> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<AnagraficheClientiFornitori> list = (List<AnagraficheClientiFornitori>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_ANAGRAFICHECLIENTIFORNITORI);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_ANAGRAFICHECLIENTIFORNITORI;

                if (pagination) {
                    sql = sql.concat(AnagraficheClientiFornitoriModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<AnagraficheClientiFornitori>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<AnagraficheClientiFornitori>(list);
                } else {
                    list = (List<AnagraficheClientiFornitori>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the anagrafiche clienti fornitoris from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (AnagraficheClientiFornitori anagraficheClientiFornitori : findAll()) {
            remove(anagraficheClientiFornitori);
        }
    }

    /**
     * Returns the number of anagrafiche clienti fornitoris.
     *
     * @return the number of anagrafiche clienti fornitoris
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_ANAGRAFICHECLIENTIFORNITORI);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the anagrafiche clienti fornitori persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.AnagraficheClientiFornitori")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<AnagraficheClientiFornitori>> listenersList = new ArrayList<ModelListener<AnagraficheClientiFornitori>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<AnagraficheClientiFornitori>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(AnagraficheClientiFornitoriImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
