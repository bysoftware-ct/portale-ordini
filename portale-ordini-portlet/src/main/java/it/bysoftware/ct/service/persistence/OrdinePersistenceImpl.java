package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.CalendarUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchOrdineException;
import it.bysoftware.ct.model.Ordine;
import it.bysoftware.ct.model.impl.OrdineImpl;
import it.bysoftware.ct.model.impl.OrdineModelImpl;
import it.bysoftware.ct.service.persistence.OrdinePersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the ordine service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see OrdinePersistence
 * @see OrdineUtil
 * @generated
 */
public class OrdinePersistenceImpl extends BasePersistenceImpl<Ordine>
    implements OrdinePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link OrdineUtil} to access the ordine persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = OrdineImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, OrdineImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, OrdineImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICECLIENTE =
        new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, OrdineImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCodiceCliente",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTE =
        new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, OrdineImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCodiceCliente",
            new String[] { String.class.getName() },
            OrdineModelImpl.IDCLIENTE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_CODICECLIENTE = new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCodiceCliente",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_CODICECLIENTE_IDCLIENTE_1 = "ordine.idCliente IS NULL";
    private static final String _FINDER_COLUMN_CODICECLIENTE_IDCLIENTE_2 = "ordine.idCliente = ?";
    private static final String _FINDER_COLUMN_CODICECLIENTE_IDCLIENTE_3 = "(ordine.idCliente IS NULL OR ordine.idCliente = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STATO = new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, OrdineImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByStato",
            new String[] {
                Integer.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATO = new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, OrdineImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByStato",
            new String[] { Integer.class.getName() },
            OrdineModelImpl.STATO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_STATO = new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByStato",
            new String[] { Integer.class.getName() });
    private static final String _FINDER_COLUMN_STATO_STATO_2 = "ordine.stato = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CONSEGNASTATO =
        new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, OrdineImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByConsegnaStato",
            new String[] {
                Date.class.getName(), Integer.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CONSEGNASTATO =
        new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, OrdineImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByConsegnaStato",
            new String[] { Date.class.getName(), Integer.class.getName() },
            OrdineModelImpl.DATACONSEGNA_COLUMN_BITMASK |
            OrdineModelImpl.STATO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_CONSEGNASTATO = new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByConsegnaStato",
            new String[] { Date.class.getName(), Integer.class.getName() });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_CONSEGNASTATO =
        new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByConsegnaStato",
            new String[] { Date.class.getName(), Integer.class.getName() });
    private static final String _FINDER_COLUMN_CONSEGNASTATO_DATACONSEGNA_1 = "ordine.dataConsegna IS NULL AND ";
    private static final String _FINDER_COLUMN_CONSEGNASTATO_DATACONSEGNA_2 = "ordine.dataConsegna = ? AND ";
    private static final String _FINDER_COLUMN_CONSEGNASTATO_DATACONSEGNA_4 = "(" +
        removeConjunction(_FINDER_COLUMN_CONSEGNASTATO_DATACONSEGNA_1) + ")";
    private static final String _FINDER_COLUMN_CONSEGNASTATO_DATACONSEGNA_5 = "(" +
        removeConjunction(_FINDER_COLUMN_CONSEGNASTATO_DATACONSEGNA_2) + ")";
    private static final String _FINDER_COLUMN_CONSEGNASTATO_STATO_2 = "ordine.stato = ?";
    private static final String _FINDER_COLUMN_CONSEGNASTATO_STATO_5 = "(" +
        removeConjunction(_FINDER_COLUMN_CONSEGNASTATO_STATO_2) + ")";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICECLIENTESTATO =
        new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, OrdineImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCodiceClienteStato",
            new String[] {
                String.class.getName(), Integer.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTESTATO =
        new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, OrdineImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByCodiceClienteStato",
            new String[] { String.class.getName(), Integer.class.getName() },
            OrdineModelImpl.IDCLIENTE_COLUMN_BITMASK |
            OrdineModelImpl.STATO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_CODICECLIENTESTATO = new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByCodiceClienteStato",
            new String[] { String.class.getName(), Integer.class.getName() });
    private static final String _FINDER_COLUMN_CODICECLIENTESTATO_IDCLIENTE_1 = "ordine.idCliente IS NULL AND ";
    private static final String _FINDER_COLUMN_CODICECLIENTESTATO_IDCLIENTE_2 = "ordine.idCliente = ? AND ";
    private static final String _FINDER_COLUMN_CODICECLIENTESTATO_IDCLIENTE_3 = "(ordine.idCliente IS NULL OR ordine.idCliente = '') AND ";
    private static final String _FINDER_COLUMN_CODICECLIENTESTATO_STATO_2 = "ordine.stato = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_INSERIMENTOCONSEGNASTATO =
        new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, OrdineImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByInserimentoConsegnaStato",
            new String[] {
                Date.class.getName(), Date.class.getName(),
                Integer.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_INSERIMENTOCONSEGNASTATO =
        new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, OrdineImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByInserimentoConsegnaStato",
            new String[] {
                Date.class.getName(), Date.class.getName(),
                Integer.class.getName()
            },
            OrdineModelImpl.DATAINSERIMENTO_COLUMN_BITMASK |
            OrdineModelImpl.DATACONSEGNA_COLUMN_BITMASK |
            OrdineModelImpl.STATO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_INSERIMENTOCONSEGNASTATO =
        new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByInserimentoConsegnaStato",
            new String[] {
                Date.class.getName(), Date.class.getName(),
                Integer.class.getName()
            });
    private static final String _FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_DATAINSERIMENTO_1 =
        "ordine.dataInserimento IS NULL AND ";
    private static final String _FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_DATAINSERIMENTO_2 =
        "ordine.dataInserimento = ? AND ";
    private static final String _FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_DATACONSEGNA_1 =
        "ordine.dataConsegna IS NULL AND ";
    private static final String _FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_DATACONSEGNA_2 =
        "ordine.dataConsegna = ? AND ";
    private static final String _FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_STATO_2 = "ordine.stato = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ANNOCENTRO =
        new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, OrdineImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByAnnoCentro",
            new String[] {
                Integer.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOCENTRO =
        new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, OrdineImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByAnnoCentro",
            new String[] { Integer.class.getName(), String.class.getName() },
            OrdineModelImpl.ANNO_COLUMN_BITMASK |
            OrdineModelImpl.CENTRO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_ANNOCENTRO = new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByAnnoCentro",
            new String[] { Integer.class.getName(), String.class.getName() });
    private static final String _FINDER_COLUMN_ANNOCENTRO_ANNO_2 = "ordine.anno = ? AND ";
    private static final String _FINDER_COLUMN_ANNOCENTRO_CENTRO_1 = "ordine.centro IS NULL";
    private static final String _FINDER_COLUMN_ANNOCENTRO_CENTRO_2 = "ordine.centro = ?";
    private static final String _FINDER_COLUMN_ANNOCENTRO_CENTRO_3 = "(ordine.centro IS NULL OR ordine.centro = '')";
    public static final FinderPath FINDER_PATH_FETCH_BY_ANNONUMEROCENTRO = new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, OrdineImpl.class,
            FINDER_CLASS_NAME_ENTITY, "fetchByAnnoNumeroCentro",
            new String[] {
                Integer.class.getName(), Integer.class.getName(),
                String.class.getName()
            },
            OrdineModelImpl.ANNO_COLUMN_BITMASK |
            OrdineModelImpl.NUMERO_COLUMN_BITMASK |
            OrdineModelImpl.CENTRO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_ANNONUMEROCENTRO = new FinderPath(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByAnnoNumeroCentro",
            new String[] {
                Integer.class.getName(), Integer.class.getName(),
                String.class.getName()
            });
    private static final String _FINDER_COLUMN_ANNONUMEROCENTRO_ANNO_2 = "ordine.anno = ? AND ";
    private static final String _FINDER_COLUMN_ANNONUMEROCENTRO_NUMERO_2 = "ordine.numero = ? AND ";
    private static final String _FINDER_COLUMN_ANNONUMEROCENTRO_CENTRO_1 = "ordine.centro IS NULL";
    private static final String _FINDER_COLUMN_ANNONUMEROCENTRO_CENTRO_2 = "ordine.centro = ?";
    private static final String _FINDER_COLUMN_ANNONUMEROCENTRO_CENTRO_3 = "(ordine.centro IS NULL OR ordine.centro = '')";
    private static final String _SQL_SELECT_ORDINE = "SELECT ordine FROM Ordine ordine";
    private static final String _SQL_SELECT_ORDINE_WHERE = "SELECT ordine FROM Ordine ordine WHERE ";
    private static final String _SQL_COUNT_ORDINE = "SELECT COUNT(ordine) FROM Ordine ordine";
    private static final String _SQL_COUNT_ORDINE_WHERE = "SELECT COUNT(ordine) FROM Ordine ordine WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "ordine.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Ordine exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Ordine exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(OrdinePersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "dataInserimento", "idCliente", "dataConsegna",
                "idTrasportatore", "idTrasportatore2", "idAgente",
                "idTipoCarrello", "luogoCarico", "noteCliente", "codiceIva",
                "ddtGenerati", "stampaEtichette", "numOrdineGT", "dataOrdineGT"
            });
    private static Ordine _nullOrdine = new OrdineImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Ordine> toCacheModel() {
                return _nullOrdineCacheModel;
            }
        };

    private static CacheModel<Ordine> _nullOrdineCacheModel = new CacheModel<Ordine>() {
            @Override
            public Ordine toEntityModel() {
                return _nullOrdine;
            }
        };

    public OrdinePersistenceImpl() {
        setModelClass(Ordine.class);
    }

    /**
     * Returns all the ordines where idCliente = &#63;.
     *
     * @param idCliente the id cliente
     * @return the matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByCodiceCliente(String idCliente)
        throws SystemException {
        return findByCodiceCliente(idCliente, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the ordines where idCliente = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param idCliente the id cliente
     * @param start the lower bound of the range of ordines
     * @param end the upper bound of the range of ordines (not inclusive)
     * @return the range of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByCodiceCliente(String idCliente, int start, int end)
        throws SystemException {
        return findByCodiceCliente(idCliente, start, end, null);
    }

    /**
     * Returns an ordered range of all the ordines where idCliente = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param idCliente the id cliente
     * @param start the lower bound of the range of ordines
     * @param end the upper bound of the range of ordines (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByCodiceCliente(String idCliente, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTE;
            finderArgs = new Object[] { idCliente };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICECLIENTE;
            finderArgs = new Object[] { idCliente, start, end, orderByComparator };
        }

        List<Ordine> list = (List<Ordine>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Ordine ordine : list) {
                if (!Validator.equals(idCliente, ordine.getIdCliente())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_ORDINE_WHERE);

            boolean bindIdCliente = false;

            if (idCliente == null) {
                query.append(_FINDER_COLUMN_CODICECLIENTE_IDCLIENTE_1);
            } else if (idCliente.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICECLIENTE_IDCLIENTE_3);
            } else {
                bindIdCliente = true;

                query.append(_FINDER_COLUMN_CODICECLIENTE_IDCLIENTE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(OrdineModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindIdCliente) {
                    qPos.add(idCliente);
                }

                if (!pagination) {
                    list = (List<Ordine>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Ordine>(list);
                } else {
                    list = (List<Ordine>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first ordine in the ordered set where idCliente = &#63;.
     *
     * @param idCliente the id cliente
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine findByCodiceCliente_First(String idCliente,
        OrderByComparator orderByComparator)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = fetchByCodiceCliente_First(idCliente, orderByComparator);

        if (ordine != null) {
            return ordine;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("idCliente=");
        msg.append(idCliente);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchOrdineException(msg.toString());
    }

    /**
     * Returns the first ordine in the ordered set where idCliente = &#63;.
     *
     * @param idCliente the id cliente
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching ordine, or <code>null</code> if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine fetchByCodiceCliente_First(String idCliente,
        OrderByComparator orderByComparator) throws SystemException {
        List<Ordine> list = findByCodiceCliente(idCliente, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last ordine in the ordered set where idCliente = &#63;.
     *
     * @param idCliente the id cliente
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine findByCodiceCliente_Last(String idCliente,
        OrderByComparator orderByComparator)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = fetchByCodiceCliente_Last(idCliente, orderByComparator);

        if (ordine != null) {
            return ordine;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("idCliente=");
        msg.append(idCliente);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchOrdineException(msg.toString());
    }

    /**
     * Returns the last ordine in the ordered set where idCliente = &#63;.
     *
     * @param idCliente the id cliente
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching ordine, or <code>null</code> if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine fetchByCodiceCliente_Last(String idCliente,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByCodiceCliente(idCliente);

        if (count == 0) {
            return null;
        }

        List<Ordine> list = findByCodiceCliente(idCliente, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the ordines before and after the current ordine in the ordered set where idCliente = &#63;.
     *
     * @param id the primary key of the current ordine
     * @param idCliente the id cliente
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a ordine with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine[] findByCodiceCliente_PrevAndNext(long id, String idCliente,
        OrderByComparator orderByComparator)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Ordine[] array = new OrdineImpl[3];

            array[0] = getByCodiceCliente_PrevAndNext(session, ordine,
                    idCliente, orderByComparator, true);

            array[1] = ordine;

            array[2] = getByCodiceCliente_PrevAndNext(session, ordine,
                    idCliente, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Ordine getByCodiceCliente_PrevAndNext(Session session,
        Ordine ordine, String idCliente, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_ORDINE_WHERE);

        boolean bindIdCliente = false;

        if (idCliente == null) {
            query.append(_FINDER_COLUMN_CODICECLIENTE_IDCLIENTE_1);
        } else if (idCliente.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CODICECLIENTE_IDCLIENTE_3);
        } else {
            bindIdCliente = true;

            query.append(_FINDER_COLUMN_CODICECLIENTE_IDCLIENTE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(OrdineModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindIdCliente) {
            qPos.add(idCliente);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(ordine);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Ordine> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the ordines where idCliente = &#63; from the database.
     *
     * @param idCliente the id cliente
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByCodiceCliente(String idCliente)
        throws SystemException {
        for (Ordine ordine : findByCodiceCliente(idCliente, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(ordine);
        }
    }

    /**
     * Returns the number of ordines where idCliente = &#63;.
     *
     * @param idCliente the id cliente
     * @return the number of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByCodiceCliente(String idCliente) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_CODICECLIENTE;

        Object[] finderArgs = new Object[] { idCliente };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_ORDINE_WHERE);

            boolean bindIdCliente = false;

            if (idCliente == null) {
                query.append(_FINDER_COLUMN_CODICECLIENTE_IDCLIENTE_1);
            } else if (idCliente.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICECLIENTE_IDCLIENTE_3);
            } else {
                bindIdCliente = true;

                query.append(_FINDER_COLUMN_CODICECLIENTE_IDCLIENTE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindIdCliente) {
                    qPos.add(idCliente);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the ordines where stato = &#63;.
     *
     * @param stato the stato
     * @return the matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByStato(int stato) throws SystemException {
        return findByStato(stato, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the ordines where stato = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param stato the stato
     * @param start the lower bound of the range of ordines
     * @param end the upper bound of the range of ordines (not inclusive)
     * @return the range of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByStato(int stato, int start, int end)
        throws SystemException {
        return findByStato(stato, start, end, null);
    }

    /**
     * Returns an ordered range of all the ordines where stato = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param stato the stato
     * @param start the lower bound of the range of ordines
     * @param end the upper bound of the range of ordines (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByStato(int stato, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATO;
            finderArgs = new Object[] { stato };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STATO;
            finderArgs = new Object[] { stato, start, end, orderByComparator };
        }

        List<Ordine> list = (List<Ordine>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Ordine ordine : list) {
                if ((stato != ordine.getStato())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_ORDINE_WHERE);

            query.append(_FINDER_COLUMN_STATO_STATO_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(OrdineModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(stato);

                if (!pagination) {
                    list = (List<Ordine>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Ordine>(list);
                } else {
                    list = (List<Ordine>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first ordine in the ordered set where stato = &#63;.
     *
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine findByStato_First(int stato,
        OrderByComparator orderByComparator)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = fetchByStato_First(stato, orderByComparator);

        if (ordine != null) {
            return ordine;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("stato=");
        msg.append(stato);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchOrdineException(msg.toString());
    }

    /**
     * Returns the first ordine in the ordered set where stato = &#63;.
     *
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching ordine, or <code>null</code> if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine fetchByStato_First(int stato,
        OrderByComparator orderByComparator) throws SystemException {
        List<Ordine> list = findByStato(stato, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last ordine in the ordered set where stato = &#63;.
     *
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine findByStato_Last(int stato,
        OrderByComparator orderByComparator)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = fetchByStato_Last(stato, orderByComparator);

        if (ordine != null) {
            return ordine;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("stato=");
        msg.append(stato);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchOrdineException(msg.toString());
    }

    /**
     * Returns the last ordine in the ordered set where stato = &#63;.
     *
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching ordine, or <code>null</code> if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine fetchByStato_Last(int stato,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByStato(stato);

        if (count == 0) {
            return null;
        }

        List<Ordine> list = findByStato(stato, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the ordines before and after the current ordine in the ordered set where stato = &#63;.
     *
     * @param id the primary key of the current ordine
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a ordine with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine[] findByStato_PrevAndNext(long id, int stato,
        OrderByComparator orderByComparator)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Ordine[] array = new OrdineImpl[3];

            array[0] = getByStato_PrevAndNext(session, ordine, stato,
                    orderByComparator, true);

            array[1] = ordine;

            array[2] = getByStato_PrevAndNext(session, ordine, stato,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Ordine getByStato_PrevAndNext(Session session, Ordine ordine,
        int stato, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_ORDINE_WHERE);

        query.append(_FINDER_COLUMN_STATO_STATO_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(OrdineModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(stato);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(ordine);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Ordine> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the ordines where stato = &#63; from the database.
     *
     * @param stato the stato
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByStato(int stato) throws SystemException {
        for (Ordine ordine : findByStato(stato, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(ordine);
        }
    }

    /**
     * Returns the number of ordines where stato = &#63;.
     *
     * @param stato the stato
     * @return the number of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByStato(int stato) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_STATO;

        Object[] finderArgs = new Object[] { stato };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_ORDINE_WHERE);

            query.append(_FINDER_COLUMN_STATO_STATO_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(stato);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the ordines where dataConsegna = &#63; and stato = &#63;.
     *
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @return the matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByConsegnaStato(Date dataConsegna, int stato)
        throws SystemException {
        return findByConsegnaStato(dataConsegna, stato, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the ordines where dataConsegna = &#63; and stato = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @param start the lower bound of the range of ordines
     * @param end the upper bound of the range of ordines (not inclusive)
     * @return the range of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByConsegnaStato(Date dataConsegna, int stato,
        int start, int end) throws SystemException {
        return findByConsegnaStato(dataConsegna, stato, start, end, null);
    }

    /**
     * Returns an ordered range of all the ordines where dataConsegna = &#63; and stato = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @param start the lower bound of the range of ordines
     * @param end the upper bound of the range of ordines (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByConsegnaStato(Date dataConsegna, int stato,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CONSEGNASTATO;
            finderArgs = new Object[] { dataConsegna, stato };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CONSEGNASTATO;
            finderArgs = new Object[] {
                    dataConsegna, stato,
                    
                    start, end, orderByComparator
                };
        }

        List<Ordine> list = (List<Ordine>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Ordine ordine : list) {
                if (!Validator.equals(dataConsegna, ordine.getDataConsegna()) ||
                        (stato != ordine.getStato())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_ORDINE_WHERE);

            boolean bindDataConsegna = false;

            if (dataConsegna == null) {
                query.append(_FINDER_COLUMN_CONSEGNASTATO_DATACONSEGNA_1);
            } else {
                bindDataConsegna = true;

                query.append(_FINDER_COLUMN_CONSEGNASTATO_DATACONSEGNA_2);
            }

            query.append(_FINDER_COLUMN_CONSEGNASTATO_STATO_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(OrdineModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindDataConsegna) {
                    qPos.add(CalendarUtil.getTimestamp(dataConsegna));
                }

                qPos.add(stato);

                if (!pagination) {
                    list = (List<Ordine>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Ordine>(list);
                } else {
                    list = (List<Ordine>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first ordine in the ordered set where dataConsegna = &#63; and stato = &#63;.
     *
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine findByConsegnaStato_First(Date dataConsegna, int stato,
        OrderByComparator orderByComparator)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = fetchByConsegnaStato_First(dataConsegna, stato,
                orderByComparator);

        if (ordine != null) {
            return ordine;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("dataConsegna=");
        msg.append(dataConsegna);

        msg.append(", stato=");
        msg.append(stato);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchOrdineException(msg.toString());
    }

    /**
     * Returns the first ordine in the ordered set where dataConsegna = &#63; and stato = &#63;.
     *
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching ordine, or <code>null</code> if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine fetchByConsegnaStato_First(Date dataConsegna, int stato,
        OrderByComparator orderByComparator) throws SystemException {
        List<Ordine> list = findByConsegnaStato(dataConsegna, stato, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last ordine in the ordered set where dataConsegna = &#63; and stato = &#63;.
     *
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine findByConsegnaStato_Last(Date dataConsegna, int stato,
        OrderByComparator orderByComparator)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = fetchByConsegnaStato_Last(dataConsegna, stato,
                orderByComparator);

        if (ordine != null) {
            return ordine;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("dataConsegna=");
        msg.append(dataConsegna);

        msg.append(", stato=");
        msg.append(stato);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchOrdineException(msg.toString());
    }

    /**
     * Returns the last ordine in the ordered set where dataConsegna = &#63; and stato = &#63;.
     *
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching ordine, or <code>null</code> if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine fetchByConsegnaStato_Last(Date dataConsegna, int stato,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByConsegnaStato(dataConsegna, stato);

        if (count == 0) {
            return null;
        }

        List<Ordine> list = findByConsegnaStato(dataConsegna, stato, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the ordines before and after the current ordine in the ordered set where dataConsegna = &#63; and stato = &#63;.
     *
     * @param id the primary key of the current ordine
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a ordine with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine[] findByConsegnaStato_PrevAndNext(long id, Date dataConsegna,
        int stato, OrderByComparator orderByComparator)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Ordine[] array = new OrdineImpl[3];

            array[0] = getByConsegnaStato_PrevAndNext(session, ordine,
                    dataConsegna, stato, orderByComparator, true);

            array[1] = ordine;

            array[2] = getByConsegnaStato_PrevAndNext(session, ordine,
                    dataConsegna, stato, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Ordine getByConsegnaStato_PrevAndNext(Session session,
        Ordine ordine, Date dataConsegna, int stato,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_ORDINE_WHERE);

        boolean bindDataConsegna = false;

        if (dataConsegna == null) {
            query.append(_FINDER_COLUMN_CONSEGNASTATO_DATACONSEGNA_1);
        } else {
            bindDataConsegna = true;

            query.append(_FINDER_COLUMN_CONSEGNASTATO_DATACONSEGNA_2);
        }

        query.append(_FINDER_COLUMN_CONSEGNASTATO_STATO_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(OrdineModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindDataConsegna) {
            qPos.add(CalendarUtil.getTimestamp(dataConsegna));
        }

        qPos.add(stato);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(ordine);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Ordine> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Returns all the ordines where dataConsegna = &#63; and stato = any &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param dataConsegna the data consegna
     * @param statos the statos
     * @return the matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByConsegnaStato(Date dataConsegna, int[] statos)
        throws SystemException {
        return findByConsegnaStato(dataConsegna, statos, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the ordines where dataConsegna = &#63; and stato = any &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param dataConsegna the data consegna
     * @param statos the statos
     * @param start the lower bound of the range of ordines
     * @param end the upper bound of the range of ordines (not inclusive)
     * @return the range of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByConsegnaStato(Date dataConsegna, int[] statos,
        int start, int end) throws SystemException {
        return findByConsegnaStato(dataConsegna, statos, start, end, null);
    }

    /**
     * Returns an ordered range of all the ordines where dataConsegna = &#63; and stato = any &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param dataConsegna the data consegna
     * @param statos the statos
     * @param start the lower bound of the range of ordines
     * @param end the upper bound of the range of ordines (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByConsegnaStato(Date dataConsegna, int[] statos,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        if ((statos != null) && (statos.length == 1)) {
            return findByConsegnaStato(dataConsegna, statos[0], start, end,
                orderByComparator);
        }

        boolean pagination = true;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderArgs = new Object[] { dataConsegna, StringUtil.merge(statos) };
        } else {
            finderArgs = new Object[] {
                    dataConsegna, StringUtil.merge(statos),
                    
                    start, end, orderByComparator
                };
        }

        List<Ordine> list = (List<Ordine>) FinderCacheUtil.getResult(FINDER_PATH_WITH_PAGINATION_FIND_BY_CONSEGNASTATO,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Ordine ordine : list) {
                if (!Validator.equals(dataConsegna, ordine.getDataConsegna()) ||
                        !ArrayUtil.contains(statos, ordine.getStato())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = new StringBundler();

            query.append(_SQL_SELECT_ORDINE_WHERE);

            boolean conjunctionable = false;

            if (conjunctionable) {
                query.append(WHERE_AND);
            }

            boolean bindDataConsegna = false;

            if (dataConsegna == null) {
                query.append(_FINDER_COLUMN_CONSEGNASTATO_DATACONSEGNA_4);
            } else {
                bindDataConsegna = true;

                query.append(_FINDER_COLUMN_CONSEGNASTATO_DATACONSEGNA_5);
            }

            conjunctionable = true;

            if ((statos == null) || (statos.length > 0)) {
                if (conjunctionable) {
                    query.append(WHERE_AND);
                }

                query.append(StringPool.OPEN_PARENTHESIS);

                for (int i = 0; i < statos.length; i++) {
                    query.append(_FINDER_COLUMN_CONSEGNASTATO_STATO_5);

                    if ((i + 1) < statos.length) {
                        query.append(WHERE_OR);
                    }
                }

                query.append(StringPool.CLOSE_PARENTHESIS);

                conjunctionable = true;
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(OrdineModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindDataConsegna) {
                    qPos.add(CalendarUtil.getTimestamp(dataConsegna));
                }

                if (statos != null) {
                    qPos.add(statos);
                }

                if (!pagination) {
                    list = (List<Ordine>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Ordine>(list);
                } else {
                    list = (List<Ordine>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_WITH_PAGINATION_FIND_BY_CONSEGNASTATO,
                    finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_WITH_PAGINATION_FIND_BY_CONSEGNASTATO,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the ordines where dataConsegna = &#63; and stato = &#63; from the database.
     *
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByConsegnaStato(Date dataConsegna, int stato)
        throws SystemException {
        for (Ordine ordine : findByConsegnaStato(dataConsegna, stato,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(ordine);
        }
    }

    /**
     * Returns the number of ordines where dataConsegna = &#63; and stato = &#63;.
     *
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @return the number of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByConsegnaStato(Date dataConsegna, int stato)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_CONSEGNASTATO;

        Object[] finderArgs = new Object[] { dataConsegna, stato };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_ORDINE_WHERE);

            boolean bindDataConsegna = false;

            if (dataConsegna == null) {
                query.append(_FINDER_COLUMN_CONSEGNASTATO_DATACONSEGNA_1);
            } else {
                bindDataConsegna = true;

                query.append(_FINDER_COLUMN_CONSEGNASTATO_DATACONSEGNA_2);
            }

            query.append(_FINDER_COLUMN_CONSEGNASTATO_STATO_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindDataConsegna) {
                    qPos.add(CalendarUtil.getTimestamp(dataConsegna));
                }

                qPos.add(stato);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the number of ordines where dataConsegna = &#63; and stato = any &#63;.
     *
     * @param dataConsegna the data consegna
     * @param statos the statos
     * @return the number of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByConsegnaStato(Date dataConsegna, int[] statos)
        throws SystemException {
        Object[] finderArgs = new Object[] {
                dataConsegna, StringUtil.merge(statos)
            };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_WITH_PAGINATION_COUNT_BY_CONSEGNASTATO,
                finderArgs, this);

        if (count == null) {
            StringBundler query = new StringBundler();

            query.append(_SQL_COUNT_ORDINE_WHERE);

            boolean conjunctionable = false;

            if (conjunctionable) {
                query.append(WHERE_AND);
            }

            boolean bindDataConsegna = false;

            if (dataConsegna == null) {
                query.append(_FINDER_COLUMN_CONSEGNASTATO_DATACONSEGNA_4);
            } else {
                bindDataConsegna = true;

                query.append(_FINDER_COLUMN_CONSEGNASTATO_DATACONSEGNA_5);
            }

            conjunctionable = true;

            if ((statos == null) || (statos.length > 0)) {
                if (conjunctionable) {
                    query.append(WHERE_AND);
                }

                query.append(StringPool.OPEN_PARENTHESIS);

                for (int i = 0; i < statos.length; i++) {
                    query.append(_FINDER_COLUMN_CONSEGNASTATO_STATO_5);

                    if ((i + 1) < statos.length) {
                        query.append(WHERE_OR);
                    }
                }

                query.append(StringPool.CLOSE_PARENTHESIS);

                conjunctionable = true;
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindDataConsegna) {
                    qPos.add(CalendarUtil.getTimestamp(dataConsegna));
                }

                if (statos != null) {
                    qPos.add(statos);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_WITH_PAGINATION_COUNT_BY_CONSEGNASTATO,
                    finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_WITH_PAGINATION_COUNT_BY_CONSEGNASTATO,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the ordines where idCliente = &#63; and stato = &#63;.
     *
     * @param idCliente the id cliente
     * @param stato the stato
     * @return the matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByCodiceClienteStato(String idCliente, int stato)
        throws SystemException {
        return findByCodiceClienteStato(idCliente, stato, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the ordines where idCliente = &#63; and stato = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param idCliente the id cliente
     * @param stato the stato
     * @param start the lower bound of the range of ordines
     * @param end the upper bound of the range of ordines (not inclusive)
     * @return the range of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByCodiceClienteStato(String idCliente, int stato,
        int start, int end) throws SystemException {
        return findByCodiceClienteStato(idCliente, stato, start, end, null);
    }

    /**
     * Returns an ordered range of all the ordines where idCliente = &#63; and stato = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param idCliente the id cliente
     * @param stato the stato
     * @param start the lower bound of the range of ordines
     * @param end the upper bound of the range of ordines (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByCodiceClienteStato(String idCliente, int stato,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTESTATO;
            finderArgs = new Object[] { idCliente, stato };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICECLIENTESTATO;
            finderArgs = new Object[] {
                    idCliente, stato,
                    
                    start, end, orderByComparator
                };
        }

        List<Ordine> list = (List<Ordine>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Ordine ordine : list) {
                if (!Validator.equals(idCliente, ordine.getIdCliente()) ||
                        (stato != ordine.getStato())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_ORDINE_WHERE);

            boolean bindIdCliente = false;

            if (idCliente == null) {
                query.append(_FINDER_COLUMN_CODICECLIENTESTATO_IDCLIENTE_1);
            } else if (idCliente.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICECLIENTESTATO_IDCLIENTE_3);
            } else {
                bindIdCliente = true;

                query.append(_FINDER_COLUMN_CODICECLIENTESTATO_IDCLIENTE_2);
            }

            query.append(_FINDER_COLUMN_CODICECLIENTESTATO_STATO_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(OrdineModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindIdCliente) {
                    qPos.add(idCliente);
                }

                qPos.add(stato);

                if (!pagination) {
                    list = (List<Ordine>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Ordine>(list);
                } else {
                    list = (List<Ordine>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first ordine in the ordered set where idCliente = &#63; and stato = &#63;.
     *
     * @param idCliente the id cliente
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine findByCodiceClienteStato_First(String idCliente, int stato,
        OrderByComparator orderByComparator)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = fetchByCodiceClienteStato_First(idCliente, stato,
                orderByComparator);

        if (ordine != null) {
            return ordine;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("idCliente=");
        msg.append(idCliente);

        msg.append(", stato=");
        msg.append(stato);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchOrdineException(msg.toString());
    }

    /**
     * Returns the first ordine in the ordered set where idCliente = &#63; and stato = &#63;.
     *
     * @param idCliente the id cliente
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching ordine, or <code>null</code> if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine fetchByCodiceClienteStato_First(String idCliente, int stato,
        OrderByComparator orderByComparator) throws SystemException {
        List<Ordine> list = findByCodiceClienteStato(idCliente, stato, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last ordine in the ordered set where idCliente = &#63; and stato = &#63;.
     *
     * @param idCliente the id cliente
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine findByCodiceClienteStato_Last(String idCliente, int stato,
        OrderByComparator orderByComparator)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = fetchByCodiceClienteStato_Last(idCliente, stato,
                orderByComparator);

        if (ordine != null) {
            return ordine;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("idCliente=");
        msg.append(idCliente);

        msg.append(", stato=");
        msg.append(stato);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchOrdineException(msg.toString());
    }

    /**
     * Returns the last ordine in the ordered set where idCliente = &#63; and stato = &#63;.
     *
     * @param idCliente the id cliente
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching ordine, or <code>null</code> if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine fetchByCodiceClienteStato_Last(String idCliente, int stato,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByCodiceClienteStato(idCliente, stato);

        if (count == 0) {
            return null;
        }

        List<Ordine> list = findByCodiceClienteStato(idCliente, stato,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the ordines before and after the current ordine in the ordered set where idCliente = &#63; and stato = &#63;.
     *
     * @param id the primary key of the current ordine
     * @param idCliente the id cliente
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a ordine with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine[] findByCodiceClienteStato_PrevAndNext(long id,
        String idCliente, int stato, OrderByComparator orderByComparator)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Ordine[] array = new OrdineImpl[3];

            array[0] = getByCodiceClienteStato_PrevAndNext(session, ordine,
                    idCliente, stato, orderByComparator, true);

            array[1] = ordine;

            array[2] = getByCodiceClienteStato_PrevAndNext(session, ordine,
                    idCliente, stato, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Ordine getByCodiceClienteStato_PrevAndNext(Session session,
        Ordine ordine, String idCliente, int stato,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_ORDINE_WHERE);

        boolean bindIdCliente = false;

        if (idCliente == null) {
            query.append(_FINDER_COLUMN_CODICECLIENTESTATO_IDCLIENTE_1);
        } else if (idCliente.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CODICECLIENTESTATO_IDCLIENTE_3);
        } else {
            bindIdCliente = true;

            query.append(_FINDER_COLUMN_CODICECLIENTESTATO_IDCLIENTE_2);
        }

        query.append(_FINDER_COLUMN_CODICECLIENTESTATO_STATO_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(OrdineModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindIdCliente) {
            qPos.add(idCliente);
        }

        qPos.add(stato);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(ordine);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Ordine> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the ordines where idCliente = &#63; and stato = &#63; from the database.
     *
     * @param idCliente the id cliente
     * @param stato the stato
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByCodiceClienteStato(String idCliente, int stato)
        throws SystemException {
        for (Ordine ordine : findByCodiceClienteStato(idCliente, stato,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(ordine);
        }
    }

    /**
     * Returns the number of ordines where idCliente = &#63; and stato = &#63;.
     *
     * @param idCliente the id cliente
     * @param stato the stato
     * @return the number of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByCodiceClienteStato(String idCliente, int stato)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_CODICECLIENTESTATO;

        Object[] finderArgs = new Object[] { idCliente, stato };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_ORDINE_WHERE);

            boolean bindIdCliente = false;

            if (idCliente == null) {
                query.append(_FINDER_COLUMN_CODICECLIENTESTATO_IDCLIENTE_1);
            } else if (idCliente.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICECLIENTESTATO_IDCLIENTE_3);
            } else {
                bindIdCliente = true;

                query.append(_FINDER_COLUMN_CODICECLIENTESTATO_IDCLIENTE_2);
            }

            query.append(_FINDER_COLUMN_CODICECLIENTESTATO_STATO_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindIdCliente) {
                    qPos.add(idCliente);
                }

                qPos.add(stato);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the ordines where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63;.
     *
     * @param dataInserimento the data inserimento
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @return the matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByInserimentoConsegnaStato(Date dataInserimento,
        Date dataConsegna, int stato) throws SystemException {
        return findByInserimentoConsegnaStato(dataInserimento, dataConsegna,
            stato, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the ordines where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param dataInserimento the data inserimento
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @param start the lower bound of the range of ordines
     * @param end the upper bound of the range of ordines (not inclusive)
     * @return the range of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByInserimentoConsegnaStato(Date dataInserimento,
        Date dataConsegna, int stato, int start, int end)
        throws SystemException {
        return findByInserimentoConsegnaStato(dataInserimento, dataConsegna,
            stato, start, end, null);
    }

    /**
     * Returns an ordered range of all the ordines where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param dataInserimento the data inserimento
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @param start the lower bound of the range of ordines
     * @param end the upper bound of the range of ordines (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByInserimentoConsegnaStato(Date dataInserimento,
        Date dataConsegna, int stato, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_INSERIMENTOCONSEGNASTATO;
            finderArgs = new Object[] { dataInserimento, dataConsegna, stato };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_INSERIMENTOCONSEGNASTATO;
            finderArgs = new Object[] {
                    dataInserimento, dataConsegna, stato,
                    
                    start, end, orderByComparator
                };
        }

        List<Ordine> list = (List<Ordine>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Ordine ordine : list) {
                if (!Validator.equals(dataInserimento,
                            ordine.getDataInserimento()) ||
                        !Validator.equals(dataConsegna, ordine.getDataConsegna()) ||
                        (stato != ordine.getStato())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(5 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(5);
            }

            query.append(_SQL_SELECT_ORDINE_WHERE);

            boolean bindDataInserimento = false;

            if (dataInserimento == null) {
                query.append(_FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_DATAINSERIMENTO_1);
            } else {
                bindDataInserimento = true;

                query.append(_FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_DATAINSERIMENTO_2);
            }

            boolean bindDataConsegna = false;

            if (dataConsegna == null) {
                query.append(_FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_DATACONSEGNA_1);
            } else {
                bindDataConsegna = true;

                query.append(_FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_DATACONSEGNA_2);
            }

            query.append(_FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_STATO_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(OrdineModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindDataInserimento) {
                    qPos.add(CalendarUtil.getTimestamp(dataInserimento));
                }

                if (bindDataConsegna) {
                    qPos.add(CalendarUtil.getTimestamp(dataConsegna));
                }

                qPos.add(stato);

                if (!pagination) {
                    list = (List<Ordine>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Ordine>(list);
                } else {
                    list = (List<Ordine>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first ordine in the ordered set where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63;.
     *
     * @param dataInserimento the data inserimento
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine findByInserimentoConsegnaStato_First(Date dataInserimento,
        Date dataConsegna, int stato, OrderByComparator orderByComparator)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = fetchByInserimentoConsegnaStato_First(dataInserimento,
                dataConsegna, stato, orderByComparator);

        if (ordine != null) {
            return ordine;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("dataInserimento=");
        msg.append(dataInserimento);

        msg.append(", dataConsegna=");
        msg.append(dataConsegna);

        msg.append(", stato=");
        msg.append(stato);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchOrdineException(msg.toString());
    }

    /**
     * Returns the first ordine in the ordered set where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63;.
     *
     * @param dataInserimento the data inserimento
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching ordine, or <code>null</code> if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine fetchByInserimentoConsegnaStato_First(Date dataInserimento,
        Date dataConsegna, int stato, OrderByComparator orderByComparator)
        throws SystemException {
        List<Ordine> list = findByInserimentoConsegnaStato(dataInserimento,
                dataConsegna, stato, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last ordine in the ordered set where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63;.
     *
     * @param dataInserimento the data inserimento
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine findByInserimentoConsegnaStato_Last(Date dataInserimento,
        Date dataConsegna, int stato, OrderByComparator orderByComparator)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = fetchByInserimentoConsegnaStato_Last(dataInserimento,
                dataConsegna, stato, orderByComparator);

        if (ordine != null) {
            return ordine;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("dataInserimento=");
        msg.append(dataInserimento);

        msg.append(", dataConsegna=");
        msg.append(dataConsegna);

        msg.append(", stato=");
        msg.append(stato);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchOrdineException(msg.toString());
    }

    /**
     * Returns the last ordine in the ordered set where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63;.
     *
     * @param dataInserimento the data inserimento
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching ordine, or <code>null</code> if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine fetchByInserimentoConsegnaStato_Last(Date dataInserimento,
        Date dataConsegna, int stato, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByInserimentoConsegnaStato(dataInserimento,
                dataConsegna, stato);

        if (count == 0) {
            return null;
        }

        List<Ordine> list = findByInserimentoConsegnaStato(dataInserimento,
                dataConsegna, stato, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the ordines before and after the current ordine in the ordered set where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63;.
     *
     * @param id the primary key of the current ordine
     * @param dataInserimento the data inserimento
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a ordine with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine[] findByInserimentoConsegnaStato_PrevAndNext(long id,
        Date dataInserimento, Date dataConsegna, int stato,
        OrderByComparator orderByComparator)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Ordine[] array = new OrdineImpl[3];

            array[0] = getByInserimentoConsegnaStato_PrevAndNext(session,
                    ordine, dataInserimento, dataConsegna, stato,
                    orderByComparator, true);

            array[1] = ordine;

            array[2] = getByInserimentoConsegnaStato_PrevAndNext(session,
                    ordine, dataInserimento, dataConsegna, stato,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Ordine getByInserimentoConsegnaStato_PrevAndNext(
        Session session, Ordine ordine, Date dataInserimento,
        Date dataConsegna, int stato, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_ORDINE_WHERE);

        boolean bindDataInserimento = false;

        if (dataInserimento == null) {
            query.append(_FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_DATAINSERIMENTO_1);
        } else {
            bindDataInserimento = true;

            query.append(_FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_DATAINSERIMENTO_2);
        }

        boolean bindDataConsegna = false;

        if (dataConsegna == null) {
            query.append(_FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_DATACONSEGNA_1);
        } else {
            bindDataConsegna = true;

            query.append(_FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_DATACONSEGNA_2);
        }

        query.append(_FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_STATO_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(OrdineModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindDataInserimento) {
            qPos.add(CalendarUtil.getTimestamp(dataInserimento));
        }

        if (bindDataConsegna) {
            qPos.add(CalendarUtil.getTimestamp(dataConsegna));
        }

        qPos.add(stato);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(ordine);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Ordine> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the ordines where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63; from the database.
     *
     * @param dataInserimento the data inserimento
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByInserimentoConsegnaStato(Date dataInserimento,
        Date dataConsegna, int stato) throws SystemException {
        for (Ordine ordine : findByInserimentoConsegnaStato(dataInserimento,
                dataConsegna, stato, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(ordine);
        }
    }

    /**
     * Returns the number of ordines where dataInserimento = &#63; and dataConsegna = &#63; and stato = &#63;.
     *
     * @param dataInserimento the data inserimento
     * @param dataConsegna the data consegna
     * @param stato the stato
     * @return the number of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByInserimentoConsegnaStato(Date dataInserimento,
        Date dataConsegna, int stato) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_INSERIMENTOCONSEGNASTATO;

        Object[] finderArgs = new Object[] { dataInserimento, dataConsegna, stato };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(4);

            query.append(_SQL_COUNT_ORDINE_WHERE);

            boolean bindDataInserimento = false;

            if (dataInserimento == null) {
                query.append(_FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_DATAINSERIMENTO_1);
            } else {
                bindDataInserimento = true;

                query.append(_FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_DATAINSERIMENTO_2);
            }

            boolean bindDataConsegna = false;

            if (dataConsegna == null) {
                query.append(_FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_DATACONSEGNA_1);
            } else {
                bindDataConsegna = true;

                query.append(_FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_DATACONSEGNA_2);
            }

            query.append(_FINDER_COLUMN_INSERIMENTOCONSEGNASTATO_STATO_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindDataInserimento) {
                    qPos.add(CalendarUtil.getTimestamp(dataInserimento));
                }

                if (bindDataConsegna) {
                    qPos.add(CalendarUtil.getTimestamp(dataConsegna));
                }

                qPos.add(stato);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the ordines where anno = &#63; and centro = &#63;.
     *
     * @param anno the anno
     * @param centro the centro
     * @return the matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByAnnoCentro(int anno, String centro)
        throws SystemException {
        return findByAnnoCentro(anno, centro, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the ordines where anno = &#63; and centro = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param anno the anno
     * @param centro the centro
     * @param start the lower bound of the range of ordines
     * @param end the upper bound of the range of ordines (not inclusive)
     * @return the range of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByAnnoCentro(int anno, String centro, int start,
        int end) throws SystemException {
        return findByAnnoCentro(anno, centro, start, end, null);
    }

    /**
     * Returns an ordered range of all the ordines where anno = &#63; and centro = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param anno the anno
     * @param centro the centro
     * @param start the lower bound of the range of ordines
     * @param end the upper bound of the range of ordines (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findByAnnoCentro(int anno, String centro, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOCENTRO;
            finderArgs = new Object[] { anno, centro };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ANNOCENTRO;
            finderArgs = new Object[] {
                    anno, centro,
                    
                    start, end, orderByComparator
                };
        }

        List<Ordine> list = (List<Ordine>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Ordine ordine : list) {
                if ((anno != ordine.getAnno()) ||
                        !Validator.equals(centro, ordine.getCentro())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_ORDINE_WHERE);

            query.append(_FINDER_COLUMN_ANNOCENTRO_ANNO_2);

            boolean bindCentro = false;

            if (centro == null) {
                query.append(_FINDER_COLUMN_ANNOCENTRO_CENTRO_1);
            } else if (centro.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ANNOCENTRO_CENTRO_3);
            } else {
                bindCentro = true;

                query.append(_FINDER_COLUMN_ANNOCENTRO_CENTRO_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(OrdineModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(anno);

                if (bindCentro) {
                    qPos.add(centro);
                }

                if (!pagination) {
                    list = (List<Ordine>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Ordine>(list);
                } else {
                    list = (List<Ordine>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first ordine in the ordered set where anno = &#63; and centro = &#63;.
     *
     * @param anno the anno
     * @param centro the centro
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine findByAnnoCentro_First(int anno, String centro,
        OrderByComparator orderByComparator)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = fetchByAnnoCentro_First(anno, centro, orderByComparator);

        if (ordine != null) {
            return ordine;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("anno=");
        msg.append(anno);

        msg.append(", centro=");
        msg.append(centro);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchOrdineException(msg.toString());
    }

    /**
     * Returns the first ordine in the ordered set where anno = &#63; and centro = &#63;.
     *
     * @param anno the anno
     * @param centro the centro
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching ordine, or <code>null</code> if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine fetchByAnnoCentro_First(int anno, String centro,
        OrderByComparator orderByComparator) throws SystemException {
        List<Ordine> list = findByAnnoCentro(anno, centro, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last ordine in the ordered set where anno = &#63; and centro = &#63;.
     *
     * @param anno the anno
     * @param centro the centro
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine findByAnnoCentro_Last(int anno, String centro,
        OrderByComparator orderByComparator)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = fetchByAnnoCentro_Last(anno, centro, orderByComparator);

        if (ordine != null) {
            return ordine;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("anno=");
        msg.append(anno);

        msg.append(", centro=");
        msg.append(centro);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchOrdineException(msg.toString());
    }

    /**
     * Returns the last ordine in the ordered set where anno = &#63; and centro = &#63;.
     *
     * @param anno the anno
     * @param centro the centro
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching ordine, or <code>null</code> if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine fetchByAnnoCentro_Last(int anno, String centro,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByAnnoCentro(anno, centro);

        if (count == 0) {
            return null;
        }

        List<Ordine> list = findByAnnoCentro(anno, centro, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the ordines before and after the current ordine in the ordered set where anno = &#63; and centro = &#63;.
     *
     * @param id the primary key of the current ordine
     * @param anno the anno
     * @param centro the centro
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a ordine with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine[] findByAnnoCentro_PrevAndNext(long id, int anno,
        String centro, OrderByComparator orderByComparator)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Ordine[] array = new OrdineImpl[3];

            array[0] = getByAnnoCentro_PrevAndNext(session, ordine, anno,
                    centro, orderByComparator, true);

            array[1] = ordine;

            array[2] = getByAnnoCentro_PrevAndNext(session, ordine, anno,
                    centro, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Ordine getByAnnoCentro_PrevAndNext(Session session,
        Ordine ordine, int anno, String centro,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_ORDINE_WHERE);

        query.append(_FINDER_COLUMN_ANNOCENTRO_ANNO_2);

        boolean bindCentro = false;

        if (centro == null) {
            query.append(_FINDER_COLUMN_ANNOCENTRO_CENTRO_1);
        } else if (centro.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_ANNOCENTRO_CENTRO_3);
        } else {
            bindCentro = true;

            query.append(_FINDER_COLUMN_ANNOCENTRO_CENTRO_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(OrdineModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(anno);

        if (bindCentro) {
            qPos.add(centro);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(ordine);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Ordine> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the ordines where anno = &#63; and centro = &#63; from the database.
     *
     * @param anno the anno
     * @param centro the centro
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByAnnoCentro(int anno, String centro)
        throws SystemException {
        for (Ordine ordine : findByAnnoCentro(anno, centro, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(ordine);
        }
    }

    /**
     * Returns the number of ordines where anno = &#63; and centro = &#63;.
     *
     * @param anno the anno
     * @param centro the centro
     * @return the number of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByAnnoCentro(int anno, String centro)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_ANNOCENTRO;

        Object[] finderArgs = new Object[] { anno, centro };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_ORDINE_WHERE);

            query.append(_FINDER_COLUMN_ANNOCENTRO_ANNO_2);

            boolean bindCentro = false;

            if (centro == null) {
                query.append(_FINDER_COLUMN_ANNOCENTRO_CENTRO_1);
            } else if (centro.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ANNOCENTRO_CENTRO_3);
            } else {
                bindCentro = true;

                query.append(_FINDER_COLUMN_ANNOCENTRO_CENTRO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(anno);

                if (bindCentro) {
                    qPos.add(centro);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the ordine where anno = &#63; and numero = &#63; and centro = &#63; or throws a {@link it.bysoftware.ct.NoSuchOrdineException} if it could not be found.
     *
     * @param anno the anno
     * @param numero the numero
     * @param centro the centro
     * @return the matching ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine findByAnnoNumeroCentro(int anno, int numero, String centro)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = fetchByAnnoNumeroCentro(anno, numero, centro);

        if (ordine == null) {
            StringBundler msg = new StringBundler(8);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("anno=");
            msg.append(anno);

            msg.append(", numero=");
            msg.append(numero);

            msg.append(", centro=");
            msg.append(centro);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchOrdineException(msg.toString());
        }

        return ordine;
    }

    /**
     * Returns the ordine where anno = &#63; and numero = &#63; and centro = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param anno the anno
     * @param numero the numero
     * @param centro the centro
     * @return the matching ordine, or <code>null</code> if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine fetchByAnnoNumeroCentro(int anno, int numero, String centro)
        throws SystemException {
        return fetchByAnnoNumeroCentro(anno, numero, centro, true);
    }

    /**
     * Returns the ordine where anno = &#63; and numero = &#63; and centro = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param anno the anno
     * @param numero the numero
     * @param centro the centro
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching ordine, or <code>null</code> if a matching ordine could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine fetchByAnnoNumeroCentro(int anno, int numero, String centro,
        boolean retrieveFromCache) throws SystemException {
        Object[] finderArgs = new Object[] { anno, numero, centro };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_ANNONUMEROCENTRO,
                    finderArgs, this);
        }

        if (result instanceof Ordine) {
            Ordine ordine = (Ordine) result;

            if ((anno != ordine.getAnno()) || (numero != ordine.getNumero()) ||
                    !Validator.equals(centro, ordine.getCentro())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(5);

            query.append(_SQL_SELECT_ORDINE_WHERE);

            query.append(_FINDER_COLUMN_ANNONUMEROCENTRO_ANNO_2);

            query.append(_FINDER_COLUMN_ANNONUMEROCENTRO_NUMERO_2);

            boolean bindCentro = false;

            if (centro == null) {
                query.append(_FINDER_COLUMN_ANNONUMEROCENTRO_CENTRO_1);
            } else if (centro.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ANNONUMEROCENTRO_CENTRO_3);
            } else {
                bindCentro = true;

                query.append(_FINDER_COLUMN_ANNONUMEROCENTRO_CENTRO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(anno);

                qPos.add(numero);

                if (bindCentro) {
                    qPos.add(centro);
                }

                List<Ordine> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ANNONUMEROCENTRO,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "OrdinePersistenceImpl.fetchByAnnoNumeroCentro(int, int, String, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    Ordine ordine = list.get(0);

                    result = ordine;

                    cacheResult(ordine);

                    if ((ordine.getAnno() != anno) ||
                            (ordine.getNumero() != numero) ||
                            (ordine.getCentro() == null) ||
                            !ordine.getCentro().equals(centro)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ANNONUMEROCENTRO,
                            finderArgs, ordine);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ANNONUMEROCENTRO,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (Ordine) result;
        }
    }

    /**
     * Removes the ordine where anno = &#63; and numero = &#63; and centro = &#63; from the database.
     *
     * @param anno the anno
     * @param numero the numero
     * @param centro the centro
     * @return the ordine that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine removeByAnnoNumeroCentro(int anno, int numero, String centro)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = findByAnnoNumeroCentro(anno, numero, centro);

        return remove(ordine);
    }

    /**
     * Returns the number of ordines where anno = &#63; and numero = &#63; and centro = &#63;.
     *
     * @param anno the anno
     * @param numero the numero
     * @param centro the centro
     * @return the number of matching ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByAnnoNumeroCentro(int anno, int numero, String centro)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_ANNONUMEROCENTRO;

        Object[] finderArgs = new Object[] { anno, numero, centro };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(4);

            query.append(_SQL_COUNT_ORDINE_WHERE);

            query.append(_FINDER_COLUMN_ANNONUMEROCENTRO_ANNO_2);

            query.append(_FINDER_COLUMN_ANNONUMEROCENTRO_NUMERO_2);

            boolean bindCentro = false;

            if (centro == null) {
                query.append(_FINDER_COLUMN_ANNONUMEROCENTRO_CENTRO_1);
            } else if (centro.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ANNONUMEROCENTRO_CENTRO_3);
            } else {
                bindCentro = true;

                query.append(_FINDER_COLUMN_ANNONUMEROCENTRO_CENTRO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(anno);

                qPos.add(numero);

                if (bindCentro) {
                    qPos.add(centro);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the ordine in the entity cache if it is enabled.
     *
     * @param ordine the ordine
     */
    @Override
    public void cacheResult(Ordine ordine) {
        EntityCacheUtil.putResult(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineImpl.class, ordine.getPrimaryKey(), ordine);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ANNONUMEROCENTRO,
            new Object[] {
                ordine.getAnno(), ordine.getNumero(), ordine.getCentro()
            }, ordine);

        ordine.resetOriginalValues();
    }

    /**
     * Caches the ordines in the entity cache if it is enabled.
     *
     * @param ordines the ordines
     */
    @Override
    public void cacheResult(List<Ordine> ordines) {
        for (Ordine ordine : ordines) {
            if (EntityCacheUtil.getResult(
                        OrdineModelImpl.ENTITY_CACHE_ENABLED, OrdineImpl.class,
                        ordine.getPrimaryKey()) == null) {
                cacheResult(ordine);
            } else {
                ordine.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all ordines.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(OrdineImpl.class.getName());
        }

        EntityCacheUtil.clearCache(OrdineImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the ordine.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Ordine ordine) {
        EntityCacheUtil.removeResult(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineImpl.class, ordine.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        clearUniqueFindersCache(ordine);
    }

    @Override
    public void clearCache(List<Ordine> ordines) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Ordine ordine : ordines) {
            EntityCacheUtil.removeResult(OrdineModelImpl.ENTITY_CACHE_ENABLED,
                OrdineImpl.class, ordine.getPrimaryKey());

            clearUniqueFindersCache(ordine);
        }
    }

    protected void cacheUniqueFindersCache(Ordine ordine) {
        if (ordine.isNew()) {
            Object[] args = new Object[] {
                    ordine.getAnno(), ordine.getNumero(), ordine.getCentro()
                };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ANNONUMEROCENTRO,
                args, Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ANNONUMEROCENTRO,
                args, ordine);
        } else {
            OrdineModelImpl ordineModelImpl = (OrdineModelImpl) ordine;

            if ((ordineModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_ANNONUMEROCENTRO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        ordine.getAnno(), ordine.getNumero(), ordine.getCentro()
                    };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ANNONUMEROCENTRO,
                    args, Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ANNONUMEROCENTRO,
                    args, ordine);
            }
        }
    }

    protected void clearUniqueFindersCache(Ordine ordine) {
        OrdineModelImpl ordineModelImpl = (OrdineModelImpl) ordine;

        Object[] args = new Object[] {
                ordine.getAnno(), ordine.getNumero(), ordine.getCentro()
            };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ANNONUMEROCENTRO, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ANNONUMEROCENTRO, args);

        if ((ordineModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_ANNONUMEROCENTRO.getColumnBitmask()) != 0) {
            args = new Object[] {
                    ordineModelImpl.getOriginalAnno(),
                    ordineModelImpl.getOriginalNumero(),
                    ordineModelImpl.getOriginalCentro()
                };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ANNONUMEROCENTRO,
                args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ANNONUMEROCENTRO,
                args);
        }
    }

    /**
     * Creates a new ordine with the primary key. Does not add the ordine to the database.
     *
     * @param id the primary key for the new ordine
     * @return the new ordine
     */
    @Override
    public Ordine create(long id) {
        Ordine ordine = new OrdineImpl();

        ordine.setNew(true);
        ordine.setPrimaryKey(id);

        return ordine;
    }

    /**
     * Removes the ordine with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the ordine
     * @return the ordine that was removed
     * @throws it.bysoftware.ct.NoSuchOrdineException if a ordine with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine remove(long id) throws NoSuchOrdineException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the ordine with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the ordine
     * @return the ordine that was removed
     * @throws it.bysoftware.ct.NoSuchOrdineException if a ordine with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine remove(Serializable primaryKey)
        throws NoSuchOrdineException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Ordine ordine = (Ordine) session.get(OrdineImpl.class, primaryKey);

            if (ordine == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchOrdineException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(ordine);
        } catch (NoSuchOrdineException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Ordine removeImpl(Ordine ordine) throws SystemException {
        ordine = toUnwrappedModel(ordine);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(ordine)) {
                ordine = (Ordine) session.get(OrdineImpl.class,
                        ordine.getPrimaryKeyObj());
            }

            if (ordine != null) {
                session.delete(ordine);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (ordine != null) {
            clearCache(ordine);
        }

        return ordine;
    }

    @Override
    public Ordine updateImpl(it.bysoftware.ct.model.Ordine ordine)
        throws SystemException {
        ordine = toUnwrappedModel(ordine);

        boolean isNew = ordine.isNew();

        OrdineModelImpl ordineModelImpl = (OrdineModelImpl) ordine;

        Session session = null;

        try {
            session = openSession();

            if (ordine.isNew()) {
                session.save(ordine);

                ordine.setNew(false);
            } else {
                session.merge(ordine);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !OrdineModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((ordineModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        ordineModelImpl.getOriginalIdCliente()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICECLIENTE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTE,
                    args);

                args = new Object[] { ordineModelImpl.getIdCliente() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICECLIENTE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTE,
                    args);
            }

            if ((ordineModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { ordineModelImpl.getOriginalStato() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATO, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATO,
                    args);

                args = new Object[] { ordineModelImpl.getStato() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATO, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATO,
                    args);
            }

            if ((ordineModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CONSEGNASTATO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        ordineModelImpl.getOriginalDataConsegna(),
                        ordineModelImpl.getOriginalStato()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CONSEGNASTATO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CONSEGNASTATO,
                    args);

                args = new Object[] {
                        ordineModelImpl.getDataConsegna(),
                        ordineModelImpl.getStato()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CONSEGNASTATO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CONSEGNASTATO,
                    args);
            }

            if ((ordineModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTESTATO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        ordineModelImpl.getOriginalIdCliente(),
                        ordineModelImpl.getOriginalStato()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICECLIENTESTATO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTESTATO,
                    args);

                args = new Object[] {
                        ordineModelImpl.getIdCliente(),
                        ordineModelImpl.getStato()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICECLIENTESTATO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTESTATO,
                    args);
            }

            if ((ordineModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_INSERIMENTOCONSEGNASTATO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        ordineModelImpl.getOriginalDataInserimento(),
                        ordineModelImpl.getOriginalDataConsegna(),
                        ordineModelImpl.getOriginalStato()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_INSERIMENTOCONSEGNASTATO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_INSERIMENTOCONSEGNASTATO,
                    args);

                args = new Object[] {
                        ordineModelImpl.getDataInserimento(),
                        ordineModelImpl.getDataConsegna(),
                        ordineModelImpl.getStato()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_INSERIMENTOCONSEGNASTATO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_INSERIMENTOCONSEGNASTATO,
                    args);
            }

            if ((ordineModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOCENTRO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        ordineModelImpl.getOriginalAnno(),
                        ordineModelImpl.getOriginalCentro()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ANNOCENTRO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOCENTRO,
                    args);

                args = new Object[] {
                        ordineModelImpl.getAnno(), ordineModelImpl.getCentro()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ANNOCENTRO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOCENTRO,
                    args);
            }
        }

        EntityCacheUtil.putResult(OrdineModelImpl.ENTITY_CACHE_ENABLED,
            OrdineImpl.class, ordine.getPrimaryKey(), ordine);

        clearUniqueFindersCache(ordine);
        cacheUniqueFindersCache(ordine);

        return ordine;
    }

    protected Ordine toUnwrappedModel(Ordine ordine) {
        if (ordine instanceof OrdineImpl) {
            return ordine;
        }

        OrdineImpl ordineImpl = new OrdineImpl();

        ordineImpl.setNew(ordine.isNew());
        ordineImpl.setPrimaryKey(ordine.getPrimaryKey());

        ordineImpl.setId(ordine.getId());
        ordineImpl.setDataInserimento(ordine.getDataInserimento());
        ordineImpl.setIdCliente(ordine.getIdCliente());
        ordineImpl.setStato(ordine.getStato());
        ordineImpl.setImporto(ordine.getImporto());
        ordineImpl.setNote(ordine.getNote());
        ordineImpl.setDataConsegna(ordine.getDataConsegna());
        ordineImpl.setIdTrasportatore(ordine.getIdTrasportatore());
        ordineImpl.setIdTrasportatore2(ordine.getIdTrasportatore2());
        ordineImpl.setIdAgente(ordine.getIdAgente());
        ordineImpl.setIdTipoCarrello(ordine.getIdTipoCarrello());
        ordineImpl.setLuogoCarico(ordine.getLuogoCarico());
        ordineImpl.setNoteCliente(ordine.getNoteCliente());
        ordineImpl.setAnno(ordine.getAnno());
        ordineImpl.setNumero(ordine.getNumero());
        ordineImpl.setCentro(ordine.getCentro());
        ordineImpl.setModificato(ordine.isModificato());
        ordineImpl.setCodiceIva(ordine.getCodiceIva());
        ordineImpl.setCliente(ordine.getCliente());
        ordineImpl.setVettore(ordine.getVettore());
        ordineImpl.setPagamento(ordine.getPagamento());
        ordineImpl.setDdtGenerati(ordine.isDdtGenerati());
        ordineImpl.setStampaEtichette(ordine.isStampaEtichette());
        ordineImpl.setNumOrdineGT(ordine.getNumOrdineGT());
        ordineImpl.setDataOrdineGT(ordine.getDataOrdineGT());

        return ordineImpl;
    }

    /**
     * Returns the ordine with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the ordine
     * @return the ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a ordine with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine findByPrimaryKey(Serializable primaryKey)
        throws NoSuchOrdineException, SystemException {
        Ordine ordine = fetchByPrimaryKey(primaryKey);

        if (ordine == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchOrdineException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return ordine;
    }

    /**
     * Returns the ordine with the primary key or throws a {@link it.bysoftware.ct.NoSuchOrdineException} if it could not be found.
     *
     * @param id the primary key of the ordine
     * @return the ordine
     * @throws it.bysoftware.ct.NoSuchOrdineException if a ordine with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine findByPrimaryKey(long id)
        throws NoSuchOrdineException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the ordine with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the ordine
     * @return the ordine, or <code>null</code> if a ordine with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Ordine ordine = (Ordine) EntityCacheUtil.getResult(OrdineModelImpl.ENTITY_CACHE_ENABLED,
                OrdineImpl.class, primaryKey);

        if (ordine == _nullOrdine) {
            return null;
        }

        if (ordine == null) {
            Session session = null;

            try {
                session = openSession();

                ordine = (Ordine) session.get(OrdineImpl.class, primaryKey);

                if (ordine != null) {
                    cacheResult(ordine);
                } else {
                    EntityCacheUtil.putResult(OrdineModelImpl.ENTITY_CACHE_ENABLED,
                        OrdineImpl.class, primaryKey, _nullOrdine);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(OrdineModelImpl.ENTITY_CACHE_ENABLED,
                    OrdineImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return ordine;
    }

    /**
     * Returns the ordine with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the ordine
     * @return the ordine, or <code>null</code> if a ordine with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Ordine fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the ordines.
     *
     * @return the ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the ordines.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of ordines
     * @param end the upper bound of the range of ordines (not inclusive)
     * @return the range of ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the ordines.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdineModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of ordines
     * @param end the upper bound of the range of ordines (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Ordine> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Ordine> list = (List<Ordine>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_ORDINE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_ORDINE;

                if (pagination) {
                    sql = sql.concat(OrdineModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Ordine>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Ordine>(list);
                } else {
                    list = (List<Ordine>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the ordines from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Ordine ordine : findAll()) {
            remove(ordine);
        }
    }

    /**
     * Returns the number of ordines.
     *
     * @return the number of ordines
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_ORDINE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the ordine persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.Ordine")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Ordine>> listenersList = new ArrayList<ModelListener<Ordine>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Ordine>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(OrdineImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
