package it.bysoftware.ct.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.bysoftware.ct.model.VariantiPianta;
import it.bysoftware.ct.service.base.VariantiPiantaLocalServiceBaseImpl;

/**
 * The implementation of the varianti pianta local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods 
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.VariantiPiantaLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.VariantiPiantaLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.VariantiPiantaLocalServiceUtil
 */
public class VariantiPiantaLocalServiceImpl extends
        VariantiPiantaLocalServiceBaseImpl {

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(VarianteLocalServiceImpl.class);

    @Override
    public final List<VariantiPianta> findByIdPianta(final long idPianta) {
        try {
            return this.variantiPiantaPersistence.
                    findByVariantiPianta(idPianta);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            return new ArrayList<VariantiPianta>();
        }
    }
}
