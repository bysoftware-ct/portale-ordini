package it.bysoftware.ct.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;

import it.bysoftware.ct.model.RigoDocumento;
import it.bysoftware.ct.service.TestataDocumentoLocalServiceUtil;
import it.bysoftware.ct.service.base.RigoDocumentoLocalServiceBaseImpl;
import it.bysoftware.ct.service.persistence.TestataDocumentoPK;

/**
 * The implementation of the rigo documento local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.RigoDocumentoLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.RigoDocumentoLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.RigoDocumentoLocalServiceUtil
 */
public class RigoDocumentoLocalServiceImpl extends
        RigoDocumentoLocalServiceBaseImpl {
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(RigoDocumentoLocalServiceImpl.
            class);

    @Override
    public final List<RigoDocumento> findByHeaderItem(
            final TestataDocumentoPK headerPK) {
        List<RigoDocumento> result = new ArrayList<RigoDocumento>();
        try {
            result = this.rigoDocumentoPersistence.
                    findByTestataDocumento(headerPK.getAnno(),
                            headerPK.getCodiceAttivita(),
                            headerPK.getCodiceCentro(),
                            headerPK.getCodiceDeposito(),
                            headerPK.getProtocollo(),
                            headerPK.getTipoDocumento(),
                            headerPK.getCodiceFornitore());
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        return result;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final int getNumber(final TestataDocumentoPK headerPK) {

        int result = 0;

        DynamicQuery userQuery = DynamicQueryFactoryUtil.forClass(
                RigoDocumento.class, getClassLoader());
        userQuery.add(PropertyFactoryUtil.forName("primaryKey.anno").
                eq(headerPK.getAnno()));
        userQuery.add(PropertyFactoryUtil.forName("primaryKey.codiceAttivita").
                eq(headerPK.getCodiceAttivita()));
        userQuery.add(PropertyFactoryUtil.forName("primaryKey.codiceCentro").
                eq(headerPK.getCodiceCentro()));
        userQuery.add(PropertyFactoryUtil.forName("primaryKey.codiceDeposito").
                eq(headerPK.getCodiceDeposito()));
        userQuery.add(PropertyFactoryUtil.forName("primaryKey.codiceFornitore").
                eq(headerPK.getCodiceFornitore()));
        userQuery.add(PropertyFactoryUtil.forName("primaryKey.protocollo").
                eq(headerPK.getProtocollo()));
        userQuery.add(PropertyFactoryUtil.forName("primaryKey.tipoDocumento").
                eq(headerPK.getTipoDocumento()));
        Projection projection = PropertyFactoryUtil.forName(
                "primaryKey.rigo").max();
        userQuery.setProjection(projection);
        try {
            List<Object> list = TestataDocumentoLocalServiceUtil.
                    dynamicQuery(userQuery);

            if (Validator.isNumber(String.valueOf(list.get(0)))) {
                result = Integer.parseInt(String.valueOf(list.get(0)));
            } else {
                result = 0;
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        return result + 1;
    }
}
