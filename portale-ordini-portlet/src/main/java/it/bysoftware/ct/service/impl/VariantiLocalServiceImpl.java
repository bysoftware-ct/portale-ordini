package it.bysoftware.ct.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.bysoftware.ct.model.Varianti;
import it.bysoftware.ct.service.base.VariantiLocalServiceBaseImpl;

/**
 * The implementation of the varianti local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.bysoftware.ct.service.VariantiLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.VariantiLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.VariantiLocalServiceUtil
 */
public class VariantiLocalServiceImpl extends VariantiLocalServiceBaseImpl {
	/**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(VariantiLocalServiceImpl.class);
    
    @Override
    public final List<Varianti> findByVariantType(final String type) {

        List<Varianti> result = new ArrayList<Varianti>();

        try {
            result = this.variantiPersistence.findByTipoVariante(type);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        return result;
    }
}
