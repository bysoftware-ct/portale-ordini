package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.DescrizioniVariantiServiceBaseImpl;

/**
 * The implementation of the descrizioni varianti remote service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.DescrizioniVariantiService} interface.
 *
 * <p>This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.DescrizioniVariantiServiceBaseImpl
 * @see it.bysoftware.ct.service.DescrizioniVariantiServiceUtil
 */
public class DescrizioniVariantiServiceImpl extends
        DescrizioniVariantiServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.DescrizioniVariantiServiceUtil} to access the
     * descrizioni varianti remote service.
     */
}
