package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.SottocontiLocalServiceBaseImpl;

/**
 * The implementation of the sottoconti local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.SottocontiLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.SottocontiLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.SottocontiLocalServiceUtil
 */
public class SottocontiLocalServiceImpl extends SottocontiLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.SottocontiLocalServiceUtil} to access the
     * sottoconti local service.
     */
}
