package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchVuotoException;
import it.bysoftware.ct.model.Vuoto;
import it.bysoftware.ct.model.impl.VuotoImpl;
import it.bysoftware.ct.model.impl.VuotoModelImpl;
import it.bysoftware.ct.service.persistence.VuotoPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the vuoto service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see VuotoPersistence
 * @see VuotoUtil
 * @generated
 */
public class VuotoPersistenceImpl extends BasePersistenceImpl<Vuoto>
    implements VuotoPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link VuotoUtil} to access the vuoto persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = VuotoImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VuotoModelImpl.ENTITY_CACHE_ENABLED,
            VuotoModelImpl.FINDER_CACHE_ENABLED, VuotoImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VuotoModelImpl.ENTITY_CACHE_ENABLED,
            VuotoModelImpl.FINDER_CACHE_ENABLED, VuotoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VuotoModelImpl.ENTITY_CACHE_ENABLED,
            VuotoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_VUOTO = "SELECT vuoto FROM Vuoto vuoto";
    private static final String _SQL_COUNT_VUOTO = "SELECT COUNT(vuoto) FROM Vuoto vuoto";
    private static final String _ORDER_BY_ENTITY_ALIAS = "vuoto.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Vuoto exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(VuotoPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "codiceVuoto", "unitaMisura"
            });
    private static Vuoto _nullVuoto = new VuotoImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Vuoto> toCacheModel() {
                return _nullVuotoCacheModel;
            }
        };

    private static CacheModel<Vuoto> _nullVuotoCacheModel = new CacheModel<Vuoto>() {
            @Override
            public Vuoto toEntityModel() {
                return _nullVuoto;
            }
        };

    public VuotoPersistenceImpl() {
        setModelClass(Vuoto.class);
    }

    /**
     * Caches the vuoto in the entity cache if it is enabled.
     *
     * @param vuoto the vuoto
     */
    @Override
    public void cacheResult(Vuoto vuoto) {
        EntityCacheUtil.putResult(VuotoModelImpl.ENTITY_CACHE_ENABLED,
            VuotoImpl.class, vuoto.getPrimaryKey(), vuoto);

        vuoto.resetOriginalValues();
    }

    /**
     * Caches the vuotos in the entity cache if it is enabled.
     *
     * @param vuotos the vuotos
     */
    @Override
    public void cacheResult(List<Vuoto> vuotos) {
        for (Vuoto vuoto : vuotos) {
            if (EntityCacheUtil.getResult(VuotoModelImpl.ENTITY_CACHE_ENABLED,
                        VuotoImpl.class, vuoto.getPrimaryKey()) == null) {
                cacheResult(vuoto);
            } else {
                vuoto.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all vuotos.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(VuotoImpl.class.getName());
        }

        EntityCacheUtil.clearCache(VuotoImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the vuoto.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Vuoto vuoto) {
        EntityCacheUtil.removeResult(VuotoModelImpl.ENTITY_CACHE_ENABLED,
            VuotoImpl.class, vuoto.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Vuoto> vuotos) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Vuoto vuoto : vuotos) {
            EntityCacheUtil.removeResult(VuotoModelImpl.ENTITY_CACHE_ENABLED,
                VuotoImpl.class, vuoto.getPrimaryKey());
        }
    }

    /**
     * Creates a new vuoto with the primary key. Does not add the vuoto to the database.
     *
     * @param codiceVuoto the primary key for the new vuoto
     * @return the new vuoto
     */
    @Override
    public Vuoto create(String codiceVuoto) {
        Vuoto vuoto = new VuotoImpl();

        vuoto.setNew(true);
        vuoto.setPrimaryKey(codiceVuoto);

        return vuoto;
    }

    /**
     * Removes the vuoto with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param codiceVuoto the primary key of the vuoto
     * @return the vuoto that was removed
     * @throws it.bysoftware.ct.NoSuchVuotoException if a vuoto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vuoto remove(String codiceVuoto)
        throws NoSuchVuotoException, SystemException {
        return remove((Serializable) codiceVuoto);
    }

    /**
     * Removes the vuoto with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the vuoto
     * @return the vuoto that was removed
     * @throws it.bysoftware.ct.NoSuchVuotoException if a vuoto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vuoto remove(Serializable primaryKey)
        throws NoSuchVuotoException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Vuoto vuoto = (Vuoto) session.get(VuotoImpl.class, primaryKey);

            if (vuoto == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchVuotoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(vuoto);
        } catch (NoSuchVuotoException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Vuoto removeImpl(Vuoto vuoto) throws SystemException {
        vuoto = toUnwrappedModel(vuoto);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(vuoto)) {
                vuoto = (Vuoto) session.get(VuotoImpl.class,
                        vuoto.getPrimaryKeyObj());
            }

            if (vuoto != null) {
                session.delete(vuoto);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (vuoto != null) {
            clearCache(vuoto);
        }

        return vuoto;
    }

    @Override
    public Vuoto updateImpl(it.bysoftware.ct.model.Vuoto vuoto)
        throws SystemException {
        vuoto = toUnwrappedModel(vuoto);

        boolean isNew = vuoto.isNew();

        Session session = null;

        try {
            session = openSession();

            if (vuoto.isNew()) {
                session.save(vuoto);

                vuoto.setNew(false);
            } else {
                session.merge(vuoto);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(VuotoModelImpl.ENTITY_CACHE_ENABLED,
            VuotoImpl.class, vuoto.getPrimaryKey(), vuoto);

        return vuoto;
    }

    protected Vuoto toUnwrappedModel(Vuoto vuoto) {
        if (vuoto instanceof VuotoImpl) {
            return vuoto;
        }

        VuotoImpl vuotoImpl = new VuotoImpl();

        vuotoImpl.setNew(vuoto.isNew());
        vuotoImpl.setPrimaryKey(vuoto.getPrimaryKey());

        vuotoImpl.setCodiceVuoto(vuoto.getCodiceVuoto());
        vuotoImpl.setNome(vuoto.getNome());
        vuotoImpl.setDescrizione(vuoto.getDescrizione());
        vuotoImpl.setUnitaMisura(vuoto.getUnitaMisura());
        vuotoImpl.setNote(vuoto.getNote());

        return vuotoImpl;
    }

    /**
     * Returns the vuoto with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the vuoto
     * @return the vuoto
     * @throws it.bysoftware.ct.NoSuchVuotoException if a vuoto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vuoto findByPrimaryKey(Serializable primaryKey)
        throws NoSuchVuotoException, SystemException {
        Vuoto vuoto = fetchByPrimaryKey(primaryKey);

        if (vuoto == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchVuotoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return vuoto;
    }

    /**
     * Returns the vuoto with the primary key or throws a {@link it.bysoftware.ct.NoSuchVuotoException} if it could not be found.
     *
     * @param codiceVuoto the primary key of the vuoto
     * @return the vuoto
     * @throws it.bysoftware.ct.NoSuchVuotoException if a vuoto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vuoto findByPrimaryKey(String codiceVuoto)
        throws NoSuchVuotoException, SystemException {
        return findByPrimaryKey((Serializable) codiceVuoto);
    }

    /**
     * Returns the vuoto with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the vuoto
     * @return the vuoto, or <code>null</code> if a vuoto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vuoto fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Vuoto vuoto = (Vuoto) EntityCacheUtil.getResult(VuotoModelImpl.ENTITY_CACHE_ENABLED,
                VuotoImpl.class, primaryKey);

        if (vuoto == _nullVuoto) {
            return null;
        }

        if (vuoto == null) {
            Session session = null;

            try {
                session = openSession();

                vuoto = (Vuoto) session.get(VuotoImpl.class, primaryKey);

                if (vuoto != null) {
                    cacheResult(vuoto);
                } else {
                    EntityCacheUtil.putResult(VuotoModelImpl.ENTITY_CACHE_ENABLED,
                        VuotoImpl.class, primaryKey, _nullVuoto);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(VuotoModelImpl.ENTITY_CACHE_ENABLED,
                    VuotoImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return vuoto;
    }

    /**
     * Returns the vuoto with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param codiceVuoto the primary key of the vuoto
     * @return the vuoto, or <code>null</code> if a vuoto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vuoto fetchByPrimaryKey(String codiceVuoto)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) codiceVuoto);
    }

    /**
     * Returns all the vuotos.
     *
     * @return the vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vuoto> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vuotos.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vuotos
     * @param end the upper bound of the range of vuotos (not inclusive)
     * @return the range of vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vuoto> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the vuotos.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vuotos
     * @param end the upper bound of the range of vuotos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vuoto> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Vuoto> list = (List<Vuoto>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_VUOTO);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_VUOTO;

                if (pagination) {
                    sql = sql.concat(VuotoModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Vuoto>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Vuoto>(list);
                } else {
                    list = (List<Vuoto>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the vuotos from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Vuoto vuoto : findAll()) {
            remove(vuoto);
        }
    }

    /**
     * Returns the number of vuotos.
     *
     * @return the number of vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_VUOTO);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the vuoto persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.Vuoto")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Vuoto>> listenersList = new ArrayList<ModelListener<Vuoto>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Vuoto>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(VuotoImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
