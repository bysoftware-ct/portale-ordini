package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchCausaliContabiliException;
import it.bysoftware.ct.model.CausaliContabili;
import it.bysoftware.ct.model.impl.CausaliContabiliImpl;
import it.bysoftware.ct.model.impl.CausaliContabiliModelImpl;
import it.bysoftware.ct.service.persistence.CausaliContabiliPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the causali contabili service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CausaliContabiliPersistence
 * @see CausaliContabiliUtil
 * @generated
 */
public class CausaliContabiliPersistenceImpl extends BasePersistenceImpl<CausaliContabili>
    implements CausaliContabiliPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link CausaliContabiliUtil} to access the causali contabili persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = CausaliContabiliImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CausaliContabiliModelImpl.ENTITY_CACHE_ENABLED,
            CausaliContabiliModelImpl.FINDER_CACHE_ENABLED,
            CausaliContabiliImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CausaliContabiliModelImpl.ENTITY_CACHE_ENABLED,
            CausaliContabiliModelImpl.FINDER_CACHE_ENABLED,
            CausaliContabiliImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CausaliContabiliModelImpl.ENTITY_CACHE_ENABLED,
            CausaliContabiliModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_CAUSALICONTABILI = "SELECT causaliContabili FROM CausaliContabili causaliContabili";
    private static final String _SQL_COUNT_CAUSALICONTABILI = "SELECT COUNT(causaliContabili) FROM CausaliContabili causaliContabili";
    private static final String _ORDER_BY_ENTITY_ALIAS = "causaliContabili.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CausaliContabili exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(CausaliContabiliPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "codiceCausaleCont", "codiceCausaleEC", "descrizioneCausaleCont",
                "testDareAvere", "tipoSoggettoMovimentato"
            });
    private static CausaliContabili _nullCausaliContabili = new CausaliContabiliImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<CausaliContabili> toCacheModel() {
                return _nullCausaliContabiliCacheModel;
            }
        };

    private static CacheModel<CausaliContabili> _nullCausaliContabiliCacheModel = new CacheModel<CausaliContabili>() {
            @Override
            public CausaliContabili toEntityModel() {
                return _nullCausaliContabili;
            }
        };

    public CausaliContabiliPersistenceImpl() {
        setModelClass(CausaliContabili.class);
    }

    /**
     * Caches the causali contabili in the entity cache if it is enabled.
     *
     * @param causaliContabili the causali contabili
     */
    @Override
    public void cacheResult(CausaliContabili causaliContabili) {
        EntityCacheUtil.putResult(CausaliContabiliModelImpl.ENTITY_CACHE_ENABLED,
            CausaliContabiliImpl.class, causaliContabili.getPrimaryKey(),
            causaliContabili);

        causaliContabili.resetOriginalValues();
    }

    /**
     * Caches the causali contabilis in the entity cache if it is enabled.
     *
     * @param causaliContabilis the causali contabilis
     */
    @Override
    public void cacheResult(List<CausaliContabili> causaliContabilis) {
        for (CausaliContabili causaliContabili : causaliContabilis) {
            if (EntityCacheUtil.getResult(
                        CausaliContabiliModelImpl.ENTITY_CACHE_ENABLED,
                        CausaliContabiliImpl.class,
                        causaliContabili.getPrimaryKey()) == null) {
                cacheResult(causaliContabili);
            } else {
                causaliContabili.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all causali contabilis.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(CausaliContabiliImpl.class.getName());
        }

        EntityCacheUtil.clearCache(CausaliContabiliImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the causali contabili.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(CausaliContabili causaliContabili) {
        EntityCacheUtil.removeResult(CausaliContabiliModelImpl.ENTITY_CACHE_ENABLED,
            CausaliContabiliImpl.class, causaliContabili.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<CausaliContabili> causaliContabilis) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (CausaliContabili causaliContabili : causaliContabilis) {
            EntityCacheUtil.removeResult(CausaliContabiliModelImpl.ENTITY_CACHE_ENABLED,
                CausaliContabiliImpl.class, causaliContabili.getPrimaryKey());
        }
    }

    /**
     * Creates a new causali contabili with the primary key. Does not add the causali contabili to the database.
     *
     * @param codiceCausaleCont the primary key for the new causali contabili
     * @return the new causali contabili
     */
    @Override
    public CausaliContabili create(String codiceCausaleCont) {
        CausaliContabili causaliContabili = new CausaliContabiliImpl();

        causaliContabili.setNew(true);
        causaliContabili.setPrimaryKey(codiceCausaleCont);

        return causaliContabili;
    }

    /**
     * Removes the causali contabili with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param codiceCausaleCont the primary key of the causali contabili
     * @return the causali contabili that was removed
     * @throws it.bysoftware.ct.NoSuchCausaliContabiliException if a causali contabili with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CausaliContabili remove(String codiceCausaleCont)
        throws NoSuchCausaliContabiliException, SystemException {
        return remove((Serializable) codiceCausaleCont);
    }

    /**
     * Removes the causali contabili with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the causali contabili
     * @return the causali contabili that was removed
     * @throws it.bysoftware.ct.NoSuchCausaliContabiliException if a causali contabili with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CausaliContabili remove(Serializable primaryKey)
        throws NoSuchCausaliContabiliException, SystemException {
        Session session = null;

        try {
            session = openSession();

            CausaliContabili causaliContabili = (CausaliContabili) session.get(CausaliContabiliImpl.class,
                    primaryKey);

            if (causaliContabili == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchCausaliContabiliException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(causaliContabili);
        } catch (NoSuchCausaliContabiliException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected CausaliContabili removeImpl(CausaliContabili causaliContabili)
        throws SystemException {
        causaliContabili = toUnwrappedModel(causaliContabili);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(causaliContabili)) {
                causaliContabili = (CausaliContabili) session.get(CausaliContabiliImpl.class,
                        causaliContabili.getPrimaryKeyObj());
            }

            if (causaliContabili != null) {
                session.delete(causaliContabili);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (causaliContabili != null) {
            clearCache(causaliContabili);
        }

        return causaliContabili;
    }

    @Override
    public CausaliContabili updateImpl(
        it.bysoftware.ct.model.CausaliContabili causaliContabili)
        throws SystemException {
        causaliContabili = toUnwrappedModel(causaliContabili);

        boolean isNew = causaliContabili.isNew();

        Session session = null;

        try {
            session = openSession();

            if (causaliContabili.isNew()) {
                session.save(causaliContabili);

                causaliContabili.setNew(false);
            } else {
                session.merge(causaliContabili);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(CausaliContabiliModelImpl.ENTITY_CACHE_ENABLED,
            CausaliContabiliImpl.class, causaliContabili.getPrimaryKey(),
            causaliContabili);

        return causaliContabili;
    }

    protected CausaliContabili toUnwrappedModel(
        CausaliContabili causaliContabili) {
        if (causaliContabili instanceof CausaliContabiliImpl) {
            return causaliContabili;
        }

        CausaliContabiliImpl causaliContabiliImpl = new CausaliContabiliImpl();

        causaliContabiliImpl.setNew(causaliContabili.isNew());
        causaliContabiliImpl.setPrimaryKey(causaliContabili.getPrimaryKey());

        causaliContabiliImpl.setCodiceCausaleCont(causaliContabili.getCodiceCausaleCont());
        causaliContabiliImpl.setCodiceCausaleEC(causaliContabili.getCodiceCausaleEC());
        causaliContabiliImpl.setDescrizioneCausaleCont(causaliContabili.getDescrizioneCausaleCont());
        causaliContabiliImpl.setTestDareAvere(causaliContabili.getTestDareAvere());
        causaliContabiliImpl.setTipoSoggettoMovimentato(causaliContabili.getTipoSoggettoMovimentato());

        return causaliContabiliImpl;
    }

    /**
     * Returns the causali contabili with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the causali contabili
     * @return the causali contabili
     * @throws it.bysoftware.ct.NoSuchCausaliContabiliException if a causali contabili with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CausaliContabili findByPrimaryKey(Serializable primaryKey)
        throws NoSuchCausaliContabiliException, SystemException {
        CausaliContabili causaliContabili = fetchByPrimaryKey(primaryKey);

        if (causaliContabili == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchCausaliContabiliException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return causaliContabili;
    }

    /**
     * Returns the causali contabili with the primary key or throws a {@link it.bysoftware.ct.NoSuchCausaliContabiliException} if it could not be found.
     *
     * @param codiceCausaleCont the primary key of the causali contabili
     * @return the causali contabili
     * @throws it.bysoftware.ct.NoSuchCausaliContabiliException if a causali contabili with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CausaliContabili findByPrimaryKey(String codiceCausaleCont)
        throws NoSuchCausaliContabiliException, SystemException {
        return findByPrimaryKey((Serializable) codiceCausaleCont);
    }

    /**
     * Returns the causali contabili with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the causali contabili
     * @return the causali contabili, or <code>null</code> if a causali contabili with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CausaliContabili fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        CausaliContabili causaliContabili = (CausaliContabili) EntityCacheUtil.getResult(CausaliContabiliModelImpl.ENTITY_CACHE_ENABLED,
                CausaliContabiliImpl.class, primaryKey);

        if (causaliContabili == _nullCausaliContabili) {
            return null;
        }

        if (causaliContabili == null) {
            Session session = null;

            try {
                session = openSession();

                causaliContabili = (CausaliContabili) session.get(CausaliContabiliImpl.class,
                        primaryKey);

                if (causaliContabili != null) {
                    cacheResult(causaliContabili);
                } else {
                    EntityCacheUtil.putResult(CausaliContabiliModelImpl.ENTITY_CACHE_ENABLED,
                        CausaliContabiliImpl.class, primaryKey,
                        _nullCausaliContabili);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(CausaliContabiliModelImpl.ENTITY_CACHE_ENABLED,
                    CausaliContabiliImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return causaliContabili;
    }

    /**
     * Returns the causali contabili with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param codiceCausaleCont the primary key of the causali contabili
     * @return the causali contabili, or <code>null</code> if a causali contabili with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CausaliContabili fetchByPrimaryKey(String codiceCausaleCont)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) codiceCausaleCont);
    }

    /**
     * Returns all the causali contabilis.
     *
     * @return the causali contabilis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<CausaliContabili> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the causali contabilis.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CausaliContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of causali contabilis
     * @param end the upper bound of the range of causali contabilis (not inclusive)
     * @return the range of causali contabilis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<CausaliContabili> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the causali contabilis.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CausaliContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of causali contabilis
     * @param end the upper bound of the range of causali contabilis (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of causali contabilis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<CausaliContabili> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<CausaliContabili> list = (List<CausaliContabili>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_CAUSALICONTABILI);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_CAUSALICONTABILI;

                if (pagination) {
                    sql = sql.concat(CausaliContabiliModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<CausaliContabili>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<CausaliContabili>(list);
                } else {
                    list = (List<CausaliContabili>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the causali contabilis from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (CausaliContabili causaliContabili : findAll()) {
            remove(causaliContabili);
        }
    }

    /**
     * Returns the number of causali contabilis.
     *
     * @return the number of causali contabilis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_CAUSALICONTABILI);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the causali contabili persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.CausaliContabili")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<CausaliContabili>> listenersList = new ArrayList<ModelListener<CausaliContabili>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<CausaliContabili>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(CausaliContabiliImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
