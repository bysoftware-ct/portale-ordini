package it.bysoftware.ct.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.bysoftware.ct.model.DescrizioniVarianti;
import it.bysoftware.ct.service.base.DescrizioniVariantiLocalServiceBaseImpl;

/**
 * The implementation of the descrizioni varianti local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.DescrizioniVariantiLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.DescrizioniVariantiLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.DescrizioniVariantiLocalServiceUtil
 */
public class DescrizioniVariantiLocalServiceImpl extends
        DescrizioniVariantiLocalServiceBaseImpl {
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(
            DescrizioniVariantiLocalServiceImpl.class);

    @Override
    public final List<DescrizioniVarianti> findVariants(final String itemCode) {
        List<DescrizioniVarianti> result = null;
        try {
            result = this.descrizioniVariantiPersistence.findByCodiceArticolo(
                    itemCode);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            result = new ArrayList<DescrizioniVarianti>();
        }
        return result;
    }
}
