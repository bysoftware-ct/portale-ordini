package it.bysoftware.ct.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.bysoftware.ct.model.Rubrica;
import it.bysoftware.ct.service.base.RubricaLocalServiceBaseImpl;

/**
 * The implementation of the rubrica local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.RubricaLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.RubricaLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.RubricaLocalServiceUtil
 */
public class RubricaLocalServiceImpl extends RubricaLocalServiceBaseImpl {
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(RubricaLocalServiceImpl.class);

    @Override
    public final List<Rubrica> findContacts(final String code) {
        List<Rubrica> result = new ArrayList<Rubrica>();
        try {
            result = this.rubricaPersistence.findByCodiceSoggetto(code);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        
        return result;
    }
}
