package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.VettoriServiceBaseImpl;

/**
 * The implementation of the vettori remote service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.VettoriService} interface.
 *
 * <p>This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.VettoriServiceBaseImpl
 * @see it.bysoftware.ct.service.VettoriServiceUtil
 */
public class VettoriServiceImpl extends VettoriServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.VettoriServiceUtil} to access the vettori remote
     * service.
     */
}
