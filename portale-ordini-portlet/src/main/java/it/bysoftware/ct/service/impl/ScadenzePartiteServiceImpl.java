package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.ScadenzePartiteServiceBaseImpl;

/**
 * The implementation of the scadenze partite remote service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.ScadenzePartiteService} interface.
 *
 * <p>This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.ScadenzePartiteServiceBaseImpl
 * @see it.bysoftware.ct.service.ScadenzePartiteServiceUtil
 */
public class ScadenzePartiteServiceImpl extends ScadenzePartiteServiceBaseImpl {

}
