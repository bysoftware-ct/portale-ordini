package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.CausaliContabiliLocalServiceBaseImpl;

/**
 * The implementation of the causali contabili local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.CausaliContabiliLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.CausaliContabiliLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.CausaliContabiliLocalServiceUtil
 */
public class CausaliContabiliLocalServiceImpl extends
        CausaliContabiliLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.CausaliContabiliLocalServiceUtil} to access the
     * causali contabili local service.
     */
}
