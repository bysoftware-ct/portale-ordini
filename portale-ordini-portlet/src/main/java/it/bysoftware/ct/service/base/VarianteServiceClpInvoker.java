package it.bysoftware.ct.service.base;

import it.bysoftware.ct.service.VarianteServiceUtil;

import java.util.Arrays;

/**
 * @author Mario Torrisi
 * @generated
 */
public class VarianteServiceClpInvoker {
    private String _methodName108;
    private String[] _methodParameterTypes108;
    private String _methodName109;
    private String[] _methodParameterTypes109;

    public VarianteServiceClpInvoker() {
        _methodName108 = "getBeanIdentifier";

        _methodParameterTypes108 = new String[] {  };

        _methodName109 = "setBeanIdentifier";

        _methodParameterTypes109 = new String[] { "java.lang.String" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName108.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes108, parameterTypes)) {
            return VarianteServiceUtil.getBeanIdentifier();
        }

        if (_methodName109.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes109, parameterTypes)) {
            VarianteServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        throw new UnsupportedOperationException();
    }
}
