package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CalendarUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchTestataDocumentoException;
import it.bysoftware.ct.model.TestataDocumento;
import it.bysoftware.ct.model.impl.TestataDocumentoImpl;
import it.bysoftware.ct.model.impl.TestataDocumentoModelImpl;
import it.bysoftware.ct.service.persistence.TestataDocumentoPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the testata documento service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see TestataDocumentoPersistence
 * @see TestataDocumentoUtil
 * @generated
 */
public class TestataDocumentoPersistenceImpl extends BasePersistenceImpl<TestataDocumento>
    implements TestataDocumentoPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link TestataDocumentoUtil} to access the testata documento persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = TestataDocumentoImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED,
            TestataDocumentoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED,
            TestataDocumentoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ANNOATTIVITACENTRODEPOSITO =
        new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED,
            TestataDocumentoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByAnnoAttivitaCentroDeposito",
            new String[] {
                Integer.class.getName(), String.class.getName(),
                String.class.getName(), String.class.getName(),
                String.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOATTIVITACENTRODEPOSITO =
        new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED,
            TestataDocumentoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByAnnoAttivitaCentroDeposito",
            new String[] {
                Integer.class.getName(), String.class.getName(),
                String.class.getName(), String.class.getName(),
                String.class.getName(), String.class.getName()
            },
            TestataDocumentoModelImpl.ANNO_COLUMN_BITMASK |
            TestataDocumentoModelImpl.CODICEATTIVITA_COLUMN_BITMASK |
            TestataDocumentoModelImpl.CODICECENTRO_COLUMN_BITMASK |
            TestataDocumentoModelImpl.CODICEDEPOSITO_COLUMN_BITMASK |
            TestataDocumentoModelImpl.CODICEFORNITORE_COLUMN_BITMASK |
            TestataDocumentoModelImpl.TIPODOCUMENTO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_ANNOATTIVITACENTRODEPOSITO =
        new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByAnnoAttivitaCentroDeposito",
            new String[] {
                Integer.class.getName(), String.class.getName(),
                String.class.getName(), String.class.getName(),
                String.class.getName(), String.class.getName()
            });
    private static final String _FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_ANNO_2 =
        "testataDocumento.id.anno = ? AND ";
    private static final String _FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEATTIVITA_1 =
        "testataDocumento.id.codiceAttivita IS NULL AND ";
    private static final String _FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEATTIVITA_2 =
        "testataDocumento.id.codiceAttivita = ? AND ";
    private static final String _FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEATTIVITA_3 =
        "(testataDocumento.id.codiceAttivita IS NULL OR testataDocumento.id.codiceAttivita = '') AND ";
    private static final String _FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICECENTRO_1 =
        "testataDocumento.id.codiceCentro IS NULL AND ";
    private static final String _FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICECENTRO_2 =
        "testataDocumento.id.codiceCentro = ? AND ";
    private static final String _FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICECENTRO_3 =
        "(testataDocumento.id.codiceCentro IS NULL OR testataDocumento.id.codiceCentro = '') AND ";
    private static final String _FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEDEPOSITO_1 =
        "testataDocumento.id.codiceDeposito IS NULL AND ";
    private static final String _FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEDEPOSITO_2 =
        "testataDocumento.id.codiceDeposito = ? AND ";
    private static final String _FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEDEPOSITO_3 =
        "(testataDocumento.id.codiceDeposito IS NULL OR testataDocumento.id.codiceDeposito = '') AND ";
    private static final String _FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEFORNITORE_1 =
        "testataDocumento.id.codiceFornitore IS NULL AND ";
    private static final String _FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEFORNITORE_2 =
        "testataDocumento.id.codiceFornitore = ? AND ";
    private static final String _FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEFORNITORE_3 =
        "(testataDocumento.id.codiceFornitore IS NULL OR testataDocumento.id.codiceFornitore = '') AND ";
    private static final String _FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_TIPODOCUMENTO_1 =
        "testataDocumento.id.tipoDocumento IS NULL";
    private static final String _FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_TIPODOCUMENTO_2 =
        "testataDocumento.id.tipoDocumento = ?";
    private static final String _FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_TIPODOCUMENTO_3 =
        "(testataDocumento.id.tipoDocumento IS NULL OR testataDocumento.id.tipoDocumento = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DATASTATOTIPODOCUMENTO =
        new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED,
            TestataDocumentoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByDataStatoTipoDocumento",
            new String[] {
                Date.class.getName(), Boolean.class.getName(),
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DATASTATOTIPODOCUMENTO =
        new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED,
            TestataDocumentoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByDataStatoTipoDocumento",
            new String[] {
                Date.class.getName(), Boolean.class.getName(),
                String.class.getName()
            },
            TestataDocumentoModelImpl.DATAREGISTRAZIONE_COLUMN_BITMASK |
            TestataDocumentoModelImpl.STATO_COLUMN_BITMASK |
            TestataDocumentoModelImpl.TIPODOCUMENTO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_DATASTATOTIPODOCUMENTO = new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByDataStatoTipoDocumento",
            new String[] {
                Date.class.getName(), Boolean.class.getName(),
                String.class.getName()
            });
    private static final String _FINDER_COLUMN_DATASTATOTIPODOCUMENTO_DATAREGISTRAZIONE_1 =
        "testataDocumento.dataRegistrazione IS NULL AND ";
    private static final String _FINDER_COLUMN_DATASTATOTIPODOCUMENTO_DATAREGISTRAZIONE_2 =
        "testataDocumento.dataRegistrazione = ? AND ";
    private static final String _FINDER_COLUMN_DATASTATOTIPODOCUMENTO_STATO_2 = "testataDocumento.stato = ? AND ";
    private static final String _FINDER_COLUMN_DATASTATOTIPODOCUMENTO_TIPODOCUMENTO_1 =
        "testataDocumento.id.tipoDocumento IS NULL";
    private static final String _FINDER_COLUMN_DATASTATOTIPODOCUMENTO_TIPODOCUMENTO_2 =
        "testataDocumento.id.tipoDocumento = ?";
    private static final String _FINDER_COLUMN_DATASTATOTIPODOCUMENTO_TIPODOCUMENTO_3 =
        "(testataDocumento.id.tipoDocumento IS NULL OR testataDocumento.id.tipoDocumento = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DATAFORNITORETIPODOCUMENTO =
        new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED,
            TestataDocumentoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByDataFornitoreTipoDocumento",
            new String[] {
                Date.class.getName(), String.class.getName(),
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DATAFORNITORETIPODOCUMENTO =
        new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED,
            TestataDocumentoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByDataFornitoreTipoDocumento",
            new String[] {
                Date.class.getName(), String.class.getName(),
                String.class.getName()
            },
            TestataDocumentoModelImpl.DATAREGISTRAZIONE_COLUMN_BITMASK |
            TestataDocumentoModelImpl.CODICEFORNITORE_COLUMN_BITMASK |
            TestataDocumentoModelImpl.TIPODOCUMENTO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_DATAFORNITORETIPODOCUMENTO =
        new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByDataFornitoreTipoDocumento",
            new String[] {
                Date.class.getName(), String.class.getName(),
                String.class.getName()
            });
    private static final String _FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_DATAREGISTRAZIONE_1 =
        "testataDocumento.dataRegistrazione IS NULL AND ";
    private static final String _FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_DATAREGISTRAZIONE_2 =
        "testataDocumento.dataRegistrazione = ? AND ";
    private static final String _FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_CODICEFORNITORE_1 =
        "testataDocumento.id.codiceFornitore IS NULL AND ";
    private static final String _FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_CODICEFORNITORE_2 =
        "testataDocumento.id.codiceFornitore = ? AND ";
    private static final String _FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_CODICEFORNITORE_3 =
        "(testataDocumento.id.codiceFornitore IS NULL OR testataDocumento.id.codiceFornitore = '') AND ";
    private static final String _FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_TIPODOCUMENTO_1 =
        "testataDocumento.id.tipoDocumento IS NULL";
    private static final String _FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_TIPODOCUMENTO_2 =
        "testataDocumento.id.tipoDocumento = ?";
    private static final String _FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_TIPODOCUMENTO_3 =
        "(testataDocumento.id.tipoDocumento IS NULL OR testataDocumento.id.tipoDocumento = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STATOTIPODOCUMENTO =
        new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED,
            TestataDocumentoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByStatoTipoDocumento",
            new String[] {
                Boolean.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATOTIPODOCUMENTO =
        new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED,
            TestataDocumentoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByStatoTipoDocumento",
            new String[] { Boolean.class.getName(), String.class.getName() },
            TestataDocumentoModelImpl.STATO_COLUMN_BITMASK |
            TestataDocumentoModelImpl.TIPODOCUMENTO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_STATOTIPODOCUMENTO = new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByStatoTipoDocumento",
            new String[] { Boolean.class.getName(), String.class.getName() });
    private static final String _FINDER_COLUMN_STATOTIPODOCUMENTO_STATO_2 = "testataDocumento.stato = ? AND ";
    private static final String _FINDER_COLUMN_STATOTIPODOCUMENTO_TIPODOCUMENTO_1 =
        "testataDocumento.id.tipoDocumento IS NULL";
    private static final String _FINDER_COLUMN_STATOTIPODOCUMENTO_TIPODOCUMENTO_2 =
        "testataDocumento.id.tipoDocumento = ?";
    private static final String _FINDER_COLUMN_STATOTIPODOCUMENTO_TIPODOCUMENTO_3 =
        "(testataDocumento.id.tipoDocumento IS NULL OR testataDocumento.id.tipoDocumento = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDORDINETIPODOCUMENTOSTATO =
        new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED,
            TestataDocumentoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByIdOrdineTipoDocumentoStato",
            new String[] {
                Long.class.getName(), Boolean.class.getName(),
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDORDINETIPODOCUMENTOSTATO =
        new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED,
            TestataDocumentoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByIdOrdineTipoDocumentoStato",
            new String[] {
                Long.class.getName(), Boolean.class.getName(),
                String.class.getName()
            },
            TestataDocumentoModelImpl.LIBLNG1_COLUMN_BITMASK |
            TestataDocumentoModelImpl.STATO_COLUMN_BITMASK |
            TestataDocumentoModelImpl.TIPODOCUMENTO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_IDORDINETIPODOCUMENTOSTATO =
        new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByIdOrdineTipoDocumentoStato",
            new String[] {
                Long.class.getName(), Boolean.class.getName(),
                String.class.getName()
            });
    private static final String _FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_LIBLNG1_2 =
        "testataDocumento.libLng1 = ? AND ";
    private static final String _FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_STATO_2 =
        "testataDocumento.stato = ? AND ";
    private static final String _FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_TIPODOCUMENTO_1 =
        "testataDocumento.id.tipoDocumento IS NULL";
    private static final String _FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_TIPODOCUMENTO_2 =
        "testataDocumento.id.tipoDocumento = ?";
    private static final String _FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_TIPODOCUMENTO_3 =
        "(testataDocumento.id.tipoDocumento IS NULL OR testataDocumento.id.tipoDocumento = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDORDINETIPO =
        new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED,
            TestataDocumentoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByIdOrdineTipo",
            new String[] {
                Long.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDORDINETIPO =
        new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED,
            TestataDocumentoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByIdOrdineTipo",
            new String[] { Long.class.getName(), String.class.getName() },
            TestataDocumentoModelImpl.LIBLNG1_COLUMN_BITMASK |
            TestataDocumentoModelImpl.TIPODOCUMENTO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_IDORDINETIPO = new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByIdOrdineTipo",
            new String[] { Long.class.getName(), String.class.getName() });
    private static final String _FINDER_COLUMN_IDORDINETIPO_LIBLNG1_2 = "testataDocumento.libLng1 = ? AND ";
    private static final String _FINDER_COLUMN_IDORDINETIPO_TIPODOCUMENTO_1 = "testataDocumento.id.tipoDocumento IS NULL";
    private static final String _FINDER_COLUMN_IDORDINETIPO_TIPODOCUMENTO_2 = "testataDocumento.id.tipoDocumento = ?";
    private static final String _FINDER_COLUMN_IDORDINETIPO_TIPODOCUMENTO_3 = "(testataDocumento.id.tipoDocumento IS NULL OR testataDocumento.id.tipoDocumento = '')";
    public static final FinderPath FINDER_PATH_FETCH_BY_IDORDINEFORNITORETIPO = new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED,
            TestataDocumentoImpl.class, FINDER_CLASS_NAME_ENTITY,
            "fetchByIdOrdineFornitoreTipo",
            new String[] {
                Long.class.getName(), String.class.getName(),
                String.class.getName()
            },
            TestataDocumentoModelImpl.LIBLNG1_COLUMN_BITMASK |
            TestataDocumentoModelImpl.CODICEFORNITORE_COLUMN_BITMASK |
            TestataDocumentoModelImpl.TIPODOCUMENTO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_IDORDINEFORNITORETIPO = new FinderPath(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByIdOrdineFornitoreTipo",
            new String[] {
                Long.class.getName(), String.class.getName(),
                String.class.getName()
            });
    private static final String _FINDER_COLUMN_IDORDINEFORNITORETIPO_LIBLNG1_2 = "testataDocumento.libLng1 = ? AND ";
    private static final String _FINDER_COLUMN_IDORDINEFORNITORETIPO_CODICEFORNITORE_1 =
        "testataDocumento.id.codiceFornitore IS NULL AND ";
    private static final String _FINDER_COLUMN_IDORDINEFORNITORETIPO_CODICEFORNITORE_2 =
        "testataDocumento.id.codiceFornitore = ? AND ";
    private static final String _FINDER_COLUMN_IDORDINEFORNITORETIPO_CODICEFORNITORE_3 =
        "(testataDocumento.id.codiceFornitore IS NULL OR testataDocumento.id.codiceFornitore = '') AND ";
    private static final String _FINDER_COLUMN_IDORDINEFORNITORETIPO_TIPODOCUMENTO_1 =
        "testataDocumento.id.tipoDocumento IS NULL";
    private static final String _FINDER_COLUMN_IDORDINEFORNITORETIPO_TIPODOCUMENTO_2 =
        "testataDocumento.id.tipoDocumento = ?";
    private static final String _FINDER_COLUMN_IDORDINEFORNITORETIPO_TIPODOCUMENTO_3 =
        "(testataDocumento.id.tipoDocumento IS NULL OR testataDocumento.id.tipoDocumento = '')";
    private static final String _SQL_SELECT_TESTATADOCUMENTO = "SELECT testataDocumento FROM TestataDocumento testataDocumento";
    private static final String _SQL_SELECT_TESTATADOCUMENTO_WHERE = "SELECT testataDocumento FROM TestataDocumento testataDocumento WHERE ";
    private static final String _SQL_COUNT_TESTATADOCUMENTO = "SELECT COUNT(testataDocumento) FROM TestataDocumento testataDocumento";
    private static final String _SQL_COUNT_TESTATADOCUMENTO_WHERE = "SELECT COUNT(testataDocumento) FROM TestataDocumento testataDocumento WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "testataDocumento.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No TestataDocumento exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No TestataDocumento exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(TestataDocumentoPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "anno", "codiceAttivita", "codiceCentro", "codiceDeposito",
                "protocollo", "codiceFornitore", "tipoDocumento",
                "dataRegistrazione", "codiceSpedizione",
                "codiceCausaleTrasporto", "codiceCuraTrasporto", "codicePorto",
                "codiceAspettoEsteriore", "codiceVettore1", "codiceVettore2",
                "libStr1", "libStr2", "libStr3", "libDbl1", "libDbl2", "libDbl3",
                "libLng1", "libLng2", "libLng3", "libDat1", "libDat2", "libDat3",
                "stato", "inviato", "codiceConsorzio"
            });
    private static TestataDocumento _nullTestataDocumento = new TestataDocumentoImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<TestataDocumento> toCacheModel() {
                return _nullTestataDocumentoCacheModel;
            }
        };

    private static CacheModel<TestataDocumento> _nullTestataDocumentoCacheModel = new CacheModel<TestataDocumento>() {
            @Override
            public TestataDocumento toEntityModel() {
                return _nullTestataDocumento;
            }
        };

    public TestataDocumentoPersistenceImpl() {
        setModelClass(TestataDocumento.class);
    }

    /**
     * Returns all the testata documentos where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param codiceDeposito the codice deposito
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @return the matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findByAnnoAttivitaCentroDeposito(int anno,
        String codiceAttivita, String codiceCentro, String codiceDeposito,
        String codiceFornitore, String tipoDocumento) throws SystemException {
        return findByAnnoAttivitaCentroDeposito(anno, codiceAttivita,
            codiceCentro, codiceDeposito, codiceFornitore, tipoDocumento,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the testata documentos where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param codiceDeposito the codice deposito
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @param start the lower bound of the range of testata documentos
     * @param end the upper bound of the range of testata documentos (not inclusive)
     * @return the range of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findByAnnoAttivitaCentroDeposito(int anno,
        String codiceAttivita, String codiceCentro, String codiceDeposito,
        String codiceFornitore, String tipoDocumento, int start, int end)
        throws SystemException {
        return findByAnnoAttivitaCentroDeposito(anno, codiceAttivita,
            codiceCentro, codiceDeposito, codiceFornitore, tipoDocumento,
            start, end, null);
    }

    /**
     * Returns an ordered range of all the testata documentos where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param codiceDeposito the codice deposito
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @param start the lower bound of the range of testata documentos
     * @param end the upper bound of the range of testata documentos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findByAnnoAttivitaCentroDeposito(int anno,
        String codiceAttivita, String codiceCentro, String codiceDeposito,
        String codiceFornitore, String tipoDocumento, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOATTIVITACENTRODEPOSITO;
            finderArgs = new Object[] {
                    anno, codiceAttivita, codiceCentro, codiceDeposito,
                    codiceFornitore, tipoDocumento
                };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ANNOATTIVITACENTRODEPOSITO;
            finderArgs = new Object[] {
                    anno, codiceAttivita, codiceCentro, codiceDeposito,
                    codiceFornitore, tipoDocumento,
                    
                    start, end, orderByComparator
                };
        }

        List<TestataDocumento> list = (List<TestataDocumento>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (TestataDocumento testataDocumento : list) {
                if ((anno != testataDocumento.getAnno()) ||
                        !Validator.equals(codiceAttivita,
                            testataDocumento.getCodiceAttivita()) ||
                        !Validator.equals(codiceCentro,
                            testataDocumento.getCodiceCentro()) ||
                        !Validator.equals(codiceDeposito,
                            testataDocumento.getCodiceDeposito()) ||
                        !Validator.equals(codiceFornitore,
                            testataDocumento.getCodiceFornitore()) ||
                        !Validator.equals(tipoDocumento,
                            testataDocumento.getTipoDocumento())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(8 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(8);
            }

            query.append(_SQL_SELECT_TESTATADOCUMENTO_WHERE);

            query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_ANNO_2);

            boolean bindCodiceAttivita = false;

            if (codiceAttivita == null) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEATTIVITA_1);
            } else if (codiceAttivita.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEATTIVITA_3);
            } else {
                bindCodiceAttivita = true;

                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEATTIVITA_2);
            }

            boolean bindCodiceCentro = false;

            if (codiceCentro == null) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICECENTRO_1);
            } else if (codiceCentro.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICECENTRO_3);
            } else {
                bindCodiceCentro = true;

                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICECENTRO_2);
            }

            boolean bindCodiceDeposito = false;

            if (codiceDeposito == null) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEDEPOSITO_1);
            } else if (codiceDeposito.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEDEPOSITO_3);
            } else {
                bindCodiceDeposito = true;

                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEDEPOSITO_2);
            }

            boolean bindCodiceFornitore = false;

            if (codiceFornitore == null) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEFORNITORE_1);
            } else if (codiceFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEFORNITORE_3);
            } else {
                bindCodiceFornitore = true;

                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEFORNITORE_2);
            }

            boolean bindTipoDocumento = false;

            if (tipoDocumento == null) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_TIPODOCUMENTO_1);
            } else if (tipoDocumento.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_TIPODOCUMENTO_3);
            } else {
                bindTipoDocumento = true;

                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_TIPODOCUMENTO_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TestataDocumentoModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(anno);

                if (bindCodiceAttivita) {
                    qPos.add(codiceAttivita);
                }

                if (bindCodiceCentro) {
                    qPos.add(codiceCentro);
                }

                if (bindCodiceDeposito) {
                    qPos.add(codiceDeposito);
                }

                if (bindCodiceFornitore) {
                    qPos.add(codiceFornitore);
                }

                if (bindTipoDocumento) {
                    qPos.add(tipoDocumento);
                }

                if (!pagination) {
                    list = (List<TestataDocumento>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<TestataDocumento>(list);
                } else {
                    list = (List<TestataDocumento>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first testata documento in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param codiceDeposito the codice deposito
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento findByAnnoAttivitaCentroDeposito_First(int anno,
        String codiceAttivita, String codiceCentro, String codiceDeposito,
        String codiceFornitore, String tipoDocumento,
        OrderByComparator orderByComparator)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = fetchByAnnoAttivitaCentroDeposito_First(anno,
                codiceAttivita, codiceCentro, codiceDeposito, codiceFornitore,
                tipoDocumento, orderByComparator);

        if (testataDocumento != null) {
            return testataDocumento;
        }

        StringBundler msg = new StringBundler(14);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("anno=");
        msg.append(anno);

        msg.append(", codiceAttivita=");
        msg.append(codiceAttivita);

        msg.append(", codiceCentro=");
        msg.append(codiceCentro);

        msg.append(", codiceDeposito=");
        msg.append(codiceDeposito);

        msg.append(", codiceFornitore=");
        msg.append(codiceFornitore);

        msg.append(", tipoDocumento=");
        msg.append(tipoDocumento);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTestataDocumentoException(msg.toString());
    }

    /**
     * Returns the first testata documento in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param codiceDeposito the codice deposito
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching testata documento, or <code>null</code> if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento fetchByAnnoAttivitaCentroDeposito_First(int anno,
        String codiceAttivita, String codiceCentro, String codiceDeposito,
        String codiceFornitore, String tipoDocumento,
        OrderByComparator orderByComparator) throws SystemException {
        List<TestataDocumento> list = findByAnnoAttivitaCentroDeposito(anno,
                codiceAttivita, codiceCentro, codiceDeposito, codiceFornitore,
                tipoDocumento, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last testata documento in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param codiceDeposito the codice deposito
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento findByAnnoAttivitaCentroDeposito_Last(int anno,
        String codiceAttivita, String codiceCentro, String codiceDeposito,
        String codiceFornitore, String tipoDocumento,
        OrderByComparator orderByComparator)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = fetchByAnnoAttivitaCentroDeposito_Last(anno,
                codiceAttivita, codiceCentro, codiceDeposito, codiceFornitore,
                tipoDocumento, orderByComparator);

        if (testataDocumento != null) {
            return testataDocumento;
        }

        StringBundler msg = new StringBundler(14);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("anno=");
        msg.append(anno);

        msg.append(", codiceAttivita=");
        msg.append(codiceAttivita);

        msg.append(", codiceCentro=");
        msg.append(codiceCentro);

        msg.append(", codiceDeposito=");
        msg.append(codiceDeposito);

        msg.append(", codiceFornitore=");
        msg.append(codiceFornitore);

        msg.append(", tipoDocumento=");
        msg.append(tipoDocumento);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTestataDocumentoException(msg.toString());
    }

    /**
     * Returns the last testata documento in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param codiceDeposito the codice deposito
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching testata documento, or <code>null</code> if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento fetchByAnnoAttivitaCentroDeposito_Last(int anno,
        String codiceAttivita, String codiceCentro, String codiceDeposito,
        String codiceFornitore, String tipoDocumento,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByAnnoAttivitaCentroDeposito(anno, codiceAttivita,
                codiceCentro, codiceDeposito, codiceFornitore, tipoDocumento);

        if (count == 0) {
            return null;
        }

        List<TestataDocumento> list = findByAnnoAttivitaCentroDeposito(anno,
                codiceAttivita, codiceCentro, codiceDeposito, codiceFornitore,
                tipoDocumento, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the testata documentos before and after the current testata documento in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * @param testataDocumentoPK the primary key of the current testata documento
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param codiceDeposito the codice deposito
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento[] findByAnnoAttivitaCentroDeposito_PrevAndNext(
        TestataDocumentoPK testataDocumentoPK, int anno, String codiceAttivita,
        String codiceCentro, String codiceDeposito, String codiceFornitore,
        String tipoDocumento, OrderByComparator orderByComparator)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = findByPrimaryKey(testataDocumentoPK);

        Session session = null;

        try {
            session = openSession();

            TestataDocumento[] array = new TestataDocumentoImpl[3];

            array[0] = getByAnnoAttivitaCentroDeposito_PrevAndNext(session,
                    testataDocumento, anno, codiceAttivita, codiceCentro,
                    codiceDeposito, codiceFornitore, tipoDocumento,
                    orderByComparator, true);

            array[1] = testataDocumento;

            array[2] = getByAnnoAttivitaCentroDeposito_PrevAndNext(session,
                    testataDocumento, anno, codiceAttivita, codiceCentro,
                    codiceDeposito, codiceFornitore, tipoDocumento,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected TestataDocumento getByAnnoAttivitaCentroDeposito_PrevAndNext(
        Session session, TestataDocumento testataDocumento, int anno,
        String codiceAttivita, String codiceCentro, String codiceDeposito,
        String codiceFornitore, String tipoDocumento,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TESTATADOCUMENTO_WHERE);

        query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_ANNO_2);

        boolean bindCodiceAttivita = false;

        if (codiceAttivita == null) {
            query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEATTIVITA_1);
        } else if (codiceAttivita.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEATTIVITA_3);
        } else {
            bindCodiceAttivita = true;

            query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEATTIVITA_2);
        }

        boolean bindCodiceCentro = false;

        if (codiceCentro == null) {
            query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICECENTRO_1);
        } else if (codiceCentro.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICECENTRO_3);
        } else {
            bindCodiceCentro = true;

            query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICECENTRO_2);
        }

        boolean bindCodiceDeposito = false;

        if (codiceDeposito == null) {
            query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEDEPOSITO_1);
        } else if (codiceDeposito.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEDEPOSITO_3);
        } else {
            bindCodiceDeposito = true;

            query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEDEPOSITO_2);
        }

        boolean bindCodiceFornitore = false;

        if (codiceFornitore == null) {
            query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEFORNITORE_1);
        } else if (codiceFornitore.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEFORNITORE_3);
        } else {
            bindCodiceFornitore = true;

            query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEFORNITORE_2);
        }

        boolean bindTipoDocumento = false;

        if (tipoDocumento == null) {
            query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_TIPODOCUMENTO_1);
        } else if (tipoDocumento.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_TIPODOCUMENTO_3);
        } else {
            bindTipoDocumento = true;

            query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_TIPODOCUMENTO_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TestataDocumentoModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(anno);

        if (bindCodiceAttivita) {
            qPos.add(codiceAttivita);
        }

        if (bindCodiceCentro) {
            qPos.add(codiceCentro);
        }

        if (bindCodiceDeposito) {
            qPos.add(codiceDeposito);
        }

        if (bindCodiceFornitore) {
            qPos.add(codiceFornitore);
        }

        if (bindTipoDocumento) {
            qPos.add(tipoDocumento);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(testataDocumento);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<TestataDocumento> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the testata documentos where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63; from the database.
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param codiceDeposito the codice deposito
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByAnnoAttivitaCentroDeposito(int anno,
        String codiceAttivita, String codiceCentro, String codiceDeposito,
        String codiceFornitore, String tipoDocumento) throws SystemException {
        for (TestataDocumento testataDocumento : findByAnnoAttivitaCentroDeposito(
                anno, codiceAttivita, codiceCentro, codiceDeposito,
                codiceFornitore, tipoDocumento, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(testataDocumento);
        }
    }

    /**
     * Returns the number of testata documentos where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * @param anno the anno
     * @param codiceAttivita the codice attivita
     * @param codiceCentro the codice centro
     * @param codiceDeposito the codice deposito
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @return the number of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByAnnoAttivitaCentroDeposito(int anno,
        String codiceAttivita, String codiceCentro, String codiceDeposito,
        String codiceFornitore, String tipoDocumento) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_ANNOATTIVITACENTRODEPOSITO;

        Object[] finderArgs = new Object[] {
                anno, codiceAttivita, codiceCentro, codiceDeposito,
                codiceFornitore, tipoDocumento
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(7);

            query.append(_SQL_COUNT_TESTATADOCUMENTO_WHERE);

            query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_ANNO_2);

            boolean bindCodiceAttivita = false;

            if (codiceAttivita == null) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEATTIVITA_1);
            } else if (codiceAttivita.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEATTIVITA_3);
            } else {
                bindCodiceAttivita = true;

                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEATTIVITA_2);
            }

            boolean bindCodiceCentro = false;

            if (codiceCentro == null) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICECENTRO_1);
            } else if (codiceCentro.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICECENTRO_3);
            } else {
                bindCodiceCentro = true;

                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICECENTRO_2);
            }

            boolean bindCodiceDeposito = false;

            if (codiceDeposito == null) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEDEPOSITO_1);
            } else if (codiceDeposito.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEDEPOSITO_3);
            } else {
                bindCodiceDeposito = true;

                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEDEPOSITO_2);
            }

            boolean bindCodiceFornitore = false;

            if (codiceFornitore == null) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEFORNITORE_1);
            } else if (codiceFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEFORNITORE_3);
            } else {
                bindCodiceFornitore = true;

                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_CODICEFORNITORE_2);
            }

            boolean bindTipoDocumento = false;

            if (tipoDocumento == null) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_TIPODOCUMENTO_1);
            } else if (tipoDocumento.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_TIPODOCUMENTO_3);
            } else {
                bindTipoDocumento = true;

                query.append(_FINDER_COLUMN_ANNOATTIVITACENTRODEPOSITO_TIPODOCUMENTO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(anno);

                if (bindCodiceAttivita) {
                    qPos.add(codiceAttivita);
                }

                if (bindCodiceCentro) {
                    qPos.add(codiceCentro);
                }

                if (bindCodiceDeposito) {
                    qPos.add(codiceDeposito);
                }

                if (bindCodiceFornitore) {
                    qPos.add(codiceFornitore);
                }

                if (bindTipoDocumento) {
                    qPos.add(tipoDocumento);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the testata documentos where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
     *
     * @param dataRegistrazione the data registrazione
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @return the matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findByDataStatoTipoDocumento(
        Date dataRegistrazione, boolean stato, String tipoDocumento)
        throws SystemException {
        return findByDataStatoTipoDocumento(dataRegistrazione, stato,
            tipoDocumento, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the testata documentos where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param dataRegistrazione the data registrazione
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param start the lower bound of the range of testata documentos
     * @param end the upper bound of the range of testata documentos (not inclusive)
     * @return the range of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findByDataStatoTipoDocumento(
        Date dataRegistrazione, boolean stato, String tipoDocumento, int start,
        int end) throws SystemException {
        return findByDataStatoTipoDocumento(dataRegistrazione, stato,
            tipoDocumento, start, end, null);
    }

    /**
     * Returns an ordered range of all the testata documentos where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param dataRegistrazione the data registrazione
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param start the lower bound of the range of testata documentos
     * @param end the upper bound of the range of testata documentos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findByDataStatoTipoDocumento(
        Date dataRegistrazione, boolean stato, String tipoDocumento, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DATASTATOTIPODOCUMENTO;
            finderArgs = new Object[] { dataRegistrazione, stato, tipoDocumento };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DATASTATOTIPODOCUMENTO;
            finderArgs = new Object[] {
                    dataRegistrazione, stato, tipoDocumento,
                    
                    start, end, orderByComparator
                };
        }

        List<TestataDocumento> list = (List<TestataDocumento>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (TestataDocumento testataDocumento : list) {
                if (!Validator.equals(dataRegistrazione,
                            testataDocumento.getDataRegistrazione()) ||
                        (stato != testataDocumento.getStato()) ||
                        !Validator.equals(tipoDocumento,
                            testataDocumento.getTipoDocumento())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(5 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(5);
            }

            query.append(_SQL_SELECT_TESTATADOCUMENTO_WHERE);

            boolean bindDataRegistrazione = false;

            if (dataRegistrazione == null) {
                query.append(_FINDER_COLUMN_DATASTATOTIPODOCUMENTO_DATAREGISTRAZIONE_1);
            } else {
                bindDataRegistrazione = true;

                query.append(_FINDER_COLUMN_DATASTATOTIPODOCUMENTO_DATAREGISTRAZIONE_2);
            }

            query.append(_FINDER_COLUMN_DATASTATOTIPODOCUMENTO_STATO_2);

            boolean bindTipoDocumento = false;

            if (tipoDocumento == null) {
                query.append(_FINDER_COLUMN_DATASTATOTIPODOCUMENTO_TIPODOCUMENTO_1);
            } else if (tipoDocumento.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_DATASTATOTIPODOCUMENTO_TIPODOCUMENTO_3);
            } else {
                bindTipoDocumento = true;

                query.append(_FINDER_COLUMN_DATASTATOTIPODOCUMENTO_TIPODOCUMENTO_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TestataDocumentoModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindDataRegistrazione) {
                    qPos.add(CalendarUtil.getTimestamp(dataRegistrazione));
                }

                qPos.add(stato);

                if (bindTipoDocumento) {
                    qPos.add(tipoDocumento);
                }

                if (!pagination) {
                    list = (List<TestataDocumento>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<TestataDocumento>(list);
                } else {
                    list = (List<TestataDocumento>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first testata documento in the ordered set where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
     *
     * @param dataRegistrazione the data registrazione
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento findByDataStatoTipoDocumento_First(
        Date dataRegistrazione, boolean stato, String tipoDocumento,
        OrderByComparator orderByComparator)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = fetchByDataStatoTipoDocumento_First(dataRegistrazione,
                stato, tipoDocumento, orderByComparator);

        if (testataDocumento != null) {
            return testataDocumento;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("dataRegistrazione=");
        msg.append(dataRegistrazione);

        msg.append(", stato=");
        msg.append(stato);

        msg.append(", tipoDocumento=");
        msg.append(tipoDocumento);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTestataDocumentoException(msg.toString());
    }

    /**
     * Returns the first testata documento in the ordered set where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
     *
     * @param dataRegistrazione the data registrazione
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching testata documento, or <code>null</code> if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento fetchByDataStatoTipoDocumento_First(
        Date dataRegistrazione, boolean stato, String tipoDocumento,
        OrderByComparator orderByComparator) throws SystemException {
        List<TestataDocumento> list = findByDataStatoTipoDocumento(dataRegistrazione,
                stato, tipoDocumento, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last testata documento in the ordered set where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
     *
     * @param dataRegistrazione the data registrazione
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento findByDataStatoTipoDocumento_Last(
        Date dataRegistrazione, boolean stato, String tipoDocumento,
        OrderByComparator orderByComparator)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = fetchByDataStatoTipoDocumento_Last(dataRegistrazione,
                stato, tipoDocumento, orderByComparator);

        if (testataDocumento != null) {
            return testataDocumento;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("dataRegistrazione=");
        msg.append(dataRegistrazione);

        msg.append(", stato=");
        msg.append(stato);

        msg.append(", tipoDocumento=");
        msg.append(tipoDocumento);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTestataDocumentoException(msg.toString());
    }

    /**
     * Returns the last testata documento in the ordered set where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
     *
     * @param dataRegistrazione the data registrazione
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching testata documento, or <code>null</code> if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento fetchByDataStatoTipoDocumento_Last(
        Date dataRegistrazione, boolean stato, String tipoDocumento,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByDataStatoTipoDocumento(dataRegistrazione, stato,
                tipoDocumento);

        if (count == 0) {
            return null;
        }

        List<TestataDocumento> list = findByDataStatoTipoDocumento(dataRegistrazione,
                stato, tipoDocumento, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the testata documentos before and after the current testata documento in the ordered set where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
     *
     * @param testataDocumentoPK the primary key of the current testata documento
     * @param dataRegistrazione the data registrazione
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento[] findByDataStatoTipoDocumento_PrevAndNext(
        TestataDocumentoPK testataDocumentoPK, Date dataRegistrazione,
        boolean stato, String tipoDocumento, OrderByComparator orderByComparator)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = findByPrimaryKey(testataDocumentoPK);

        Session session = null;

        try {
            session = openSession();

            TestataDocumento[] array = new TestataDocumentoImpl[3];

            array[0] = getByDataStatoTipoDocumento_PrevAndNext(session,
                    testataDocumento, dataRegistrazione, stato, tipoDocumento,
                    orderByComparator, true);

            array[1] = testataDocumento;

            array[2] = getByDataStatoTipoDocumento_PrevAndNext(session,
                    testataDocumento, dataRegistrazione, stato, tipoDocumento,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected TestataDocumento getByDataStatoTipoDocumento_PrevAndNext(
        Session session, TestataDocumento testataDocumento,
        Date dataRegistrazione, boolean stato, String tipoDocumento,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TESTATADOCUMENTO_WHERE);

        boolean bindDataRegistrazione = false;

        if (dataRegistrazione == null) {
            query.append(_FINDER_COLUMN_DATASTATOTIPODOCUMENTO_DATAREGISTRAZIONE_1);
        } else {
            bindDataRegistrazione = true;

            query.append(_FINDER_COLUMN_DATASTATOTIPODOCUMENTO_DATAREGISTRAZIONE_2);
        }

        query.append(_FINDER_COLUMN_DATASTATOTIPODOCUMENTO_STATO_2);

        boolean bindTipoDocumento = false;

        if (tipoDocumento == null) {
            query.append(_FINDER_COLUMN_DATASTATOTIPODOCUMENTO_TIPODOCUMENTO_1);
        } else if (tipoDocumento.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_DATASTATOTIPODOCUMENTO_TIPODOCUMENTO_3);
        } else {
            bindTipoDocumento = true;

            query.append(_FINDER_COLUMN_DATASTATOTIPODOCUMENTO_TIPODOCUMENTO_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TestataDocumentoModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindDataRegistrazione) {
            qPos.add(CalendarUtil.getTimestamp(dataRegistrazione));
        }

        qPos.add(stato);

        if (bindTipoDocumento) {
            qPos.add(tipoDocumento);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(testataDocumento);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<TestataDocumento> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the testata documentos where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63; from the database.
     *
     * @param dataRegistrazione the data registrazione
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByDataStatoTipoDocumento(Date dataRegistrazione,
        boolean stato, String tipoDocumento) throws SystemException {
        for (TestataDocumento testataDocumento : findByDataStatoTipoDocumento(
                dataRegistrazione, stato, tipoDocumento, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(testataDocumento);
        }
    }

    /**
     * Returns the number of testata documentos where dataRegistrazione = &#63; and stato = &#63; and tipoDocumento = &#63;.
     *
     * @param dataRegistrazione the data registrazione
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @return the number of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByDataStatoTipoDocumento(Date dataRegistrazione,
        boolean stato, String tipoDocumento) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_DATASTATOTIPODOCUMENTO;

        Object[] finderArgs = new Object[] {
                dataRegistrazione, stato, tipoDocumento
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(4);

            query.append(_SQL_COUNT_TESTATADOCUMENTO_WHERE);

            boolean bindDataRegistrazione = false;

            if (dataRegistrazione == null) {
                query.append(_FINDER_COLUMN_DATASTATOTIPODOCUMENTO_DATAREGISTRAZIONE_1);
            } else {
                bindDataRegistrazione = true;

                query.append(_FINDER_COLUMN_DATASTATOTIPODOCUMENTO_DATAREGISTRAZIONE_2);
            }

            query.append(_FINDER_COLUMN_DATASTATOTIPODOCUMENTO_STATO_2);

            boolean bindTipoDocumento = false;

            if (tipoDocumento == null) {
                query.append(_FINDER_COLUMN_DATASTATOTIPODOCUMENTO_TIPODOCUMENTO_1);
            } else if (tipoDocumento.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_DATASTATOTIPODOCUMENTO_TIPODOCUMENTO_3);
            } else {
                bindTipoDocumento = true;

                query.append(_FINDER_COLUMN_DATASTATOTIPODOCUMENTO_TIPODOCUMENTO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindDataRegistrazione) {
                    qPos.add(CalendarUtil.getTimestamp(dataRegistrazione));
                }

                qPos.add(stato);

                if (bindTipoDocumento) {
                    qPos.add(tipoDocumento);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the testata documentos where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * @param dataRegistrazione the data registrazione
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @return the matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findByDataFornitoreTipoDocumento(
        Date dataRegistrazione, String codiceFornitore, String tipoDocumento)
        throws SystemException {
        return findByDataFornitoreTipoDocumento(dataRegistrazione,
            codiceFornitore, tipoDocumento, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the testata documentos where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param dataRegistrazione the data registrazione
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @param start the lower bound of the range of testata documentos
     * @param end the upper bound of the range of testata documentos (not inclusive)
     * @return the range of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findByDataFornitoreTipoDocumento(
        Date dataRegistrazione, String codiceFornitore, String tipoDocumento,
        int start, int end) throws SystemException {
        return findByDataFornitoreTipoDocumento(dataRegistrazione,
            codiceFornitore, tipoDocumento, start, end, null);
    }

    /**
     * Returns an ordered range of all the testata documentos where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param dataRegistrazione the data registrazione
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @param start the lower bound of the range of testata documentos
     * @param end the upper bound of the range of testata documentos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findByDataFornitoreTipoDocumento(
        Date dataRegistrazione, String codiceFornitore, String tipoDocumento,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DATAFORNITORETIPODOCUMENTO;
            finderArgs = new Object[] {
                    dataRegistrazione, codiceFornitore, tipoDocumento
                };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DATAFORNITORETIPODOCUMENTO;
            finderArgs = new Object[] {
                    dataRegistrazione, codiceFornitore, tipoDocumento,
                    
                    start, end, orderByComparator
                };
        }

        List<TestataDocumento> list = (List<TestataDocumento>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (TestataDocumento testataDocumento : list) {
                if (!Validator.equals(dataRegistrazione,
                            testataDocumento.getDataRegistrazione()) ||
                        !Validator.equals(codiceFornitore,
                            testataDocumento.getCodiceFornitore()) ||
                        !Validator.equals(tipoDocumento,
                            testataDocumento.getTipoDocumento())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(5 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(5);
            }

            query.append(_SQL_SELECT_TESTATADOCUMENTO_WHERE);

            boolean bindDataRegistrazione = false;

            if (dataRegistrazione == null) {
                query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_DATAREGISTRAZIONE_1);
            } else {
                bindDataRegistrazione = true;

                query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_DATAREGISTRAZIONE_2);
            }

            boolean bindCodiceFornitore = false;

            if (codiceFornitore == null) {
                query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_CODICEFORNITORE_1);
            } else if (codiceFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_CODICEFORNITORE_3);
            } else {
                bindCodiceFornitore = true;

                query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_CODICEFORNITORE_2);
            }

            boolean bindTipoDocumento = false;

            if (tipoDocumento == null) {
                query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_TIPODOCUMENTO_1);
            } else if (tipoDocumento.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_TIPODOCUMENTO_3);
            } else {
                bindTipoDocumento = true;

                query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_TIPODOCUMENTO_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TestataDocumentoModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindDataRegistrazione) {
                    qPos.add(CalendarUtil.getTimestamp(dataRegistrazione));
                }

                if (bindCodiceFornitore) {
                    qPos.add(codiceFornitore);
                }

                if (bindTipoDocumento) {
                    qPos.add(tipoDocumento);
                }

                if (!pagination) {
                    list = (List<TestataDocumento>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<TestataDocumento>(list);
                } else {
                    list = (List<TestataDocumento>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first testata documento in the ordered set where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * @param dataRegistrazione the data registrazione
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento findByDataFornitoreTipoDocumento_First(
        Date dataRegistrazione, String codiceFornitore, String tipoDocumento,
        OrderByComparator orderByComparator)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = fetchByDataFornitoreTipoDocumento_First(dataRegistrazione,
                codiceFornitore, tipoDocumento, orderByComparator);

        if (testataDocumento != null) {
            return testataDocumento;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("dataRegistrazione=");
        msg.append(dataRegistrazione);

        msg.append(", codiceFornitore=");
        msg.append(codiceFornitore);

        msg.append(", tipoDocumento=");
        msg.append(tipoDocumento);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTestataDocumentoException(msg.toString());
    }

    /**
     * Returns the first testata documento in the ordered set where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * @param dataRegistrazione the data registrazione
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching testata documento, or <code>null</code> if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento fetchByDataFornitoreTipoDocumento_First(
        Date dataRegistrazione, String codiceFornitore, String tipoDocumento,
        OrderByComparator orderByComparator) throws SystemException {
        List<TestataDocumento> list = findByDataFornitoreTipoDocumento(dataRegistrazione,
                codiceFornitore, tipoDocumento, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last testata documento in the ordered set where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * @param dataRegistrazione the data registrazione
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento findByDataFornitoreTipoDocumento_Last(
        Date dataRegistrazione, String codiceFornitore, String tipoDocumento,
        OrderByComparator orderByComparator)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = fetchByDataFornitoreTipoDocumento_Last(dataRegistrazione,
                codiceFornitore, tipoDocumento, orderByComparator);

        if (testataDocumento != null) {
            return testataDocumento;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("dataRegistrazione=");
        msg.append(dataRegistrazione);

        msg.append(", codiceFornitore=");
        msg.append(codiceFornitore);

        msg.append(", tipoDocumento=");
        msg.append(tipoDocumento);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTestataDocumentoException(msg.toString());
    }

    /**
     * Returns the last testata documento in the ordered set where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * @param dataRegistrazione the data registrazione
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching testata documento, or <code>null</code> if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento fetchByDataFornitoreTipoDocumento_Last(
        Date dataRegistrazione, String codiceFornitore, String tipoDocumento,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByDataFornitoreTipoDocumento(dataRegistrazione,
                codiceFornitore, tipoDocumento);

        if (count == 0) {
            return null;
        }

        List<TestataDocumento> list = findByDataFornitoreTipoDocumento(dataRegistrazione,
                codiceFornitore, tipoDocumento, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the testata documentos before and after the current testata documento in the ordered set where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * @param testataDocumentoPK the primary key of the current testata documento
     * @param dataRegistrazione the data registrazione
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento[] findByDataFornitoreTipoDocumento_PrevAndNext(
        TestataDocumentoPK testataDocumentoPK, Date dataRegistrazione,
        String codiceFornitore, String tipoDocumento,
        OrderByComparator orderByComparator)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = findByPrimaryKey(testataDocumentoPK);

        Session session = null;

        try {
            session = openSession();

            TestataDocumento[] array = new TestataDocumentoImpl[3];

            array[0] = getByDataFornitoreTipoDocumento_PrevAndNext(session,
                    testataDocumento, dataRegistrazione, codiceFornitore,
                    tipoDocumento, orderByComparator, true);

            array[1] = testataDocumento;

            array[2] = getByDataFornitoreTipoDocumento_PrevAndNext(session,
                    testataDocumento, dataRegistrazione, codiceFornitore,
                    tipoDocumento, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected TestataDocumento getByDataFornitoreTipoDocumento_PrevAndNext(
        Session session, TestataDocumento testataDocumento,
        Date dataRegistrazione, String codiceFornitore, String tipoDocumento,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TESTATADOCUMENTO_WHERE);

        boolean bindDataRegistrazione = false;

        if (dataRegistrazione == null) {
            query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_DATAREGISTRAZIONE_1);
        } else {
            bindDataRegistrazione = true;

            query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_DATAREGISTRAZIONE_2);
        }

        boolean bindCodiceFornitore = false;

        if (codiceFornitore == null) {
            query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_CODICEFORNITORE_1);
        } else if (codiceFornitore.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_CODICEFORNITORE_3);
        } else {
            bindCodiceFornitore = true;

            query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_CODICEFORNITORE_2);
        }

        boolean bindTipoDocumento = false;

        if (tipoDocumento == null) {
            query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_TIPODOCUMENTO_1);
        } else if (tipoDocumento.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_TIPODOCUMENTO_3);
        } else {
            bindTipoDocumento = true;

            query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_TIPODOCUMENTO_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TestataDocumentoModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindDataRegistrazione) {
            qPos.add(CalendarUtil.getTimestamp(dataRegistrazione));
        }

        if (bindCodiceFornitore) {
            qPos.add(codiceFornitore);
        }

        if (bindTipoDocumento) {
            qPos.add(tipoDocumento);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(testataDocumento);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<TestataDocumento> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the testata documentos where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63; from the database.
     *
     * @param dataRegistrazione the data registrazione
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByDataFornitoreTipoDocumento(Date dataRegistrazione,
        String codiceFornitore, String tipoDocumento) throws SystemException {
        for (TestataDocumento testataDocumento : findByDataFornitoreTipoDocumento(
                dataRegistrazione, codiceFornitore, tipoDocumento,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(testataDocumento);
        }
    }

    /**
     * Returns the number of testata documentos where dataRegistrazione = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * @param dataRegistrazione the data registrazione
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @return the number of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByDataFornitoreTipoDocumento(Date dataRegistrazione,
        String codiceFornitore, String tipoDocumento) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_DATAFORNITORETIPODOCUMENTO;

        Object[] finderArgs = new Object[] {
                dataRegistrazione, codiceFornitore, tipoDocumento
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(4);

            query.append(_SQL_COUNT_TESTATADOCUMENTO_WHERE);

            boolean bindDataRegistrazione = false;

            if (dataRegistrazione == null) {
                query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_DATAREGISTRAZIONE_1);
            } else {
                bindDataRegistrazione = true;

                query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_DATAREGISTRAZIONE_2);
            }

            boolean bindCodiceFornitore = false;

            if (codiceFornitore == null) {
                query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_CODICEFORNITORE_1);
            } else if (codiceFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_CODICEFORNITORE_3);
            } else {
                bindCodiceFornitore = true;

                query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_CODICEFORNITORE_2);
            }

            boolean bindTipoDocumento = false;

            if (tipoDocumento == null) {
                query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_TIPODOCUMENTO_1);
            } else if (tipoDocumento.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_TIPODOCUMENTO_3);
            } else {
                bindTipoDocumento = true;

                query.append(_FINDER_COLUMN_DATAFORNITORETIPODOCUMENTO_TIPODOCUMENTO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindDataRegistrazione) {
                    qPos.add(CalendarUtil.getTimestamp(dataRegistrazione));
                }

                if (bindCodiceFornitore) {
                    qPos.add(codiceFornitore);
                }

                if (bindTipoDocumento) {
                    qPos.add(tipoDocumento);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the testata documentos where stato = &#63; and tipoDocumento = &#63;.
     *
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @return the matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findByStatoTipoDocumento(boolean stato,
        String tipoDocumento) throws SystemException {
        return findByStatoTipoDocumento(stato, tipoDocumento,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the testata documentos where stato = &#63; and tipoDocumento = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param start the lower bound of the range of testata documentos
     * @param end the upper bound of the range of testata documentos (not inclusive)
     * @return the range of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findByStatoTipoDocumento(boolean stato,
        String tipoDocumento, int start, int end) throws SystemException {
        return findByStatoTipoDocumento(stato, tipoDocumento, start, end, null);
    }

    /**
     * Returns an ordered range of all the testata documentos where stato = &#63; and tipoDocumento = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param start the lower bound of the range of testata documentos
     * @param end the upper bound of the range of testata documentos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findByStatoTipoDocumento(boolean stato,
        String tipoDocumento, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATOTIPODOCUMENTO;
            finderArgs = new Object[] { stato, tipoDocumento };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STATOTIPODOCUMENTO;
            finderArgs = new Object[] {
                    stato, tipoDocumento,
                    
                    start, end, orderByComparator
                };
        }

        List<TestataDocumento> list = (List<TestataDocumento>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (TestataDocumento testataDocumento : list) {
                if ((stato != testataDocumento.getStato()) ||
                        !Validator.equals(tipoDocumento,
                            testataDocumento.getTipoDocumento())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_TESTATADOCUMENTO_WHERE);

            query.append(_FINDER_COLUMN_STATOTIPODOCUMENTO_STATO_2);

            boolean bindTipoDocumento = false;

            if (tipoDocumento == null) {
                query.append(_FINDER_COLUMN_STATOTIPODOCUMENTO_TIPODOCUMENTO_1);
            } else if (tipoDocumento.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_STATOTIPODOCUMENTO_TIPODOCUMENTO_3);
            } else {
                bindTipoDocumento = true;

                query.append(_FINDER_COLUMN_STATOTIPODOCUMENTO_TIPODOCUMENTO_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TestataDocumentoModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(stato);

                if (bindTipoDocumento) {
                    qPos.add(tipoDocumento);
                }

                if (!pagination) {
                    list = (List<TestataDocumento>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<TestataDocumento>(list);
                } else {
                    list = (List<TestataDocumento>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first testata documento in the ordered set where stato = &#63; and tipoDocumento = &#63;.
     *
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento findByStatoTipoDocumento_First(boolean stato,
        String tipoDocumento, OrderByComparator orderByComparator)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = fetchByStatoTipoDocumento_First(stato,
                tipoDocumento, orderByComparator);

        if (testataDocumento != null) {
            return testataDocumento;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("stato=");
        msg.append(stato);

        msg.append(", tipoDocumento=");
        msg.append(tipoDocumento);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTestataDocumentoException(msg.toString());
    }

    /**
     * Returns the first testata documento in the ordered set where stato = &#63; and tipoDocumento = &#63;.
     *
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching testata documento, or <code>null</code> if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento fetchByStatoTipoDocumento_First(boolean stato,
        String tipoDocumento, OrderByComparator orderByComparator)
        throws SystemException {
        List<TestataDocumento> list = findByStatoTipoDocumento(stato,
                tipoDocumento, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last testata documento in the ordered set where stato = &#63; and tipoDocumento = &#63;.
     *
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento findByStatoTipoDocumento_Last(boolean stato,
        String tipoDocumento, OrderByComparator orderByComparator)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = fetchByStatoTipoDocumento_Last(stato,
                tipoDocumento, orderByComparator);

        if (testataDocumento != null) {
            return testataDocumento;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("stato=");
        msg.append(stato);

        msg.append(", tipoDocumento=");
        msg.append(tipoDocumento);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTestataDocumentoException(msg.toString());
    }

    /**
     * Returns the last testata documento in the ordered set where stato = &#63; and tipoDocumento = &#63;.
     *
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching testata documento, or <code>null</code> if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento fetchByStatoTipoDocumento_Last(boolean stato,
        String tipoDocumento, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByStatoTipoDocumento(stato, tipoDocumento);

        if (count == 0) {
            return null;
        }

        List<TestataDocumento> list = findByStatoTipoDocumento(stato,
                tipoDocumento, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the testata documentos before and after the current testata documento in the ordered set where stato = &#63; and tipoDocumento = &#63;.
     *
     * @param testataDocumentoPK the primary key of the current testata documento
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento[] findByStatoTipoDocumento_PrevAndNext(
        TestataDocumentoPK testataDocumentoPK, boolean stato,
        String tipoDocumento, OrderByComparator orderByComparator)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = findByPrimaryKey(testataDocumentoPK);

        Session session = null;

        try {
            session = openSession();

            TestataDocumento[] array = new TestataDocumentoImpl[3];

            array[0] = getByStatoTipoDocumento_PrevAndNext(session,
                    testataDocumento, stato, tipoDocumento, orderByComparator,
                    true);

            array[1] = testataDocumento;

            array[2] = getByStatoTipoDocumento_PrevAndNext(session,
                    testataDocumento, stato, tipoDocumento, orderByComparator,
                    false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected TestataDocumento getByStatoTipoDocumento_PrevAndNext(
        Session session, TestataDocumento testataDocumento, boolean stato,
        String tipoDocumento, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TESTATADOCUMENTO_WHERE);

        query.append(_FINDER_COLUMN_STATOTIPODOCUMENTO_STATO_2);

        boolean bindTipoDocumento = false;

        if (tipoDocumento == null) {
            query.append(_FINDER_COLUMN_STATOTIPODOCUMENTO_TIPODOCUMENTO_1);
        } else if (tipoDocumento.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_STATOTIPODOCUMENTO_TIPODOCUMENTO_3);
        } else {
            bindTipoDocumento = true;

            query.append(_FINDER_COLUMN_STATOTIPODOCUMENTO_TIPODOCUMENTO_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TestataDocumentoModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(stato);

        if (bindTipoDocumento) {
            qPos.add(tipoDocumento);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(testataDocumento);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<TestataDocumento> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the testata documentos where stato = &#63; and tipoDocumento = &#63; from the database.
     *
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByStatoTipoDocumento(boolean stato, String tipoDocumento)
        throws SystemException {
        for (TestataDocumento testataDocumento : findByStatoTipoDocumento(
                stato, tipoDocumento, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(testataDocumento);
        }
    }

    /**
     * Returns the number of testata documentos where stato = &#63; and tipoDocumento = &#63;.
     *
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @return the number of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByStatoTipoDocumento(boolean stato, String tipoDocumento)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_STATOTIPODOCUMENTO;

        Object[] finderArgs = new Object[] { stato, tipoDocumento };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_TESTATADOCUMENTO_WHERE);

            query.append(_FINDER_COLUMN_STATOTIPODOCUMENTO_STATO_2);

            boolean bindTipoDocumento = false;

            if (tipoDocumento == null) {
                query.append(_FINDER_COLUMN_STATOTIPODOCUMENTO_TIPODOCUMENTO_1);
            } else if (tipoDocumento.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_STATOTIPODOCUMENTO_TIPODOCUMENTO_3);
            } else {
                bindTipoDocumento = true;

                query.append(_FINDER_COLUMN_STATOTIPODOCUMENTO_TIPODOCUMENTO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(stato);

                if (bindTipoDocumento) {
                    qPos.add(tipoDocumento);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the testata documentos where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
     *
     * @param libLng1 the lib lng1
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @return the matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findByIdOrdineTipoDocumentoStato(
        long libLng1, boolean stato, String tipoDocumento)
        throws SystemException {
        return findByIdOrdineTipoDocumentoStato(libLng1, stato, tipoDocumento,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the testata documentos where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param libLng1 the lib lng1
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param start the lower bound of the range of testata documentos
     * @param end the upper bound of the range of testata documentos (not inclusive)
     * @return the range of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findByIdOrdineTipoDocumentoStato(
        long libLng1, boolean stato, String tipoDocumento, int start, int end)
        throws SystemException {
        return findByIdOrdineTipoDocumentoStato(libLng1, stato, tipoDocumento,
            start, end, null);
    }

    /**
     * Returns an ordered range of all the testata documentos where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param libLng1 the lib lng1
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param start the lower bound of the range of testata documentos
     * @param end the upper bound of the range of testata documentos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findByIdOrdineTipoDocumentoStato(
        long libLng1, boolean stato, String tipoDocumento, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDORDINETIPODOCUMENTOSTATO;
            finderArgs = new Object[] { libLng1, stato, tipoDocumento };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDORDINETIPODOCUMENTOSTATO;
            finderArgs = new Object[] {
                    libLng1, stato, tipoDocumento,
                    
                    start, end, orderByComparator
                };
        }

        List<TestataDocumento> list = (List<TestataDocumento>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (TestataDocumento testataDocumento : list) {
                if ((libLng1 != testataDocumento.getLibLng1()) ||
                        (stato != testataDocumento.getStato()) ||
                        !Validator.equals(tipoDocumento,
                            testataDocumento.getTipoDocumento())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(5 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(5);
            }

            query.append(_SQL_SELECT_TESTATADOCUMENTO_WHERE);

            query.append(_FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_LIBLNG1_2);

            query.append(_FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_STATO_2);

            boolean bindTipoDocumento = false;

            if (tipoDocumento == null) {
                query.append(_FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_TIPODOCUMENTO_1);
            } else if (tipoDocumento.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_TIPODOCUMENTO_3);
            } else {
                bindTipoDocumento = true;

                query.append(_FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_TIPODOCUMENTO_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TestataDocumentoModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(libLng1);

                qPos.add(stato);

                if (bindTipoDocumento) {
                    qPos.add(tipoDocumento);
                }

                if (!pagination) {
                    list = (List<TestataDocumento>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<TestataDocumento>(list);
                } else {
                    list = (List<TestataDocumento>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first testata documento in the ordered set where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
     *
     * @param libLng1 the lib lng1
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento findByIdOrdineTipoDocumentoStato_First(
        long libLng1, boolean stato, String tipoDocumento,
        OrderByComparator orderByComparator)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = fetchByIdOrdineTipoDocumentoStato_First(libLng1,
                stato, tipoDocumento, orderByComparator);

        if (testataDocumento != null) {
            return testataDocumento;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("libLng1=");
        msg.append(libLng1);

        msg.append(", stato=");
        msg.append(stato);

        msg.append(", tipoDocumento=");
        msg.append(tipoDocumento);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTestataDocumentoException(msg.toString());
    }

    /**
     * Returns the first testata documento in the ordered set where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
     *
     * @param libLng1 the lib lng1
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching testata documento, or <code>null</code> if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento fetchByIdOrdineTipoDocumentoStato_First(
        long libLng1, boolean stato, String tipoDocumento,
        OrderByComparator orderByComparator) throws SystemException {
        List<TestataDocumento> list = findByIdOrdineTipoDocumentoStato(libLng1,
                stato, tipoDocumento, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last testata documento in the ordered set where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
     *
     * @param libLng1 the lib lng1
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento findByIdOrdineTipoDocumentoStato_Last(
        long libLng1, boolean stato, String tipoDocumento,
        OrderByComparator orderByComparator)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = fetchByIdOrdineTipoDocumentoStato_Last(libLng1,
                stato, tipoDocumento, orderByComparator);

        if (testataDocumento != null) {
            return testataDocumento;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("libLng1=");
        msg.append(libLng1);

        msg.append(", stato=");
        msg.append(stato);

        msg.append(", tipoDocumento=");
        msg.append(tipoDocumento);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTestataDocumentoException(msg.toString());
    }

    /**
     * Returns the last testata documento in the ordered set where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
     *
     * @param libLng1 the lib lng1
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching testata documento, or <code>null</code> if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento fetchByIdOrdineTipoDocumentoStato_Last(
        long libLng1, boolean stato, String tipoDocumento,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByIdOrdineTipoDocumentoStato(libLng1, stato,
                tipoDocumento);

        if (count == 0) {
            return null;
        }

        List<TestataDocumento> list = findByIdOrdineTipoDocumentoStato(libLng1,
                stato, tipoDocumento, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the testata documentos before and after the current testata documento in the ordered set where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
     *
     * @param testataDocumentoPK the primary key of the current testata documento
     * @param libLng1 the lib lng1
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento[] findByIdOrdineTipoDocumentoStato_PrevAndNext(
        TestataDocumentoPK testataDocumentoPK, long libLng1, boolean stato,
        String tipoDocumento, OrderByComparator orderByComparator)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = findByPrimaryKey(testataDocumentoPK);

        Session session = null;

        try {
            session = openSession();

            TestataDocumento[] array = new TestataDocumentoImpl[3];

            array[0] = getByIdOrdineTipoDocumentoStato_PrevAndNext(session,
                    testataDocumento, libLng1, stato, tipoDocumento,
                    orderByComparator, true);

            array[1] = testataDocumento;

            array[2] = getByIdOrdineTipoDocumentoStato_PrevAndNext(session,
                    testataDocumento, libLng1, stato, tipoDocumento,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected TestataDocumento getByIdOrdineTipoDocumentoStato_PrevAndNext(
        Session session, TestataDocumento testataDocumento, long libLng1,
        boolean stato, String tipoDocumento,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TESTATADOCUMENTO_WHERE);

        query.append(_FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_LIBLNG1_2);

        query.append(_FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_STATO_2);

        boolean bindTipoDocumento = false;

        if (tipoDocumento == null) {
            query.append(_FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_TIPODOCUMENTO_1);
        } else if (tipoDocumento.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_TIPODOCUMENTO_3);
        } else {
            bindTipoDocumento = true;

            query.append(_FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_TIPODOCUMENTO_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TestataDocumentoModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(libLng1);

        qPos.add(stato);

        if (bindTipoDocumento) {
            qPos.add(tipoDocumento);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(testataDocumento);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<TestataDocumento> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the testata documentos where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63; from the database.
     *
     * @param libLng1 the lib lng1
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByIdOrdineTipoDocumentoStato(long libLng1, boolean stato,
        String tipoDocumento) throws SystemException {
        for (TestataDocumento testataDocumento : findByIdOrdineTipoDocumentoStato(
                libLng1, stato, tipoDocumento, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(testataDocumento);
        }
    }

    /**
     * Returns the number of testata documentos where libLng1 = &#63; and stato = &#63; and tipoDocumento = &#63;.
     *
     * @param libLng1 the lib lng1
     * @param stato the stato
     * @param tipoDocumento the tipo documento
     * @return the number of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByIdOrdineTipoDocumentoStato(long libLng1, boolean stato,
        String tipoDocumento) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_IDORDINETIPODOCUMENTOSTATO;

        Object[] finderArgs = new Object[] { libLng1, stato, tipoDocumento };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(4);

            query.append(_SQL_COUNT_TESTATADOCUMENTO_WHERE);

            query.append(_FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_LIBLNG1_2);

            query.append(_FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_STATO_2);

            boolean bindTipoDocumento = false;

            if (tipoDocumento == null) {
                query.append(_FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_TIPODOCUMENTO_1);
            } else if (tipoDocumento.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_TIPODOCUMENTO_3);
            } else {
                bindTipoDocumento = true;

                query.append(_FINDER_COLUMN_IDORDINETIPODOCUMENTOSTATO_TIPODOCUMENTO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(libLng1);

                qPos.add(stato);

                if (bindTipoDocumento) {
                    qPos.add(tipoDocumento);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the testata documentos where libLng1 = &#63; and tipoDocumento = &#63;.
     *
     * @param libLng1 the lib lng1
     * @param tipoDocumento the tipo documento
     * @return the matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findByIdOrdineTipo(long libLng1,
        String tipoDocumento) throws SystemException {
        return findByIdOrdineTipo(libLng1, tipoDocumento, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the testata documentos where libLng1 = &#63; and tipoDocumento = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param libLng1 the lib lng1
     * @param tipoDocumento the tipo documento
     * @param start the lower bound of the range of testata documentos
     * @param end the upper bound of the range of testata documentos (not inclusive)
     * @return the range of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findByIdOrdineTipo(long libLng1,
        String tipoDocumento, int start, int end) throws SystemException {
        return findByIdOrdineTipo(libLng1, tipoDocumento, start, end, null);
    }

    /**
     * Returns an ordered range of all the testata documentos where libLng1 = &#63; and tipoDocumento = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param libLng1 the lib lng1
     * @param tipoDocumento the tipo documento
     * @param start the lower bound of the range of testata documentos
     * @param end the upper bound of the range of testata documentos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findByIdOrdineTipo(long libLng1,
        String tipoDocumento, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDORDINETIPO;
            finderArgs = new Object[] { libLng1, tipoDocumento };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDORDINETIPO;
            finderArgs = new Object[] {
                    libLng1, tipoDocumento,
                    
                    start, end, orderByComparator
                };
        }

        List<TestataDocumento> list = (List<TestataDocumento>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (TestataDocumento testataDocumento : list) {
                if ((libLng1 != testataDocumento.getLibLng1()) ||
                        !Validator.equals(tipoDocumento,
                            testataDocumento.getTipoDocumento())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_TESTATADOCUMENTO_WHERE);

            query.append(_FINDER_COLUMN_IDORDINETIPO_LIBLNG1_2);

            boolean bindTipoDocumento = false;

            if (tipoDocumento == null) {
                query.append(_FINDER_COLUMN_IDORDINETIPO_TIPODOCUMENTO_1);
            } else if (tipoDocumento.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_IDORDINETIPO_TIPODOCUMENTO_3);
            } else {
                bindTipoDocumento = true;

                query.append(_FINDER_COLUMN_IDORDINETIPO_TIPODOCUMENTO_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TestataDocumentoModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(libLng1);

                if (bindTipoDocumento) {
                    qPos.add(tipoDocumento);
                }

                if (!pagination) {
                    list = (List<TestataDocumento>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<TestataDocumento>(list);
                } else {
                    list = (List<TestataDocumento>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first testata documento in the ordered set where libLng1 = &#63; and tipoDocumento = &#63;.
     *
     * @param libLng1 the lib lng1
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento findByIdOrdineTipo_First(long libLng1,
        String tipoDocumento, OrderByComparator orderByComparator)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = fetchByIdOrdineTipo_First(libLng1,
                tipoDocumento, orderByComparator);

        if (testataDocumento != null) {
            return testataDocumento;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("libLng1=");
        msg.append(libLng1);

        msg.append(", tipoDocumento=");
        msg.append(tipoDocumento);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTestataDocumentoException(msg.toString());
    }

    /**
     * Returns the first testata documento in the ordered set where libLng1 = &#63; and tipoDocumento = &#63;.
     *
     * @param libLng1 the lib lng1
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching testata documento, or <code>null</code> if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento fetchByIdOrdineTipo_First(long libLng1,
        String tipoDocumento, OrderByComparator orderByComparator)
        throws SystemException {
        List<TestataDocumento> list = findByIdOrdineTipo(libLng1,
                tipoDocumento, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last testata documento in the ordered set where libLng1 = &#63; and tipoDocumento = &#63;.
     *
     * @param libLng1 the lib lng1
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento findByIdOrdineTipo_Last(long libLng1,
        String tipoDocumento, OrderByComparator orderByComparator)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = fetchByIdOrdineTipo_Last(libLng1,
                tipoDocumento, orderByComparator);

        if (testataDocumento != null) {
            return testataDocumento;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("libLng1=");
        msg.append(libLng1);

        msg.append(", tipoDocumento=");
        msg.append(tipoDocumento);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTestataDocumentoException(msg.toString());
    }

    /**
     * Returns the last testata documento in the ordered set where libLng1 = &#63; and tipoDocumento = &#63;.
     *
     * @param libLng1 the lib lng1
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching testata documento, or <code>null</code> if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento fetchByIdOrdineTipo_Last(long libLng1,
        String tipoDocumento, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByIdOrdineTipo(libLng1, tipoDocumento);

        if (count == 0) {
            return null;
        }

        List<TestataDocumento> list = findByIdOrdineTipo(libLng1,
                tipoDocumento, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the testata documentos before and after the current testata documento in the ordered set where libLng1 = &#63; and tipoDocumento = &#63;.
     *
     * @param testataDocumentoPK the primary key of the current testata documento
     * @param libLng1 the lib lng1
     * @param tipoDocumento the tipo documento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento[] findByIdOrdineTipo_PrevAndNext(
        TestataDocumentoPK testataDocumentoPK, long libLng1,
        String tipoDocumento, OrderByComparator orderByComparator)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = findByPrimaryKey(testataDocumentoPK);

        Session session = null;

        try {
            session = openSession();

            TestataDocumento[] array = new TestataDocumentoImpl[3];

            array[0] = getByIdOrdineTipo_PrevAndNext(session, testataDocumento,
                    libLng1, tipoDocumento, orderByComparator, true);

            array[1] = testataDocumento;

            array[2] = getByIdOrdineTipo_PrevAndNext(session, testataDocumento,
                    libLng1, tipoDocumento, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected TestataDocumento getByIdOrdineTipo_PrevAndNext(Session session,
        TestataDocumento testataDocumento, long libLng1, String tipoDocumento,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TESTATADOCUMENTO_WHERE);

        query.append(_FINDER_COLUMN_IDORDINETIPO_LIBLNG1_2);

        boolean bindTipoDocumento = false;

        if (tipoDocumento == null) {
            query.append(_FINDER_COLUMN_IDORDINETIPO_TIPODOCUMENTO_1);
        } else if (tipoDocumento.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_IDORDINETIPO_TIPODOCUMENTO_3);
        } else {
            bindTipoDocumento = true;

            query.append(_FINDER_COLUMN_IDORDINETIPO_TIPODOCUMENTO_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TestataDocumentoModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(libLng1);

        if (bindTipoDocumento) {
            qPos.add(tipoDocumento);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(testataDocumento);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<TestataDocumento> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the testata documentos where libLng1 = &#63; and tipoDocumento = &#63; from the database.
     *
     * @param libLng1 the lib lng1
     * @param tipoDocumento the tipo documento
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByIdOrdineTipo(long libLng1, String tipoDocumento)
        throws SystemException {
        for (TestataDocumento testataDocumento : findByIdOrdineTipo(libLng1,
                tipoDocumento, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(testataDocumento);
        }
    }

    /**
     * Returns the number of testata documentos where libLng1 = &#63; and tipoDocumento = &#63;.
     *
     * @param libLng1 the lib lng1
     * @param tipoDocumento the tipo documento
     * @return the number of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByIdOrdineTipo(long libLng1, String tipoDocumento)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_IDORDINETIPO;

        Object[] finderArgs = new Object[] { libLng1, tipoDocumento };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_TESTATADOCUMENTO_WHERE);

            query.append(_FINDER_COLUMN_IDORDINETIPO_LIBLNG1_2);

            boolean bindTipoDocumento = false;

            if (tipoDocumento == null) {
                query.append(_FINDER_COLUMN_IDORDINETIPO_TIPODOCUMENTO_1);
            } else if (tipoDocumento.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_IDORDINETIPO_TIPODOCUMENTO_3);
            } else {
                bindTipoDocumento = true;

                query.append(_FINDER_COLUMN_IDORDINETIPO_TIPODOCUMENTO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(libLng1);

                if (bindTipoDocumento) {
                    qPos.add(tipoDocumento);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the testata documento where libLng1 = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63; or throws a {@link it.bysoftware.ct.NoSuchTestataDocumentoException} if it could not be found.
     *
     * @param libLng1 the lib lng1
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @return the matching testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento findByIdOrdineFornitoreTipo(long libLng1,
        String codiceFornitore, String tipoDocumento)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = fetchByIdOrdineFornitoreTipo(libLng1,
                codiceFornitore, tipoDocumento);

        if (testataDocumento == null) {
            StringBundler msg = new StringBundler(8);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("libLng1=");
            msg.append(libLng1);

            msg.append(", codiceFornitore=");
            msg.append(codiceFornitore);

            msg.append(", tipoDocumento=");
            msg.append(tipoDocumento);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchTestataDocumentoException(msg.toString());
        }

        return testataDocumento;
    }

    /**
     * Returns the testata documento where libLng1 = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param libLng1 the lib lng1
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @return the matching testata documento, or <code>null</code> if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento fetchByIdOrdineFornitoreTipo(long libLng1,
        String codiceFornitore, String tipoDocumento) throws SystemException {
        return fetchByIdOrdineFornitoreTipo(libLng1, codiceFornitore,
            tipoDocumento, true);
    }

    /**
     * Returns the testata documento where libLng1 = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param libLng1 the lib lng1
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching testata documento, or <code>null</code> if a matching testata documento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento fetchByIdOrdineFornitoreTipo(long libLng1,
        String codiceFornitore, String tipoDocumento, boolean retrieveFromCache)
        throws SystemException {
        Object[] finderArgs = new Object[] {
                libLng1, codiceFornitore, tipoDocumento
            };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_IDORDINEFORNITORETIPO,
                    finderArgs, this);
        }

        if (result instanceof TestataDocumento) {
            TestataDocumento testataDocumento = (TestataDocumento) result;

            if ((libLng1 != testataDocumento.getLibLng1()) ||
                    !Validator.equals(codiceFornitore,
                        testataDocumento.getCodiceFornitore()) ||
                    !Validator.equals(tipoDocumento,
                        testataDocumento.getTipoDocumento())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(5);

            query.append(_SQL_SELECT_TESTATADOCUMENTO_WHERE);

            query.append(_FINDER_COLUMN_IDORDINEFORNITORETIPO_LIBLNG1_2);

            boolean bindCodiceFornitore = false;

            if (codiceFornitore == null) {
                query.append(_FINDER_COLUMN_IDORDINEFORNITORETIPO_CODICEFORNITORE_1);
            } else if (codiceFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_IDORDINEFORNITORETIPO_CODICEFORNITORE_3);
            } else {
                bindCodiceFornitore = true;

                query.append(_FINDER_COLUMN_IDORDINEFORNITORETIPO_CODICEFORNITORE_2);
            }

            boolean bindTipoDocumento = false;

            if (tipoDocumento == null) {
                query.append(_FINDER_COLUMN_IDORDINEFORNITORETIPO_TIPODOCUMENTO_1);
            } else if (tipoDocumento.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_IDORDINEFORNITORETIPO_TIPODOCUMENTO_3);
            } else {
                bindTipoDocumento = true;

                query.append(_FINDER_COLUMN_IDORDINEFORNITORETIPO_TIPODOCUMENTO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(libLng1);

                if (bindCodiceFornitore) {
                    qPos.add(codiceFornitore);
                }

                if (bindTipoDocumento) {
                    qPos.add(tipoDocumento);
                }

                List<TestataDocumento> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDORDINEFORNITORETIPO,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "TestataDocumentoPersistenceImpl.fetchByIdOrdineFornitoreTipo(long, String, String, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    TestataDocumento testataDocumento = list.get(0);

                    result = testataDocumento;

                    cacheResult(testataDocumento);

                    if ((testataDocumento.getLibLng1() != libLng1) ||
                            (testataDocumento.getCodiceFornitore() == null) ||
                            !testataDocumento.getCodiceFornitore()
                                                 .equals(codiceFornitore) ||
                            (testataDocumento.getTipoDocumento() == null) ||
                            !testataDocumento.getTipoDocumento()
                                                 .equals(tipoDocumento)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDORDINEFORNITORETIPO,
                            finderArgs, testataDocumento);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDORDINEFORNITORETIPO,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (TestataDocumento) result;
        }
    }

    /**
     * Removes the testata documento where libLng1 = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63; from the database.
     *
     * @param libLng1 the lib lng1
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @return the testata documento that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento removeByIdOrdineFornitoreTipo(long libLng1,
        String codiceFornitore, String tipoDocumento)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = findByIdOrdineFornitoreTipo(libLng1,
                codiceFornitore, tipoDocumento);

        return remove(testataDocumento);
    }

    /**
     * Returns the number of testata documentos where libLng1 = &#63; and codiceFornitore = &#63; and tipoDocumento = &#63;.
     *
     * @param libLng1 the lib lng1
     * @param codiceFornitore the codice fornitore
     * @param tipoDocumento the tipo documento
     * @return the number of matching testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByIdOrdineFornitoreTipo(long libLng1,
        String codiceFornitore, String tipoDocumento) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_IDORDINEFORNITORETIPO;

        Object[] finderArgs = new Object[] {
                libLng1, codiceFornitore, tipoDocumento
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(4);

            query.append(_SQL_COUNT_TESTATADOCUMENTO_WHERE);

            query.append(_FINDER_COLUMN_IDORDINEFORNITORETIPO_LIBLNG1_2);

            boolean bindCodiceFornitore = false;

            if (codiceFornitore == null) {
                query.append(_FINDER_COLUMN_IDORDINEFORNITORETIPO_CODICEFORNITORE_1);
            } else if (codiceFornitore.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_IDORDINEFORNITORETIPO_CODICEFORNITORE_3);
            } else {
                bindCodiceFornitore = true;

                query.append(_FINDER_COLUMN_IDORDINEFORNITORETIPO_CODICEFORNITORE_2);
            }

            boolean bindTipoDocumento = false;

            if (tipoDocumento == null) {
                query.append(_FINDER_COLUMN_IDORDINEFORNITORETIPO_TIPODOCUMENTO_1);
            } else if (tipoDocumento.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_IDORDINEFORNITORETIPO_TIPODOCUMENTO_3);
            } else {
                bindTipoDocumento = true;

                query.append(_FINDER_COLUMN_IDORDINEFORNITORETIPO_TIPODOCUMENTO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(libLng1);

                if (bindCodiceFornitore) {
                    qPos.add(codiceFornitore);
                }

                if (bindTipoDocumento) {
                    qPos.add(tipoDocumento);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the testata documento in the entity cache if it is enabled.
     *
     * @param testataDocumento the testata documento
     */
    @Override
    public void cacheResult(TestataDocumento testataDocumento) {
        EntityCacheUtil.putResult(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoImpl.class, testataDocumento.getPrimaryKey(),
            testataDocumento);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDORDINEFORNITORETIPO,
            new Object[] {
                testataDocumento.getLibLng1(),
                testataDocumento.getCodiceFornitore(),
                testataDocumento.getTipoDocumento()
            }, testataDocumento);

        testataDocumento.resetOriginalValues();
    }

    /**
     * Caches the testata documentos in the entity cache if it is enabled.
     *
     * @param testataDocumentos the testata documentos
     */
    @Override
    public void cacheResult(List<TestataDocumento> testataDocumentos) {
        for (TestataDocumento testataDocumento : testataDocumentos) {
            if (EntityCacheUtil.getResult(
                        TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
                        TestataDocumentoImpl.class,
                        testataDocumento.getPrimaryKey()) == null) {
                cacheResult(testataDocumento);
            } else {
                testataDocumento.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all testata documentos.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(TestataDocumentoImpl.class.getName());
        }

        EntityCacheUtil.clearCache(TestataDocumentoImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the testata documento.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(TestataDocumento testataDocumento) {
        EntityCacheUtil.removeResult(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoImpl.class, testataDocumento.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        clearUniqueFindersCache(testataDocumento);
    }

    @Override
    public void clearCache(List<TestataDocumento> testataDocumentos) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (TestataDocumento testataDocumento : testataDocumentos) {
            EntityCacheUtil.removeResult(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
                TestataDocumentoImpl.class, testataDocumento.getPrimaryKey());

            clearUniqueFindersCache(testataDocumento);
        }
    }

    protected void cacheUniqueFindersCache(TestataDocumento testataDocumento) {
        if (testataDocumento.isNew()) {
            Object[] args = new Object[] {
                    testataDocumento.getLibLng1(),
                    testataDocumento.getCodiceFornitore(),
                    testataDocumento.getTipoDocumento()
                };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_IDORDINEFORNITORETIPO,
                args, Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDORDINEFORNITORETIPO,
                args, testataDocumento);
        } else {
            TestataDocumentoModelImpl testataDocumentoModelImpl = (TestataDocumentoModelImpl) testataDocumento;

            if ((testataDocumentoModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_IDORDINEFORNITORETIPO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        testataDocumento.getLibLng1(),
                        testataDocumento.getCodiceFornitore(),
                        testataDocumento.getTipoDocumento()
                    };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_IDORDINEFORNITORETIPO,
                    args, Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDORDINEFORNITORETIPO,
                    args, testataDocumento);
            }
        }
    }

    protected void clearUniqueFindersCache(TestataDocumento testataDocumento) {
        TestataDocumentoModelImpl testataDocumentoModelImpl = (TestataDocumentoModelImpl) testataDocumento;

        Object[] args = new Object[] {
                testataDocumento.getLibLng1(),
                testataDocumento.getCodiceFornitore(),
                testataDocumento.getTipoDocumento()
            };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDORDINEFORNITORETIPO,
            args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDORDINEFORNITORETIPO,
            args);

        if ((testataDocumentoModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_IDORDINEFORNITORETIPO.getColumnBitmask()) != 0) {
            args = new Object[] {
                    testataDocumentoModelImpl.getOriginalLibLng1(),
                    testataDocumentoModelImpl.getOriginalCodiceFornitore(),
                    testataDocumentoModelImpl.getOriginalTipoDocumento()
                };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDORDINEFORNITORETIPO,
                args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDORDINEFORNITORETIPO,
                args);
        }
    }

    /**
     * Creates a new testata documento with the primary key. Does not add the testata documento to the database.
     *
     * @param testataDocumentoPK the primary key for the new testata documento
     * @return the new testata documento
     */
    @Override
    public TestataDocumento create(TestataDocumentoPK testataDocumentoPK) {
        TestataDocumento testataDocumento = new TestataDocumentoImpl();

        testataDocumento.setNew(true);
        testataDocumento.setPrimaryKey(testataDocumentoPK);

        return testataDocumento;
    }

    /**
     * Removes the testata documento with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param testataDocumentoPK the primary key of the testata documento
     * @return the testata documento that was removed
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento remove(TestataDocumentoPK testataDocumentoPK)
        throws NoSuchTestataDocumentoException, SystemException {
        return remove((Serializable) testataDocumentoPK);
    }

    /**
     * Removes the testata documento with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the testata documento
     * @return the testata documento that was removed
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento remove(Serializable primaryKey)
        throws NoSuchTestataDocumentoException, SystemException {
        Session session = null;

        try {
            session = openSession();

            TestataDocumento testataDocumento = (TestataDocumento) session.get(TestataDocumentoImpl.class,
                    primaryKey);

            if (testataDocumento == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchTestataDocumentoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(testataDocumento);
        } catch (NoSuchTestataDocumentoException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected TestataDocumento removeImpl(TestataDocumento testataDocumento)
        throws SystemException {
        testataDocumento = toUnwrappedModel(testataDocumento);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(testataDocumento)) {
                testataDocumento = (TestataDocumento) session.get(TestataDocumentoImpl.class,
                        testataDocumento.getPrimaryKeyObj());
            }

            if (testataDocumento != null) {
                session.delete(testataDocumento);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (testataDocumento != null) {
            clearCache(testataDocumento);
        }

        return testataDocumento;
    }

    @Override
    public TestataDocumento updateImpl(
        it.bysoftware.ct.model.TestataDocumento testataDocumento)
        throws SystemException {
        testataDocumento = toUnwrappedModel(testataDocumento);

        boolean isNew = testataDocumento.isNew();

        TestataDocumentoModelImpl testataDocumentoModelImpl = (TestataDocumentoModelImpl) testataDocumento;

        Session session = null;

        try {
            session = openSession();

            if (testataDocumento.isNew()) {
                session.save(testataDocumento);

                testataDocumento.setNew(false);
            } else {
                session.merge(testataDocumento);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !TestataDocumentoModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((testataDocumentoModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOATTIVITACENTRODEPOSITO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        testataDocumentoModelImpl.getOriginalAnno(),
                        testataDocumentoModelImpl.getOriginalCodiceAttivita(),
                        testataDocumentoModelImpl.getOriginalCodiceCentro(),
                        testataDocumentoModelImpl.getOriginalCodiceDeposito(),
                        testataDocumentoModelImpl.getOriginalCodiceFornitore(),
                        testataDocumentoModelImpl.getOriginalTipoDocumento()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ANNOATTIVITACENTRODEPOSITO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOATTIVITACENTRODEPOSITO,
                    args);

                args = new Object[] {
                        testataDocumentoModelImpl.getAnno(),
                        testataDocumentoModelImpl.getCodiceAttivita(),
                        testataDocumentoModelImpl.getCodiceCentro(),
                        testataDocumentoModelImpl.getCodiceDeposito(),
                        testataDocumentoModelImpl.getCodiceFornitore(),
                        testataDocumentoModelImpl.getTipoDocumento()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ANNOATTIVITACENTRODEPOSITO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOATTIVITACENTRODEPOSITO,
                    args);
            }

            if ((testataDocumentoModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DATASTATOTIPODOCUMENTO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        testataDocumentoModelImpl.getOriginalDataRegistrazione(),
                        testataDocumentoModelImpl.getOriginalStato(),
                        testataDocumentoModelImpl.getOriginalTipoDocumento()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DATASTATOTIPODOCUMENTO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DATASTATOTIPODOCUMENTO,
                    args);

                args = new Object[] {
                        testataDocumentoModelImpl.getDataRegistrazione(),
                        testataDocumentoModelImpl.getStato(),
                        testataDocumentoModelImpl.getTipoDocumento()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DATASTATOTIPODOCUMENTO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DATASTATOTIPODOCUMENTO,
                    args);
            }

            if ((testataDocumentoModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DATAFORNITORETIPODOCUMENTO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        testataDocumentoModelImpl.getOriginalDataRegistrazione(),
                        testataDocumentoModelImpl.getOriginalCodiceFornitore(),
                        testataDocumentoModelImpl.getOriginalTipoDocumento()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DATAFORNITORETIPODOCUMENTO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DATAFORNITORETIPODOCUMENTO,
                    args);

                args = new Object[] {
                        testataDocumentoModelImpl.getDataRegistrazione(),
                        testataDocumentoModelImpl.getCodiceFornitore(),
                        testataDocumentoModelImpl.getTipoDocumento()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DATAFORNITORETIPODOCUMENTO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DATAFORNITORETIPODOCUMENTO,
                    args);
            }

            if ((testataDocumentoModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATOTIPODOCUMENTO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        testataDocumentoModelImpl.getOriginalStato(),
                        testataDocumentoModelImpl.getOriginalTipoDocumento()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATOTIPODOCUMENTO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATOTIPODOCUMENTO,
                    args);

                args = new Object[] {
                        testataDocumentoModelImpl.getStato(),
                        testataDocumentoModelImpl.getTipoDocumento()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATOTIPODOCUMENTO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATOTIPODOCUMENTO,
                    args);
            }

            if ((testataDocumentoModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDORDINETIPODOCUMENTOSTATO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        testataDocumentoModelImpl.getOriginalLibLng1(),
                        testataDocumentoModelImpl.getOriginalStato(),
                        testataDocumentoModelImpl.getOriginalTipoDocumento()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDORDINETIPODOCUMENTOSTATO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDORDINETIPODOCUMENTOSTATO,
                    args);

                args = new Object[] {
                        testataDocumentoModelImpl.getLibLng1(),
                        testataDocumentoModelImpl.getStato(),
                        testataDocumentoModelImpl.getTipoDocumento()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDORDINETIPODOCUMENTOSTATO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDORDINETIPODOCUMENTOSTATO,
                    args);
            }

            if ((testataDocumentoModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDORDINETIPO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        testataDocumentoModelImpl.getOriginalLibLng1(),
                        testataDocumentoModelImpl.getOriginalTipoDocumento()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDORDINETIPO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDORDINETIPO,
                    args);

                args = new Object[] {
                        testataDocumentoModelImpl.getLibLng1(),
                        testataDocumentoModelImpl.getTipoDocumento()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDORDINETIPO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDORDINETIPO,
                    args);
            }
        }

        EntityCacheUtil.putResult(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
            TestataDocumentoImpl.class, testataDocumento.getPrimaryKey(),
            testataDocumento);

        clearUniqueFindersCache(testataDocumento);
        cacheUniqueFindersCache(testataDocumento);

        return testataDocumento;
    }

    protected TestataDocumento toUnwrappedModel(
        TestataDocumento testataDocumento) {
        if (testataDocumento instanceof TestataDocumentoImpl) {
            return testataDocumento;
        }

        TestataDocumentoImpl testataDocumentoImpl = new TestataDocumentoImpl();

        testataDocumentoImpl.setNew(testataDocumento.isNew());
        testataDocumentoImpl.setPrimaryKey(testataDocumento.getPrimaryKey());

        testataDocumentoImpl.setAnno(testataDocumento.getAnno());
        testataDocumentoImpl.setCodiceAttivita(testataDocumento.getCodiceAttivita());
        testataDocumentoImpl.setCodiceCentro(testataDocumento.getCodiceCentro());
        testataDocumentoImpl.setCodiceDeposito(testataDocumento.getCodiceDeposito());
        testataDocumentoImpl.setProtocollo(testataDocumento.getProtocollo());
        testataDocumentoImpl.setCodiceFornitore(testataDocumento.getCodiceFornitore());
        testataDocumentoImpl.setTipoDocumento(testataDocumento.getTipoDocumento());
        testataDocumentoImpl.setDataRegistrazione(testataDocumento.getDataRegistrazione());
        testataDocumentoImpl.setCodiceSpedizione(testataDocumento.getCodiceSpedizione());
        testataDocumentoImpl.setCodiceCausaleTrasporto(testataDocumento.getCodiceCausaleTrasporto());
        testataDocumentoImpl.setCodiceCuraTrasporto(testataDocumento.getCodiceCuraTrasporto());
        testataDocumentoImpl.setCodicePorto(testataDocumento.getCodicePorto());
        testataDocumentoImpl.setCodiceAspettoEsteriore(testataDocumento.getCodiceAspettoEsteriore());
        testataDocumentoImpl.setCodiceVettore1(testataDocumento.getCodiceVettore1());
        testataDocumentoImpl.setCodiceVettore2(testataDocumento.getCodiceVettore2());
        testataDocumentoImpl.setLibStr1(testataDocumento.getLibStr1());
        testataDocumentoImpl.setLibStr2(testataDocumento.getLibStr2());
        testataDocumentoImpl.setLibStr3(testataDocumento.getLibStr3());
        testataDocumentoImpl.setLibDbl1(testataDocumento.getLibDbl1());
        testataDocumentoImpl.setLibDbl2(testataDocumento.getLibDbl2());
        testataDocumentoImpl.setLibDbl3(testataDocumento.getLibDbl3());
        testataDocumentoImpl.setLibLng1(testataDocumento.getLibLng1());
        testataDocumentoImpl.setLibLng2(testataDocumento.getLibLng2());
        testataDocumentoImpl.setLibLng3(testataDocumento.getLibLng3());
        testataDocumentoImpl.setLibDat1(testataDocumento.getLibDat1());
        testataDocumentoImpl.setLibDat2(testataDocumento.getLibDat2());
        testataDocumentoImpl.setLibDat3(testataDocumento.getLibDat3());
        testataDocumentoImpl.setStato(testataDocumento.isStato());
        testataDocumentoImpl.setInviato(testataDocumento.isInviato());
        testataDocumentoImpl.setCodiceConsorzio(testataDocumento.getCodiceConsorzio());

        return testataDocumentoImpl;
    }

    /**
     * Returns the testata documento with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the testata documento
     * @return the testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento findByPrimaryKey(Serializable primaryKey)
        throws NoSuchTestataDocumentoException, SystemException {
        TestataDocumento testataDocumento = fetchByPrimaryKey(primaryKey);

        if (testataDocumento == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchTestataDocumentoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return testataDocumento;
    }

    /**
     * Returns the testata documento with the primary key or throws a {@link it.bysoftware.ct.NoSuchTestataDocumentoException} if it could not be found.
     *
     * @param testataDocumentoPK the primary key of the testata documento
     * @return the testata documento
     * @throws it.bysoftware.ct.NoSuchTestataDocumentoException if a testata documento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento findByPrimaryKey(
        TestataDocumentoPK testataDocumentoPK)
        throws NoSuchTestataDocumentoException, SystemException {
        return findByPrimaryKey((Serializable) testataDocumentoPK);
    }

    /**
     * Returns the testata documento with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the testata documento
     * @return the testata documento, or <code>null</code> if a testata documento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        TestataDocumento testataDocumento = (TestataDocumento) EntityCacheUtil.getResult(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
                TestataDocumentoImpl.class, primaryKey);

        if (testataDocumento == _nullTestataDocumento) {
            return null;
        }

        if (testataDocumento == null) {
            Session session = null;

            try {
                session = openSession();

                testataDocumento = (TestataDocumento) session.get(TestataDocumentoImpl.class,
                        primaryKey);

                if (testataDocumento != null) {
                    cacheResult(testataDocumento);
                } else {
                    EntityCacheUtil.putResult(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
                        TestataDocumentoImpl.class, primaryKey,
                        _nullTestataDocumento);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(TestataDocumentoModelImpl.ENTITY_CACHE_ENABLED,
                    TestataDocumentoImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return testataDocumento;
    }

    /**
     * Returns the testata documento with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param testataDocumentoPK the primary key of the testata documento
     * @return the testata documento, or <code>null</code> if a testata documento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TestataDocumento fetchByPrimaryKey(
        TestataDocumentoPK testataDocumentoPK) throws SystemException {
        return fetchByPrimaryKey((Serializable) testataDocumentoPK);
    }

    /**
     * Returns all the testata documentos.
     *
     * @return the testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the testata documentos.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of testata documentos
     * @param end the upper bound of the range of testata documentos (not inclusive)
     * @return the range of testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the testata documentos.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataDocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of testata documentos
     * @param end the upper bound of the range of testata documentos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TestataDocumento> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<TestataDocumento> list = (List<TestataDocumento>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_TESTATADOCUMENTO);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_TESTATADOCUMENTO;

                if (pagination) {
                    sql = sql.concat(TestataDocumentoModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<TestataDocumento>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<TestataDocumento>(list);
                } else {
                    list = (List<TestataDocumento>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the testata documentos from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (TestataDocumento testataDocumento : findAll()) {
            remove(testataDocumento);
        }
    }

    /**
     * Returns the number of testata documentos.
     *
     * @return the number of testata documentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_TESTATADOCUMENTO);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the testata documento persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.TestataDocumento")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<TestataDocumento>> listenersList = new ArrayList<ModelListener<TestataDocumento>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<TestataDocumento>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(TestataDocumentoImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
