package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchRubricaException;
import it.bysoftware.ct.model.Rubrica;
import it.bysoftware.ct.model.impl.RubricaImpl;
import it.bysoftware.ct.model.impl.RubricaModelImpl;
import it.bysoftware.ct.service.persistence.RubricaPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the rubrica service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see RubricaPersistence
 * @see RubricaUtil
 * @generated
 */
public class RubricaPersistenceImpl extends BasePersistenceImpl<Rubrica>
    implements RubricaPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link RubricaUtil} to access the rubrica persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = RubricaImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(RubricaModelImpl.ENTITY_CACHE_ENABLED,
            RubricaModelImpl.FINDER_CACHE_ENABLED, RubricaImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(RubricaModelImpl.ENTITY_CACHE_ENABLED,
            RubricaModelImpl.FINDER_CACHE_ENABLED, RubricaImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(RubricaModelImpl.ENTITY_CACHE_ENABLED,
            RubricaModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICESOGGETTO =
        new FinderPath(RubricaModelImpl.ENTITY_CACHE_ENABLED,
            RubricaModelImpl.FINDER_CACHE_ENABLED, RubricaImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCodiceSoggetto",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICESOGGETTO =
        new FinderPath(RubricaModelImpl.ENTITY_CACHE_ENABLED,
            RubricaModelImpl.FINDER_CACHE_ENABLED, RubricaImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCodiceSoggetto",
            new String[] { String.class.getName() },
            RubricaModelImpl.CODICESOGGETTO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_CODICESOGGETTO = new FinderPath(RubricaModelImpl.ENTITY_CACHE_ENABLED,
            RubricaModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCodiceSoggetto",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_1 = "rubrica.id.codiceSoggetto IS NULL";
    private static final String _FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_2 = "rubrica.id.codiceSoggetto = ?";
    private static final String _FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_3 = "(rubrica.id.codiceSoggetto IS NULL OR rubrica.id.codiceSoggetto = '')";
    private static final String _SQL_SELECT_RUBRICA = "SELECT rubrica FROM Rubrica rubrica";
    private static final String _SQL_SELECT_RUBRICA_WHERE = "SELECT rubrica FROM Rubrica rubrica WHERE ";
    private static final String _SQL_COUNT_RUBRICA = "SELECT COUNT(rubrica) FROM Rubrica rubrica";
    private static final String _SQL_COUNT_RUBRICA_WHERE = "SELECT COUNT(rubrica) FROM Rubrica rubrica WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "rubrica.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Rubrica exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Rubrica exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(RubricaPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "codiceSoggetto", "numeroVoce", "descrizione", "telefono",
                "cellulare", "fax", "eMailTo", "eMailCc", "eMailCcn"
            });
    private static Rubrica _nullRubrica = new RubricaImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Rubrica> toCacheModel() {
                return _nullRubricaCacheModel;
            }
        };

    private static CacheModel<Rubrica> _nullRubricaCacheModel = new CacheModel<Rubrica>() {
            @Override
            public Rubrica toEntityModel() {
                return _nullRubrica;
            }
        };

    public RubricaPersistenceImpl() {
        setModelClass(Rubrica.class);
    }

    /**
     * Returns all the rubricas where codiceSoggetto = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @return the matching rubricas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Rubrica> findByCodiceSoggetto(String codiceSoggetto)
        throws SystemException {
        return findByCodiceSoggetto(codiceSoggetto, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the rubricas where codiceSoggetto = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RubricaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceSoggetto the codice soggetto
     * @param start the lower bound of the range of rubricas
     * @param end the upper bound of the range of rubricas (not inclusive)
     * @return the range of matching rubricas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Rubrica> findByCodiceSoggetto(String codiceSoggetto, int start,
        int end) throws SystemException {
        return findByCodiceSoggetto(codiceSoggetto, start, end, null);
    }

    /**
     * Returns an ordered range of all the rubricas where codiceSoggetto = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RubricaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceSoggetto the codice soggetto
     * @param start the lower bound of the range of rubricas
     * @param end the upper bound of the range of rubricas (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching rubricas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Rubrica> findByCodiceSoggetto(String codiceSoggetto, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICESOGGETTO;
            finderArgs = new Object[] { codiceSoggetto };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICESOGGETTO;
            finderArgs = new Object[] {
                    codiceSoggetto,
                    
                    start, end, orderByComparator
                };
        }

        List<Rubrica> list = (List<Rubrica>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Rubrica rubrica : list) {
                if (!Validator.equals(codiceSoggetto,
                            rubrica.getCodiceSoggetto())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_RUBRICA_WHERE);

            boolean bindCodiceSoggetto = false;

            if (codiceSoggetto == null) {
                query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_1);
            } else if (codiceSoggetto.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_3);
            } else {
                bindCodiceSoggetto = true;

                query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(RubricaModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceSoggetto) {
                    qPos.add(codiceSoggetto);
                }

                if (!pagination) {
                    list = (List<Rubrica>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Rubrica>(list);
                } else {
                    list = (List<Rubrica>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first rubrica in the ordered set where codiceSoggetto = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching rubrica
     * @throws it.bysoftware.ct.NoSuchRubricaException if a matching rubrica could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Rubrica findByCodiceSoggetto_First(String codiceSoggetto,
        OrderByComparator orderByComparator)
        throws NoSuchRubricaException, SystemException {
        Rubrica rubrica = fetchByCodiceSoggetto_First(codiceSoggetto,
                orderByComparator);

        if (rubrica != null) {
            return rubrica;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceSoggetto=");
        msg.append(codiceSoggetto);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchRubricaException(msg.toString());
    }

    /**
     * Returns the first rubrica in the ordered set where codiceSoggetto = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching rubrica, or <code>null</code> if a matching rubrica could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Rubrica fetchByCodiceSoggetto_First(String codiceSoggetto,
        OrderByComparator orderByComparator) throws SystemException {
        List<Rubrica> list = findByCodiceSoggetto(codiceSoggetto, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last rubrica in the ordered set where codiceSoggetto = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching rubrica
     * @throws it.bysoftware.ct.NoSuchRubricaException if a matching rubrica could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Rubrica findByCodiceSoggetto_Last(String codiceSoggetto,
        OrderByComparator orderByComparator)
        throws NoSuchRubricaException, SystemException {
        Rubrica rubrica = fetchByCodiceSoggetto_Last(codiceSoggetto,
                orderByComparator);

        if (rubrica != null) {
            return rubrica;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceSoggetto=");
        msg.append(codiceSoggetto);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchRubricaException(msg.toString());
    }

    /**
     * Returns the last rubrica in the ordered set where codiceSoggetto = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching rubrica, or <code>null</code> if a matching rubrica could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Rubrica fetchByCodiceSoggetto_Last(String codiceSoggetto,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByCodiceSoggetto(codiceSoggetto);

        if (count == 0) {
            return null;
        }

        List<Rubrica> list = findByCodiceSoggetto(codiceSoggetto, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the rubricas before and after the current rubrica in the ordered set where codiceSoggetto = &#63;.
     *
     * @param rubricaPK the primary key of the current rubrica
     * @param codiceSoggetto the codice soggetto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next rubrica
     * @throws it.bysoftware.ct.NoSuchRubricaException if a rubrica with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Rubrica[] findByCodiceSoggetto_PrevAndNext(RubricaPK rubricaPK,
        String codiceSoggetto, OrderByComparator orderByComparator)
        throws NoSuchRubricaException, SystemException {
        Rubrica rubrica = findByPrimaryKey(rubricaPK);

        Session session = null;

        try {
            session = openSession();

            Rubrica[] array = new RubricaImpl[3];

            array[0] = getByCodiceSoggetto_PrevAndNext(session, rubrica,
                    codiceSoggetto, orderByComparator, true);

            array[1] = rubrica;

            array[2] = getByCodiceSoggetto_PrevAndNext(session, rubrica,
                    codiceSoggetto, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Rubrica getByCodiceSoggetto_PrevAndNext(Session session,
        Rubrica rubrica, String codiceSoggetto,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_RUBRICA_WHERE);

        boolean bindCodiceSoggetto = false;

        if (codiceSoggetto == null) {
            query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_1);
        } else if (codiceSoggetto.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_3);
        } else {
            bindCodiceSoggetto = true;

            query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(RubricaModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindCodiceSoggetto) {
            qPos.add(codiceSoggetto);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(rubrica);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Rubrica> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the rubricas where codiceSoggetto = &#63; from the database.
     *
     * @param codiceSoggetto the codice soggetto
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByCodiceSoggetto(String codiceSoggetto)
        throws SystemException {
        for (Rubrica rubrica : findByCodiceSoggetto(codiceSoggetto,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(rubrica);
        }
    }

    /**
     * Returns the number of rubricas where codiceSoggetto = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @return the number of matching rubricas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByCodiceSoggetto(String codiceSoggetto)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_CODICESOGGETTO;

        Object[] finderArgs = new Object[] { codiceSoggetto };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_RUBRICA_WHERE);

            boolean bindCodiceSoggetto = false;

            if (codiceSoggetto == null) {
                query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_1);
            } else if (codiceSoggetto.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_3);
            } else {
                bindCodiceSoggetto = true;

                query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceSoggetto) {
                    qPos.add(codiceSoggetto);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the rubrica in the entity cache if it is enabled.
     *
     * @param rubrica the rubrica
     */
    @Override
    public void cacheResult(Rubrica rubrica) {
        EntityCacheUtil.putResult(RubricaModelImpl.ENTITY_CACHE_ENABLED,
            RubricaImpl.class, rubrica.getPrimaryKey(), rubrica);

        rubrica.resetOriginalValues();
    }

    /**
     * Caches the rubricas in the entity cache if it is enabled.
     *
     * @param rubricas the rubricas
     */
    @Override
    public void cacheResult(List<Rubrica> rubricas) {
        for (Rubrica rubrica : rubricas) {
            if (EntityCacheUtil.getResult(
                        RubricaModelImpl.ENTITY_CACHE_ENABLED,
                        RubricaImpl.class, rubrica.getPrimaryKey()) == null) {
                cacheResult(rubrica);
            } else {
                rubrica.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all rubricas.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(RubricaImpl.class.getName());
        }

        EntityCacheUtil.clearCache(RubricaImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the rubrica.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Rubrica rubrica) {
        EntityCacheUtil.removeResult(RubricaModelImpl.ENTITY_CACHE_ENABLED,
            RubricaImpl.class, rubrica.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Rubrica> rubricas) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Rubrica rubrica : rubricas) {
            EntityCacheUtil.removeResult(RubricaModelImpl.ENTITY_CACHE_ENABLED,
                RubricaImpl.class, rubrica.getPrimaryKey());
        }
    }

    /**
     * Creates a new rubrica with the primary key. Does not add the rubrica to the database.
     *
     * @param rubricaPK the primary key for the new rubrica
     * @return the new rubrica
     */
    @Override
    public Rubrica create(RubricaPK rubricaPK) {
        Rubrica rubrica = new RubricaImpl();

        rubrica.setNew(true);
        rubrica.setPrimaryKey(rubricaPK);

        return rubrica;
    }

    /**
     * Removes the rubrica with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param rubricaPK the primary key of the rubrica
     * @return the rubrica that was removed
     * @throws it.bysoftware.ct.NoSuchRubricaException if a rubrica with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Rubrica remove(RubricaPK rubricaPK)
        throws NoSuchRubricaException, SystemException {
        return remove((Serializable) rubricaPK);
    }

    /**
     * Removes the rubrica with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the rubrica
     * @return the rubrica that was removed
     * @throws it.bysoftware.ct.NoSuchRubricaException if a rubrica with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Rubrica remove(Serializable primaryKey)
        throws NoSuchRubricaException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Rubrica rubrica = (Rubrica) session.get(RubricaImpl.class,
                    primaryKey);

            if (rubrica == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchRubricaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(rubrica);
        } catch (NoSuchRubricaException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Rubrica removeImpl(Rubrica rubrica) throws SystemException {
        rubrica = toUnwrappedModel(rubrica);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(rubrica)) {
                rubrica = (Rubrica) session.get(RubricaImpl.class,
                        rubrica.getPrimaryKeyObj());
            }

            if (rubrica != null) {
                session.delete(rubrica);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (rubrica != null) {
            clearCache(rubrica);
        }

        return rubrica;
    }

    @Override
    public Rubrica updateImpl(it.bysoftware.ct.model.Rubrica rubrica)
        throws SystemException {
        rubrica = toUnwrappedModel(rubrica);

        boolean isNew = rubrica.isNew();

        RubricaModelImpl rubricaModelImpl = (RubricaModelImpl) rubrica;

        Session session = null;

        try {
            session = openSession();

            if (rubrica.isNew()) {
                session.save(rubrica);

                rubrica.setNew(false);
            } else {
                session.merge(rubrica);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !RubricaModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((rubricaModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICESOGGETTO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        rubricaModelImpl.getOriginalCodiceSoggetto()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICESOGGETTO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICESOGGETTO,
                    args);

                args = new Object[] { rubricaModelImpl.getCodiceSoggetto() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICESOGGETTO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICESOGGETTO,
                    args);
            }
        }

        EntityCacheUtil.putResult(RubricaModelImpl.ENTITY_CACHE_ENABLED,
            RubricaImpl.class, rubrica.getPrimaryKey(), rubrica);

        return rubrica;
    }

    protected Rubrica toUnwrappedModel(Rubrica rubrica) {
        if (rubrica instanceof RubricaImpl) {
            return rubrica;
        }

        RubricaImpl rubricaImpl = new RubricaImpl();

        rubricaImpl.setNew(rubrica.isNew());
        rubricaImpl.setPrimaryKey(rubrica.getPrimaryKey());

        rubricaImpl.setCodiceSoggetto(rubrica.getCodiceSoggetto());
        rubricaImpl.setNumeroVoce(rubrica.getNumeroVoce());
        rubricaImpl.setDescrizione(rubrica.getDescrizione());
        rubricaImpl.setTelefono(rubrica.getTelefono());
        rubricaImpl.setCellulare(rubrica.getCellulare());
        rubricaImpl.setFax(rubrica.getFax());
        rubricaImpl.setEMailTo(rubrica.getEMailTo());
        rubricaImpl.setEMailCc(rubrica.getEMailCc());
        rubricaImpl.setEMailCcn(rubrica.getEMailCcn());

        return rubricaImpl;
    }

    /**
     * Returns the rubrica with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the rubrica
     * @return the rubrica
     * @throws it.bysoftware.ct.NoSuchRubricaException if a rubrica with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Rubrica findByPrimaryKey(Serializable primaryKey)
        throws NoSuchRubricaException, SystemException {
        Rubrica rubrica = fetchByPrimaryKey(primaryKey);

        if (rubrica == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchRubricaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return rubrica;
    }

    /**
     * Returns the rubrica with the primary key or throws a {@link it.bysoftware.ct.NoSuchRubricaException} if it could not be found.
     *
     * @param rubricaPK the primary key of the rubrica
     * @return the rubrica
     * @throws it.bysoftware.ct.NoSuchRubricaException if a rubrica with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Rubrica findByPrimaryKey(RubricaPK rubricaPK)
        throws NoSuchRubricaException, SystemException {
        return findByPrimaryKey((Serializable) rubricaPK);
    }

    /**
     * Returns the rubrica with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the rubrica
     * @return the rubrica, or <code>null</code> if a rubrica with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Rubrica fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Rubrica rubrica = (Rubrica) EntityCacheUtil.getResult(RubricaModelImpl.ENTITY_CACHE_ENABLED,
                RubricaImpl.class, primaryKey);

        if (rubrica == _nullRubrica) {
            return null;
        }

        if (rubrica == null) {
            Session session = null;

            try {
                session = openSession();

                rubrica = (Rubrica) session.get(RubricaImpl.class, primaryKey);

                if (rubrica != null) {
                    cacheResult(rubrica);
                } else {
                    EntityCacheUtil.putResult(RubricaModelImpl.ENTITY_CACHE_ENABLED,
                        RubricaImpl.class, primaryKey, _nullRubrica);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(RubricaModelImpl.ENTITY_CACHE_ENABLED,
                    RubricaImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return rubrica;
    }

    /**
     * Returns the rubrica with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param rubricaPK the primary key of the rubrica
     * @return the rubrica, or <code>null</code> if a rubrica with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Rubrica fetchByPrimaryKey(RubricaPK rubricaPK)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) rubricaPK);
    }

    /**
     * Returns all the rubricas.
     *
     * @return the rubricas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Rubrica> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the rubricas.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RubricaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of rubricas
     * @param end the upper bound of the range of rubricas (not inclusive)
     * @return the range of rubricas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Rubrica> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the rubricas.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RubricaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of rubricas
     * @param end the upper bound of the range of rubricas (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of rubricas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Rubrica> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Rubrica> list = (List<Rubrica>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_RUBRICA);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_RUBRICA;

                if (pagination) {
                    sql = sql.concat(RubricaModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Rubrica>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Rubrica>(list);
                } else {
                    list = (List<Rubrica>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the rubricas from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Rubrica rubrica : findAll()) {
            remove(rubrica);
        }
    }

    /**
     * Returns the number of rubricas.
     *
     * @return the number of rubricas
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_RUBRICA);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the rubrica persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.Rubrica")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Rubrica>> listenersList = new ArrayList<ModelListener<Rubrica>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Rubrica>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(RubricaImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
