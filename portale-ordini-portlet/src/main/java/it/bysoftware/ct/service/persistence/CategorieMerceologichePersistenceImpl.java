package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchCategorieMerceologicheException;
import it.bysoftware.ct.model.CategorieMerceologiche;
import it.bysoftware.ct.model.impl.CategorieMerceologicheImpl;
import it.bysoftware.ct.model.impl.CategorieMerceologicheModelImpl;
import it.bysoftware.ct.service.persistence.CategorieMerceologichePersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the categorie merceologiche service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CategorieMerceologichePersistence
 * @see CategorieMerceologicheUtil
 * @generated
 */
public class CategorieMerceologichePersistenceImpl extends BasePersistenceImpl<CategorieMerceologiche>
    implements CategorieMerceologichePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link CategorieMerceologicheUtil} to access the categorie merceologiche persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = CategorieMerceologicheImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CategorieMerceologicheModelImpl.ENTITY_CACHE_ENABLED,
            CategorieMerceologicheModelImpl.FINDER_CACHE_ENABLED,
            CategorieMerceologicheImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CategorieMerceologicheModelImpl.ENTITY_CACHE_ENABLED,
            CategorieMerceologicheModelImpl.FINDER_CACHE_ENABLED,
            CategorieMerceologicheImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CategorieMerceologicheModelImpl.ENTITY_CACHE_ENABLED,
            CategorieMerceologicheModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_CATEGORIEMERCEOLOGICHE = "SELECT categorieMerceologiche FROM CategorieMerceologiche categorieMerceologiche";
    private static final String _SQL_COUNT_CATEGORIEMERCEOLOGICHE = "SELECT COUNT(categorieMerceologiche) FROM CategorieMerceologiche categorieMerceologiche";
    private static final String _ORDER_BY_ENTITY_ALIAS = "categorieMerceologiche.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CategorieMerceologiche exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(CategorieMerceologichePersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "codiceCategoria", "descrizione"
            });
    private static CategorieMerceologiche _nullCategorieMerceologiche = new CategorieMerceologicheImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<CategorieMerceologiche> toCacheModel() {
                return _nullCategorieMerceologicheCacheModel;
            }
        };

    private static CacheModel<CategorieMerceologiche> _nullCategorieMerceologicheCacheModel =
        new CacheModel<CategorieMerceologiche>() {
            @Override
            public CategorieMerceologiche toEntityModel() {
                return _nullCategorieMerceologiche;
            }
        };

    public CategorieMerceologichePersistenceImpl() {
        setModelClass(CategorieMerceologiche.class);
    }

    /**
     * Caches the categorie merceologiche in the entity cache if it is enabled.
     *
     * @param categorieMerceologiche the categorie merceologiche
     */
    @Override
    public void cacheResult(CategorieMerceologiche categorieMerceologiche) {
        EntityCacheUtil.putResult(CategorieMerceologicheModelImpl.ENTITY_CACHE_ENABLED,
            CategorieMerceologicheImpl.class,
            categorieMerceologiche.getPrimaryKey(), categorieMerceologiche);

        categorieMerceologiche.resetOriginalValues();
    }

    /**
     * Caches the categorie merceologiches in the entity cache if it is enabled.
     *
     * @param categorieMerceologiches the categorie merceologiches
     */
    @Override
    public void cacheResult(
        List<CategorieMerceologiche> categorieMerceologiches) {
        for (CategorieMerceologiche categorieMerceologiche : categorieMerceologiches) {
            if (EntityCacheUtil.getResult(
                        CategorieMerceologicheModelImpl.ENTITY_CACHE_ENABLED,
                        CategorieMerceologicheImpl.class,
                        categorieMerceologiche.getPrimaryKey()) == null) {
                cacheResult(categorieMerceologiche);
            } else {
                categorieMerceologiche.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all categorie merceologiches.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(CategorieMerceologicheImpl.class.getName());
        }

        EntityCacheUtil.clearCache(CategorieMerceologicheImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the categorie merceologiche.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(CategorieMerceologiche categorieMerceologiche) {
        EntityCacheUtil.removeResult(CategorieMerceologicheModelImpl.ENTITY_CACHE_ENABLED,
            CategorieMerceologicheImpl.class,
            categorieMerceologiche.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<CategorieMerceologiche> categorieMerceologiches) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (CategorieMerceologiche categorieMerceologiche : categorieMerceologiches) {
            EntityCacheUtil.removeResult(CategorieMerceologicheModelImpl.ENTITY_CACHE_ENABLED,
                CategorieMerceologicheImpl.class,
                categorieMerceologiche.getPrimaryKey());
        }
    }

    /**
     * Creates a new categorie merceologiche with the primary key. Does not add the categorie merceologiche to the database.
     *
     * @param codiceCategoria the primary key for the new categorie merceologiche
     * @return the new categorie merceologiche
     */
    @Override
    public CategorieMerceologiche create(String codiceCategoria) {
        CategorieMerceologiche categorieMerceologiche = new CategorieMerceologicheImpl();

        categorieMerceologiche.setNew(true);
        categorieMerceologiche.setPrimaryKey(codiceCategoria);

        return categorieMerceologiche;
    }

    /**
     * Removes the categorie merceologiche with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param codiceCategoria the primary key of the categorie merceologiche
     * @return the categorie merceologiche that was removed
     * @throws it.bysoftware.ct.NoSuchCategorieMerceologicheException if a categorie merceologiche with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CategorieMerceologiche remove(String codiceCategoria)
        throws NoSuchCategorieMerceologicheException, SystemException {
        return remove((Serializable) codiceCategoria);
    }

    /**
     * Removes the categorie merceologiche with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the categorie merceologiche
     * @return the categorie merceologiche that was removed
     * @throws it.bysoftware.ct.NoSuchCategorieMerceologicheException if a categorie merceologiche with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CategorieMerceologiche remove(Serializable primaryKey)
        throws NoSuchCategorieMerceologicheException, SystemException {
        Session session = null;

        try {
            session = openSession();

            CategorieMerceologiche categorieMerceologiche = (CategorieMerceologiche) session.get(CategorieMerceologicheImpl.class,
                    primaryKey);

            if (categorieMerceologiche == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchCategorieMerceologicheException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(categorieMerceologiche);
        } catch (NoSuchCategorieMerceologicheException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected CategorieMerceologiche removeImpl(
        CategorieMerceologiche categorieMerceologiche)
        throws SystemException {
        categorieMerceologiche = toUnwrappedModel(categorieMerceologiche);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(categorieMerceologiche)) {
                categorieMerceologiche = (CategorieMerceologiche) session.get(CategorieMerceologicheImpl.class,
                        categorieMerceologiche.getPrimaryKeyObj());
            }

            if (categorieMerceologiche != null) {
                session.delete(categorieMerceologiche);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (categorieMerceologiche != null) {
            clearCache(categorieMerceologiche);
        }

        return categorieMerceologiche;
    }

    @Override
    public CategorieMerceologiche updateImpl(
        it.bysoftware.ct.model.CategorieMerceologiche categorieMerceologiche)
        throws SystemException {
        categorieMerceologiche = toUnwrappedModel(categorieMerceologiche);

        boolean isNew = categorieMerceologiche.isNew();

        Session session = null;

        try {
            session = openSession();

            if (categorieMerceologiche.isNew()) {
                session.save(categorieMerceologiche);

                categorieMerceologiche.setNew(false);
            } else {
                session.merge(categorieMerceologiche);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(CategorieMerceologicheModelImpl.ENTITY_CACHE_ENABLED,
            CategorieMerceologicheImpl.class,
            categorieMerceologiche.getPrimaryKey(), categorieMerceologiche);

        return categorieMerceologiche;
    }

    protected CategorieMerceologiche toUnwrappedModel(
        CategorieMerceologiche categorieMerceologiche) {
        if (categorieMerceologiche instanceof CategorieMerceologicheImpl) {
            return categorieMerceologiche;
        }

        CategorieMerceologicheImpl categorieMerceologicheImpl = new CategorieMerceologicheImpl();

        categorieMerceologicheImpl.setNew(categorieMerceologiche.isNew());
        categorieMerceologicheImpl.setPrimaryKey(categorieMerceologiche.getPrimaryKey());

        categorieMerceologicheImpl.setCodiceCategoria(categorieMerceologiche.getCodiceCategoria());
        categorieMerceologicheImpl.setDescrizione(categorieMerceologiche.getDescrizione());

        return categorieMerceologicheImpl;
    }

    /**
     * Returns the categorie merceologiche with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the categorie merceologiche
     * @return the categorie merceologiche
     * @throws it.bysoftware.ct.NoSuchCategorieMerceologicheException if a categorie merceologiche with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CategorieMerceologiche findByPrimaryKey(Serializable primaryKey)
        throws NoSuchCategorieMerceologicheException, SystemException {
        CategorieMerceologiche categorieMerceologiche = fetchByPrimaryKey(primaryKey);

        if (categorieMerceologiche == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchCategorieMerceologicheException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return categorieMerceologiche;
    }

    /**
     * Returns the categorie merceologiche with the primary key or throws a {@link it.bysoftware.ct.NoSuchCategorieMerceologicheException} if it could not be found.
     *
     * @param codiceCategoria the primary key of the categorie merceologiche
     * @return the categorie merceologiche
     * @throws it.bysoftware.ct.NoSuchCategorieMerceologicheException if a categorie merceologiche with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CategorieMerceologiche findByPrimaryKey(String codiceCategoria)
        throws NoSuchCategorieMerceologicheException, SystemException {
        return findByPrimaryKey((Serializable) codiceCategoria);
    }

    /**
     * Returns the categorie merceologiche with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the categorie merceologiche
     * @return the categorie merceologiche, or <code>null</code> if a categorie merceologiche with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CategorieMerceologiche fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        CategorieMerceologiche categorieMerceologiche = (CategorieMerceologiche) EntityCacheUtil.getResult(CategorieMerceologicheModelImpl.ENTITY_CACHE_ENABLED,
                CategorieMerceologicheImpl.class, primaryKey);

        if (categorieMerceologiche == _nullCategorieMerceologiche) {
            return null;
        }

        if (categorieMerceologiche == null) {
            Session session = null;

            try {
                session = openSession();

                categorieMerceologiche = (CategorieMerceologiche) session.get(CategorieMerceologicheImpl.class,
                        primaryKey);

                if (categorieMerceologiche != null) {
                    cacheResult(categorieMerceologiche);
                } else {
                    EntityCacheUtil.putResult(CategorieMerceologicheModelImpl.ENTITY_CACHE_ENABLED,
                        CategorieMerceologicheImpl.class, primaryKey,
                        _nullCategorieMerceologiche);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(CategorieMerceologicheModelImpl.ENTITY_CACHE_ENABLED,
                    CategorieMerceologicheImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return categorieMerceologiche;
    }

    /**
     * Returns the categorie merceologiche with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param codiceCategoria the primary key of the categorie merceologiche
     * @return the categorie merceologiche, or <code>null</code> if a categorie merceologiche with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CategorieMerceologiche fetchByPrimaryKey(String codiceCategoria)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) codiceCategoria);
    }

    /**
     * Returns all the categorie merceologiches.
     *
     * @return the categorie merceologiches
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<CategorieMerceologiche> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the categorie merceologiches.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CategorieMerceologicheModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of categorie merceologiches
     * @param end the upper bound of the range of categorie merceologiches (not inclusive)
     * @return the range of categorie merceologiches
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<CategorieMerceologiche> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the categorie merceologiches.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CategorieMerceologicheModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of categorie merceologiches
     * @param end the upper bound of the range of categorie merceologiches (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of categorie merceologiches
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<CategorieMerceologiche> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<CategorieMerceologiche> list = (List<CategorieMerceologiche>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_CATEGORIEMERCEOLOGICHE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_CATEGORIEMERCEOLOGICHE;

                if (pagination) {
                    sql = sql.concat(CategorieMerceologicheModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<CategorieMerceologiche>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<CategorieMerceologiche>(list);
                } else {
                    list = (List<CategorieMerceologiche>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the categorie merceologiches from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (CategorieMerceologiche categorieMerceologiche : findAll()) {
            remove(categorieMerceologiche);
        }
    }

    /**
     * Returns the number of categorie merceologiches.
     *
     * @return the number of categorie merceologiches
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_CATEGORIEMERCEOLOGICHE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the categorie merceologiche persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.CategorieMerceologiche")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<CategorieMerceologiche>> listenersList = new ArrayList<ModelListener<CategorieMerceologiche>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<CategorieMerceologiche>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(CategorieMerceologicheImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
