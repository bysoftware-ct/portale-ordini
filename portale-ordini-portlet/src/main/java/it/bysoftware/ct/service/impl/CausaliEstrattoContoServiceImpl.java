package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.CausaliEstrattoContoServiceBaseImpl;

/**
 * The implementation of the causali estratto conto remote service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.CausaliEstrattoContoService} interface.
 *
 * <p>This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.CausaliEstrattoContoServiceBaseImpl
 * @see it.bysoftware.ct.service.CausaliEstrattoContoServiceUtil
 */
public class CausaliEstrattoContoServiceImpl extends
        CausaliEstrattoContoServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.CausaliEstrattoContoServiceUtil} to access the
     * causali estratto conto remote service.
     */
}
