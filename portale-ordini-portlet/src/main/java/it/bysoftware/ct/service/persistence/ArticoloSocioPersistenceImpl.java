package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchArticoloSocioException;
import it.bysoftware.ct.model.ArticoloSocio;
import it.bysoftware.ct.model.impl.ArticoloSocioImpl;
import it.bysoftware.ct.model.impl.ArticoloSocioModelImpl;
import it.bysoftware.ct.service.persistence.ArticoloSocioPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the articolo socio service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see ArticoloSocioPersistence
 * @see ArticoloSocioUtil
 * @generated
 */
public class ArticoloSocioPersistenceImpl extends BasePersistenceImpl<ArticoloSocio>
    implements ArticoloSocioPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link ArticoloSocioUtil} to access the articolo socio persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = ArticoloSocioImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
            ArticoloSocioModelImpl.FINDER_CACHE_ENABLED,
            ArticoloSocioImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
            ArticoloSocioModelImpl.FINDER_CACHE_ENABLED,
            ArticoloSocioImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
            ArticoloSocioModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SOCIOARTICOLO =
        new FinderPath(ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
            ArticoloSocioModelImpl.FINDER_CACHE_ENABLED,
            ArticoloSocioImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findBySocioArticolo",
            new String[] {
                String.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SOCIOARTICOLO =
        new FinderPath(ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
            ArticoloSocioModelImpl.FINDER_CACHE_ENABLED,
            ArticoloSocioImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findBySocioArticolo",
            new String[] { String.class.getName(), String.class.getName() },
            ArticoloSocioModelImpl.ASSOCIATO_COLUMN_BITMASK |
            ArticoloSocioModelImpl.CODICEARTICOLO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_SOCIOARTICOLO = new FinderPath(ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
            ArticoloSocioModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBySocioArticolo",
            new String[] { String.class.getName(), String.class.getName() });
    private static final String _FINDER_COLUMN_SOCIOARTICOLO_ASSOCIATO_1 = "articoloSocio.id.associato IS NULL AND ";
    private static final String _FINDER_COLUMN_SOCIOARTICOLO_ASSOCIATO_2 = "articoloSocio.id.associato = ? AND ";
    private static final String _FINDER_COLUMN_SOCIOARTICOLO_ASSOCIATO_3 = "(articoloSocio.id.associato IS NULL OR articoloSocio.id.associato = '') AND ";
    private static final String _FINDER_COLUMN_SOCIOARTICOLO_CODICEARTICOLO_1 = "articoloSocio.codiceArticolo IS NULL";
    private static final String _FINDER_COLUMN_SOCIOARTICOLO_CODICEARTICOLO_2 = "articoloSocio.codiceArticolo = ?";
    private static final String _FINDER_COLUMN_SOCIOARTICOLO_CODICEARTICOLO_3 = "(articoloSocio.codiceArticolo IS NULL OR articoloSocio.codiceArticolo = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SOCIO = new FinderPath(ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
            ArticoloSocioModelImpl.FINDER_CACHE_ENABLED,
            ArticoloSocioImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findBySocio",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SOCIO = new FinderPath(ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
            ArticoloSocioModelImpl.FINDER_CACHE_ENABLED,
            ArticoloSocioImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findBySocio", new String[] { String.class.getName() },
            ArticoloSocioModelImpl.ASSOCIATO_COLUMN_BITMASK |
            ArticoloSocioModelImpl.CODICEARTICOLO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_SOCIO = new FinderPath(ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
            ArticoloSocioModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBySocio",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_SOCIO_ASSOCIATO_1 = "articoloSocio.id.associato IS NULL";
    private static final String _FINDER_COLUMN_SOCIO_ASSOCIATO_2 = "articoloSocio.id.associato = ?";
    private static final String _FINDER_COLUMN_SOCIO_ASSOCIATO_3 = "(articoloSocio.id.associato IS NULL OR articoloSocio.id.associato = '')";
    public static final FinderPath FINDER_PATH_FETCH_BY_ARTICOLOVARIANTESOCIO = new FinderPath(ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
            ArticoloSocioModelImpl.FINDER_CACHE_ENABLED,
            ArticoloSocioImpl.class, FINDER_CLASS_NAME_ENTITY,
            "fetchByArticoloVarianteSocio",
            new String[] {
                String.class.getName(), String.class.getName(),
                String.class.getName()
            },
            ArticoloSocioModelImpl.ASSOCIATO_COLUMN_BITMASK |
            ArticoloSocioModelImpl.CODICEARTICOLOSOCIO_COLUMN_BITMASK |
            ArticoloSocioModelImpl.CODICEVARIANTESOCIO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_ARTICOLOVARIANTESOCIO = new FinderPath(ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
            ArticoloSocioModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByArticoloVarianteSocio",
            new String[] {
                String.class.getName(), String.class.getName(),
                String.class.getName()
            });
    private static final String _FINDER_COLUMN_ARTICOLOVARIANTESOCIO_ASSOCIATO_1 =
        "articoloSocio.id.associato IS NULL AND ";
    private static final String _FINDER_COLUMN_ARTICOLOVARIANTESOCIO_ASSOCIATO_2 =
        "articoloSocio.id.associato = ? AND ";
    private static final String _FINDER_COLUMN_ARTICOLOVARIANTESOCIO_ASSOCIATO_3 =
        "(articoloSocio.id.associato IS NULL OR articoloSocio.id.associato = '') AND ";
    private static final String _FINDER_COLUMN_ARTICOLOVARIANTESOCIO_CODICEARTICOLOSOCIO_1 =
        "articoloSocio.id.codiceArticoloSocio IS NULL AND ";
    private static final String _FINDER_COLUMN_ARTICOLOVARIANTESOCIO_CODICEARTICOLOSOCIO_2 =
        "articoloSocio.id.codiceArticoloSocio = ? AND ";
    private static final String _FINDER_COLUMN_ARTICOLOVARIANTESOCIO_CODICEARTICOLOSOCIO_3 =
        "(articoloSocio.id.codiceArticoloSocio IS NULL OR articoloSocio.id.codiceArticoloSocio = '') AND ";
    private static final String _FINDER_COLUMN_ARTICOLOVARIANTESOCIO_CODICEVARIANTESOCIO_1 =
        "articoloSocio.id.codiceVarianteSocio IS NULL";
    private static final String _FINDER_COLUMN_ARTICOLOVARIANTESOCIO_CODICEVARIANTESOCIO_2 =
        "articoloSocio.id.codiceVarianteSocio = ?";
    private static final String _FINDER_COLUMN_ARTICOLOVARIANTESOCIO_CODICEVARIANTESOCIO_3 =
        "(articoloSocio.id.codiceVarianteSocio IS NULL OR articoloSocio.id.codiceVarianteSocio = '')";
    private static final String _SQL_SELECT_ARTICOLOSOCIO = "SELECT articoloSocio FROM ArticoloSocio articoloSocio";
    private static final String _SQL_SELECT_ARTICOLOSOCIO_WHERE = "SELECT articoloSocio FROM ArticoloSocio articoloSocio WHERE ";
    private static final String _SQL_COUNT_ARTICOLOSOCIO = "SELECT COUNT(articoloSocio) FROM ArticoloSocio articoloSocio";
    private static final String _SQL_COUNT_ARTICOLOSOCIO_WHERE = "SELECT COUNT(articoloSocio) FROM ArticoloSocio articoloSocio WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "articoloSocio.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ArticoloSocio exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ArticoloSocio exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(ArticoloSocioPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "codiceArticoloSocio", "codiceVarianteSocio", "codiceArticolo",
                "codiceVariante"
            });
    private static ArticoloSocio _nullArticoloSocio = new ArticoloSocioImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<ArticoloSocio> toCacheModel() {
                return _nullArticoloSocioCacheModel;
            }
        };

    private static CacheModel<ArticoloSocio> _nullArticoloSocioCacheModel = new CacheModel<ArticoloSocio>() {
            @Override
            public ArticoloSocio toEntityModel() {
                return _nullArticoloSocio;
            }
        };

    public ArticoloSocioPersistenceImpl() {
        setModelClass(ArticoloSocio.class);
    }

    /**
     * Returns all the articolo socios where associato = &#63; and codiceArticolo = &#63;.
     *
     * @param associato the associato
     * @param codiceArticolo the codice articolo
     * @return the matching articolo socios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ArticoloSocio> findBySocioArticolo(String associato,
        String codiceArticolo) throws SystemException {
        return findBySocioArticolo(associato, codiceArticolo,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the articolo socios where associato = &#63; and codiceArticolo = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param associato the associato
     * @param codiceArticolo the codice articolo
     * @param start the lower bound of the range of articolo socios
     * @param end the upper bound of the range of articolo socios (not inclusive)
     * @return the range of matching articolo socios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ArticoloSocio> findBySocioArticolo(String associato,
        String codiceArticolo, int start, int end) throws SystemException {
        return findBySocioArticolo(associato, codiceArticolo, start, end, null);
    }

    /**
     * Returns an ordered range of all the articolo socios where associato = &#63; and codiceArticolo = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param associato the associato
     * @param codiceArticolo the codice articolo
     * @param start the lower bound of the range of articolo socios
     * @param end the upper bound of the range of articolo socios (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching articolo socios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ArticoloSocio> findBySocioArticolo(String associato,
        String codiceArticolo, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SOCIOARTICOLO;
            finderArgs = new Object[] { associato, codiceArticolo };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SOCIOARTICOLO;
            finderArgs = new Object[] {
                    associato, codiceArticolo,
                    
                    start, end, orderByComparator
                };
        }

        List<ArticoloSocio> list = (List<ArticoloSocio>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (ArticoloSocio articoloSocio : list) {
                if (!Validator.equals(associato, articoloSocio.getAssociato()) ||
                        !Validator.equals(codiceArticolo,
                            articoloSocio.getCodiceArticolo())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_ARTICOLOSOCIO_WHERE);

            boolean bindAssociato = false;

            if (associato == null) {
                query.append(_FINDER_COLUMN_SOCIOARTICOLO_ASSOCIATO_1);
            } else if (associato.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_SOCIOARTICOLO_ASSOCIATO_3);
            } else {
                bindAssociato = true;

                query.append(_FINDER_COLUMN_SOCIOARTICOLO_ASSOCIATO_2);
            }

            boolean bindCodiceArticolo = false;

            if (codiceArticolo == null) {
                query.append(_FINDER_COLUMN_SOCIOARTICOLO_CODICEARTICOLO_1);
            } else if (codiceArticolo.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_SOCIOARTICOLO_CODICEARTICOLO_3);
            } else {
                bindCodiceArticolo = true;

                query.append(_FINDER_COLUMN_SOCIOARTICOLO_CODICEARTICOLO_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(ArticoloSocioModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindAssociato) {
                    qPos.add(associato);
                }

                if (bindCodiceArticolo) {
                    qPos.add(codiceArticolo);
                }

                if (!pagination) {
                    list = (List<ArticoloSocio>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<ArticoloSocio>(list);
                } else {
                    list = (List<ArticoloSocio>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first articolo socio in the ordered set where associato = &#63; and codiceArticolo = &#63;.
     *
     * @param associato the associato
     * @param codiceArticolo the codice articolo
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching articolo socio
     * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a matching articolo socio could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio findBySocioArticolo_First(String associato,
        String codiceArticolo, OrderByComparator orderByComparator)
        throws NoSuchArticoloSocioException, SystemException {
        ArticoloSocio articoloSocio = fetchBySocioArticolo_First(associato,
                codiceArticolo, orderByComparator);

        if (articoloSocio != null) {
            return articoloSocio;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("associato=");
        msg.append(associato);

        msg.append(", codiceArticolo=");
        msg.append(codiceArticolo);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchArticoloSocioException(msg.toString());
    }

    /**
     * Returns the first articolo socio in the ordered set where associato = &#63; and codiceArticolo = &#63;.
     *
     * @param associato the associato
     * @param codiceArticolo the codice articolo
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching articolo socio, or <code>null</code> if a matching articolo socio could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio fetchBySocioArticolo_First(String associato,
        String codiceArticolo, OrderByComparator orderByComparator)
        throws SystemException {
        List<ArticoloSocio> list = findBySocioArticolo(associato,
                codiceArticolo, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last articolo socio in the ordered set where associato = &#63; and codiceArticolo = &#63;.
     *
     * @param associato the associato
     * @param codiceArticolo the codice articolo
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching articolo socio
     * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a matching articolo socio could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio findBySocioArticolo_Last(String associato,
        String codiceArticolo, OrderByComparator orderByComparator)
        throws NoSuchArticoloSocioException, SystemException {
        ArticoloSocio articoloSocio = fetchBySocioArticolo_Last(associato,
                codiceArticolo, orderByComparator);

        if (articoloSocio != null) {
            return articoloSocio;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("associato=");
        msg.append(associato);

        msg.append(", codiceArticolo=");
        msg.append(codiceArticolo);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchArticoloSocioException(msg.toString());
    }

    /**
     * Returns the last articolo socio in the ordered set where associato = &#63; and codiceArticolo = &#63;.
     *
     * @param associato the associato
     * @param codiceArticolo the codice articolo
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching articolo socio, or <code>null</code> if a matching articolo socio could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio fetchBySocioArticolo_Last(String associato,
        String codiceArticolo, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countBySocioArticolo(associato, codiceArticolo);

        if (count == 0) {
            return null;
        }

        List<ArticoloSocio> list = findBySocioArticolo(associato,
                codiceArticolo, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the articolo socios before and after the current articolo socio in the ordered set where associato = &#63; and codiceArticolo = &#63;.
     *
     * @param articoloSocioPK the primary key of the current articolo socio
     * @param associato the associato
     * @param codiceArticolo the codice articolo
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next articolo socio
     * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a articolo socio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio[] findBySocioArticolo_PrevAndNext(
        ArticoloSocioPK articoloSocioPK, String associato,
        String codiceArticolo, OrderByComparator orderByComparator)
        throws NoSuchArticoloSocioException, SystemException {
        ArticoloSocio articoloSocio = findByPrimaryKey(articoloSocioPK);

        Session session = null;

        try {
            session = openSession();

            ArticoloSocio[] array = new ArticoloSocioImpl[3];

            array[0] = getBySocioArticolo_PrevAndNext(session, articoloSocio,
                    associato, codiceArticolo, orderByComparator, true);

            array[1] = articoloSocio;

            array[2] = getBySocioArticolo_PrevAndNext(session, articoloSocio,
                    associato, codiceArticolo, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected ArticoloSocio getBySocioArticolo_PrevAndNext(Session session,
        ArticoloSocio articoloSocio, String associato, String codiceArticolo,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_ARTICOLOSOCIO_WHERE);

        boolean bindAssociato = false;

        if (associato == null) {
            query.append(_FINDER_COLUMN_SOCIOARTICOLO_ASSOCIATO_1);
        } else if (associato.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_SOCIOARTICOLO_ASSOCIATO_3);
        } else {
            bindAssociato = true;

            query.append(_FINDER_COLUMN_SOCIOARTICOLO_ASSOCIATO_2);
        }

        boolean bindCodiceArticolo = false;

        if (codiceArticolo == null) {
            query.append(_FINDER_COLUMN_SOCIOARTICOLO_CODICEARTICOLO_1);
        } else if (codiceArticolo.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_SOCIOARTICOLO_CODICEARTICOLO_3);
        } else {
            bindCodiceArticolo = true;

            query.append(_FINDER_COLUMN_SOCIOARTICOLO_CODICEARTICOLO_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(ArticoloSocioModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindAssociato) {
            qPos.add(associato);
        }

        if (bindCodiceArticolo) {
            qPos.add(codiceArticolo);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(articoloSocio);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<ArticoloSocio> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the articolo socios where associato = &#63; and codiceArticolo = &#63; from the database.
     *
     * @param associato the associato
     * @param codiceArticolo the codice articolo
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeBySocioArticolo(String associato, String codiceArticolo)
        throws SystemException {
        for (ArticoloSocio articoloSocio : findBySocioArticolo(associato,
                codiceArticolo, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(articoloSocio);
        }
    }

    /**
     * Returns the number of articolo socios where associato = &#63; and codiceArticolo = &#63;.
     *
     * @param associato the associato
     * @param codiceArticolo the codice articolo
     * @return the number of matching articolo socios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countBySocioArticolo(String associato, String codiceArticolo)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_SOCIOARTICOLO;

        Object[] finderArgs = new Object[] { associato, codiceArticolo };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_ARTICOLOSOCIO_WHERE);

            boolean bindAssociato = false;

            if (associato == null) {
                query.append(_FINDER_COLUMN_SOCIOARTICOLO_ASSOCIATO_1);
            } else if (associato.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_SOCIOARTICOLO_ASSOCIATO_3);
            } else {
                bindAssociato = true;

                query.append(_FINDER_COLUMN_SOCIOARTICOLO_ASSOCIATO_2);
            }

            boolean bindCodiceArticolo = false;

            if (codiceArticolo == null) {
                query.append(_FINDER_COLUMN_SOCIOARTICOLO_CODICEARTICOLO_1);
            } else if (codiceArticolo.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_SOCIOARTICOLO_CODICEARTICOLO_3);
            } else {
                bindCodiceArticolo = true;

                query.append(_FINDER_COLUMN_SOCIOARTICOLO_CODICEARTICOLO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindAssociato) {
                    qPos.add(associato);
                }

                if (bindCodiceArticolo) {
                    qPos.add(codiceArticolo);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the articolo socios where associato = &#63;.
     *
     * @param associato the associato
     * @return the matching articolo socios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ArticoloSocio> findBySocio(String associato)
        throws SystemException {
        return findBySocio(associato, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the articolo socios where associato = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param associato the associato
     * @param start the lower bound of the range of articolo socios
     * @param end the upper bound of the range of articolo socios (not inclusive)
     * @return the range of matching articolo socios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ArticoloSocio> findBySocio(String associato, int start, int end)
        throws SystemException {
        return findBySocio(associato, start, end, null);
    }

    /**
     * Returns an ordered range of all the articolo socios where associato = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param associato the associato
     * @param start the lower bound of the range of articolo socios
     * @param end the upper bound of the range of articolo socios (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching articolo socios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ArticoloSocio> findBySocio(String associato, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SOCIO;
            finderArgs = new Object[] { associato };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SOCIO;
            finderArgs = new Object[] { associato, start, end, orderByComparator };
        }

        List<ArticoloSocio> list = (List<ArticoloSocio>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (ArticoloSocio articoloSocio : list) {
                if (!Validator.equals(associato, articoloSocio.getAssociato())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_ARTICOLOSOCIO_WHERE);

            boolean bindAssociato = false;

            if (associato == null) {
                query.append(_FINDER_COLUMN_SOCIO_ASSOCIATO_1);
            } else if (associato.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_SOCIO_ASSOCIATO_3);
            } else {
                bindAssociato = true;

                query.append(_FINDER_COLUMN_SOCIO_ASSOCIATO_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(ArticoloSocioModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindAssociato) {
                    qPos.add(associato);
                }

                if (!pagination) {
                    list = (List<ArticoloSocio>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<ArticoloSocio>(list);
                } else {
                    list = (List<ArticoloSocio>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first articolo socio in the ordered set where associato = &#63;.
     *
     * @param associato the associato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching articolo socio
     * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a matching articolo socio could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio findBySocio_First(String associato,
        OrderByComparator orderByComparator)
        throws NoSuchArticoloSocioException, SystemException {
        ArticoloSocio articoloSocio = fetchBySocio_First(associato,
                orderByComparator);

        if (articoloSocio != null) {
            return articoloSocio;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("associato=");
        msg.append(associato);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchArticoloSocioException(msg.toString());
    }

    /**
     * Returns the first articolo socio in the ordered set where associato = &#63;.
     *
     * @param associato the associato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching articolo socio, or <code>null</code> if a matching articolo socio could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio fetchBySocio_First(String associato,
        OrderByComparator orderByComparator) throws SystemException {
        List<ArticoloSocio> list = findBySocio(associato, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last articolo socio in the ordered set where associato = &#63;.
     *
     * @param associato the associato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching articolo socio
     * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a matching articolo socio could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio findBySocio_Last(String associato,
        OrderByComparator orderByComparator)
        throws NoSuchArticoloSocioException, SystemException {
        ArticoloSocio articoloSocio = fetchBySocio_Last(associato,
                orderByComparator);

        if (articoloSocio != null) {
            return articoloSocio;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("associato=");
        msg.append(associato);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchArticoloSocioException(msg.toString());
    }

    /**
     * Returns the last articolo socio in the ordered set where associato = &#63;.
     *
     * @param associato the associato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching articolo socio, or <code>null</code> if a matching articolo socio could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio fetchBySocio_Last(String associato,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countBySocio(associato);

        if (count == 0) {
            return null;
        }

        List<ArticoloSocio> list = findBySocio(associato, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the articolo socios before and after the current articolo socio in the ordered set where associato = &#63;.
     *
     * @param articoloSocioPK the primary key of the current articolo socio
     * @param associato the associato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next articolo socio
     * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a articolo socio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio[] findBySocio_PrevAndNext(
        ArticoloSocioPK articoloSocioPK, String associato,
        OrderByComparator orderByComparator)
        throws NoSuchArticoloSocioException, SystemException {
        ArticoloSocio articoloSocio = findByPrimaryKey(articoloSocioPK);

        Session session = null;

        try {
            session = openSession();

            ArticoloSocio[] array = new ArticoloSocioImpl[3];

            array[0] = getBySocio_PrevAndNext(session, articoloSocio,
                    associato, orderByComparator, true);

            array[1] = articoloSocio;

            array[2] = getBySocio_PrevAndNext(session, articoloSocio,
                    associato, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected ArticoloSocio getBySocio_PrevAndNext(Session session,
        ArticoloSocio articoloSocio, String associato,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_ARTICOLOSOCIO_WHERE);

        boolean bindAssociato = false;

        if (associato == null) {
            query.append(_FINDER_COLUMN_SOCIO_ASSOCIATO_1);
        } else if (associato.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_SOCIO_ASSOCIATO_3);
        } else {
            bindAssociato = true;

            query.append(_FINDER_COLUMN_SOCIO_ASSOCIATO_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(ArticoloSocioModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindAssociato) {
            qPos.add(associato);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(articoloSocio);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<ArticoloSocio> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the articolo socios where associato = &#63; from the database.
     *
     * @param associato the associato
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeBySocio(String associato) throws SystemException {
        for (ArticoloSocio articoloSocio : findBySocio(associato,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(articoloSocio);
        }
    }

    /**
     * Returns the number of articolo socios where associato = &#63;.
     *
     * @param associato the associato
     * @return the number of matching articolo socios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countBySocio(String associato) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_SOCIO;

        Object[] finderArgs = new Object[] { associato };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_ARTICOLOSOCIO_WHERE);

            boolean bindAssociato = false;

            if (associato == null) {
                query.append(_FINDER_COLUMN_SOCIO_ASSOCIATO_1);
            } else if (associato.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_SOCIO_ASSOCIATO_3);
            } else {
                bindAssociato = true;

                query.append(_FINDER_COLUMN_SOCIO_ASSOCIATO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindAssociato) {
                    qPos.add(associato);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the articolo socio where associato = &#63; and codiceArticoloSocio = &#63; and codiceVarianteSocio = &#63; or throws a {@link it.bysoftware.ct.NoSuchArticoloSocioException} if it could not be found.
     *
     * @param associato the associato
     * @param codiceArticoloSocio the codice articolo socio
     * @param codiceVarianteSocio the codice variante socio
     * @return the matching articolo socio
     * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a matching articolo socio could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio findByArticoloVarianteSocio(String associato,
        String codiceArticoloSocio, String codiceVarianteSocio)
        throws NoSuchArticoloSocioException, SystemException {
        ArticoloSocio articoloSocio = fetchByArticoloVarianteSocio(associato,
                codiceArticoloSocio, codiceVarianteSocio);

        if (articoloSocio == null) {
            StringBundler msg = new StringBundler(8);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("associato=");
            msg.append(associato);

            msg.append(", codiceArticoloSocio=");
            msg.append(codiceArticoloSocio);

            msg.append(", codiceVarianteSocio=");
            msg.append(codiceVarianteSocio);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchArticoloSocioException(msg.toString());
        }

        return articoloSocio;
    }

    /**
     * Returns the articolo socio where associato = &#63; and codiceArticoloSocio = &#63; and codiceVarianteSocio = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param associato the associato
     * @param codiceArticoloSocio the codice articolo socio
     * @param codiceVarianteSocio the codice variante socio
     * @return the matching articolo socio, or <code>null</code> if a matching articolo socio could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio fetchByArticoloVarianteSocio(String associato,
        String codiceArticoloSocio, String codiceVarianteSocio)
        throws SystemException {
        return fetchByArticoloVarianteSocio(associato, codiceArticoloSocio,
            codiceVarianteSocio, true);
    }

    /**
     * Returns the articolo socio where associato = &#63; and codiceArticoloSocio = &#63; and codiceVarianteSocio = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param associato the associato
     * @param codiceArticoloSocio the codice articolo socio
     * @param codiceVarianteSocio the codice variante socio
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching articolo socio, or <code>null</code> if a matching articolo socio could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio fetchByArticoloVarianteSocio(String associato,
        String codiceArticoloSocio, String codiceVarianteSocio,
        boolean retrieveFromCache) throws SystemException {
        Object[] finderArgs = new Object[] {
                associato, codiceArticoloSocio, codiceVarianteSocio
            };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_ARTICOLOVARIANTESOCIO,
                    finderArgs, this);
        }

        if (result instanceof ArticoloSocio) {
            ArticoloSocio articoloSocio = (ArticoloSocio) result;

            if (!Validator.equals(associato, articoloSocio.getAssociato()) ||
                    !Validator.equals(codiceArticoloSocio,
                        articoloSocio.getCodiceArticoloSocio()) ||
                    !Validator.equals(codiceVarianteSocio,
                        articoloSocio.getCodiceVarianteSocio())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(5);

            query.append(_SQL_SELECT_ARTICOLOSOCIO_WHERE);

            boolean bindAssociato = false;

            if (associato == null) {
                query.append(_FINDER_COLUMN_ARTICOLOVARIANTESOCIO_ASSOCIATO_1);
            } else if (associato.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ARTICOLOVARIANTESOCIO_ASSOCIATO_3);
            } else {
                bindAssociato = true;

                query.append(_FINDER_COLUMN_ARTICOLOVARIANTESOCIO_ASSOCIATO_2);
            }

            boolean bindCodiceArticoloSocio = false;

            if (codiceArticoloSocio == null) {
                query.append(_FINDER_COLUMN_ARTICOLOVARIANTESOCIO_CODICEARTICOLOSOCIO_1);
            } else if (codiceArticoloSocio.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ARTICOLOVARIANTESOCIO_CODICEARTICOLOSOCIO_3);
            } else {
                bindCodiceArticoloSocio = true;

                query.append(_FINDER_COLUMN_ARTICOLOVARIANTESOCIO_CODICEARTICOLOSOCIO_2);
            }

            boolean bindCodiceVarianteSocio = false;

            if (codiceVarianteSocio == null) {
                query.append(_FINDER_COLUMN_ARTICOLOVARIANTESOCIO_CODICEVARIANTESOCIO_1);
            } else if (codiceVarianteSocio.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ARTICOLOVARIANTESOCIO_CODICEVARIANTESOCIO_3);
            } else {
                bindCodiceVarianteSocio = true;

                query.append(_FINDER_COLUMN_ARTICOLOVARIANTESOCIO_CODICEVARIANTESOCIO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindAssociato) {
                    qPos.add(associato);
                }

                if (bindCodiceArticoloSocio) {
                    qPos.add(codiceArticoloSocio);
                }

                if (bindCodiceVarianteSocio) {
                    qPos.add(codiceVarianteSocio);
                }

                List<ArticoloSocio> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ARTICOLOVARIANTESOCIO,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "ArticoloSocioPersistenceImpl.fetchByArticoloVarianteSocio(String, String, String, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    ArticoloSocio articoloSocio = list.get(0);

                    result = articoloSocio;

                    cacheResult(articoloSocio);

                    if ((articoloSocio.getAssociato() == null) ||
                            !articoloSocio.getAssociato().equals(associato) ||
                            (articoloSocio.getCodiceArticoloSocio() == null) ||
                            !articoloSocio.getCodiceArticoloSocio()
                                              .equals(codiceArticoloSocio) ||
                            (articoloSocio.getCodiceVarianteSocio() == null) ||
                            !articoloSocio.getCodiceVarianteSocio()
                                              .equals(codiceVarianteSocio)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ARTICOLOVARIANTESOCIO,
                            finderArgs, articoloSocio);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ARTICOLOVARIANTESOCIO,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (ArticoloSocio) result;
        }
    }

    /**
     * Removes the articolo socio where associato = &#63; and codiceArticoloSocio = &#63; and codiceVarianteSocio = &#63; from the database.
     *
     * @param associato the associato
     * @param codiceArticoloSocio the codice articolo socio
     * @param codiceVarianteSocio the codice variante socio
     * @return the articolo socio that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio removeByArticoloVarianteSocio(String associato,
        String codiceArticoloSocio, String codiceVarianteSocio)
        throws NoSuchArticoloSocioException, SystemException {
        ArticoloSocio articoloSocio = findByArticoloVarianteSocio(associato,
                codiceArticoloSocio, codiceVarianteSocio);

        return remove(articoloSocio);
    }

    /**
     * Returns the number of articolo socios where associato = &#63; and codiceArticoloSocio = &#63; and codiceVarianteSocio = &#63;.
     *
     * @param associato the associato
     * @param codiceArticoloSocio the codice articolo socio
     * @param codiceVarianteSocio the codice variante socio
     * @return the number of matching articolo socios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByArticoloVarianteSocio(String associato,
        String codiceArticoloSocio, String codiceVarianteSocio)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_ARTICOLOVARIANTESOCIO;

        Object[] finderArgs = new Object[] {
                associato, codiceArticoloSocio, codiceVarianteSocio
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(4);

            query.append(_SQL_COUNT_ARTICOLOSOCIO_WHERE);

            boolean bindAssociato = false;

            if (associato == null) {
                query.append(_FINDER_COLUMN_ARTICOLOVARIANTESOCIO_ASSOCIATO_1);
            } else if (associato.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ARTICOLOVARIANTESOCIO_ASSOCIATO_3);
            } else {
                bindAssociato = true;

                query.append(_FINDER_COLUMN_ARTICOLOVARIANTESOCIO_ASSOCIATO_2);
            }

            boolean bindCodiceArticoloSocio = false;

            if (codiceArticoloSocio == null) {
                query.append(_FINDER_COLUMN_ARTICOLOVARIANTESOCIO_CODICEARTICOLOSOCIO_1);
            } else if (codiceArticoloSocio.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ARTICOLOVARIANTESOCIO_CODICEARTICOLOSOCIO_3);
            } else {
                bindCodiceArticoloSocio = true;

                query.append(_FINDER_COLUMN_ARTICOLOVARIANTESOCIO_CODICEARTICOLOSOCIO_2);
            }

            boolean bindCodiceVarianteSocio = false;

            if (codiceVarianteSocio == null) {
                query.append(_FINDER_COLUMN_ARTICOLOVARIANTESOCIO_CODICEVARIANTESOCIO_1);
            } else if (codiceVarianteSocio.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_ARTICOLOVARIANTESOCIO_CODICEVARIANTESOCIO_3);
            } else {
                bindCodiceVarianteSocio = true;

                query.append(_FINDER_COLUMN_ARTICOLOVARIANTESOCIO_CODICEVARIANTESOCIO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindAssociato) {
                    qPos.add(associato);
                }

                if (bindCodiceArticoloSocio) {
                    qPos.add(codiceArticoloSocio);
                }

                if (bindCodiceVarianteSocio) {
                    qPos.add(codiceVarianteSocio);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the articolo socio in the entity cache if it is enabled.
     *
     * @param articoloSocio the articolo socio
     */
    @Override
    public void cacheResult(ArticoloSocio articoloSocio) {
        EntityCacheUtil.putResult(ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
            ArticoloSocioImpl.class, articoloSocio.getPrimaryKey(),
            articoloSocio);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ARTICOLOVARIANTESOCIO,
            new Object[] {
                articoloSocio.getAssociato(),
                articoloSocio.getCodiceArticoloSocio(),
                articoloSocio.getCodiceVarianteSocio()
            }, articoloSocio);

        articoloSocio.resetOriginalValues();
    }

    /**
     * Caches the articolo socios in the entity cache if it is enabled.
     *
     * @param articoloSocios the articolo socios
     */
    @Override
    public void cacheResult(List<ArticoloSocio> articoloSocios) {
        for (ArticoloSocio articoloSocio : articoloSocios) {
            if (EntityCacheUtil.getResult(
                        ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
                        ArticoloSocioImpl.class, articoloSocio.getPrimaryKey()) == null) {
                cacheResult(articoloSocio);
            } else {
                articoloSocio.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all articolo socios.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(ArticoloSocioImpl.class.getName());
        }

        EntityCacheUtil.clearCache(ArticoloSocioImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the articolo socio.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(ArticoloSocio articoloSocio) {
        EntityCacheUtil.removeResult(ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
            ArticoloSocioImpl.class, articoloSocio.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        clearUniqueFindersCache(articoloSocio);
    }

    @Override
    public void clearCache(List<ArticoloSocio> articoloSocios) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (ArticoloSocio articoloSocio : articoloSocios) {
            EntityCacheUtil.removeResult(ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
                ArticoloSocioImpl.class, articoloSocio.getPrimaryKey());

            clearUniqueFindersCache(articoloSocio);
        }
    }

    protected void cacheUniqueFindersCache(ArticoloSocio articoloSocio) {
        if (articoloSocio.isNew()) {
            Object[] args = new Object[] {
                    articoloSocio.getAssociato(),
                    articoloSocio.getCodiceArticoloSocio(),
                    articoloSocio.getCodiceVarianteSocio()
                };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ARTICOLOVARIANTESOCIO,
                args, Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ARTICOLOVARIANTESOCIO,
                args, articoloSocio);
        } else {
            ArticoloSocioModelImpl articoloSocioModelImpl = (ArticoloSocioModelImpl) articoloSocio;

            if ((articoloSocioModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_ARTICOLOVARIANTESOCIO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        articoloSocio.getAssociato(),
                        articoloSocio.getCodiceArticoloSocio(),
                        articoloSocio.getCodiceVarianteSocio()
                    };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ARTICOLOVARIANTESOCIO,
                    args, Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ARTICOLOVARIANTESOCIO,
                    args, articoloSocio);
            }
        }
    }

    protected void clearUniqueFindersCache(ArticoloSocio articoloSocio) {
        ArticoloSocioModelImpl articoloSocioModelImpl = (ArticoloSocioModelImpl) articoloSocio;

        Object[] args = new Object[] {
                articoloSocio.getAssociato(),
                articoloSocio.getCodiceArticoloSocio(),
                articoloSocio.getCodiceVarianteSocio()
            };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTICOLOVARIANTESOCIO,
            args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ARTICOLOVARIANTESOCIO,
            args);

        if ((articoloSocioModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_ARTICOLOVARIANTESOCIO.getColumnBitmask()) != 0) {
            args = new Object[] {
                    articoloSocioModelImpl.getOriginalAssociato(),
                    articoloSocioModelImpl.getOriginalCodiceArticoloSocio(),
                    articoloSocioModelImpl.getOriginalCodiceVarianteSocio()
                };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTICOLOVARIANTESOCIO,
                args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ARTICOLOVARIANTESOCIO,
                args);
        }
    }

    /**
     * Creates a new articolo socio with the primary key. Does not add the articolo socio to the database.
     *
     * @param articoloSocioPK the primary key for the new articolo socio
     * @return the new articolo socio
     */
    @Override
    public ArticoloSocio create(ArticoloSocioPK articoloSocioPK) {
        ArticoloSocio articoloSocio = new ArticoloSocioImpl();

        articoloSocio.setNew(true);
        articoloSocio.setPrimaryKey(articoloSocioPK);

        return articoloSocio;
    }

    /**
     * Removes the articolo socio with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param articoloSocioPK the primary key of the articolo socio
     * @return the articolo socio that was removed
     * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a articolo socio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio remove(ArticoloSocioPK articoloSocioPK)
        throws NoSuchArticoloSocioException, SystemException {
        return remove((Serializable) articoloSocioPK);
    }

    /**
     * Removes the articolo socio with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the articolo socio
     * @return the articolo socio that was removed
     * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a articolo socio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio remove(Serializable primaryKey)
        throws NoSuchArticoloSocioException, SystemException {
        Session session = null;

        try {
            session = openSession();

            ArticoloSocio articoloSocio = (ArticoloSocio) session.get(ArticoloSocioImpl.class,
                    primaryKey);

            if (articoloSocio == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchArticoloSocioException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(articoloSocio);
        } catch (NoSuchArticoloSocioException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected ArticoloSocio removeImpl(ArticoloSocio articoloSocio)
        throws SystemException {
        articoloSocio = toUnwrappedModel(articoloSocio);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(articoloSocio)) {
                articoloSocio = (ArticoloSocio) session.get(ArticoloSocioImpl.class,
                        articoloSocio.getPrimaryKeyObj());
            }

            if (articoloSocio != null) {
                session.delete(articoloSocio);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (articoloSocio != null) {
            clearCache(articoloSocio);
        }

        return articoloSocio;
    }

    @Override
    public ArticoloSocio updateImpl(
        it.bysoftware.ct.model.ArticoloSocio articoloSocio)
        throws SystemException {
        articoloSocio = toUnwrappedModel(articoloSocio);

        boolean isNew = articoloSocio.isNew();

        ArticoloSocioModelImpl articoloSocioModelImpl = (ArticoloSocioModelImpl) articoloSocio;

        Session session = null;

        try {
            session = openSession();

            if (articoloSocio.isNew()) {
                session.save(articoloSocio);

                articoloSocio.setNew(false);
            } else {
                session.merge(articoloSocio);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !ArticoloSocioModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((articoloSocioModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SOCIOARTICOLO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        articoloSocioModelImpl.getOriginalAssociato(),
                        articoloSocioModelImpl.getOriginalCodiceArticolo()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SOCIOARTICOLO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SOCIOARTICOLO,
                    args);

                args = new Object[] {
                        articoloSocioModelImpl.getAssociato(),
                        articoloSocioModelImpl.getCodiceArticolo()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SOCIOARTICOLO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SOCIOARTICOLO,
                    args);
            }

            if ((articoloSocioModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SOCIO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        articoloSocioModelImpl.getOriginalAssociato()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SOCIO, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SOCIO,
                    args);

                args = new Object[] { articoloSocioModelImpl.getAssociato() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SOCIO, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SOCIO,
                    args);
            }
        }

        EntityCacheUtil.putResult(ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
            ArticoloSocioImpl.class, articoloSocio.getPrimaryKey(),
            articoloSocio);

        clearUniqueFindersCache(articoloSocio);
        cacheUniqueFindersCache(articoloSocio);

        return articoloSocio;
    }

    protected ArticoloSocio toUnwrappedModel(ArticoloSocio articoloSocio) {
        if (articoloSocio instanceof ArticoloSocioImpl) {
            return articoloSocio;
        }

        ArticoloSocioImpl articoloSocioImpl = new ArticoloSocioImpl();

        articoloSocioImpl.setNew(articoloSocio.isNew());
        articoloSocioImpl.setPrimaryKey(articoloSocio.getPrimaryKey());

        articoloSocioImpl.setAssociato(articoloSocio.getAssociato());
        articoloSocioImpl.setCodiceArticoloSocio(articoloSocio.getCodiceArticoloSocio());
        articoloSocioImpl.setCodiceVarianteSocio(articoloSocio.getCodiceVarianteSocio());
        articoloSocioImpl.setCodiceArticolo(articoloSocio.getCodiceArticolo());
        articoloSocioImpl.setCodiceVariante(articoloSocio.getCodiceVariante());

        return articoloSocioImpl;
    }

    /**
     * Returns the articolo socio with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the articolo socio
     * @return the articolo socio
     * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a articolo socio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio findByPrimaryKey(Serializable primaryKey)
        throws NoSuchArticoloSocioException, SystemException {
        ArticoloSocio articoloSocio = fetchByPrimaryKey(primaryKey);

        if (articoloSocio == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchArticoloSocioException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return articoloSocio;
    }

    /**
     * Returns the articolo socio with the primary key or throws a {@link it.bysoftware.ct.NoSuchArticoloSocioException} if it could not be found.
     *
     * @param articoloSocioPK the primary key of the articolo socio
     * @return the articolo socio
     * @throws it.bysoftware.ct.NoSuchArticoloSocioException if a articolo socio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio findByPrimaryKey(ArticoloSocioPK articoloSocioPK)
        throws NoSuchArticoloSocioException, SystemException {
        return findByPrimaryKey((Serializable) articoloSocioPK);
    }

    /**
     * Returns the articolo socio with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the articolo socio
     * @return the articolo socio, or <code>null</code> if a articolo socio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        ArticoloSocio articoloSocio = (ArticoloSocio) EntityCacheUtil.getResult(ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
                ArticoloSocioImpl.class, primaryKey);

        if (articoloSocio == _nullArticoloSocio) {
            return null;
        }

        if (articoloSocio == null) {
            Session session = null;

            try {
                session = openSession();

                articoloSocio = (ArticoloSocio) session.get(ArticoloSocioImpl.class,
                        primaryKey);

                if (articoloSocio != null) {
                    cacheResult(articoloSocio);
                } else {
                    EntityCacheUtil.putResult(ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
                        ArticoloSocioImpl.class, primaryKey, _nullArticoloSocio);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(ArticoloSocioModelImpl.ENTITY_CACHE_ENABLED,
                    ArticoloSocioImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return articoloSocio;
    }

    /**
     * Returns the articolo socio with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param articoloSocioPK the primary key of the articolo socio
     * @return the articolo socio, or <code>null</code> if a articolo socio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ArticoloSocio fetchByPrimaryKey(ArticoloSocioPK articoloSocioPK)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) articoloSocioPK);
    }

    /**
     * Returns all the articolo socios.
     *
     * @return the articolo socios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ArticoloSocio> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the articolo socios.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of articolo socios
     * @param end the upper bound of the range of articolo socios (not inclusive)
     * @return the range of articolo socios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ArticoloSocio> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the articolo socios.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ArticoloSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of articolo socios
     * @param end the upper bound of the range of articolo socios (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of articolo socios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ArticoloSocio> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<ArticoloSocio> list = (List<ArticoloSocio>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_ARTICOLOSOCIO);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_ARTICOLOSOCIO;

                if (pagination) {
                    sql = sql.concat(ArticoloSocioModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<ArticoloSocio>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<ArticoloSocio>(list);
                } else {
                    list = (List<ArticoloSocio>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the articolo socios from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (ArticoloSocio articoloSocio : findAll()) {
            remove(articoloSocio);
        }
    }

    /**
     * Returns the number of articolo socios.
     *
     * @return the number of articolo socios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_ARTICOLOSOCIO);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the articolo socio persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.ArticoloSocio")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<ArticoloSocio>> listenersList = new ArrayList<ModelListener<ArticoloSocio>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<ArticoloSocio>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(ArticoloSocioImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
