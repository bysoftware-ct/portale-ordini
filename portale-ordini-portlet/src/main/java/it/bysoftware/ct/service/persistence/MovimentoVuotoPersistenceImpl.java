package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CalendarUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchMovimentoVuotoException;
import it.bysoftware.ct.model.MovimentoVuoto;
import it.bysoftware.ct.model.impl.MovimentoVuotoImpl;
import it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl;
import it.bysoftware.ct.service.persistence.MovimentoVuotoPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the movimento vuoto service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see MovimentoVuotoPersistence
 * @see MovimentoVuotoUtil
 * @generated
 */
public class MovimentoVuotoPersistenceImpl extends BasePersistenceImpl<MovimentoVuoto>
    implements MovimentoVuotoPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link MovimentoVuotoUtil} to access the movimento vuoto persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = MovimentoVuotoImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
            MovimentoVuotoModelImpl.FINDER_CACHE_ENABLED,
            MovimentoVuotoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
            MovimentoVuotoModelImpl.FINDER_CACHE_ENABLED,
            MovimentoVuotoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
            MovimentoVuotoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICEVUOTO =
        new FinderPath(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
            MovimentoVuotoModelImpl.FINDER_CACHE_ENABLED,
            MovimentoVuotoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByCodiceVuoto",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEVUOTO =
        new FinderPath(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
            MovimentoVuotoModelImpl.FINDER_CACHE_ENABLED,
            MovimentoVuotoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCodiceVuoto",
            new String[] { String.class.getName() },
            MovimentoVuotoModelImpl.CODICEVUOTO_COLUMN_BITMASK |
            MovimentoVuotoModelImpl.DATAMOVIMENTO_COLUMN_BITMASK |
            MovimentoVuotoModelImpl.UUID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_CODICEVUOTO = new FinderPath(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
            MovimentoVuotoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCodiceVuoto",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_CODICEVUOTO_CODICEVUOTO_1 = "movimentoVuoto.codiceVuoto IS NULL";
    private static final String _FINDER_COLUMN_CODICEVUOTO_CODICEVUOTO_2 = "movimentoVuoto.codiceVuoto = ?";
    private static final String _FINDER_COLUMN_CODICEVUOTO_CODICEVUOTO_3 = "(movimentoVuoto.codiceVuoto IS NULL OR movimentoVuoto.codiceVuoto = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYVUOTOSOGGETTODATA =
        new FinderPath(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
            MovimentoVuotoModelImpl.FINDER_CACHE_ENABLED,
            MovimentoVuotoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByfindByVuotoSoggettoData",
            new String[] {
                String.class.getName(), String.class.getName(),
                Date.class.getName(), Integer.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYVUOTOSOGGETTODATA =
        new FinderPath(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
            MovimentoVuotoModelImpl.FINDER_CACHE_ENABLED,
            MovimentoVuotoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByfindByVuotoSoggettoData",
            new String[] {
                String.class.getName(), String.class.getName(),
                Date.class.getName(), Integer.class.getName()
            },
            MovimentoVuotoModelImpl.CODICEVUOTO_COLUMN_BITMASK |
            MovimentoVuotoModelImpl.CODICESOGGETTO_COLUMN_BITMASK |
            MovimentoVuotoModelImpl.DATAMOVIMENTO_COLUMN_BITMASK |
            MovimentoVuotoModelImpl.TIPOMOVIMENTO_COLUMN_BITMASK |
            MovimentoVuotoModelImpl.UUID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_FINDBYVUOTOSOGGETTODATA = new FinderPath(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
            MovimentoVuotoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByfindByVuotoSoggettoData",
            new String[] {
                String.class.getName(), String.class.getName(),
                Date.class.getName(), Integer.class.getName()
            });
    private static final String _FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICEVUOTO_1 =
        "movimentoVuoto.codiceVuoto IS NULL AND ";
    private static final String _FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICEVUOTO_2 =
        "movimentoVuoto.codiceVuoto = ? AND ";
    private static final String _FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICEVUOTO_3 =
        "(movimentoVuoto.codiceVuoto IS NULL OR movimentoVuoto.codiceVuoto = '') AND ";
    private static final String _FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICESOGGETTO_1 =
        "movimentoVuoto.codiceSoggetto IS NULL AND ";
    private static final String _FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICESOGGETTO_2 =
        "movimentoVuoto.codiceSoggetto = ? AND ";
    private static final String _FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICESOGGETTO_3 =
        "(movimentoVuoto.codiceSoggetto IS NULL OR movimentoVuoto.codiceSoggetto = '') AND ";
    private static final String _FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_DATAMOVIMENTO_1 =
        "movimentoVuoto.dataMovimento IS NULL AND ";
    private static final String _FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_DATAMOVIMENTO_2 =
        "movimentoVuoto.dataMovimento = ? AND ";
    private static final String _FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_TIPOMOVIMENTO_2 =
        "movimentoVuoto.tipoMovimento = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICESOGGETTO =
        new FinderPath(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
            MovimentoVuotoModelImpl.FINDER_CACHE_ENABLED,
            MovimentoVuotoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByCodiceSoggetto",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICESOGGETTO =
        new FinderPath(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
            MovimentoVuotoModelImpl.FINDER_CACHE_ENABLED,
            MovimentoVuotoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCodiceSoggetto",
            new String[] { String.class.getName() },
            MovimentoVuotoModelImpl.CODICESOGGETTO_COLUMN_BITMASK |
            MovimentoVuotoModelImpl.DATAMOVIMENTO_COLUMN_BITMASK |
            MovimentoVuotoModelImpl.UUID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_CODICESOGGETTO = new FinderPath(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
            MovimentoVuotoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCodiceSoggetto",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_1 = "movimentoVuoto.codiceSoggetto IS NULL";
    private static final String _FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_2 = "movimentoVuoto.codiceSoggetto = ?";
    private static final String _FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_3 = "(movimentoVuoto.codiceSoggetto IS NULL OR movimentoVuoto.codiceSoggetto = '')";
    private static final String _SQL_SELECT_MOVIMENTOVUOTO = "SELECT movimentoVuoto FROM MovimentoVuoto movimentoVuoto";
    private static final String _SQL_SELECT_MOVIMENTOVUOTO_WHERE = "SELECT movimentoVuoto FROM MovimentoVuoto movimentoVuoto WHERE ";
    private static final String _SQL_COUNT_MOVIMENTOVUOTO = "SELECT COUNT(movimentoVuoto) FROM MovimentoVuoto movimentoVuoto";
    private static final String _SQL_COUNT_MOVIMENTOVUOTO_WHERE = "SELECT COUNT(movimentoVuoto) FROM MovimentoVuoto movimentoVuoto WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "movimentoVuoto.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No MovimentoVuoto exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No MovimentoVuoto exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(MovimentoVuotoPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "idMovimento", "codiceVuoto", "dataMovimento", "codiceSoggetto",
                "tipoMovimento"
            });
    private static MovimentoVuoto _nullMovimentoVuoto = new MovimentoVuotoImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<MovimentoVuoto> toCacheModel() {
                return _nullMovimentoVuotoCacheModel;
            }
        };

    private static CacheModel<MovimentoVuoto> _nullMovimentoVuotoCacheModel = new CacheModel<MovimentoVuoto>() {
            @Override
            public MovimentoVuoto toEntityModel() {
                return _nullMovimentoVuoto;
            }
        };

    public MovimentoVuotoPersistenceImpl() {
        setModelClass(MovimentoVuoto.class);
    }

    /**
     * Returns all the movimento vuotos where codiceVuoto = &#63;.
     *
     * @param codiceVuoto the codice vuoto
     * @return the matching movimento vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<MovimentoVuoto> findByCodiceVuoto(String codiceVuoto)
        throws SystemException {
        return findByCodiceVuoto(codiceVuoto, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the movimento vuotos where codiceVuoto = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceVuoto the codice vuoto
     * @param start the lower bound of the range of movimento vuotos
     * @param end the upper bound of the range of movimento vuotos (not inclusive)
     * @return the range of matching movimento vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<MovimentoVuoto> findByCodiceVuoto(String codiceVuoto,
        int start, int end) throws SystemException {
        return findByCodiceVuoto(codiceVuoto, start, end, null);
    }

    /**
     * Returns an ordered range of all the movimento vuotos where codiceVuoto = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceVuoto the codice vuoto
     * @param start the lower bound of the range of movimento vuotos
     * @param end the upper bound of the range of movimento vuotos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching movimento vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<MovimentoVuoto> findByCodiceVuoto(String codiceVuoto,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEVUOTO;
            finderArgs = new Object[] { codiceVuoto };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICEVUOTO;
            finderArgs = new Object[] { codiceVuoto, start, end, orderByComparator };
        }

        List<MovimentoVuoto> list = (List<MovimentoVuoto>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (MovimentoVuoto movimentoVuoto : list) {
                if (!Validator.equals(codiceVuoto,
                            movimentoVuoto.getCodiceVuoto())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_MOVIMENTOVUOTO_WHERE);

            boolean bindCodiceVuoto = false;

            if (codiceVuoto == null) {
                query.append(_FINDER_COLUMN_CODICEVUOTO_CODICEVUOTO_1);
            } else if (codiceVuoto.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICEVUOTO_CODICEVUOTO_3);
            } else {
                bindCodiceVuoto = true;

                query.append(_FINDER_COLUMN_CODICEVUOTO_CODICEVUOTO_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(MovimentoVuotoModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceVuoto) {
                    qPos.add(codiceVuoto);
                }

                if (!pagination) {
                    list = (List<MovimentoVuoto>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<MovimentoVuoto>(list);
                } else {
                    list = (List<MovimentoVuoto>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first movimento vuoto in the ordered set where codiceVuoto = &#63;.
     *
     * @param codiceVuoto the codice vuoto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching movimento vuoto
     * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a matching movimento vuoto could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto findByCodiceVuoto_First(String codiceVuoto,
        OrderByComparator orderByComparator)
        throws NoSuchMovimentoVuotoException, SystemException {
        MovimentoVuoto movimentoVuoto = fetchByCodiceVuoto_First(codiceVuoto,
                orderByComparator);

        if (movimentoVuoto != null) {
            return movimentoVuoto;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceVuoto=");
        msg.append(codiceVuoto);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMovimentoVuotoException(msg.toString());
    }

    /**
     * Returns the first movimento vuoto in the ordered set where codiceVuoto = &#63;.
     *
     * @param codiceVuoto the codice vuoto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching movimento vuoto, or <code>null</code> if a matching movimento vuoto could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto fetchByCodiceVuoto_First(String codiceVuoto,
        OrderByComparator orderByComparator) throws SystemException {
        List<MovimentoVuoto> list = findByCodiceVuoto(codiceVuoto, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last movimento vuoto in the ordered set where codiceVuoto = &#63;.
     *
     * @param codiceVuoto the codice vuoto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching movimento vuoto
     * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a matching movimento vuoto could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto findByCodiceVuoto_Last(String codiceVuoto,
        OrderByComparator orderByComparator)
        throws NoSuchMovimentoVuotoException, SystemException {
        MovimentoVuoto movimentoVuoto = fetchByCodiceVuoto_Last(codiceVuoto,
                orderByComparator);

        if (movimentoVuoto != null) {
            return movimentoVuoto;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceVuoto=");
        msg.append(codiceVuoto);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMovimentoVuotoException(msg.toString());
    }

    /**
     * Returns the last movimento vuoto in the ordered set where codiceVuoto = &#63;.
     *
     * @param codiceVuoto the codice vuoto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching movimento vuoto, or <code>null</code> if a matching movimento vuoto could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto fetchByCodiceVuoto_Last(String codiceVuoto,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByCodiceVuoto(codiceVuoto);

        if (count == 0) {
            return null;
        }

        List<MovimentoVuoto> list = findByCodiceVuoto(codiceVuoto, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the movimento vuotos before and after the current movimento vuoto in the ordered set where codiceVuoto = &#63;.
     *
     * @param idMovimento the primary key of the current movimento vuoto
     * @param codiceVuoto the codice vuoto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next movimento vuoto
     * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a movimento vuoto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto[] findByCodiceVuoto_PrevAndNext(long idMovimento,
        String codiceVuoto, OrderByComparator orderByComparator)
        throws NoSuchMovimentoVuotoException, SystemException {
        MovimentoVuoto movimentoVuoto = findByPrimaryKey(idMovimento);

        Session session = null;

        try {
            session = openSession();

            MovimentoVuoto[] array = new MovimentoVuotoImpl[3];

            array[0] = getByCodiceVuoto_PrevAndNext(session, movimentoVuoto,
                    codiceVuoto, orderByComparator, true);

            array[1] = movimentoVuoto;

            array[2] = getByCodiceVuoto_PrevAndNext(session, movimentoVuoto,
                    codiceVuoto, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected MovimentoVuoto getByCodiceVuoto_PrevAndNext(Session session,
        MovimentoVuoto movimentoVuoto, String codiceVuoto,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_MOVIMENTOVUOTO_WHERE);

        boolean bindCodiceVuoto = false;

        if (codiceVuoto == null) {
            query.append(_FINDER_COLUMN_CODICEVUOTO_CODICEVUOTO_1);
        } else if (codiceVuoto.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CODICEVUOTO_CODICEVUOTO_3);
        } else {
            bindCodiceVuoto = true;

            query.append(_FINDER_COLUMN_CODICEVUOTO_CODICEVUOTO_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(MovimentoVuotoModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindCodiceVuoto) {
            qPos.add(codiceVuoto);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(movimentoVuoto);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<MovimentoVuoto> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the movimento vuotos where codiceVuoto = &#63; from the database.
     *
     * @param codiceVuoto the codice vuoto
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByCodiceVuoto(String codiceVuoto)
        throws SystemException {
        for (MovimentoVuoto movimentoVuoto : findByCodiceVuoto(codiceVuoto,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(movimentoVuoto);
        }
    }

    /**
     * Returns the number of movimento vuotos where codiceVuoto = &#63;.
     *
     * @param codiceVuoto the codice vuoto
     * @return the number of matching movimento vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByCodiceVuoto(String codiceVuoto) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_CODICEVUOTO;

        Object[] finderArgs = new Object[] { codiceVuoto };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_MOVIMENTOVUOTO_WHERE);

            boolean bindCodiceVuoto = false;

            if (codiceVuoto == null) {
                query.append(_FINDER_COLUMN_CODICEVUOTO_CODICEVUOTO_1);
            } else if (codiceVuoto.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICEVUOTO_CODICEVUOTO_3);
            } else {
                bindCodiceVuoto = true;

                query.append(_FINDER_COLUMN_CODICEVUOTO_CODICEVUOTO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceVuoto) {
                    qPos.add(codiceVuoto);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the movimento vuotos where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
     *
     * @param codiceVuoto the codice vuoto
     * @param codiceSoggetto the codice soggetto
     * @param dataMovimento the data movimento
     * @param tipoMovimento the tipo movimento
     * @return the matching movimento vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<MovimentoVuoto> findByfindByVuotoSoggettoData(
        String codiceVuoto, String codiceSoggetto, Date dataMovimento,
        int tipoMovimento) throws SystemException {
        return findByfindByVuotoSoggettoData(codiceVuoto, codiceSoggetto,
            dataMovimento, tipoMovimento, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the movimento vuotos where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceVuoto the codice vuoto
     * @param codiceSoggetto the codice soggetto
     * @param dataMovimento the data movimento
     * @param tipoMovimento the tipo movimento
     * @param start the lower bound of the range of movimento vuotos
     * @param end the upper bound of the range of movimento vuotos (not inclusive)
     * @return the range of matching movimento vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<MovimentoVuoto> findByfindByVuotoSoggettoData(
        String codiceVuoto, String codiceSoggetto, Date dataMovimento,
        int tipoMovimento, int start, int end) throws SystemException {
        return findByfindByVuotoSoggettoData(codiceVuoto, codiceSoggetto,
            dataMovimento, tipoMovimento, start, end, null);
    }

    /**
     * Returns an ordered range of all the movimento vuotos where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceVuoto the codice vuoto
     * @param codiceSoggetto the codice soggetto
     * @param dataMovimento the data movimento
     * @param tipoMovimento the tipo movimento
     * @param start the lower bound of the range of movimento vuotos
     * @param end the upper bound of the range of movimento vuotos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching movimento vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<MovimentoVuoto> findByfindByVuotoSoggettoData(
        String codiceVuoto, String codiceSoggetto, Date dataMovimento,
        int tipoMovimento, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYVUOTOSOGGETTODATA;
            finderArgs = new Object[] {
                    codiceVuoto, codiceSoggetto, dataMovimento, tipoMovimento
                };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYVUOTOSOGGETTODATA;
            finderArgs = new Object[] {
                    codiceVuoto, codiceSoggetto, dataMovimento, tipoMovimento,
                    
                    start, end, orderByComparator
                };
        }

        List<MovimentoVuoto> list = (List<MovimentoVuoto>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (MovimentoVuoto movimentoVuoto : list) {
                if (!Validator.equals(codiceVuoto,
                            movimentoVuoto.getCodiceVuoto()) ||
                        !Validator.equals(codiceSoggetto,
                            movimentoVuoto.getCodiceSoggetto()) ||
                        !Validator.equals(dataMovimento,
                            movimentoVuoto.getDataMovimento()) ||
                        (tipoMovimento != movimentoVuoto.getTipoMovimento())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(6 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(6);
            }

            query.append(_SQL_SELECT_MOVIMENTOVUOTO_WHERE);

            boolean bindCodiceVuoto = false;

            if (codiceVuoto == null) {
                query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICEVUOTO_1);
            } else if (codiceVuoto.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICEVUOTO_3);
            } else {
                bindCodiceVuoto = true;

                query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICEVUOTO_2);
            }

            boolean bindCodiceSoggetto = false;

            if (codiceSoggetto == null) {
                query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICESOGGETTO_1);
            } else if (codiceSoggetto.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICESOGGETTO_3);
            } else {
                bindCodiceSoggetto = true;

                query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICESOGGETTO_2);
            }

            boolean bindDataMovimento = false;

            if (dataMovimento == null) {
                query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_DATAMOVIMENTO_1);
            } else {
                bindDataMovimento = true;

                query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_DATAMOVIMENTO_2);
            }

            query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_TIPOMOVIMENTO_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(MovimentoVuotoModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceVuoto) {
                    qPos.add(codiceVuoto);
                }

                if (bindCodiceSoggetto) {
                    qPos.add(codiceSoggetto);
                }

                if (bindDataMovimento) {
                    qPos.add(CalendarUtil.getTimestamp(dataMovimento));
                }

                qPos.add(tipoMovimento);

                if (!pagination) {
                    list = (List<MovimentoVuoto>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<MovimentoVuoto>(list);
                } else {
                    list = (List<MovimentoVuoto>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first movimento vuoto in the ordered set where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
     *
     * @param codiceVuoto the codice vuoto
     * @param codiceSoggetto the codice soggetto
     * @param dataMovimento the data movimento
     * @param tipoMovimento the tipo movimento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching movimento vuoto
     * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a matching movimento vuoto could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto findByfindByVuotoSoggettoData_First(
        String codiceVuoto, String codiceSoggetto, Date dataMovimento,
        int tipoMovimento, OrderByComparator orderByComparator)
        throws NoSuchMovimentoVuotoException, SystemException {
        MovimentoVuoto movimentoVuoto = fetchByfindByVuotoSoggettoData_First(codiceVuoto,
                codiceSoggetto, dataMovimento, tipoMovimento, orderByComparator);

        if (movimentoVuoto != null) {
            return movimentoVuoto;
        }

        StringBundler msg = new StringBundler(10);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceVuoto=");
        msg.append(codiceVuoto);

        msg.append(", codiceSoggetto=");
        msg.append(codiceSoggetto);

        msg.append(", dataMovimento=");
        msg.append(dataMovimento);

        msg.append(", tipoMovimento=");
        msg.append(tipoMovimento);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMovimentoVuotoException(msg.toString());
    }

    /**
     * Returns the first movimento vuoto in the ordered set where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
     *
     * @param codiceVuoto the codice vuoto
     * @param codiceSoggetto the codice soggetto
     * @param dataMovimento the data movimento
     * @param tipoMovimento the tipo movimento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching movimento vuoto, or <code>null</code> if a matching movimento vuoto could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto fetchByfindByVuotoSoggettoData_First(
        String codiceVuoto, String codiceSoggetto, Date dataMovimento,
        int tipoMovimento, OrderByComparator orderByComparator)
        throws SystemException {
        List<MovimentoVuoto> list = findByfindByVuotoSoggettoData(codiceVuoto,
                codiceSoggetto, dataMovimento, tipoMovimento, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last movimento vuoto in the ordered set where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
     *
     * @param codiceVuoto the codice vuoto
     * @param codiceSoggetto the codice soggetto
     * @param dataMovimento the data movimento
     * @param tipoMovimento the tipo movimento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching movimento vuoto
     * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a matching movimento vuoto could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto findByfindByVuotoSoggettoData_Last(
        String codiceVuoto, String codiceSoggetto, Date dataMovimento,
        int tipoMovimento, OrderByComparator orderByComparator)
        throws NoSuchMovimentoVuotoException, SystemException {
        MovimentoVuoto movimentoVuoto = fetchByfindByVuotoSoggettoData_Last(codiceVuoto,
                codiceSoggetto, dataMovimento, tipoMovimento, orderByComparator);

        if (movimentoVuoto != null) {
            return movimentoVuoto;
        }

        StringBundler msg = new StringBundler(10);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceVuoto=");
        msg.append(codiceVuoto);

        msg.append(", codiceSoggetto=");
        msg.append(codiceSoggetto);

        msg.append(", dataMovimento=");
        msg.append(dataMovimento);

        msg.append(", tipoMovimento=");
        msg.append(tipoMovimento);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMovimentoVuotoException(msg.toString());
    }

    /**
     * Returns the last movimento vuoto in the ordered set where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
     *
     * @param codiceVuoto the codice vuoto
     * @param codiceSoggetto the codice soggetto
     * @param dataMovimento the data movimento
     * @param tipoMovimento the tipo movimento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching movimento vuoto, or <code>null</code> if a matching movimento vuoto could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto fetchByfindByVuotoSoggettoData_Last(
        String codiceVuoto, String codiceSoggetto, Date dataMovimento,
        int tipoMovimento, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByfindByVuotoSoggettoData(codiceVuoto, codiceSoggetto,
                dataMovimento, tipoMovimento);

        if (count == 0) {
            return null;
        }

        List<MovimentoVuoto> list = findByfindByVuotoSoggettoData(codiceVuoto,
                codiceSoggetto, dataMovimento, tipoMovimento, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the movimento vuotos before and after the current movimento vuoto in the ordered set where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
     *
     * @param idMovimento the primary key of the current movimento vuoto
     * @param codiceVuoto the codice vuoto
     * @param codiceSoggetto the codice soggetto
     * @param dataMovimento the data movimento
     * @param tipoMovimento the tipo movimento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next movimento vuoto
     * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a movimento vuoto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto[] findByfindByVuotoSoggettoData_PrevAndNext(
        long idMovimento, String codiceVuoto, String codiceSoggetto,
        Date dataMovimento, int tipoMovimento,
        OrderByComparator orderByComparator)
        throws NoSuchMovimentoVuotoException, SystemException {
        MovimentoVuoto movimentoVuoto = findByPrimaryKey(idMovimento);

        Session session = null;

        try {
            session = openSession();

            MovimentoVuoto[] array = new MovimentoVuotoImpl[3];

            array[0] = getByfindByVuotoSoggettoData_PrevAndNext(session,
                    movimentoVuoto, codiceVuoto, codiceSoggetto, dataMovimento,
                    tipoMovimento, orderByComparator, true);

            array[1] = movimentoVuoto;

            array[2] = getByfindByVuotoSoggettoData_PrevAndNext(session,
                    movimentoVuoto, codiceVuoto, codiceSoggetto, dataMovimento,
                    tipoMovimento, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected MovimentoVuoto getByfindByVuotoSoggettoData_PrevAndNext(
        Session session, MovimentoVuoto movimentoVuoto, String codiceVuoto,
        String codiceSoggetto, Date dataMovimento, int tipoMovimento,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_MOVIMENTOVUOTO_WHERE);

        boolean bindCodiceVuoto = false;

        if (codiceVuoto == null) {
            query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICEVUOTO_1);
        } else if (codiceVuoto.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICEVUOTO_3);
        } else {
            bindCodiceVuoto = true;

            query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICEVUOTO_2);
        }

        boolean bindCodiceSoggetto = false;

        if (codiceSoggetto == null) {
            query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICESOGGETTO_1);
        } else if (codiceSoggetto.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICESOGGETTO_3);
        } else {
            bindCodiceSoggetto = true;

            query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICESOGGETTO_2);
        }

        boolean bindDataMovimento = false;

        if (dataMovimento == null) {
            query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_DATAMOVIMENTO_1);
        } else {
            bindDataMovimento = true;

            query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_DATAMOVIMENTO_2);
        }

        query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_TIPOMOVIMENTO_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(MovimentoVuotoModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindCodiceVuoto) {
            qPos.add(codiceVuoto);
        }

        if (bindCodiceSoggetto) {
            qPos.add(codiceSoggetto);
        }

        if (bindDataMovimento) {
            qPos.add(CalendarUtil.getTimestamp(dataMovimento));
        }

        qPos.add(tipoMovimento);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(movimentoVuoto);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<MovimentoVuoto> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the movimento vuotos where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63; from the database.
     *
     * @param codiceVuoto the codice vuoto
     * @param codiceSoggetto the codice soggetto
     * @param dataMovimento the data movimento
     * @param tipoMovimento the tipo movimento
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByfindByVuotoSoggettoData(String codiceVuoto,
        String codiceSoggetto, Date dataMovimento, int tipoMovimento)
        throws SystemException {
        for (MovimentoVuoto movimentoVuoto : findByfindByVuotoSoggettoData(
                codiceVuoto, codiceSoggetto, dataMovimento, tipoMovimento,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(movimentoVuoto);
        }
    }

    /**
     * Returns the number of movimento vuotos where codiceVuoto = &#63; and codiceSoggetto = &#63; and dataMovimento = &#63; and tipoMovimento = &#63;.
     *
     * @param codiceVuoto the codice vuoto
     * @param codiceSoggetto the codice soggetto
     * @param dataMovimento the data movimento
     * @param tipoMovimento the tipo movimento
     * @return the number of matching movimento vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByfindByVuotoSoggettoData(String codiceVuoto,
        String codiceSoggetto, Date dataMovimento, int tipoMovimento)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_FINDBYVUOTOSOGGETTODATA;

        Object[] finderArgs = new Object[] {
                codiceVuoto, codiceSoggetto, dataMovimento, tipoMovimento
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(5);

            query.append(_SQL_COUNT_MOVIMENTOVUOTO_WHERE);

            boolean bindCodiceVuoto = false;

            if (codiceVuoto == null) {
                query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICEVUOTO_1);
            } else if (codiceVuoto.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICEVUOTO_3);
            } else {
                bindCodiceVuoto = true;

                query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICEVUOTO_2);
            }

            boolean bindCodiceSoggetto = false;

            if (codiceSoggetto == null) {
                query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICESOGGETTO_1);
            } else if (codiceSoggetto.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICESOGGETTO_3);
            } else {
                bindCodiceSoggetto = true;

                query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_CODICESOGGETTO_2);
            }

            boolean bindDataMovimento = false;

            if (dataMovimento == null) {
                query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_DATAMOVIMENTO_1);
            } else {
                bindDataMovimento = true;

                query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_DATAMOVIMENTO_2);
            }

            query.append(_FINDER_COLUMN_FINDBYVUOTOSOGGETTODATA_TIPOMOVIMENTO_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceVuoto) {
                    qPos.add(codiceVuoto);
                }

                if (bindCodiceSoggetto) {
                    qPos.add(codiceSoggetto);
                }

                if (bindDataMovimento) {
                    qPos.add(CalendarUtil.getTimestamp(dataMovimento));
                }

                qPos.add(tipoMovimento);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the movimento vuotos where codiceSoggetto = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @return the matching movimento vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<MovimentoVuoto> findByCodiceSoggetto(String codiceSoggetto)
        throws SystemException {
        return findByCodiceSoggetto(codiceSoggetto, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the movimento vuotos where codiceSoggetto = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceSoggetto the codice soggetto
     * @param start the lower bound of the range of movimento vuotos
     * @param end the upper bound of the range of movimento vuotos (not inclusive)
     * @return the range of matching movimento vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<MovimentoVuoto> findByCodiceSoggetto(String codiceSoggetto,
        int start, int end) throws SystemException {
        return findByCodiceSoggetto(codiceSoggetto, start, end, null);
    }

    /**
     * Returns an ordered range of all the movimento vuotos where codiceSoggetto = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceSoggetto the codice soggetto
     * @param start the lower bound of the range of movimento vuotos
     * @param end the upper bound of the range of movimento vuotos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching movimento vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<MovimentoVuoto> findByCodiceSoggetto(String codiceSoggetto,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICESOGGETTO;
            finderArgs = new Object[] { codiceSoggetto };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICESOGGETTO;
            finderArgs = new Object[] {
                    codiceSoggetto,
                    
                    start, end, orderByComparator
                };
        }

        List<MovimentoVuoto> list = (List<MovimentoVuoto>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (MovimentoVuoto movimentoVuoto : list) {
                if (!Validator.equals(codiceSoggetto,
                            movimentoVuoto.getCodiceSoggetto())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_MOVIMENTOVUOTO_WHERE);

            boolean bindCodiceSoggetto = false;

            if (codiceSoggetto == null) {
                query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_1);
            } else if (codiceSoggetto.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_3);
            } else {
                bindCodiceSoggetto = true;

                query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(MovimentoVuotoModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceSoggetto) {
                    qPos.add(codiceSoggetto);
                }

                if (!pagination) {
                    list = (List<MovimentoVuoto>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<MovimentoVuoto>(list);
                } else {
                    list = (List<MovimentoVuoto>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first movimento vuoto in the ordered set where codiceSoggetto = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching movimento vuoto
     * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a matching movimento vuoto could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto findByCodiceSoggetto_First(String codiceSoggetto,
        OrderByComparator orderByComparator)
        throws NoSuchMovimentoVuotoException, SystemException {
        MovimentoVuoto movimentoVuoto = fetchByCodiceSoggetto_First(codiceSoggetto,
                orderByComparator);

        if (movimentoVuoto != null) {
            return movimentoVuoto;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceSoggetto=");
        msg.append(codiceSoggetto);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMovimentoVuotoException(msg.toString());
    }

    /**
     * Returns the first movimento vuoto in the ordered set where codiceSoggetto = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching movimento vuoto, or <code>null</code> if a matching movimento vuoto could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto fetchByCodiceSoggetto_First(String codiceSoggetto,
        OrderByComparator orderByComparator) throws SystemException {
        List<MovimentoVuoto> list = findByCodiceSoggetto(codiceSoggetto, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last movimento vuoto in the ordered set where codiceSoggetto = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching movimento vuoto
     * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a matching movimento vuoto could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto findByCodiceSoggetto_Last(String codiceSoggetto,
        OrderByComparator orderByComparator)
        throws NoSuchMovimentoVuotoException, SystemException {
        MovimentoVuoto movimentoVuoto = fetchByCodiceSoggetto_Last(codiceSoggetto,
                orderByComparator);

        if (movimentoVuoto != null) {
            return movimentoVuoto;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceSoggetto=");
        msg.append(codiceSoggetto);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMovimentoVuotoException(msg.toString());
    }

    /**
     * Returns the last movimento vuoto in the ordered set where codiceSoggetto = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching movimento vuoto, or <code>null</code> if a matching movimento vuoto could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto fetchByCodiceSoggetto_Last(String codiceSoggetto,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByCodiceSoggetto(codiceSoggetto);

        if (count == 0) {
            return null;
        }

        List<MovimentoVuoto> list = findByCodiceSoggetto(codiceSoggetto,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the movimento vuotos before and after the current movimento vuoto in the ordered set where codiceSoggetto = &#63;.
     *
     * @param idMovimento the primary key of the current movimento vuoto
     * @param codiceSoggetto the codice soggetto
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next movimento vuoto
     * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a movimento vuoto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto[] findByCodiceSoggetto_PrevAndNext(long idMovimento,
        String codiceSoggetto, OrderByComparator orderByComparator)
        throws NoSuchMovimentoVuotoException, SystemException {
        MovimentoVuoto movimentoVuoto = findByPrimaryKey(idMovimento);

        Session session = null;

        try {
            session = openSession();

            MovimentoVuoto[] array = new MovimentoVuotoImpl[3];

            array[0] = getByCodiceSoggetto_PrevAndNext(session, movimentoVuoto,
                    codiceSoggetto, orderByComparator, true);

            array[1] = movimentoVuoto;

            array[2] = getByCodiceSoggetto_PrevAndNext(session, movimentoVuoto,
                    codiceSoggetto, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected MovimentoVuoto getByCodiceSoggetto_PrevAndNext(Session session,
        MovimentoVuoto movimentoVuoto, String codiceSoggetto,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_MOVIMENTOVUOTO_WHERE);

        boolean bindCodiceSoggetto = false;

        if (codiceSoggetto == null) {
            query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_1);
        } else if (codiceSoggetto.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_3);
        } else {
            bindCodiceSoggetto = true;

            query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(MovimentoVuotoModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindCodiceSoggetto) {
            qPos.add(codiceSoggetto);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(movimentoVuoto);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<MovimentoVuoto> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the movimento vuotos where codiceSoggetto = &#63; from the database.
     *
     * @param codiceSoggetto the codice soggetto
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByCodiceSoggetto(String codiceSoggetto)
        throws SystemException {
        for (MovimentoVuoto movimentoVuoto : findByCodiceSoggetto(
                codiceSoggetto, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(movimentoVuoto);
        }
    }

    /**
     * Returns the number of movimento vuotos where codiceSoggetto = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @return the number of matching movimento vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByCodiceSoggetto(String codiceSoggetto)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_CODICESOGGETTO;

        Object[] finderArgs = new Object[] { codiceSoggetto };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_MOVIMENTOVUOTO_WHERE);

            boolean bindCodiceSoggetto = false;

            if (codiceSoggetto == null) {
                query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_1);
            } else if (codiceSoggetto.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_3);
            } else {
                bindCodiceSoggetto = true;

                query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceSoggetto) {
                    qPos.add(codiceSoggetto);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the movimento vuoto in the entity cache if it is enabled.
     *
     * @param movimentoVuoto the movimento vuoto
     */
    @Override
    public void cacheResult(MovimentoVuoto movimentoVuoto) {
        EntityCacheUtil.putResult(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
            MovimentoVuotoImpl.class, movimentoVuoto.getPrimaryKey(),
            movimentoVuoto);

        movimentoVuoto.resetOriginalValues();
    }

    /**
     * Caches the movimento vuotos in the entity cache if it is enabled.
     *
     * @param movimentoVuotos the movimento vuotos
     */
    @Override
    public void cacheResult(List<MovimentoVuoto> movimentoVuotos) {
        for (MovimentoVuoto movimentoVuoto : movimentoVuotos) {
            if (EntityCacheUtil.getResult(
                        MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
                        MovimentoVuotoImpl.class, movimentoVuoto.getPrimaryKey()) == null) {
                cacheResult(movimentoVuoto);
            } else {
                movimentoVuoto.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all movimento vuotos.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(MovimentoVuotoImpl.class.getName());
        }

        EntityCacheUtil.clearCache(MovimentoVuotoImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the movimento vuoto.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(MovimentoVuoto movimentoVuoto) {
        EntityCacheUtil.removeResult(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
            MovimentoVuotoImpl.class, movimentoVuoto.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<MovimentoVuoto> movimentoVuotos) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (MovimentoVuoto movimentoVuoto : movimentoVuotos) {
            EntityCacheUtil.removeResult(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
                MovimentoVuotoImpl.class, movimentoVuoto.getPrimaryKey());
        }
    }

    /**
     * Creates a new movimento vuoto with the primary key. Does not add the movimento vuoto to the database.
     *
     * @param idMovimento the primary key for the new movimento vuoto
     * @return the new movimento vuoto
     */
    @Override
    public MovimentoVuoto create(long idMovimento) {
        MovimentoVuoto movimentoVuoto = new MovimentoVuotoImpl();

        movimentoVuoto.setNew(true);
        movimentoVuoto.setPrimaryKey(idMovimento);

        return movimentoVuoto;
    }

    /**
     * Removes the movimento vuoto with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param idMovimento the primary key of the movimento vuoto
     * @return the movimento vuoto that was removed
     * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a movimento vuoto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto remove(long idMovimento)
        throws NoSuchMovimentoVuotoException, SystemException {
        return remove((Serializable) idMovimento);
    }

    /**
     * Removes the movimento vuoto with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the movimento vuoto
     * @return the movimento vuoto that was removed
     * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a movimento vuoto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto remove(Serializable primaryKey)
        throws NoSuchMovimentoVuotoException, SystemException {
        Session session = null;

        try {
            session = openSession();

            MovimentoVuoto movimentoVuoto = (MovimentoVuoto) session.get(MovimentoVuotoImpl.class,
                    primaryKey);

            if (movimentoVuoto == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchMovimentoVuotoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(movimentoVuoto);
        } catch (NoSuchMovimentoVuotoException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected MovimentoVuoto removeImpl(MovimentoVuoto movimentoVuoto)
        throws SystemException {
        movimentoVuoto = toUnwrappedModel(movimentoVuoto);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(movimentoVuoto)) {
                movimentoVuoto = (MovimentoVuoto) session.get(MovimentoVuotoImpl.class,
                        movimentoVuoto.getPrimaryKeyObj());
            }

            if (movimentoVuoto != null) {
                session.delete(movimentoVuoto);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (movimentoVuoto != null) {
            clearCache(movimentoVuoto);
        }

        return movimentoVuoto;
    }

    @Override
    public MovimentoVuoto updateImpl(
        it.bysoftware.ct.model.MovimentoVuoto movimentoVuoto)
        throws SystemException {
        movimentoVuoto = toUnwrappedModel(movimentoVuoto);

        boolean isNew = movimentoVuoto.isNew();

        MovimentoVuotoModelImpl movimentoVuotoModelImpl = (MovimentoVuotoModelImpl) movimentoVuoto;

        Session session = null;

        try {
            session = openSession();

            if (movimentoVuoto.isNew()) {
                session.save(movimentoVuoto);

                movimentoVuoto.setNew(false);
            } else {
                session.merge(movimentoVuoto);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !MovimentoVuotoModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((movimentoVuotoModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEVUOTO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        movimentoVuotoModelImpl.getOriginalCodiceVuoto()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICEVUOTO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEVUOTO,
                    args);

                args = new Object[] { movimentoVuotoModelImpl.getCodiceVuoto() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICEVUOTO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEVUOTO,
                    args);
            }

            if ((movimentoVuotoModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYVUOTOSOGGETTODATA.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        movimentoVuotoModelImpl.getOriginalCodiceVuoto(),
                        movimentoVuotoModelImpl.getOriginalCodiceSoggetto(),
                        movimentoVuotoModelImpl.getOriginalDataMovimento(),
                        movimentoVuotoModelImpl.getOriginalTipoMovimento()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FINDBYVUOTOSOGGETTODATA,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYVUOTOSOGGETTODATA,
                    args);

                args = new Object[] {
                        movimentoVuotoModelImpl.getCodiceVuoto(),
                        movimentoVuotoModelImpl.getCodiceSoggetto(),
                        movimentoVuotoModelImpl.getDataMovimento(),
                        movimentoVuotoModelImpl.getTipoMovimento()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FINDBYVUOTOSOGGETTODATA,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYVUOTOSOGGETTODATA,
                    args);
            }

            if ((movimentoVuotoModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICESOGGETTO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        movimentoVuotoModelImpl.getOriginalCodiceSoggetto()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICESOGGETTO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICESOGGETTO,
                    args);

                args = new Object[] { movimentoVuotoModelImpl.getCodiceSoggetto() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICESOGGETTO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICESOGGETTO,
                    args);
            }
        }

        EntityCacheUtil.putResult(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
            MovimentoVuotoImpl.class, movimentoVuoto.getPrimaryKey(),
            movimentoVuoto);

        return movimentoVuoto;
    }

    protected MovimentoVuoto toUnwrappedModel(MovimentoVuoto movimentoVuoto) {
        if (movimentoVuoto instanceof MovimentoVuotoImpl) {
            return movimentoVuoto;
        }

        MovimentoVuotoImpl movimentoVuotoImpl = new MovimentoVuotoImpl();

        movimentoVuotoImpl.setNew(movimentoVuoto.isNew());
        movimentoVuotoImpl.setPrimaryKey(movimentoVuoto.getPrimaryKey());

        movimentoVuotoImpl.setIdMovimento(movimentoVuoto.getIdMovimento());
        movimentoVuotoImpl.setCodiceVuoto(movimentoVuoto.getCodiceVuoto());
        movimentoVuotoImpl.setDataMovimento(movimentoVuoto.getDataMovimento());
        movimentoVuotoImpl.setCodiceSoggetto(movimentoVuoto.getCodiceSoggetto());
        movimentoVuotoImpl.setQuantita(movimentoVuoto.getQuantita());
        movimentoVuotoImpl.setTipoMovimento(movimentoVuoto.getTipoMovimento());
        movimentoVuotoImpl.setNote(movimentoVuoto.getNote());
        movimentoVuotoImpl.setUuid(movimentoVuoto.getUuid());

        return movimentoVuotoImpl;
    }

    /**
     * Returns the movimento vuoto with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the movimento vuoto
     * @return the movimento vuoto
     * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a movimento vuoto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto findByPrimaryKey(Serializable primaryKey)
        throws NoSuchMovimentoVuotoException, SystemException {
        MovimentoVuoto movimentoVuoto = fetchByPrimaryKey(primaryKey);

        if (movimentoVuoto == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchMovimentoVuotoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return movimentoVuoto;
    }

    /**
     * Returns the movimento vuoto with the primary key or throws a {@link it.bysoftware.ct.NoSuchMovimentoVuotoException} if it could not be found.
     *
     * @param idMovimento the primary key of the movimento vuoto
     * @return the movimento vuoto
     * @throws it.bysoftware.ct.NoSuchMovimentoVuotoException if a movimento vuoto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto findByPrimaryKey(long idMovimento)
        throws NoSuchMovimentoVuotoException, SystemException {
        return findByPrimaryKey((Serializable) idMovimento);
    }

    /**
     * Returns the movimento vuoto with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the movimento vuoto
     * @return the movimento vuoto, or <code>null</code> if a movimento vuoto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        MovimentoVuoto movimentoVuoto = (MovimentoVuoto) EntityCacheUtil.getResult(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
                MovimentoVuotoImpl.class, primaryKey);

        if (movimentoVuoto == _nullMovimentoVuoto) {
            return null;
        }

        if (movimentoVuoto == null) {
            Session session = null;

            try {
                session = openSession();

                movimentoVuoto = (MovimentoVuoto) session.get(MovimentoVuotoImpl.class,
                        primaryKey);

                if (movimentoVuoto != null) {
                    cacheResult(movimentoVuoto);
                } else {
                    EntityCacheUtil.putResult(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
                        MovimentoVuotoImpl.class, primaryKey,
                        _nullMovimentoVuoto);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(MovimentoVuotoModelImpl.ENTITY_CACHE_ENABLED,
                    MovimentoVuotoImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return movimentoVuoto;
    }

    /**
     * Returns the movimento vuoto with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param idMovimento the primary key of the movimento vuoto
     * @return the movimento vuoto, or <code>null</code> if a movimento vuoto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MovimentoVuoto fetchByPrimaryKey(long idMovimento)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) idMovimento);
    }

    /**
     * Returns all the movimento vuotos.
     *
     * @return the movimento vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<MovimentoVuoto> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the movimento vuotos.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of movimento vuotos
     * @param end the upper bound of the range of movimento vuotos (not inclusive)
     * @return the range of movimento vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<MovimentoVuoto> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the movimento vuotos.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentoVuotoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of movimento vuotos
     * @param end the upper bound of the range of movimento vuotos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of movimento vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<MovimentoVuoto> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<MovimentoVuoto> list = (List<MovimentoVuoto>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_MOVIMENTOVUOTO);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_MOVIMENTOVUOTO;

                if (pagination) {
                    sql = sql.concat(MovimentoVuotoModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<MovimentoVuoto>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<MovimentoVuoto>(list);
                } else {
                    list = (List<MovimentoVuoto>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the movimento vuotos from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (MovimentoVuoto movimentoVuoto : findAll()) {
            remove(movimentoVuoto);
        }
    }

    /**
     * Returns the number of movimento vuotos.
     *
     * @return the number of movimento vuotos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_MOVIMENTOVUOTO);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the movimento vuoto persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.MovimentoVuoto")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<MovimentoVuoto>> listenersList = new ArrayList<ModelListener<MovimentoVuoto>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<MovimentoVuoto>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(MovimentoVuotoImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
