package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.NoSuchTestataFattureClientiException;
import it.bysoftware.ct.model.TestataFattureClienti;
import it.bysoftware.ct.service.TestataFattureClientiLocalServiceUtil;
import it.bysoftware.ct.service.base.TestataFattureClientiLocalServiceBaseImpl;
import it.bysoftware.ct.utils.Utils;

import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Junction;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.util.portlet.PortletProps;

/**
 * The implementation of the testata fatture clienti local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.TestataFattureClientiLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.TestataFattureClientiLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.TestataFattureClientiLocalServiceUtil
 */
public class TestataFattureClientiLocalServiceImpl extends
		TestataFattureClientiLocalServiceBaseImpl {

	/**
	 * Logger object. Based on Liferay logger
	 * {@link com.liferay.portal.kernel.log.Log} commons logging.
	 */
	private Log logger = LogFactoryUtil
			.getLog(TestataFattureClientiLocalServiceImpl.class);

	@Override
	public final TestataFattureClienti getInvoice(final int year,
			final String activity, final String center, final int docNumber,
			final Date docDate, final boolean subjectType,
			final String subjectCode) {

		TestataFattureClienti result = null;
		try {
			this.logger.debug("Look for invoice: " + year + " " + activity
					+ " " + center + " " + docNumber);
			result = this.testataFattureClientiPersistence
					.findByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(year,
							activity, center, docNumber, docDate, subjectType,
							subjectCode);
		} catch (SystemException e) {
			this.logger.error(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
		} catch (NoSuchTestataFattureClientiException e) {
			this.logger.error(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
		}

		return result;
	}

	@Override
	public final int getInvoicesCount(final String customerCode,
			final String category, final Date from, final Date to,
			final boolean andSearch) throws SystemException {
		DynamicQuery dynamicQuery = this.buildDynamicQuery(customerCode,
				category, from, to, andSearch);
		return (int) TestataFattureClientiLocalServiceUtil
				.dynamicQueryCount(dynamicQuery);
	}

	@Override
	public final List<TestataFattureClienti> getInvoices(final int docNum,
			final String centerCode) throws SystemException {
		return this.getInvoices(docNum, centerCode, Utils.getYear());
	}

	@Override
	public final List<TestataFattureClienti> getInvoices(final int docNum,
			final String centerCode, final int year) throws SystemException {
		return this.testataFattureClientiPersistence
				.findByAnnoCodiceAttivitaCodiceCentroNumeroDocumentoTipoSoggetto(
						year, PortletProps.get("activity-code"), centerCode,
						docNum, false);
	}

	@SuppressWarnings("unchecked")
	@Override
	public final List<TestataFattureClienti> getInvoices(
			final String customerCode, final String category, final Date from,
			final Date to, final boolean andSearch, final int start,
			final int end, final OrderByComparator orderByComparator)
			throws SystemException {
		DynamicQuery dynamicQuery = this.buildDynamicQuery(customerCode,
				category, from, to, andSearch);
		return TestataFattureClientiLocalServiceUtil.dynamicQuery(dynamicQuery,
				start, end, orderByComparator);
	}

	private DynamicQuery buildDynamicQuery(String customerCode,
			String category, Date from, Date to, boolean andSearch) {

		Junction junction = null;
		if (andSearch) {
			junction = RestrictionsFactoryUtil.conjunction();
		} else {
			junction = RestrictionsFactoryUtil.disjunction();
		}

		if (Validator.isNotNull(category)) {
			Property property = PropertyFactoryUtil
					.forName("codiceCategoriaEconomica");
			junction.add(property.eq(category));
		}

		if (Validator.isNotNull(customerCode)) {
			Property property = PropertyFactoryUtil.forName("codiceCliente");
			junction.add(property.eq(customerCode));
		}

		if (Validator.isNotNull(from)) {
			Property property = PropertyFactoryUtil.forName("dataDocumento");
			if (Validator.isNotNull(to)) {
				junction.add(property.between(from, to));
			} else {
				junction.add(property.ge(from));
			}
		} else if (Validator.isNotNull(to)) {
			Property property = PropertyFactoryUtil.forName("dataDocumento");
			junction.add(property.le(to));
		}

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
				TestataFattureClienti.class, getClassLoader());
		return dynamicQuery.add(junction);
	}

}
