package it.bysoftware.ct.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Junction;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import it.bysoftware.ct.model.AnagraficheClientiFornitori;
import it.bysoftware.ct.model.ClientiFornitoriDatiAgg;
import it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil;
import it.bysoftware.ct.service.base.
    AnagraficheClientiFornitoriLocalServiceBaseImpl;
import it.bysoftware.ct.utils.SubjectComparator;

/**
 * The implementation of the anagrafiche clienti fornitori local service.
 *
 * <p> All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalService}
 * interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.
 *      AnagraficheClientiFornitoriLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil
 */
public class AnagraficheClientiFornitoriLocalServiceImpl extends
        AnagraficheClientiFornitoriLocalServiceBaseImpl {

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil
            .getLog(AnagraficheClientiFornitoriLocalServiceImpl.class);

    @Override
    public final List<AnagraficheClientiFornitori> findCustomers() {
        List<AnagraficheClientiFornitori> customers = this.getSubjects(false,
                false);
        return customers;
    }
    
    @Override
    public final List<AnagraficheClientiFornitori> findCustomersInCategory(
    		String category) {
        List<AnagraficheClientiFornitori> customers = this.getSubjects(false,
                false, category);
        return customers;
    }

    @Override
    public final List<AnagraficheClientiFornitori> findAllSuppliers() {
        List<AnagraficheClientiFornitori> suppliers = this.getSubjects(true,
                null);
        return suppliers;
    }
    
    @Override
    public final List<AnagraficheClientiFornitori> findSuppliers() {
        List<AnagraficheClientiFornitori> suppliers = this.getSubjects(true,
                false);
        return suppliers;
    }

    @Override
    public final List<AnagraficheClientiFornitori> findAssociates() {
        List<AnagraficheClientiFornitori> suppliers = this.getSubjects(true,
                true);
        return suppliers;
    }

    @SuppressWarnings("unchecked")
    @Override
    public final List<AnagraficheClientiFornitori> getCustomers(
            final String customerCode, final String businessName,
            final String vatCode, final String country, final String city,
            final boolean andSearch, final int start, final int end,
            final OrderByComparator orderByComparator) throws SystemException {
        DynamicQuery dynamicQuery = this.buildDynamicQuery(customerCode,
                businessName, vatCode, country, city, andSearch);
        return AnagraficheClientiFornitoriLocalServiceUtil.dynamicQuery(
                dynamicQuery, start, end, orderByComparator);
    }
    
    @Override
    public final int getCustomersCount(final String customerCode,
            final String businessName, final String vatCode,
            final String country, final String city, final boolean andSearch)
                    throws SystemException {
        DynamicQuery dynamicQuery = this.buildDynamicQuery(customerCode,
                businessName, vatCode, country, city, andSearch);
        return (int) AnagraficheClientiFornitoriLocalServiceUtil
                .dynamicQueryCount(dynamicQuery);
    }

    /**
     * Build {@link DynamicQuery} based on the filter specified.
     * 
     * @param customerCode
     *            customer code
     * @param businessName
     *            customer business name
     * @param vatCode
     *            customer vat code
     * @param country
     *            customer country code
     * @param city
     *            customer city
     * @param andSearch
     *            true if is and search false otherwise
     * @return return a {@link DynamicQuery}
     */
    private DynamicQuery buildDynamicQuery(final String customerCode,
            final String businessName, final String vatCode,
            final String country, final String city, final boolean andSearch) {
        Junction junction = null;
        if (andSearch) {
            junction = RestrictionsFactoryUtil.conjunction();
        } else {
            junction = RestrictionsFactoryUtil.disjunction();
        }
        if (Validator.isNotNull(customerCode)) {
            Property property = PropertyFactoryUtil.forName("codiceAnagrafica");
            String value = (new StringBuilder(StringPool.PERCENT)).
                    append(customerCode).append(StringPool.PERCENT).toString();
            junction.add(property.like(value));
        }
        if (Validator.isNotNull(businessName)) {
            Property property1 = PropertyFactoryUtil.forName("ragioneSociale1");
            String value = (new StringBuilder(StringPool.PERCENT)).
                    append(businessName).append(StringPool.PERCENT).toString();
            Property property2 = PropertyFactoryUtil.forName("ragioneSociale2");
            value = (new StringBuilder(StringPool.PERCENT)).
                    append(businessName).append(StringPool.PERCENT).toString();
            junction.add(RestrictionsFactoryUtil.or(property1.like(value),
            		property2.like(value)));
        }
        if (Validator.isNotNull(vatCode)) {
            Property property = PropertyFactoryUtil.forName("partitaIVA");
            String value = (new StringBuilder(StringPool.PERCENT)).
                    append(vatCode).append(StringPool.PERCENT).toString();
            junction.add(property.like(value));
        }
        if (Validator.isNotNull(city)) {
            Property property = PropertyFactoryUtil.forName("comune");
            String value = (new StringBuilder(StringPool.PERCENT)).
                    append(city).append(StringPool.PERCENT).toString();
            junction.add(property.like(value));
        }
        if (country != null && !country.isEmpty()) {
            Property property = PropertyFactoryUtil.forName("siglaStato");
            junction.add(property.eq(country));
        }
        
        DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
                AnagraficheClientiFornitori.class, getClassLoader());
        return dynamicQuery.add(junction);
    }

    /**
     * Returns a list of subjects.
     * 
     * @param supplier
     *            true if look for suppliers, false otherwise.
     * @param associate
     *            true if look for associate suppliers, false for generic
     *            supplier.
     * @return list of subjects
     */
    private List<AnagraficheClientiFornitori> getSubjects(
            final Boolean supplier, final Boolean associate) {
        return this.getSubjects(supplier, associate, null);
    }
    
    /**
     * Returns a list of subjects.
     * 
     * @param supplier
     *            true if look for suppliers, false otherwise.
     * @param associate
     *            true if look for associate suppliers, false for generic
     *            supplier.
     * @return list of subjects
     */
    @SuppressWarnings("unchecked")
	private List<AnagraficheClientiFornitori> getSubjects(
			final Boolean supplier, final Boolean associate,
			final String category) {
		List<AnagraficheClientiFornitori> result =
                new ArrayList<AnagraficheClientiFornitori>();
        List<ClientiFornitoriDatiAgg> subjectsData = null;

        if (supplier) {
            if (associate != null && associate) {
                subjectsData = ClientiFornitoriDatiAggLocalServiceUtil.
                        findAllAssociates();
            } else if (associate != null && !associate) {
                subjectsData = ClientiFornitoriDatiAggLocalServiceUtil.
                        findAllSuppliers();
            } else {
                subjectsData = ClientiFornitoriDatiAggLocalServiceUtil.
                        findGenericSuppliers();
            }
		} else if (Validator.isNotNull(category)) {
			subjectsData = ClientiFornitoriDatiAggLocalServiceUtil
					.findAllCustomersInCategory(category);
		} else {
            subjectsData = ClientiFornitoriDatiAggLocalServiceUtil
                    .findAllCustomers();
        }

        for (ClientiFornitoriDatiAgg subjectData : subjectsData) {
            try {
                AnagraficheClientiFornitori customer =
                        AnagraficheClientiFornitoriLocalServiceUtil.
                        getAnagraficheClientiFornitori(subjectData.
                                getCodiceAnagrafica());
                result.add(customer);
            } catch (PortalException ex) {
                this.logger.error(ex.getMessage());
                if (this.logger.isDebugEnabled()) {
                    ex.printStackTrace();
                }
            } catch (SystemException ex) {
                this.logger.error(ex.getMessage());
                if (this.logger.isDebugEnabled()) {
                    ex.printStackTrace();
                }
            }
        }

        Collections.sort(result, new SubjectComparator());
        return result;
    }
}
