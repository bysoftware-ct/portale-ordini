package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchCategorieException;
import it.bysoftware.ct.model.Categorie;
import it.bysoftware.ct.model.impl.CategorieImpl;
import it.bysoftware.ct.model.impl.CategorieModelImpl;
import it.bysoftware.ct.service.persistence.CategoriePersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the categorie service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CategoriePersistence
 * @see CategorieUtil
 * @generated
 */
public class CategoriePersistenceImpl extends BasePersistenceImpl<Categorie>
    implements CategoriePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link CategorieUtil} to access the categorie persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = CategorieImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CategorieModelImpl.ENTITY_CACHE_ENABLED,
            CategorieModelImpl.FINDER_CACHE_ENABLED, CategorieImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CategorieModelImpl.ENTITY_CACHE_ENABLED,
            CategorieModelImpl.FINDER_CACHE_ENABLED, CategorieImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CategorieModelImpl.ENTITY_CACHE_ENABLED,
            CategorieModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_CATEGORIE = "SELECT categorie FROM Categorie categorie";
    private static final String _SQL_COUNT_CATEGORIE = "SELECT COUNT(categorie) FROM Categorie categorie";
    private static final String _ORDER_BY_ENTITY_ALIAS = "categorie.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Categorie exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(CategoriePersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "nomeIta", "nomeEng", "nomeTed", "nomeFra", "nomeSpa"
            });
    private static Categorie _nullCategorie = new CategorieImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Categorie> toCacheModel() {
                return _nullCategorieCacheModel;
            }
        };

    private static CacheModel<Categorie> _nullCategorieCacheModel = new CacheModel<Categorie>() {
            @Override
            public Categorie toEntityModel() {
                return _nullCategorie;
            }
        };

    public CategoriePersistenceImpl() {
        setModelClass(Categorie.class);
    }

    /**
     * Caches the categorie in the entity cache if it is enabled.
     *
     * @param categorie the categorie
     */
    @Override
    public void cacheResult(Categorie categorie) {
        EntityCacheUtil.putResult(CategorieModelImpl.ENTITY_CACHE_ENABLED,
            CategorieImpl.class, categorie.getPrimaryKey(), categorie);

        categorie.resetOriginalValues();
    }

    /**
     * Caches the categories in the entity cache if it is enabled.
     *
     * @param categories the categories
     */
    @Override
    public void cacheResult(List<Categorie> categories) {
        for (Categorie categorie : categories) {
            if (EntityCacheUtil.getResult(
                        CategorieModelImpl.ENTITY_CACHE_ENABLED,
                        CategorieImpl.class, categorie.getPrimaryKey()) == null) {
                cacheResult(categorie);
            } else {
                categorie.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all categories.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(CategorieImpl.class.getName());
        }

        EntityCacheUtil.clearCache(CategorieImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the categorie.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Categorie categorie) {
        EntityCacheUtil.removeResult(CategorieModelImpl.ENTITY_CACHE_ENABLED,
            CategorieImpl.class, categorie.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Categorie> categories) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Categorie categorie : categories) {
            EntityCacheUtil.removeResult(CategorieModelImpl.ENTITY_CACHE_ENABLED,
                CategorieImpl.class, categorie.getPrimaryKey());
        }
    }

    /**
     * Creates a new categorie with the primary key. Does not add the categorie to the database.
     *
     * @param id the primary key for the new categorie
     * @return the new categorie
     */
    @Override
    public Categorie create(long id) {
        Categorie categorie = new CategorieImpl();

        categorie.setNew(true);
        categorie.setPrimaryKey(id);

        return categorie;
    }

    /**
     * Removes the categorie with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the categorie
     * @return the categorie that was removed
     * @throws it.bysoftware.ct.NoSuchCategorieException if a categorie with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Categorie remove(long id)
        throws NoSuchCategorieException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the categorie with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the categorie
     * @return the categorie that was removed
     * @throws it.bysoftware.ct.NoSuchCategorieException if a categorie with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Categorie remove(Serializable primaryKey)
        throws NoSuchCategorieException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Categorie categorie = (Categorie) session.get(CategorieImpl.class,
                    primaryKey);

            if (categorie == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchCategorieException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(categorie);
        } catch (NoSuchCategorieException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Categorie removeImpl(Categorie categorie)
        throws SystemException {
        categorie = toUnwrappedModel(categorie);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(categorie)) {
                categorie = (Categorie) session.get(CategorieImpl.class,
                        categorie.getPrimaryKeyObj());
            }

            if (categorie != null) {
                session.delete(categorie);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (categorie != null) {
            clearCache(categorie);
        }

        return categorie;
    }

    @Override
    public Categorie updateImpl(it.bysoftware.ct.model.Categorie categorie)
        throws SystemException {
        categorie = toUnwrappedModel(categorie);

        boolean isNew = categorie.isNew();

        Session session = null;

        try {
            session = openSession();

            if (categorie.isNew()) {
                session.save(categorie);

                categorie.setNew(false);
            } else {
                session.merge(categorie);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(CategorieModelImpl.ENTITY_CACHE_ENABLED,
            CategorieImpl.class, categorie.getPrimaryKey(), categorie);

        return categorie;
    }

    protected Categorie toUnwrappedModel(Categorie categorie) {
        if (categorie instanceof CategorieImpl) {
            return categorie;
        }

        CategorieImpl categorieImpl = new CategorieImpl();

        categorieImpl.setNew(categorie.isNew());
        categorieImpl.setPrimaryKey(categorie.getPrimaryKey());

        categorieImpl.setId(categorie.getId());
        categorieImpl.setNomeIta(categorie.getNomeIta());
        categorieImpl.setNomeEng(categorie.getNomeEng());
        categorieImpl.setNomeTed(categorie.getNomeTed());
        categorieImpl.setNomeFra(categorie.getNomeFra());
        categorieImpl.setNomeSpa(categorie.getNomeSpa());
        categorieImpl.setPredefinita(categorie.isPredefinita());

        return categorieImpl;
    }

    /**
     * Returns the categorie with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the categorie
     * @return the categorie
     * @throws it.bysoftware.ct.NoSuchCategorieException if a categorie with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Categorie findByPrimaryKey(Serializable primaryKey)
        throws NoSuchCategorieException, SystemException {
        Categorie categorie = fetchByPrimaryKey(primaryKey);

        if (categorie == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchCategorieException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return categorie;
    }

    /**
     * Returns the categorie with the primary key or throws a {@link it.bysoftware.ct.NoSuchCategorieException} if it could not be found.
     *
     * @param id the primary key of the categorie
     * @return the categorie
     * @throws it.bysoftware.ct.NoSuchCategorieException if a categorie with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Categorie findByPrimaryKey(long id)
        throws NoSuchCategorieException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the categorie with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the categorie
     * @return the categorie, or <code>null</code> if a categorie with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Categorie fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Categorie categorie = (Categorie) EntityCacheUtil.getResult(CategorieModelImpl.ENTITY_CACHE_ENABLED,
                CategorieImpl.class, primaryKey);

        if (categorie == _nullCategorie) {
            return null;
        }

        if (categorie == null) {
            Session session = null;

            try {
                session = openSession();

                categorie = (Categorie) session.get(CategorieImpl.class,
                        primaryKey);

                if (categorie != null) {
                    cacheResult(categorie);
                } else {
                    EntityCacheUtil.putResult(CategorieModelImpl.ENTITY_CACHE_ENABLED,
                        CategorieImpl.class, primaryKey, _nullCategorie);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(CategorieModelImpl.ENTITY_CACHE_ENABLED,
                    CategorieImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return categorie;
    }

    /**
     * Returns the categorie with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the categorie
     * @return the categorie, or <code>null</code> if a categorie with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Categorie fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the categories.
     *
     * @return the categories
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Categorie> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the categories.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CategorieModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of categories
     * @param end the upper bound of the range of categories (not inclusive)
     * @return the range of categories
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Categorie> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the categories.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CategorieModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of categories
     * @param end the upper bound of the range of categories (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of categories
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Categorie> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Categorie> list = (List<Categorie>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_CATEGORIE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_CATEGORIE;

                if (pagination) {
                    sql = sql.concat(CategorieModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Categorie>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Categorie>(list);
                } else {
                    list = (List<Categorie>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the categories from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Categorie categorie : findAll()) {
            remove(categorie);
        }
    }

    /**
     * Returns the number of categories.
     *
     * @return the number of categories
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_CATEGORIE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the categorie persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.Categorie")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Categorie>> listenersList = new ArrayList<ModelListener<Categorie>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Categorie>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(CategorieImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
