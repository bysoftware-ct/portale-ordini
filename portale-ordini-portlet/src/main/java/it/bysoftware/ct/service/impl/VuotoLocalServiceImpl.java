package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.VuotoLocalServiceBaseImpl;

/**
 * The implementation of the vuoto local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.bysoftware.ct.service.VuotoLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.VuotoLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.VuotoLocalServiceUtil
 */
public class VuotoLocalServiceImpl extends VuotoLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link it.bysoftware.ct.service.VuotoLocalServiceUtil} to access the vuoto local service.
     */
}
