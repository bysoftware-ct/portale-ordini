package it.bysoftware.ct.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.portlet.PortletProps;

import it.bysoftware.ct.model.ClientiFornitoriDatiAgg;
import it.bysoftware.ct.service.base.ClientiFornitoriDatiAggLocalServiceBaseImpl;

/**
 * The implementation of the clienti fornitori dati agg local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalService}
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.
 *      ClientiFornitoriDatiAggLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil
 */
public class ClientiFornitoriDatiAggLocalServiceImpl extends
		ClientiFornitoriDatiAggLocalServiceBaseImpl {

	/**
	 * Items supplier economic category code.
	 */
	private static final String ITEMS_SUPPLIER = PortletProps
			.get("supplier-category-code");

	/**
	 * Logger object. Based on Liferay logger
	 * {@link com.liferay.portal.kernel.log.Log} commons logging.
	 */
	private Log logger = LogFactoryUtil
			.getLog(ClientiFornitoriDatiAggLocalServiceImpl.class);

	@Override
	public final List<ClientiFornitoriDatiAgg> findAllCustomersInCategory(
			String category) {
		try {
			return this.clientiFornitoriDatiAggPersistence
					.findByClientiCategoria(false, category);
		} catch (SystemException e) {
			this.logger.error(e.getMessage());
			return new ArrayList<ClientiFornitoriDatiAgg>();
		}
	}

	@Override
	public final List<ClientiFornitoriDatiAgg> findAllCustomers() {
		try {
			return this.clientiFornitoriDatiAggPersistence.findByClienti(false);
		} catch (SystemException e) {
			this.logger.error(e.getMessage());
			return new ArrayList<ClientiFornitoriDatiAgg>();
		}
	}

	@Override
	public final List<ClientiFornitoriDatiAgg> findAllSuppliers() {
		try {
			return this.clientiFornitoriDatiAggPersistence.findByFornitori(
					true, ITEMS_SUPPLIER);
		} catch (SystemException e) {
			this.logger.error(e.getMessage());
			return new ArrayList<ClientiFornitoriDatiAgg>();
		}
	}

	@Override
	public final List<ClientiFornitoriDatiAgg> findGenericSuppliers() {
		try {
			return this.clientiFornitoriDatiAggPersistence
					.findByTuttiFornitori(true);
		} catch (SystemException e) {
			this.logger.error(e.getMessage());
			return new ArrayList<ClientiFornitoriDatiAgg>();
		}
	}

	@Override
	public final List<ClientiFornitoriDatiAgg> findAllAssociates() {
		try {
			return this.clientiFornitoriDatiAggPersistence.findByAssociato(
					true, true);
		} catch (SystemException e) {
			this.logger.error(e.getMessage());
			return new ArrayList<ClientiFornitoriDatiAgg>();
		}
	}
}
