package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.StatiLocalServiceBaseImpl;

/**
 * The implementation of the stati local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.StatiLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.StatiLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.StatiLocalServiceUtil
 */
public class StatiLocalServiceImpl extends StatiLocalServiceBaseImpl {

}
