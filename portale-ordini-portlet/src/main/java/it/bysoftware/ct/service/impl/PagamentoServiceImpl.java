package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.PagamentoServiceBaseImpl;

/**
 * The implementation of the pagamento remote service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.PagamentoService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.PagamentoServiceBaseImpl
 * @see it.bysoftware.ct.service.PagamentoServiceUtil
 */
public class PagamentoServiceImpl extends PagamentoServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.PagamentoServiceUtil} to access the pagamento
     * remote service.
     */
}
