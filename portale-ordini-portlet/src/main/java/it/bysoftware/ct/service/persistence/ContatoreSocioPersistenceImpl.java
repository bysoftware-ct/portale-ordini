package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchContatoreSocioException;
import it.bysoftware.ct.model.ContatoreSocio;
import it.bysoftware.ct.model.impl.ContatoreSocioImpl;
import it.bysoftware.ct.model.impl.ContatoreSocioModelImpl;
import it.bysoftware.ct.service.persistence.ContatoreSocioPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the contatore socio service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see ContatoreSocioPersistence
 * @see ContatoreSocioUtil
 * @generated
 */
public class ContatoreSocioPersistenceImpl extends BasePersistenceImpl<ContatoreSocio>
    implements ContatoreSocioPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link ContatoreSocioUtil} to access the contatore socio persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = ContatoreSocioImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ContatoreSocioModelImpl.ENTITY_CACHE_ENABLED,
            ContatoreSocioModelImpl.FINDER_CACHE_ENABLED,
            ContatoreSocioImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ContatoreSocioModelImpl.ENTITY_CACHE_ENABLED,
            ContatoreSocioModelImpl.FINDER_CACHE_ENABLED,
            ContatoreSocioImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ContatoreSocioModelImpl.ENTITY_CACHE_ENABLED,
            ContatoreSocioModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_CONTATORESOCIO = "SELECT contatoreSocio FROM ContatoreSocio contatoreSocio";
    private static final String _SQL_COUNT_CONTATORESOCIO = "SELECT COUNT(contatoreSocio) FROM ContatoreSocio contatoreSocio";
    private static final String _ORDER_BY_ENTITY_ALIAS = "contatoreSocio.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ContatoreSocio exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(ContatoreSocioPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "codiceSocio", "tipoDocumento"
            });
    private static ContatoreSocio _nullContatoreSocio = new ContatoreSocioImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<ContatoreSocio> toCacheModel() {
                return _nullContatoreSocioCacheModel;
            }
        };

    private static CacheModel<ContatoreSocio> _nullContatoreSocioCacheModel = new CacheModel<ContatoreSocio>() {
            @Override
            public ContatoreSocio toEntityModel() {
                return _nullContatoreSocio;
            }
        };

    public ContatoreSocioPersistenceImpl() {
        setModelClass(ContatoreSocio.class);
    }

    /**
     * Caches the contatore socio in the entity cache if it is enabled.
     *
     * @param contatoreSocio the contatore socio
     */
    @Override
    public void cacheResult(ContatoreSocio contatoreSocio) {
        EntityCacheUtil.putResult(ContatoreSocioModelImpl.ENTITY_CACHE_ENABLED,
            ContatoreSocioImpl.class, contatoreSocio.getPrimaryKey(),
            contatoreSocio);

        contatoreSocio.resetOriginalValues();
    }

    /**
     * Caches the contatore socios in the entity cache if it is enabled.
     *
     * @param contatoreSocios the contatore socios
     */
    @Override
    public void cacheResult(List<ContatoreSocio> contatoreSocios) {
        for (ContatoreSocio contatoreSocio : contatoreSocios) {
            if (EntityCacheUtil.getResult(
                        ContatoreSocioModelImpl.ENTITY_CACHE_ENABLED,
                        ContatoreSocioImpl.class, contatoreSocio.getPrimaryKey()) == null) {
                cacheResult(contatoreSocio);
            } else {
                contatoreSocio.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all contatore socios.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(ContatoreSocioImpl.class.getName());
        }

        EntityCacheUtil.clearCache(ContatoreSocioImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the contatore socio.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(ContatoreSocio contatoreSocio) {
        EntityCacheUtil.removeResult(ContatoreSocioModelImpl.ENTITY_CACHE_ENABLED,
            ContatoreSocioImpl.class, contatoreSocio.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<ContatoreSocio> contatoreSocios) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (ContatoreSocio contatoreSocio : contatoreSocios) {
            EntityCacheUtil.removeResult(ContatoreSocioModelImpl.ENTITY_CACHE_ENABLED,
                ContatoreSocioImpl.class, contatoreSocio.getPrimaryKey());
        }
    }

    /**
     * Creates a new contatore socio with the primary key. Does not add the contatore socio to the database.
     *
     * @param contatoreSocioPK the primary key for the new contatore socio
     * @return the new contatore socio
     */
    @Override
    public ContatoreSocio create(ContatoreSocioPK contatoreSocioPK) {
        ContatoreSocio contatoreSocio = new ContatoreSocioImpl();

        contatoreSocio.setNew(true);
        contatoreSocio.setPrimaryKey(contatoreSocioPK);

        return contatoreSocio;
    }

    /**
     * Removes the contatore socio with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param contatoreSocioPK the primary key of the contatore socio
     * @return the contatore socio that was removed
     * @throws it.bysoftware.ct.NoSuchContatoreSocioException if a contatore socio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ContatoreSocio remove(ContatoreSocioPK contatoreSocioPK)
        throws NoSuchContatoreSocioException, SystemException {
        return remove((Serializable) contatoreSocioPK);
    }

    /**
     * Removes the contatore socio with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the contatore socio
     * @return the contatore socio that was removed
     * @throws it.bysoftware.ct.NoSuchContatoreSocioException if a contatore socio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ContatoreSocio remove(Serializable primaryKey)
        throws NoSuchContatoreSocioException, SystemException {
        Session session = null;

        try {
            session = openSession();

            ContatoreSocio contatoreSocio = (ContatoreSocio) session.get(ContatoreSocioImpl.class,
                    primaryKey);

            if (contatoreSocio == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchContatoreSocioException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(contatoreSocio);
        } catch (NoSuchContatoreSocioException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected ContatoreSocio removeImpl(ContatoreSocio contatoreSocio)
        throws SystemException {
        contatoreSocio = toUnwrappedModel(contatoreSocio);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(contatoreSocio)) {
                contatoreSocio = (ContatoreSocio) session.get(ContatoreSocioImpl.class,
                        contatoreSocio.getPrimaryKeyObj());
            }

            if (contatoreSocio != null) {
                session.delete(contatoreSocio);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (contatoreSocio != null) {
            clearCache(contatoreSocio);
        }

        return contatoreSocio;
    }

    @Override
    public ContatoreSocio updateImpl(
        it.bysoftware.ct.model.ContatoreSocio contatoreSocio)
        throws SystemException {
        contatoreSocio = toUnwrappedModel(contatoreSocio);

        boolean isNew = contatoreSocio.isNew();

        Session session = null;

        try {
            session = openSession();

            if (contatoreSocio.isNew()) {
                session.save(contatoreSocio);

                contatoreSocio.setNew(false);
            } else {
                session.merge(contatoreSocio);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(ContatoreSocioModelImpl.ENTITY_CACHE_ENABLED,
            ContatoreSocioImpl.class, contatoreSocio.getPrimaryKey(),
            contatoreSocio);

        return contatoreSocio;
    }

    protected ContatoreSocio toUnwrappedModel(ContatoreSocio contatoreSocio) {
        if (contatoreSocio instanceof ContatoreSocioImpl) {
            return contatoreSocio;
        }

        ContatoreSocioImpl contatoreSocioImpl = new ContatoreSocioImpl();

        contatoreSocioImpl.setNew(contatoreSocio.isNew());
        contatoreSocioImpl.setPrimaryKey(contatoreSocio.getPrimaryKey());

        contatoreSocioImpl.setAnno(contatoreSocio.getAnno());
        contatoreSocioImpl.setCodiceSocio(contatoreSocio.getCodiceSocio());
        contatoreSocioImpl.setTipoDocumento(contatoreSocio.getTipoDocumento());
        contatoreSocioImpl.setNumero(contatoreSocio.getNumero());

        return contatoreSocioImpl;
    }

    /**
     * Returns the contatore socio with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the contatore socio
     * @return the contatore socio
     * @throws it.bysoftware.ct.NoSuchContatoreSocioException if a contatore socio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ContatoreSocio findByPrimaryKey(Serializable primaryKey)
        throws NoSuchContatoreSocioException, SystemException {
        ContatoreSocio contatoreSocio = fetchByPrimaryKey(primaryKey);

        if (contatoreSocio == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchContatoreSocioException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return contatoreSocio;
    }

    /**
     * Returns the contatore socio with the primary key or throws a {@link it.bysoftware.ct.NoSuchContatoreSocioException} if it could not be found.
     *
     * @param contatoreSocioPK the primary key of the contatore socio
     * @return the contatore socio
     * @throws it.bysoftware.ct.NoSuchContatoreSocioException if a contatore socio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ContatoreSocio findByPrimaryKey(ContatoreSocioPK contatoreSocioPK)
        throws NoSuchContatoreSocioException, SystemException {
        return findByPrimaryKey((Serializable) contatoreSocioPK);
    }

    /**
     * Returns the contatore socio with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the contatore socio
     * @return the contatore socio, or <code>null</code> if a contatore socio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ContatoreSocio fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        ContatoreSocio contatoreSocio = (ContatoreSocio) EntityCacheUtil.getResult(ContatoreSocioModelImpl.ENTITY_CACHE_ENABLED,
                ContatoreSocioImpl.class, primaryKey);

        if (contatoreSocio == _nullContatoreSocio) {
            return null;
        }

        if (contatoreSocio == null) {
            Session session = null;

            try {
                session = openSession();

                contatoreSocio = (ContatoreSocio) session.get(ContatoreSocioImpl.class,
                        primaryKey);

                if (contatoreSocio != null) {
                    cacheResult(contatoreSocio);
                } else {
                    EntityCacheUtil.putResult(ContatoreSocioModelImpl.ENTITY_CACHE_ENABLED,
                        ContatoreSocioImpl.class, primaryKey,
                        _nullContatoreSocio);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(ContatoreSocioModelImpl.ENTITY_CACHE_ENABLED,
                    ContatoreSocioImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return contatoreSocio;
    }

    /**
     * Returns the contatore socio with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param contatoreSocioPK the primary key of the contatore socio
     * @return the contatore socio, or <code>null</code> if a contatore socio with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ContatoreSocio fetchByPrimaryKey(ContatoreSocioPK contatoreSocioPK)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) contatoreSocioPK);
    }

    /**
     * Returns all the contatore socios.
     *
     * @return the contatore socios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ContatoreSocio> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the contatore socios.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ContatoreSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of contatore socios
     * @param end the upper bound of the range of contatore socios (not inclusive)
     * @return the range of contatore socios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ContatoreSocio> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the contatore socios.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ContatoreSocioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of contatore socios
     * @param end the upper bound of the range of contatore socios (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of contatore socios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ContatoreSocio> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<ContatoreSocio> list = (List<ContatoreSocio>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_CONTATORESOCIO);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_CONTATORESOCIO;

                if (pagination) {
                    sql = sql.concat(ContatoreSocioModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<ContatoreSocio>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<ContatoreSocio>(list);
                } else {
                    list = (List<ContatoreSocio>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the contatore socios from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (ContatoreSocio contatoreSocio : findAll()) {
            remove(contatoreSocio);
        }
    }

    /**
     * Returns the number of contatore socios.
     *
     * @return the number of contatore socios
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_CONTATORESOCIO);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the contatore socio persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.ContatoreSocio")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<ContatoreSocio>> listenersList = new ArrayList<ModelListener<ContatoreSocio>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<ContatoreSocio>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(ContatoreSocioImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
