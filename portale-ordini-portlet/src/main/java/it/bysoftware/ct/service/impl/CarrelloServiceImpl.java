package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.CarrelloServiceBaseImpl;

/**
 * The implementation of the carrello remote service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.CarrelloService} interface.
 *
 * <p>This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.CarrelloServiceBaseImpl
 * @see it.bysoftware.ct.service.CarrelloServiceUtil
 */
public class CarrelloServiceImpl extends CarrelloServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.CarrelloServiceUtil} to access the carrello
     * remote service.
     */
}
