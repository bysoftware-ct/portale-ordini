package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.CategorieMerceologicheLocalServiceBaseImpl;

/**
 * The implementation of the categorie merceologiche local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.CategorieMerceologicheLocalService}
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.CategorieMerceologicheLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.CategorieMerceologicheLocalServiceUtil
 */
public class CategorieMerceologicheLocalServiceImpl extends
        CategorieMerceologicheLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.CategorieMerceologicheLocalServiceUtil} to
     * access the categorie merceologiche local service.
     */
}
