package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchTipoCarrelloException;
import it.bysoftware.ct.model.TipoCarrello;
import it.bysoftware.ct.model.impl.TipoCarrelloImpl;
import it.bysoftware.ct.model.impl.TipoCarrelloModelImpl;
import it.bysoftware.ct.service.persistence.TipoCarrelloPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the tipo carrello service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see TipoCarrelloPersistence
 * @see TipoCarrelloUtil
 * @generated
 */
public class TipoCarrelloPersistenceImpl extends BasePersistenceImpl<TipoCarrello>
    implements TipoCarrelloPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link TipoCarrelloUtil} to access the tipo carrello persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = TipoCarrelloImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TipoCarrelloModelImpl.ENTITY_CACHE_ENABLED,
            TipoCarrelloModelImpl.FINDER_CACHE_ENABLED, TipoCarrelloImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TipoCarrelloModelImpl.ENTITY_CACHE_ENABLED,
            TipoCarrelloModelImpl.FINDER_CACHE_ENABLED, TipoCarrelloImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TipoCarrelloModelImpl.ENTITY_CACHE_ENABLED,
            TipoCarrelloModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_TIPOCARRELLO = "SELECT tipoCarrello FROM TipoCarrello tipoCarrello";
    private static final String _SQL_COUNT_TIPOCARRELLO = "SELECT COUNT(tipoCarrello) FROM TipoCarrello tipoCarrello";
    private static final String _ORDER_BY_ENTITY_ALIAS = "tipoCarrello.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No TipoCarrello exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(TipoCarrelloPersistenceImpl.class);
    private static TipoCarrello _nullTipoCarrello = new TipoCarrelloImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<TipoCarrello> toCacheModel() {
                return _nullTipoCarrelloCacheModel;
            }
        };

    private static CacheModel<TipoCarrello> _nullTipoCarrelloCacheModel = new CacheModel<TipoCarrello>() {
            @Override
            public TipoCarrello toEntityModel() {
                return _nullTipoCarrello;
            }
        };

    public TipoCarrelloPersistenceImpl() {
        setModelClass(TipoCarrello.class);
    }

    /**
     * Caches the tipo carrello in the entity cache if it is enabled.
     *
     * @param tipoCarrello the tipo carrello
     */
    @Override
    public void cacheResult(TipoCarrello tipoCarrello) {
        EntityCacheUtil.putResult(TipoCarrelloModelImpl.ENTITY_CACHE_ENABLED,
            TipoCarrelloImpl.class, tipoCarrello.getPrimaryKey(), tipoCarrello);

        tipoCarrello.resetOriginalValues();
    }

    /**
     * Caches the tipo carrellos in the entity cache if it is enabled.
     *
     * @param tipoCarrellos the tipo carrellos
     */
    @Override
    public void cacheResult(List<TipoCarrello> tipoCarrellos) {
        for (TipoCarrello tipoCarrello : tipoCarrellos) {
            if (EntityCacheUtil.getResult(
                        TipoCarrelloModelImpl.ENTITY_CACHE_ENABLED,
                        TipoCarrelloImpl.class, tipoCarrello.getPrimaryKey()) == null) {
                cacheResult(tipoCarrello);
            } else {
                tipoCarrello.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all tipo carrellos.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(TipoCarrelloImpl.class.getName());
        }

        EntityCacheUtil.clearCache(TipoCarrelloImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the tipo carrello.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(TipoCarrello tipoCarrello) {
        EntityCacheUtil.removeResult(TipoCarrelloModelImpl.ENTITY_CACHE_ENABLED,
            TipoCarrelloImpl.class, tipoCarrello.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<TipoCarrello> tipoCarrellos) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (TipoCarrello tipoCarrello : tipoCarrellos) {
            EntityCacheUtil.removeResult(TipoCarrelloModelImpl.ENTITY_CACHE_ENABLED,
                TipoCarrelloImpl.class, tipoCarrello.getPrimaryKey());
        }
    }

    /**
     * Creates a new tipo carrello with the primary key. Does not add the tipo carrello to the database.
     *
     * @param id the primary key for the new tipo carrello
     * @return the new tipo carrello
     */
    @Override
    public TipoCarrello create(long id) {
        TipoCarrello tipoCarrello = new TipoCarrelloImpl();

        tipoCarrello.setNew(true);
        tipoCarrello.setPrimaryKey(id);

        return tipoCarrello;
    }

    /**
     * Removes the tipo carrello with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the tipo carrello
     * @return the tipo carrello that was removed
     * @throws it.bysoftware.ct.NoSuchTipoCarrelloException if a tipo carrello with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TipoCarrello remove(long id)
        throws NoSuchTipoCarrelloException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the tipo carrello with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the tipo carrello
     * @return the tipo carrello that was removed
     * @throws it.bysoftware.ct.NoSuchTipoCarrelloException if a tipo carrello with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TipoCarrello remove(Serializable primaryKey)
        throws NoSuchTipoCarrelloException, SystemException {
        Session session = null;

        try {
            session = openSession();

            TipoCarrello tipoCarrello = (TipoCarrello) session.get(TipoCarrelloImpl.class,
                    primaryKey);

            if (tipoCarrello == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchTipoCarrelloException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(tipoCarrello);
        } catch (NoSuchTipoCarrelloException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected TipoCarrello removeImpl(TipoCarrello tipoCarrello)
        throws SystemException {
        tipoCarrello = toUnwrappedModel(tipoCarrello);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(tipoCarrello)) {
                tipoCarrello = (TipoCarrello) session.get(TipoCarrelloImpl.class,
                        tipoCarrello.getPrimaryKeyObj());
            }

            if (tipoCarrello != null) {
                session.delete(tipoCarrello);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (tipoCarrello != null) {
            clearCache(tipoCarrello);
        }

        return tipoCarrello;
    }

    @Override
    public TipoCarrello updateImpl(
        it.bysoftware.ct.model.TipoCarrello tipoCarrello)
        throws SystemException {
        tipoCarrello = toUnwrappedModel(tipoCarrello);

        boolean isNew = tipoCarrello.isNew();

        Session session = null;

        try {
            session = openSession();

            if (tipoCarrello.isNew()) {
                session.save(tipoCarrello);

                tipoCarrello.setNew(false);
            } else {
                session.merge(tipoCarrello);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(TipoCarrelloModelImpl.ENTITY_CACHE_ENABLED,
            TipoCarrelloImpl.class, tipoCarrello.getPrimaryKey(), tipoCarrello);

        return tipoCarrello;
    }

    protected TipoCarrello toUnwrappedModel(TipoCarrello tipoCarrello) {
        if (tipoCarrello instanceof TipoCarrelloImpl) {
            return tipoCarrello;
        }

        TipoCarrelloImpl tipoCarrelloImpl = new TipoCarrelloImpl();

        tipoCarrelloImpl.setNew(tipoCarrello.isNew());
        tipoCarrelloImpl.setPrimaryKey(tipoCarrello.getPrimaryKey());

        tipoCarrelloImpl.setId(tipoCarrello.getId());
        tipoCarrelloImpl.setTipologia(tipoCarrello.getTipologia());

        return tipoCarrelloImpl;
    }

    /**
     * Returns the tipo carrello with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the tipo carrello
     * @return the tipo carrello
     * @throws it.bysoftware.ct.NoSuchTipoCarrelloException if a tipo carrello with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TipoCarrello findByPrimaryKey(Serializable primaryKey)
        throws NoSuchTipoCarrelloException, SystemException {
        TipoCarrello tipoCarrello = fetchByPrimaryKey(primaryKey);

        if (tipoCarrello == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchTipoCarrelloException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return tipoCarrello;
    }

    /**
     * Returns the tipo carrello with the primary key or throws a {@link it.bysoftware.ct.NoSuchTipoCarrelloException} if it could not be found.
     *
     * @param id the primary key of the tipo carrello
     * @return the tipo carrello
     * @throws it.bysoftware.ct.NoSuchTipoCarrelloException if a tipo carrello with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TipoCarrello findByPrimaryKey(long id)
        throws NoSuchTipoCarrelloException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the tipo carrello with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the tipo carrello
     * @return the tipo carrello, or <code>null</code> if a tipo carrello with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TipoCarrello fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        TipoCarrello tipoCarrello = (TipoCarrello) EntityCacheUtil.getResult(TipoCarrelloModelImpl.ENTITY_CACHE_ENABLED,
                TipoCarrelloImpl.class, primaryKey);

        if (tipoCarrello == _nullTipoCarrello) {
            return null;
        }

        if (tipoCarrello == null) {
            Session session = null;

            try {
                session = openSession();

                tipoCarrello = (TipoCarrello) session.get(TipoCarrelloImpl.class,
                        primaryKey);

                if (tipoCarrello != null) {
                    cacheResult(tipoCarrello);
                } else {
                    EntityCacheUtil.putResult(TipoCarrelloModelImpl.ENTITY_CACHE_ENABLED,
                        TipoCarrelloImpl.class, primaryKey, _nullTipoCarrello);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(TipoCarrelloModelImpl.ENTITY_CACHE_ENABLED,
                    TipoCarrelloImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return tipoCarrello;
    }

    /**
     * Returns the tipo carrello with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the tipo carrello
     * @return the tipo carrello, or <code>null</code> if a tipo carrello with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TipoCarrello fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the tipo carrellos.
     *
     * @return the tipo carrellos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TipoCarrello> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the tipo carrellos.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TipoCarrelloModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of tipo carrellos
     * @param end the upper bound of the range of tipo carrellos (not inclusive)
     * @return the range of tipo carrellos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TipoCarrello> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the tipo carrellos.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TipoCarrelloModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of tipo carrellos
     * @param end the upper bound of the range of tipo carrellos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of tipo carrellos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TipoCarrello> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<TipoCarrello> list = (List<TipoCarrello>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_TIPOCARRELLO);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_TIPOCARRELLO;

                if (pagination) {
                    sql = sql.concat(TipoCarrelloModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<TipoCarrello>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<TipoCarrello>(list);
                } else {
                    list = (List<TipoCarrello>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the tipo carrellos from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (TipoCarrello tipoCarrello : findAll()) {
            remove(tipoCarrello);
        }
    }

    /**
     * Returns the number of tipo carrellos.
     *
     * @return the number of tipo carrellos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_TIPOCARRELLO);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the tipo carrello persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.TipoCarrello")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<TipoCarrello>> listenersList = new ArrayList<ModelListener<TipoCarrello>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<TipoCarrello>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(TipoCarrelloImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
