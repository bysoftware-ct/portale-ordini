package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.model.Riga;
import it.bysoftware.ct.service.base.RigaLocalServiceBaseImpl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * The implementation of the riga local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.RigaLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.RigaLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.RigaLocalServiceUtil
 */
public class RigaLocalServiceImpl extends RigaLocalServiceBaseImpl {
	/**
	 * Logger object. Based on Liferay logger
	 * {@link com.liferay.portal.kernel.log.Log} commons logging.
	 */
	private Log logger = LogFactoryUtil.getLog(RigaLocalServiceImpl.class);

	/**
	 * Query to retrieve the rows of a specified order.
	 */
	private static String QUERY1 = "SELECT r.id, r.ricevuto, r.piante_ripiano"
			+ " FROM _ordini AS o INNER JOIN _carrelli AS c ON"
			+ " o.id = c.id_ordine INNER JOIN _righe AS r ON"
			+ " c.id = r.id_carrello INNER JOIN _piante AS p ON"
			+ " r.id_articolo = p.id WHERE o.id = ? AND p.id_fornitore = ?";

	@Override
	public final List<Riga> findRowsInCart(final long cartId) {
		List<Riga> result = null;
		try {
			result = this.rigaPersistence.findByIdCarello(cartId);
		} catch (SystemException e) {
			this.logger.error(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
			result = new ArrayList<Riga>();
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public final List<Object[]> getReceivedPerPartnerAndOrder(long orderId,
			final String partnerCode) {
		Session session = this.rigaPersistence.openSession();
		SQLQuery query = session.createSQLQuery(RigaLocalServiceImpl.QUERY1);
		QueryPos pos = QueryPos.getInstance(query);
		pos.add(orderId);
		pos.add(partnerCode);
		return query.list();
	}
}
