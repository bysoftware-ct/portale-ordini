package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchPagamentoException;
import it.bysoftware.ct.model.Pagamento;
import it.bysoftware.ct.model.impl.PagamentoImpl;
import it.bysoftware.ct.model.impl.PagamentoModelImpl;
import it.bysoftware.ct.service.persistence.PagamentoPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the pagamento service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see PagamentoPersistence
 * @see PagamentoUtil
 * @generated
 */
public class PagamentoPersistenceImpl extends BasePersistenceImpl<Pagamento>
    implements PagamentoPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link PagamentoUtil} to access the pagamento persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = PagamentoImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PagamentoModelImpl.ENTITY_CACHE_ENABLED,
            PagamentoModelImpl.FINDER_CACHE_ENABLED, PagamentoImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PagamentoModelImpl.ENTITY_CACHE_ENABLED,
            PagamentoModelImpl.FINDER_CACHE_ENABLED, PagamentoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PagamentoModelImpl.ENTITY_CACHE_ENABLED,
            PagamentoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ANNO = new FinderPath(PagamentoModelImpl.ENTITY_CACHE_ENABLED,
            PagamentoModelImpl.FINDER_CACHE_ENABLED, PagamentoImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByAnno",
            new String[] {
                Integer.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNO = new FinderPath(PagamentoModelImpl.ENTITY_CACHE_ENABLED,
            PagamentoModelImpl.FINDER_CACHE_ENABLED, PagamentoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByAnno",
            new String[] { Integer.class.getName() },
            PagamentoModelImpl.ANNO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_ANNO = new FinderPath(PagamentoModelImpl.ENTITY_CACHE_ENABLED,
            PagamentoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByAnno",
            new String[] { Integer.class.getName() });
    private static final String _FINDER_COLUMN_ANNO_ANNO_2 = "pagamento.id.anno = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ANNOSTATO =
        new FinderPath(PagamentoModelImpl.ENTITY_CACHE_ENABLED,
            PagamentoModelImpl.FINDER_CACHE_ENABLED, PagamentoImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByAnnoStato",
            new String[] {
                Integer.class.getName(), Integer.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOSTATO =
        new FinderPath(PagamentoModelImpl.ENTITY_CACHE_ENABLED,
            PagamentoModelImpl.FINDER_CACHE_ENABLED, PagamentoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByAnnoStato",
            new String[] { Integer.class.getName(), Integer.class.getName() },
            PagamentoModelImpl.ANNO_COLUMN_BITMASK |
            PagamentoModelImpl.STATO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_ANNOSTATO = new FinderPath(PagamentoModelImpl.ENTITY_CACHE_ENABLED,
            PagamentoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByAnnoStato",
            new String[] { Integer.class.getName(), Integer.class.getName() });
    private static final String _FINDER_COLUMN_ANNOSTATO_ANNO_2 = "pagamento.id.anno = ? AND ";
    private static final String _FINDER_COLUMN_ANNOSTATO_STATO_2 = "pagamento.stato = ?";
    private static final String _SQL_SELECT_PAGAMENTO = "SELECT pagamento FROM Pagamento pagamento";
    private static final String _SQL_SELECT_PAGAMENTO_WHERE = "SELECT pagamento FROM Pagamento pagamento WHERE ";
    private static final String _SQL_COUNT_PAGAMENTO = "SELECT COUNT(pagamento) FROM Pagamento pagamento";
    private static final String _SQL_COUNT_PAGAMENTO_WHERE = "SELECT COUNT(pagamento) FROM Pagamento pagamento WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "pagamento.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Pagamento exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Pagamento exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(PagamentoPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "codiceSoggetto", "dataCreazione", "dataPagamento"
            });
    private static Pagamento _nullPagamento = new PagamentoImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Pagamento> toCacheModel() {
                return _nullPagamentoCacheModel;
            }
        };

    private static CacheModel<Pagamento> _nullPagamentoCacheModel = new CacheModel<Pagamento>() {
            @Override
            public Pagamento toEntityModel() {
                return _nullPagamento;
            }
        };

    public PagamentoPersistenceImpl() {
        setModelClass(Pagamento.class);
    }

    /**
     * Returns all the pagamentos where anno = &#63;.
     *
     * @param anno the anno
     * @return the matching pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Pagamento> findByAnno(int anno) throws SystemException {
        return findByAnno(anno, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the pagamentos where anno = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param anno the anno
     * @param start the lower bound of the range of pagamentos
     * @param end the upper bound of the range of pagamentos (not inclusive)
     * @return the range of matching pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Pagamento> findByAnno(int anno, int start, int end)
        throws SystemException {
        return findByAnno(anno, start, end, null);
    }

    /**
     * Returns an ordered range of all the pagamentos where anno = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param anno the anno
     * @param start the lower bound of the range of pagamentos
     * @param end the upper bound of the range of pagamentos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Pagamento> findByAnno(int anno, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNO;
            finderArgs = new Object[] { anno };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ANNO;
            finderArgs = new Object[] { anno, start, end, orderByComparator };
        }

        List<Pagamento> list = (List<Pagamento>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Pagamento pagamento : list) {
                if ((anno != pagamento.getAnno())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_PAGAMENTO_WHERE);

            query.append(_FINDER_COLUMN_ANNO_ANNO_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PagamentoModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(anno);

                if (!pagination) {
                    list = (List<Pagamento>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Pagamento>(list);
                } else {
                    list = (List<Pagamento>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first pagamento in the ordered set where anno = &#63;.
     *
     * @param anno the anno
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching pagamento
     * @throws it.bysoftware.ct.NoSuchPagamentoException if a matching pagamento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pagamento findByAnno_First(int anno,
        OrderByComparator orderByComparator)
        throws NoSuchPagamentoException, SystemException {
        Pagamento pagamento = fetchByAnno_First(anno, orderByComparator);

        if (pagamento != null) {
            return pagamento;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("anno=");
        msg.append(anno);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPagamentoException(msg.toString());
    }

    /**
     * Returns the first pagamento in the ordered set where anno = &#63;.
     *
     * @param anno the anno
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching pagamento, or <code>null</code> if a matching pagamento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pagamento fetchByAnno_First(int anno,
        OrderByComparator orderByComparator) throws SystemException {
        List<Pagamento> list = findByAnno(anno, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last pagamento in the ordered set where anno = &#63;.
     *
     * @param anno the anno
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching pagamento
     * @throws it.bysoftware.ct.NoSuchPagamentoException if a matching pagamento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pagamento findByAnno_Last(int anno,
        OrderByComparator orderByComparator)
        throws NoSuchPagamentoException, SystemException {
        Pagamento pagamento = fetchByAnno_Last(anno, orderByComparator);

        if (pagamento != null) {
            return pagamento;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("anno=");
        msg.append(anno);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPagamentoException(msg.toString());
    }

    /**
     * Returns the last pagamento in the ordered set where anno = &#63;.
     *
     * @param anno the anno
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching pagamento, or <code>null</code> if a matching pagamento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pagamento fetchByAnno_Last(int anno,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByAnno(anno);

        if (count == 0) {
            return null;
        }

        List<Pagamento> list = findByAnno(anno, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the pagamentos before and after the current pagamento in the ordered set where anno = &#63;.
     *
     * @param pagamentoPK the primary key of the current pagamento
     * @param anno the anno
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next pagamento
     * @throws it.bysoftware.ct.NoSuchPagamentoException if a pagamento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pagamento[] findByAnno_PrevAndNext(PagamentoPK pagamentoPK,
        int anno, OrderByComparator orderByComparator)
        throws NoSuchPagamentoException, SystemException {
        Pagamento pagamento = findByPrimaryKey(pagamentoPK);

        Session session = null;

        try {
            session = openSession();

            Pagamento[] array = new PagamentoImpl[3];

            array[0] = getByAnno_PrevAndNext(session, pagamento, anno,
                    orderByComparator, true);

            array[1] = pagamento;

            array[2] = getByAnno_PrevAndNext(session, pagamento, anno,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Pagamento getByAnno_PrevAndNext(Session session,
        Pagamento pagamento, int anno, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PAGAMENTO_WHERE);

        query.append(_FINDER_COLUMN_ANNO_ANNO_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(PagamentoModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(anno);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(pagamento);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Pagamento> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the pagamentos where anno = &#63; from the database.
     *
     * @param anno the anno
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByAnno(int anno) throws SystemException {
        for (Pagamento pagamento : findByAnno(anno, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(pagamento);
        }
    }

    /**
     * Returns the number of pagamentos where anno = &#63;.
     *
     * @param anno the anno
     * @return the number of matching pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByAnno(int anno) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_ANNO;

        Object[] finderArgs = new Object[] { anno };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_PAGAMENTO_WHERE);

            query.append(_FINDER_COLUMN_ANNO_ANNO_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(anno);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the pagamentos where anno = &#63; and stato = &#63;.
     *
     * @param anno the anno
     * @param stato the stato
     * @return the matching pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Pagamento> findByAnnoStato(int anno, int stato)
        throws SystemException {
        return findByAnnoStato(anno, stato, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the pagamentos where anno = &#63; and stato = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param anno the anno
     * @param stato the stato
     * @param start the lower bound of the range of pagamentos
     * @param end the upper bound of the range of pagamentos (not inclusive)
     * @return the range of matching pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Pagamento> findByAnnoStato(int anno, int stato, int start,
        int end) throws SystemException {
        return findByAnnoStato(anno, stato, start, end, null);
    }

    /**
     * Returns an ordered range of all the pagamentos where anno = &#63; and stato = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param anno the anno
     * @param stato the stato
     * @param start the lower bound of the range of pagamentos
     * @param end the upper bound of the range of pagamentos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Pagamento> findByAnnoStato(int anno, int stato, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOSTATO;
            finderArgs = new Object[] { anno, stato };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ANNOSTATO;
            finderArgs = new Object[] { anno, stato, start, end, orderByComparator };
        }

        List<Pagamento> list = (List<Pagamento>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Pagamento pagamento : list) {
                if ((anno != pagamento.getAnno()) ||
                        (stato != pagamento.getStato())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_PAGAMENTO_WHERE);

            query.append(_FINDER_COLUMN_ANNOSTATO_ANNO_2);

            query.append(_FINDER_COLUMN_ANNOSTATO_STATO_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PagamentoModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(anno);

                qPos.add(stato);

                if (!pagination) {
                    list = (List<Pagamento>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Pagamento>(list);
                } else {
                    list = (List<Pagamento>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first pagamento in the ordered set where anno = &#63; and stato = &#63;.
     *
     * @param anno the anno
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching pagamento
     * @throws it.bysoftware.ct.NoSuchPagamentoException if a matching pagamento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pagamento findByAnnoStato_First(int anno, int stato,
        OrderByComparator orderByComparator)
        throws NoSuchPagamentoException, SystemException {
        Pagamento pagamento = fetchByAnnoStato_First(anno, stato,
                orderByComparator);

        if (pagamento != null) {
            return pagamento;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("anno=");
        msg.append(anno);

        msg.append(", stato=");
        msg.append(stato);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPagamentoException(msg.toString());
    }

    /**
     * Returns the first pagamento in the ordered set where anno = &#63; and stato = &#63;.
     *
     * @param anno the anno
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching pagamento, or <code>null</code> if a matching pagamento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pagamento fetchByAnnoStato_First(int anno, int stato,
        OrderByComparator orderByComparator) throws SystemException {
        List<Pagamento> list = findByAnnoStato(anno, stato, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last pagamento in the ordered set where anno = &#63; and stato = &#63;.
     *
     * @param anno the anno
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching pagamento
     * @throws it.bysoftware.ct.NoSuchPagamentoException if a matching pagamento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pagamento findByAnnoStato_Last(int anno, int stato,
        OrderByComparator orderByComparator)
        throws NoSuchPagamentoException, SystemException {
        Pagamento pagamento = fetchByAnnoStato_Last(anno, stato,
                orderByComparator);

        if (pagamento != null) {
            return pagamento;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("anno=");
        msg.append(anno);

        msg.append(", stato=");
        msg.append(stato);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPagamentoException(msg.toString());
    }

    /**
     * Returns the last pagamento in the ordered set where anno = &#63; and stato = &#63;.
     *
     * @param anno the anno
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching pagamento, or <code>null</code> if a matching pagamento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pagamento fetchByAnnoStato_Last(int anno, int stato,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByAnnoStato(anno, stato);

        if (count == 0) {
            return null;
        }

        List<Pagamento> list = findByAnnoStato(anno, stato, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the pagamentos before and after the current pagamento in the ordered set where anno = &#63; and stato = &#63;.
     *
     * @param pagamentoPK the primary key of the current pagamento
     * @param anno the anno
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next pagamento
     * @throws it.bysoftware.ct.NoSuchPagamentoException if a pagamento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pagamento[] findByAnnoStato_PrevAndNext(PagamentoPK pagamentoPK,
        int anno, int stato, OrderByComparator orderByComparator)
        throws NoSuchPagamentoException, SystemException {
        Pagamento pagamento = findByPrimaryKey(pagamentoPK);

        Session session = null;

        try {
            session = openSession();

            Pagamento[] array = new PagamentoImpl[3];

            array[0] = getByAnnoStato_PrevAndNext(session, pagamento, anno,
                    stato, orderByComparator, true);

            array[1] = pagamento;

            array[2] = getByAnnoStato_PrevAndNext(session, pagamento, anno,
                    stato, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Pagamento getByAnnoStato_PrevAndNext(Session session,
        Pagamento pagamento, int anno, int stato,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PAGAMENTO_WHERE);

        query.append(_FINDER_COLUMN_ANNOSTATO_ANNO_2);

        query.append(_FINDER_COLUMN_ANNOSTATO_STATO_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(PagamentoModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(anno);

        qPos.add(stato);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(pagamento);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Pagamento> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the pagamentos where anno = &#63; and stato = &#63; from the database.
     *
     * @param anno the anno
     * @param stato the stato
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByAnnoStato(int anno, int stato)
        throws SystemException {
        for (Pagamento pagamento : findByAnnoStato(anno, stato,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(pagamento);
        }
    }

    /**
     * Returns the number of pagamentos where anno = &#63; and stato = &#63;.
     *
     * @param anno the anno
     * @param stato the stato
     * @return the number of matching pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByAnnoStato(int anno, int stato) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_ANNOSTATO;

        Object[] finderArgs = new Object[] { anno, stato };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_PAGAMENTO_WHERE);

            query.append(_FINDER_COLUMN_ANNOSTATO_ANNO_2);

            query.append(_FINDER_COLUMN_ANNOSTATO_STATO_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(anno);

                qPos.add(stato);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the pagamento in the entity cache if it is enabled.
     *
     * @param pagamento the pagamento
     */
    @Override
    public void cacheResult(Pagamento pagamento) {
        EntityCacheUtil.putResult(PagamentoModelImpl.ENTITY_CACHE_ENABLED,
            PagamentoImpl.class, pagamento.getPrimaryKey(), pagamento);

        pagamento.resetOriginalValues();
    }

    /**
     * Caches the pagamentos in the entity cache if it is enabled.
     *
     * @param pagamentos the pagamentos
     */
    @Override
    public void cacheResult(List<Pagamento> pagamentos) {
        for (Pagamento pagamento : pagamentos) {
            if (EntityCacheUtil.getResult(
                        PagamentoModelImpl.ENTITY_CACHE_ENABLED,
                        PagamentoImpl.class, pagamento.getPrimaryKey()) == null) {
                cacheResult(pagamento);
            } else {
                pagamento.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all pagamentos.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(PagamentoImpl.class.getName());
        }

        EntityCacheUtil.clearCache(PagamentoImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the pagamento.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Pagamento pagamento) {
        EntityCacheUtil.removeResult(PagamentoModelImpl.ENTITY_CACHE_ENABLED,
            PagamentoImpl.class, pagamento.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Pagamento> pagamentos) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Pagamento pagamento : pagamentos) {
            EntityCacheUtil.removeResult(PagamentoModelImpl.ENTITY_CACHE_ENABLED,
                PagamentoImpl.class, pagamento.getPrimaryKey());
        }
    }

    /**
     * Creates a new pagamento with the primary key. Does not add the pagamento to the database.
     *
     * @param pagamentoPK the primary key for the new pagamento
     * @return the new pagamento
     */
    @Override
    public Pagamento create(PagamentoPK pagamentoPK) {
        Pagamento pagamento = new PagamentoImpl();

        pagamento.setNew(true);
        pagamento.setPrimaryKey(pagamentoPK);

        return pagamento;
    }

    /**
     * Removes the pagamento with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param pagamentoPK the primary key of the pagamento
     * @return the pagamento that was removed
     * @throws it.bysoftware.ct.NoSuchPagamentoException if a pagamento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pagamento remove(PagamentoPK pagamentoPK)
        throws NoSuchPagamentoException, SystemException {
        return remove((Serializable) pagamentoPK);
    }

    /**
     * Removes the pagamento with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the pagamento
     * @return the pagamento that was removed
     * @throws it.bysoftware.ct.NoSuchPagamentoException if a pagamento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pagamento remove(Serializable primaryKey)
        throws NoSuchPagamentoException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Pagamento pagamento = (Pagamento) session.get(PagamentoImpl.class,
                    primaryKey);

            if (pagamento == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchPagamentoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(pagamento);
        } catch (NoSuchPagamentoException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Pagamento removeImpl(Pagamento pagamento)
        throws SystemException {
        pagamento = toUnwrappedModel(pagamento);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(pagamento)) {
                pagamento = (Pagamento) session.get(PagamentoImpl.class,
                        pagamento.getPrimaryKeyObj());
            }

            if (pagamento != null) {
                session.delete(pagamento);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (pagamento != null) {
            clearCache(pagamento);
        }

        return pagamento;
    }

    @Override
    public Pagamento updateImpl(it.bysoftware.ct.model.Pagamento pagamento)
        throws SystemException {
        pagamento = toUnwrappedModel(pagamento);

        boolean isNew = pagamento.isNew();

        PagamentoModelImpl pagamentoModelImpl = (PagamentoModelImpl) pagamento;

        Session session = null;

        try {
            session = openSession();

            if (pagamento.isNew()) {
                session.save(pagamento);

                pagamento.setNew(false);
            } else {
                session.merge(pagamento);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !PagamentoModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((pagamentoModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        pagamentoModelImpl.getOriginalAnno()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ANNO, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNO,
                    args);

                args = new Object[] { pagamentoModelImpl.getAnno() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ANNO, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNO,
                    args);
            }

            if ((pagamentoModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOSTATO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        pagamentoModelImpl.getOriginalAnno(),
                        pagamentoModelImpl.getOriginalStato()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ANNOSTATO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOSTATO,
                    args);

                args = new Object[] {
                        pagamentoModelImpl.getAnno(),
                        pagamentoModelImpl.getStato()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ANNOSTATO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOSTATO,
                    args);
            }
        }

        EntityCacheUtil.putResult(PagamentoModelImpl.ENTITY_CACHE_ENABLED,
            PagamentoImpl.class, pagamento.getPrimaryKey(), pagamento);

        return pagamento;
    }

    protected Pagamento toUnwrappedModel(Pagamento pagamento) {
        if (pagamento instanceof PagamentoImpl) {
            return pagamento;
        }

        PagamentoImpl pagamentoImpl = new PagamentoImpl();

        pagamentoImpl.setNew(pagamento.isNew());
        pagamentoImpl.setPrimaryKey(pagamento.getPrimaryKey());

        pagamentoImpl.setId(pagamento.getId());
        pagamentoImpl.setAnno(pagamento.getAnno());
        pagamentoImpl.setCodiceSoggetto(pagamento.getCodiceSoggetto());
        pagamentoImpl.setDataCreazione(pagamento.getDataCreazione());
        pagamentoImpl.setDataPagamento(pagamento.getDataPagamento());
        pagamentoImpl.setCredito(pagamento.getCredito());
        pagamentoImpl.setImporto(pagamento.getImporto());
        pagamentoImpl.setSaldo(pagamento.getSaldo());
        pagamentoImpl.setStato(pagamento.getStato());
        pagamentoImpl.setNota(pagamento.getNota());

        return pagamentoImpl;
    }

    /**
     * Returns the pagamento with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the pagamento
     * @return the pagamento
     * @throws it.bysoftware.ct.NoSuchPagamentoException if a pagamento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pagamento findByPrimaryKey(Serializable primaryKey)
        throws NoSuchPagamentoException, SystemException {
        Pagamento pagamento = fetchByPrimaryKey(primaryKey);

        if (pagamento == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchPagamentoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return pagamento;
    }

    /**
     * Returns the pagamento with the primary key or throws a {@link it.bysoftware.ct.NoSuchPagamentoException} if it could not be found.
     *
     * @param pagamentoPK the primary key of the pagamento
     * @return the pagamento
     * @throws it.bysoftware.ct.NoSuchPagamentoException if a pagamento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pagamento findByPrimaryKey(PagamentoPK pagamentoPK)
        throws NoSuchPagamentoException, SystemException {
        return findByPrimaryKey((Serializable) pagamentoPK);
    }

    /**
     * Returns the pagamento with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the pagamento
     * @return the pagamento, or <code>null</code> if a pagamento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pagamento fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Pagamento pagamento = (Pagamento) EntityCacheUtil.getResult(PagamentoModelImpl.ENTITY_CACHE_ENABLED,
                PagamentoImpl.class, primaryKey);

        if (pagamento == _nullPagamento) {
            return null;
        }

        if (pagamento == null) {
            Session session = null;

            try {
                session = openSession();

                pagamento = (Pagamento) session.get(PagamentoImpl.class,
                        primaryKey);

                if (pagamento != null) {
                    cacheResult(pagamento);
                } else {
                    EntityCacheUtil.putResult(PagamentoModelImpl.ENTITY_CACHE_ENABLED,
                        PagamentoImpl.class, primaryKey, _nullPagamento);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(PagamentoModelImpl.ENTITY_CACHE_ENABLED,
                    PagamentoImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return pagamento;
    }

    /**
     * Returns the pagamento with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param pagamentoPK the primary key of the pagamento
     * @return the pagamento, or <code>null</code> if a pagamento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pagamento fetchByPrimaryKey(PagamentoPK pagamentoPK)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) pagamentoPK);
    }

    /**
     * Returns all the pagamentos.
     *
     * @return the pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Pagamento> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the pagamentos.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of pagamentos
     * @param end the upper bound of the range of pagamentos (not inclusive)
     * @return the range of pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Pagamento> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the pagamentos.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of pagamentos
     * @param end the upper bound of the range of pagamentos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Pagamento> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Pagamento> list = (List<Pagamento>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_PAGAMENTO);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_PAGAMENTO;

                if (pagination) {
                    sql = sql.concat(PagamentoModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Pagamento>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Pagamento>(list);
                } else {
                    list = (List<Pagamento>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the pagamentos from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Pagamento pagamento : findAll()) {
            remove(pagamento);
        }
    }

    /**
     * Returns the number of pagamentos.
     *
     * @return the number of pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_PAGAMENTO);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the pagamento persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.Pagamento")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Pagamento>> listenersList = new ArrayList<ModelListener<Pagamento>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Pagamento>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(PagamentoImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
