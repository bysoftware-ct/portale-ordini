package it.bysoftware.ct.service.base;

import it.bysoftware.ct.service.OrdineLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Mario Torrisi
 * @generated
 */
public class OrdineLocalServiceClpInvoker {
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName2;
    private String[] _methodParameterTypes2;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName238;
    private String[] _methodParameterTypes238;
    private String _methodName239;
    private String[] _methodParameterTypes239;
    private String _methodName244;
    private String[] _methodParameterTypes244;
    private String _methodName246;
    private String[] _methodParameterTypes246;
    private String _methodName247;
    private String[] _methodParameterTypes247;
    private String _methodName248;
    private String[] _methodParameterTypes248;
    private String _methodName249;
    private String[] _methodParameterTypes249;
    private String _methodName250;
    private String[] _methodParameterTypes250;
    private String _methodName251;
    private String[] _methodParameterTypes251;
    private String _methodName252;
    private String[] _methodParameterTypes252;
    private String _methodName253;
    private String[] _methodParameterTypes253;
    private String _methodName254;
    private String[] _methodParameterTypes254;
    private String _methodName255;
    private String[] _methodParameterTypes255;
    private String _methodName256;
    private String[] _methodParameterTypes256;
    private String _methodName257;
    private String[] _methodParameterTypes257;
    private String _methodName258;
    private String[] _methodParameterTypes258;
    private String _methodName259;
    private String[] _methodParameterTypes259;
    private String _methodName260;
    private String[] _methodParameterTypes260;
    private String _methodName261;
    private String[] _methodParameterTypes261;
    private String _methodName262;
    private String[] _methodParameterTypes262;
    private String _methodName263;
    private String[] _methodParameterTypes263;
    private String _methodName264;
    private String[] _methodParameterTypes264;
    private String _methodName265;
    private String[] _methodParameterTypes265;
    private String _methodName266;
    private String[] _methodParameterTypes266;
    private String _methodName267;
    private String[] _methodParameterTypes267;
    private String _methodName268;
    private String[] _methodParameterTypes268;

    public OrdineLocalServiceClpInvoker() {
        _methodName0 = "addOrdine";

        _methodParameterTypes0 = new String[] { "it.bysoftware.ct.model.Ordine" };

        _methodName1 = "createOrdine";

        _methodParameterTypes1 = new String[] { "long" };

        _methodName2 = "deleteOrdine";

        _methodParameterTypes2 = new String[] { "long" };

        _methodName3 = "deleteOrdine";

        _methodParameterTypes3 = new String[] { "it.bysoftware.ct.model.Ordine" };

        _methodName4 = "dynamicQuery";

        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "dynamicQuery";

        _methodParameterTypes5 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName6 = "dynamicQuery";

        _methodParameterTypes6 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
            };

        _methodName7 = "dynamicQuery";

        _methodParameterTypes7 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName8 = "dynamicQueryCount";

        _methodParameterTypes8 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName9 = "dynamicQueryCount";

        _methodParameterTypes9 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery",
                "com.liferay.portal.kernel.dao.orm.Projection"
            };

        _methodName10 = "fetchOrdine";

        _methodParameterTypes10 = new String[] { "long" };

        _methodName11 = "getOrdine";

        _methodParameterTypes11 = new String[] { "long" };

        _methodName12 = "getPersistedModel";

        _methodParameterTypes12 = new String[] { "java.io.Serializable" };

        _methodName13 = "getOrdines";

        _methodParameterTypes13 = new String[] { "int", "int" };

        _methodName14 = "getOrdinesCount";

        _methodParameterTypes14 = new String[] {  };

        _methodName15 = "updateOrdine";

        _methodParameterTypes15 = new String[] { "it.bysoftware.ct.model.Ordine" };

        _methodName238 = "getBeanIdentifier";

        _methodParameterTypes238 = new String[] {  };

        _methodName239 = "setBeanIdentifier";

        _methodParameterTypes239 = new String[] { "java.lang.String" };

        _methodName244 = "getNumber";

        _methodParameterTypes244 = new String[] { "int", "java.lang.String" };

        _methodName246 = "getOrderByCodeStatePassport";

        _methodParameterTypes246 = new String[] {
                "java.lang.String", "int", "java.lang.String"
            };

        _methodName247 = "getOrderByCustomer";

        _methodParameterTypes247 = new String[] { "java.lang.String" };

        _methodName248 = "getOrderByState";

        _methodParameterTypes248 = new String[] { "int" };

        _methodName249 = "getOrderByStatePassport";

        _methodParameterTypes249 = new String[] { "int", "java.lang.String" };

        _methodName250 = "getOrderByStatePassportPartner";

        _methodParameterTypes250 = new String[] {
                "int", "java.lang.String", "java.lang.String"
            };

        _methodName251 = "getOrderBetweenDatePerPartner";

        _methodParameterTypes251 = new String[] {
                "java.lang.String", "java.util.Date", "java.util.Date"
            };

        _methodName252 = "getOrderBetweenDate";

        _methodParameterTypes252 = new String[] {
                "java.util.Date", "java.util.Date"
            };

        _methodName253 = "getOrderBetweenDatePerItem";

        _methodParameterTypes253 = new String[] {
                "java.lang.String", "java.util.Date", "java.util.Date"
            };

        _methodName254 = "getItemsInOrderBetweenDate";

        _methodParameterTypes254 = new String[] {
                "java.util.Date", "java.util.Date"
            };

        _methodName255 = "getOrderWithPassportByPartnerAndDate";

        _methodParameterTypes255 = new String[] {
                "java.lang.String", "java.util.Date", "java.util.Date"
            };

        _methodName256 = "getOrderWithPassportByDate";

        _methodParameterTypes256 = new String[] {
                "java.util.Date", "java.util.Date"
            };

        _methodName257 = "getCustomerConfirmedOrders";

        _methodParameterTypes257 = new String[] { "java.lang.String" };

        _methodName258 = "getCustomerPendingOrders";

        _methodParameterTypes258 = new String[] { "java.lang.String" };

        _methodName259 = "getSavedCustomerOrders";

        _methodParameterTypes259 = new String[] { "java.lang.String" };

        _methodName260 = "getScheduledCustomerOrders";

        _methodParameterTypes260 = new String[] { "java.lang.String" };

        _methodName261 = "getProcessedCustomerOrders";

        _methodParameterTypes261 = new String[] { "java.lang.String" };

        _methodName262 = "getUnScheduledOrderByCustomer";

        _methodParameterTypes262 = new String[] { "java.lang.String" };

        _methodName263 = "getTodayReadyOrders";

        _methodParameterTypes263 = new String[] {  };

        _methodName264 = "getTodayConfirmedOrders";

        _methodParameterTypes264 = new String[] {  };

        _methodName265 = "getTodayProcessedOrders";

        _methodParameterTypes265 = new String[] {  };

        _methodName266 = "findOrders";

        _methodParameterTypes266 = new String[] {
                "java.lang.String", "java.util.Date", "java.util.Date",
                "boolean", "boolean", "boolean", "boolean", "boolean", "boolean",
                "int", "int", "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName267 = "getFindOrdersCount";

        _methodParameterTypes267 = new String[] {
                "java.lang.String", "java.util.Date", "java.util.Date",
                "boolean", "boolean", "boolean", "boolean", "boolean", "boolean"
            };

        _methodName268 = "getByYearNumberCenter";

        _methodParameterTypes268 = new String[] { "int", "java.lang.String" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName0.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
            return OrdineLocalServiceUtil.addOrdine((it.bysoftware.ct.model.Ordine) arguments[0]);
        }

        if (_methodName1.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
            return OrdineLocalServiceUtil.createOrdine(((Long) arguments[0]).longValue());
        }

        if (_methodName2.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
            return OrdineLocalServiceUtil.deleteOrdine(((Long) arguments[0]).longValue());
        }

        if (_methodName3.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
            return OrdineLocalServiceUtil.deleteOrdine((it.bysoftware.ct.model.Ordine) arguments[0]);
        }

        if (_methodName4.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
            return OrdineLocalServiceUtil.dynamicQuery();
        }

        if (_methodName5.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
            return OrdineLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName6.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
            return OrdineLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName7.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
            return OrdineLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[3]);
        }

        if (_methodName8.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
            return OrdineLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName9.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
            return OrdineLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                (com.liferay.portal.kernel.dao.orm.Projection) arguments[1]);
        }

        if (_methodName10.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
            return OrdineLocalServiceUtil.fetchOrdine(((Long) arguments[0]).longValue());
        }

        if (_methodName11.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
            return OrdineLocalServiceUtil.getOrdine(((Long) arguments[0]).longValue());
        }

        if (_methodName12.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
            return OrdineLocalServiceUtil.getPersistedModel((java.io.Serializable) arguments[0]);
        }

        if (_methodName13.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
            return OrdineLocalServiceUtil.getOrdines(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName14.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
            return OrdineLocalServiceUtil.getOrdinesCount();
        }

        if (_methodName15.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
            return OrdineLocalServiceUtil.updateOrdine((it.bysoftware.ct.model.Ordine) arguments[0]);
        }

        if (_methodName238.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes238, parameterTypes)) {
            return OrdineLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName239.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes239, parameterTypes)) {
            OrdineLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName244.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes244, parameterTypes)) {
            return OrdineLocalServiceUtil.getNumber(((Integer) arguments[0]).intValue(),
                (java.lang.String) arguments[1]);
        }

        if (_methodName246.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes246, parameterTypes)) {
            return OrdineLocalServiceUtil.getOrderByCodeStatePassport((java.lang.String) arguments[0],
                ((Integer) arguments[1]).intValue(),
                (java.lang.String) arguments[2]);
        }

        if (_methodName247.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes247, parameterTypes)) {
            return OrdineLocalServiceUtil.getOrderByCustomer((java.lang.String) arguments[0]);
        }

        if (_methodName248.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes248, parameterTypes)) {
            return OrdineLocalServiceUtil.getOrderByState(((Integer) arguments[0]).intValue());
        }

        if (_methodName249.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes249, parameterTypes)) {
            return OrdineLocalServiceUtil.getOrderByStatePassport(((Integer) arguments[0]).intValue(),
                (java.lang.String) arguments[1]);
        }

        if (_methodName250.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes250, parameterTypes)) {
            return OrdineLocalServiceUtil.getOrderByStatePassportPartner(((Integer) arguments[0]).intValue(),
                (java.lang.String) arguments[1], (java.lang.String) arguments[2]);
        }

        if (_methodName251.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes251, parameterTypes)) {
            return OrdineLocalServiceUtil.getOrderBetweenDatePerPartner((java.lang.String) arguments[0],
                (java.util.Date) arguments[1], (java.util.Date) arguments[2]);
        }

        if (_methodName252.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes252, parameterTypes)) {
            return OrdineLocalServiceUtil.getOrderBetweenDate((java.util.Date) arguments[0],
                (java.util.Date) arguments[1]);
        }

        if (_methodName253.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes253, parameterTypes)) {
            return OrdineLocalServiceUtil.getOrderBetweenDatePerItem((java.lang.String) arguments[0],
                (java.util.Date) arguments[1], (java.util.Date) arguments[2]);
        }

        if (_methodName254.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes254, parameterTypes)) {
            return OrdineLocalServiceUtil.getItemsInOrderBetweenDate((java.util.Date) arguments[0],
                (java.util.Date) arguments[1]);
        }

        if (_methodName255.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes255, parameterTypes)) {
            return OrdineLocalServiceUtil.getOrderWithPassportByPartnerAndDate((java.lang.String) arguments[0],
                (java.util.Date) arguments[1], (java.util.Date) arguments[2]);
        }

        if (_methodName256.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes256, parameterTypes)) {
            return OrdineLocalServiceUtil.getOrderWithPassportByDate((java.util.Date) arguments[0],
                (java.util.Date) arguments[1]);
        }

        if (_methodName257.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes257, parameterTypes)) {
            return OrdineLocalServiceUtil.getCustomerConfirmedOrders((java.lang.String) arguments[0]);
        }

        if (_methodName258.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes258, parameterTypes)) {
            return OrdineLocalServiceUtil.getCustomerPendingOrders((java.lang.String) arguments[0]);
        }

        if (_methodName259.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes259, parameterTypes)) {
            return OrdineLocalServiceUtil.getSavedCustomerOrders((java.lang.String) arguments[0]);
        }

        if (_methodName260.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes260, parameterTypes)) {
            return OrdineLocalServiceUtil.getScheduledCustomerOrders((java.lang.String) arguments[0]);
        }

        if (_methodName261.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes261, parameterTypes)) {
            return OrdineLocalServiceUtil.getProcessedCustomerOrders((java.lang.String) arguments[0]);
        }

        if (_methodName262.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes262, parameterTypes)) {
            return OrdineLocalServiceUtil.getUnScheduledOrderByCustomer((java.lang.String) arguments[0]);
        }

        if (_methodName263.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes263, parameterTypes)) {
            return OrdineLocalServiceUtil.getTodayReadyOrders();
        }

        if (_methodName264.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes264, parameterTypes)) {
            return OrdineLocalServiceUtil.getTodayConfirmedOrders();
        }

        if (_methodName265.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes265, parameterTypes)) {
            return OrdineLocalServiceUtil.getTodayProcessedOrders();
        }

        if (_methodName266.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes266, parameterTypes)) {
            return OrdineLocalServiceUtil.findOrders((java.lang.String) arguments[0],
                (java.util.Date) arguments[1], (java.util.Date) arguments[2],
                ((Boolean) arguments[3]).booleanValue(),
                ((Boolean) arguments[4]).booleanValue(),
                ((Boolean) arguments[5]).booleanValue(),
                ((Boolean) arguments[6]).booleanValue(),
                ((Boolean) arguments[7]).booleanValue(),
                ((Boolean) arguments[8]).booleanValue(),
                ((Integer) arguments[9]).intValue(),
                ((Integer) arguments[10]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[11]);
        }

        if (_methodName267.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes267, parameterTypes)) {
            return OrdineLocalServiceUtil.getFindOrdersCount((java.lang.String) arguments[0],
                (java.util.Date) arguments[1], (java.util.Date) arguments[2],
                ((Boolean) arguments[3]).booleanValue(),
                ((Boolean) arguments[4]).booleanValue(),
                ((Boolean) arguments[5]).booleanValue(),
                ((Boolean) arguments[6]).booleanValue(),
                ((Boolean) arguments[7]).booleanValue(),
                ((Boolean) arguments[8]).booleanValue());
        }

        if (_methodName268.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes268, parameterTypes)) {
            return OrdineLocalServiceUtil.getByYearNumberCenter(((Integer) arguments[0]).intValue(),
                (java.lang.String) arguments[1]);
        }

        throw new UnsupportedOperationException();
    }
}
