package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.CodiceConsorzioServiceBaseImpl;

/**
 * The implementation of the codice consorzio remote service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.CodiceConsorzioService} interface.
 *
 * <p>This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.CodiceConsorzioServiceBaseImpl
 * @see it.bysoftware.ct.service.CodiceConsorzioServiceUtil
 */
public class CodiceConsorzioServiceImpl extends CodiceConsorzioServiceBaseImpl {

}
