package it.bysoftware.ct.service.impl;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Junction;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;

import it.bysoftware.ct.NoSuchSpettanzeSociException;
import it.bysoftware.ct.model.SpettanzeSoci;
import it.bysoftware.ct.service.SpettanzeSociLocalServiceUtil;
import it.bysoftware.ct.service.base.SpettanzeSociLocalServiceBaseImpl;
import it.bysoftware.ct.service.persistence.TestataFattureClientiPK;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The implementation of the spettanze soci local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.SpettanzeSociLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.SpettanzeSociLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.SpettanzeSociLocalServiceUtil
 */
public class SpettanzeSociLocalServiceImpl extends
        SpettanzeSociLocalServiceBaseImpl {

    /**
     * Query to retrieve the order of all partners having the specified state
     * and passport number. 
     */
    private static final String QUERY1 = "SELECT Rt1Anno, codice_fornitore,"
            + " stato, SUM(importo), id_pagamento FROM _spettanze_soci WHERE"
            + " stato = ? AND data_calcolo BETWEEN ? AND ? GROUP BY Rt1Anno,"
            + " codice_fornitore, stato, id_pagamento"; 
    
    /**
     * Query to retrieve the order of all partners having the specified state
     * and passport number. 
     */
    private static final String QUERY2 = "SELECT Rt1Anno, codice_fornitore,"
            + " stato, SUM(importo), id_pagamento, anno_pagamento FROM _spettanze_soci WHERE"
            + " data_calcolo BETWEEN ? AND ? GROUP BY Rt1Anno,"
            + " codice_fornitore, stato, id_pagamento, anno_pagamento";
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil
            .getLog(SpettanzeSociLocalServiceImpl.class);
    
    @Override
    public final List<SpettanzeSoci> findByPartner(
            final String subjectCode) {
        return this.findByPartnerState(subjectCode, null);
    }
    
    @Override
    public final List<SpettanzeSoci> findByPartnerState(
            final String subjectCode, final Integer state) {

        List<SpettanzeSoci> result = new ArrayList<SpettanzeSoci>();

        try {
            if (state != null) {
                result = this.spettanzeSociPersistence.findByFornitoreStato(
                        subjectCode, state);
            } else {
                result = this.spettanzeSociPersistence.findByFornitore(
                        subjectCode);
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        return result;
    }
    
    @Override
    public final List<SpettanzeSoci> findByPartnerPaymentYearPaymentId(
            final String subjectCode, final int year, final int paymentId) {

        List<SpettanzeSoci> result = new ArrayList<SpettanzeSoci>();

        try {
            result = this.spettanzeSociPersistence.
                    findByFornitoreAnnoPagamentoIdPagamento(subjectCode, year,
                            paymentId);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        return result;
    }

    @Override
    public final SpettanzeSoci findByInvoice(
            final TestataFattureClientiPK invoicePK, final String subjectCode) {

        return this.findByInvoiceState(invoicePK, subjectCode, null);
    }

    @Override
    public final SpettanzeSoci findByInvoiceState(
            final TestataFattureClientiPK invoicePK, final String subjectCode,
            final Integer state) {

        SpettanzeSoci result = null;

        try {
            if (state != null) {
                result = this.spettanzeSociPersistence.
                        findByFatturaVenditaStato(invoicePK.getAnno(),
                                invoicePK.getCodiceAttivita(),
                                invoicePK.getCodiceCentro(),
                                invoicePK.getNumeroProtocollo(), subjectCode,
                                state);
            } else {
                result = this.spettanzeSociPersistence.findByFatturaVendita(
                        invoicePK.getAnno(), invoicePK.getCodiceAttivita(),
                        invoicePK.getCodiceCentro(),
                        invoicePK.getNumeroProtocollo(), subjectCode);
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (NoSuchSpettanzeSociException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        return result;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<Object[]> getCreditAmountPerPartner(final Integer state,
            final Date from, final Date to) {
        Session session = this.spettanzeSociPersistence.openSession();
        SQLQuery query;
        QueryPos pos;
        if (state != null) {
            query = session.createSQLQuery(
                    SpettanzeSociLocalServiceImpl.QUERY1);
            pos = QueryPos.getInstance(query);
            pos.add(state);
        } else {
            query = session.createSQLQuery(
                    SpettanzeSociLocalServiceImpl.QUERY2);
            pos = QueryPos.getInstance(query);
        }
        
        pos.add(from);
        pos.add(to);
        return query.list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<SpettanzeSoci> findByPartnerAndComputeDate(
            final String partnerCode, final Date from, final Date to,
            final int start, final int end, final OrderByComparator comp) {

        DynamicQuery dynamicQuery = this.buildDynamicQuery(partnerCode, from,
                to);
        try {
            return SpettanzeSociLocalServiceUtil.dynamicQuery(dynamicQuery,
                    start, end, comp);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            return new ArrayList<SpettanzeSoci>();
        }
    }

    /**
     * Builds the {@link DynamicQuery} based on the specified fields.
     * 
     * @param partnerCode
     *            selected partner
     * @param from
     *            date
     * @param to
     *            date
     * @return the {@link DynamicQuery} created
     */
    private DynamicQuery buildDynamicQuery(final String partnerCode,
            final Date from, final Date to) {
        Junction junction = RestrictionsFactoryUtil.conjunction();
        
        if (Validator.isNotNull(partnerCode)) {
            Property property = PropertyFactoryUtil.forName("codiceFornitore");
            junction.add(property.eq(partnerCode));
        }
        if (Validator.isNotNull(from) && Validator.isNotNull(to)) {
            Property property = PropertyFactoryUtil.forName("dataCalcolo");
            junction.add(property.ge(from));
            junction.add(property.le(to));
        } else if (Validator.isNotNull(from)) {
            Property property = PropertyFactoryUtil.forName("dataCalcolo");
            junction.add(property.ge(from));
        } else {
            Property property = PropertyFactoryUtil.forName("dataCalcolo");
            junction.add(property.le(to));
        }
        
        DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
                SpettanzeSoci.class, getClassLoader());
        return dynamicQuery.add(junction);
    }
}
