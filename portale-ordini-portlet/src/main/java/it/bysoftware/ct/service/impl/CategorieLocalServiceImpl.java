package it.bysoftware.ct.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.bysoftware.ct.model.Categorie;
import it.bysoftware.ct.service.base.CategorieLocalServiceBaseImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * The implementation of the categorie local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.CategorieLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.CategorieLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.CategorieLocalServiceUtil
 */
public class CategorieLocalServiceImpl extends CategorieLocalServiceBaseImpl {

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(
            CategorieLocalServiceBaseImpl.class);

    @Override
    public final List<Categorie> findAll() {
        try {
            return this.categoriePersistence.findAll();
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            return new ArrayList<Categorie>();
        }
    }

}
