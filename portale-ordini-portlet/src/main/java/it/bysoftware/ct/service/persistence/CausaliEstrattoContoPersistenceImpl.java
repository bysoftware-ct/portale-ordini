package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchCausaliEstrattoContoException;
import it.bysoftware.ct.model.CausaliEstrattoConto;
import it.bysoftware.ct.model.impl.CausaliEstrattoContoImpl;
import it.bysoftware.ct.model.impl.CausaliEstrattoContoModelImpl;
import it.bysoftware.ct.service.persistence.CausaliEstrattoContoPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the causali estratto conto service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CausaliEstrattoContoPersistence
 * @see CausaliEstrattoContoUtil
 * @generated
 */
public class CausaliEstrattoContoPersistenceImpl extends BasePersistenceImpl<CausaliEstrattoConto>
    implements CausaliEstrattoContoPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link CausaliEstrattoContoUtil} to access the causali estratto conto persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = CausaliEstrattoContoImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CausaliEstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
            CausaliEstrattoContoModelImpl.FINDER_CACHE_ENABLED,
            CausaliEstrattoContoImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CausaliEstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
            CausaliEstrattoContoModelImpl.FINDER_CACHE_ENABLED,
            CausaliEstrattoContoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CausaliEstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
            CausaliEstrattoContoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_CAUSALIESTRATTOCONTO = "SELECT causaliEstrattoConto FROM CausaliEstrattoConto causaliEstrattoConto";
    private static final String _SQL_COUNT_CAUSALIESTRATTOCONTO = "SELECT COUNT(causaliEstrattoConto) FROM CausaliEstrattoConto causaliEstrattoConto";
    private static final String _ORDER_BY_ENTITY_ALIAS = "causaliEstrattoConto.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CausaliEstrattoConto exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(CausaliEstrattoContoPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "codiceCausaleEC", "descrizioneCausaleEC"
            });
    private static CausaliEstrattoConto _nullCausaliEstrattoConto = new CausaliEstrattoContoImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<CausaliEstrattoConto> toCacheModel() {
                return _nullCausaliEstrattoContoCacheModel;
            }
        };

    private static CacheModel<CausaliEstrattoConto> _nullCausaliEstrattoContoCacheModel =
        new CacheModel<CausaliEstrattoConto>() {
            @Override
            public CausaliEstrattoConto toEntityModel() {
                return _nullCausaliEstrattoConto;
            }
        };

    public CausaliEstrattoContoPersistenceImpl() {
        setModelClass(CausaliEstrattoConto.class);
    }

    /**
     * Caches the causali estratto conto in the entity cache if it is enabled.
     *
     * @param causaliEstrattoConto the causali estratto conto
     */
    @Override
    public void cacheResult(CausaliEstrattoConto causaliEstrattoConto) {
        EntityCacheUtil.putResult(CausaliEstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
            CausaliEstrattoContoImpl.class,
            causaliEstrattoConto.getPrimaryKey(), causaliEstrattoConto);

        causaliEstrattoConto.resetOriginalValues();
    }

    /**
     * Caches the causali estratto contos in the entity cache if it is enabled.
     *
     * @param causaliEstrattoContos the causali estratto contos
     */
    @Override
    public void cacheResult(List<CausaliEstrattoConto> causaliEstrattoContos) {
        for (CausaliEstrattoConto causaliEstrattoConto : causaliEstrattoContos) {
            if (EntityCacheUtil.getResult(
                        CausaliEstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
                        CausaliEstrattoContoImpl.class,
                        causaliEstrattoConto.getPrimaryKey()) == null) {
                cacheResult(causaliEstrattoConto);
            } else {
                causaliEstrattoConto.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all causali estratto contos.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(CausaliEstrattoContoImpl.class.getName());
        }

        EntityCacheUtil.clearCache(CausaliEstrattoContoImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the causali estratto conto.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(CausaliEstrattoConto causaliEstrattoConto) {
        EntityCacheUtil.removeResult(CausaliEstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
            CausaliEstrattoContoImpl.class, causaliEstrattoConto.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<CausaliEstrattoConto> causaliEstrattoContos) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (CausaliEstrattoConto causaliEstrattoConto : causaliEstrattoContos) {
            EntityCacheUtil.removeResult(CausaliEstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
                CausaliEstrattoContoImpl.class,
                causaliEstrattoConto.getPrimaryKey());
        }
    }

    /**
     * Creates a new causali estratto conto with the primary key. Does not add the causali estratto conto to the database.
     *
     * @param codiceCausaleEC the primary key for the new causali estratto conto
     * @return the new causali estratto conto
     */
    @Override
    public CausaliEstrattoConto create(String codiceCausaleEC) {
        CausaliEstrattoConto causaliEstrattoConto = new CausaliEstrattoContoImpl();

        causaliEstrattoConto.setNew(true);
        causaliEstrattoConto.setPrimaryKey(codiceCausaleEC);

        return causaliEstrattoConto;
    }

    /**
     * Removes the causali estratto conto with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param codiceCausaleEC the primary key of the causali estratto conto
     * @return the causali estratto conto that was removed
     * @throws it.bysoftware.ct.NoSuchCausaliEstrattoContoException if a causali estratto conto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CausaliEstrattoConto remove(String codiceCausaleEC)
        throws NoSuchCausaliEstrattoContoException, SystemException {
        return remove((Serializable) codiceCausaleEC);
    }

    /**
     * Removes the causali estratto conto with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the causali estratto conto
     * @return the causali estratto conto that was removed
     * @throws it.bysoftware.ct.NoSuchCausaliEstrattoContoException if a causali estratto conto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CausaliEstrattoConto remove(Serializable primaryKey)
        throws NoSuchCausaliEstrattoContoException, SystemException {
        Session session = null;

        try {
            session = openSession();

            CausaliEstrattoConto causaliEstrattoConto = (CausaliEstrattoConto) session.get(CausaliEstrattoContoImpl.class,
                    primaryKey);

            if (causaliEstrattoConto == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchCausaliEstrattoContoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(causaliEstrattoConto);
        } catch (NoSuchCausaliEstrattoContoException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected CausaliEstrattoConto removeImpl(
        CausaliEstrattoConto causaliEstrattoConto) throws SystemException {
        causaliEstrattoConto = toUnwrappedModel(causaliEstrattoConto);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(causaliEstrattoConto)) {
                causaliEstrattoConto = (CausaliEstrattoConto) session.get(CausaliEstrattoContoImpl.class,
                        causaliEstrattoConto.getPrimaryKeyObj());
            }

            if (causaliEstrattoConto != null) {
                session.delete(causaliEstrattoConto);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (causaliEstrattoConto != null) {
            clearCache(causaliEstrattoConto);
        }

        return causaliEstrattoConto;
    }

    @Override
    public CausaliEstrattoConto updateImpl(
        it.bysoftware.ct.model.CausaliEstrattoConto causaliEstrattoConto)
        throws SystemException {
        causaliEstrattoConto = toUnwrappedModel(causaliEstrattoConto);

        boolean isNew = causaliEstrattoConto.isNew();

        Session session = null;

        try {
            session = openSession();

            if (causaliEstrattoConto.isNew()) {
                session.save(causaliEstrattoConto);

                causaliEstrattoConto.setNew(false);
            } else {
                session.merge(causaliEstrattoConto);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(CausaliEstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
            CausaliEstrattoContoImpl.class,
            causaliEstrattoConto.getPrimaryKey(), causaliEstrattoConto);

        return causaliEstrattoConto;
    }

    protected CausaliEstrattoConto toUnwrappedModel(
        CausaliEstrattoConto causaliEstrattoConto) {
        if (causaliEstrattoConto instanceof CausaliEstrattoContoImpl) {
            return causaliEstrattoConto;
        }

        CausaliEstrattoContoImpl causaliEstrattoContoImpl = new CausaliEstrattoContoImpl();

        causaliEstrattoContoImpl.setNew(causaliEstrattoConto.isNew());
        causaliEstrattoContoImpl.setPrimaryKey(causaliEstrattoConto.getPrimaryKey());

        causaliEstrattoContoImpl.setCodiceCausaleEC(causaliEstrattoConto.getCodiceCausaleEC());
        causaliEstrattoContoImpl.setDescrizioneCausaleEC(causaliEstrattoConto.getDescrizioneCausaleEC());

        return causaliEstrattoContoImpl;
    }

    /**
     * Returns the causali estratto conto with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the causali estratto conto
     * @return the causali estratto conto
     * @throws it.bysoftware.ct.NoSuchCausaliEstrattoContoException if a causali estratto conto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CausaliEstrattoConto findByPrimaryKey(Serializable primaryKey)
        throws NoSuchCausaliEstrattoContoException, SystemException {
        CausaliEstrattoConto causaliEstrattoConto = fetchByPrimaryKey(primaryKey);

        if (causaliEstrattoConto == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchCausaliEstrattoContoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return causaliEstrattoConto;
    }

    /**
     * Returns the causali estratto conto with the primary key or throws a {@link it.bysoftware.ct.NoSuchCausaliEstrattoContoException} if it could not be found.
     *
     * @param codiceCausaleEC the primary key of the causali estratto conto
     * @return the causali estratto conto
     * @throws it.bysoftware.ct.NoSuchCausaliEstrattoContoException if a causali estratto conto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CausaliEstrattoConto findByPrimaryKey(String codiceCausaleEC)
        throws NoSuchCausaliEstrattoContoException, SystemException {
        return findByPrimaryKey((Serializable) codiceCausaleEC);
    }

    /**
     * Returns the causali estratto conto with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the causali estratto conto
     * @return the causali estratto conto, or <code>null</code> if a causali estratto conto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CausaliEstrattoConto fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        CausaliEstrattoConto causaliEstrattoConto = (CausaliEstrattoConto) EntityCacheUtil.getResult(CausaliEstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
                CausaliEstrattoContoImpl.class, primaryKey);

        if (causaliEstrattoConto == _nullCausaliEstrattoConto) {
            return null;
        }

        if (causaliEstrattoConto == null) {
            Session session = null;

            try {
                session = openSession();

                causaliEstrattoConto = (CausaliEstrattoConto) session.get(CausaliEstrattoContoImpl.class,
                        primaryKey);

                if (causaliEstrattoConto != null) {
                    cacheResult(causaliEstrattoConto);
                } else {
                    EntityCacheUtil.putResult(CausaliEstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
                        CausaliEstrattoContoImpl.class, primaryKey,
                        _nullCausaliEstrattoConto);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(CausaliEstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
                    CausaliEstrattoContoImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return causaliEstrattoConto;
    }

    /**
     * Returns the causali estratto conto with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param codiceCausaleEC the primary key of the causali estratto conto
     * @return the causali estratto conto, or <code>null</code> if a causali estratto conto with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CausaliEstrattoConto fetchByPrimaryKey(String codiceCausaleEC)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) codiceCausaleEC);
    }

    /**
     * Returns all the causali estratto contos.
     *
     * @return the causali estratto contos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<CausaliEstrattoConto> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the causali estratto contos.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CausaliEstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of causali estratto contos
     * @param end the upper bound of the range of causali estratto contos (not inclusive)
     * @return the range of causali estratto contos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<CausaliEstrattoConto> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the causali estratto contos.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CausaliEstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of causali estratto contos
     * @param end the upper bound of the range of causali estratto contos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of causali estratto contos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<CausaliEstrattoConto> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<CausaliEstrattoConto> list = (List<CausaliEstrattoConto>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_CAUSALIESTRATTOCONTO);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_CAUSALIESTRATTOCONTO;

                if (pagination) {
                    sql = sql.concat(CausaliEstrattoContoModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<CausaliEstrattoConto>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<CausaliEstrattoConto>(list);
                } else {
                    list = (List<CausaliEstrattoConto>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the causali estratto contos from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (CausaliEstrattoConto causaliEstrattoConto : findAll()) {
            remove(causaliEstrattoConto);
        }
    }

    /**
     * Returns the number of causali estratto contos.
     *
     * @return the number of causali estratto contos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_CAUSALIESTRATTOCONTO);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the causali estratto conto persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.CausaliEstrattoConto")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<CausaliEstrattoConto>> listenersList = new ArrayList<ModelListener<CausaliEstrattoConto>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<CausaliEstrattoConto>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(CausaliEstrattoContoImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
