package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.VociIvaLocalServiceBaseImpl;

/**
 * The implementation of the voci iva local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.VociIvaLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.VociIvaLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.VociIvaLocalServiceUtil
 */
public class VociIvaLocalServiceImpl extends VociIvaLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.VociIvaLocalServiceUtil} to access the voci iva
     * local service.
     */
}
