package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.VarianteLocalServiceBaseImpl;

/**
 * The implementation of the variante local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods 
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.VarianteLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.VarianteLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.VarianteLocalServiceUtil
 */
public class VarianteLocalServiceImpl extends VarianteLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.VarianteLocalServiceUtil} to access the variante
     * local service.
     */
}
