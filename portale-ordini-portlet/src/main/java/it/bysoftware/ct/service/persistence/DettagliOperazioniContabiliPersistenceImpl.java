package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException;
import it.bysoftware.ct.model.DettagliOperazioniContabili;
import it.bysoftware.ct.model.impl.DettagliOperazioniContabiliImpl;
import it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl;
import it.bysoftware.ct.service.persistence.DettagliOperazioniContabiliPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the dettagli operazioni contabili service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see DettagliOperazioniContabiliPersistence
 * @see DettagliOperazioniContabiliUtil
 * @generated
 */
public class DettagliOperazioniContabiliPersistenceImpl
    extends BasePersistenceImpl<DettagliOperazioniContabili>
    implements DettagliOperazioniContabiliPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link DettagliOperazioniContabiliUtil} to access the dettagli operazioni contabili persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = DettagliOperazioniContabiliImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(DettagliOperazioniContabiliModelImpl.ENTITY_CACHE_ENABLED,
            DettagliOperazioniContabiliModelImpl.FINDER_CACHE_ENABLED,
            DettagliOperazioniContabiliImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(DettagliOperazioniContabiliModelImpl.ENTITY_CACHE_ENABLED,
            DettagliOperazioniContabiliModelImpl.FINDER_CACHE_ENABLED,
            DettagliOperazioniContabiliImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(DettagliOperazioniContabiliModelImpl.ENTITY_CACHE_ENABLED,
            DettagliOperazioniContabiliModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
            new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICEOPERAZIONE =
        new FinderPath(DettagliOperazioniContabiliModelImpl.ENTITY_CACHE_ENABLED,
            DettagliOperazioniContabiliModelImpl.FINDER_CACHE_ENABLED,
            DettagliOperazioniContabiliImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCodiceOperazione",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEOPERAZIONE =
        new FinderPath(DettagliOperazioniContabiliModelImpl.ENTITY_CACHE_ENABLED,
            DettagliOperazioniContabiliModelImpl.FINDER_CACHE_ENABLED,
            DettagliOperazioniContabiliImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByCodiceOperazione", new String[] { String.class.getName() },
            DettagliOperazioniContabiliModelImpl.CODICEOPERAZIONE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_CODICEOPERAZIONE = new FinderPath(DettagliOperazioniContabiliModelImpl.ENTITY_CACHE_ENABLED,
            DettagliOperazioniContabiliModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByCodiceOperazione", new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_CODICEOPERAZIONE_CODICEOPERAZIONE_1 =
        "dettagliOperazioniContabili.id.codiceOperazione IS NULL";
    private static final String _FINDER_COLUMN_CODICEOPERAZIONE_CODICEOPERAZIONE_2 =
        "dettagliOperazioniContabili.id.codiceOperazione = ?";
    private static final String _FINDER_COLUMN_CODICEOPERAZIONE_CODICEOPERAZIONE_3 =
        "(dettagliOperazioniContabili.id.codiceOperazione IS NULL OR dettagliOperazioniContabili.id.codiceOperazione = '')";
    private static final String _SQL_SELECT_DETTAGLIOPERAZIONICONTABILI = "SELECT dettagliOperazioniContabili FROM DettagliOperazioniContabili dettagliOperazioniContabili";
    private static final String _SQL_SELECT_DETTAGLIOPERAZIONICONTABILI_WHERE = "SELECT dettagliOperazioniContabili FROM DettagliOperazioniContabili dettagliOperazioniContabili WHERE ";
    private static final String _SQL_COUNT_DETTAGLIOPERAZIONICONTABILI = "SELECT COUNT(dettagliOperazioniContabili) FROM DettagliOperazioniContabili dettagliOperazioniContabili";
    private static final String _SQL_COUNT_DETTAGLIOPERAZIONICONTABILI_WHERE = "SELECT COUNT(dettagliOperazioniContabili) FROM DettagliOperazioniContabili dettagliOperazioniContabili WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "dettagliOperazioniContabili.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No DettagliOperazioniContabili exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No DettagliOperazioniContabili exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(DettagliOperazioniContabiliPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "codiceOperazione", "progressivoLegame", "tipoLegame",
                "causaleContabile", "sottoconto", "naturaConto", "tipoImporto"
            });
    private static DettagliOperazioniContabili _nullDettagliOperazioniContabili = new DettagliOperazioniContabiliImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<DettagliOperazioniContabili> toCacheModel() {
                return _nullDettagliOperazioniContabiliCacheModel;
            }
        };

    private static CacheModel<DettagliOperazioniContabili> _nullDettagliOperazioniContabiliCacheModel =
        new CacheModel<DettagliOperazioniContabili>() {
            @Override
            public DettagliOperazioniContabili toEntityModel() {
                return _nullDettagliOperazioniContabili;
            }
        };

    public DettagliOperazioniContabiliPersistenceImpl() {
        setModelClass(DettagliOperazioniContabili.class);
    }

    /**
     * Returns all the dettagli operazioni contabilis where codiceOperazione = &#63;.
     *
     * @param codiceOperazione the codice operazione
     * @return the matching dettagli operazioni contabilis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<DettagliOperazioniContabili> findByCodiceOperazione(
        String codiceOperazione) throws SystemException {
        return findByCodiceOperazione(codiceOperazione, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the dettagli operazioni contabilis where codiceOperazione = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceOperazione the codice operazione
     * @param start the lower bound of the range of dettagli operazioni contabilis
     * @param end the upper bound of the range of dettagli operazioni contabilis (not inclusive)
     * @return the range of matching dettagli operazioni contabilis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<DettagliOperazioniContabili> findByCodiceOperazione(
        String codiceOperazione, int start, int end) throws SystemException {
        return findByCodiceOperazione(codiceOperazione, start, end, null);
    }

    /**
     * Returns an ordered range of all the dettagli operazioni contabilis where codiceOperazione = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceOperazione the codice operazione
     * @param start the lower bound of the range of dettagli operazioni contabilis
     * @param end the upper bound of the range of dettagli operazioni contabilis (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching dettagli operazioni contabilis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<DettagliOperazioniContabili> findByCodiceOperazione(
        String codiceOperazione, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEOPERAZIONE;
            finderArgs = new Object[] { codiceOperazione };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICEOPERAZIONE;
            finderArgs = new Object[] {
                    codiceOperazione,
                    
                    start, end, orderByComparator
                };
        }

        List<DettagliOperazioniContabili> list = (List<DettagliOperazioniContabili>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (DettagliOperazioniContabili dettagliOperazioniContabili : list) {
                if (!Validator.equals(codiceOperazione,
                            dettagliOperazioniContabili.getCodiceOperazione())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_DETTAGLIOPERAZIONICONTABILI_WHERE);

            boolean bindCodiceOperazione = false;

            if (codiceOperazione == null) {
                query.append(_FINDER_COLUMN_CODICEOPERAZIONE_CODICEOPERAZIONE_1);
            } else if (codiceOperazione.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICEOPERAZIONE_CODICEOPERAZIONE_3);
            } else {
                bindCodiceOperazione = true;

                query.append(_FINDER_COLUMN_CODICEOPERAZIONE_CODICEOPERAZIONE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(DettagliOperazioniContabiliModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceOperazione) {
                    qPos.add(codiceOperazione);
                }

                if (!pagination) {
                    list = (List<DettagliOperazioniContabili>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<DettagliOperazioniContabili>(list);
                } else {
                    list = (List<DettagliOperazioniContabili>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first dettagli operazioni contabili in the ordered set where codiceOperazione = &#63;.
     *
     * @param codiceOperazione the codice operazione
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching dettagli operazioni contabili
     * @throws it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException if a matching dettagli operazioni contabili could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DettagliOperazioniContabili findByCodiceOperazione_First(
        String codiceOperazione, OrderByComparator orderByComparator)
        throws NoSuchDettagliOperazioniContabiliException, SystemException {
        DettagliOperazioniContabili dettagliOperazioniContabili = fetchByCodiceOperazione_First(codiceOperazione,
                orderByComparator);

        if (dettagliOperazioniContabili != null) {
            return dettagliOperazioniContabili;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceOperazione=");
        msg.append(codiceOperazione);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchDettagliOperazioniContabiliException(msg.toString());
    }

    /**
     * Returns the first dettagli operazioni contabili in the ordered set where codiceOperazione = &#63;.
     *
     * @param codiceOperazione the codice operazione
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching dettagli operazioni contabili, or <code>null</code> if a matching dettagli operazioni contabili could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DettagliOperazioniContabili fetchByCodiceOperazione_First(
        String codiceOperazione, OrderByComparator orderByComparator)
        throws SystemException {
        List<DettagliOperazioniContabili> list = findByCodiceOperazione(codiceOperazione,
                0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last dettagli operazioni contabili in the ordered set where codiceOperazione = &#63;.
     *
     * @param codiceOperazione the codice operazione
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching dettagli operazioni contabili
     * @throws it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException if a matching dettagli operazioni contabili could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DettagliOperazioniContabili findByCodiceOperazione_Last(
        String codiceOperazione, OrderByComparator orderByComparator)
        throws NoSuchDettagliOperazioniContabiliException, SystemException {
        DettagliOperazioniContabili dettagliOperazioniContabili = fetchByCodiceOperazione_Last(codiceOperazione,
                orderByComparator);

        if (dettagliOperazioniContabili != null) {
            return dettagliOperazioniContabili;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceOperazione=");
        msg.append(codiceOperazione);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchDettagliOperazioniContabiliException(msg.toString());
    }

    /**
     * Returns the last dettagli operazioni contabili in the ordered set where codiceOperazione = &#63;.
     *
     * @param codiceOperazione the codice operazione
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching dettagli operazioni contabili, or <code>null</code> if a matching dettagli operazioni contabili could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DettagliOperazioniContabili fetchByCodiceOperazione_Last(
        String codiceOperazione, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByCodiceOperazione(codiceOperazione);

        if (count == 0) {
            return null;
        }

        List<DettagliOperazioniContabili> list = findByCodiceOperazione(codiceOperazione,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the dettagli operazioni contabilis before and after the current dettagli operazioni contabili in the ordered set where codiceOperazione = &#63;.
     *
     * @param dettagliOperazioniContabiliPK the primary key of the current dettagli operazioni contabili
     * @param codiceOperazione the codice operazione
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next dettagli operazioni contabili
     * @throws it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException if a dettagli operazioni contabili with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DettagliOperazioniContabili[] findByCodiceOperazione_PrevAndNext(
        DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK,
        String codiceOperazione, OrderByComparator orderByComparator)
        throws NoSuchDettagliOperazioniContabiliException, SystemException {
        DettagliOperazioniContabili dettagliOperazioniContabili = findByPrimaryKey(dettagliOperazioniContabiliPK);

        Session session = null;

        try {
            session = openSession();

            DettagliOperazioniContabili[] array = new DettagliOperazioniContabiliImpl[3];

            array[0] = getByCodiceOperazione_PrevAndNext(session,
                    dettagliOperazioniContabili, codiceOperazione,
                    orderByComparator, true);

            array[1] = dettagliOperazioniContabili;

            array[2] = getByCodiceOperazione_PrevAndNext(session,
                    dettagliOperazioniContabili, codiceOperazione,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected DettagliOperazioniContabili getByCodiceOperazione_PrevAndNext(
        Session session,
        DettagliOperazioniContabili dettagliOperazioniContabili,
        String codiceOperazione, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_DETTAGLIOPERAZIONICONTABILI_WHERE);

        boolean bindCodiceOperazione = false;

        if (codiceOperazione == null) {
            query.append(_FINDER_COLUMN_CODICEOPERAZIONE_CODICEOPERAZIONE_1);
        } else if (codiceOperazione.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CODICEOPERAZIONE_CODICEOPERAZIONE_3);
        } else {
            bindCodiceOperazione = true;

            query.append(_FINDER_COLUMN_CODICEOPERAZIONE_CODICEOPERAZIONE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(DettagliOperazioniContabiliModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindCodiceOperazione) {
            qPos.add(codiceOperazione);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(dettagliOperazioniContabili);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<DettagliOperazioniContabili> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the dettagli operazioni contabilis where codiceOperazione = &#63; from the database.
     *
     * @param codiceOperazione the codice operazione
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByCodiceOperazione(String codiceOperazione)
        throws SystemException {
        for (DettagliOperazioniContabili dettagliOperazioniContabili : findByCodiceOperazione(
                codiceOperazione, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(dettagliOperazioniContabili);
        }
    }

    /**
     * Returns the number of dettagli operazioni contabilis where codiceOperazione = &#63;.
     *
     * @param codiceOperazione the codice operazione
     * @return the number of matching dettagli operazioni contabilis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByCodiceOperazione(String codiceOperazione)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_CODICEOPERAZIONE;

        Object[] finderArgs = new Object[] { codiceOperazione };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_DETTAGLIOPERAZIONICONTABILI_WHERE);

            boolean bindCodiceOperazione = false;

            if (codiceOperazione == null) {
                query.append(_FINDER_COLUMN_CODICEOPERAZIONE_CODICEOPERAZIONE_1);
            } else if (codiceOperazione.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICEOPERAZIONE_CODICEOPERAZIONE_3);
            } else {
                bindCodiceOperazione = true;

                query.append(_FINDER_COLUMN_CODICEOPERAZIONE_CODICEOPERAZIONE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceOperazione) {
                    qPos.add(codiceOperazione);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the dettagli operazioni contabili in the entity cache if it is enabled.
     *
     * @param dettagliOperazioniContabili the dettagli operazioni contabili
     */
    @Override
    public void cacheResult(
        DettagliOperazioniContabili dettagliOperazioniContabili) {
        EntityCacheUtil.putResult(DettagliOperazioniContabiliModelImpl.ENTITY_CACHE_ENABLED,
            DettagliOperazioniContabiliImpl.class,
            dettagliOperazioniContabili.getPrimaryKey(),
            dettagliOperazioniContabili);

        dettagliOperazioniContabili.resetOriginalValues();
    }

    /**
     * Caches the dettagli operazioni contabilis in the entity cache if it is enabled.
     *
     * @param dettagliOperazioniContabilis the dettagli operazioni contabilis
     */
    @Override
    public void cacheResult(
        List<DettagliOperazioniContabili> dettagliOperazioniContabilis) {
        for (DettagliOperazioniContabili dettagliOperazioniContabili : dettagliOperazioniContabilis) {
            if (EntityCacheUtil.getResult(
                        DettagliOperazioniContabiliModelImpl.ENTITY_CACHE_ENABLED,
                        DettagliOperazioniContabiliImpl.class,
                        dettagliOperazioniContabili.getPrimaryKey()) == null) {
                cacheResult(dettagliOperazioniContabili);
            } else {
                dettagliOperazioniContabili.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all dettagli operazioni contabilis.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(DettagliOperazioniContabiliImpl.class.getName());
        }

        EntityCacheUtil.clearCache(DettagliOperazioniContabiliImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the dettagli operazioni contabili.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(
        DettagliOperazioniContabili dettagliOperazioniContabili) {
        EntityCacheUtil.removeResult(DettagliOperazioniContabiliModelImpl.ENTITY_CACHE_ENABLED,
            DettagliOperazioniContabiliImpl.class,
            dettagliOperazioniContabili.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(
        List<DettagliOperazioniContabili> dettagliOperazioniContabilis) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (DettagliOperazioniContabili dettagliOperazioniContabili : dettagliOperazioniContabilis) {
            EntityCacheUtil.removeResult(DettagliOperazioniContabiliModelImpl.ENTITY_CACHE_ENABLED,
                DettagliOperazioniContabiliImpl.class,
                dettagliOperazioniContabili.getPrimaryKey());
        }
    }

    /**
     * Creates a new dettagli operazioni contabili with the primary key. Does not add the dettagli operazioni contabili to the database.
     *
     * @param dettagliOperazioniContabiliPK the primary key for the new dettagli operazioni contabili
     * @return the new dettagli operazioni contabili
     */
    @Override
    public DettagliOperazioniContabili create(
        DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK) {
        DettagliOperazioniContabili dettagliOperazioniContabili = new DettagliOperazioniContabiliImpl();

        dettagliOperazioniContabili.setNew(true);
        dettagliOperazioniContabili.setPrimaryKey(dettagliOperazioniContabiliPK);

        return dettagliOperazioniContabili;
    }

    /**
     * Removes the dettagli operazioni contabili with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param dettagliOperazioniContabiliPK the primary key of the dettagli operazioni contabili
     * @return the dettagli operazioni contabili that was removed
     * @throws it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException if a dettagli operazioni contabili with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DettagliOperazioniContabili remove(
        DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK)
        throws NoSuchDettagliOperazioniContabiliException, SystemException {
        return remove((Serializable) dettagliOperazioniContabiliPK);
    }

    /**
     * Removes the dettagli operazioni contabili with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the dettagli operazioni contabili
     * @return the dettagli operazioni contabili that was removed
     * @throws it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException if a dettagli operazioni contabili with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DettagliOperazioniContabili remove(Serializable primaryKey)
        throws NoSuchDettagliOperazioniContabiliException, SystemException {
        Session session = null;

        try {
            session = openSession();

            DettagliOperazioniContabili dettagliOperazioniContabili = (DettagliOperazioniContabili) session.get(DettagliOperazioniContabiliImpl.class,
                    primaryKey);

            if (dettagliOperazioniContabili == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchDettagliOperazioniContabiliException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(dettagliOperazioniContabili);
        } catch (NoSuchDettagliOperazioniContabiliException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected DettagliOperazioniContabili removeImpl(
        DettagliOperazioniContabili dettagliOperazioniContabili)
        throws SystemException {
        dettagliOperazioniContabili = toUnwrappedModel(dettagliOperazioniContabili);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(dettagliOperazioniContabili)) {
                dettagliOperazioniContabili = (DettagliOperazioniContabili) session.get(DettagliOperazioniContabiliImpl.class,
                        dettagliOperazioniContabili.getPrimaryKeyObj());
            }

            if (dettagliOperazioniContabili != null) {
                session.delete(dettagliOperazioniContabili);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (dettagliOperazioniContabili != null) {
            clearCache(dettagliOperazioniContabili);
        }

        return dettagliOperazioniContabili;
    }

    @Override
    public DettagliOperazioniContabili updateImpl(
        it.bysoftware.ct.model.DettagliOperazioniContabili dettagliOperazioniContabili)
        throws SystemException {
        dettagliOperazioniContabili = toUnwrappedModel(dettagliOperazioniContabili);

        boolean isNew = dettagliOperazioniContabili.isNew();

        DettagliOperazioniContabiliModelImpl dettagliOperazioniContabiliModelImpl =
            (DettagliOperazioniContabiliModelImpl) dettagliOperazioniContabili;

        Session session = null;

        try {
            session = openSession();

            if (dettagliOperazioniContabili.isNew()) {
                session.save(dettagliOperazioniContabili);

                dettagliOperazioniContabili.setNew(false);
            } else {
                session.merge(dettagliOperazioniContabili);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew ||
                !DettagliOperazioniContabiliModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((dettagliOperazioniContabiliModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEOPERAZIONE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        dettagliOperazioniContabiliModelImpl.getOriginalCodiceOperazione()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICEOPERAZIONE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEOPERAZIONE,
                    args);

                args = new Object[] {
                        dettagliOperazioniContabiliModelImpl.getCodiceOperazione()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICEOPERAZIONE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEOPERAZIONE,
                    args);
            }
        }

        EntityCacheUtil.putResult(DettagliOperazioniContabiliModelImpl.ENTITY_CACHE_ENABLED,
            DettagliOperazioniContabiliImpl.class,
            dettagliOperazioniContabili.getPrimaryKey(),
            dettagliOperazioniContabili);

        return dettagliOperazioniContabili;
    }

    protected DettagliOperazioniContabili toUnwrappedModel(
        DettagliOperazioniContabili dettagliOperazioniContabili) {
        if (dettagliOperazioniContabili instanceof DettagliOperazioniContabiliImpl) {
            return dettagliOperazioniContabili;
        }

        DettagliOperazioniContabiliImpl dettagliOperazioniContabiliImpl = new DettagliOperazioniContabiliImpl();

        dettagliOperazioniContabiliImpl.setNew(dettagliOperazioniContabili.isNew());
        dettagliOperazioniContabiliImpl.setPrimaryKey(dettagliOperazioniContabili.getPrimaryKey());

        dettagliOperazioniContabiliImpl.setCodiceOperazione(dettagliOperazioniContabili.getCodiceOperazione());
        dettagliOperazioniContabiliImpl.setProgressivoLegame(dettagliOperazioniContabili.getProgressivoLegame());
        dettagliOperazioniContabiliImpl.setTipoLegame(dettagliOperazioniContabili.getTipoLegame());
        dettagliOperazioniContabiliImpl.setCausaleContabile(dettagliOperazioniContabili.getCausaleContabile());
        dettagliOperazioniContabiliImpl.setSottoconto(dettagliOperazioniContabili.getSottoconto());
        dettagliOperazioniContabiliImpl.setNaturaConto(dettagliOperazioniContabili.getNaturaConto());
        dettagliOperazioniContabiliImpl.setTipoImporto(dettagliOperazioniContabili.getTipoImporto());

        return dettagliOperazioniContabiliImpl;
    }

    /**
     * Returns the dettagli operazioni contabili with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the dettagli operazioni contabili
     * @return the dettagli operazioni contabili
     * @throws it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException if a dettagli operazioni contabili with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DettagliOperazioniContabili findByPrimaryKey(Serializable primaryKey)
        throws NoSuchDettagliOperazioniContabiliException, SystemException {
        DettagliOperazioniContabili dettagliOperazioniContabili = fetchByPrimaryKey(primaryKey);

        if (dettagliOperazioniContabili == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchDettagliOperazioniContabiliException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return dettagliOperazioniContabili;
    }

    /**
     * Returns the dettagli operazioni contabili with the primary key or throws a {@link it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException} if it could not be found.
     *
     * @param dettagliOperazioniContabiliPK the primary key of the dettagli operazioni contabili
     * @return the dettagli operazioni contabili
     * @throws it.bysoftware.ct.NoSuchDettagliOperazioniContabiliException if a dettagli operazioni contabili with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DettagliOperazioniContabili findByPrimaryKey(
        DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK)
        throws NoSuchDettagliOperazioniContabiliException, SystemException {
        return findByPrimaryKey((Serializable) dettagliOperazioniContabiliPK);
    }

    /**
     * Returns the dettagli operazioni contabili with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the dettagli operazioni contabili
     * @return the dettagli operazioni contabili, or <code>null</code> if a dettagli operazioni contabili with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DettagliOperazioniContabili fetchByPrimaryKey(
        Serializable primaryKey) throws SystemException {
        DettagliOperazioniContabili dettagliOperazioniContabili = (DettagliOperazioniContabili) EntityCacheUtil.getResult(DettagliOperazioniContabiliModelImpl.ENTITY_CACHE_ENABLED,
                DettagliOperazioniContabiliImpl.class, primaryKey);

        if (dettagliOperazioniContabili == _nullDettagliOperazioniContabili) {
            return null;
        }

        if (dettagliOperazioniContabili == null) {
            Session session = null;

            try {
                session = openSession();

                dettagliOperazioniContabili = (DettagliOperazioniContabili) session.get(DettagliOperazioniContabiliImpl.class,
                        primaryKey);

                if (dettagliOperazioniContabili != null) {
                    cacheResult(dettagliOperazioniContabili);
                } else {
                    EntityCacheUtil.putResult(DettagliOperazioniContabiliModelImpl.ENTITY_CACHE_ENABLED,
                        DettagliOperazioniContabiliImpl.class, primaryKey,
                        _nullDettagliOperazioniContabili);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(DettagliOperazioniContabiliModelImpl.ENTITY_CACHE_ENABLED,
                    DettagliOperazioniContabiliImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return dettagliOperazioniContabili;
    }

    /**
     * Returns the dettagli operazioni contabili with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param dettagliOperazioniContabiliPK the primary key of the dettagli operazioni contabili
     * @return the dettagli operazioni contabili, or <code>null</code> if a dettagli operazioni contabili with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public DettagliOperazioniContabili fetchByPrimaryKey(
        DettagliOperazioniContabiliPK dettagliOperazioniContabiliPK)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) dettagliOperazioniContabiliPK);
    }

    /**
     * Returns all the dettagli operazioni contabilis.
     *
     * @return the dettagli operazioni contabilis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<DettagliOperazioniContabili> findAll()
        throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the dettagli operazioni contabilis.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of dettagli operazioni contabilis
     * @param end the upper bound of the range of dettagli operazioni contabilis (not inclusive)
     * @return the range of dettagli operazioni contabilis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<DettagliOperazioniContabili> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the dettagli operazioni contabilis.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DettagliOperazioniContabiliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of dettagli operazioni contabilis
     * @param end the upper bound of the range of dettagli operazioni contabilis (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of dettagli operazioni contabilis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<DettagliOperazioniContabili> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<DettagliOperazioniContabili> list = (List<DettagliOperazioniContabili>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_DETTAGLIOPERAZIONICONTABILI);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_DETTAGLIOPERAZIONICONTABILI;

                if (pagination) {
                    sql = sql.concat(DettagliOperazioniContabiliModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<DettagliOperazioniContabili>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<DettagliOperazioniContabili>(list);
                } else {
                    list = (List<DettagliOperazioniContabili>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the dettagli operazioni contabilis from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (DettagliOperazioniContabili dettagliOperazioniContabili : findAll()) {
            remove(dettagliOperazioniContabili);
        }
    }

    /**
     * Returns the number of dettagli operazioni contabilis.
     *
     * @return the number of dettagli operazioni contabilis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_DETTAGLIOPERAZIONICONTABILI);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the dettagli operazioni contabili persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.DettagliOperazioniContabili")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<DettagliOperazioniContabili>> listenersList = new ArrayList<ModelListener<DettagliOperazioniContabili>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<DettagliOperazioniContabili>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(DettagliOperazioniContabiliImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
