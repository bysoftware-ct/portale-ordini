package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.ArticoliServiceBaseImpl;

/**
 * The implementation of the articoli remote service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.ArticoliService} interface.
 *
 * <p>This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.ArticoliServiceBaseImpl
 * @see it.bysoftware.ct.service.ArticoliServiceUtil
 */
public class ArticoliServiceImpl extends ArticoliServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.ArticoliServiceUtil} to access the articoli
     * remote service.
     */
}
