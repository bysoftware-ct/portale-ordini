package it.bysoftware.ct.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Junction;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import it.bysoftware.ct.NoSuchArticoloSocioException;
import it.bysoftware.ct.model.ArticoloSocio;
import it.bysoftware.ct.service.ArticoloSocioLocalServiceUtil;
import it.bysoftware.ct.service.base.ArticoloSocioLocalServiceBaseImpl;

/**
 * The implementation of the articolo socio local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.ArticoloSocioLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.ArticoloSocioLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.ArticoloSocioLocalServiceUtil
 */
public class ArticoloSocioLocalServiceImpl extends
        ArticoloSocioLocalServiceBaseImpl {

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil
            .getLog(ArticoloSocioLocalServiceImpl.class);

    @Override
    public final ArticoloSocio findPartnerItems(final String partner,
            final String item, final String variant) {
        ArticoloSocio result = null;

        try {
            List<ArticoloSocio> tmp = this.articoloSocioPersistence.
                    findBySocioArticolo(partner, item);
            for (ArticoloSocio articoloSocio : tmp) {
                if (variant.isEmpty()
                        && articoloSocio.getCodiceVariante().isEmpty()) {
                    result = articoloSocio;
                } else if (!variant.isEmpty()
                        && articoloSocio.getCodiceVariante().equals(variant)) {
                    result = articoloSocio;
                }
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        return result;
    }
    
    @Override
    public final ArticoloSocio findCompanyItem(final String partner,
            final String item, final String variant) {
        ArticoloSocio result = null;

        try {
            result = this.articoloSocioPersistence.findByArticoloVarianteSocio(
                    partner, item, variant);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (NoSuchArticoloSocioException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        return result;
    }
    
    @Override
    public final List<ArticoloSocio> findAllPartnerItems(final String partner) {
        List<ArticoloSocio> result = new ArrayList<ArticoloSocio>();
        try {
            result = this.articoloSocioPersistence.findBySocio(partner);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        return result;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<ArticoloSocio> findPartnerItems(final String partner,
            final String itemCode, final String itemVariant,
            final String prtnerItemCode, final String partnerItemVariant,
            final boolean andSearch, final int start, final int end,
            final OrderByComparator comparator) {
        
        DynamicQuery dynamicQuery = this.buildDynamicQuery(partner, itemCode,
                itemVariant, prtnerItemCode, partnerItemVariant, andSearch);
        try {
            return ArticoloSocioLocalServiceUtil.dynamicQuery(dynamicQuery,
                    start, end, comparator);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            return new ArrayList<ArticoloSocio>();
        }
        
    }

    @Override
    public final int findPartnerItemsCount(final String partner,
            final String itemCode, final String itemVariant,
            final String partnerItemCode, final String partnerItemVariant,
            final boolean andSearch) {
        
        DynamicQuery dynamicQuery = this.buildDynamicQuery(partner, itemCode,
                itemVariant, partnerItemCode, partnerItemVariant, andSearch);
        try {
            return (int) ArticoloSocioLocalServiceUtil.dynamicQueryCount(
                    dynamicQuery);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            return 0;
        }
        
    }
    
    /**
     * Returns {@link DynamicQuery} using the specified filters.
     * 
     * @param partner
     *            partner code
     * @param itemCode
     *            company item code
     * @param itemVariant
     *            company item variant code
     * @param partnerItemCode
     *            partner item code
     * @param partnerItemVariant
     *            partner item variant code
     * @param andSearch
     *            true for and query false othewise.
     * @return {@link DynamicQuery}
     */
    private DynamicQuery buildDynamicQuery(final String partner,
            final String itemCode, final String itemVariant,
            final String partnerItemCode, final String partnerItemVariant,
            final boolean andSearch) {
        Junction junction = RestrictionsFactoryUtil.conjunction();
        Property property = PropertyFactoryUtil.forName("primaryKey.associato");
        junction.add(property.eq(partner));
        Junction junction1 = null;
        if (andSearch) {
            junction1 = RestrictionsFactoryUtil.conjunction();
        } else {
            junction1 = RestrictionsFactoryUtil.disjunction();
        }
        if (Validator.isNotNull(itemCode)) {
            property = PropertyFactoryUtil.forName("codiceArticolo");
            String value = (new StringBuilder(StringPool.PERCENT)).append(
                    itemCode).append(StringPool.PERCENT).toString();
            junction1.add(property.like(value));
        }
        if (Validator.isNotNull(itemVariant)) {
            property = PropertyFactoryUtil.forName("codiceVariante");
            String value = (new StringBuilder(StringPool.PERCENT)).append(
                    itemVariant).append(StringPool.PERCENT).toString();
            junction1.add(property.like(value));
        }
        if (Validator.isNotNull(partnerItemCode)) {
            property = PropertyFactoryUtil.forName(
                    "primaryKey.codiceArticoloSocio");
            String value = (new StringBuilder(StringPool.PERCENT)).append(
                    partnerItemCode).append(StringPool.PERCENT).toString();
            junction1.add(property.like(value));
        }
        if (Validator.isNotNull(partnerItemVariant)) {
            property = PropertyFactoryUtil.forName(
                    "primaryKey.codiceVarianteSocio");
            String value = (new StringBuilder(StringPool.PERCENT)).append(
                    partnerItemVariant).append(StringPool.PERCENT).toString();
            junction1.add(property.like(value));
        }
        
        this.logger.debug("PARTNER: " + partner);
        this.logger.debug("itemCode: " + itemCode);
        this.logger.debug("itemVariant: " + itemVariant);
        this.logger.debug("prtnerItemCode: " + partnerItemCode);
        this.logger.debug("partnerItemVariant: " + partnerItemVariant);
        
        junction.add(junction1);
        DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
                ArticoloSocio.class, getClassLoader());
        return dynamicQuery.add(junction);
    }
}
