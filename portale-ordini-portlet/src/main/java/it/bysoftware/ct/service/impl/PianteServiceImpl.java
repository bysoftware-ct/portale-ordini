package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.PianteServiceBaseImpl;

/**
 * The implementation of the piante remote service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.PianteService} interface.
 *
 * <p>This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.PianteServiceBaseImpl
 * @see it.bysoftware.ct.service.PianteServiceUtil
 */
public class PianteServiceImpl extends PianteServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.PianteServiceUtil} to access the piante remote
     * service.
     */
}
