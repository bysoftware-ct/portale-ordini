package it.bysoftware.ct.service.impl;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.util.portlet.PortletProps;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import it.bysoftware.ct.NoSuchTestataDocumentoException;
import it.bysoftware.ct.model.TestataDocumento;
import it.bysoftware.ct.service.TestataDocumentoLocalServiceUtil;
import it.bysoftware.ct.service.base.TestataDocumentoLocalServiceBaseImpl;

/**
 * The implementation of the testata documento local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.TestataDocumentoLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.TestataDocumentoLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.TestataDocumentoLocalServiceUtil
 */
public class TestataDocumentoLocalServiceImpl extends
        TestataDocumentoLocalServiceBaseImpl {
    
    /**
     * Give document type constant.
     */
    private static final String DDM = PortletProps.get("give-document");
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(TestataDocumentoLocalServiceImpl.
            class);

    @SuppressWarnings("unchecked")
    @Override
    public final int getNumber(final int year, final String activity,
            final String center, final String storage, final String partner,
            final String docType) {

        int result = 0;

        DynamicQuery userQuery = DynamicQueryFactoryUtil.forClass(
                TestataDocumento.class, getClassLoader());
        userQuery.add(PropertyFactoryUtil.forName("primaryKey.anno").eq(year));
        userQuery.add(PropertyFactoryUtil.forName("primaryKey.codiceAttivita").
                eq(activity));
        userQuery.add(PropertyFactoryUtil.forName("primaryKey.codiceCentro").
                eq(center));
        userQuery.add(PropertyFactoryUtil.forName("primaryKey.codiceDeposito").
                eq(storage));
        userQuery.add(PropertyFactoryUtil.forName("primaryKey.codiceFornitore").
                eq(partner));
        userQuery.add(PropertyFactoryUtil.forName("primaryKey.tipoDocumento").
                eq(docType));
        Projection projection = PropertyFactoryUtil.forName(
                "primaryKey.protocollo").max();
        userQuery.setProjection(projection);
        try {
            List<Object> list = TestataDocumentoLocalServiceUtil
                    .dynamicQuery(userQuery);

            if (Validator.isNumber(String.valueOf(list.get(0)))) {
                result = Integer.parseInt(String.valueOf(list.get(0)));
            } else {
                result = 0;
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        return result + 1;
    }

    @Override
    public final List<TestataDocumento> findTodayDocuments(final Date date,
            final String type) {
        List<TestataDocumento> result = null;

        try {
            result = this.testataDocumentoPersistence.
                    findByDataStatoTipoDocumento(date, false, type);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        return result;
    }
    
    @Override
    public final List<TestataDocumento> findDocumentsByDatePartnerType(
    		final Date date, final String code, final String type) {
        List<TestataDocumento> result = null;

        try {
            result = this.testataDocumentoPersistence.
            		findByDataFornitoreTipoDocumento(date, code, type);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        return result;
    }
    
    @Override
    public final List<TestataDocumento> findDocuments(final String type) {
        List<TestataDocumento> result = null;

        try {
            result = this.testataDocumentoPersistence.
                    findByStatoTipoDocumento(false, type);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        return result;
    }
    
    @Override
    public final int getOpenDocuments(final long orderId) {
        int count = 0;
        try {
            count = this.testataDocumentoPersistence.
                    findByIdOrdineTipoDocumentoStato(orderId, false,
                            TestataDocumentoLocalServiceImpl.DDM).
                    size();
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
        }
    }
        return count;
    }
    
    @Override
    public final List<TestataDocumento> getDocumentsByOrderId(final long oId) {
        try {
            return this.testataDocumentoPersistence
                    .findByIdOrdineTipoDocumentoStato(oId, true,
                            TestataDocumentoLocalServiceImpl.DDM);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            return new ArrayList<TestataDocumento>();
        }
    }
    
    @Override
    public final List<TestataDocumento> findDocumentsByOrderId(final long oId) {
        List<TestataDocumento> result = null;
        try {
            result = this.testataDocumentoPersistence.
                    findByIdOrdineTipo(oId,
                            TestataDocumentoLocalServiceImpl.DDM);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        return result;
    }
    
    @Override
    public final TestataDocumento findDocumentsByOrderIdPartner(final long oId,
            final String partnerCode) {
        TestataDocumento result = null;
        try {
            result =  this.testataDocumentoPersistence
                    .findByIdOrdineFornitoreTipo(oId, partnerCode,
                            TestataDocumentoLocalServiceImpl.DDM);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (NoSuchTestataDocumentoException e) {
            this.logger.debug(e.getMessage() + ".\nNew one will be created.");
        }
        return result;
    }
}
