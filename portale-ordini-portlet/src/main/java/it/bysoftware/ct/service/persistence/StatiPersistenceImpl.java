package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchStatiException;
import it.bysoftware.ct.model.Stati;
import it.bysoftware.ct.model.impl.StatiImpl;
import it.bysoftware.ct.model.impl.StatiModelImpl;
import it.bysoftware.ct.service.persistence.StatiPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the stati service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see StatiPersistence
 * @see StatiUtil
 * @generated
 */
public class StatiPersistenceImpl extends BasePersistenceImpl<Stati>
    implements StatiPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link StatiUtil} to access the stati persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = StatiImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(StatiModelImpl.ENTITY_CACHE_ENABLED,
            StatiModelImpl.FINDER_CACHE_ENABLED, StatiImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(StatiModelImpl.ENTITY_CACHE_ENABLED,
            StatiModelImpl.FINDER_CACHE_ENABLED, StatiImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(StatiModelImpl.ENTITY_CACHE_ENABLED,
            StatiModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_STATI = "SELECT stati FROM Stati stati";
    private static final String _SQL_COUNT_STATI = "SELECT COUNT(stati) FROM Stati stati";
    private static final String _ORDER_BY_ENTITY_ALIAS = "stati.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Stati exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(StatiPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "black", "codiceStato", "codiceISO", "codicePaese", "stato",
                "codiceAzienda"
            });
    private static Stati _nullStati = new StatiImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Stati> toCacheModel() {
                return _nullStatiCacheModel;
            }
        };

    private static CacheModel<Stati> _nullStatiCacheModel = new CacheModel<Stati>() {
            @Override
            public Stati toEntityModel() {
                return _nullStati;
            }
        };

    public StatiPersistenceImpl() {
        setModelClass(Stati.class);
    }

    /**
     * Caches the stati in the entity cache if it is enabled.
     *
     * @param stati the stati
     */
    @Override
    public void cacheResult(Stati stati) {
        EntityCacheUtil.putResult(StatiModelImpl.ENTITY_CACHE_ENABLED,
            StatiImpl.class, stati.getPrimaryKey(), stati);

        stati.resetOriginalValues();
    }

    /**
     * Caches the statis in the entity cache if it is enabled.
     *
     * @param statis the statis
     */
    @Override
    public void cacheResult(List<Stati> statis) {
        for (Stati stati : statis) {
            if (EntityCacheUtil.getResult(StatiModelImpl.ENTITY_CACHE_ENABLED,
                        StatiImpl.class, stati.getPrimaryKey()) == null) {
                cacheResult(stati);
            } else {
                stati.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all statis.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(StatiImpl.class.getName());
        }

        EntityCacheUtil.clearCache(StatiImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the stati.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Stati stati) {
        EntityCacheUtil.removeResult(StatiModelImpl.ENTITY_CACHE_ENABLED,
            StatiImpl.class, stati.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Stati> statis) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Stati stati : statis) {
            EntityCacheUtil.removeResult(StatiModelImpl.ENTITY_CACHE_ENABLED,
                StatiImpl.class, stati.getPrimaryKey());
        }
    }

    /**
     * Creates a new stati with the primary key. Does not add the stati to the database.
     *
     * @param codiceStato the primary key for the new stati
     * @return the new stati
     */
    @Override
    public Stati create(String codiceStato) {
        Stati stati = new StatiImpl();

        stati.setNew(true);
        stati.setPrimaryKey(codiceStato);

        return stati;
    }

    /**
     * Removes the stati with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param codiceStato the primary key of the stati
     * @return the stati that was removed
     * @throws it.bysoftware.ct.NoSuchStatiException if a stati with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Stati remove(String codiceStato)
        throws NoSuchStatiException, SystemException {
        return remove((Serializable) codiceStato);
    }

    /**
     * Removes the stati with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the stati
     * @return the stati that was removed
     * @throws it.bysoftware.ct.NoSuchStatiException if a stati with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Stati remove(Serializable primaryKey)
        throws NoSuchStatiException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Stati stati = (Stati) session.get(StatiImpl.class, primaryKey);

            if (stati == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchStatiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(stati);
        } catch (NoSuchStatiException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Stati removeImpl(Stati stati) throws SystemException {
        stati = toUnwrappedModel(stati);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(stati)) {
                stati = (Stati) session.get(StatiImpl.class,
                        stati.getPrimaryKeyObj());
            }

            if (stati != null) {
                session.delete(stati);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (stati != null) {
            clearCache(stati);
        }

        return stati;
    }

    @Override
    public Stati updateImpl(it.bysoftware.ct.model.Stati stati)
        throws SystemException {
        stati = toUnwrappedModel(stati);

        boolean isNew = stati.isNew();

        Session session = null;

        try {
            session = openSession();

            if (stati.isNew()) {
                session.save(stati);

                stati.setNew(false);
            } else {
                session.merge(stati);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(StatiModelImpl.ENTITY_CACHE_ENABLED,
            StatiImpl.class, stati.getPrimaryKey(), stati);

        return stati;
    }

    protected Stati toUnwrappedModel(Stati stati) {
        if (stati instanceof StatiImpl) {
            return stati;
        }

        StatiImpl statiImpl = new StatiImpl();

        statiImpl.setNew(stati.isNew());
        statiImpl.setPrimaryKey(stati.getPrimaryKey());

        statiImpl.setBlack(stati.isBlack());
        statiImpl.setCodiceStato(stati.getCodiceStato());
        statiImpl.setCodiceISO(stati.getCodiceISO());
        statiImpl.setCodicePaese(stati.getCodicePaese());
        statiImpl.setStato(stati.getStato());
        statiImpl.setCodiceAzienda(stati.getCodiceAzienda());

        return statiImpl;
    }

    /**
     * Returns the stati with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the stati
     * @return the stati
     * @throws it.bysoftware.ct.NoSuchStatiException if a stati with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Stati findByPrimaryKey(Serializable primaryKey)
        throws NoSuchStatiException, SystemException {
        Stati stati = fetchByPrimaryKey(primaryKey);

        if (stati == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchStatiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return stati;
    }

    /**
     * Returns the stati with the primary key or throws a {@link it.bysoftware.ct.NoSuchStatiException} if it could not be found.
     *
     * @param codiceStato the primary key of the stati
     * @return the stati
     * @throws it.bysoftware.ct.NoSuchStatiException if a stati with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Stati findByPrimaryKey(String codiceStato)
        throws NoSuchStatiException, SystemException {
        return findByPrimaryKey((Serializable) codiceStato);
    }

    /**
     * Returns the stati with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the stati
     * @return the stati, or <code>null</code> if a stati with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Stati fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Stati stati = (Stati) EntityCacheUtil.getResult(StatiModelImpl.ENTITY_CACHE_ENABLED,
                StatiImpl.class, primaryKey);

        if (stati == _nullStati) {
            return null;
        }

        if (stati == null) {
            Session session = null;

            try {
                session = openSession();

                stati = (Stati) session.get(StatiImpl.class, primaryKey);

                if (stati != null) {
                    cacheResult(stati);
                } else {
                    EntityCacheUtil.putResult(StatiModelImpl.ENTITY_CACHE_ENABLED,
                        StatiImpl.class, primaryKey, _nullStati);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(StatiModelImpl.ENTITY_CACHE_ENABLED,
                    StatiImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return stati;
    }

    /**
     * Returns the stati with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param codiceStato the primary key of the stati
     * @return the stati, or <code>null</code> if a stati with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Stati fetchByPrimaryKey(String codiceStato)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) codiceStato);
    }

    /**
     * Returns all the statis.
     *
     * @return the statis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Stati> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the statis.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.StatiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of statis
     * @param end the upper bound of the range of statis (not inclusive)
     * @return the range of statis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Stati> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the statis.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.StatiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of statis
     * @param end the upper bound of the range of statis (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of statis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Stati> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Stati> list = (List<Stati>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_STATI);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_STATI;

                if (pagination) {
                    sql = sql.concat(StatiModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Stati>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Stati>(list);
                } else {
                    list = (List<Stati>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the statis from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Stati stati : findAll()) {
            remove(stati);
        }
    }

    /**
     * Returns the number of statis.
     *
     * @return the number of statis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_STATI);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the stati persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.Stati")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Stati>> listenersList = new ArrayList<ModelListener<Stati>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Stati>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(StatiImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
