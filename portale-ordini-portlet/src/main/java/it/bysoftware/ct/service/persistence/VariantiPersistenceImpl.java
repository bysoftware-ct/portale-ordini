package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchVariantiException;
import it.bysoftware.ct.model.Varianti;
import it.bysoftware.ct.model.impl.VariantiImpl;
import it.bysoftware.ct.model.impl.VariantiModelImpl;
import it.bysoftware.ct.service.persistence.VariantiPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the varianti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see VariantiPersistence
 * @see VariantiUtil
 * @generated
 */
public class VariantiPersistenceImpl extends BasePersistenceImpl<Varianti>
    implements VariantiPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link VariantiUtil} to access the varianti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = VariantiImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VariantiModelImpl.ENTITY_CACHE_ENABLED,
            VariantiModelImpl.FINDER_CACHE_ENABLED, VariantiImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VariantiModelImpl.ENTITY_CACHE_ENABLED,
            VariantiModelImpl.FINDER_CACHE_ENABLED, VariantiImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VariantiModelImpl.ENTITY_CACHE_ENABLED,
            VariantiModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TIPOVARIANTE =
        new FinderPath(VariantiModelImpl.ENTITY_CACHE_ENABLED,
            VariantiModelImpl.FINDER_CACHE_ENABLED, VariantiImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByTipoVariante",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TIPOVARIANTE =
        new FinderPath(VariantiModelImpl.ENTITY_CACHE_ENABLED,
            VariantiModelImpl.FINDER_CACHE_ENABLED, VariantiImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByTipoVariante",
            new String[] { String.class.getName() },
            VariantiModelImpl.TIPOVARIANTE_COLUMN_BITMASK |
            VariantiModelImpl.DESCRIZIONE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_TIPOVARIANTE = new FinderPath(VariantiModelImpl.ENTITY_CACHE_ENABLED,
            VariantiModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTipoVariante",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_TIPOVARIANTE_TIPOVARIANTE_1 = "varianti.id.tipoVariante IS NULL";
    private static final String _FINDER_COLUMN_TIPOVARIANTE_TIPOVARIANTE_2 = "varianti.id.tipoVariante = ?";
    private static final String _FINDER_COLUMN_TIPOVARIANTE_TIPOVARIANTE_3 = "(varianti.id.tipoVariante IS NULL OR varianti.id.tipoVariante = '')";
    private static final String _SQL_SELECT_VARIANTI = "SELECT varianti FROM Varianti varianti";
    private static final String _SQL_SELECT_VARIANTI_WHERE = "SELECT varianti FROM Varianti varianti WHERE ";
    private static final String _SQL_COUNT_VARIANTI = "SELECT COUNT(varianti) FROM Varianti varianti";
    private static final String _SQL_COUNT_VARIANTI_WHERE = "SELECT COUNT(varianti) FROM Varianti varianti WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "varianti.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Varianti exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Varianti exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(VariantiPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "codiceVariante", "tipoVariante", "descrizione"
            });
    private static Varianti _nullVarianti = new VariantiImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Varianti> toCacheModel() {
                return _nullVariantiCacheModel;
            }
        };

    private static CacheModel<Varianti> _nullVariantiCacheModel = new CacheModel<Varianti>() {
            @Override
            public Varianti toEntityModel() {
                return _nullVarianti;
            }
        };

    public VariantiPersistenceImpl() {
        setModelClass(Varianti.class);
    }

    /**
     * Returns all the variantis where tipoVariante = &#63;.
     *
     * @param tipoVariante the tipo variante
     * @return the matching variantis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Varianti> findByTipoVariante(String tipoVariante)
        throws SystemException {
        return findByTipoVariante(tipoVariante, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the variantis where tipoVariante = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VariantiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param tipoVariante the tipo variante
     * @param start the lower bound of the range of variantis
     * @param end the upper bound of the range of variantis (not inclusive)
     * @return the range of matching variantis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Varianti> findByTipoVariante(String tipoVariante, int start,
        int end) throws SystemException {
        return findByTipoVariante(tipoVariante, start, end, null);
    }

    /**
     * Returns an ordered range of all the variantis where tipoVariante = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VariantiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param tipoVariante the tipo variante
     * @param start the lower bound of the range of variantis
     * @param end the upper bound of the range of variantis (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching variantis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Varianti> findByTipoVariante(String tipoVariante, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TIPOVARIANTE;
            finderArgs = new Object[] { tipoVariante };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TIPOVARIANTE;
            finderArgs = new Object[] {
                    tipoVariante,
                    
                    start, end, orderByComparator
                };
        }

        List<Varianti> list = (List<Varianti>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Varianti varianti : list) {
                if (!Validator.equals(tipoVariante, varianti.getTipoVariante())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VARIANTI_WHERE);

            boolean bindTipoVariante = false;

            if (tipoVariante == null) {
                query.append(_FINDER_COLUMN_TIPOVARIANTE_TIPOVARIANTE_1);
            } else if (tipoVariante.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_TIPOVARIANTE_TIPOVARIANTE_3);
            } else {
                bindTipoVariante = true;

                query.append(_FINDER_COLUMN_TIPOVARIANTE_TIPOVARIANTE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VariantiModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindTipoVariante) {
                    qPos.add(tipoVariante);
                }

                if (!pagination) {
                    list = (List<Varianti>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Varianti>(list);
                } else {
                    list = (List<Varianti>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first varianti in the ordered set where tipoVariante = &#63;.
     *
     * @param tipoVariante the tipo variante
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching varianti
     * @throws it.bysoftware.ct.NoSuchVariantiException if a matching varianti could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Varianti findByTipoVariante_First(String tipoVariante,
        OrderByComparator orderByComparator)
        throws NoSuchVariantiException, SystemException {
        Varianti varianti = fetchByTipoVariante_First(tipoVariante,
                orderByComparator);

        if (varianti != null) {
            return varianti;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("tipoVariante=");
        msg.append(tipoVariante);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVariantiException(msg.toString());
    }

    /**
     * Returns the first varianti in the ordered set where tipoVariante = &#63;.
     *
     * @param tipoVariante the tipo variante
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching varianti, or <code>null</code> if a matching varianti could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Varianti fetchByTipoVariante_First(String tipoVariante,
        OrderByComparator orderByComparator) throws SystemException {
        List<Varianti> list = findByTipoVariante(tipoVariante, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last varianti in the ordered set where tipoVariante = &#63;.
     *
     * @param tipoVariante the tipo variante
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching varianti
     * @throws it.bysoftware.ct.NoSuchVariantiException if a matching varianti could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Varianti findByTipoVariante_Last(String tipoVariante,
        OrderByComparator orderByComparator)
        throws NoSuchVariantiException, SystemException {
        Varianti varianti = fetchByTipoVariante_Last(tipoVariante,
                orderByComparator);

        if (varianti != null) {
            return varianti;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("tipoVariante=");
        msg.append(tipoVariante);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVariantiException(msg.toString());
    }

    /**
     * Returns the last varianti in the ordered set where tipoVariante = &#63;.
     *
     * @param tipoVariante the tipo variante
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching varianti, or <code>null</code> if a matching varianti could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Varianti fetchByTipoVariante_Last(String tipoVariante,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByTipoVariante(tipoVariante);

        if (count == 0) {
            return null;
        }

        List<Varianti> list = findByTipoVariante(tipoVariante, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the variantis before and after the current varianti in the ordered set where tipoVariante = &#63;.
     *
     * @param variantiPK the primary key of the current varianti
     * @param tipoVariante the tipo variante
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next varianti
     * @throws it.bysoftware.ct.NoSuchVariantiException if a varianti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Varianti[] findByTipoVariante_PrevAndNext(VariantiPK variantiPK,
        String tipoVariante, OrderByComparator orderByComparator)
        throws NoSuchVariantiException, SystemException {
        Varianti varianti = findByPrimaryKey(variantiPK);

        Session session = null;

        try {
            session = openSession();

            Varianti[] array = new VariantiImpl[3];

            array[0] = getByTipoVariante_PrevAndNext(session, varianti,
                    tipoVariante, orderByComparator, true);

            array[1] = varianti;

            array[2] = getByTipoVariante_PrevAndNext(session, varianti,
                    tipoVariante, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Varianti getByTipoVariante_PrevAndNext(Session session,
        Varianti varianti, String tipoVariante,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VARIANTI_WHERE);

        boolean bindTipoVariante = false;

        if (tipoVariante == null) {
            query.append(_FINDER_COLUMN_TIPOVARIANTE_TIPOVARIANTE_1);
        } else if (tipoVariante.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_TIPOVARIANTE_TIPOVARIANTE_3);
        } else {
            bindTipoVariante = true;

            query.append(_FINDER_COLUMN_TIPOVARIANTE_TIPOVARIANTE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VariantiModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindTipoVariante) {
            qPos.add(tipoVariante);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(varianti);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Varianti> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the variantis where tipoVariante = &#63; from the database.
     *
     * @param tipoVariante the tipo variante
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByTipoVariante(String tipoVariante)
        throws SystemException {
        for (Varianti varianti : findByTipoVariante(tipoVariante,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(varianti);
        }
    }

    /**
     * Returns the number of variantis where tipoVariante = &#63;.
     *
     * @param tipoVariante the tipo variante
     * @return the number of matching variantis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByTipoVariante(String tipoVariante)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_TIPOVARIANTE;

        Object[] finderArgs = new Object[] { tipoVariante };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VARIANTI_WHERE);

            boolean bindTipoVariante = false;

            if (tipoVariante == null) {
                query.append(_FINDER_COLUMN_TIPOVARIANTE_TIPOVARIANTE_1);
            } else if (tipoVariante.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_TIPOVARIANTE_TIPOVARIANTE_3);
            } else {
                bindTipoVariante = true;

                query.append(_FINDER_COLUMN_TIPOVARIANTE_TIPOVARIANTE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindTipoVariante) {
                    qPos.add(tipoVariante);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the varianti in the entity cache if it is enabled.
     *
     * @param varianti the varianti
     */
    @Override
    public void cacheResult(Varianti varianti) {
        EntityCacheUtil.putResult(VariantiModelImpl.ENTITY_CACHE_ENABLED,
            VariantiImpl.class, varianti.getPrimaryKey(), varianti);

        varianti.resetOriginalValues();
    }

    /**
     * Caches the variantis in the entity cache if it is enabled.
     *
     * @param variantis the variantis
     */
    @Override
    public void cacheResult(List<Varianti> variantis) {
        for (Varianti varianti : variantis) {
            if (EntityCacheUtil.getResult(
                        VariantiModelImpl.ENTITY_CACHE_ENABLED,
                        VariantiImpl.class, varianti.getPrimaryKey()) == null) {
                cacheResult(varianti);
            } else {
                varianti.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all variantis.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(VariantiImpl.class.getName());
        }

        EntityCacheUtil.clearCache(VariantiImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the varianti.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Varianti varianti) {
        EntityCacheUtil.removeResult(VariantiModelImpl.ENTITY_CACHE_ENABLED,
            VariantiImpl.class, varianti.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Varianti> variantis) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Varianti varianti : variantis) {
            EntityCacheUtil.removeResult(VariantiModelImpl.ENTITY_CACHE_ENABLED,
                VariantiImpl.class, varianti.getPrimaryKey());
        }
    }

    /**
     * Creates a new varianti with the primary key. Does not add the varianti to the database.
     *
     * @param variantiPK the primary key for the new varianti
     * @return the new varianti
     */
    @Override
    public Varianti create(VariantiPK variantiPK) {
        Varianti varianti = new VariantiImpl();

        varianti.setNew(true);
        varianti.setPrimaryKey(variantiPK);

        return varianti;
    }

    /**
     * Removes the varianti with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param variantiPK the primary key of the varianti
     * @return the varianti that was removed
     * @throws it.bysoftware.ct.NoSuchVariantiException if a varianti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Varianti remove(VariantiPK variantiPK)
        throws NoSuchVariantiException, SystemException {
        return remove((Serializable) variantiPK);
    }

    /**
     * Removes the varianti with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the varianti
     * @return the varianti that was removed
     * @throws it.bysoftware.ct.NoSuchVariantiException if a varianti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Varianti remove(Serializable primaryKey)
        throws NoSuchVariantiException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Varianti varianti = (Varianti) session.get(VariantiImpl.class,
                    primaryKey);

            if (varianti == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchVariantiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(varianti);
        } catch (NoSuchVariantiException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Varianti removeImpl(Varianti varianti) throws SystemException {
        varianti = toUnwrappedModel(varianti);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(varianti)) {
                varianti = (Varianti) session.get(VariantiImpl.class,
                        varianti.getPrimaryKeyObj());
            }

            if (varianti != null) {
                session.delete(varianti);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (varianti != null) {
            clearCache(varianti);
        }

        return varianti;
    }

    @Override
    public Varianti updateImpl(it.bysoftware.ct.model.Varianti varianti)
        throws SystemException {
        varianti = toUnwrappedModel(varianti);

        boolean isNew = varianti.isNew();

        VariantiModelImpl variantiModelImpl = (VariantiModelImpl) varianti;

        Session session = null;

        try {
            session = openSession();

            if (varianti.isNew()) {
                session.save(varianti);

                varianti.setNew(false);
            } else {
                session.merge(varianti);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !VariantiModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((variantiModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TIPOVARIANTE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        variantiModelImpl.getOriginalTipoVariante()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TIPOVARIANTE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TIPOVARIANTE,
                    args);

                args = new Object[] { variantiModelImpl.getTipoVariante() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TIPOVARIANTE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TIPOVARIANTE,
                    args);
            }
        }

        EntityCacheUtil.putResult(VariantiModelImpl.ENTITY_CACHE_ENABLED,
            VariantiImpl.class, varianti.getPrimaryKey(), varianti);

        return varianti;
    }

    protected Varianti toUnwrappedModel(Varianti varianti) {
        if (varianti instanceof VariantiImpl) {
            return varianti;
        }

        VariantiImpl variantiImpl = new VariantiImpl();

        variantiImpl.setNew(varianti.isNew());
        variantiImpl.setPrimaryKey(varianti.getPrimaryKey());

        variantiImpl.setCodiceVariante(varianti.getCodiceVariante());
        variantiImpl.setTipoVariante(varianti.getTipoVariante());
        variantiImpl.setDescrizione(varianti.getDescrizione());

        return variantiImpl;
    }

    /**
     * Returns the varianti with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the varianti
     * @return the varianti
     * @throws it.bysoftware.ct.NoSuchVariantiException if a varianti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Varianti findByPrimaryKey(Serializable primaryKey)
        throws NoSuchVariantiException, SystemException {
        Varianti varianti = fetchByPrimaryKey(primaryKey);

        if (varianti == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchVariantiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return varianti;
    }

    /**
     * Returns the varianti with the primary key or throws a {@link it.bysoftware.ct.NoSuchVariantiException} if it could not be found.
     *
     * @param variantiPK the primary key of the varianti
     * @return the varianti
     * @throws it.bysoftware.ct.NoSuchVariantiException if a varianti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Varianti findByPrimaryKey(VariantiPK variantiPK)
        throws NoSuchVariantiException, SystemException {
        return findByPrimaryKey((Serializable) variantiPK);
    }

    /**
     * Returns the varianti with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the varianti
     * @return the varianti, or <code>null</code> if a varianti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Varianti fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Varianti varianti = (Varianti) EntityCacheUtil.getResult(VariantiModelImpl.ENTITY_CACHE_ENABLED,
                VariantiImpl.class, primaryKey);

        if (varianti == _nullVarianti) {
            return null;
        }

        if (varianti == null) {
            Session session = null;

            try {
                session = openSession();

                varianti = (Varianti) session.get(VariantiImpl.class, primaryKey);

                if (varianti != null) {
                    cacheResult(varianti);
                } else {
                    EntityCacheUtil.putResult(VariantiModelImpl.ENTITY_CACHE_ENABLED,
                        VariantiImpl.class, primaryKey, _nullVarianti);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(VariantiModelImpl.ENTITY_CACHE_ENABLED,
                    VariantiImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return varianti;
    }

    /**
     * Returns the varianti with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param variantiPK the primary key of the varianti
     * @return the varianti, or <code>null</code> if a varianti with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Varianti fetchByPrimaryKey(VariantiPK variantiPK)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) variantiPK);
    }

    /**
     * Returns all the variantis.
     *
     * @return the variantis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Varianti> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the variantis.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VariantiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of variantis
     * @param end the upper bound of the range of variantis (not inclusive)
     * @return the range of variantis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Varianti> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the variantis.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.VariantiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of variantis
     * @param end the upper bound of the range of variantis (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of variantis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Varianti> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Varianti> list = (List<Varianti>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_VARIANTI);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_VARIANTI;

                if (pagination) {
                    sql = sql.concat(VariantiModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Varianti>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Varianti>(list);
                } else {
                    list = (List<Varianti>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the variantis from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Varianti varianti : findAll()) {
            remove(varianti);
        }
    }

    /**
     * Returns the number of variantis.
     *
     * @return the number of variantis
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_VARIANTI);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the varianti persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.Varianti")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Varianti>> listenersList = new ArrayList<ModelListener<Varianti>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Varianti>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(VariantiImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
