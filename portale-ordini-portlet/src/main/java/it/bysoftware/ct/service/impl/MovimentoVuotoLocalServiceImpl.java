package it.bysoftware.ct.service.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Junction;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;

import it.bysoftware.ct.model.MovimentoVuoto;
import it.bysoftware.ct.service.MovimentoVuotoLocalServiceUtil;
import it.bysoftware.ct.service.base.MovimentoVuotoLocalServiceBaseImpl;

/**
 * The implementation of the movimento vuoto local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 *  are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.MovimentoVuotoLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.MovimentoVuotoLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.MovimentoVuotoLocalServiceUtil
 */
public class MovimentoVuotoLocalServiceImpl extends
        MovimentoVuotoLocalServiceBaseImpl {
    
    /**
     * Query to retrieve the stock value of each returnable item for the
     * specified partner. 
     */
    private static final String QUERY1 = "SELECT codice_vuoto,"
            + " SUM(quantita * tipo_movimento) AS s FROM _movimenti_vuoti WHERE"
            + " data_movimento < ? AND codice_sogetto = ? GROUP BY"
            + " codice_vuoto {0}";
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil
            .getLog(MovimentoVuotoLocalServiceImpl.class);
    
    @Override
    public final List<MovimentoVuoto> findMovementiByCodiceVuoto(
            final String c) {
        List<MovimentoVuoto> list = null;
        try {
            list = this.movimentoVuotoPersistence.findByCodiceVuoto(c);
        } catch (SystemException ex) {
            this.logger.error(ex.getMessage());
            if (this.logger.isDebugEnabled()) {
                ex.printStackTrace();
            }
        }
        return list;
    }
    
    @Override
    public final List<MovimentoVuoto> findMovementiByCodiceSoggetto(
            final String c) {
        List<MovimentoVuoto> list = null;
        try {
            list = this.movimentoVuotoPersistence.findByCodiceSoggetto(c);
        } catch (SystemException ex) {
            this.logger.error(ex.getMessage());
            if (this.logger.isDebugEnabled()) {
                ex.printStackTrace();
            }
        }
        return list;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<Object[]> getPartnerReturnablesStock(final String partner,
            final Date date, final String code) {
        Object[] filters = new String[] { "" };
        if (code != null) {
            filters[0] = "HAVING codice_vuoto = ?";
        }
        String q = MessageFormat.format(MovimentoVuotoLocalServiceImpl.QUERY1,
                filters);
        Session session = this.movimentoVuotoPersistence.openSession();
        
        this.logger.debug(q);
        SQLQuery query = session.createSQLQuery(q);
        QueryPos pos = QueryPos.getInstance(query);
        pos.add(date);
        pos.add(partner);
        if (code != null) {
            pos.add(code);
        }
        return query.list();
    }

    @Override
    public final List<MovimentoVuoto> findMovementiByVuotoSoggettoDataTipo(
            final String itemCode, final String parternCode, final Date date,
            final int type) {
        List<MovimentoVuoto> list = new ArrayList<MovimentoVuoto>();
        try {
            list.addAll(this.movimentoVuotoPersistence.
                    findByfindByVuotoSoggettoData(itemCode, parternCode, date,
                            type));
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        return list;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<MovimentoVuoto> findMovementiByCodiceSoggetto(
            final String c, final Date from, final Date to, final String code,
            final int start, final int end, final OrderByComparator comp) {
        List<MovimentoVuoto> list = null;
        
        DynamicQuery dynamicQuery = this.buildDynamicQuery(c, from, to, code);
        try {
            return MovimentoVuotoLocalServiceUtil.dynamicQuery(dynamicQuery,
                    start, end, comp);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            return list;
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<MovimentoVuoto> findMovementiByCodiceSoggetto(
            final String c, final int type, final Date from, final Date to, 
            final int start, final int end, final OrderByComparator comp) {
        List<MovimentoVuoto> list = null;
        DynamicQuery dynamicQuery = this.buildDynamicQuery(c, from, to, type);
        try {
            return MovimentoVuotoLocalServiceUtil.dynamicQuery(dynamicQuery,
                    start, end, comp);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            return list;
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final int getNumber(final String code, final Date date) {
        int result = 0;
        DynamicQuery userQuery = DynamicQueryFactoryUtil.forClass(MovimentoVuoto.class,
                getClassLoader());
        userQuery.add(PropertyFactoryUtil.forName("dataMovimento").eq(date));
        userQuery.add(PropertyFactoryUtil.forName("codiceSoggetto").eq(code));
        Projection projection = PropertyFactoryUtil.forName("uuid").max();
        userQuery.setProjection(projection);
        try {
            List<Object> list = MovimentoVuotoLocalServiceUtil.dynamicQuery(userQuery);
            if (Validator.isNumber(String.valueOf(list.get(0)))) {
                result = Integer.parseInt(String.valueOf(list.get(0)));
            } else {
                result = 0;
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        return result + 1;
    }

    /**
     * Create query with the specified filters.
     * 
     * @param c
     *            partner code
     * @param from
     *            date from
     * @param to
     *            date to
     * @param code
     *            returnable item code
     * @return {@link DynamicQuery} with the specified filters
     */
    private DynamicQuery buildDynamicQuery(final String c, final Date from,
            final Date to, final String code) {
        Junction junction = RestrictionsFactoryUtil.conjunction();

        if (Validator.isNotNull(c)) {
            Property property = PropertyFactoryUtil.forName("codiceSoggetto");
            junction.add(property.eq(c));
        }
        if (Validator.isNotNull(from) && Validator.isNotNull(to)) {
            Property property = PropertyFactoryUtil.forName("dataMovimento");
            junction.add(property.ge(from));
            junction.add(property.le(to));
        } else if (Validator.isNotNull(from)) {
            Property property = PropertyFactoryUtil.forName("dataMovimento");
            junction.add(property.ge(from));
        } else {
            Property property = PropertyFactoryUtil.forName("dataMovimento");
            junction.add(property.le(to));
        }
        if (Validator.isNotNull(code)) {
            Property property = PropertyFactoryUtil.forName("codiceVuoto");
            junction.add(property.eq(code));
        }

        Property property = PropertyFactoryUtil.forName("quantita");
        junction.add(property.gt(0D));
        
        DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
                MovimentoVuoto.class, getClassLoader());
        return dynamicQuery.add(junction);
    }
    
    private DynamicQuery buildDynamicQuery(final String c, final Date from,
            final Date to, final int type) {
        Junction junction = RestrictionsFactoryUtil.conjunction();

        if (Validator.isNotNull(c)) {
            Property property = PropertyFactoryUtil.forName("codiceSoggetto");
            junction.add(property.eq(c));
        }
        if (Validator.isNotNull(from) && Validator.isNotNull(to)) {
            Property property = PropertyFactoryUtil.forName("dataMovimento");
            junction.add(property.ge(from));
            junction.add(property.le(to));
        } else if (Validator.isNotNull(from)) {
            Property property = PropertyFactoryUtil.forName("dataMovimento");
            junction.add(property.ge(from));
        } else {
            Property property = PropertyFactoryUtil.forName("dataMovimento");
            junction.add(property.le(to));
        }
        if (Validator.isNotNull(type)) {
            Property property = PropertyFactoryUtil.forName("tipoMovimento");
            junction.add(property.eq(type));
        }

        DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
                MovimentoVuoto.class, getClassLoader());
        return dynamicQuery.add(junction);
    }
}
