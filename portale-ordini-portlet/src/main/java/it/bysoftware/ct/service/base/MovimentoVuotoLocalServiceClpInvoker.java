package it.bysoftware.ct.service.base;

import it.bysoftware.ct.service.MovimentoVuotoLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Mario Torrisi
 * @generated
 */
public class MovimentoVuotoLocalServiceClpInvoker {
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName2;
    private String[] _methodParameterTypes2;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName238;
    private String[] _methodParameterTypes238;
    private String _methodName239;
    private String[] _methodParameterTypes239;
    private String _methodName244;
    private String[] _methodParameterTypes244;
    private String _methodName245;
    private String[] _methodParameterTypes245;
    private String _methodName246;
    private String[] _methodParameterTypes246;
    private String _methodName247;
    private String[] _methodParameterTypes247;
    private String _methodName248;
    private String[] _methodParameterTypes248;
    private String _methodName249;
    private String[] _methodParameterTypes249;
    private String _methodName250;
    private String[] _methodParameterTypes250;

    public MovimentoVuotoLocalServiceClpInvoker() {
        _methodName0 = "addMovimentoVuoto";

        _methodParameterTypes0 = new String[] {
                "it.bysoftware.ct.model.MovimentoVuoto"
            };

        _methodName1 = "createMovimentoVuoto";

        _methodParameterTypes1 = new String[] { "long" };

        _methodName2 = "deleteMovimentoVuoto";

        _methodParameterTypes2 = new String[] { "long" };

        _methodName3 = "deleteMovimentoVuoto";

        _methodParameterTypes3 = new String[] {
                "it.bysoftware.ct.model.MovimentoVuoto"
            };

        _methodName4 = "dynamicQuery";

        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "dynamicQuery";

        _methodParameterTypes5 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName6 = "dynamicQuery";

        _methodParameterTypes6 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
            };

        _methodName7 = "dynamicQuery";

        _methodParameterTypes7 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName8 = "dynamicQueryCount";

        _methodParameterTypes8 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName9 = "dynamicQueryCount";

        _methodParameterTypes9 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery",
                "com.liferay.portal.kernel.dao.orm.Projection"
            };

        _methodName10 = "fetchMovimentoVuoto";

        _methodParameterTypes10 = new String[] { "long" };

        _methodName11 = "getMovimentoVuoto";

        _methodParameterTypes11 = new String[] { "long" };

        _methodName12 = "getPersistedModel";

        _methodParameterTypes12 = new String[] { "java.io.Serializable" };

        _methodName13 = "getMovimentoVuotos";

        _methodParameterTypes13 = new String[] { "int", "int" };

        _methodName14 = "getMovimentoVuotosCount";

        _methodParameterTypes14 = new String[] {  };

        _methodName15 = "updateMovimentoVuoto";

        _methodParameterTypes15 = new String[] {
                "it.bysoftware.ct.model.MovimentoVuoto"
            };

        _methodName238 = "getBeanIdentifier";

        _methodParameterTypes238 = new String[] {  };

        _methodName239 = "setBeanIdentifier";

        _methodParameterTypes239 = new String[] { "java.lang.String" };

        _methodName244 = "findMovementiByCodiceVuoto";

        _methodParameterTypes244 = new String[] { "java.lang.String" };

        _methodName245 = "findMovementiByCodiceSoggetto";

        _methodParameterTypes245 = new String[] { "java.lang.String" };

        _methodName246 = "getPartnerReturnablesStock";

        _methodParameterTypes246 = new String[] {
                "java.lang.String", "java.util.Date", "java.lang.String"
            };

        _methodName247 = "findMovementiByVuotoSoggettoDataTipo";

        _methodParameterTypes247 = new String[] {
                "java.lang.String", "java.lang.String", "java.util.Date", "int"
            };

        _methodName248 = "findMovementiByCodiceSoggetto";

        _methodParameterTypes248 = new String[] {
                "java.lang.String", "java.util.Date", "java.util.Date",
                "java.lang.String", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName249 = "findMovementiByCodiceSoggetto";

        _methodParameterTypes249 = new String[] {
                "java.lang.String", "int", "java.util.Date", "java.util.Date",
                "int", "int", "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName250 = "getNumber";

        _methodParameterTypes250 = new String[] {
                "java.lang.String", "java.util.Date"
            };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName0.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.addMovimentoVuoto((it.bysoftware.ct.model.MovimentoVuoto) arguments[0]);
        }

        if (_methodName1.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.createMovimentoVuoto(((Long) arguments[0]).longValue());
        }

        if (_methodName2.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.deleteMovimentoVuoto(((Long) arguments[0]).longValue());
        }

        if (_methodName3.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.deleteMovimentoVuoto((it.bysoftware.ct.model.MovimentoVuoto) arguments[0]);
        }

        if (_methodName4.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.dynamicQuery();
        }

        if (_methodName5.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName6.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName7.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[3]);
        }

        if (_methodName8.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName9.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                (com.liferay.portal.kernel.dao.orm.Projection) arguments[1]);
        }

        if (_methodName10.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.fetchMovimentoVuoto(((Long) arguments[0]).longValue());
        }

        if (_methodName11.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.getMovimentoVuoto(((Long) arguments[0]).longValue());
        }

        if (_methodName12.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.getPersistedModel((java.io.Serializable) arguments[0]);
        }

        if (_methodName13.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.getMovimentoVuotos(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName14.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.getMovimentoVuotosCount();
        }

        if (_methodName15.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.updateMovimentoVuoto((it.bysoftware.ct.model.MovimentoVuoto) arguments[0]);
        }

        if (_methodName238.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes238, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName239.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes239, parameterTypes)) {
            MovimentoVuotoLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName244.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes244, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.findMovementiByCodiceVuoto((java.lang.String) arguments[0]);
        }

        if (_methodName245.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes245, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.findMovementiByCodiceSoggetto((java.lang.String) arguments[0]);
        }

        if (_methodName246.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes246, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.getPartnerReturnablesStock((java.lang.String) arguments[0],
                (java.util.Date) arguments[1], (java.lang.String) arguments[2]);
        }

        if (_methodName247.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes247, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.findMovementiByVuotoSoggettoDataTipo((java.lang.String) arguments[0],
                (java.lang.String) arguments[1], (java.util.Date) arguments[2],
                ((Integer) arguments[3]).intValue());
        }

        if (_methodName248.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes248, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.findMovementiByCodiceSoggetto((java.lang.String) arguments[0],
                (java.util.Date) arguments[1], (java.util.Date) arguments[2],
                (java.lang.String) arguments[3],
                ((Integer) arguments[4]).intValue(),
                ((Integer) arguments[5]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[6]);
        }

        if (_methodName249.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes249, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.findMovementiByCodiceSoggetto((java.lang.String) arguments[0],
                ((Integer) arguments[1]).intValue(),
                (java.util.Date) arguments[2], (java.util.Date) arguments[3],
                ((Integer) arguments[4]).intValue(),
                ((Integer) arguments[5]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[6]);
        }

        if (_methodName250.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes250, parameterTypes)) {
            return MovimentoVuotoLocalServiceUtil.getNumber((java.lang.String) arguments[0],
                (java.util.Date) arguments[1]);
        }

        throw new UnsupportedOperationException();
    }
}
