package it.bysoftware.ct.service.base;

import it.bysoftware.ct.service.ArticoloSocioLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Mario Torrisi
 * @generated
 */
public class ArticoloSocioLocalServiceClpInvoker {
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName2;
    private String[] _methodParameterTypes2;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName238;
    private String[] _methodParameterTypes238;
    private String _methodName239;
    private String[] _methodParameterTypes239;
    private String _methodName244;
    private String[] _methodParameterTypes244;
    private String _methodName245;
    private String[] _methodParameterTypes245;
    private String _methodName246;
    private String[] _methodParameterTypes246;
    private String _methodName247;
    private String[] _methodParameterTypes247;
    private String _methodName248;
    private String[] _methodParameterTypes248;

    public ArticoloSocioLocalServiceClpInvoker() {
        _methodName0 = "addArticoloSocio";

        _methodParameterTypes0 = new String[] {
                "it.bysoftware.ct.model.ArticoloSocio"
            };

        _methodName1 = "createArticoloSocio";

        _methodParameterTypes1 = new String[] {
                "it.bysoftware.ct.service.persistence.ArticoloSocioPK"
            };

        _methodName2 = "deleteArticoloSocio";

        _methodParameterTypes2 = new String[] {
                "it.bysoftware.ct.service.persistence.ArticoloSocioPK"
            };

        _methodName3 = "deleteArticoloSocio";

        _methodParameterTypes3 = new String[] {
                "it.bysoftware.ct.model.ArticoloSocio"
            };

        _methodName4 = "dynamicQuery";

        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "dynamicQuery";

        _methodParameterTypes5 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName6 = "dynamicQuery";

        _methodParameterTypes6 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
            };

        _methodName7 = "dynamicQuery";

        _methodParameterTypes7 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName8 = "dynamicQueryCount";

        _methodParameterTypes8 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName9 = "dynamicQueryCount";

        _methodParameterTypes9 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery",
                "com.liferay.portal.kernel.dao.orm.Projection"
            };

        _methodName10 = "fetchArticoloSocio";

        _methodParameterTypes10 = new String[] {
                "it.bysoftware.ct.service.persistence.ArticoloSocioPK"
            };

        _methodName11 = "getArticoloSocio";

        _methodParameterTypes11 = new String[] {
                "it.bysoftware.ct.service.persistence.ArticoloSocioPK"
            };

        _methodName12 = "getPersistedModel";

        _methodParameterTypes12 = new String[] { "java.io.Serializable" };

        _methodName13 = "getArticoloSocios";

        _methodParameterTypes13 = new String[] { "int", "int" };

        _methodName14 = "getArticoloSociosCount";

        _methodParameterTypes14 = new String[] {  };

        _methodName15 = "updateArticoloSocio";

        _methodParameterTypes15 = new String[] {
                "it.bysoftware.ct.model.ArticoloSocio"
            };

        _methodName238 = "getBeanIdentifier";

        _methodParameterTypes238 = new String[] {  };

        _methodName239 = "setBeanIdentifier";

        _methodParameterTypes239 = new String[] { "java.lang.String" };

        _methodName244 = "findPartnerItems";

        _methodParameterTypes244 = new String[] {
                "java.lang.String", "java.lang.String", "java.lang.String"
            };

        _methodName245 = "findCompanyItem";

        _methodParameterTypes245 = new String[] {
                "java.lang.String", "java.lang.String", "java.lang.String"
            };

        _methodName246 = "findAllPartnerItems";

        _methodParameterTypes246 = new String[] { "java.lang.String" };

        _methodName247 = "findPartnerItems";

        _methodParameterTypes247 = new String[] {
                "java.lang.String", "java.lang.String", "java.lang.String",
                "java.lang.String", "java.lang.String", "boolean", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName248 = "findPartnerItemsCount";

        _methodParameterTypes248 = new String[] {
                "java.lang.String", "java.lang.String", "java.lang.String",
                "java.lang.String", "java.lang.String", "boolean"
            };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName0.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.addArticoloSocio((it.bysoftware.ct.model.ArticoloSocio) arguments[0]);
        }

        if (_methodName1.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.createArticoloSocio((it.bysoftware.ct.service.persistence.ArticoloSocioPK) arguments[0]);
        }

        if (_methodName2.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.deleteArticoloSocio((it.bysoftware.ct.service.persistence.ArticoloSocioPK) arguments[0]);
        }

        if (_methodName3.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.deleteArticoloSocio((it.bysoftware.ct.model.ArticoloSocio) arguments[0]);
        }

        if (_methodName4.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.dynamicQuery();
        }

        if (_methodName5.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName6.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName7.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[3]);
        }

        if (_methodName8.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName9.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                (com.liferay.portal.kernel.dao.orm.Projection) arguments[1]);
        }

        if (_methodName10.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.fetchArticoloSocio((it.bysoftware.ct.service.persistence.ArticoloSocioPK) arguments[0]);
        }

        if (_methodName11.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.getArticoloSocio((it.bysoftware.ct.service.persistence.ArticoloSocioPK) arguments[0]);
        }

        if (_methodName12.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.getPersistedModel((java.io.Serializable) arguments[0]);
        }

        if (_methodName13.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.getArticoloSocios(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName14.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.getArticoloSociosCount();
        }

        if (_methodName15.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.updateArticoloSocio((it.bysoftware.ct.model.ArticoloSocio) arguments[0]);
        }

        if (_methodName238.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes238, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName239.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes239, parameterTypes)) {
            ArticoloSocioLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName244.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes244, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.findPartnerItems((java.lang.String) arguments[0],
                (java.lang.String) arguments[1], (java.lang.String) arguments[2]);
        }

        if (_methodName245.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes245, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.findCompanyItem((java.lang.String) arguments[0],
                (java.lang.String) arguments[1], (java.lang.String) arguments[2]);
        }

        if (_methodName246.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes246, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.findAllPartnerItems((java.lang.String) arguments[0]);
        }

        if (_methodName247.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes247, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.findPartnerItems((java.lang.String) arguments[0],
                (java.lang.String) arguments[1],
                (java.lang.String) arguments[2],
                (java.lang.String) arguments[3],
                (java.lang.String) arguments[4],
                ((Boolean) arguments[5]).booleanValue(),
                ((Integer) arguments[6]).intValue(),
                ((Integer) arguments[7]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[8]);
        }

        if (_methodName248.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes248, parameterTypes)) {
            return ArticoloSocioLocalServiceUtil.findPartnerItemsCount((java.lang.String) arguments[0],
                (java.lang.String) arguments[1],
                (java.lang.String) arguments[2],
                (java.lang.String) arguments[3],
                (java.lang.String) arguments[4],
                ((Boolean) arguments[5]).booleanValue());
        }

        throw new UnsupportedOperationException();
    }
}
