package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.service.base.CentroLocalServiceBaseImpl;

/**
 * The implementation of the centro local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.CentroLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.CentroLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.CentroLocalServiceUtil
 */
public class CentroLocalServiceImpl extends CentroLocalServiceBaseImpl {
}
