package it.bysoftware.ct.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

import it.bysoftware.ct.model.Articoli;
import it.bysoftware.ct.service.base.ArticoliLocalServiceBaseImpl;

/**
 * The implementation of the articoli local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * areadded, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.ArticoliLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.ArticoliLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.ArticoliLocalServiceUtil
 */
public class ArticoliLocalServiceImpl extends ArticoliLocalServiceBaseImpl {
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(ArticoliLocalServiceImpl.class);

    @Override
    public final List<Articoli> findItems(final String code) {
        List<Articoli> result = null;
        
        try {
            result = this.articoliPersistence.findByCodice(StringPool.PERCENT 
                    + code + StringPool.PERCENT);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            result = new ArrayList<Articoli>();
        }
        
        return result;
    }
}
