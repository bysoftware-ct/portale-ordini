package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchSchedaPagamentoException;
import it.bysoftware.ct.model.SchedaPagamento;
import it.bysoftware.ct.model.impl.SchedaPagamentoImpl;
import it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl;
import it.bysoftware.ct.service.persistence.SchedaPagamentoPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the scheda pagamento service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see SchedaPagamentoPersistence
 * @see SchedaPagamentoUtil
 * @generated
 */
public class SchedaPagamentoPersistenceImpl extends BasePersistenceImpl<SchedaPagamento>
    implements SchedaPagamentoPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link SchedaPagamentoUtil} to access the scheda pagamento persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = SchedaPagamentoImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(SchedaPagamentoModelImpl.ENTITY_CACHE_ENABLED,
            SchedaPagamentoModelImpl.FINDER_CACHE_ENABLED,
            SchedaPagamentoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(SchedaPagamentoModelImpl.ENTITY_CACHE_ENABLED,
            SchedaPagamentoModelImpl.FINDER_CACHE_ENABLED,
            SchedaPagamentoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(SchedaPagamentoModelImpl.ENTITY_CACHE_ENABLED,
            SchedaPagamentoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICEFORNITOREANNOIDPAGAMENTO =
        new FinderPath(SchedaPagamentoModelImpl.ENTITY_CACHE_ENABLED,
            SchedaPagamentoModelImpl.FINDER_CACHE_ENABLED,
            SchedaPagamentoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByCodiceFornitoreAnnoIdPagamento",
            new String[] {
                String.class.getName(), Integer.class.getName(),
                Integer.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEFORNITOREANNOIDPAGAMENTO =
        new FinderPath(SchedaPagamentoModelImpl.ENTITY_CACHE_ENABLED,
            SchedaPagamentoModelImpl.FINDER_CACHE_ENABLED,
            SchedaPagamentoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByCodiceFornitoreAnnoIdPagamento",
            new String[] {
                String.class.getName(), Integer.class.getName(),
                Integer.class.getName()
            },
            SchedaPagamentoModelImpl.CODICESOGGETTO_COLUMN_BITMASK |
            SchedaPagamentoModelImpl.ANNOPAGAMENTO_COLUMN_BITMASK |
            SchedaPagamentoModelImpl.IDPAGAMENTO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_CODICEFORNITOREANNOIDPAGAMENTO =
        new FinderPath(SchedaPagamentoModelImpl.ENTITY_CACHE_ENABLED,
            SchedaPagamentoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByCodiceFornitoreAnnoIdPagamento",
            new String[] {
                String.class.getName(), Integer.class.getName(),
                Integer.class.getName()
            });
    private static final String _FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_CODICESOGGETTO_1 =
        "schedaPagamento.id.codiceSoggetto IS NULL AND ";
    private static final String _FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_CODICESOGGETTO_2 =
        "schedaPagamento.id.codiceSoggetto = ? AND ";
    private static final String _FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_CODICESOGGETTO_3 =
        "(schedaPagamento.id.codiceSoggetto IS NULL OR schedaPagamento.id.codiceSoggetto = '') AND ";
    private static final String _FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_ANNOPAGAMENTO_2 =
        "schedaPagamento.annoPagamento = ? AND ";
    private static final String _FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_IDPAGAMENTO_2 =
        "schedaPagamento.idPagamento = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICEFORNITOREANNOIDPAGAMENTOSTATO =
        new FinderPath(SchedaPagamentoModelImpl.ENTITY_CACHE_ENABLED,
            SchedaPagamentoModelImpl.FINDER_CACHE_ENABLED,
            SchedaPagamentoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByCodiceFornitoreAnnoIdPagamentoStato",
            new String[] {
                String.class.getName(), Integer.class.getName(),
                Integer.class.getName(), Integer.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEFORNITOREANNOIDPAGAMENTOSTATO =
        new FinderPath(SchedaPagamentoModelImpl.ENTITY_CACHE_ENABLED,
            SchedaPagamentoModelImpl.FINDER_CACHE_ENABLED,
            SchedaPagamentoImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByCodiceFornitoreAnnoIdPagamentoStato",
            new String[] {
                String.class.getName(), Integer.class.getName(),
                Integer.class.getName(), Integer.class.getName()
            },
            SchedaPagamentoModelImpl.CODICESOGGETTO_COLUMN_BITMASK |
            SchedaPagamentoModelImpl.ANNOPAGAMENTO_COLUMN_BITMASK |
            SchedaPagamentoModelImpl.IDPAGAMENTO_COLUMN_BITMASK |
            SchedaPagamentoModelImpl.STATO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_CODICEFORNITOREANNOIDPAGAMENTOSTATO =
        new FinderPath(SchedaPagamentoModelImpl.ENTITY_CACHE_ENABLED,
            SchedaPagamentoModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByCodiceFornitoreAnnoIdPagamentoStato",
            new String[] {
                String.class.getName(), Integer.class.getName(),
                Integer.class.getName(), Integer.class.getName()
            });
    private static final String _FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_CODICESOGGETTO_1 =
        "schedaPagamento.id.codiceSoggetto IS NULL AND ";
    private static final String _FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_CODICESOGGETTO_2 =
        "schedaPagamento.id.codiceSoggetto = ? AND ";
    private static final String _FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_CODICESOGGETTO_3 =
        "(schedaPagamento.id.codiceSoggetto IS NULL OR schedaPagamento.id.codiceSoggetto = '') AND ";
    private static final String _FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_ANNOPAGAMENTO_2 =
        "schedaPagamento.annoPagamento = ? AND ";
    private static final String _FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_IDPAGAMENTO_2 =
        "schedaPagamento.idPagamento = ? AND ";
    private static final String _FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_STATO_2 =
        "schedaPagamento.stato = ?";
    private static final String _SQL_SELECT_SCHEDAPAGAMENTO = "SELECT schedaPagamento FROM SchedaPagamento schedaPagamento";
    private static final String _SQL_SELECT_SCHEDAPAGAMENTO_WHERE = "SELECT schedaPagamento FROM SchedaPagamento schedaPagamento WHERE ";
    private static final String _SQL_COUNT_SCHEDAPAGAMENTO = "SELECT COUNT(schedaPagamento) FROM SchedaPagamento schedaPagamento";
    private static final String _SQL_COUNT_SCHEDAPAGAMENTO_WHERE = "SELECT COUNT(schedaPagamento) FROM SchedaPagamento schedaPagamento WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "schedaPagamento.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No SchedaPagamento exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No SchedaPagamento exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(SchedaPagamentoPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "esercizioRegistrazione", "codiceSoggetto", "numeroPartita",
                "numeroScadenza", "tipoSoggetto", "esercizioRegistrazioneComp",
                "codiceSoggettoComp", "numeroPartitaComp", "numeroScadenzaComp",
                "tipoSoggettoComp", "idPagamento", "annoPagamento"
            });
    private static SchedaPagamento _nullSchedaPagamento = new SchedaPagamentoImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<SchedaPagamento> toCacheModel() {
                return _nullSchedaPagamentoCacheModel;
            }
        };

    private static CacheModel<SchedaPagamento> _nullSchedaPagamentoCacheModel = new CacheModel<SchedaPagamento>() {
            @Override
            public SchedaPagamento toEntityModel() {
                return _nullSchedaPagamento;
            }
        };

    public SchedaPagamentoPersistenceImpl() {
        setModelClass(SchedaPagamento.class);
    }

    /**
     * Returns all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @return the matching scheda pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SchedaPagamento> findByCodiceFornitoreAnnoIdPagamento(
        String codiceSoggetto, int annoPagamento, int idPagamento)
        throws SystemException {
        return findByCodiceFornitoreAnnoIdPagamento(codiceSoggetto,
            annoPagamento, idPagamento, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param start the lower bound of the range of scheda pagamentos
     * @param end the upper bound of the range of scheda pagamentos (not inclusive)
     * @return the range of matching scheda pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SchedaPagamento> findByCodiceFornitoreAnnoIdPagamento(
        String codiceSoggetto, int annoPagamento, int idPagamento, int start,
        int end) throws SystemException {
        return findByCodiceFornitoreAnnoIdPagamento(codiceSoggetto,
            annoPagamento, idPagamento, start, end, null);
    }

    /**
     * Returns an ordered range of all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param start the lower bound of the range of scheda pagamentos
     * @param end the upper bound of the range of scheda pagamentos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching scheda pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SchedaPagamento> findByCodiceFornitoreAnnoIdPagamento(
        String codiceSoggetto, int annoPagamento, int idPagamento, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEFORNITOREANNOIDPAGAMENTO;
            finderArgs = new Object[] { codiceSoggetto, annoPagamento, idPagamento };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICEFORNITOREANNOIDPAGAMENTO;
            finderArgs = new Object[] {
                    codiceSoggetto, annoPagamento, idPagamento,
                    
                    start, end, orderByComparator
                };
        }

        List<SchedaPagamento> list = (List<SchedaPagamento>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (SchedaPagamento schedaPagamento : list) {
                if (!Validator.equals(codiceSoggetto,
                            schedaPagamento.getCodiceSoggetto()) ||
                        (annoPagamento != schedaPagamento.getAnnoPagamento()) ||
                        (idPagamento != schedaPagamento.getIdPagamento())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(5 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(5);
            }

            query.append(_SQL_SELECT_SCHEDAPAGAMENTO_WHERE);

            boolean bindCodiceSoggetto = false;

            if (codiceSoggetto == null) {
                query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_CODICESOGGETTO_1);
            } else if (codiceSoggetto.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_CODICESOGGETTO_3);
            } else {
                bindCodiceSoggetto = true;

                query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_CODICESOGGETTO_2);
            }

            query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_ANNOPAGAMENTO_2);

            query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_IDPAGAMENTO_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(SchedaPagamentoModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceSoggetto) {
                    qPos.add(codiceSoggetto);
                }

                qPos.add(annoPagamento);

                qPos.add(idPagamento);

                if (!pagination) {
                    list = (List<SchedaPagamento>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<SchedaPagamento>(list);
                } else {
                    list = (List<SchedaPagamento>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching scheda pagamento
     * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a matching scheda pagamento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SchedaPagamento findByCodiceFornitoreAnnoIdPagamento_First(
        String codiceSoggetto, int annoPagamento, int idPagamento,
        OrderByComparator orderByComparator)
        throws NoSuchSchedaPagamentoException, SystemException {
        SchedaPagamento schedaPagamento = fetchByCodiceFornitoreAnnoIdPagamento_First(codiceSoggetto,
                annoPagamento, idPagamento, orderByComparator);

        if (schedaPagamento != null) {
            return schedaPagamento;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceSoggetto=");
        msg.append(codiceSoggetto);

        msg.append(", annoPagamento=");
        msg.append(annoPagamento);

        msg.append(", idPagamento=");
        msg.append(idPagamento);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchSchedaPagamentoException(msg.toString());
    }

    /**
     * Returns the first scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching scheda pagamento, or <code>null</code> if a matching scheda pagamento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SchedaPagamento fetchByCodiceFornitoreAnnoIdPagamento_First(
        String codiceSoggetto, int annoPagamento, int idPagamento,
        OrderByComparator orderByComparator) throws SystemException {
        List<SchedaPagamento> list = findByCodiceFornitoreAnnoIdPagamento(codiceSoggetto,
                annoPagamento, idPagamento, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching scheda pagamento
     * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a matching scheda pagamento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SchedaPagamento findByCodiceFornitoreAnnoIdPagamento_Last(
        String codiceSoggetto, int annoPagamento, int idPagamento,
        OrderByComparator orderByComparator)
        throws NoSuchSchedaPagamentoException, SystemException {
        SchedaPagamento schedaPagamento = fetchByCodiceFornitoreAnnoIdPagamento_Last(codiceSoggetto,
                annoPagamento, idPagamento, orderByComparator);

        if (schedaPagamento != null) {
            return schedaPagamento;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceSoggetto=");
        msg.append(codiceSoggetto);

        msg.append(", annoPagamento=");
        msg.append(annoPagamento);

        msg.append(", idPagamento=");
        msg.append(idPagamento);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchSchedaPagamentoException(msg.toString());
    }

    /**
     * Returns the last scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching scheda pagamento, or <code>null</code> if a matching scheda pagamento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SchedaPagamento fetchByCodiceFornitoreAnnoIdPagamento_Last(
        String codiceSoggetto, int annoPagamento, int idPagamento,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByCodiceFornitoreAnnoIdPagamento(codiceSoggetto,
                annoPagamento, idPagamento);

        if (count == 0) {
            return null;
        }

        List<SchedaPagamento> list = findByCodiceFornitoreAnnoIdPagamento(codiceSoggetto,
                annoPagamento, idPagamento, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the scheda pagamentos before and after the current scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
     *
     * @param schedaPagamentoPK the primary key of the current scheda pagamento
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next scheda pagamento
     * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a scheda pagamento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SchedaPagamento[] findByCodiceFornitoreAnnoIdPagamento_PrevAndNext(
        SchedaPagamentoPK schedaPagamentoPK, String codiceSoggetto,
        int annoPagamento, int idPagamento, OrderByComparator orderByComparator)
        throws NoSuchSchedaPagamentoException, SystemException {
        SchedaPagamento schedaPagamento = findByPrimaryKey(schedaPagamentoPK);

        Session session = null;

        try {
            session = openSession();

            SchedaPagamento[] array = new SchedaPagamentoImpl[3];

            array[0] = getByCodiceFornitoreAnnoIdPagamento_PrevAndNext(session,
                    schedaPagamento, codiceSoggetto, annoPagamento,
                    idPagamento, orderByComparator, true);

            array[1] = schedaPagamento;

            array[2] = getByCodiceFornitoreAnnoIdPagamento_PrevAndNext(session,
                    schedaPagamento, codiceSoggetto, annoPagamento,
                    idPagamento, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected SchedaPagamento getByCodiceFornitoreAnnoIdPagamento_PrevAndNext(
        Session session, SchedaPagamento schedaPagamento,
        String codiceSoggetto, int annoPagamento, int idPagamento,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_SCHEDAPAGAMENTO_WHERE);

        boolean bindCodiceSoggetto = false;

        if (codiceSoggetto == null) {
            query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_CODICESOGGETTO_1);
        } else if (codiceSoggetto.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_CODICESOGGETTO_3);
        } else {
            bindCodiceSoggetto = true;

            query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_CODICESOGGETTO_2);
        }

        query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_ANNOPAGAMENTO_2);

        query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_IDPAGAMENTO_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(SchedaPagamentoModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindCodiceSoggetto) {
            qPos.add(codiceSoggetto);
        }

        qPos.add(annoPagamento);

        qPos.add(idPagamento);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(schedaPagamento);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<SchedaPagamento> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; from the database.
     *
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByCodiceFornitoreAnnoIdPagamento(String codiceSoggetto,
        int annoPagamento, int idPagamento) throws SystemException {
        for (SchedaPagamento schedaPagamento : findByCodiceFornitoreAnnoIdPagamento(
                codiceSoggetto, annoPagamento, idPagamento, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(schedaPagamento);
        }
    }

    /**
     * Returns the number of scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @return the number of matching scheda pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByCodiceFornitoreAnnoIdPagamento(String codiceSoggetto,
        int annoPagamento, int idPagamento) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_CODICEFORNITOREANNOIDPAGAMENTO;

        Object[] finderArgs = new Object[] {
                codiceSoggetto, annoPagamento, idPagamento
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(4);

            query.append(_SQL_COUNT_SCHEDAPAGAMENTO_WHERE);

            boolean bindCodiceSoggetto = false;

            if (codiceSoggetto == null) {
                query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_CODICESOGGETTO_1);
            } else if (codiceSoggetto.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_CODICESOGGETTO_3);
            } else {
                bindCodiceSoggetto = true;

                query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_CODICESOGGETTO_2);
            }

            query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_ANNOPAGAMENTO_2);

            query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTO_IDPAGAMENTO_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceSoggetto) {
                    qPos.add(codiceSoggetto);
                }

                qPos.add(annoPagamento);

                qPos.add(idPagamento);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param stato the stato
     * @return the matching scheda pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SchedaPagamento> findByCodiceFornitoreAnnoIdPagamentoStato(
        String codiceSoggetto, int annoPagamento, int idPagamento, int stato)
        throws SystemException {
        return findByCodiceFornitoreAnnoIdPagamentoStato(codiceSoggetto,
            annoPagamento, idPagamento, stato, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param stato the stato
     * @param start the lower bound of the range of scheda pagamentos
     * @param end the upper bound of the range of scheda pagamentos (not inclusive)
     * @return the range of matching scheda pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SchedaPagamento> findByCodiceFornitoreAnnoIdPagamentoStato(
        String codiceSoggetto, int annoPagamento, int idPagamento, int stato,
        int start, int end) throws SystemException {
        return findByCodiceFornitoreAnnoIdPagamentoStato(codiceSoggetto,
            annoPagamento, idPagamento, stato, start, end, null);
    }

    /**
     * Returns an ordered range of all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param stato the stato
     * @param start the lower bound of the range of scheda pagamentos
     * @param end the upper bound of the range of scheda pagamentos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching scheda pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SchedaPagamento> findByCodiceFornitoreAnnoIdPagamentoStato(
        String codiceSoggetto, int annoPagamento, int idPagamento, int stato,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEFORNITOREANNOIDPAGAMENTOSTATO;
            finderArgs = new Object[] {
                    codiceSoggetto, annoPagamento, idPagamento, stato
                };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICEFORNITOREANNOIDPAGAMENTOSTATO;
            finderArgs = new Object[] {
                    codiceSoggetto, annoPagamento, idPagamento, stato,
                    
                    start, end, orderByComparator
                };
        }

        List<SchedaPagamento> list = (List<SchedaPagamento>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (SchedaPagamento schedaPagamento : list) {
                if (!Validator.equals(codiceSoggetto,
                            schedaPagamento.getCodiceSoggetto()) ||
                        (annoPagamento != schedaPagamento.getAnnoPagamento()) ||
                        (idPagamento != schedaPagamento.getIdPagamento()) ||
                        (stato != schedaPagamento.getStato())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(6 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(6);
            }

            query.append(_SQL_SELECT_SCHEDAPAGAMENTO_WHERE);

            boolean bindCodiceSoggetto = false;

            if (codiceSoggetto == null) {
                query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_CODICESOGGETTO_1);
            } else if (codiceSoggetto.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_CODICESOGGETTO_3);
            } else {
                bindCodiceSoggetto = true;

                query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_CODICESOGGETTO_2);
            }

            query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_ANNOPAGAMENTO_2);

            query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_IDPAGAMENTO_2);

            query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_STATO_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(SchedaPagamentoModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceSoggetto) {
                    qPos.add(codiceSoggetto);
                }

                qPos.add(annoPagamento);

                qPos.add(idPagamento);

                qPos.add(stato);

                if (!pagination) {
                    list = (List<SchedaPagamento>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<SchedaPagamento>(list);
                } else {
                    list = (List<SchedaPagamento>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching scheda pagamento
     * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a matching scheda pagamento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SchedaPagamento findByCodiceFornitoreAnnoIdPagamentoStato_First(
        String codiceSoggetto, int annoPagamento, int idPagamento, int stato,
        OrderByComparator orderByComparator)
        throws NoSuchSchedaPagamentoException, SystemException {
        SchedaPagamento schedaPagamento = fetchByCodiceFornitoreAnnoIdPagamentoStato_First(codiceSoggetto,
                annoPagamento, idPagamento, stato, orderByComparator);

        if (schedaPagamento != null) {
            return schedaPagamento;
        }

        StringBundler msg = new StringBundler(10);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceSoggetto=");
        msg.append(codiceSoggetto);

        msg.append(", annoPagamento=");
        msg.append(annoPagamento);

        msg.append(", idPagamento=");
        msg.append(idPagamento);

        msg.append(", stato=");
        msg.append(stato);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchSchedaPagamentoException(msg.toString());
    }

    /**
     * Returns the first scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching scheda pagamento, or <code>null</code> if a matching scheda pagamento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SchedaPagamento fetchByCodiceFornitoreAnnoIdPagamentoStato_First(
        String codiceSoggetto, int annoPagamento, int idPagamento, int stato,
        OrderByComparator orderByComparator) throws SystemException {
        List<SchedaPagamento> list = findByCodiceFornitoreAnnoIdPagamentoStato(codiceSoggetto,
                annoPagamento, idPagamento, stato, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching scheda pagamento
     * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a matching scheda pagamento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SchedaPagamento findByCodiceFornitoreAnnoIdPagamentoStato_Last(
        String codiceSoggetto, int annoPagamento, int idPagamento, int stato,
        OrderByComparator orderByComparator)
        throws NoSuchSchedaPagamentoException, SystemException {
        SchedaPagamento schedaPagamento = fetchByCodiceFornitoreAnnoIdPagamentoStato_Last(codiceSoggetto,
                annoPagamento, idPagamento, stato, orderByComparator);

        if (schedaPagamento != null) {
            return schedaPagamento;
        }

        StringBundler msg = new StringBundler(10);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("codiceSoggetto=");
        msg.append(codiceSoggetto);

        msg.append(", annoPagamento=");
        msg.append(annoPagamento);

        msg.append(", idPagamento=");
        msg.append(idPagamento);

        msg.append(", stato=");
        msg.append(stato);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchSchedaPagamentoException(msg.toString());
    }

    /**
     * Returns the last scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching scheda pagamento, or <code>null</code> if a matching scheda pagamento could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SchedaPagamento fetchByCodiceFornitoreAnnoIdPagamentoStato_Last(
        String codiceSoggetto, int annoPagamento, int idPagamento, int stato,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByCodiceFornitoreAnnoIdPagamentoStato(codiceSoggetto,
                annoPagamento, idPagamento, stato);

        if (count == 0) {
            return null;
        }

        List<SchedaPagamento> list = findByCodiceFornitoreAnnoIdPagamentoStato(codiceSoggetto,
                annoPagamento, idPagamento, stato, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the scheda pagamentos before and after the current scheda pagamento in the ordered set where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
     *
     * @param schedaPagamentoPK the primary key of the current scheda pagamento
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param stato the stato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next scheda pagamento
     * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a scheda pagamento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SchedaPagamento[] findByCodiceFornitoreAnnoIdPagamentoStato_PrevAndNext(
        SchedaPagamentoPK schedaPagamentoPK, String codiceSoggetto,
        int annoPagamento, int idPagamento, int stato,
        OrderByComparator orderByComparator)
        throws NoSuchSchedaPagamentoException, SystemException {
        SchedaPagamento schedaPagamento = findByPrimaryKey(schedaPagamentoPK);

        Session session = null;

        try {
            session = openSession();

            SchedaPagamento[] array = new SchedaPagamentoImpl[3];

            array[0] = getByCodiceFornitoreAnnoIdPagamentoStato_PrevAndNext(session,
                    schedaPagamento, codiceSoggetto, annoPagamento,
                    idPagamento, stato, orderByComparator, true);

            array[1] = schedaPagamento;

            array[2] = getByCodiceFornitoreAnnoIdPagamentoStato_PrevAndNext(session,
                    schedaPagamento, codiceSoggetto, annoPagamento,
                    idPagamento, stato, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected SchedaPagamento getByCodiceFornitoreAnnoIdPagamentoStato_PrevAndNext(
        Session session, SchedaPagamento schedaPagamento,
        String codiceSoggetto, int annoPagamento, int idPagamento, int stato,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_SCHEDAPAGAMENTO_WHERE);

        boolean bindCodiceSoggetto = false;

        if (codiceSoggetto == null) {
            query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_CODICESOGGETTO_1);
        } else if (codiceSoggetto.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_CODICESOGGETTO_3);
        } else {
            bindCodiceSoggetto = true;

            query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_CODICESOGGETTO_2);
        }

        query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_ANNOPAGAMENTO_2);

        query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_IDPAGAMENTO_2);

        query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_STATO_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(SchedaPagamentoModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindCodiceSoggetto) {
            qPos.add(codiceSoggetto);
        }

        qPos.add(annoPagamento);

        qPos.add(idPagamento);

        qPos.add(stato);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(schedaPagamento);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<SchedaPagamento> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63; from the database.
     *
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param stato the stato
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByCodiceFornitoreAnnoIdPagamentoStato(
        String codiceSoggetto, int annoPagamento, int idPagamento, int stato)
        throws SystemException {
        for (SchedaPagamento schedaPagamento : findByCodiceFornitoreAnnoIdPagamentoStato(
                codiceSoggetto, annoPagamento, idPagamento, stato,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(schedaPagamento);
        }
    }

    /**
     * Returns the number of scheda pagamentos where codiceSoggetto = &#63; and annoPagamento = &#63; and idPagamento = &#63; and stato = &#63;.
     *
     * @param codiceSoggetto the codice soggetto
     * @param annoPagamento the anno pagamento
     * @param idPagamento the id pagamento
     * @param stato the stato
     * @return the number of matching scheda pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByCodiceFornitoreAnnoIdPagamentoStato(
        String codiceSoggetto, int annoPagamento, int idPagamento, int stato)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_CODICEFORNITOREANNOIDPAGAMENTOSTATO;

        Object[] finderArgs = new Object[] {
                codiceSoggetto, annoPagamento, idPagamento, stato
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(5);

            query.append(_SQL_COUNT_SCHEDAPAGAMENTO_WHERE);

            boolean bindCodiceSoggetto = false;

            if (codiceSoggetto == null) {
                query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_CODICESOGGETTO_1);
            } else if (codiceSoggetto.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_CODICESOGGETTO_3);
            } else {
                bindCodiceSoggetto = true;

                query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_CODICESOGGETTO_2);
            }

            query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_ANNOPAGAMENTO_2);

            query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_IDPAGAMENTO_2);

            query.append(_FINDER_COLUMN_CODICEFORNITOREANNOIDPAGAMENTOSTATO_STATO_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCodiceSoggetto) {
                    qPos.add(codiceSoggetto);
                }

                qPos.add(annoPagamento);

                qPos.add(idPagamento);

                qPos.add(stato);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the scheda pagamento in the entity cache if it is enabled.
     *
     * @param schedaPagamento the scheda pagamento
     */
    @Override
    public void cacheResult(SchedaPagamento schedaPagamento) {
        EntityCacheUtil.putResult(SchedaPagamentoModelImpl.ENTITY_CACHE_ENABLED,
            SchedaPagamentoImpl.class, schedaPagamento.getPrimaryKey(),
            schedaPagamento);

        schedaPagamento.resetOriginalValues();
    }

    /**
     * Caches the scheda pagamentos in the entity cache if it is enabled.
     *
     * @param schedaPagamentos the scheda pagamentos
     */
    @Override
    public void cacheResult(List<SchedaPagamento> schedaPagamentos) {
        for (SchedaPagamento schedaPagamento : schedaPagamentos) {
            if (EntityCacheUtil.getResult(
                        SchedaPagamentoModelImpl.ENTITY_CACHE_ENABLED,
                        SchedaPagamentoImpl.class,
                        schedaPagamento.getPrimaryKey()) == null) {
                cacheResult(schedaPagamento);
            } else {
                schedaPagamento.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all scheda pagamentos.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(SchedaPagamentoImpl.class.getName());
        }

        EntityCacheUtil.clearCache(SchedaPagamentoImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the scheda pagamento.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(SchedaPagamento schedaPagamento) {
        EntityCacheUtil.removeResult(SchedaPagamentoModelImpl.ENTITY_CACHE_ENABLED,
            SchedaPagamentoImpl.class, schedaPagamento.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<SchedaPagamento> schedaPagamentos) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (SchedaPagamento schedaPagamento : schedaPagamentos) {
            EntityCacheUtil.removeResult(SchedaPagamentoModelImpl.ENTITY_CACHE_ENABLED,
                SchedaPagamentoImpl.class, schedaPagamento.getPrimaryKey());
        }
    }

    /**
     * Creates a new scheda pagamento with the primary key. Does not add the scheda pagamento to the database.
     *
     * @param schedaPagamentoPK the primary key for the new scheda pagamento
     * @return the new scheda pagamento
     */
    @Override
    public SchedaPagamento create(SchedaPagamentoPK schedaPagamentoPK) {
        SchedaPagamento schedaPagamento = new SchedaPagamentoImpl();

        schedaPagamento.setNew(true);
        schedaPagamento.setPrimaryKey(schedaPagamentoPK);

        return schedaPagamento;
    }

    /**
     * Removes the scheda pagamento with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param schedaPagamentoPK the primary key of the scheda pagamento
     * @return the scheda pagamento that was removed
     * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a scheda pagamento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SchedaPagamento remove(SchedaPagamentoPK schedaPagamentoPK)
        throws NoSuchSchedaPagamentoException, SystemException {
        return remove((Serializable) schedaPagamentoPK);
    }

    /**
     * Removes the scheda pagamento with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the scheda pagamento
     * @return the scheda pagamento that was removed
     * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a scheda pagamento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SchedaPagamento remove(Serializable primaryKey)
        throws NoSuchSchedaPagamentoException, SystemException {
        Session session = null;

        try {
            session = openSession();

            SchedaPagamento schedaPagamento = (SchedaPagamento) session.get(SchedaPagamentoImpl.class,
                    primaryKey);

            if (schedaPagamento == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchSchedaPagamentoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(schedaPagamento);
        } catch (NoSuchSchedaPagamentoException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected SchedaPagamento removeImpl(SchedaPagamento schedaPagamento)
        throws SystemException {
        schedaPagamento = toUnwrappedModel(schedaPagamento);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(schedaPagamento)) {
                schedaPagamento = (SchedaPagamento) session.get(SchedaPagamentoImpl.class,
                        schedaPagamento.getPrimaryKeyObj());
            }

            if (schedaPagamento != null) {
                session.delete(schedaPagamento);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (schedaPagamento != null) {
            clearCache(schedaPagamento);
        }

        return schedaPagamento;
    }

    @Override
    public SchedaPagamento updateImpl(
        it.bysoftware.ct.model.SchedaPagamento schedaPagamento)
        throws SystemException {
        schedaPagamento = toUnwrappedModel(schedaPagamento);

        boolean isNew = schedaPagamento.isNew();

        SchedaPagamentoModelImpl schedaPagamentoModelImpl = (SchedaPagamentoModelImpl) schedaPagamento;

        Session session = null;

        try {
            session = openSession();

            if (schedaPagamento.isNew()) {
                session.save(schedaPagamento);

                schedaPagamento.setNew(false);
            } else {
                session.merge(schedaPagamento);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !SchedaPagamentoModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((schedaPagamentoModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEFORNITOREANNOIDPAGAMENTO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        schedaPagamentoModelImpl.getOriginalCodiceSoggetto(),
                        schedaPagamentoModelImpl.getOriginalAnnoPagamento(),
                        schedaPagamentoModelImpl.getOriginalIdPagamento()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICEFORNITOREANNOIDPAGAMENTO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEFORNITOREANNOIDPAGAMENTO,
                    args);

                args = new Object[] {
                        schedaPagamentoModelImpl.getCodiceSoggetto(),
                        schedaPagamentoModelImpl.getAnnoPagamento(),
                        schedaPagamentoModelImpl.getIdPagamento()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICEFORNITOREANNOIDPAGAMENTO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEFORNITOREANNOIDPAGAMENTO,
                    args);
            }

            if ((schedaPagamentoModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEFORNITOREANNOIDPAGAMENTOSTATO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        schedaPagamentoModelImpl.getOriginalCodiceSoggetto(),
                        schedaPagamentoModelImpl.getOriginalAnnoPagamento(),
                        schedaPagamentoModelImpl.getOriginalIdPagamento(),
                        schedaPagamentoModelImpl.getOriginalStato()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICEFORNITOREANNOIDPAGAMENTOSTATO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEFORNITOREANNOIDPAGAMENTOSTATO,
                    args);

                args = new Object[] {
                        schedaPagamentoModelImpl.getCodiceSoggetto(),
                        schedaPagamentoModelImpl.getAnnoPagamento(),
                        schedaPagamentoModelImpl.getIdPagamento(),
                        schedaPagamentoModelImpl.getStato()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICEFORNITOREANNOIDPAGAMENTOSTATO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEFORNITOREANNOIDPAGAMENTOSTATO,
                    args);
            }
        }

        EntityCacheUtil.putResult(SchedaPagamentoModelImpl.ENTITY_CACHE_ENABLED,
            SchedaPagamentoImpl.class, schedaPagamento.getPrimaryKey(),
            schedaPagamento);

        return schedaPagamento;
    }

    protected SchedaPagamento toUnwrappedModel(SchedaPagamento schedaPagamento) {
        if (schedaPagamento instanceof SchedaPagamentoImpl) {
            return schedaPagamento;
        }

        SchedaPagamentoImpl schedaPagamentoImpl = new SchedaPagamentoImpl();

        schedaPagamentoImpl.setNew(schedaPagamento.isNew());
        schedaPagamentoImpl.setPrimaryKey(schedaPagamento.getPrimaryKey());

        schedaPagamentoImpl.setEsercizioRegistrazione(schedaPagamento.getEsercizioRegistrazione());
        schedaPagamentoImpl.setCodiceSoggetto(schedaPagamento.getCodiceSoggetto());
        schedaPagamentoImpl.setNumeroPartita(schedaPagamento.getNumeroPartita());
        schedaPagamentoImpl.setNumeroScadenza(schedaPagamento.getNumeroScadenza());
        schedaPagamentoImpl.setTipoSoggetto(schedaPagamento.isTipoSoggetto());
        schedaPagamentoImpl.setEsercizioRegistrazioneComp(schedaPagamento.getEsercizioRegistrazioneComp());
        schedaPagamentoImpl.setCodiceSoggettoComp(schedaPagamento.getCodiceSoggettoComp());
        schedaPagamentoImpl.setNumeroPartitaComp(schedaPagamento.getNumeroPartitaComp());
        schedaPagamentoImpl.setNumeroScadenzaComp(schedaPagamento.getNumeroScadenzaComp());
        schedaPagamentoImpl.setTipoSoggettoComp(schedaPagamento.isTipoSoggettoComp());
        schedaPagamentoImpl.setDifferenza(schedaPagamento.getDifferenza());
        schedaPagamentoImpl.setIdPagamento(schedaPagamento.getIdPagamento());
        schedaPagamentoImpl.setAnnoPagamento(schedaPagamento.getAnnoPagamento());
        schedaPagamentoImpl.setStato(schedaPagamento.getStato());
        schedaPagamentoImpl.setNota(schedaPagamento.getNota());

        return schedaPagamentoImpl;
    }

    /**
     * Returns the scheda pagamento with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the scheda pagamento
     * @return the scheda pagamento
     * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a scheda pagamento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SchedaPagamento findByPrimaryKey(Serializable primaryKey)
        throws NoSuchSchedaPagamentoException, SystemException {
        SchedaPagamento schedaPagamento = fetchByPrimaryKey(primaryKey);

        if (schedaPagamento == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchSchedaPagamentoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return schedaPagamento;
    }

    /**
     * Returns the scheda pagamento with the primary key or throws a {@link it.bysoftware.ct.NoSuchSchedaPagamentoException} if it could not be found.
     *
     * @param schedaPagamentoPK the primary key of the scheda pagamento
     * @return the scheda pagamento
     * @throws it.bysoftware.ct.NoSuchSchedaPagamentoException if a scheda pagamento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SchedaPagamento findByPrimaryKey(SchedaPagamentoPK schedaPagamentoPK)
        throws NoSuchSchedaPagamentoException, SystemException {
        return findByPrimaryKey((Serializable) schedaPagamentoPK);
    }

    /**
     * Returns the scheda pagamento with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the scheda pagamento
     * @return the scheda pagamento, or <code>null</code> if a scheda pagamento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SchedaPagamento fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        SchedaPagamento schedaPagamento = (SchedaPagamento) EntityCacheUtil.getResult(SchedaPagamentoModelImpl.ENTITY_CACHE_ENABLED,
                SchedaPagamentoImpl.class, primaryKey);

        if (schedaPagamento == _nullSchedaPagamento) {
            return null;
        }

        if (schedaPagamento == null) {
            Session session = null;

            try {
                session = openSession();

                schedaPagamento = (SchedaPagamento) session.get(SchedaPagamentoImpl.class,
                        primaryKey);

                if (schedaPagamento != null) {
                    cacheResult(schedaPagamento);
                } else {
                    EntityCacheUtil.putResult(SchedaPagamentoModelImpl.ENTITY_CACHE_ENABLED,
                        SchedaPagamentoImpl.class, primaryKey,
                        _nullSchedaPagamento);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(SchedaPagamentoModelImpl.ENTITY_CACHE_ENABLED,
                    SchedaPagamentoImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return schedaPagamento;
    }

    /**
     * Returns the scheda pagamento with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param schedaPagamentoPK the primary key of the scheda pagamento
     * @return the scheda pagamento, or <code>null</code> if a scheda pagamento with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SchedaPagamento fetchByPrimaryKey(
        SchedaPagamentoPK schedaPagamentoPK) throws SystemException {
        return fetchByPrimaryKey((Serializable) schedaPagamentoPK);
    }

    /**
     * Returns all the scheda pagamentos.
     *
     * @return the scheda pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SchedaPagamento> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the scheda pagamentos.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of scheda pagamentos
     * @param end the upper bound of the range of scheda pagamentos (not inclusive)
     * @return the range of scheda pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SchedaPagamento> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the scheda pagamentos.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.SchedaPagamentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of scheda pagamentos
     * @param end the upper bound of the range of scheda pagamentos (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of scheda pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<SchedaPagamento> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<SchedaPagamento> list = (List<SchedaPagamento>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_SCHEDAPAGAMENTO);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_SCHEDAPAGAMENTO;

                if (pagination) {
                    sql = sql.concat(SchedaPagamentoModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<SchedaPagamento>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<SchedaPagamento>(list);
                } else {
                    list = (List<SchedaPagamento>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the scheda pagamentos from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (SchedaPagamento schedaPagamento : findAll()) {
            remove(schedaPagamento);
        }
    }

    /**
     * Returns the number of scheda pagamentos.
     *
     * @return the number of scheda pagamentos
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_SCHEDAPAGAMENTO);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the scheda pagamento persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.SchedaPagamento")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<SchedaPagamento>> listenersList = new ArrayList<ModelListener<SchedaPagamento>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<SchedaPagamento>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(SchedaPagamentoImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
