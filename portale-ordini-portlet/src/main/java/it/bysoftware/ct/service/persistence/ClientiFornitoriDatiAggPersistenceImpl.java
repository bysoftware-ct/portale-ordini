package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException;
import it.bysoftware.ct.model.ClientiFornitoriDatiAgg;
import it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggImpl;
import it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl;
import it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the clienti fornitori dati agg service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see ClientiFornitoriDatiAggPersistence
 * @see ClientiFornitoriDatiAggUtil
 * @generated
 */
public class ClientiFornitoriDatiAggPersistenceImpl extends BasePersistenceImpl<ClientiFornitoriDatiAgg>
    implements ClientiFornitoriDatiAggPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link ClientiFornitoriDatiAggUtil} to access the clienti fornitori dati agg persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = ClientiFornitoriDatiAggImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggModelImpl.FINDER_CACHE_ENABLED,
            ClientiFornitoriDatiAggImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggModelImpl.FINDER_CACHE_ENABLED,
            ClientiFornitoriDatiAggImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CLIENTI = new FinderPath(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggModelImpl.FINDER_CACHE_ENABLED,
            ClientiFornitoriDatiAggImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByClienti",
            new String[] {
                Boolean.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTI =
        new FinderPath(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggModelImpl.FINDER_CACHE_ENABLED,
            ClientiFornitoriDatiAggImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByClienti",
            new String[] { Boolean.class.getName() },
            ClientiFornitoriDatiAggModelImpl.FORNITORE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_CLIENTI = new FinderPath(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByClienti",
            new String[] { Boolean.class.getName() });
    private static final String _FINDER_COLUMN_CLIENTI_FORNITORE_2 = "clientiFornitoriDatiAgg.id.fornitore = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CLIENTICATEGORIA =
        new FinderPath(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggModelImpl.FINDER_CACHE_ENABLED,
            ClientiFornitoriDatiAggImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByClientiCategoria",
            new String[] {
                Boolean.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTICATEGORIA =
        new FinderPath(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggModelImpl.FINDER_CACHE_ENABLED,
            ClientiFornitoriDatiAggImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByClientiCategoria",
            new String[] { Boolean.class.getName(), String.class.getName() },
            ClientiFornitoriDatiAggModelImpl.FORNITORE_COLUMN_BITMASK |
            ClientiFornitoriDatiAggModelImpl.CATEGORIAECONOMICA_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_CLIENTICATEGORIA = new FinderPath(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByClientiCategoria",
            new String[] { Boolean.class.getName(), String.class.getName() });
    private static final String _FINDER_COLUMN_CLIENTICATEGORIA_FORNITORE_2 = "clientiFornitoriDatiAgg.id.fornitore = ? AND ";
    private static final String _FINDER_COLUMN_CLIENTICATEGORIA_CATEGORIAECONOMICA_1 =
        "clientiFornitoriDatiAgg.categoriaEconomica IS NULL";
    private static final String _FINDER_COLUMN_CLIENTICATEGORIA_CATEGORIAECONOMICA_2 =
        "clientiFornitoriDatiAgg.categoriaEconomica = ?";
    private static final String _FINDER_COLUMN_CLIENTICATEGORIA_CATEGORIAECONOMICA_3 =
        "(clientiFornitoriDatiAgg.categoriaEconomica IS NULL OR clientiFornitoriDatiAgg.categoriaEconomica = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ASSOCIATO =
        new FinderPath(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggModelImpl.FINDER_CACHE_ENABLED,
            ClientiFornitoriDatiAggImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByAssociato",
            new String[] {
                Boolean.class.getName(), Boolean.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSOCIATO =
        new FinderPath(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggModelImpl.FINDER_CACHE_ENABLED,
            ClientiFornitoriDatiAggImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByAssociato",
            new String[] { Boolean.class.getName(), Boolean.class.getName() },
            ClientiFornitoriDatiAggModelImpl.FORNITORE_COLUMN_BITMASK |
            ClientiFornitoriDatiAggModelImpl.ASSOCIATO_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_ASSOCIATO = new FinderPath(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByAssociato",
            new String[] { Boolean.class.getName(), Boolean.class.getName() });
    private static final String _FINDER_COLUMN_ASSOCIATO_FORNITORE_2 = "clientiFornitoriDatiAgg.id.fornitore = ? AND ";
    private static final String _FINDER_COLUMN_ASSOCIATO_ASSOCIATO_2 = "clientiFornitoriDatiAgg.associato = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FORNITORI =
        new FinderPath(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggModelImpl.FINDER_CACHE_ENABLED,
            ClientiFornitoriDatiAggImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByFornitori",
            new String[] {
                Boolean.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITORI =
        new FinderPath(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggModelImpl.FINDER_CACHE_ENABLED,
            ClientiFornitoriDatiAggImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByFornitori",
            new String[] { Boolean.class.getName(), String.class.getName() },
            ClientiFornitoriDatiAggModelImpl.FORNITORE_COLUMN_BITMASK |
            ClientiFornitoriDatiAggModelImpl.CATEGORIAECONOMICA_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_FORNITORI = new FinderPath(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByFornitori",
            new String[] { Boolean.class.getName(), String.class.getName() });
    private static final String _FINDER_COLUMN_FORNITORI_FORNITORE_2 = "clientiFornitoriDatiAgg.id.fornitore = ? AND ";
    private static final String _FINDER_COLUMN_FORNITORI_CATEGORIAECONOMICA_1 = "clientiFornitoriDatiAgg.categoriaEconomica IS NULL";
    private static final String _FINDER_COLUMN_FORNITORI_CATEGORIAECONOMICA_2 = "clientiFornitoriDatiAgg.categoriaEconomica = ?";
    private static final String _FINDER_COLUMN_FORNITORI_CATEGORIAECONOMICA_3 = "(clientiFornitoriDatiAgg.categoriaEconomica IS NULL OR clientiFornitoriDatiAgg.categoriaEconomica = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TUTTIFORNITORI =
        new FinderPath(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggModelImpl.FINDER_CACHE_ENABLED,
            ClientiFornitoriDatiAggImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByTuttiFornitori",
            new String[] {
                Boolean.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TUTTIFORNITORI =
        new FinderPath(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggModelImpl.FINDER_CACHE_ENABLED,
            ClientiFornitoriDatiAggImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByTuttiFornitori",
            new String[] { Boolean.class.getName() },
            ClientiFornitoriDatiAggModelImpl.FORNITORE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_TUTTIFORNITORI = new FinderPath(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTuttiFornitori",
            new String[] { Boolean.class.getName() });
    private static final String _FINDER_COLUMN_TUTTIFORNITORI_FORNITORE_2 = "clientiFornitoriDatiAgg.id.fornitore = ?";
    private static final String _SQL_SELECT_CLIENTIFORNITORIDATIAGG = "SELECT clientiFornitoriDatiAgg FROM ClientiFornitoriDatiAgg clientiFornitoriDatiAgg";
    private static final String _SQL_SELECT_CLIENTIFORNITORIDATIAGG_WHERE = "SELECT clientiFornitoriDatiAgg FROM ClientiFornitoriDatiAgg clientiFornitoriDatiAgg WHERE ";
    private static final String _SQL_COUNT_CLIENTIFORNITORIDATIAGG = "SELECT COUNT(clientiFornitoriDatiAgg) FROM ClientiFornitoriDatiAgg clientiFornitoriDatiAgg";
    private static final String _SQL_COUNT_CLIENTIFORNITORIDATIAGG_WHERE = "SELECT COUNT(clientiFornitoriDatiAgg) FROM ClientiFornitoriDatiAgg clientiFornitoriDatiAgg WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "clientiFornitoriDatiAgg.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ClientiFornitoriDatiAgg exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ClientiFornitoriDatiAgg exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(ClientiFornitoriDatiAggPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "codiceAnagrafica", "fornitore", "codiceAgente",
                "codiceEsenzione", "categoriaEconomica", "codiceRegionale",
                "codicePuntoVenditaGT", "tipoPagamento", "associato", "sconto",
                "codiceIBAN"
            });
    private static ClientiFornitoriDatiAgg _nullClientiFornitoriDatiAgg = new ClientiFornitoriDatiAggImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<ClientiFornitoriDatiAgg> toCacheModel() {
                return _nullClientiFornitoriDatiAggCacheModel;
            }
        };

    private static CacheModel<ClientiFornitoriDatiAgg> _nullClientiFornitoriDatiAggCacheModel =
        new CacheModel<ClientiFornitoriDatiAgg>() {
            @Override
            public ClientiFornitoriDatiAgg toEntityModel() {
                return _nullClientiFornitoriDatiAgg;
            }
        };

    public ClientiFornitoriDatiAggPersistenceImpl() {
        setModelClass(ClientiFornitoriDatiAgg.class);
    }

    /**
     * Returns all the clienti fornitori dati aggs where fornitore = &#63;.
     *
     * @param fornitore the fornitore
     * @return the matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ClientiFornitoriDatiAgg> findByClienti(boolean fornitore)
        throws SystemException {
        return findByClienti(fornitore, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the clienti fornitori dati aggs where fornitore = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param fornitore the fornitore
     * @param start the lower bound of the range of clienti fornitori dati aggs
     * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
     * @return the range of matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ClientiFornitoriDatiAgg> findByClienti(boolean fornitore,
        int start, int end) throws SystemException {
        return findByClienti(fornitore, start, end, null);
    }

    /**
     * Returns an ordered range of all the clienti fornitori dati aggs where fornitore = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param fornitore the fornitore
     * @param start the lower bound of the range of clienti fornitori dati aggs
     * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ClientiFornitoriDatiAgg> findByClienti(boolean fornitore,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTI;
            finderArgs = new Object[] { fornitore };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CLIENTI;
            finderArgs = new Object[] { fornitore, start, end, orderByComparator };
        }

        List<ClientiFornitoriDatiAgg> list = (List<ClientiFornitoriDatiAgg>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (ClientiFornitoriDatiAgg clientiFornitoriDatiAgg : list) {
                if ((fornitore != clientiFornitoriDatiAgg.getFornitore())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_CLIENTIFORNITORIDATIAGG_WHERE);

            query.append(_FINDER_COLUMN_CLIENTI_FORNITORE_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(ClientiFornitoriDatiAggModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(fornitore);

                if (!pagination) {
                    list = (List<ClientiFornitoriDatiAgg>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<ClientiFornitoriDatiAgg>(list);
                } else {
                    list = (List<ClientiFornitoriDatiAgg>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63;.
     *
     * @param fornitore the fornitore
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching clienti fornitori dati agg
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg findByClienti_First(boolean fornitore,
        OrderByComparator orderByComparator)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = fetchByClienti_First(fornitore,
                orderByComparator);

        if (clientiFornitoriDatiAgg != null) {
            return clientiFornitoriDatiAgg;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("fornitore=");
        msg.append(fornitore);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchClientiFornitoriDatiAggException(msg.toString());
    }

    /**
     * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63;.
     *
     * @param fornitore the fornitore
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg fetchByClienti_First(boolean fornitore,
        OrderByComparator orderByComparator) throws SystemException {
        List<ClientiFornitoriDatiAgg> list = findByClienti(fornitore, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63;.
     *
     * @param fornitore the fornitore
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching clienti fornitori dati agg
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg findByClienti_Last(boolean fornitore,
        OrderByComparator orderByComparator)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = fetchByClienti_Last(fornitore,
                orderByComparator);

        if (clientiFornitoriDatiAgg != null) {
            return clientiFornitoriDatiAgg;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("fornitore=");
        msg.append(fornitore);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchClientiFornitoriDatiAggException(msg.toString());
    }

    /**
     * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63;.
     *
     * @param fornitore the fornitore
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg fetchByClienti_Last(boolean fornitore,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByClienti(fornitore);

        if (count == 0) {
            return null;
        }

        List<ClientiFornitoriDatiAgg> list = findByClienti(fornitore,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the clienti fornitori dati aggs before and after the current clienti fornitori dati agg in the ordered set where fornitore = &#63;.
     *
     * @param clientiFornitoriDatiAggPK the primary key of the current clienti fornitori dati agg
     * @param fornitore the fornitore
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next clienti fornitori dati agg
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a clienti fornitori dati agg with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg[] findByClienti_PrevAndNext(
        ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK, boolean fornitore,
        OrderByComparator orderByComparator)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = findByPrimaryKey(clientiFornitoriDatiAggPK);

        Session session = null;

        try {
            session = openSession();

            ClientiFornitoriDatiAgg[] array = new ClientiFornitoriDatiAggImpl[3];

            array[0] = getByClienti_PrevAndNext(session,
                    clientiFornitoriDatiAgg, fornitore, orderByComparator, true);

            array[1] = clientiFornitoriDatiAgg;

            array[2] = getByClienti_PrevAndNext(session,
                    clientiFornitoriDatiAgg, fornitore, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected ClientiFornitoriDatiAgg getByClienti_PrevAndNext(
        Session session, ClientiFornitoriDatiAgg clientiFornitoriDatiAgg,
        boolean fornitore, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_CLIENTIFORNITORIDATIAGG_WHERE);

        query.append(_FINDER_COLUMN_CLIENTI_FORNITORE_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(ClientiFornitoriDatiAggModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(fornitore);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(clientiFornitoriDatiAgg);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<ClientiFornitoriDatiAgg> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the clienti fornitori dati aggs where fornitore = &#63; from the database.
     *
     * @param fornitore the fornitore
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByClienti(boolean fornitore) throws SystemException {
        for (ClientiFornitoriDatiAgg clientiFornitoriDatiAgg : findByClienti(
                fornitore, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(clientiFornitoriDatiAgg);
        }
    }

    /**
     * Returns the number of clienti fornitori dati aggs where fornitore = &#63;.
     *
     * @param fornitore the fornitore
     * @return the number of matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByClienti(boolean fornitore) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_CLIENTI;

        Object[] finderArgs = new Object[] { fornitore };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_CLIENTIFORNITORIDATIAGG_WHERE);

            query.append(_FINDER_COLUMN_CLIENTI_FORNITORE_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(fornitore);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63;.
     *
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @return the matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ClientiFornitoriDatiAgg> findByClientiCategoria(
        boolean fornitore, String categoriaEconomica) throws SystemException {
        return findByClientiCategoria(fornitore, categoriaEconomica,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @param start the lower bound of the range of clienti fornitori dati aggs
     * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
     * @return the range of matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ClientiFornitoriDatiAgg> findByClientiCategoria(
        boolean fornitore, String categoriaEconomica, int start, int end)
        throws SystemException {
        return findByClientiCategoria(fornitore, categoriaEconomica, start,
            end, null);
    }

    /**
     * Returns an ordered range of all the clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @param start the lower bound of the range of clienti fornitori dati aggs
     * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ClientiFornitoriDatiAgg> findByClientiCategoria(
        boolean fornitore, String categoriaEconomica, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTICATEGORIA;
            finderArgs = new Object[] { fornitore, categoriaEconomica };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CLIENTICATEGORIA;
            finderArgs = new Object[] {
                    fornitore, categoriaEconomica,
                    
                    start, end, orderByComparator
                };
        }

        List<ClientiFornitoriDatiAgg> list = (List<ClientiFornitoriDatiAgg>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (ClientiFornitoriDatiAgg clientiFornitoriDatiAgg : list) {
                if ((fornitore != clientiFornitoriDatiAgg.getFornitore()) ||
                        !Validator.equals(categoriaEconomica,
                            clientiFornitoriDatiAgg.getCategoriaEconomica())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_CLIENTIFORNITORIDATIAGG_WHERE);

            query.append(_FINDER_COLUMN_CLIENTICATEGORIA_FORNITORE_2);

            boolean bindCategoriaEconomica = false;

            if (categoriaEconomica == null) {
                query.append(_FINDER_COLUMN_CLIENTICATEGORIA_CATEGORIAECONOMICA_1);
            } else if (categoriaEconomica.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CLIENTICATEGORIA_CATEGORIAECONOMICA_3);
            } else {
                bindCategoriaEconomica = true;

                query.append(_FINDER_COLUMN_CLIENTICATEGORIA_CATEGORIAECONOMICA_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(ClientiFornitoriDatiAggModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(fornitore);

                if (bindCategoriaEconomica) {
                    qPos.add(categoriaEconomica);
                }

                if (!pagination) {
                    list = (List<ClientiFornitoriDatiAgg>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<ClientiFornitoriDatiAgg>(list);
                } else {
                    list = (List<ClientiFornitoriDatiAgg>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
     *
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching clienti fornitori dati agg
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg findByClientiCategoria_First(
        boolean fornitore, String categoriaEconomica,
        OrderByComparator orderByComparator)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = fetchByClientiCategoria_First(fornitore,
                categoriaEconomica, orderByComparator);

        if (clientiFornitoriDatiAgg != null) {
            return clientiFornitoriDatiAgg;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("fornitore=");
        msg.append(fornitore);

        msg.append(", categoriaEconomica=");
        msg.append(categoriaEconomica);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchClientiFornitoriDatiAggException(msg.toString());
    }

    /**
     * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
     *
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg fetchByClientiCategoria_First(
        boolean fornitore, String categoriaEconomica,
        OrderByComparator orderByComparator) throws SystemException {
        List<ClientiFornitoriDatiAgg> list = findByClientiCategoria(fornitore,
                categoriaEconomica, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
     *
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching clienti fornitori dati agg
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg findByClientiCategoria_Last(
        boolean fornitore, String categoriaEconomica,
        OrderByComparator orderByComparator)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = fetchByClientiCategoria_Last(fornitore,
                categoriaEconomica, orderByComparator);

        if (clientiFornitoriDatiAgg != null) {
            return clientiFornitoriDatiAgg;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("fornitore=");
        msg.append(fornitore);

        msg.append(", categoriaEconomica=");
        msg.append(categoriaEconomica);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchClientiFornitoriDatiAggException(msg.toString());
    }

    /**
     * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
     *
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg fetchByClientiCategoria_Last(
        boolean fornitore, String categoriaEconomica,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByClientiCategoria(fornitore, categoriaEconomica);

        if (count == 0) {
            return null;
        }

        List<ClientiFornitoriDatiAgg> list = findByClientiCategoria(fornitore,
                categoriaEconomica, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the clienti fornitori dati aggs before and after the current clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
     *
     * @param clientiFornitoriDatiAggPK the primary key of the current clienti fornitori dati agg
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next clienti fornitori dati agg
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a clienti fornitori dati agg with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg[] findByClientiCategoria_PrevAndNext(
        ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK, boolean fornitore,
        String categoriaEconomica, OrderByComparator orderByComparator)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = findByPrimaryKey(clientiFornitoriDatiAggPK);

        Session session = null;

        try {
            session = openSession();

            ClientiFornitoriDatiAgg[] array = new ClientiFornitoriDatiAggImpl[3];

            array[0] = getByClientiCategoria_PrevAndNext(session,
                    clientiFornitoriDatiAgg, fornitore, categoriaEconomica,
                    orderByComparator, true);

            array[1] = clientiFornitoriDatiAgg;

            array[2] = getByClientiCategoria_PrevAndNext(session,
                    clientiFornitoriDatiAgg, fornitore, categoriaEconomica,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected ClientiFornitoriDatiAgg getByClientiCategoria_PrevAndNext(
        Session session, ClientiFornitoriDatiAgg clientiFornitoriDatiAgg,
        boolean fornitore, String categoriaEconomica,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_CLIENTIFORNITORIDATIAGG_WHERE);

        query.append(_FINDER_COLUMN_CLIENTICATEGORIA_FORNITORE_2);

        boolean bindCategoriaEconomica = false;

        if (categoriaEconomica == null) {
            query.append(_FINDER_COLUMN_CLIENTICATEGORIA_CATEGORIAECONOMICA_1);
        } else if (categoriaEconomica.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_CLIENTICATEGORIA_CATEGORIAECONOMICA_3);
        } else {
            bindCategoriaEconomica = true;

            query.append(_FINDER_COLUMN_CLIENTICATEGORIA_CATEGORIAECONOMICA_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(ClientiFornitoriDatiAggModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(fornitore);

        if (bindCategoriaEconomica) {
            qPos.add(categoriaEconomica);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(clientiFornitoriDatiAgg);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<ClientiFornitoriDatiAgg> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63; from the database.
     *
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByClientiCategoria(boolean fornitore,
        String categoriaEconomica) throws SystemException {
        for (ClientiFornitoriDatiAgg clientiFornitoriDatiAgg : findByClientiCategoria(
                fornitore, categoriaEconomica, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(clientiFornitoriDatiAgg);
        }
    }

    /**
     * Returns the number of clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63;.
     *
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @return the number of matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByClientiCategoria(boolean fornitore,
        String categoriaEconomica) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_CLIENTICATEGORIA;

        Object[] finderArgs = new Object[] { fornitore, categoriaEconomica };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_CLIENTIFORNITORIDATIAGG_WHERE);

            query.append(_FINDER_COLUMN_CLIENTICATEGORIA_FORNITORE_2);

            boolean bindCategoriaEconomica = false;

            if (categoriaEconomica == null) {
                query.append(_FINDER_COLUMN_CLIENTICATEGORIA_CATEGORIAECONOMICA_1);
            } else if (categoriaEconomica.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CLIENTICATEGORIA_CATEGORIAECONOMICA_3);
            } else {
                bindCategoriaEconomica = true;

                query.append(_FINDER_COLUMN_CLIENTICATEGORIA_CATEGORIAECONOMICA_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(fornitore);

                if (bindCategoriaEconomica) {
                    qPos.add(categoriaEconomica);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the clienti fornitori dati aggs where fornitore = &#63; and associato = &#63;.
     *
     * @param fornitore the fornitore
     * @param associato the associato
     * @return the matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ClientiFornitoriDatiAgg> findByAssociato(boolean fornitore,
        boolean associato) throws SystemException {
        return findByAssociato(fornitore, associato, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the clienti fornitori dati aggs where fornitore = &#63; and associato = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param fornitore the fornitore
     * @param associato the associato
     * @param start the lower bound of the range of clienti fornitori dati aggs
     * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
     * @return the range of matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ClientiFornitoriDatiAgg> findByAssociato(boolean fornitore,
        boolean associato, int start, int end) throws SystemException {
        return findByAssociato(fornitore, associato, start, end, null);
    }

    /**
     * Returns an ordered range of all the clienti fornitori dati aggs where fornitore = &#63; and associato = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param fornitore the fornitore
     * @param associato the associato
     * @param start the lower bound of the range of clienti fornitori dati aggs
     * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ClientiFornitoriDatiAgg> findByAssociato(boolean fornitore,
        boolean associato, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSOCIATO;
            finderArgs = new Object[] { fornitore, associato };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ASSOCIATO;
            finderArgs = new Object[] {
                    fornitore, associato,
                    
                    start, end, orderByComparator
                };
        }

        List<ClientiFornitoriDatiAgg> list = (List<ClientiFornitoriDatiAgg>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (ClientiFornitoriDatiAgg clientiFornitoriDatiAgg : list) {
                if ((fornitore != clientiFornitoriDatiAgg.getFornitore()) ||
                        (associato != clientiFornitoriDatiAgg.getAssociato())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_CLIENTIFORNITORIDATIAGG_WHERE);

            query.append(_FINDER_COLUMN_ASSOCIATO_FORNITORE_2);

            query.append(_FINDER_COLUMN_ASSOCIATO_ASSOCIATO_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(ClientiFornitoriDatiAggModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(fornitore);

                qPos.add(associato);

                if (!pagination) {
                    list = (List<ClientiFornitoriDatiAgg>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<ClientiFornitoriDatiAgg>(list);
                } else {
                    list = (List<ClientiFornitoriDatiAgg>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63; and associato = &#63;.
     *
     * @param fornitore the fornitore
     * @param associato the associato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching clienti fornitori dati agg
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg findByAssociato_First(boolean fornitore,
        boolean associato, OrderByComparator orderByComparator)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = fetchByAssociato_First(fornitore,
                associato, orderByComparator);

        if (clientiFornitoriDatiAgg != null) {
            return clientiFornitoriDatiAgg;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("fornitore=");
        msg.append(fornitore);

        msg.append(", associato=");
        msg.append(associato);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchClientiFornitoriDatiAggException(msg.toString());
    }

    /**
     * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63; and associato = &#63;.
     *
     * @param fornitore the fornitore
     * @param associato the associato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg fetchByAssociato_First(boolean fornitore,
        boolean associato, OrderByComparator orderByComparator)
        throws SystemException {
        List<ClientiFornitoriDatiAgg> list = findByAssociato(fornitore,
                associato, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63; and associato = &#63;.
     *
     * @param fornitore the fornitore
     * @param associato the associato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching clienti fornitori dati agg
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg findByAssociato_Last(boolean fornitore,
        boolean associato, OrderByComparator orderByComparator)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = fetchByAssociato_Last(fornitore,
                associato, orderByComparator);

        if (clientiFornitoriDatiAgg != null) {
            return clientiFornitoriDatiAgg;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("fornitore=");
        msg.append(fornitore);

        msg.append(", associato=");
        msg.append(associato);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchClientiFornitoriDatiAggException(msg.toString());
    }

    /**
     * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63; and associato = &#63;.
     *
     * @param fornitore the fornitore
     * @param associato the associato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg fetchByAssociato_Last(boolean fornitore,
        boolean associato, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByAssociato(fornitore, associato);

        if (count == 0) {
            return null;
        }

        List<ClientiFornitoriDatiAgg> list = findByAssociato(fornitore,
                associato, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the clienti fornitori dati aggs before and after the current clienti fornitori dati agg in the ordered set where fornitore = &#63; and associato = &#63;.
     *
     * @param clientiFornitoriDatiAggPK the primary key of the current clienti fornitori dati agg
     * @param fornitore the fornitore
     * @param associato the associato
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next clienti fornitori dati agg
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a clienti fornitori dati agg with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg[] findByAssociato_PrevAndNext(
        ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK, boolean fornitore,
        boolean associato, OrderByComparator orderByComparator)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = findByPrimaryKey(clientiFornitoriDatiAggPK);

        Session session = null;

        try {
            session = openSession();

            ClientiFornitoriDatiAgg[] array = new ClientiFornitoriDatiAggImpl[3];

            array[0] = getByAssociato_PrevAndNext(session,
                    clientiFornitoriDatiAgg, fornitore, associato,
                    orderByComparator, true);

            array[1] = clientiFornitoriDatiAgg;

            array[2] = getByAssociato_PrevAndNext(session,
                    clientiFornitoriDatiAgg, fornitore, associato,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected ClientiFornitoriDatiAgg getByAssociato_PrevAndNext(
        Session session, ClientiFornitoriDatiAgg clientiFornitoriDatiAgg,
        boolean fornitore, boolean associato,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_CLIENTIFORNITORIDATIAGG_WHERE);

        query.append(_FINDER_COLUMN_ASSOCIATO_FORNITORE_2);

        query.append(_FINDER_COLUMN_ASSOCIATO_ASSOCIATO_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(ClientiFornitoriDatiAggModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(fornitore);

        qPos.add(associato);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(clientiFornitoriDatiAgg);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<ClientiFornitoriDatiAgg> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the clienti fornitori dati aggs where fornitore = &#63; and associato = &#63; from the database.
     *
     * @param fornitore the fornitore
     * @param associato the associato
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByAssociato(boolean fornitore, boolean associato)
        throws SystemException {
        for (ClientiFornitoriDatiAgg clientiFornitoriDatiAgg : findByAssociato(
                fornitore, associato, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(clientiFornitoriDatiAgg);
        }
    }

    /**
     * Returns the number of clienti fornitori dati aggs where fornitore = &#63; and associato = &#63;.
     *
     * @param fornitore the fornitore
     * @param associato the associato
     * @return the number of matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByAssociato(boolean fornitore, boolean associato)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_ASSOCIATO;

        Object[] finderArgs = new Object[] { fornitore, associato };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_CLIENTIFORNITORIDATIAGG_WHERE);

            query.append(_FINDER_COLUMN_ASSOCIATO_FORNITORE_2);

            query.append(_FINDER_COLUMN_ASSOCIATO_ASSOCIATO_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(fornitore);

                qPos.add(associato);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63;.
     *
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @return the matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ClientiFornitoriDatiAgg> findByFornitori(boolean fornitore,
        String categoriaEconomica) throws SystemException {
        return findByFornitori(fornitore, categoriaEconomica,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @param start the lower bound of the range of clienti fornitori dati aggs
     * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
     * @return the range of matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ClientiFornitoriDatiAgg> findByFornitori(boolean fornitore,
        String categoriaEconomica, int start, int end)
        throws SystemException {
        return findByFornitori(fornitore, categoriaEconomica, start, end, null);
    }

    /**
     * Returns an ordered range of all the clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @param start the lower bound of the range of clienti fornitori dati aggs
     * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ClientiFornitoriDatiAgg> findByFornitori(boolean fornitore,
        String categoriaEconomica, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITORI;
            finderArgs = new Object[] { fornitore, categoriaEconomica };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FORNITORI;
            finderArgs = new Object[] {
                    fornitore, categoriaEconomica,
                    
                    start, end, orderByComparator
                };
        }

        List<ClientiFornitoriDatiAgg> list = (List<ClientiFornitoriDatiAgg>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (ClientiFornitoriDatiAgg clientiFornitoriDatiAgg : list) {
                if ((fornitore != clientiFornitoriDatiAgg.getFornitore()) ||
                        !Validator.equals(categoriaEconomica,
                            clientiFornitoriDatiAgg.getCategoriaEconomica())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_CLIENTIFORNITORIDATIAGG_WHERE);

            query.append(_FINDER_COLUMN_FORNITORI_FORNITORE_2);

            boolean bindCategoriaEconomica = false;

            if (categoriaEconomica == null) {
                query.append(_FINDER_COLUMN_FORNITORI_CATEGORIAECONOMICA_1);
            } else if (categoriaEconomica.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITORI_CATEGORIAECONOMICA_3);
            } else {
                bindCategoriaEconomica = true;

                query.append(_FINDER_COLUMN_FORNITORI_CATEGORIAECONOMICA_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(ClientiFornitoriDatiAggModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(fornitore);

                if (bindCategoriaEconomica) {
                    qPos.add(categoriaEconomica);
                }

                if (!pagination) {
                    list = (List<ClientiFornitoriDatiAgg>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<ClientiFornitoriDatiAgg>(list);
                } else {
                    list = (List<ClientiFornitoriDatiAgg>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
     *
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching clienti fornitori dati agg
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg findByFornitori_First(boolean fornitore,
        String categoriaEconomica, OrderByComparator orderByComparator)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = fetchByFornitori_First(fornitore,
                categoriaEconomica, orderByComparator);

        if (clientiFornitoriDatiAgg != null) {
            return clientiFornitoriDatiAgg;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("fornitore=");
        msg.append(fornitore);

        msg.append(", categoriaEconomica=");
        msg.append(categoriaEconomica);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchClientiFornitoriDatiAggException(msg.toString());
    }

    /**
     * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
     *
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg fetchByFornitori_First(boolean fornitore,
        String categoriaEconomica, OrderByComparator orderByComparator)
        throws SystemException {
        List<ClientiFornitoriDatiAgg> list = findByFornitori(fornitore,
                categoriaEconomica, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
     *
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching clienti fornitori dati agg
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg findByFornitori_Last(boolean fornitore,
        String categoriaEconomica, OrderByComparator orderByComparator)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = fetchByFornitori_Last(fornitore,
                categoriaEconomica, orderByComparator);

        if (clientiFornitoriDatiAgg != null) {
            return clientiFornitoriDatiAgg;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("fornitore=");
        msg.append(fornitore);

        msg.append(", categoriaEconomica=");
        msg.append(categoriaEconomica);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchClientiFornitoriDatiAggException(msg.toString());
    }

    /**
     * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
     *
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg fetchByFornitori_Last(boolean fornitore,
        String categoriaEconomica, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByFornitori(fornitore, categoriaEconomica);

        if (count == 0) {
            return null;
        }

        List<ClientiFornitoriDatiAgg> list = findByFornitori(fornitore,
                categoriaEconomica, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the clienti fornitori dati aggs before and after the current clienti fornitori dati agg in the ordered set where fornitore = &#63; and categoriaEconomica = &#63;.
     *
     * @param clientiFornitoriDatiAggPK the primary key of the current clienti fornitori dati agg
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next clienti fornitori dati agg
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a clienti fornitori dati agg with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg[] findByFornitori_PrevAndNext(
        ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK, boolean fornitore,
        String categoriaEconomica, OrderByComparator orderByComparator)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = findByPrimaryKey(clientiFornitoriDatiAggPK);

        Session session = null;

        try {
            session = openSession();

            ClientiFornitoriDatiAgg[] array = new ClientiFornitoriDatiAggImpl[3];

            array[0] = getByFornitori_PrevAndNext(session,
                    clientiFornitoriDatiAgg, fornitore, categoriaEconomica,
                    orderByComparator, true);

            array[1] = clientiFornitoriDatiAgg;

            array[2] = getByFornitori_PrevAndNext(session,
                    clientiFornitoriDatiAgg, fornitore, categoriaEconomica,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected ClientiFornitoriDatiAgg getByFornitori_PrevAndNext(
        Session session, ClientiFornitoriDatiAgg clientiFornitoriDatiAgg,
        boolean fornitore, String categoriaEconomica,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_CLIENTIFORNITORIDATIAGG_WHERE);

        query.append(_FINDER_COLUMN_FORNITORI_FORNITORE_2);

        boolean bindCategoriaEconomica = false;

        if (categoriaEconomica == null) {
            query.append(_FINDER_COLUMN_FORNITORI_CATEGORIAECONOMICA_1);
        } else if (categoriaEconomica.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_FORNITORI_CATEGORIAECONOMICA_3);
        } else {
            bindCategoriaEconomica = true;

            query.append(_FINDER_COLUMN_FORNITORI_CATEGORIAECONOMICA_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(ClientiFornitoriDatiAggModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(fornitore);

        if (bindCategoriaEconomica) {
            qPos.add(categoriaEconomica);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(clientiFornitoriDatiAgg);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<ClientiFornitoriDatiAgg> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63; from the database.
     *
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByFornitori(boolean fornitore, String categoriaEconomica)
        throws SystemException {
        for (ClientiFornitoriDatiAgg clientiFornitoriDatiAgg : findByFornitori(
                fornitore, categoriaEconomica, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(clientiFornitoriDatiAgg);
        }
    }

    /**
     * Returns the number of clienti fornitori dati aggs where fornitore = &#63; and categoriaEconomica = &#63;.
     *
     * @param fornitore the fornitore
     * @param categoriaEconomica the categoria economica
     * @return the number of matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByFornitori(boolean fornitore, String categoriaEconomica)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_FORNITORI;

        Object[] finderArgs = new Object[] { fornitore, categoriaEconomica };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_CLIENTIFORNITORIDATIAGG_WHERE);

            query.append(_FINDER_COLUMN_FORNITORI_FORNITORE_2);

            boolean bindCategoriaEconomica = false;

            if (categoriaEconomica == null) {
                query.append(_FINDER_COLUMN_FORNITORI_CATEGORIAECONOMICA_1);
            } else if (categoriaEconomica.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_FORNITORI_CATEGORIAECONOMICA_3);
            } else {
                bindCategoriaEconomica = true;

                query.append(_FINDER_COLUMN_FORNITORI_CATEGORIAECONOMICA_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(fornitore);

                if (bindCategoriaEconomica) {
                    qPos.add(categoriaEconomica);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the clienti fornitori dati aggs where fornitore = &#63;.
     *
     * @param fornitore the fornitore
     * @return the matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ClientiFornitoriDatiAgg> findByTuttiFornitori(boolean fornitore)
        throws SystemException {
        return findByTuttiFornitori(fornitore, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the clienti fornitori dati aggs where fornitore = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param fornitore the fornitore
     * @param start the lower bound of the range of clienti fornitori dati aggs
     * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
     * @return the range of matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ClientiFornitoriDatiAgg> findByTuttiFornitori(
        boolean fornitore, int start, int end) throws SystemException {
        return findByTuttiFornitori(fornitore, start, end, null);
    }

    /**
     * Returns an ordered range of all the clienti fornitori dati aggs where fornitore = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param fornitore the fornitore
     * @param start the lower bound of the range of clienti fornitori dati aggs
     * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ClientiFornitoriDatiAgg> findByTuttiFornitori(
        boolean fornitore, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TUTTIFORNITORI;
            finderArgs = new Object[] { fornitore };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TUTTIFORNITORI;
            finderArgs = new Object[] { fornitore, start, end, orderByComparator };
        }

        List<ClientiFornitoriDatiAgg> list = (List<ClientiFornitoriDatiAgg>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (ClientiFornitoriDatiAgg clientiFornitoriDatiAgg : list) {
                if ((fornitore != clientiFornitoriDatiAgg.getFornitore())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_CLIENTIFORNITORIDATIAGG_WHERE);

            query.append(_FINDER_COLUMN_TUTTIFORNITORI_FORNITORE_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(ClientiFornitoriDatiAggModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(fornitore);

                if (!pagination) {
                    list = (List<ClientiFornitoriDatiAgg>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<ClientiFornitoriDatiAgg>(list);
                } else {
                    list = (List<ClientiFornitoriDatiAgg>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63;.
     *
     * @param fornitore the fornitore
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching clienti fornitori dati agg
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg findByTuttiFornitori_First(
        boolean fornitore, OrderByComparator orderByComparator)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = fetchByTuttiFornitori_First(fornitore,
                orderByComparator);

        if (clientiFornitoriDatiAgg != null) {
            return clientiFornitoriDatiAgg;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("fornitore=");
        msg.append(fornitore);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchClientiFornitoriDatiAggException(msg.toString());
    }

    /**
     * Returns the first clienti fornitori dati agg in the ordered set where fornitore = &#63;.
     *
     * @param fornitore the fornitore
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg fetchByTuttiFornitori_First(
        boolean fornitore, OrderByComparator orderByComparator)
        throws SystemException {
        List<ClientiFornitoriDatiAgg> list = findByTuttiFornitori(fornitore, 0,
                1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63;.
     *
     * @param fornitore the fornitore
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching clienti fornitori dati agg
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg findByTuttiFornitori_Last(
        boolean fornitore, OrderByComparator orderByComparator)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = fetchByTuttiFornitori_Last(fornitore,
                orderByComparator);

        if (clientiFornitoriDatiAgg != null) {
            return clientiFornitoriDatiAgg;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("fornitore=");
        msg.append(fornitore);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchClientiFornitoriDatiAggException(msg.toString());
    }

    /**
     * Returns the last clienti fornitori dati agg in the ordered set where fornitore = &#63;.
     *
     * @param fornitore the fornitore
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching clienti fornitori dati agg, or <code>null</code> if a matching clienti fornitori dati agg could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg fetchByTuttiFornitori_Last(
        boolean fornitore, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByTuttiFornitori(fornitore);

        if (count == 0) {
            return null;
        }

        List<ClientiFornitoriDatiAgg> list = findByTuttiFornitori(fornitore,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the clienti fornitori dati aggs before and after the current clienti fornitori dati agg in the ordered set where fornitore = &#63;.
     *
     * @param clientiFornitoriDatiAggPK the primary key of the current clienti fornitori dati agg
     * @param fornitore the fornitore
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next clienti fornitori dati agg
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a clienti fornitori dati agg with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg[] findByTuttiFornitori_PrevAndNext(
        ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK, boolean fornitore,
        OrderByComparator orderByComparator)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = findByPrimaryKey(clientiFornitoriDatiAggPK);

        Session session = null;

        try {
            session = openSession();

            ClientiFornitoriDatiAgg[] array = new ClientiFornitoriDatiAggImpl[3];

            array[0] = getByTuttiFornitori_PrevAndNext(session,
                    clientiFornitoriDatiAgg, fornitore, orderByComparator, true);

            array[1] = clientiFornitoriDatiAgg;

            array[2] = getByTuttiFornitori_PrevAndNext(session,
                    clientiFornitoriDatiAgg, fornitore, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected ClientiFornitoriDatiAgg getByTuttiFornitori_PrevAndNext(
        Session session, ClientiFornitoriDatiAgg clientiFornitoriDatiAgg,
        boolean fornitore, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_CLIENTIFORNITORIDATIAGG_WHERE);

        query.append(_FINDER_COLUMN_TUTTIFORNITORI_FORNITORE_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(ClientiFornitoriDatiAggModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(fornitore);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(clientiFornitoriDatiAgg);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<ClientiFornitoriDatiAgg> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the clienti fornitori dati aggs where fornitore = &#63; from the database.
     *
     * @param fornitore the fornitore
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByTuttiFornitori(boolean fornitore)
        throws SystemException {
        for (ClientiFornitoriDatiAgg clientiFornitoriDatiAgg : findByTuttiFornitori(
                fornitore, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(clientiFornitoriDatiAgg);
        }
    }

    /**
     * Returns the number of clienti fornitori dati aggs where fornitore = &#63;.
     *
     * @param fornitore the fornitore
     * @return the number of matching clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByTuttiFornitori(boolean fornitore)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_TUTTIFORNITORI;

        Object[] finderArgs = new Object[] { fornitore };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_CLIENTIFORNITORIDATIAGG_WHERE);

            query.append(_FINDER_COLUMN_TUTTIFORNITORI_FORNITORE_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(fornitore);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the clienti fornitori dati agg in the entity cache if it is enabled.
     *
     * @param clientiFornitoriDatiAgg the clienti fornitori dati agg
     */
    @Override
    public void cacheResult(ClientiFornitoriDatiAgg clientiFornitoriDatiAgg) {
        EntityCacheUtil.putResult(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggImpl.class,
            clientiFornitoriDatiAgg.getPrimaryKey(), clientiFornitoriDatiAgg);

        clientiFornitoriDatiAgg.resetOriginalValues();
    }

    /**
     * Caches the clienti fornitori dati aggs in the entity cache if it is enabled.
     *
     * @param clientiFornitoriDatiAggs the clienti fornitori dati aggs
     */
    @Override
    public void cacheResult(
        List<ClientiFornitoriDatiAgg> clientiFornitoriDatiAggs) {
        for (ClientiFornitoriDatiAgg clientiFornitoriDatiAgg : clientiFornitoriDatiAggs) {
            if (EntityCacheUtil.getResult(
                        ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
                        ClientiFornitoriDatiAggImpl.class,
                        clientiFornitoriDatiAgg.getPrimaryKey()) == null) {
                cacheResult(clientiFornitoriDatiAgg);
            } else {
                clientiFornitoriDatiAgg.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all clienti fornitori dati aggs.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(ClientiFornitoriDatiAggImpl.class.getName());
        }

        EntityCacheUtil.clearCache(ClientiFornitoriDatiAggImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the clienti fornitori dati agg.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(ClientiFornitoriDatiAgg clientiFornitoriDatiAgg) {
        EntityCacheUtil.removeResult(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggImpl.class,
            clientiFornitoriDatiAgg.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(
        List<ClientiFornitoriDatiAgg> clientiFornitoriDatiAggs) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (ClientiFornitoriDatiAgg clientiFornitoriDatiAgg : clientiFornitoriDatiAggs) {
            EntityCacheUtil.removeResult(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
                ClientiFornitoriDatiAggImpl.class,
                clientiFornitoriDatiAgg.getPrimaryKey());
        }
    }

    /**
     * Creates a new clienti fornitori dati agg with the primary key. Does not add the clienti fornitori dati agg to the database.
     *
     * @param clientiFornitoriDatiAggPK the primary key for the new clienti fornitori dati agg
     * @return the new clienti fornitori dati agg
     */
    @Override
    public ClientiFornitoriDatiAgg create(
        ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK) {
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = new ClientiFornitoriDatiAggImpl();

        clientiFornitoriDatiAgg.setNew(true);
        clientiFornitoriDatiAgg.setPrimaryKey(clientiFornitoriDatiAggPK);

        return clientiFornitoriDatiAgg;
    }

    /**
     * Removes the clienti fornitori dati agg with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param clientiFornitoriDatiAggPK the primary key of the clienti fornitori dati agg
     * @return the clienti fornitori dati agg that was removed
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a clienti fornitori dati agg with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg remove(
        ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        return remove((Serializable) clientiFornitoriDatiAggPK);
    }

    /**
     * Removes the clienti fornitori dati agg with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the clienti fornitori dati agg
     * @return the clienti fornitori dati agg that was removed
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a clienti fornitori dati agg with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg remove(Serializable primaryKey)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        Session session = null;

        try {
            session = openSession();

            ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = (ClientiFornitoriDatiAgg) session.get(ClientiFornitoriDatiAggImpl.class,
                    primaryKey);

            if (clientiFornitoriDatiAgg == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchClientiFornitoriDatiAggException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(clientiFornitoriDatiAgg);
        } catch (NoSuchClientiFornitoriDatiAggException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected ClientiFornitoriDatiAgg removeImpl(
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg)
        throws SystemException {
        clientiFornitoriDatiAgg = toUnwrappedModel(clientiFornitoriDatiAgg);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(clientiFornitoriDatiAgg)) {
                clientiFornitoriDatiAgg = (ClientiFornitoriDatiAgg) session.get(ClientiFornitoriDatiAggImpl.class,
                        clientiFornitoriDatiAgg.getPrimaryKeyObj());
            }

            if (clientiFornitoriDatiAgg != null) {
                session.delete(clientiFornitoriDatiAgg);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (clientiFornitoriDatiAgg != null) {
            clearCache(clientiFornitoriDatiAgg);
        }

        return clientiFornitoriDatiAgg;
    }

    @Override
    public ClientiFornitoriDatiAgg updateImpl(
        it.bysoftware.ct.model.ClientiFornitoriDatiAgg clientiFornitoriDatiAgg)
        throws SystemException {
        clientiFornitoriDatiAgg = toUnwrappedModel(clientiFornitoriDatiAgg);

        boolean isNew = clientiFornitoriDatiAgg.isNew();

        ClientiFornitoriDatiAggModelImpl clientiFornitoriDatiAggModelImpl = (ClientiFornitoriDatiAggModelImpl) clientiFornitoriDatiAgg;

        Session session = null;

        try {
            session = openSession();

            if (clientiFornitoriDatiAgg.isNew()) {
                session.save(clientiFornitoriDatiAgg);

                clientiFornitoriDatiAgg.setNew(false);
            } else {
                session.merge(clientiFornitoriDatiAgg);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !ClientiFornitoriDatiAggModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((clientiFornitoriDatiAggModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTI.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        clientiFornitoriDatiAggModelImpl.getOriginalFornitore()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CLIENTI, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTI,
                    args);

                args = new Object[] {
                        clientiFornitoriDatiAggModelImpl.getFornitore()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CLIENTI, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTI,
                    args);
            }

            if ((clientiFornitoriDatiAggModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTICATEGORIA.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        clientiFornitoriDatiAggModelImpl.getOriginalFornitore(),
                        clientiFornitoriDatiAggModelImpl.getOriginalCategoriaEconomica()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CLIENTICATEGORIA,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTICATEGORIA,
                    args);

                args = new Object[] {
                        clientiFornitoriDatiAggModelImpl.getFornitore(),
                        clientiFornitoriDatiAggModelImpl.getCategoriaEconomica()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CLIENTICATEGORIA,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLIENTICATEGORIA,
                    args);
            }

            if ((clientiFornitoriDatiAggModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSOCIATO.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        clientiFornitoriDatiAggModelImpl.getOriginalFornitore(),
                        clientiFornitoriDatiAggModelImpl.getOriginalAssociato()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ASSOCIATO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSOCIATO,
                    args);

                args = new Object[] {
                        clientiFornitoriDatiAggModelImpl.getFornitore(),
                        clientiFornitoriDatiAggModelImpl.getAssociato()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ASSOCIATO,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSOCIATO,
                    args);
            }

            if ((clientiFornitoriDatiAggModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITORI.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        clientiFornitoriDatiAggModelImpl.getOriginalFornitore(),
                        clientiFornitoriDatiAggModelImpl.getOriginalCategoriaEconomica()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FORNITORI,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITORI,
                    args);

                args = new Object[] {
                        clientiFornitoriDatiAggModelImpl.getFornitore(),
                        clientiFornitoriDatiAggModelImpl.getCategoriaEconomica()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FORNITORI,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FORNITORI,
                    args);
            }

            if ((clientiFornitoriDatiAggModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TUTTIFORNITORI.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        clientiFornitoriDatiAggModelImpl.getOriginalFornitore()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TUTTIFORNITORI,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TUTTIFORNITORI,
                    args);

                args = new Object[] {
                        clientiFornitoriDatiAggModelImpl.getFornitore()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TUTTIFORNITORI,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TUTTIFORNITORI,
                    args);
            }
        }

        EntityCacheUtil.putResult(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
            ClientiFornitoriDatiAggImpl.class,
            clientiFornitoriDatiAgg.getPrimaryKey(), clientiFornitoriDatiAgg);

        return clientiFornitoriDatiAgg;
    }

    protected ClientiFornitoriDatiAgg toUnwrappedModel(
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg) {
        if (clientiFornitoriDatiAgg instanceof ClientiFornitoriDatiAggImpl) {
            return clientiFornitoriDatiAgg;
        }

        ClientiFornitoriDatiAggImpl clientiFornitoriDatiAggImpl = new ClientiFornitoriDatiAggImpl();

        clientiFornitoriDatiAggImpl.setNew(clientiFornitoriDatiAgg.isNew());
        clientiFornitoriDatiAggImpl.setPrimaryKey(clientiFornitoriDatiAgg.getPrimaryKey());

        clientiFornitoriDatiAggImpl.setCodiceAnagrafica(clientiFornitoriDatiAgg.getCodiceAnagrafica());
        clientiFornitoriDatiAggImpl.setFornitore(clientiFornitoriDatiAgg.isFornitore());
        clientiFornitoriDatiAggImpl.setCodiceAgente(clientiFornitoriDatiAgg.getCodiceAgente());
        clientiFornitoriDatiAggImpl.setCodiceEsenzione(clientiFornitoriDatiAgg.getCodiceEsenzione());
        clientiFornitoriDatiAggImpl.setCategoriaEconomica(clientiFornitoriDatiAgg.getCategoriaEconomica());
        clientiFornitoriDatiAggImpl.setCodiceRegionale(clientiFornitoriDatiAgg.getCodiceRegionale());
        clientiFornitoriDatiAggImpl.setCodicePuntoVenditaGT(clientiFornitoriDatiAgg.getCodicePuntoVenditaGT());
        clientiFornitoriDatiAggImpl.setTipoPagamento(clientiFornitoriDatiAgg.getTipoPagamento());
        clientiFornitoriDatiAggImpl.setAssociato(clientiFornitoriDatiAgg.isAssociato());
        clientiFornitoriDatiAggImpl.setSconto(clientiFornitoriDatiAgg.getSconto());
        clientiFornitoriDatiAggImpl.setCodiceIBAN(clientiFornitoriDatiAgg.getCodiceIBAN());

        return clientiFornitoriDatiAggImpl;
    }

    /**
     * Returns the clienti fornitori dati agg with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the clienti fornitori dati agg
     * @return the clienti fornitori dati agg
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a clienti fornitori dati agg with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg findByPrimaryKey(Serializable primaryKey)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = fetchByPrimaryKey(primaryKey);

        if (clientiFornitoriDatiAgg == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchClientiFornitoriDatiAggException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return clientiFornitoriDatiAgg;
    }

    /**
     * Returns the clienti fornitori dati agg with the primary key or throws a {@link it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException} if it could not be found.
     *
     * @param clientiFornitoriDatiAggPK the primary key of the clienti fornitori dati agg
     * @return the clienti fornitori dati agg
     * @throws it.bysoftware.ct.NoSuchClientiFornitoriDatiAggException if a clienti fornitori dati agg with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg findByPrimaryKey(
        ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK)
        throws NoSuchClientiFornitoriDatiAggException, SystemException {
        return findByPrimaryKey((Serializable) clientiFornitoriDatiAggPK);
    }

    /**
     * Returns the clienti fornitori dati agg with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the clienti fornitori dati agg
     * @return the clienti fornitori dati agg, or <code>null</code> if a clienti fornitori dati agg with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        ClientiFornitoriDatiAgg clientiFornitoriDatiAgg = (ClientiFornitoriDatiAgg) EntityCacheUtil.getResult(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
                ClientiFornitoriDatiAggImpl.class, primaryKey);

        if (clientiFornitoriDatiAgg == _nullClientiFornitoriDatiAgg) {
            return null;
        }

        if (clientiFornitoriDatiAgg == null) {
            Session session = null;

            try {
                session = openSession();

                clientiFornitoriDatiAgg = (ClientiFornitoriDatiAgg) session.get(ClientiFornitoriDatiAggImpl.class,
                        primaryKey);

                if (clientiFornitoriDatiAgg != null) {
                    cacheResult(clientiFornitoriDatiAgg);
                } else {
                    EntityCacheUtil.putResult(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
                        ClientiFornitoriDatiAggImpl.class, primaryKey,
                        _nullClientiFornitoriDatiAgg);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(ClientiFornitoriDatiAggModelImpl.ENTITY_CACHE_ENABLED,
                    ClientiFornitoriDatiAggImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return clientiFornitoriDatiAgg;
    }

    /**
     * Returns the clienti fornitori dati agg with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param clientiFornitoriDatiAggPK the primary key of the clienti fornitori dati agg
     * @return the clienti fornitori dati agg, or <code>null</code> if a clienti fornitori dati agg with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ClientiFornitoriDatiAgg fetchByPrimaryKey(
        ClientiFornitoriDatiAggPK clientiFornitoriDatiAggPK)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) clientiFornitoriDatiAggPK);
    }

    /**
     * Returns all the clienti fornitori dati aggs.
     *
     * @return the clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ClientiFornitoriDatiAgg> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the clienti fornitori dati aggs.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of clienti fornitori dati aggs
     * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
     * @return the range of clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ClientiFornitoriDatiAgg> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the clienti fornitori dati aggs.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ClientiFornitoriDatiAggModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of clienti fornitori dati aggs
     * @param end the upper bound of the range of clienti fornitori dati aggs (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<ClientiFornitoriDatiAgg> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<ClientiFornitoriDatiAgg> list = (List<ClientiFornitoriDatiAgg>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_CLIENTIFORNITORIDATIAGG);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_CLIENTIFORNITORIDATIAGG;

                if (pagination) {
                    sql = sql.concat(ClientiFornitoriDatiAggModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<ClientiFornitoriDatiAgg>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<ClientiFornitoriDatiAgg>(list);
                } else {
                    list = (List<ClientiFornitoriDatiAgg>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the clienti fornitori dati aggs from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (ClientiFornitoriDatiAgg clientiFornitoriDatiAgg : findAll()) {
            remove(clientiFornitoriDatiAgg);
        }
    }

    /**
     * Returns the number of clienti fornitori dati aggs.
     *
     * @return the number of clienti fornitori dati aggs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_CLIENTIFORNITORIDATIAGG);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the clienti fornitori dati agg persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.it.bysoftware.ct.model.ClientiFornitoriDatiAgg")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<ClientiFornitoriDatiAgg>> listenersList = new ArrayList<ModelListener<ClientiFornitoriDatiAgg>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<ClientiFornitoriDatiAgg>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(ClientiFornitoriDatiAggImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
