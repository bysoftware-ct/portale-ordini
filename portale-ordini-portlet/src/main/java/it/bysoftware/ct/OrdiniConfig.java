package it.bysoftware.ct;

import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;

/**
 * Ordini configuration portlet implementation.
 *
 * @author Mario Torrisi torrisi.mario@gmail.com
 */
public class OrdiniConfig extends DefaultConfigurationAction {

    @Override
    public final void processAction(final PortletConfig portletConfig,
            final ActionRequest actionRequest,
            final ActionResponse actionResponse) throws Exception {

        super.processAction(portletConfig, actionRequest, actionResponse);
    }

}
