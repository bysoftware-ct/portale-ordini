package it.bysoftware.ct.returnable;

import it.bysoftware.ct.model.AnagraficheClientiFornitori;
import it.bysoftware.ct.model.MovimentoVuoto;
import it.bysoftware.ct.model.Vuoto;
import it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.MovimentoVuotoLocalServiceUtil;
import it.bysoftware.ct.service.VuotoLocalServiceUtil;
import it.bysoftware.ct.utils.Report;
import it.bysoftware.ct.utils.ResourceId;
import it.bysoftware.ct.utils.Response;
import it.bysoftware.ct.utils.Utils;
import it.bysoftware.ct.utils.Response.Code;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;

import net.sf.jasperreports.engine.JRException;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.servlet.ServletResponseUtil;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.portlet.documentlibrary.util.DLUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.liferay.util.portlet.PortletProps;

/**
 * Portlet implementation class ReturnableManagementPortlet.
 */
public class ReturnableManagementPortlet extends MVCPortlet {
 
    /**
     * Export folder path.
     */
    private static final String EXPORT_FOLDER = PortletProps.get(
            "export-folder");
    
    /**
     * Export folder path.
     */
    private static final String DOCUMETS_FOLDER = EXPORT_FOLDER + File.separator
            + "BUONI_CC" + File.separator;
    
    /**
     * No sobject specified constant.
     */
    private static final String NO_SUBJECT_SPECIFIED = "No subject specified";

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(
            ReturnableManagementPortlet.class);

    /**
     * Data source connection.
     */
    private DataSource ds;

    @Override
    public final void init(final PortletConfig config) throws PortletException {
        Context ctx;
        try {
            ctx = new InitialContext();
            this.ds = (DataSource) ctx.lookup("jdbc/OrdiniPool");
        } catch (NamingException e) {
            this.logger.fatal(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        super.init(config);
    }
    
    @Override
    public final void serveResource(final ResourceRequest resourceRequest,
            final ResourceResponse resourceResponse) throws IOException,
            PortletException {
        String resourceID = resourceRequest.getResourceID();
        PrintWriter out;
        Response response;
        switch (ResourceId.valueOf(resourceID)) {
            case saveMovements:
                this.logger.info("Saving movements");
                response = this.saveMovements(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case printMovements:
                this.logger.info("Printing movements.");
                response = this.printMovements(resourceRequest);
                if (response.getCode() == Code.OK.ordinal()) {
                    File f = new File(response.getMessage());
                    ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.
                            getAttribute(WebKeys.THEME_DISPLAY);
                    User company = themeDisplay.getUser();
                    long groupId = themeDisplay.getLayout().getGroupId();
                    long repoId = themeDisplay.getScopeGroupId();
                    DLFolder companyFolder;
                    try {
                        companyFolder = Utils.getAziendaFolder(groupId,
                                company);
                        DLFolder itemSheetFolder = Utils.getPicturesFolder(
                                groupId, companyFolder);
                        FileEntry fileEntry = DLAppServiceUtil.addFileEntry(
                                repoId, itemSheetFolder.getFolderId(),
                                f.getName(), MimeTypesUtil.getContentType(f),
                                f.getName(), "", "", f, new ServiceContext());
                        this.logger.debug("Added: " + fileEntry.getTitle()
                                + " to: /" + companyFolder.getName());
                        response.setMessage(DLUtil.getPreviewURL(fileEntry,
                                fileEntry.getFileVersion(), themeDisplay, ""));
                    } catch (PortalException e) {
                        this.logger.error(e.getMessage());
                        if (this.logger.isDebugEnabled()) {
                            e.printStackTrace();                        
                        }
                    } catch (SystemException e) {
                        this.logger.error(e.getMessage());
                        if (this.logger.isDebugEnabled()) {
                            e.printStackTrace();                        
                        }
                    }
                }
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case downloadMovements:
				String code = ParamUtil.getString(resourceRequest,
				        "code", "");
				Date dateFrom;
				Date dateTo;
				try {
					dateFrom = new Date(ParamUtil.getLong(resourceRequest,
					        "dateFrom"));
					this.logger.debug("dateFrom: " + dateFrom);
					dateTo = new Date(ParamUtil.getLong(resourceRequest,
					        "dateTo"));
					this.logger.debug("dateTo: " + dateTo);
					User user = UserLocalServiceUtil.getUser(Long.
	                        parseLong(resourceRequest.getRemoteUser()));
					response = this.generateFile(user, code, dateFrom, dateTo);
					if (response.getCode() == 0) {
					    File file = new File(response.getMessage());
					    InputStream in = new FileInputStream(file);
					    HttpServletResponse httpRes = PortalUtil.
					            getHttpServletResponse(resourceResponse);
					    HttpServletRequest httpReq = PortalUtil.
					            getHttpServletRequest(resourceRequest);
					    ServletResponseUtil.sendFile(httpReq, httpRes,
					            file.getName(), in, "application/download");
					    in.close();
					} else {
					    out = resourceResponse.getWriter();
					    out.print(JSONFactoryUtil.looseSerialize(response));
					    out.flush();
					    out.close();
					}
				} catch (PortalException e) {
					this.logger.error(e.getMessage());
                    if (this.logger.isDebugEnabled()) {
                        e.printStackTrace();                        
                    }
					out = resourceResponse.getWriter();
                    out.print(JSONFactoryUtil.looseSerialize(new Response(
                            Code.GENERIC_ERROR, e.getMessage())));
                    out.flush();
                    out.close();
				} catch (SystemException e) {
					this.logger.error(e.getMessage());
                    if (this.logger.isDebugEnabled()) {
                        e.printStackTrace();                        
                    }
					out = resourceResponse.getWriter();
                    out.print(JSONFactoryUtil.looseSerialize(new Response(
                            Code.GENERIC_ERROR, e.getMessage())));
                    out.flush();
                    out.close();
				}
            	break;
            default:
                break;
        }
    }

    private Response generateFile(final User u, final String subjectCode,
    		final Date dateFrom, final Date dateTo) throws PortalException,
    		SystemException {
    	AnagraficheClientiFornitori supplier =
    			AnagraficheClientiFornitoriLocalServiceUtil.
    				getAnagraficheClientiFornitori(subjectCode);
		List<Vuoto> returnables = VuotoLocalServiceUtil.getVuotos(0,
				VuotoLocalServiceUtil.getVuotosCount());
//		OrderByComparator comparator = OrderByComparatorFactoryUtil.
//				create("MovimentoVuoto", "dataMovimento", "uuid",
//						"codiceVuoto", false);
		List<MovimentoVuoto> giveMovs = MovimentoVuotoLocalServiceUtil.
				findMovementiByCodiceSoggetto(subjectCode, 1, dateFrom, dateTo,
						0, MovimentoVuotoLocalServiceUtil.getMovimentoVuotosCount(),
						null);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyy");
		Map<Long, List<Object>> rows = new TreeMap<Long, List<Object>>();
		for (MovimentoVuoto move : giveMovs) {
			Long k = move.getDataMovimento().toInstant().getEpochSecond() + move.getUuid();
			if (rows.containsKey(k)) {
				rows.get(k).add((int)move.getQuantita());
			} else {
				List<Object> l = new ArrayList<Object>();
				l.add(sdf.format(move.getDataMovimento()));
				l.add(move.getUuid());
				l.add(move.getNote());
				l.add((int)move.getQuantita());
				rows.put(k, l);
			}
		}
		List<MovimentoVuoto> getMovs = MovimentoVuotoLocalServiceUtil.
				findMovementiByCodiceSoggetto(subjectCode, -1, dateFrom, dateTo,
						0, MovimentoVuotoLocalServiceUtil.getMovimentoVuotosCount(),
						null);
		for (MovimentoVuoto move : getMovs) {
			Long k = move.getDataMovimento().toInstant().getEpochSecond() + move.getUuid();
			if (rows.containsKey(k)) {
				rows.get(k).add((int)move.getQuantita());
			} else {
				List<Object> l = new ArrayList<Object>();
				l.add(sdf.format(move.getDataMovimento()));
				l.add(move.getUuid());
				l.add(move.getNote());
				l.add((int)move.getQuantita());
				rows.put(k, l);
			}
		}
		HSSFWorkbook workbook = new HSSFWorkbook();
        File f = FileUtil.createTempFile("xls");
        HSSFSheet sheet = workbook.createSheet(f.getName());

        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
        int index = 1;

        data.put(++index, new String[] { supplier.getRagioneSociale1() + " "
        		+ supplier.getRagioneSociale2() });
        data.put(++index, new String[] { "MOVIMENTI DAL "
                + sdf.format(dateFrom) + " AL " + sdf.format(dateTo) });
         
        List<Object> column = new ArrayList<Object>();
        column.add("DATA");
        column.add("NUMERO");
        column.add("NOTE");
        for (Vuoto v : returnables) {
			column.add(v.getNome());
        }
        int oldSize = column.size();
        
        for (Vuoto v : returnables) {
			column.add(v.getNome());
		}
        
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, column.size() - 1));
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, column.size() - 1));
        data.put(++index, new String[] { u.getFullName() + " consegna a: "
        		+ supplier.getRagioneSociale1() + " " + supplier.getRagioneSociale2(), "", "", "", "",
        		"", "", "", "", u.getFullName() + " riceve da: " + supplier.getRagioneSociale1()
        		+ " " + supplier.getRagioneSociale2() });
        sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, oldSize - 1));
        sheet.addMergedRegion(new CellRangeAddress(2, 2, oldSize, column.size() - 1));
        data.put(++index, column.toArray(new String[column.size()]));
        for (Long k : rows.keySet()) {
        	data.put(++index, rows.get(k).toArray(new Object[rows.get(k).size()]));
		}
        Set<Integer> keyset = data.keySet();
        int rownum = 0;
        for (Integer key : keyset) {
        	Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
            	Cell cell = row.createCell(cellnum++);
            	if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                	cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                } else if (obj instanceof Integer) {
                    cell.setCellValue((Integer) obj);
                }
            }
        }
        HSSFCell firstCell = workbook.getSheetAt(0).getRow(0).getCell(0);
        HSSFCellStyle cellStyle = workbook.getSheetAt(0).getRow(0).getSheet().
                getWorkbook().createCellStyle();
        HSSFFont boldfont = workbook.createFont();
        boldfont.setBold(true);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setFont(boldfont);
        firstCell.setCellStyle(cellStyle);
        int noOfColumns = workbook.getSheetAt(0).getRow(3).getLastCellNum();
        for (int i = 0; i < noOfColumns; i++) {
            workbook.getSheetAt(0).autoSizeColumn(i);
        }
    	Code code = null;
        String message = null;
        try {
            FileOutputStream out = new FileOutputStream(f);
            workbook.write(out);
            out.close();
            this.logger.info("Excel written successfully: "
                    + f.getPath());
            code = Code.OK;
            message = f.getPath();
        } catch (FileNotFoundException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            code = Code.GENERIC_ERROR;
            message = e.getMessage();
        } catch (IOException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            code = Code.GENERIC_ERROR;
            message = e.getMessage();
        }
        return new Response(code, message);
	}

	/**
     * Prints returnable movements.
     * 
     * @param resourceRequest {@link ResourceRequest}
     * @return return {@link Response}
     */
    private Response printMovements(final ResourceRequest resourceRequest) {
        String partnerCode = ParamUtil.getString(resourceRequest,
                "partnerCode", null);
        this.logger.debug("partnerCode: " + partnerCode);
        String num = ParamUtil.getString(resourceRequest,
                "num", "");
        this.logger.debug("NUM: " + num);
        boolean empty = ParamUtil.getBoolean(resourceRequest, "empty");
        this.logger.debug("EMPTY: " + empty);
        String message = null;
        Code c = null;
        if (partnerCode != null) {
            try {
                Report report = new Report(this.ds.getConnection());
                User user = UserLocalServiceUtil.getUser(Long.
                        parseLong(resourceRequest.getRemoteUser()));
                this.logger.debug("DATALNG: " + ParamUtil.getLong(resourceRequest,
                        "date"));
                Date date = new Date(ParamUtil.getLong(resourceRequest,
                        "date"));
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyy");
                this.logger.debug("DATA: " + sdf.format(date));
                String filePath = report.print(user.getUserId(),
                        user.getScreenName(), "returnable-movements",
                        partnerCode, num, empty, date);

                report.closeConnection();
                if (!empty && !num.isEmpty()) {
                	String dateStr = new SimpleDateFormat("yyyyMMdd_HH-mm-ss").
                            format(new Date());
                    File pdf = new File(DOCUMETS_FOLDER + partnerCode + '_'
                            + "BUONO_CC" + '_' + dateStr + ".pdf");
                    FileUtil.copyFile(filePath, pdf.getAbsolutePath());
				}

                c = Response.Code.OK;
                message = filePath;
            } catch (SQLException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (NumberFormatException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (PortalException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (SystemException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (JRException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (IOException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
			}
            return new Response(c, message);
        } else {
            c = Response.Code.GENERIC_ERROR;
            message = ReturnableManagementPortlet.NO_SUBJECT_SPECIFIED;
        }
        return null;
    }

    /**
     * Add new movement for a returnable item.
     * 
     * @param areq
     *            {@link ActionRequest}
     * @param ares
     *            {@link ActionResponse}
     */
    public final void addMovements(final ActionRequest areq,
            final ActionResponse ares) {
        this.logger.debug("Adding new movements...");
        int num = -1;
        List<MovimentoVuoto> added = new ArrayList<MovimentoVuoto>();
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date date = formatter.parse(ParamUtil.getString(areq,
                    "movementDate"));
            List<Vuoto> list = VuotoLocalServiceUtil.getVuotos(0,
                    VuotoLocalServiceUtil.getVuotosCount());
            List<MovimentoVuoto> movements = new ArrayList<MovimentoVuoto>();
            num = MovimentoVuotoLocalServiceUtil.getNumber(
            		ParamUtil.getString(areq, "partnerCode"), date);
            for (int i = 0; i < list.size(); i++) {
                double quant = ParamUtil.getDouble(areq, list.get(i).
                        getCodiceVuoto() + "_giveTo", 0);
                MovimentoVuoto m = null;
                m = MovimentoVuotoLocalServiceUtil.createMovimentoVuoto(0);
                m.setCodiceVuoto(list.get(i).getCodiceVuoto());
                m.setDataMovimento(date);
                m.setCodiceSoggetto(ParamUtil.getString(areq,
                        "partnerCode"));
                m.setQuantita(quant);
                m.setTipoMovimento(1);
                m.setNote(ParamUtil.getString(areq, "note"));
                m.setUuid(num);
                movements.add(m);
                
                quant = ParamUtil.getDouble(areq, list.get(i).getCodiceVuoto()
                        + "_getFrom", 0);
                m = MovimentoVuotoLocalServiceUtil.createMovimentoVuoto(0);
                m.setCodiceVuoto(list.get(i).getCodiceVuoto());
                m.setDataMovimento(date);
                m.setCodiceSoggetto(ParamUtil.getString(areq,
                        "partnerCode"));
                m.setQuantita(quant);
                m.setTipoMovimento(-1);
                m.setNote(ParamUtil.getString(areq, "note"));
                m.setUuid(num);
                movements.add(m);
            }
            
            for (MovimentoVuoto movement : movements) {
                this.logger.debug(movement);
                added.add(MovimentoVuotoLocalServiceUtil.updateMovimentoVuoto(
                        movement));
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            this.deleteMovemnts(added);
        } catch (ParseException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            this.deleteMovemnts(added);
        } catch (NumberFormatException e) {
        	this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            this.deleteMovemnts(added);
		}
        PortalUtil.copyRequestParameters(areq, ares);
        ares.setRenderParameter("num", String.valueOf(num));
        ares.setRenderParameter("jspPage",
                "/html/returnablemanagement/new-movements.jsp");
    }
    
	/**
     * Stores movements into the database.
     * 
     * @param resourceRequest
     *            {@link ResourceRequest}
     * @return {@link Response} object containing the data stored.
     */
    private Response saveMovements(final ResourceRequest resourceRequest) {
        JSONArray data;
        String message = null;
        Code c = null;
        List<MovimentoVuoto> list = new ArrayList<MovimentoVuoto>();
        try {
            data = JSONFactoryUtil.createJSONArray(ParamUtil.getString(
                    resourceRequest, "rows"));
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            for (int i = 0; i < data.length(); i++) {
                int id = data.getJSONObject(i).getInt("id", 0);
                MovimentoVuoto m = null;
                if (id == 0) {
                    m = MovimentoVuotoLocalServiceUtil.createMovimentoVuoto(id);
                } else {
                    m = MovimentoVuotoLocalServiceUtil.getMovimentoVuoto(id);
                }
                m.setCodiceVuoto(data.getJSONObject(i).getString(
                        "codiceVuoto"));
                m.setDataMovimento(formatter.parse(
                        data.getJSONObject(i).getString("dataMovimento")));
                m.setCodiceSoggetto(data.getJSONObject(i).getString(
                        "codiceSoggetto"));
                m.setQuantita(data.getJSONObject(i).getDouble("quantita"));
                m.setTipoMovimento(
                        data.getJSONObject(i).getInt("tipoMovimento"));
                m.setNote(data.getJSONObject(i).getString("note"));
                MovimentoVuotoLocalServiceUtil.updateMovimentoVuoto(m);
                list.add(m);
            }
            c = Code.OK;
            message = data.toString();
        } catch (JSONException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            this.deleteMovemnts(list);
            c = Response.Code.GENERIC_ERROR;
            message = e.getMessage();
        } catch (ParseException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            this.deleteMovemnts(list);
            c = Response.Code.GENERIC_ERROR;
            message = e.getMessage();
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            this.deleteMovemnts(list);
            c = Response.Code.GENERIC_ERROR;
            message = e.getMessage();
        } catch (PortalException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            this.deleteMovemnts(list);
            c = Response.Code.GENERIC_ERROR;
            message = e.getMessage();
        }
        return new Response(c, message);
    }

    /** 
     * Delete the specified movements form the database.
     * @param list list of movements to remove.
     */
    private void deleteMovemnts(final List<MovimentoVuoto> list) {
        for (MovimentoVuoto m : list) {
            try {
                MovimentoVuotoLocalServiceUtil.deleteMovimentoVuoto(m);
            } catch (SystemException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
            }
        }
    }
}
