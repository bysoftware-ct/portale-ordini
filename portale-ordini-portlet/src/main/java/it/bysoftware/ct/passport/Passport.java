package it.bysoftware.ct.passport;

import it.bysoftware.ct.model.Carrello;
import it.bysoftware.ct.model.Ordine;
import it.bysoftware.ct.model.Piante;
import it.bysoftware.ct.model.Riga;
import it.bysoftware.ct.scheduler.service.GiveDocumentExporterService;
import it.bysoftware.ct.service.CarrelloLocalServiceUtil;
import it.bysoftware.ct.service.OrdineLocalServiceUtil;
import it.bysoftware.ct.service.PianteLocalServiceUtil;
import it.bysoftware.ct.service.RigaLocalServiceUtil;
import it.bysoftware.ct.service.persistence.TestataDocumentoPK;
import it.bysoftware.ct.utils.OrderState;
import it.bysoftware.ct.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.naming.NamingException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import net.sf.jasperreports.engine.JRException;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.liferay.util.portlet.PortletProps;

/**
 * Portlet implementation class Passport.
 */
public class Passport extends MVCPortlet {

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private static Log logger = LogFactoryUtil.getLog(Passport.class);

    /**
     * Stores the passport number for the current order.
     * 
     * @param areq
     *            the request {@link ActionRequest}
     * @param ares
     *            the response {@link ActionResponse}
     */
    public final void savePassportNumber(final ActionRequest areq,
            final ActionResponse ares) {
        Passport.logger.info("Saving passport number...");
        int passport = ParamUtil.getInteger(areq, "passportNumber");
        final long orderId = ParamUtil.getLong(areq, "orderId");
        final String supplierCode = ParamUtil.getString(areq, "supplierCode",
                "");

        Passport.logger.info("PASSPORT: " + passport + ", ORDER ID: " + orderId
                + ", SUPPLIER CODE: " + supplierCode);

        List<Carrello> carts = CarrelloLocalServiceUtil.findCartsInOrder(
                orderId);
        boolean ready = true;
        try {
            for (Carrello cart : carts) {
                List<Riga> rows = RigaLocalServiceUtil.findRowsInCart(cart
                        .getId());
                for (Riga row : rows) {
                    Piante p = PianteLocalServiceUtil.getPiante(
                            row.getIdArticolo());
                    if (supplierCode.equals(p.getIdFornitore())
                            && !row.getCodiceRegionale().isEmpty()) {
                        row.setPassaporto(String.valueOf(passport));
                        RigaLocalServiceUtil.updateRiga(row);
                    }
                    ready = ready && (row.getCodiceRegionale().isEmpty()
                            || !row.getPassaporto().isEmpty());
                }
            }
            if (ready) {
                Ordine order = OrdineLocalServiceUtil.getOrdine(orderId);
                order.setStato(OrderState.CONFIRMED.getValue());
                OrdineLocalServiceUtil.updateOrdine(order);
            }
            
            
            Thread t = new Thread(new Runnable() {

                @Override
                public void run() {
                    GiveDocumentExporterService service =
                            GiveDocumentExporterService.getInstance();
                    try {
                        File exportFolder = new File(PortletProps.get(
                                "export-folder"));
                        TestataDocumentoPK[] documents =
                                service.createTodayGiveDocuments(orderId,
                                        supplierCode);
                        Utils.updateCheckFile(exportFolder, documents,
                                "GIVE DOCUMENTS CREATED AFTER PARTNER: "
                        + supplierCode + " ADDED HIS PASSPORT NUMBER.");
                        
                    } catch (AddressException e) {
                        Passport.logger.error(e.getMessage());
                        if (Passport.logger.isDebugEnabled()) {
                            e.printStackTrace();
                        }
                    } catch (NamingException e) {
                        Passport.logger.error(e.getMessage());
                        if (Passport.logger.isDebugEnabled()) {
                            e.printStackTrace();
                        }
                    } catch (SQLException e) {
                        Passport.logger.error(e.getMessage());
                        if (Passport.logger.isDebugEnabled()) {
                            e.printStackTrace();
                        }
                    } catch (IOException e) {
                        Passport.logger.error(e.getMessage());
                        if (Passport.logger.isDebugEnabled()) {
                            e.printStackTrace();
                        }
                    } catch (JRException e) {
                        Passport.logger.error(e.getMessage());
                        if (Passport.logger.isDebugEnabled()) {
                            e.printStackTrace();
                        }
                    } catch (PortalException e) {
                        Passport.logger.error(e.getMessage());
                        if (Passport.logger.isDebugEnabled()) {
                            e.printStackTrace();
                        }
                    } catch (SystemException e) {
                        Passport.logger.error(e.getMessage());
                        if (Passport.logger.isDebugEnabled()) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            t.run();
        } catch (PortalException e) {
            Passport.logger.error(e.getMessage());
            SessionErrors.add(areq, "error-saving-passport", e.getMessage());
            if (Passport.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (SystemException e) {
            Passport.logger.error(e.getMessage());
            SessionErrors.add(areq, "error-saving-passport", e.getMessage());
            if (Passport.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
    }
    
	public final void saveDefaultPassportNumber(final ActionRequest areq,
			final ActionResponse ares) {
		Passport.logger.info("Saving default passport numbers...");
		final long orderId = ParamUtil.getLong(areq, "orderId");
		final String supplierCode = ParamUtil.getString(areq, "partnerCode",
				"");
		final String[] itemCodes = StringUtil.split(ParamUtil.getString(areq,
				"itemCodes"));
		Passport.logger.debug(ParamUtil.getString(areq, "customerCode"));
		for (String string : itemCodes) {
			if (!string.isEmpty() && !string.equals("on")) {
				Piante p = PianteLocalServiceUtil.findPlants(string);
				try {
					this.setTraceabilityCode(p.getPassaportoDefault(),
							p.getId(), orderId, supplierCode);
				} catch (PortalException e) {
					Passport.logger.error(e.getMessage());
					SessionErrors.add(areq, "error-saving-passport",
							e.getMessage());
					if (Passport.logger.isDebugEnabled()) {
						e.printStackTrace();
					}
				} catch (SystemException e) {
					Passport.logger.error(e.getMessage());
					SessionErrors.add(areq, "error-saving-passport",
							e.getMessage());
					if (Passport.logger.isDebugEnabled()) {
						e.printStackTrace();
					}
				}
			}
			PortalUtil.copyRequestParameters(areq, ares);
			ares.setRenderParameter("jspPage", "/html/passport/order.jsp");
		}
	}
    
    public final void saveTraceabilityCode(final ActionRequest areq,
            final ActionResponse ares) {
    	
    	Passport.logger.info("Saving passport number...");
    	String code = ParamUtil.getString(areq, "code");
//        int passport = ParamUtil.getInteger(areq, "passportNumber");
    	
    	final long piantaId = ParamUtil.getLong(areq, "piantaId");
    	final long orderId = ParamUtil.getLong(areq, "orderId");
        final String supplierCode = ParamUtil.getString(areq, "partnerCode",
                "");

        Passport.logger.info("PASSPORT: " + code + ", ORDER ID: " + orderId
                + ", SUPPLIER CODE: " + supplierCode);
       
        try {
        	setTraceabilityCode(code, piantaId, orderId, supplierCode);
        } catch (PortalException e) {
            Passport.logger.error(e.getMessage());
            SessionErrors.add(areq, "error-saving-passport", e.getMessage());
            if (Passport.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (SystemException e) {
            Passport.logger.error(e.getMessage());
            SessionErrors.add(areq, "error-saving-passport", e.getMessage());
            if (Passport.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        
    	PortalUtil.copyRequestParameters(areq, ares);
        ares.setRenderParameter("jspPage", "/html/passport/order.jsp");
    }


	private void setTraceabilityCode(String code, final long piantaId,
			final long orderId, final String supplierCode)
			throws PortalException, SystemException {
		Passport.logger.debug("Saving passport: " + code + " for item id: " +
			piantaId + " in order: " + orderId + " and supplier: " +
				supplierCode );
		boolean ready = true;
		boolean export = true;
		List<Carrello> carts = CarrelloLocalServiceUtil.findCartsInOrder(
		        orderId);
		for (Carrello cart : carts) {
		    List<Riga> rows = RigaLocalServiceUtil.findRowsInCart(cart
		            .getId());
		    for (Riga row : rows) {
		        Piante p = PianteLocalServiceUtil.getPiante(
		                row.getIdArticolo());
		        if (p.getId() == piantaId 
		        		&& !row.getCodiceRegionale().isEmpty()) {
		            row.setPassaporto(String.valueOf(code));
		            RigaLocalServiceUtil.updateRiga(row);
		        }
		        
		        if (p.getIdFornitore().equals(supplierCode)) {
					export = export && !row.getPassaporto().isEmpty();
				}
		        
		        ready = ready && !row.getPassaporto().isEmpty();
		    }
		}
		Ordine order = OrdineLocalServiceUtil.getOrdine(orderId);
		if (ready) {
		    order.setStato(OrderState.CONFIRMED.getValue());
		    if (order.getDataConsegna().compareTo(Utils.today().
		            getTime()) == 0) {
		    	order.setDdtGenerati(true);
			}
		    OrdineLocalServiceUtil.updateOrdine(order);
		}
		if (export && order.getDataConsegna().compareTo(Utils.today().
		        getTime()) == 0) {
			Thread t = exportGiveDocuments(orderId, supplierCode);
			t.run();
		}
	}


	private Thread exportGiveDocuments(final long orderId,
			final String supplierCode) {
		return new Thread(new Runnable() {

			@Override
			public void run() {
				GiveDocumentExporterService service = GiveDocumentExporterService
						.getInstance();
				try {
					File exportFolder = new File(PortletProps
							.get("export-folder"));
					TestataDocumentoPK[] documents = service
							.createTodayGiveDocuments(orderId,
									supplierCode);
					Utils.updateCheckFile(exportFolder, documents,
							"GIVE DOCUMENTS CREATED AFTER PARTNER: "
									+ supplierCode
									+ " ADDED HIS PASSPORT NUMBER.");

				} catch (AddressException e) {
					Passport.logger.error(e.getMessage());
					if (Passport.logger.isDebugEnabled()) {
						e.printStackTrace();
					}
				} catch (NamingException e) {
					Passport.logger.error(e.getMessage());
					if (Passport.logger.isDebugEnabled()) {
						e.printStackTrace();
					}
				} catch (SQLException e) {
					Passport.logger.error(e.getMessage());
					if (Passport.logger.isDebugEnabled()) {
						e.printStackTrace();
					}
				} catch (IOException e) {
					Passport.logger.error(e.getMessage());
					if (Passport.logger.isDebugEnabled()) {
						e.printStackTrace();
					}
				} catch (JRException e) {
					Passport.logger.error(e.getMessage());
					if (Passport.logger.isDebugEnabled()) {
						e.printStackTrace();
					}
				} catch (PortalException e) {
					Passport.logger.error(e.getMessage());
					if (Passport.logger.isDebugEnabled()) {
						e.printStackTrace();
					}
				} catch (SystemException e) {
					Passport.logger.error(e.getMessage());
					if (Passport.logger.isDebugEnabled()) {
						e.printStackTrace();
					}
				}
			}
		});
	}
}
