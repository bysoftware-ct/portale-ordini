package it.bysoftware.ct.items;

import it.bysoftware.ct.model.Piante;
import it.bysoftware.ct.service.PianteLocalServiceUtil;
import it.bysoftware.ct.utils.Utils;

import java.io.File;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.util.PortalUtil;

public class ItemsConfig extends DefaultConfigurationAction {

	private Log logger = LogFactoryUtil.getLog(ItemsConfig.class);

	@Override
	public final void processAction(final PortletConfig portletConfig,
			final ActionRequest actionRequest,
			final ActionResponse actionResponse) throws Exception {
		HttpServletRequest httpRequest = PortalUtil
				.getHttpServletRequest(actionRequest);
		String portalURL = PortalUtil.getPortalURL(httpRequest);
		List<Piante> items = PianteLocalServiceUtil.findPlants(true, true, "",
				"", "", false);
		int updated = 0;
		int notUpdated = 0;
		for (Piante item : items) {
			if (!item.getPathFile().isEmpty() && !item.getFoto1().isEmpty()) {
				try {
					item.setFoto3(portalURL + File.separator
							+ Utils.retrievePictureURL(item));
					PianteLocalServiceUtil.updatePiante(item);
					updated++;
				} catch (PortalException ex) {
					logger.debug(ex.getMessage());
					notUpdated++;
				} catch (SystemException ex) {
					logger.debug(ex.getMessage());
					notUpdated++;
				}
			}
		}
		SessionMessages.add(actionRequest, "requestProcessed",
				"Link aggiunto a: " + updated + " piante, per " + notUpdated
						+ " piante non e' stato possibile aggiungere il link.");
		super.processAction(portletConfig, actionRequest, actionResponse);
	}
}
