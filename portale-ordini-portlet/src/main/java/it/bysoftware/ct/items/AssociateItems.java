package it.bysoftware.ct.items;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.opencsv.CSVReader;

import it.bysoftware.ct.model.Articoli;
import it.bysoftware.ct.model.ArticoloSocio;
import it.bysoftware.ct.model.DescrizioniVarianti;
import it.bysoftware.ct.model.Piante;
import it.bysoftware.ct.service.ArticoliLocalServiceUtil;
import it.bysoftware.ct.service.ArticoloSocioLocalServiceUtil;
import it.bysoftware.ct.service.DescrizioniVariantiLocalServiceUtil;
import it.bysoftware.ct.service.PianteLocalServiceUtil;
import it.bysoftware.ct.service.persistence.ArticoloSocioPK;
import it.bysoftware.ct.utils.Constants;
import it.bysoftware.ct.utils.ResourceId;
import it.bysoftware.ct.utils.Response;
import it.bysoftware.ct.utils.Utils;
import it.bysoftware.ct.utils.Response.Code;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;



/**
 * Portlet implementation class AssociateItems.
 */
public class AssociateItems extends MVCPortlet {

    /**
     * File upload constant.
     */
    private static final String FILEUPLOAD = "fileupload";

    /**
     * Index of partner item code.
     */
    private static final int PARTNER_ITEM_CODE = 1;
    
    /**
     * Index of partner variant code.
     */
    private static final int PARTNER_VARIANT_CODE = 3;
    
    /**
     * Index of company item code.
     */
    private static final int ITEM_CODE = 5;

    /**
     * Index of company variant code.
     */
    private static final int VARIANT_CODE = 6;

    /**
     * Items constant.
     */
    private static final String ITEMS = "items";

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(AssociateItems.class);

    /**
     * Activates or Deactivates a user based on his current state.
     * 
     * @param areq
     *            {@link ActionRequest}
     * @param ares
     *            {@link ActionResponse}
     */
    public final void upload(final ActionRequest areq,
            final ActionResponse ares) {
        this.logger.info("Uploading Item...");
        UploadPortletRequest uploadRequest = PortalUtil
                .getUploadPortletRequest(areq);
        String backUrl = ParamUtil.getString(uploadRequest,
                "backUrl");
        String partnerCode = ParamUtil.getString(uploadRequest,
                "partnerCode");
        File tmpFolder = new File(System.getProperty("java.io.tmpdir"));
        if (!uploadRequest.getFileName(AssociateItems.FILEUPLOAD).
                isEmpty()) {
            if (tmpFolder.getUsableSpace() < Constants.ONE_GB) {
                this.logger.warn("No enough space in: "
                        + tmpFolder.getAbsolutePath());
                SessionErrors.add(areq, "disk-space");
            } else if (uploadRequest.getSize(
                    AssociateItems.FILEUPLOAD) == 0) {
                this.logger.warn("Uploaded an empty file.");
                SessionErrors.add(areq, "empty-file");
            } else {
                File file = uploadRequest.getFile(FILEUPLOAD);
                    try {
                        File csvFile = this.uploadFile(file, tmpFolder);
                        @SuppressWarnings("resource")
                        CSVReader reader = new CSVReader(new FileReader(
                                csvFile), ';', '\"', 1);
                        List<String[]> data = reader.readAll();
                        List<ArticoloSocio> items = this.mapToItems(partnerCode,
                                data);
                        areq.setAttribute("dataJson", JSONFactoryUtil.
                                createJSONArray(JSONFactoryUtil.looseSerialize(
                                        items)));
                    } catch (IOException e) {
                        this.logger.error(e.getMessage());
                        if (this.logger.isDebugEnabled()) {
                            e.printStackTrace();
                        }
                        SessionErrors.add(areq, e.getMessage());
                    } catch (JSONException e) {
                        this.logger.error(e.getMessage());
                        if (this.logger.isDebugEnabled()) {
                            e.printStackTrace();
                        }
                        SessionErrors.add(areq, e.getMessage());
                    }
                    
            }
        }
        ares.setRenderParameter("backUrl", backUrl);
        ares.setRenderParameter("partnerCode", partnerCode);
        ares.setRenderParameter("jspPage", "/html/associateitems/upload.jsp");
    }
    
    /**
     * Delete partner item association.
     * 
     * @param areq
     *            {@link ActionRequest}
     * @param ares
     *            {@link ActionResponse}
     */
    public final void delete(final ActionRequest areq,
            final ActionResponse ares) {
        this.logger.info("Deleting association...");
        String backUrl = ParamUtil.getString(areq, "backUrl");
        String partner = ParamUtil.getString(areq, "partner");
        String partnerItemCode = ParamUtil.getString(areq, "partnerItemCode");
        String partnerItemvariant = ParamUtil.getString(areq,
                "partnerItemVariant");
        try {
            ArticoloSocio partnerItem = ArticoloSocioLocalServiceUtil.
                    getArticoloSocio(new ArticoloSocioPK(partner,
                            partnerItemCode, partnerItemvariant));
            ArticoloSocioLocalServiceUtil.deleteArticoloSocio(partnerItem);
        } catch (PortalException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            SessionErrors.add(areq, e.getMessage());
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            SessionErrors.add(areq, e.getMessage());
        }
        
        ares.setRenderParameter("backUrl", backUrl);
        ares.setRenderParameter("partnerCode", partner);
        ares.setRenderParameter("jspPage",
                "/html/associateitems/associate-items-list.jsp");
    }
    
    @Override
    public final void serveResource(final ResourceRequest resourceRequest,
            final ResourceResponse resourceResponse) throws IOException,
            PortletException {
        String resourceID = resourceRequest.getResourceID();
        PrintWriter out;
        Response response;
        switch (ResourceId.valueOf(resourceID)) {
            case saveAssociation:
                this.logger.debug("Saving associations");
                response = this.saveAssociation(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case autoComplete:
                this.logger.debug("Looking for plants...");
                JSONArray results = this.getItems(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(results.toString());
                out.flush();
                out.close();
                break;
            case findAssociation:
                this.logger.info("Looking for existing association...");
                response = this.findAssociation(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case editAssociation:
                this.logger.info("Editing association...");
                response = this.editAssociation(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            default:
                this.logger.warn("Unknown operation: " + resourceID);
                break;
        }
    }
    
    /**
     * Edit item association.
     * 
     * @param resourceRequest @link {@link ResourceRequest} object
     * @return return {@link Response}
     */
    private Response editAssociation(final ResourceRequest resourceRequest) {
        boolean add = ParamUtil.getBoolean(resourceRequest, "add");
        String partnerCode = ParamUtil.getString(resourceRequest,
                "partnerCode");
        String itemCode = ParamUtil.getString(resourceRequest, "itemCode");
        String variantCode = ParamUtil.getString(resourceRequest, "variants");
        String oldVariantCode = ParamUtil.getString(resourceRequest,
                "oldVariantCode");
        String partnerItemCode = ParamUtil.getString(resourceRequest,
                "partnerItemCode");
        String partnerItemVariant = ParamUtil.getString(resourceRequest,
                "partnerItemVariant");
        this.logger.info("add: " + add);
        this.logger.info("partnerCode: " + partnerCode);
        this.logger.info("itemCode: " + itemCode);
        this.logger.info("variantCode: " + variantCode);
        this.logger.info("partnerItemCode: " + partnerItemCode);
        this.logger.info("partnerItemVariant: " + partnerItemVariant);
        this.logger.info("oldVariantCode: " + oldVariantCode);
        ArticoloSocio partnerItem = ArticoloSocioLocalServiceUtil
                .findPartnerItems(partnerCode, itemCode, oldVariantCode);
        this.logger.info("1. partnerItem: " + partnerItem);
        if (add && partnerItem == null) {
            partnerItem = ArticoloSocioLocalServiceUtil
                    .createArticoloSocio(new ArticoloSocioPK(partnerCode,
                            partnerItemCode, partnerItemVariant));
            partnerItem.setCodiceArticolo(itemCode);
            partnerItem.setCodiceVariante(variantCode);
        } else if (!add && partnerItem != null) {
            partnerItem.setCodiceArticoloSocio(partnerItemCode);
            partnerItem.setCodiceVarianteSocio(partnerItemVariant);
            partnerItem.setCodiceArticolo(itemCode);
            partnerItem.setCodiceVariante(variantCode);
            this.logger.info("2. partnerItem: " + partnerItem);
        } else {
            return new Response(Code.GENERIC_ERROR, "already-exists");
        }
        try {
            ArticoloSocioLocalServiceUtil.updateArticoloSocio(partnerItem);
            this.logger.info("3. partnerItem: " + partnerItem);
            return new Response(Code.OK, "success-edit-x",
                    JSONFactoryUtil.looseSerialize(partnerItem));
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            return new Response(Code.GENERIC_ERROR, "ERRORE", e.getMessage());
        }

    }
    
    /**
     * Persists data into database.
     * 
     * @param resourceRequest
     *            the {@link ResourceRequest}
     * @return @link {@link Response}
     */
    private Response saveAssociation(final ResourceRequest resourceRequest) {
        try {
            JSONArray itemsJson = JSONFactoryUtil.createJSONArray(ParamUtil
                    .getString(resourceRequest, AssociateItems.ITEMS));
            this.logger.debug(itemsJson);
            for (int i = 0; i < itemsJson.length(); i++) {
                JSONObject obj = itemsJson.getJSONObject(i);
                if (!obj.getString("codiceArticolo").isEmpty()) {
                    ArticoloSocio tmpItem = ArticoloSocioLocalServiceUtil.
                            createArticoloSocio(new ArticoloSocioPK());
                    tmpItem.setAssociato(obj.getString("associato"));
                    tmpItem.setCodiceArticoloSocio(obj.getString(
                            "codiceArticoloSocio"));
                    tmpItem.setCodiceVarianteSocio(obj.getString(
                            "codiceVarianteSocio"));
                    tmpItem.setCodiceArticolo(obj.getString("codiceArticolo"));
                    tmpItem.setCodiceVariante(obj.getString("codiceVariante"));
                    ArticoloSocio item = ArticoloSocioLocalServiceUtil.
                            fetchArticoloSocio(tmpItem.getPrimaryKey());
                    if (item != null) {
                        ArticoloSocioLocalServiceUtil.deleteArticoloSocio(item);
                    }
                    ArticoloSocioLocalServiceUtil.updateArticoloSocio(tmpItem);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return new Response(Code.GENERIC_ERROR, "ERROR");

        } catch (SystemException e) {
            e.printStackTrace();
            return new Response(Code.GENERIC_ERROR, "ERROR");
        }
        return new Response(Code.OK, "OK");
    }

    /**
     * Maps csv data to {@link ArticoloSocio}.
     * 
     * @param partnerCode
     *            partner is uploading data
     * @param data
     *            data uploaded
     * @return a list of {@link ArticoloSocio}
     */
    private List<ArticoloSocio> mapToItems(final String partnerCode,
            final List<String[]> data) {
        List<ArticoloSocio> result = new ArrayList<ArticoloSocio>();
        for (String[] fields : data) {
            ArticoloSocio item = ArticoloSocioLocalServiceUtil.
                    createArticoloSocio(new ArticoloSocioPK(partnerCode,
                            fields[AssociateItems.PARTNER_ITEM_CODE],
                            fields[AssociateItems.PARTNER_VARIANT_CODE]));
            item.setCodiceArticolo(fields[AssociateItems.ITEM_CODE]);
            item.setCodiceVariante(fields[AssociateItems.VARIANT_CODE]);
            result.add(item);
        }
        return result;
    }

    /**
     * Uploads file to server temporary folder.
     * 
     * @param file
     *            user uploaded {@link File}
     * @param tmpFolder
     *            system temporary folder
     * @return return the temporary uploaded file
     * @throws IOException
     *             exception
     */
    private File uploadFile(final File file, final File tmpFolder)
            throws IOException {

        File tempFile = FileUtil.createTempFile(FileUtil.getExtension(
                file.getName()));
        FileUtil.copyFile(file, tempFile);
        
        if (FileUtil.getExtension(tempFile.getName()).equalsIgnoreCase("xls")) {
            FileInputStream in = new FileInputStream(tempFile);
            @SuppressWarnings("resource")
            HSSFWorkbook workbook = new HSSFWorkbook(in);
            tempFile = Utils.echoAsCSV(workbook.getSheetAt(0));
        }
        return tempFile;
    }
    
    /**
     * Returns the item with the code inputed.
     * 
     * @param resourceRequest
     *            {@link ResourceRequest}
     * @return {@link JSONArray};
     */
    private JSONArray getItems(final ResourceRequest resourceRequest) {

        String itemCode = ParamUtil.getString(resourceRequest, "itemCode");
        String name = ParamUtil.getString(resourceRequest, "itemName");
//        long idCategory = ParamUtil.getLong(resourceRequest, "category");
        String idSupplier = ParamUtil.getString(resourceRequest, "supplier");
        boolean active = ParamUtil.getBoolean(resourceRequest,
                "onlyActive", false);
        boolean inactive = ParamUtil.getBoolean(resourceRequest,
                "onlyInactive", false);
        boolean available = ParamUtil.getBoolean(resourceRequest,
                "onlyAvailable");

        JSONObject json = JSONFactoryUtil.createJSONObject();
        JSONArray results = JSONFactoryUtil.createJSONArray();
        json.put("response", results);

        try {
//            List<Piante> items = PianteLocalServiceUtil.findPlants(idCategory,
//                    active, idSupplier, name, itemCode);
        	Piante p = PianteLocalServiceUtil.findPlants(itemCode);
//            List<Piante> items = PianteLocalServiceUtil.findPlants(active,
//            		inactive, idSupplier, name, itemCode, false);
        	if (p != null) {
//            for (Piante p : items) {
                JSONObject object = JSONFactoryUtil
                        .createJSONObject(JSONFactoryUtil.looseSerialize(p));
                List<DescrizioniVarianti> variants =
                        DescrizioniVariantiLocalServiceUtil.findVariants(
                                p.getCodice());
                JSONArray variantsJson = JSONFactoryUtil.createJSONArray();
                object.put("varianti", variantsJson);
                for (DescrizioniVarianti v : variants) {
                    JSONObject vJson = JSONFactoryUtil.createJSONObject(
                            JSONFactoryUtil.looseSerialize(v));
                    variantsJson.put(vJson);
                }

                Articoli item = ArticoliLocalServiceUtil.fetchArticoli(p.
                        getCodice());
                JSONObject itemDetails = JSONFactoryUtil.createJSONObject();
                if (item != null) {
                    itemDetails = JSONFactoryUtil.createJSONObject(
                            JSONFactoryUtil.looseSerialize(item));
                }
                object.put("dettagliPianta", itemDetails);
                if (available) {
                    if (p.getDisponibilita() > 0) {
                        results.put(object);
                    }
                } else {
                    results.put(object);
                }
            }
        } catch (JSONException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        this.logger.debug("RESULTS: " + results.length());

        return results;
    }
    
    /**
     * Returns the {@link ArticoloSocio} if exists null otherwise.
     * 
     * @param resourceRequest
     *            {@link ResourceRequest}
     * @return {@link Response} containing the {@link ArticoloSocio}
     */
    private Response findAssociation(final ResourceRequest resourceRequest) {
        String partnerCode = ParamUtil.getString(resourceRequest, "parterCode");
        String itemCode = ParamUtil.getString(resourceRequest, "itemCode");
        String variantCode = ParamUtil.getString(resourceRequest,
                "variantCode");
        this.logger.debug("partnerCode: " + partnerCode);
        this.logger.debug("itemCode: " + itemCode);
        this.logger.debug(" variantCode: " + variantCode);
        ArticoloSocio partnerItem = ArticoloSocioLocalServiceUtil.
                findPartnerItems(partnerCode, itemCode, variantCode);
        this.logger.debug(" partnerItem: " + partnerItem);
        return new Response(Code.OK, JSONFactoryUtil.looseSerialize(
                partnerItem));
    }
}
