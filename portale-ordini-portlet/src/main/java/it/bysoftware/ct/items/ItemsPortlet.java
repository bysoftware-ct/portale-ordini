package it.bysoftware.ct.items;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.servlet.ServletResponseUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.liferay.util.portlet.PortletProps;

import it.bysoftware.ct.model.Articoli;
import it.bysoftware.ct.model.CategorieMerceologiche;
import it.bysoftware.ct.model.DescrizioniVarianti;
import it.bysoftware.ct.model.Piante;
import it.bysoftware.ct.service.ArticoliLocalServiceUtil;
import it.bysoftware.ct.service.CategorieMerceologicheLocalServiceUtil;
import it.bysoftware.ct.service.DescrizioniVariantiLocalServiceUtil;
import it.bysoftware.ct.service.PianteLocalServiceUtil;
import it.bysoftware.ct.service.VariantiLocalServiceUtil;
import it.bysoftware.ct.service.persistence.DescrizioniVariantiPK;
import it.bysoftware.ct.service.persistence.VariantiPK;
import it.bysoftware.ct.utils.Constants;
import it.bysoftware.ct.utils.Report;
import it.bysoftware.ct.utils.ResourceId;
import it.bysoftware.ct.utils.Response;
import it.bysoftware.ct.utils.Response.Code;
import it.bysoftware.ct.utils.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;

import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFHyperlink;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor.HSSFColorPredefined;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;

/**
 * Portlet implementation class ItemsPortlet.
 */
public class ItemsPortlet extends MVCPortlet {

    /**
     * Exported order file name.
     */
    private static final String VIRTUAL_HOST = PortletProps.get(
            "virtual-host");
    
    /**
     * Columns name.
     */
    private enum Columns {
        
        /**
         * Code column.
         */
        CODICE ("codice"),
        
        /**
         * EAN column.
         */
        EAN ("ean"),
        
        /**
         * Item name column.
         */
        NOME ("nome"),
        
        /**
         * Item shape column.
         */
        FORMA ("forma"),
        
        /**
         * Pot column.
         */
        VASO ("vaso"),
        
        /**
         * Plant height column.
         */
        ALTEZZA ("altezza"),
        
        /**
         * Item per shelf column.
         */
        PIANTE_PIANALE ("piantePianale"),
        
        /**
         * Item per cart column.
         */
        PIANTE_CARRELLO ("pianteCarrello"),
        
        /**
         * Item price column.
         */
        PREZZO ("prezzo"),
        
        /**
         * Item price column.
         */
        PREZZO_PIANALE ("prezzo3"),
        
        /**
         * Item availability column.
         */
        DISPONIBILITA ("disponibilita"),
        
        /**
         * Note column.
         */
        NOTE ("tempoConsegna"),
        
        /**
         * Picture column.
         */
        FOTO ("foto1"),

        /**
         * Picture column.
         */
        LINK ("foto3");
        
        /**
         * Column name.
         */
        private final String name;
        
        /**
         * Sets the column name.
         * @param s column name
         */
        private Columns(final String s) {
            this.name = s;
        }
        
        /**
         * Returns column name.
         * @return column name
         */
        public String toString() {
            return this.name;
        }
    }

    /**
     * Item picture id constant.
     */
    private static final String ITEM_PICTURE_ID = "itemPictureId";

    /**
     * File upload constant.
     */
    private static final String FILEUPLOAD = "fileupload";

    /**
     * Item id constant.
     */
    private static final String ITEM_ID = "itemId";

    /**
     * Exception args constant.
     */
    private static final String EXCEPTION_ARGS = "exceptionArgs";

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(ItemsPortlet.class);

    /**
     * Data source connection.
     */
    private DataSource ds;

    @Override
    public final void init(final PortletConfig config) throws PortletException {
        Context ctx;
        try {
            ctx = new InitialContext();
            this.ds = (DataSource) ctx.lookup("jdbc/OrdiniPool");
        } catch (NamingException e) {
            this.logger.fatal(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        super.init(config);
    }
    
    @Override
    public final void serveResource(final ResourceRequest resourceRequest,
            final ResourceResponse resourceResponse) throws IOException,
            PortletException {
        String resourceID = resourceRequest.getResourceID();
        
        Response response = null;
        switch (ResourceId.valueOf(resourceID)) {
            case printListItem:
                ThemeDisplay themeDisplay =
                (ThemeDisplay) resourceRequest.getAttribute(
                        WebKeys.THEME_DISPLAY);
                User u = themeDisplay.getUser();
                this.logger.debug("Printing item list: " + u.getUserId() + " "
                        + u.getScreenName());
                try {
                    if (RoleLocalServiceUtil.hasUserRole(u.getUserId(),
                            RoleLocalServiceUtil.getRole(u.getCompanyId(),
                                    Constants.COMPANY).getRoleId())) {
                        String code = ParamUtil.getString(resourceRequest,
                                "code", "");
                        this.logger.debug("code: " + code);
                        String name = ParamUtil.getString(resourceRequest,
                                "name", "");
                        this.logger.debug("name: " + name);
                        String vase = ParamUtil.getString(resourceRequest,
                                "vase", "");
                        String shape = ParamUtil.getString(resourceRequest,
                                "shapes", "");
                        this.logger.debug("shape: " + shape);
                        int category = ParamUtil.getInteger(resourceRequest,
                                "category", -1);
                        this.logger.debug("category; " + category);
                        String supplier = ParamUtil.getString(resourceRequest,
                                "supplier", "");
                        this.logger.debug("supplier; " + supplier);
                        int height = ParamUtil.getInteger(resourceRequest,
                                "height", -1);
                        this.logger.debug("height; " + height);
                        boolean active = ParamUtil.getBoolean(resourceRequest,
                                "active", true);
                        boolean inactive = ParamUtil.getBoolean(resourceRequest,
                                "inactive", false);
                        this.logger.debug("active; " + active);
                        boolean available = ParamUtil.getBoolean(
                                resourceRequest, "available", false);
                        this.logger.debug("available; " + available);
                        boolean obsolete = ParamUtil.getBoolean(
                                resourceRequest, "obsolete", false);
                        this.logger.debug("obsolete; " + obsolete);
                        String type = ParamUtil.getString(
                                resourceRequest, "type", "");
                        this.logger.debug("type: " + type);
                        if (type.equals("xls")) {
							List<Object[]> items = PianteLocalServiceUtil
									.getItemsByFilters(category, code, name,
											vase, shape, height, supplier,
											active, inactive, available,
											obsolete);
							response = this.printListItem(items);
						} else {
							User user = UserLocalServiceUtil.getUser(Long.
			                        parseLong(resourceRequest.getRemoteUser()));
							response = this.printItems(user.getUserId(),
									category, code, name, vase, shape, height,
									supplier, active, inactive, available,
									obsolete, type);
						}
                        
                        if (response.getCode() == 0) {
                            File file = new File(response.getMessage());
                            InputStream in = new FileInputStream(file);
                            HttpServletResponse httpRes = PortalUtil.
                                    getHttpServletResponse(resourceResponse);
                            HttpServletRequest httpReq = PortalUtil.
                                    getHttpServletRequest(resourceRequest);
                            httpRes.setHeader("Content-Disposition",
                            		"attachment; filename=\"" + file.getName()
                            		+ "\"");
                            if (type.equals("xls")) {
                            	ServletResponseUtil.sendFile(httpReq, httpRes,
                                    file.getName(), in, "application/download");
                            } else {
                            	ServletResponseUtil.sendFile(httpReq, httpRes,
                                    file.getName(), in, "application/octet-stream");
                            }
                            in.close();
                        } else {
                            PrintWriter out;
                            out = resourceResponse.getWriter();
                            out.print(JSONFactoryUtil.looseSerialize(response));
                            out.flush();
                            out.close();
                        }
                    }
                } catch (SystemException e) {
                    this.logger.error(e.getMessage());
                    if (this.logger.isDebugEnabled()) {
                        e.printStackTrace();
                    }
                    PrintWriter out;
                    out = resourceResponse.getWriter();
                    out.print(JSONFactoryUtil.looseSerialize(new Response(
                            Code.GENERIC_ERROR, e.getMessage())));
                    out.flush();
                    out.close();
                } catch (PortalException e) {
                    this.logger.error(e.getMessage());
                    if (this.logger.isDebugEnabled()) {
                        e.printStackTrace();
                    }
                    PrintWriter out;
                    out = resourceResponse.getWriter();
                    out.print(JSONFactoryUtil.looseSerialize(new Response(
                            Code.GENERIC_ERROR, e.getMessage())));
                    out.flush();
                    out.close();
                }
                break;
            case deletePicture:
                long fileEntryId = ParamUtil.getLong(resourceRequest,
                        "fileEntryId", -1);
                this.logger.info("Deleting picture: " + fileEntryId);
                PrintWriter out;
                out = resourceResponse.getWriter();
                Response res = new Response();
                if (fileEntryId > 0) {
                    try {
                        DLAppServiceUtil.deleteFileEntry(fileEntryId);
                        response = new Response(Code.OK, "success");
                    } catch (PortalException e) {
                        res = new Response(Code.GENERIC_ERROR, e.getMessage());
                    } catch (SystemException e) {
                        res = new Response(Code.GENERIC_ERROR, e.getMessage());
                    }
                }
                out.print(JSONFactoryUtil.looseSerialize(res));
                out.flush();
                out.close();
                break;
            default:
                this.logger.warn("Unknown operation: " + resourceID);
                break;
        }
    }

	private Response printItems(long userId, int category, String code,
			String name, String vase, String shape, int height,
			String supplier, boolean active, boolean inactive,
			boolean available, boolean obsolete, String type) {

		String message = null;
		Code c = null;
		Map<String, Object> parametersMap = new HashMap<String, Object>();
		if (!Validator.isBlank(shape)) {
			parametersMap.put("shape", shape);
		}
		
		if (!Validator.isBlank(vase)) {
			parametersMap.put("vase", vase);
		}
		
		if (height != -1) {
			parametersMap.put("height", height);
		}
		
		if (!Validator.isBlank(supplier)) {
			parametersMap.put("supplierId", supplier);
		}
		
		if (!Validator.isBlank(code)) {
			parametersMap.put("code", CharPool.PERCENT + code
					+ CharPool.PERCENT);
		}
		
		if (!Validator.isBlank(name)) {
			parametersMap.put("name", CharPool.PERCENT + name
					+ CharPool.PERCENT);
		}
			
		parametersMap.put("active", active);
		parametersMap.put("inactive", inactive);
		parametersMap.put("obsolete", obsolete);
		parametersMap.put("available", available);
		
		if (type.equals("pdf-l")) {
			parametersMap.put("link", true);
		}
		
		parametersMap.put(JRParameter.REPORT_LOCALE, new Locale("it", "IT"));
		Report report;
		try {
			report = new Report(this.ds.getConnection());
			String filePath = report.print(userId, parametersMap,
					"items_price_list");
			report.closeConnection();
			message = filePath;
			c = Response.Code.OK;
		} catch (SQLException e) {
			this.logger.error(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
			c = Response.Code.GENERIC_ERROR;
			message = e.getMessage();
		} catch (JRException e) {
			this.logger.error(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
			c = Response.Code.GENERIC_ERROR;
			message = e.getMessage();
		}

		return new Response(c, message);
	}

	/**
	 * Activates or Deactivates a user based on his current state.
	 * 
	 * @param areq
	 *            {@link ActionRequest}
	 * @param ares
	 *            {@link ActionResponse}
	 */
	public final void editItem(final ActionRequest areq,
			final ActionResponse ares) {
		this.logger.info("Editing Item...");
		UploadPortletRequest uploadRequest = PortalUtil
				.getUploadPortletRequest(areq);

		if (!Validator.isBlank(ParamUtil.getString(uploadRequest, "code"))) {
			File tmpFolder = new File(System.getProperty("java.io.tmpdir"));

			long itemId = Long.parseLong(uploadRequest
					.getParameter(ItemsPortlet.ITEM_ID));

			boolean continua = Boolean.parseBoolean(uploadRequest
					.getParameter("continua"));
			Piante p;
			Articoli item = null;
			try {
				if (itemId > 0) {
					p = PianteLocalServiceUtil.fetchPiante(itemId);
					item = ArticoliLocalServiceUtil.getArticoli(p.getCodice());
				} else {
					p = PianteLocalServiceUtil.findPlants(ParamUtil.getString(
							uploadRequest, "code"));
					item = ArticoliLocalServiceUtil.createArticoli(ParamUtil
							.getString(uploadRequest, "code"));
					item.setTipoArticolo("ARTGEN");
					if (p == null) {
						p = PianteLocalServiceUtil.createPiante(0);
						p.setCodice(ParamUtil.getString(uploadRequest, "code"));
						// p.setIdCategoria(17);
						Calendar c = Calendar.getInstance();
						c.setTime(new Date());
						p.setUltimoAggiornamentoFoto(c.getTime());
						c.add(Calendar.DATE, ParamUtil.getInteger(
								uploadRequest, "expirationDays"));
						p.setScadenzaFoto(c.getTime());
					} else {
						throw new PortalException("Duplicate item code.");
					}
				}
				this.savePicture(areq, ares, uploadRequest, tmpFolder, itemId,
						p);
				p.setIdCategoria(ParamUtil.getLong(uploadRequest,
						"itemCategories", 17));
				p.setForma(ParamUtil.getString(uploadRequest, "itemShapes", ""));
				p.setNome(ParamUtil.getString(uploadRequest, "name", ""));
				p.setGiorniScadenzaFoto(ParamUtil.getInteger(uploadRequest,
						"expirationDays", Constants.HUNDRED));
				p.setVaso(ParamUtil.getString(uploadRequest, "vase", ""));
				p.setPianteCarrello(ParamUtil.getInteger(uploadRequest,
						"plant-per-cart", 0));
				p.setPiantePianale(ParamUtil.getInteger(uploadRequest,
						"plant-per-platform", 0));
				p.setAggiuntaRipiano(ParamUtil.getInteger(uploadRequest,
						"add-platform", 0));
				p.setAltezzaChioma(ParamUtil.getInteger(uploadRequest,
						"foliage-height", 0));
				p.setAltezzaPianta(ParamUtil.getInteger(uploadRequest,
						"plant-height", 0));
				p.setAltezza(ParamUtil.getInteger(uploadRequest,
						"platform-height", 0));
				p.setPassaporto(ParamUtil.getBoolean(uploadRequest, "passport",
						false));
				p.setAttiva(ParamUtil.getBoolean(uploadRequest, "active", true));
				p.setSormonto2Fila(ParamUtil.getInteger(uploadRequest,
						"item-2-over", 0));
				p.setSormonto3Fila(ParamUtil.getInteger(uploadRequest,
						"item-3-over", 0));
				p.setSormonto4Fila(ParamUtil.getInteger(uploadRequest,
						"item-4-over", 0));
				p.setEan(ParamUtil.getString(uploadRequest, "ean"));
				p.setIdFornitore(ParamUtil.getString(uploadRequest, "supplier"));
				p.setPrezzoFornitore(ParamUtil.getDouble(uploadRequest,
						"suppPrice", 0));
				p.setDisponibilita(ParamUtil.getInteger(uploadRequest,
						"availability", 0));
				p.setTempoConsegna(ParamUtil.getString(uploadRequest,
						"item-note", ""));
				p.setPassaportoDefault(ParamUtil.getString(uploadRequest,
						"default-passport"));
				boolean obsolete = ParamUtil.getBoolean(uploadRequest,
						"obsolete", false);
				p.setObsoleto(obsolete);
				if (obsolete) {
					p.setAttiva(false);
				}
				p.setFoto3(ParamUtil.getString(uploadRequest, "link-drive", ""));
				if (!"".equals(p.getFoto1()) && !"".equals(p.getPathFile()) &&
						"".equals(p.getFoto3())) {
					p.setFoto3(this.setPublicURL(areq, p));
				}
				PianteLocalServiceUtil.updatePiante(p);
				item.setDescrizione(ParamUtil.getString(uploadRequest, "name",
						""));
				String descrDoc = p.getNome();
				if (p.getVaso() != null && !p.getVaso().isEmpty()) {
					descrDoc += " v." + p.getVaso();
				}
				if (p.getAltezzaPianta() >= 0) {
					descrDoc += " H." + p.getAltezzaPianta();
				}
				item.setCategoriaMerceologica(ParamUtil.getString(
						uploadRequest, "itemShapes", ""));
				if (item.getCategoriaMerceologica() != null
						&& !item.getCategoriaMerceologica().isEmpty()) {
					CategorieMerceologiche cat = CategorieMerceologicheLocalServiceUtil
							.fetchCategorieMerceologiche(item
									.getCategoriaMerceologica());
					if (cat != null) {
						descrDoc += " " + cat.getDescrizione();
					}
				}
				item.setDescrizioneDocumento(descrDoc);
				item.setObsoleto(p.getObsoleto());
				boolean passport = ParamUtil.getBoolean(uploadRequest,
						"passport");
				if (passport) {
					item.setLibLng1(1);
				} else {
					item.setLibLng1(0);
				}
				item.setPrezzo1(ParamUtil.getDouble(uploadRequest, "price1"));
				item.setPrezzo2(ParamUtil.getDouble(uploadRequest, "price2"));
				item.setPrezzo3(ParamUtil.getDouble(uploadRequest, "price3"));
				if (item.getCodiceIVA() == null
						|| item.getCodiceIVA().isEmpty()) {
					item.setCodiceIVA("10");
				}
				if (item.getCodiceProvvigione() == null
						|| item.getCodiceProvvigione().isEmpty()) {
					item.setCodiceProvvigione("001");
				}
				if (item.getUnitaMisura() == null
						|| item.getUnitaMisura().isEmpty()) {
					item.setUnitaMisura("Num");
				}
				item.setPesoLordo(ParamUtil.getDouble(uploadRequest,
						"gross-weight", 0.0));
				ArticoliLocalServiceUtil.updateArticoli(item);
				String tmpVariantValues = ParamUtil.getString(uploadRequest,
						"variantsIds", "");
				String[] variantValues = new String[0];
				JSONArray jsnArr = null;
				if (!tmpVariantValues.isEmpty()) {
					jsnArr = JSONFactoryUtil.createJSONArray(tmpVariantValues);
					variantValues = new String[jsnArr.length()];
				}
				for (int i = 0; jsnArr != null && i < jsnArr.length(); i++) {
					this.logger.debug("####: " + jsnArr.getString(i));
					variantValues[i] = jsnArr.getString(i);
				}

				this.updateVariants(item, variantValues);
				ares.setRenderParameter("backUrl",
						ParamUtil.getString(uploadRequest, "backUrl"));
				this.logger.debug("itemId: " + p.getId());
				this.logger.debug("continua: " + continua);
				ares.setRenderParameter("itemId", String.valueOf(p.getId()));
				ares.setRenderParameter("variantValues", tmpVariantValues);
				ares.setRenderParameter("continua", String.valueOf(continua));
			} catch (SystemException e) {
				this.logger.error(e.getMessage());
				if (this.logger.isDebugEnabled()) {
					e.printStackTrace();
				}
				SessionErrors.add(areq, e.getMessage());

			} catch (PortalException e) {
				this.logger.error(e.getMessage());
				if (this.logger.isDebugEnabled()) {
					e.printStackTrace();
				}
				if (e.getMessage().toLowerCase().contains("duplicate")) {
					ares.setRenderParameter(EXCEPTION_ARGS,
							ParamUtil.getString(uploadRequest, "code"));
					SessionErrors.add(areq, "error-inserting-item-x");
				} else {
					SessionErrors.add(areq, e.getMessage());
				}
			}
		} else {
			SessionErrors.add(areq, "error-inserting-empty-code-item");
		}
		// PortalUtil.copyRequestParameters(areq, ares);
		ares.setRenderParameter("jspPage", "/html/items/edit-picture.jsp");
	}

	private String setPublicURL(ActionRequest areq, Piante p) {
		HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(areq);
		String portalURL = PortalUtil.getPortalURL(httpRequest);
		String res = "";
		if (!p.getPathFile().isEmpty() && !p.getFoto1().isEmpty()) {
//			logger.info(portalURL + Utils.retrievePictureURL(p));
			try {
				return portalURL + File.separator + Utils.retrievePictureURL(p);
			} catch (PortalException e) {
				this.logger.error(e.getMessage());
				if (this.logger.isDebugEnabled()) {
					e.printStackTrace();
				}
			} catch (SystemException e) {
				this.logger.error(e.getMessage());
				if (this.logger.isDebugEnabled()) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}

	private void savePicture(final ActionRequest areq,
			final ActionResponse ares, UploadPortletRequest uploadRequest,
			File tmpFolder, long itemId, Piante p) throws SystemException {
		String sourceFileName = uploadRequest.
		        getFileName(ItemsPortlet.FILEUPLOAD);
		String origFileEntryId = uploadRequest.getParameter(
		        "fileEntryId");
		if (!uploadRequest.getFileName(ItemsPortlet.FILEUPLOAD).
		        isEmpty()) {
		    if (tmpFolder.getUsableSpace() < Constants.ONE_GB) {
		        this.logger.warn("No enough space in: "
		                + tmpFolder.getAbsolutePath());
		        SessionErrors.add(areq, "disk-space");
		        ares.setRenderParameter(ItemsPortlet.ITEM_ID,
		                String.valueOf(itemId));
		        ares.setRenderParameter(ItemsPortlet.ITEM_PICTURE_ID,
		                origFileEntryId);
		    } else if (uploadRequest.getSize(
		            ItemsPortlet.FILEUPLOAD) == 0) {
		        this.logger.warn("Uploaded an empty file.");
		        SessionErrors.add(areq, "empty-file");
		        ares.setRenderParameter(ItemsPortlet.ITEM_ID,
		                String.valueOf(itemId));
		        ares.setRenderParameter(ItemsPortlet.ITEM_PICTURE_ID,
		                origFileEntryId);
		    } else {
		        File file = uploadRequest.getFile(FILEUPLOAD);
		        try {
		            File uploadedFile = this.uploadFile(file,
		                    tmpFolder);
		            
		            ThemeDisplay themeDisplay =
		                    (ThemeDisplay) uploadRequest.getAttribute(
		                            WebKeys.THEME_DISPLAY);
         
		            User company = themeDisplay.getUser();
		            long groupId = themeDisplay.getLayout().
		                    getGroupId();
         
		            DLFolder companyFolder = Utils.getAziendaFolder(
		                    groupId, company);
		            this.logger.debug("Company FOLDER: " 
		                    + companyFolder);
		            DLFolder itemSheetFolder = Utils.getPicturesFolder(
		                    groupId, companyFolder);
		            this.logger.debug("Item pictures FOLDER: "
		                    + itemSheetFolder);
		            FileEntry fileEntry = null;
		            try {
		                if (origFileEntryId != null
		                        && !origFileEntryId.isEmpty()
		                        && !"-1".equals(origFileEntryId)) {
		                    fileEntry = DLAppServiceUtil.getFileEntry(
		                            Long.parseLong(origFileEntryId));
		                    this.logger.debug("Entry found,"
		                            + " the file will be replaced.");
		                }
		            } catch (PortalException e) {
		                this.logger.debug("File Entry not found, a new "
		                        + "file will be created."
		                        + sourceFileName);
		            }
         
		            if (fileEntry != null) {
		                DLAppServiceUtil.deleteFileEntry(fileEntry.
		                        getFileEntryId());
		            }
		            long repositoryId = themeDisplay.getScopeGroupId();
		            fileEntry = DLAppServiceUtil.addFileEntry(
		                    repositoryId, itemSheetFolder.getFolderId(),
		                    sourceFileName,
		                    MimeTypesUtil.getContentType(uploadedFile),
		                    UUID.randomUUID().toString(), "", "",
		                    uploadedFile, new ServiceContext());
         
		            Utils.setFilePermissions(fileEntry);
		            this.logger.info("Added: " + fileEntry.getTitle());
		            p.setFoto1(String.valueOf(fileEntry.
		                    getFileEntryId()));
		            p.setPathFile(PropsUtil.get(
		                    "dl.store.file.system.root.dir")
		                    + File.separator + fileEntry.getCompanyId()
		                    + File.separator + fileEntry.getFolderId()
		                    + File.separator + ((DLFileEntry) fileEntry.
		                            getModel()).getName()
		                    + File.separator + fileEntry.getVersion());
		            Calendar c = Calendar.getInstance();
		            c.setTime(new Date());
		            p.setUltimoAggiornamentoFoto(c.getTime());
		            c.add(Calendar.DATE, p.getGiorniScadenzaFoto());
		            p.setScadenzaFoto(c.getTime());
		            
		            ares.setRenderParameter(ITEM_PICTURE_ID,
		                    p.getFoto1());
		        } catch (PortalException e) {
		            this.logger.error(e.getMessage());
		            if (this.logger.isDebugEnabled()) {
		                e.printStackTrace();
		            }
		            SessionErrors.add(areq, e.getMessage());
		        } catch (IOException e) {
		            this.logger.error(e.getMessage());
		            if (this.logger.isDebugEnabled()) {
		                e.printStackTrace();
		            }
		            SessionErrors.add(areq, e.getMessage());
		        }
		    }
		}
	}

    private void updateVariants(Articoli item, String[] variantValues)
    		throws SystemException, PortalException {

    	List<DescrizioniVarianti> currVariants =
    			DescrizioniVariantiLocalServiceUtil.findVariants(
    					item.getCodiceArticolo());
    	for (int i = 0; i < variantValues.length; i++) {
    		logger.debug("NEW VARIANT: " + variantValues[i]);
    		boolean add = true; 
    		for (DescrizioniVarianti currVar : currVariants) {
    			logger.debug("CI ENTRO ");
    			if (currVar.getCodiceVariante().equals(variantValues[i])) {
    				add = false;
    				break;
    			}
    		}
    		logger.debug("ESCO !!! add: " + add);
    		if (add) {
    			DescrizioniVarianti newVar =
    					DescrizioniVariantiLocalServiceUtil.
    					createDescrizioniVarianti(new DescrizioniVariantiPK(
    							item.getCodiceArticolo(), variantValues[i]));
    			newVar.setDescrizione(VariantiLocalServiceUtil.getVarianti(
    					new VariantiPK(variantValues[i], "VASOCOLORE")).getDescrizione());
    			logger.debug("ADDING NEW VARIANT: " + newVar);
				DescrizioniVariantiLocalServiceUtil.updateDescrizioniVarianti(
						newVar);
			}
    	}
    	/*
    	 * N.B. NON POSSO ELIMINAARE LE VARIANTI, PERCHE' CREEREBBE
    	 * INCONSISTENZA NEL DATABASE.
    	 */
//    	for (DescrizioniVarianti currVar : currVariants) {
//    		boolean delete = true;
//			for (int i = 0; i < variantValues.length; i++) {
//				if (currVar.getCodiceVariante().equals(variantValues[i])) {
//					delete = false;
//					break;
//				}
//			}
//			if (delete) {
//				logger.debug("DELETING VARIANT: " + currVar);
//				DescrizioniVariantiLocalServiceUtil.deleteDescrizioniVarianti(currVar);
//			}
//		}
	}

	/**
     * Uploads file to server temporary folder.
     * 
     * @param file
     *            user uploaded {@link File}
     * @param tmpFolder
     *            system temporary folder
     * @return return the temporary uploaded file
     * @throws IOException
     *             exception
     */
    private File uploadFile(final File file, final File tmpFolder)
            throws IOException {

        File tempFile = new File(UUID.randomUUID().toString());
        File filePath = new File(tmpFolder.getAbsolutePath() + File.separator
                + tempFile.getName());

        FileUtil.copyFile(file, filePath);
        File c = Utils.compressImage(filePath, new File(tmpFolder.getAbsolutePath()
        		+ File.separator + UUID.randomUUID().toString()));
        return c;
    }
    
    /**
     * Creates an Excel file containing all the specified items.
     * 
     * @param items
     *            the item to print in the file.
     * @return the file
     */
    private Response printListItem(final List<Object[]> items) {
        HSSFWorkbook workbook = new HSSFWorkbook();
        File f = FileUtil.createTempFile("xls");
        HSSFSheet sheet = workbook.createSheet(f.getName());

        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
        int index = 1;
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyy");
        data.put(++index, new String[] { "Listino Prezzi al "
                + sdf.format(Utils.today().getTime()) + " per"
                        + " Milazzoflora." });
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, Constants.TWELVE));
        List<Object> column = new ArrayList<Object>();
        for (Columns c : Columns.values()) {
            column.add(c.name().replace('_', ' '));
        }
        data.put(++index, column.toArray(new Object[column.size()]));
        for (int i = 0; i < items.size(); i++) {
            data.put(++index, items.get(i));
        }

        Set<Integer> keyset = data.keySet();
        int rownum = 0;
        for (Integer key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (cellnum == Constants.THIRTEEN && (rownum - 2) > 0) {
                    long imgId = -1;
                    FileEntry image = null;
                        try {
                            imgId = Long.parseLong(String.valueOf(obj));
                            image = DLAppLocalServiceUtil.getFileEntry(imgId);
                            String imageSrc = "https://"
                                    + ItemsPortlet.VIRTUAL_HOST + File.separator
                                    + Constants.DOCUMENTS_FOLDER_NAME
                                    + File.separator + image.getRepositoryId()
                                    + File.separator + image.getFolderId()
                                    + File.separator + image.getTitle(); 
                            HSSFCellStyle hlinkstyle = workbook.
                                    createCellStyle();
                            HSSFFont hlinkfont = workbook.createFont();
                            hlinkfont.setUnderline(XSSFFont.U_SINGLE);
                            hlinkfont.setColor(HSSFColorPredefined.BLUE.
                                    getIndex());
                            hlinkstyle.setFont(hlinkfont);
                            CreationHelper createHelper = workbook.
                                    getCreationHelper();
                            HSSFHyperlink link = (HSSFHyperlink) createHelper.
                                    createHyperlink(HyperlinkType.URL);
                            link.setAddress(imageSrc);
                            cell.setHyperlink((HSSFHyperlink) link);
                            cell.setCellStyle(hlinkstyle);
                            cell.setCellValue("Link");
                        } catch (NumberFormatException e) {
                            cell.setCellValue("");
                        } catch (PortalException e) {
                        	cell.setCellValue("");
                        } catch (SystemException e) {
                        	cell.setCellValue("");
                        }
                } else if (cellnum == Constants.FOURTEEN && (rownum - 2) > 0) {
                	if (Validator.isUrl(String.valueOf(obj))) {
                		HSSFCellStyle hlinkstyle = workbook.
                                createCellStyle();
                        HSSFFont hlinkfont = workbook.createFont();
                        hlinkfont.setUnderline(XSSFFont.U_SINGLE);
                        hlinkfont.setColor(HSSFColorPredefined.BLUE.
                                getIndex());
                        hlinkstyle.setFont(hlinkfont);
                        CreationHelper createHelper = workbook.
                                getCreationHelper();
                        HSSFHyperlink link = (HSSFHyperlink) createHelper.
                                createHyperlink(HyperlinkType.URL);
                        link.setAddress(String.valueOf(obj));
                        cell.setHyperlink((HSSFHyperlink) link);
                        cell.setCellStyle(hlinkstyle);
                        cell.setCellValue("Link");
					} else {
						cell.setCellValue("");
					}
                } else {
                    if (obj instanceof Date) {
                        cell.setCellValue((Date) obj);
                    } else if (obj instanceof Boolean) {
                        cell.setCellValue((Boolean) obj);
                    } else if (obj instanceof String) {
                        cell.setCellValue((String) obj);
                    } else if (obj instanceof Double) {
                        cell.setCellValue((Double) obj);
                    } else if (obj instanceof Integer) {
                        cell.setCellValue((Integer) obj);
                    }
                }
            }
        }
        HSSFCell firstCell = workbook.getSheetAt(0).getRow(0).getCell(0);
        HSSFCellStyle cellStyle = workbook.getSheetAt(0).getRow(0).getSheet().
                getWorkbook().createCellStyle();
        HSSFFont boldfont = workbook.createFont();
        boldfont.setBold(true);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setFont(boldfont);
        firstCell.setCellStyle(cellStyle);
        int noOfColumns = workbook.getSheetAt(0).getRow(1).getLastCellNum();
        for (int i = 0; i < noOfColumns; i++) {
            workbook.getSheetAt(0).autoSizeColumn(i);
        }
        Code code = null;
        String message = null;
        try {
            FileOutputStream out = new FileOutputStream(f);
            workbook.write(out);
            out.close();
            this.logger.info("Excel written successfully: "
                    + f.getPath());
            code = Code.OK;
            message = f.getPath();
        } catch (FileNotFoundException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            code = Code.GENERIC_ERROR;
            message = e.getMessage();
        } catch (IOException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            code = Code.GENERIC_ERROR;
            message = e.getMessage();
        } finally {
        	if (workbook != null)
				try {
					this.logger.info("Closing workbook.");
					workbook.close();
				} catch (IOException e) {
					this.logger.error(e.getMessage());
		            if (this.logger.isDebugEnabled()) {
		                e.printStackTrace();
		            }
		            code = Code.GENERIC_ERROR;
		            message = e.getMessage();
				}
        }
        return new Response(code, message);
    }
    
}
