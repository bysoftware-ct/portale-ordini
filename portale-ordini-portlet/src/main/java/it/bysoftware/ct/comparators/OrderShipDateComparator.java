package it.bysoftware.ct.comparators;

import it.bysoftware.ct.model.Ordine;

import com.liferay.portal.kernel.util.OrderByComparator;

public class OrderShipDateComparator extends OrderByComparator {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1348717136835003085L;

	public static String ORDER_BY_ASC = "dataConsegna ASC";
	public static String ORDER_BY_DESC = "dataConsegna DESC";

	public OrderShipDateComparator() {
		this(false);
	}

	public OrderShipDateComparator(boolean asc) {
		_asc = asc;
	}

	public int compare(Object obj1, Object obj2) {

		Ordine o1 = (Ordine) obj1;
		Ordine o2 = (Ordine) obj2;

		long o1Date = o1.getDataConsegna().getTime();
		long o2Date = o2.getDataConsegna().getTime();

		int value = 0;
		if (o1Date == o2Date) {
			if (o1.getStato() >= o2.getStato()) {
				value = 1;
			} else {
				value = -1;
			}
		}

		if (this._asc) {
			return value;
		} else {
			return -value;
		}

	}

	public String getOrderBy() {

		if (_asc) {
			return ORDER_BY_ASC;
		} else {
			return ORDER_BY_DESC;
		}
	}

	private boolean _asc;

}
