package it.bysoftware.ct.comparators;

import it.bysoftware.ct.model.Piante;

import com.liferay.portal.kernel.util.OrderByComparator;

public class PlantsByNameComparator extends OrderByComparator {

	private boolean _asc;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1276369847061145096L;

	public PlantsByNameComparator() {
		this(false);
	}

	public PlantsByNameComparator(boolean value) {
		this._asc = value;

	}

	@Override
	public int compare(Object obj1, Object obj2) {
		Piante p1 = (Piante) obj1;
		Piante p2 = (Piante) obj2;

		String p1name = p1.getNome();
		String p2name = p2.getNome();

		int value = p1name.compareToIgnoreCase(p2name);
		
		if (_asc) {
			return value;
		} else {
			return -value;
		}
	}

}
