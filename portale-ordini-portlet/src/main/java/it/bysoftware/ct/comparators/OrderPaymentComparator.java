package it.bysoftware.ct.comparators;

import it.bysoftware.ct.model.Ordine;

import com.liferay.portal.kernel.util.OrderByComparator;

public class OrderPaymentComparator extends OrderByComparator{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6838281856845870353L;
	public static String ORDER_BY_ASC = "pagamento ASC";
	public static String ORDER_BY_DESC = "pagamento DESC";

	public OrderPaymentComparator() {
		this(false);
	}

	public OrderPaymentComparator(boolean asc) {
		_asc = asc;
	}

	public int compare(Object obj1, Object obj2) {
		
		Ordine o1 = (Ordine) obj1;
		Ordine o2 = (Ordine) obj2;

		String o1Payment = o1.getPagamento();
		String o2Payment = o2.getPagamento();

		int value = o2Payment.compareToIgnoreCase(o1Payment);
		
		if (this._asc) {
			return value;
		} else {
			return -value;
		}

	}

	public String getOrderBy() {

		if (_asc) {
			return ORDER_BY_ASC;
		} else {
			return ORDER_BY_DESC;
		}
	}

	private boolean _asc;
}
