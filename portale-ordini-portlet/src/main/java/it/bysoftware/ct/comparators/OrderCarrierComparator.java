package it.bysoftware.ct.comparators;

import it.bysoftware.ct.model.Ordine;

import com.liferay.portal.kernel.util.OrderByComparator;

public class OrderCarrierComparator extends OrderByComparator {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6838281856845870353L;
	public static String ORDER_BY_ASC = "vettore ASC";
	public static String ORDER_BY_DESC = "vettore DESC";

	public OrderCarrierComparator() {
		this(false);
	}

	public OrderCarrierComparator(boolean asc) {
		_asc = asc;
	}

	public int compare(Object obj1, Object obj2) {
		
		Ordine o1 = (Ordine) obj1;
		Ordine o2 = (Ordine) obj2;

		String o1Carrier = o1.getVettore();
		String o2Carrier = o2.getVettore();

		int value = o2Carrier.compareToIgnoreCase(o1Carrier);
		
		if (this._asc) {
			return value;
		} else {
			return -value;
		}

	}

	public String getOrderBy() {

		if (_asc) {
			return ORDER_BY_ASC;
		} else {
			return ORDER_BY_DESC;
		}
	}

	private boolean _asc;

}
