package it.bysoftware.ct.comparators;

import it.bysoftware.ct.model.Ordine;

import com.liferay.portal.kernel.util.OrderByComparator;

public class OrderAmountComparator extends OrderByComparator {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1348717136835003085L;

	public static String ORDER_BY_ASC = "importo ASC";
	public static String ORDER_BY_DESC = "importo DESC";

	public OrderAmountComparator() {
		this(false);
	}

	public OrderAmountComparator(boolean asc) {
		_asc = asc;
	}

	public int compare(Object obj1, Object obj2) {
		
		Ordine o1 = (Ordine) obj1;
		Ordine o2 = (Ordine) obj2;

		double o1Amount = o1.getImporto();
		double o2Amount = o2.getImporto();
		int value = (o1Amount >= o2Amount) ? 1 : -1;
		
		if (this._asc) {
			return value;
		} else {
			return -value;
		}

	}

	public String getOrderBy() {

		if (_asc) {
			return ORDER_BY_ASC;
		} else {
			return ORDER_BY_DESC;
		}
	}

	private boolean _asc;

}
