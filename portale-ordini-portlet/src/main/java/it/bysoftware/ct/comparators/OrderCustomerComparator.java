package it.bysoftware.ct.comparators;

import it.bysoftware.ct.model.Ordine;

import com.liferay.portal.kernel.util.OrderByComparator;

public class OrderCustomerComparator extends OrderByComparator {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1348717136835003085L;

	public static String ORDER_BY_ASC = "cliente ASC";
	public static String ORDER_BY_DESC = "cliente DESC";

	public OrderCustomerComparator() {
		this(false);
	}

	public OrderCustomerComparator(boolean asc) {
		this._asc = asc;
	}

	public int compare(Object obj1, Object obj2) {
		
		Ordine o1 = (Ordine) obj1;
		Ordine o2 = (Ordine) obj2;

		String o1Customer = o1.getCliente();
		String o2Customer = o2.getCliente();

		int value = o2Customer.compareToIgnoreCase(o1Customer);
		
		if (this._asc) {
			return value;
		} else {
			return -value;
		}

	}

	public String getOrderBy() {

		if (_asc) {
			return ORDER_BY_ASC;
		} else {
			return ORDER_BY_DESC;
		}
	}

	private boolean _asc;

}
