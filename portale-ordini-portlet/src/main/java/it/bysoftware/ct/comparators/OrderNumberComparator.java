package it.bysoftware.ct.comparators;

import it.bysoftware.ct.model.Ordine;

import com.liferay.portal.kernel.util.OrderByComparator;

public class OrderNumberComparator extends OrderByComparator {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1348717136835003085L;

	public static String ORDER_BY_ASC = "numero ASC";
	public static String ORDER_BY_DESC = "numero DESC";

	public OrderNumberComparator() {
		this(false);
	}

	public OrderNumberComparator(boolean asc) {
		_asc = asc;
	}

	public int compare(Object obj1, Object obj2) {
		
		Ordine o1 = (Ordine) obj1;
		Ordine o2 = (Ordine) obj2;

		int o1Num = o1.getNumero();
		String o1Cen = o1Num + "/" + o1.getCentro();
		int o2Num = o2.getNumero();
		String o2Cen = o2Num + "/" + o2.getCentro();
		int value = 0;
		value = o2Cen.compareToIgnoreCase(o1Cen);
		
		if (this._asc) {
			return value;
		} else {
			return -value;
		}

	}

	public String getOrderBy() {

		if (_asc) {
			return ORDER_BY_ASC;
		} else {
			return ORDER_BY_DESC;
		}
	}

	private boolean _asc;

}
