package it.bysoftware.ct.comparators;

import com.liferay.portal.kernel.util.OrderByComparator;
import it.bysoftware.ct.model.Piante;

public class PlantsByCodeComparator extends OrderByComparator {

	private boolean _asc;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7103999305184114472L;
	
	public PlantsByCodeComparator() {
		this(false);
	}

	public PlantsByCodeComparator(boolean asc) {
		this._asc = asc;
	}

	@Override
	public int compare(Object obj1, Object obj2) {
		Piante p1 = (Piante) obj1;
		Piante p2 = (Piante) obj2;

		String p1name = p1.getCodice();
		String p2name = p2.getCodice();

		int value = p1name.compareToIgnoreCase(p2name);
		
		if (_asc) {
			return value;
		} else {
			return -value;
		}
	}

}
