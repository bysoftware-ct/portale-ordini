package it.bysoftware.ct.comparators;

import it.bysoftware.ct.model.Ordine;

import com.liferay.portal.kernel.util.OrderByComparator;

public class OrderDateComparator extends OrderByComparator {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1348717136835003085L;

	public static String ORDER_BY_ASC = "dataInserimento ASC";
	public static String ORDER_BY_DESC = "dataInserimento DESC";

	public OrderDateComparator() {
		this(false);
	}

	public OrderDateComparator(boolean asc) {
		_asc = asc;
	}

	public int compare(Object obj1, Object obj2) {
		
		Ordine o1 = (Ordine) obj1;
		Ordine o2 = (Ordine) obj2;

		long o1Date = o1.getDataInserimento().getTime();
		long o2Date = o2.getDataInserimento().getTime();

		int value = 0;
		if (o1Date == o2Date) {
			if (o1.getStato() >= o2.getStato()) {
				value = 1;
			} else {
				value = -1;
			}
		}

		if (this._asc) {
			return value;
		} else {
			return -value;
		}

	}

	public String getOrderBy() {

		if (_asc) {
			return ORDER_BY_ASC;
		} else {
			return ORDER_BY_DESC;
		}
	}

	private boolean _asc;

}
