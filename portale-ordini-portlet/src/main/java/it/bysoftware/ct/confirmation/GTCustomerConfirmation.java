package it.bysoftware.ct.confirmation;

import it.bysoftware.ct.model.AnagraficheClientiFornitori;
import it.bysoftware.ct.model.Ordine;
import it.bysoftware.ct.orderexport.CSVExporter;
import it.bysoftware.ct.orderexport.GTOrderExporter;
import it.bysoftware.ct.utils.Report;
import it.bysoftware.ct.utils.Utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.mail.internet.AddressException;
import javax.naming.NamingException;

import net.sf.jasperreports.engine.JRException;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.portlet.PortletProps;

public class GTCustomerConfirmation extends CustomerConfirmation {

	private static final String EXPORT_FOLDER = PortletProps
			.get("export-folder");

	private static final String GT_FOLDER = PortletProps.get("gt");

	private static final String GT_FOLDER_ORDER = PortletProps
			.get("gt-folder-order");

	private String csv;

	/**
	 * Logger object. Based on Liferay logger
	 * {@link com.liferay.portal.kernel.log.Log} commons logging.
	 */
	private Log logger = LogFactoryUtil.getLog(GTCustomerConfirmation.class);

	public String getCsv() {
		return csv;
	}

	public void setCsv(String csv) {
		this.csv = csv;
	}

	public GTCustomerConfirmation(String fromEmail, Ordine o, String name,
			String l, long uId, AnagraficheClientiFornitori r, String toEmail) {
		super(fromEmail, o, name, l, uId, r, toEmail);
	}

	@Override
	public void createConfirmation(Connection c) throws NamingException,
			SQLException, JRException {
		Report r = new Report(c);

		String[] attachments = new String[2];
		attachments[0] = r.print(this.getUserId(), this.getLogo(), this
				.getOrder().getId(), this.getReportName(), "", true);
//		r.closeConnection();
		String fileName = Utils.extractFileNameWithoutExtension(attachments[0]);
		try {
			CSVExporter exporter = new GTOrderExporter(this.getOrder(),
					this.getRecipient());
			File exportFolder = new File(EXPORT_FOLDER);
			File exportedOrder = new File(exportFolder + File.separator
					+ GT_FOLDER + File.separator + GT_FOLDER_ORDER
					+ File.separator + fileName + ".csv");
			FileWriter fw;
			BufferedWriter bw;
			PrintWriter out;
			fw = new FileWriter(exportedOrder.getAbsoluteFile(), true);
			bw = new BufferedWriter(fw);
			out = new PrintWriter(bw);
			out.print(exporter.exportDocument());
			out.close();
			attachments[1] = exportedOrder.getAbsolutePath();
		} catch (PortalException e) {
			this.logger.error(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
		} catch (SystemException e) {
			this.logger.error(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			this.logger.error(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
		}
		this.setAttachments(attachments);
	}

	@Override
	public void sendConfirmation() throws AddressException, PortalException,
			SystemException {
		super.sendConfirmation();
	}

}
