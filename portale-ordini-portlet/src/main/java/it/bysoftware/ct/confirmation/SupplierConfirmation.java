/**
 * 
 */
package it.bysoftware.ct.confirmation;

import it.bysoftware.ct.model.AnagraficheClientiFornitori;
import it.bysoftware.ct.model.Ordine;
import it.bysoftware.ct.utils.Report;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;

import net.sf.jasperreports.engine.JRException;

/**
 * Class to manage supplier confirmation email.
 *  
 * @author Mario Torrisi 
 */
public class SupplierConfirmation extends Confirmation {

    /**
     * Creates a confirmation object.
     * 
     * @param email
     *            from email address
     * @param order
     *            order id is going to be confirmed
     * @param name
     *            report name
     * @param l
     *            logo name
     * @param uId
     *            user id
     * @param r
     *            the confirmation recipient
     * @throws ConfirmationException
     *             exception
     * 
     */
    public SupplierConfirmation(final String email, final Ordine order,
            final String name, final String l, final long uId,
            final AnagraficheClientiFornitori r) throws ConfirmationException {
        super(email, order, name, l, uId, r);
    }

    /**
     * Creates a confirmation object.
     * 
     * @param fromEmail
     *            from email address
     * @param order
     *            order id is going to be confirmed
     * @param name
     *            report name
     * @param l
     *            logo name
     * @param uId
     *            user id
     * @param r
     *            the confirmation recipient
     * @param toEmail
     *            temporary recipient email
     * 
     */
    public SupplierConfirmation(final String fromEmail, final Ordine order,
            final String name, final String l, final long uId,
            final AnagraficheClientiFornitori r, final String toEmail) {
        super(fromEmail, order, name, l, uId, r, toEmail);
    }

    @Override
    public final void createConfirmation(final Connection c)
            throws NamingException, SQLException, JRException {
        Report r = new Report(c);
        this.setReport(r.print(this.getUserId(), this.getLogo(),
                this.getOrder().getId(), this.getReportName(),
                this.getRecipient().getCodiceAnagrafica(), true));
        r.closeConnection();

    }

}
