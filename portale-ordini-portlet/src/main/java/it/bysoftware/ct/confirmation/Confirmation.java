package it.bysoftware.ct.confirmation;

import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.Http;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.User;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.ContentUtil;
import com.liferay.util.portlet.PortletProps;

import it.bysoftware.ct.model.AnagraficheClientiFornitori;
import it.bysoftware.ct.model.ClientiFornitoriDatiAgg;
import it.bysoftware.ct.model.Ordine;
import it.bysoftware.ct.model.Rubrica;
import it.bysoftware.ct.model.Stati;
import it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil;
import it.bysoftware.ct.service.RubricaLocalServiceUtil;
import it.bysoftware.ct.service.StatiLocalServiceUtil;
import it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.naming.NamingException;

import net.sf.jasperreports.engine.JRException;

/**
 * Class to manage confirmation email.
 *  
 * @author Mario Torrisi 
 */
public abstract class Confirmation {

    /**
     * Template extension constant.
     */
    private static final String TMPL = ".tmpl";
    
    /**
     * Email from.
     */
    private static final String FROM = "mail-from";

    /**
     * From email address.
     */
    private String emailFrom;

    /**
     * Order.
     */
    private Ordine order;

    /**
     * The report name.
     */
    private String reportName;

    /**
     * Logo name.
     */
    private String logo;

    /**
     * Company user id.
     */
    private long userId;

    /**
     * Report file.
     */
    private String report;
    
    /**
     * Report file.
     */
    private String[] attachments;

    /**
     * Current recipient.
     */
    private AnagraficheClientiFornitori recipient;

    /**
     * Recipient's email.
     */
    private String emailTo;

    /**
     * Agent customer business name.
     */
    private String agentCustomer;

    /**
     * Creates a confirmation object.
     * 
     * @param email
     *            from email address
     * @param o
     *            order id is going to be confirmed
     * @param name
     *            report name
     * @param l
     *            logo name
     * @param uId
     *            user id
     * @param r
     *            the confirmation recipient
     * @throws ConfirmationException
     *             exception
     * 
     */
    public Confirmation(final String email, final Ordine o, final String name,
            final String l, final long uId, final AnagraficheClientiFornitori r)
            throws ConfirmationException {
        this(email, o, name, l, uId, r, getRecipientEmail(r));
    }

    /**
     * Creates a confirmation object.
     * 
     * @param fromEmail
     *            from email address
     * @param o
     *            order id is going to be confirmed
     * @param name
     *            report name
     * @param l
     *            logo name
     * @param uId
     *            user id
     * @param r
     *            the confirmation recipient
     * @param toEmail
     *            temporary recipient email
     * 
     */
    public Confirmation(final String fromEmail, final Ordine o,
            final String name, final String l, final long uId,
            final AnagraficheClientiFornitori r, final String toEmail) {
        this.emailFrom = fromEmail;
        this.emailTo = toEmail;
        this.order = o;
        this.reportName = name;
        this.logo = l;
        this.userId = uId;
        this.recipient = r;

    }

    /**
     * Creates a confirmation object.
     * 
     * @param fromEmail
     *            from email address
     * @param o
     *            order id is going to be confirmed
     * @param name
     *            report name
     * @param l
     *            logo name
     * @param uId
     *            user id
     * @param r
     *            the confirmation recipient
     * @param toEmail
     *            temporary recipient email
     * @param aCustomer
     *            agent customer business name.
     * 
     */
    public Confirmation(final String fromEmail, final Ordine o,
            final String name, final String l, final long uId,
            final AnagraficheClientiFornitori r, final String toEmail,
            final String aCustomer) {
        this.emailFrom = fromEmail;
        this.emailTo = toEmail;
        this.order = o;
        this.reportName = name;
        this.logo = l;
        this.userId = uId;
        this.recipient = r;
        this.setAgentCustomer(aCustomer);
    }
    
    /**
     * Returns the report name.
     * 
     * @return report name
     */
    public final String getReportName() {
        return this.reportName;
    }

    /**
     * Sets the report name.
     * 
     * @param name
     *            report name
     */
    public final void setReportName(final String name) {
        this.reportName = name;
    }

    /**
     * Returns the order.
     * 
     * @return order id
     */
    public final Ordine getOrder() {
        return this.order;
    }

    /**
     * Sets the order.
     * 
     * @param o
     *            order
     */
    public final void setOrderId(final Ordine o) {
        this.order = o;
    }

    /**
     * Returns the from email address.
     * 
     * @return the fromEmail
     */
    public final String getFromEmail() {
        return this.emailFrom;
    }

    /**
     * Sets the from email address.
     * 
     * @param email
     *            the fromEmail to set
     */
    public final void setFromEmail(final String email) {
        this.emailFrom = email;
    }

    
    /**
     * Returns the to email address.
     * 
     * @return the fromEmail
     */
    public final String getEmailTo() {
		return emailTo;
	}

	/**
     * Returns logo file name.
     * 
     * @return the logo
     */
    public final String getLogo() {
        return this.logo;
    }

    /**
     * Sets logo file name.
     * 
     * @param l
     *            the logo to set
     */
    public final void setLogo(final String l) {
        this.logo = l;
    }

    /**
     * Returns user id.
     * 
     * @return the userId
     */
    public final long getUserId() {
        return this.userId;
    }

    /**
     * Sets user id.
     * 
     * @param uId
     *            the userId to set
     */
    public final void setUserId(final long uId) {
        this.userId = uId;
    }

    /**
     * Returns the report file path.
     * 
     * @return the report file path
     */
    public final String getReport() {
        return this.report;
    }

    /**
     * Sets the report file path.
     * 
     * @param r
     *            the report file path to set
     */
    public final void setReport(final String r) {
        this.report = r;
    }

    /**
     * Returns the confirmation recipients.
     * 
     * @return the recipient
     */
    public final AnagraficheClientiFornitori getRecipient() {
        return this.recipient;
    }

    /**
     * Sets the confirmation recipients.
     * 
     * @param r
     *            the recipient to set
     */
    public final void setRecipient(final AnagraficheClientiFornitori r) {
        this.recipient = r;
    }

    /**
     * Sends the confirmation.
     * 
     * @throws AddressException
     *             exception
     * @throws SystemException
     *             exception
     * @throws PortalException
     *             exception
     */
    public void sendConfirmation() throws AddressException,
            PortalException, SystemException {
        Stati s = StatiLocalServiceUtil.getStati(this.getRecipient().
                getSiglaStato());
        String contentFolder = "/content/IT/";
        if (!"IT".equals(s.getCodiceISO())) {
            contentFolder = "/content/EN/";
        }
        ClientiFornitoriDatiAgg data = ClientiFornitoriDatiAggLocalServiceUtil.
                fetchClientiFornitoriDatiAgg(new ClientiFornitoriDatiAggPK(
                        this.recipient.getCodiceAnagrafica(), true));
        String body = "";
        if (this.order.getModificato()) {
            if (data != null && data.getAssociato()) {
                body = ContentUtil.get(contentFolder + this.getReportName()
                        + CharPool.DASH + "edited" + Confirmation.TMPL, true);
            } else {
                body = ContentUtil.get(contentFolder + this.getReportName()
                        + CharPool.DASH + "ext" + CharPool.DASH + "edited"
                        + Confirmation.TMPL, true);
            }
        } else {
            if (data != null && data.getAssociato()) {
                body = ContentUtil.get(contentFolder + this.getReportName()
                        + Confirmation.TMPL, true);
            } else {
                body = ContentUtil.get(contentFolder + this.getReportName()
                        + CharPool.DASH + "ext" + Confirmation.TMPL, true);
            }
        }
        User u = UserLocalServiceUtil.getUser(this.getUserId());
        body = StringUtil.replace(
                body,
                new String[] { "[$CUSTOMER$]", "[$PASSPORT_URL$]",
                        "[$ORDER_#$]", "[$CENTER$]", "[$AGENT$]" },
                new String[] {
                        this.getRecipient().getRagioneSociale1(),
                        PortalUtil.getPortalURL(CompanyLocalServiceUtil.
                                getCompanyById(u.getCompanyId()).
                                getVirtualHostname() + "/gestione-passaporto",
                                Http.HTTPS_PORT, true),
                        String.valueOf(this.order.getNumero()),
                        this.order.getCentro(), this.agentCustomer });

        MailMessage mailMessage = new MailMessage();
        mailMessage.setBody(body);

        mailMessage.setSubject("Copia Ordine N° " + this.order.getNumero() 
                + "/" + this.order.getCentro());
//        mailMessage.setFrom(new InternetAddress(this.getFromEmail()));
        String from = PortletProps.get(FROM);
        if (Validator.isEmailAddress(from)) {
        	mailMessage.setFrom(new InternetAddress(from));
		} else {
			mailMessage.setFrom(new InternetAddress(this.getFromEmail()));
		}
        
        mailMessage.setTo(new InternetAddress(this.emailTo));

        if (this.attachments == null) {
        	mailMessage.addFileAttachment(new File(this.getReport()));
		} else {
			for (int i = 0; i < attachments.length; i++) {
				mailMessage.addFileAttachment(new File(this.attachments[i]));
			}
		}
        MailServiceUtil.sendEmail(mailMessage);
    }

    /**
     * Create the confirmation report and return the path to the file.
     * 
     * @param c
     *            database connection
     * 
     * @throws SQLException
     *             exception
     * @throws NamingException
     *             exception
     * @throws JRException
     *             exception
     */
    public abstract void createConfirmation(final Connection c)
            throws NamingException, SQLException, JRException;
    
    /**
     * Retrieves the recipient email address.
     * 
     * @param r
     *            the confirmation recipient
     * @return the recipient email address
     * @throws ConfirmationException
     *             exception
     */
    private static String getRecipientEmail(final AnagraficheClientiFornitori r)
            throws ConfirmationException {
        List<Rubrica> contacts = RubricaLocalServiceUtil.findContacts(r.
                getCodiceAnagrafica());
        if (contacts.size() > 0) {
            return contacts.get(0).getEMailTo();
        } else {
            throw new ConfirmationException("No contacts found.");
        }
    }

    /**
     * Returns the agent customer business name.
     * 
     * @return the agentCustomer
     */
    public final String getAgentCustomer() {
        return this.agentCustomer;
    }

    /**
     * Sets the agent customer business name.
     * 
     * @param aC
     *            the agent customer to set
     */
    public final void setAgentCustomer(final String aC) {
        this.agentCustomer = aC;
    }

	public String[] getAttachments() {
		return attachments;
	}

	public void setAttachments(String[] attachments) {
		this.attachments = attachments;
	}
}
