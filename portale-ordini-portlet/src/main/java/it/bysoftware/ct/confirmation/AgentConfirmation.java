package it.bysoftware.ct.confirmation;

import it.bysoftware.ct.model.AnagraficheClientiFornitori;
import it.bysoftware.ct.model.Ordine;
import it.bysoftware.ct.utils.Report;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;

import net.sf.jasperreports.engine.JRException;

/**
 * Class to manage agent confirmation email.
 * 
 * @author Mario Torrisi
 *
 */
public class AgentConfirmation extends Confirmation {

    /**
     * Creates a confirmation object.
     * 
     * @param email
     *            from email address
     * @param o
     *            order is going to be confirmed
     * @param name
     *            report name
     * @param l
     *            logo name
     * @param uId
     *            user id
     * @param r
     *            the confirmation recipient
     * @throws ConfirmationException
     *             exception
     * 
     */
    public AgentConfirmation(final String email, final Ordine o,
            final String name, final String l, final long uId,
            final AnagraficheClientiFornitori r) throws ConfirmationException {
        super(email, o, name, l, uId, r);
    }

    /**
     * Creates a confirmation object.
     * 
     * @param fromEmail
     *            from email address
     * @param o
     *            order id is going to be confirmed
     * @param name
     *            report name
     * @param l
     *            logo name
     * @param uId
     *            user id
     * @param r
     *            the confirmation recipient
     * @param toEmail
     *            temporary recipient email
     * @param customer
     *            agent customer business name
     */
    public AgentConfirmation(final String fromEmail, final Ordine o,
            final String name, final String l, final long uId,
            final AnagraficheClientiFornitori r, final String toEmail,
            final String customer) {
        super(fromEmail, o, name, l, uId, r, toEmail, customer);
    }

    @Override
    public final void createConfirmation(final Connection c)
            throws NamingException, SQLException, JRException {
        Report r = new Report(c);
        this.setReport(r.print(this.getUserId(), this.getLogo(),
                this.getOrder().getId(), "customer-confirm", "", true));
    }

}
