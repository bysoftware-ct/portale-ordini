/**
 * 
 */
package it.bysoftware.ct.confirmation;

/**
 * @author Aliseo-G
 *
 */
public class ConfirmationException extends Exception {

    /**
     * Auto generated serial Id.
     */
    private static final long serialVersionUID = -3794700093861770968L;

    /**
     * Creates a confirmation exception.
     * 
     * @param msg
     *            message
     */
    public ConfirmationException(final String msg) {
        super(msg);
    }

}
