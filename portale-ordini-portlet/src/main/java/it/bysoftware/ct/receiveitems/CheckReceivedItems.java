package it.bysoftware.ct.receiveitems;

import it.bysoftware.ct.model.Riga;
import it.bysoftware.ct.service.RigaLocalServiceUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class CheckReceivedItems
 */
public class CheckReceivedItems extends MVCPortlet {
	/**
	 * Logger object. Based on Liferay logger
	 * {@link com.liferay.portal.kernel.log.Log} commons logging.
	 */
	private static Log logger = LogFactoryUtil.getLog(CheckReceivedItems.class);

	public final void saveReceivedItems(final ActionRequest areq,
			final ActionResponse ares) {
		logger.info("Saving received items...");
		final long rowId = ParamUtil.getLong(areq, "rowId", -1);
		logger.info(rowId);
		if (rowId > 0) {
			try {
				Riga row = RigaLocalServiceUtil.getRiga(rowId);
				row.setRicevuto(!row.getRicevuto());
				RigaLocalServiceUtil.updateRiga(row);
			} catch (PortalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {

		}
		PortalUtil.copyRequestParameters(areq, ares);
		ares.setRenderParameter("jspPage", "/html/receiveitems/order.jsp");
	}
}
