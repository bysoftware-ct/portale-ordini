package it.bysoftware.ct;

import it.bysoftware.ct.confirmation.CarrierConfirmation;
import it.bysoftware.ct.confirmation.Confirmation;
import it.bysoftware.ct.confirmation.ConfirmationException;
import it.bysoftware.ct.confirmation.CustomerConfirmation;
import it.bysoftware.ct.confirmation.SupplierConfirmation;
import it.bysoftware.ct.model.AnagraficheClientiFornitori;
import it.bysoftware.ct.model.Articoli;
import it.bysoftware.ct.model.Carrello;
import it.bysoftware.ct.model.ClientiFornitoriDatiAgg;
import it.bysoftware.ct.model.DescrizioniVarianti;
import it.bysoftware.ct.model.Ordine;
import it.bysoftware.ct.model.PianoPagamenti;
import it.bysoftware.ct.model.Piante;
import it.bysoftware.ct.model.Riga;
import it.bysoftware.ct.model.RigoDocumento;
import it.bysoftware.ct.model.Rubrica;
import it.bysoftware.ct.model.TestataDocumento;
import it.bysoftware.ct.model.Vettori;
import it.bysoftware.ct.scheduler.service.GiveDocumentExporterService;
import it.bysoftware.ct.scheduler.service.OrderExporterService;
import it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.ArticoliLocalServiceUtil;
import it.bysoftware.ct.service.CarrelloLocalServiceUtil;
import it.bysoftware.ct.service.CategorieMerceologicheLocalServiceUtil;
import it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil;
import it.bysoftware.ct.service.DescrizioniVariantiLocalServiceUtil;
import it.bysoftware.ct.service.OrdineLocalServiceUtil;
import it.bysoftware.ct.service.PianoPagamentiLocalServiceUtil;
import it.bysoftware.ct.service.PianteLocalServiceUtil;
import it.bysoftware.ct.service.RigaLocalServiceUtil;
import it.bysoftware.ct.service.RigoDocumentoLocalServiceUtil;
import it.bysoftware.ct.service.RubricaLocalServiceUtil;
import it.bysoftware.ct.service.TestataDocumentoLocalServiceUtil;
import it.bysoftware.ct.service.VettoriLocalServiceUtil;
import it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK;
import it.bysoftware.ct.service.persistence.DescrizioniVariantiPK;
import it.bysoftware.ct.service.persistence.RubricaPK;
import it.bysoftware.ct.service.persistence.TestataDocumentoPK;
import it.bysoftware.ct.utils.Constants;
import it.bysoftware.ct.utils.OrderState;
import it.bysoftware.ct.utils.Report;
import it.bysoftware.ct.utils.ResourceId;
import it.bysoftware.ct.utils.Response;
import it.bysoftware.ct.utils.Response.Code;
import it.bysoftware.ct.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.internet.AddressException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.portlet.documentlibrary.util.DLUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.liferay.util.portlet.PortletProps;

/**
 * Portlet implementation class Ordini.
 * 
 * @author Mario Torrisi 
 */
public class Ordini extends MVCPortlet {

    /**
     * No order specified constant.
     */
    private static final String NO_ORDER_SPECIFIED = "No order specified.";

    /**
     * Order id constant.
     */
    private static final String ORDER_ID = "orderId";

    /**
     * Customer confirm report name constant.
     */
    private static final String SUPPLIER_CONFIRM = "supplier-confirm";

    /**
     * Id ordine constant.
     */
    private static final String ID_ORDER = "idOrdine";

    /**
     * Variant code constant.
     */
    private static final String CODE_VARIANT = "codeVariant";

    /**
     * Customer code constant.
     */
    private static final String CUSTOMER_CODE = "customerCode";

    /**
     * Quantity constant.
     */
    private static final String QUANTITY = "quantity";

    /**
     * Quantity constant.
     */
    private static final String SUPPLIER_PRICE = "supplierPrice";
    
    /**
     * Id plant constant.
     */
    private static final String ID_PLANT = "idPlant";

    /**
     * Current order constant.
     */
    private static final String CURRENT_ORDER = "currentOrder";

    /**
     * Cart constant.
     */
    private static final String CART = "Cart: ";

    /**
     * Exception args constant.
     */
    private static final String EXCEPTION_ARGS = "exceptionArgs";

    /**
     * Export folder path.
     */
    private static final String EXPORT_FOLDER = PortletProps.get(
            "export-folder");
    
    /**
    * Export folder path.
    */
   private static final String DOCUMENTS_FOLDER = EXPORT_FOLDER
           + File.separator + "ORDINI" + File.separator;

    /**
     * Item over code.
     * @author Aliseo-G
     *
     */
    private static enum ItemOverCode {
        
        /**
         * Code for 2nd row.
         */
        s2,
        
        /**
         * Code for 3rd row.
         */
        s3,
        
        /**
         * Code for 4th row.
         */
        s4
    };
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(Ordini.class);

    /**
     * Cart default height.
     */
    private int height;
    
    /**
     * Print label default cost.
     */
    private double labelPrintCost;

    /**
     * Date formatter.
     */
    private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    /**
     * Data source connection.
     */
    private DataSource ds;

    /**
     * Actually adds the new row to the order.
     * 
     * @param orderJson
     *            current order
     * @param item
     *            adding item
     * @param p
     *            adding item
     * @param v
     *            selected variant
     * @param cartJson
     *            the cart where add the row
     * @param idRow
     *            row id
     * @param usedHeight
     *            cart's used height
     * @param itemOver
     *            true if item will be placed over other in the cart, false
     *            otherwise
     * @param isLastIncart
     *            true if is the last row in the cart, false otherwise
     * @param discount item discount 
     * @throws SystemException
     *             exception
     * @throws JSONException
     *             exception
     */
    private void addNewRow(final JSONObject orderJson, final Articoli item,
            final Piante p, final DescrizioniVarianti v,
            final JSONObject cartJson, final int idRow, final int usedHeight,
            final boolean itemOver, final boolean isLastIncart,
            final double discount) throws SystemException, JSONException {
        orderJson.put(Constants.ROWS_NUMBER, idRow + 1);
        JSONArray rowsJson = cartJson.getJSONArray(Constants.ROWS);
        int prevProgr = 0;
        JSONObject row = rowsJson.getJSONObject(0);
        if (row != null) {
        	int first = rowsJson.getJSONObject(0).getInt("progressivo", 0);
            int last = rowsJson.getJSONObject(rowsJson.length() - 1).getInt("progressivo", 0);
            if (last >= first) {
            	prevProgr = last;
            } else {
            	prevProgr = first;
            }
        }
        JSONObject newRow = this.createNewRow(cartJson.getLong(Constants.ID),
                idRow + 1, prevProgr + 1, item, p, v, itemOver,
                isLastIncart, discount);
        int cartPlants = cartJson.getInt(Constants.PLANTS, 0)
                + newRow.getInt(Constants.PLATFORM_ITEMS, 0);
        if (isLastIncart && p.getPianteCarrello() != cartPlants) {
            int lastRowItems = newRow.getInt(Constants.PLATFORM_ITEMS, 0); 
            if (lastRowItems + (p.getPianteCarrello() - cartPlants) > 0) {
                newRow.put(Constants.PLATFORM_ITEMS, lastRowItems
                        + (p.getPianteCarrello() - cartPlants));
                double amount = newRow.getDouble("prezzo")
                        * newRow.getInt(Constants.PLATFORM_ITEMS);
                newRow.put(Constants.AMOUNT, amount);
            }
            
            cartPlants = p.getPianteCarrello();
        }
        rowsJson.put(newRow);
        double cartPrice = cartJson.getDouble(Constants.AMOUNT)
                + newRow.getDouble(Constants.AMOUNT, 0);
        cartJson.put(Constants.AMOUNT, cartPrice);
        
        cartJson.put(Constants.PLANTS, cartPlants);
        if (itemOver) {
            int x = 0;
            if (newRow.getInt(ItemOverCode.s2.name(), 0) > 0) {
                x += p.getAggiuntaRipiano(); 
            }
            if (newRow.getInt(ItemOverCode.s3.name(), 0) > 0) {
                x += p.getAggiuntaRipiano(); 
            }
            if (newRow.getInt(ItemOverCode.s4.name(), 0) > 0) {
                x += p.getAggiuntaRipiano(); 
            }
            cartJson.put(Constants.USED_HEIGHT, usedHeight + p.getAltezza()
                    + x);
        } else {
            cartJson.put(Constants.USED_HEIGHT, usedHeight + p.getAltezza());
        }
        cartJson.put(Constants.CLOSED,
                usedHeight + p.getAltezza() == this.height);
    }

    /**
     * Actually adds the new row to the order.
     * 
     * @param orderJson
     *            current order
     * @param item
     *            adding item
     * @param p
     *            adding item
     * @param v
     *            selected variant
     * @param cartJson
     *            the cart where add the row
     * @param idRow
     *            row id
     * @param quantity
     *            items quantity 
     * @param itemOver
     *            true if item will be placed over other in the cart, false
     *            otherwise
     * @param discount item discount
     * @throws SystemException
     *             exception
     * @throws JSONException
     *             exception
     */
    private void addNewGenericRow(final JSONObject orderJson,
            final Articoli item, final Piante p, final DescrizioniVarianti v,
            final JSONObject cartJson, final int idRow, final int quantity,
            final boolean itemOver, final double discount)
                    throws SystemException, JSONException {
        orderJson.put(Constants.ROWS_NUMBER, idRow + 1);
        JSONArray rowsJson = cartJson.getJSONArray(Constants.ROWS);
        int prevProgr = 0;
        JSONObject row = rowsJson.getJSONObject(0);
        if (row != null) {
            prevProgr = row.getInt("progressivo", 0);
        }
        JSONObject newRow = this.createNewGenericRow(cartJson.getLong(
                Constants.ID), idRow + 1, prevProgr + 1, item, p, v,
                quantity, itemOver, discount);
        rowsJson.put(newRow);
        double cartPrice = cartJson.getDouble(Constants.AMOUNT)
                + newRow.getDouble(Constants.AMOUNT, 0);
        cartJson.put(Constants.AMOUNT, cartPrice);
        int cartPlants = cartJson.getInt(Constants.PLANTS, 0) + quantity;
        cartJson.put(Constants.PLANTS, cartPlants);
        cartJson.put(Constants.USED_HEIGHT, -1);
    }
    
    /**
     * Adds cart to current order.
     * 
     * @param resourceRequest
     *            {@link ResourceRequest}
     * @return {@link Response}
     */
    private Response addCart(final ResourceRequest resourceRequest) {
        JSONObject orderJson = null;

        String customerCode = ParamUtil.getString(resourceRequest,
                Ordini.CUSTOMER_CODE);
        try {
            orderJson = JSONFactoryUtil.createJSONObject(ParamUtil.getString(
                    resourceRequest, Ordini.CURRENT_ORDER));

            if (orderJson.length() == 0) {
                this.logger.debug("Empty order...");
                String orderDateStr = ParamUtil.getString(resourceRequest,
                        "orderDate");
                String shipDateStr = ParamUtil.getString(resourceRequest,
                        "shipDate");
                orderJson = this.createNewOrder(customerCode, orderDateStr,
                        shipDateStr, false);
                this.logger.debug("Creating new order tmp id: " + orderJson);
            } else {
                this.logger.debug("NOT empty order...");
            }

            long idPlant = ParamUtil.getLong(resourceRequest, Ordini.ID_PLANT);
            if (idPlant > 0) {
                Piante p = PianteLocalServiceUtil.getPiante(idPlant);
                String codeVariant = ParamUtil.getString(resourceRequest,
                        Ordini.CODE_VARIANT, null);
                double price1 = ParamUtil.getDouble(resourceRequest, "price1",
                        0.0);
                double price2 = ParamUtil.getDouble(resourceRequest, "price2",
                        0.0);
                double supplierPrice = ParamUtil.getDouble(resourceRequest,
                        "supplierPrice", 0.0);
                double discount = ParamUtil.getDouble(resourceRequest,
                        "discount", 0.0);
                Articoli item = ArticoliLocalServiceUtil.getArticoli(p.
                        getCodice());
                if (item.getPrezzo1() != price1
                        || item.getPrezzo2() != price2) {
                    item.setPrezzo1(price1);
                    item.setPrezzo2(price2);
                }
                if (p.getPrezzoFornitore() != supplierPrice) {
                    p.setPrezzoFornitore(supplierPrice);
                }
                DescrizioniVarianti v = null;
                if (Validator.isNotNull(codeVariant)
                        && !Validator.isBlank(codeVariant)) {
                    v = DescrizioniVariantiLocalServiceUtil.
                            getDescrizioniVarianti(new DescrizioniVariantiPK(
                                    item.getCodiceArticolo(), codeVariant));
                }

                int quantity = ParamUtil.getInteger(resourceRequest,
                        Ordini.QUANTITY);
                for (int i = 1; i <= quantity; i++) {
                    this.addCartToOrder(orderJson, item, p, v, discount);
                }
            }
        } catch (JSONException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (PortalException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (ParseException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        return new Response(Response.Code.OK, orderJson.toString());
    }

    /**
     * Adds the specified item's quantity to the order.
     * 
     * @param resourceRequest
     *            the resource request containing items.
     * @return {@link Response} containing the updated order.
     */
    private Response addQuantity(final ResourceRequest resourceRequest) {
        String customerCode = ParamUtil.getString(resourceRequest,
                Ordini.CUSTOMER_CODE);
        JSONObject orderJson = null;
        try {
            orderJson = JSONFactoryUtil.createJSONObject(ParamUtil.getString(
                    resourceRequest, Ordini.CURRENT_ORDER));

            this.logger.debug("orderJSON: " + orderJson.toString());

            if (orderJson.length() == 0) {
                this.logger.debug("Empty order  ...");
                String orderDateStr = ParamUtil.getString(resourceRequest,
                        "orderDate");
                String shipDateStr = ParamUtil.getString(resourceRequest,
                        "shipDate");
                orderJson = this.createNewOrder(customerCode, orderDateStr,
                        shipDateStr, true);
                this.logger.debug("Creating new order tmp id: " + orderJson);
            } else {
                this.logger.debug("NOT empty order...");
            }

            long idPlant = ParamUtil.getLong(resourceRequest, Ordini.ID_PLANT);
            if (idPlant > 0) {
                Piante p = PianteLocalServiceUtil.getPiante(idPlant);
                String codeVariant = ParamUtil.getString(resourceRequest,
                        Ordini.CODE_VARIANT, null);
                Articoli item = ArticoliLocalServiceUtil.getArticoli(p.
                        getCodice());
                double price1 = ParamUtil.getDouble(resourceRequest, "price1",
                        0.0);
                double price2 = ParamUtil.getDouble(resourceRequest, "price2",
                        0.0);
                double discount = ParamUtil.getDouble(resourceRequest,
                        "discount", 0.0);
                if (item.getPrezzo1() != price1 
                        || item.getPrezzo2() != price2) {
                    item.setPrezzo1(price1);
                    item.setPrezzo2(price2);
                }
                
                
                DescrizioniVarianti v = null;
                if (Validator.isNotNull(codeVariant)
                        && !Validator.isBlank(codeVariant)) {
                    v = DescrizioniVariantiLocalServiceUtil.
                            getDescrizioniVarianti(new DescrizioniVariantiPK(
                                    item.getCodiceArticolo(), codeVariant));
                }

                int quantity = ParamUtil.getInteger(resourceRequest,
                        Ordini.QUANTITY);
                double suppPrice = ParamUtil.getDouble(resourceRequest,
                        Ordini.SUPPLIER_PRICE, 0);
                this.logger.debug("CIAO CICCIO: " + suppPrice);
                if (p.getPrezzoFornitore() != suppPrice) {
                    p.setPrezzoFornitore(suppPrice);
                }
                this.addItemsToOrder(orderJson, item, p, v, quantity, false,
                        discount);
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (PortalException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (ParseException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        return new Response(Response.Code.OK, orderJson.toString());
    }
    
    /**
     * Adds a platform to the order.
     * 
     * @param resourceRequest
     *            the resource request containing items.
     * @return {@link Response} containing the updated order.
     */
    private Response addPlatform(final ResourceRequest resourceRequest) {

        String customerCode = ParamUtil.getString(resourceRequest,
                Ordini.CUSTOMER_CODE);
        JSONObject orderJson = null;
        try {
            orderJson = JSONFactoryUtil.createJSONObject(ParamUtil.getString(
                    resourceRequest, Ordini.CURRENT_ORDER));

            this.logger.debug("orderJSON: " + orderJson.toString());

            if (orderJson.length() == 0) {
                this.logger.debug("Empty order  ...");
                String orderDateStr = ParamUtil.getString(resourceRequest,
                        "orderDate");
                String shipDateStr = ParamUtil.getString(resourceRequest,
                        "shipDate");
                orderJson = this.createNewOrder(customerCode, orderDateStr,
                        shipDateStr, false);
                this.logger.debug("Creating new order tmp id: " + orderJson);
            } else {
                this.logger.debug("NOT empty order...");
            }

            long idPlant = ParamUtil.getLong(resourceRequest, Ordini.ID_PLANT);
            if (idPlant > 0) {
                Piante p = PianteLocalServiceUtil.getPiante(idPlant);
                String codeVariant = ParamUtil.getString(resourceRequest,
                        Ordini.CODE_VARIANT, null);
                Articoli item = ArticoliLocalServiceUtil.getArticoli(p.
                        getCodice());
                double price1 = ParamUtil.getDouble(resourceRequest, "price1",
                        0.0);
                double price2 = ParamUtil.getDouble(resourceRequest, "price2",
                        0.0);
                double discount = ParamUtil.getDouble(resourceRequest,
                        "discount", 0.0);
                if (item.getPrezzo1() != price1 
                        || item.getPrezzo2() != price2) {
                    item.setPrezzo1(price1);
                    item.setPrezzo2(price2);
                }
                DescrizioniVarianti v = null;
                if (Validator.isNotNull(codeVariant)
                        && !Validator.isBlank(codeVariant)) {
                    v = DescrizioniVariantiLocalServiceUtil.
                            getDescrizioniVarianti(new DescrizioniVariantiPK(
                                    item.getCodiceArticolo(), codeVariant));
                }

                int quantity = ParamUtil.getInteger(resourceRequest,
                        Ordini.QUANTITY);
                double suppPrice = ParamUtil.getDouble(resourceRequest,
                        Ordini.SUPPLIER_PRICE, 0);
                if (p.getPrezzoFornitore() != suppPrice) {
                    p.setPrezzoFornitore(suppPrice);
                }
                for (int i = 1; i <= quantity; i++) {
                    this.addPlatformToOrder(orderJson, item, p, v, false,
                            false, discount, false);
                }
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (PortalException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (ParseException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        return new Response(Response.Code.OK, orderJson.toString());
    }

    /**
     * Adds items over in the specified cart row.
     * 
     * @param resourceRequest
     *            {@link ResourceRequest}
     * @return the new order
     */
    private Response updateItemOver(final ResourceRequest resourceRequest) {
        ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest
                .getAttribute(WebKeys.THEME_DISPLAY);
        long cartId = ParamUtil.getLong(resourceRequest, Constants.CART_ID);
        long rowId = ParamUtil.getLong(resourceRequest, "rowId");
        String[] itemsOver = ParamUtil.getParameterValues(resourceRequest,
                "itemOver"); 
        Response response = null;
        JSONObject orderJson = null;
        try {
            orderJson = JSONFactoryUtil.createJSONObject(ParamUtil.getString(
                    resourceRequest, Ordini.CURRENT_ORDER));
            if (ParamUtil.getBoolean(resourceRequest, "add", true)) {
                this.logger.debug("Adding items over to row: " + rowId
                        + " in cart: " + cartId);
                orderJson = addItemsOverToRow(orderJson, cartId, rowId,
                        itemsOver);
            } else {
                this.logger.debug("Removing items over to row: " + rowId
                        + " in cart: " + cartId);
                orderJson = removeItemsOverToRow(orderJson, cartId, rowId,
                        itemsOver);
            }
            if (orderJson.length() == 0) {
                this.logger.warn("Empty order!!!");
            }
            response = new Response(Code.OK, orderJson.toString());
        } catch (JSONException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            response = new Response(Response.Code.GENERIC_ERROR,
                    e.getMessage());
        } catch (PortalException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            response = new Response(Response.Code.GENERIC_ERROR,
                    e.getMessage());
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            response = new Response(Response.Code.GENERIC_ERROR,
                    e.getMessage());
        } catch (Exception e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            if (e.getMessage().contains("generic")) {
                response = new Response(Response.Code.NOT_PERMITTED,
                        LanguageUtil.format(getPortletConfig(),
                                themeDisplay.getLocale(), "exceed-height-x",
                                e.getMessage()));
            } else {
                response = new Response(Response.Code.EXCEED_HEIGHT_ERROR,
                        LanguageUtil.format(getPortletConfig(),
                                themeDisplay.getLocale(), "exceed-height-x",
                                e.getMessage()));
            }
            
        }
        
        return response;
    }
    
    /**
     * Removes a platform from the current order.
     * 
     * @param resourceRequest
     *            {@link ResourceRequest}
     * @return response
     */
    private Response removePlatform(final ResourceRequest resourceRequest) {
        Response response = null;
        JSONObject orderJson = null;
        try {
            orderJson = JSONFactoryUtil.createJSONObject(ParamUtil.getString(
                    resourceRequest, Ordini.CURRENT_ORDER));

            long cartId = ParamUtil.getLong(resourceRequest, Constants.CART_ID);
            long rowId = ParamUtil.getLong(resourceRequest, "rowId");
            if (orderJson.has(Constants.CARTS)) {
                this.logger.info("Deleting row: " + rowId + " in cart: "
                        + cartId);
                orderJson = deleteOrderRow(orderJson, cartId, rowId);
            }
            if (orderJson.length() == 0) {
                this.logger.warn("Empty order!!!");
            }

            response = new Response(Response.Code.OK, orderJson.toString());
        } catch (JSONException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            response = new Response(Response.Code.GENERIC_ERROR,
                    e.getMessage());
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            response = new Response(Response.Code.GENERIC_ERROR,
                    e.getMessage());
        }
        return response;
    }

    private JSONObject addItemsOverToRow(JSONObject orderJson, long cartId,
            long rowId, String[] itemsOver) throws Exception {
        JSONArray cartsJson = orderJson.getJSONArray(Constants.CARTS);
        JSONObject newOrderJson = JSONFactoryUtil.createJSONObject();
        double price = 0.0;
        double oldRowPrice = 0.0;
        for (int i = 0; i < cartsJson.length(); i++) {
            JSONObject cartJson = cartsJson.getJSONObject(i);
            if (!newOrderJson.has(Constants.CARTS)) {
                newOrderJson.put(Constants.CARTS,
                        JSONFactoryUtil.createJSONArray());
            }
            if (cartJson.getInt(Constants.ID) != cartId) {
                newOrderJson.getJSONArray(Constants.CARTS).put(cartJson);
            } else {
                if (cartJson.getBoolean(Constants.GENERIC)) {
                    throw new Exception("is-generic-cart");
                }
                JSONObject newCartJson = JSONFactoryUtil.createJSONObject(
                        cartJson.toString());
                Piante p = null;
                for (int j = 0; j < newCartJson.getJSONArray(Constants.ROWS).
                        length(); j++) {
                    JSONObject row = newCartJson.getJSONArray(Constants.ROWS).
                            getJSONObject(j);
//                    if (!newCartJson.has(Constants.ROWS)) {
//                        newCartJson.put(Constants.ROWS,
//                                JSONFactoryUtil.createJSONArray());
//                    }
                    if (row.getInt(Constants.ID) == rowId) {
//                        newCartJson.getJSONArray(Constants.ROWS).put(row);
//                    } else {
                        p = PianteLocalServiceUtil.getPiante(row.getLong(
                                Constants.ITEM_ID));
                        int oldRowItem = 0;
                        oldRowPrice = 0.0;
                        int itemCount = 0;
                        int itemOverHeight = p.getAltezza();
                        for (int k = itemsOver.length - 1; k >= 0; k--) {
                            Ordini.ItemOverCode c = Ordini.ItemOverCode.
                                    valueOf(itemsOver[k]);
                            switch (c) {
                            case s2:
                                if (!row.getBoolean("sormontate")) {
                                    row.put("sormontate", true);
                                    itemCount = p.getPiantePianale();
                                    
                                } else {
                                    itemCount = row.getInt(
                                            Constants.PLATFORM_ITEMS);
                                }
                                oldRowItem = itemCount;
                                oldRowPrice = row.getDouble("prezzo")
                                        * oldRowItem; 
                                if (p.getSormonto2Fila() > 0
                                        && row.getInt(c.name(), 0) == 0) {
                                    itemCount += p.getSormonto2Fila();
                                    row.put(c.name(), p.getSormonto2Fila());
                                    itemOverHeight += p.getAggiuntaRipiano();
                                }
                                break;
                            case s3:
                                if (p.getSormonto3Fila() > 0
                                        && row.getInt(c.name(), 0) == 0) {
                                    itemCount += p.getSormonto3Fila();
                                    row.put(c.name(), p.getSormonto3Fila());
                                    itemOverHeight += p.getAggiuntaRipiano();
                                }
                                break;
                            case s4:
                                if (p.getSormonto4Fila() > 0
                                        && row.getInt(c.name(), 0) == 0) {
                                    itemCount += p.getSormonto4Fila();
                                    row.put(c.name(), p.getSormonto4Fila());
                                    itemOverHeight += p.getAggiuntaRipiano();
                                }
                                break;
                            default:
                                this.logger.warn("Unexpected ItemOverCode");
                                break;
                            }
                        }
                        if (newCartJson.getInt(Constants.USED_HEIGHT)
                                + itemOverHeight - p.getAltezza() <= height) {
                            row.put(Constants.PLATFORM_ITEMS, itemCount);
                            price = row.getDouble("prezzo") * itemCount;
                            row.put(Constants.AMOUNT, price);
                            row.put("modificato", true);
                            newCartJson.put(Constants.PLANTS,
                                    newCartJson.getInt(Constants.PLANTS)
                                            + itemCount - oldRowItem);
                            newCartJson.put(Constants.USED_HEIGHT,
                                    newCartJson.getInt(Constants.USED_HEIGHT)
                                            + itemOverHeight - p.getAltezza());
                            newCartJson.put(Constants.AMOUNT,
                                    newCartJson.getDouble(Constants.AMOUNT)
                                            + price - oldRowPrice);
                        } else {
                            throw new Exception(newCartJson.getInt(
                                    Constants.USED_HEIGHT) + itemOverHeight
                                    - p.getAltezza() - height + " cm");
                        }
                    }
                }
                newOrderJson.getJSONArray(Constants.CARTS).put(newCartJson);
            }
        }
        newOrderJson.put(Constants.ROWS_NUMBER,
                orderJson.getInt(Constants.ROWS_NUMBER));
        newOrderJson.put(Constants.DELIVERY_DATE,
                orderJson.getLong(Constants.DELIVERY_DATE));
        newOrderJson.put(Constants.CREATION_DATE,
                orderJson.getLong(Constants.CREATION_DATE));
        newOrderJson.put(Constants.ID, orderJson.getLong(Constants.ID));
        newOrderJson.put(Constants.AGENT_ID,
                orderJson.getLong(Constants.AGENT_ID));
        newOrderJson.put(Constants.CUSTOMER_ID,
                orderJson.getLong(Constants.CUSTOMER_ID));
        newOrderJson.put(Constants.CART_TYPE_ID,
                orderJson.getLong(Constants.CART_TYPE_ID));
        newOrderJson.put(Constants.CARRIER_ID,
                orderJson.getLong(Constants.CARRIER_ID));
        newOrderJson.put(Constants.AMOUNT,
                orderJson.getDouble(Constants.AMOUNT) + price - oldRowPrice);
        newOrderJson.put(Constants.LOADING_PLACE,
                orderJson.getString(Constants.LOADING_PLACE));
        newOrderJson.put(Constants.NOTE, orderJson.getString(Constants.NOTE));
        newOrderJson.put(Constants.CUSTOMER_NOTE,
                orderJson.getString(Constants.CUSTOMER_NOTE));
        newOrderJson.put(Constants.ROWS_NUMBER,
                orderJson.getInt(Constants.ROWS_NUMBER));
        newOrderJson.put(Constants.STATE, orderJson.getString(Constants.STATE));
        return newOrderJson;
    }
    
    private JSONObject removeItemsOverToRow(JSONObject orderJson, long cartId,
            long rowId, String[] itemsOver) throws Exception {
        JSONArray cartsJson = orderJson.getJSONArray(Constants.CARTS);
        JSONObject newOrderJson = JSONFactoryUtil.createJSONObject();
        double price = 0.0;
        for (int i = 0; i < cartsJson.length(); i++) {
            JSONObject cartJson = cartsJson.getJSONObject(i);
            if (!newOrderJson.has(Constants.CARTS)) {
                newOrderJson.put(Constants.CARTS,
                        JSONFactoryUtil.createJSONArray());
            }
            if (cartJson.getInt(Constants.ID) != cartId) {
                newOrderJson.getJSONArray(Constants.CARTS).put(cartJson);
            } else {
                JSONObject newCartJson = JSONFactoryUtil.createJSONObject(
                        cartJson.toString());
                Piante p = null;
                for (int j = 0; j < newCartJson.getJSONArray(Constants.ROWS).
                        length(); j++) {
                    JSONObject row = newCartJson.getJSONArray(Constants.ROWS).
                            getJSONObject(j);
                    if (row.getInt(Constants.ID) == rowId) {
                        p = PianteLocalServiceUtil.getPiante(row.getLong(
                                Constants.ITEM_ID));
                        int oldRowItem = 0;
                        double oldRowPrice = 0.0;
                        int itemCount = 0;
                        int itemOverHeight = p.getAltezza();
                        for (int k = itemsOver.length - 1; k >= 0; k--) {
                            Ordini.ItemOverCode c = Ordini.ItemOverCode.
                                    valueOf(itemsOver[k]);
                            switch (c) {
                            case s2:
                                row.put("sormontate", false);
                                oldRowItem = itemCount;
                                oldRowPrice = row.getDouble("prezzo")
                                        * oldRowItem; 
                                if (p.getSormonto2Fila() > 0
                                        && row.getInt(c.name(), 0) > 0) {
                                    itemCount -= p.getSormonto2Fila();
                                    row.put(c.name(), 0);
                                    itemOverHeight -= p.getAggiuntaRipiano();
                                }
                                break;
                            case s3:
                                if (p.getSormonto3Fila() > 0
                                        && row.getInt(c.name(), 0) > 0) {
                                    itemCount -= p.getSormonto3Fila();
                                    row.put(c.name(), 0);
                                    itemOverHeight -= p.getAggiuntaRipiano();
                                }
                                break;
                            case s4:
                                if (p.getSormonto4Fila() > 0
                                        && row.getInt(c.name(), 0) > 0) {
                                    itemCount -= p.getSormonto4Fila();
                                    row.put(c.name(), 0);
                                    itemOverHeight -= p.getAggiuntaRipiano();
                                }
                                break;
                            default:
                                this.logger.warn("Unexpected ItemOverCode");
                                break;
                            }
                        }
                        row.put(Constants.PLATFORM_ITEMS,
                                row.getInt(Constants.PLATFORM_ITEMS)
                                + itemCount);
                        price = row.getDouble("prezzo") * itemCount;
                        row.put(Constants.AMOUNT, row.getDouble(
                                Constants.AMOUNT) + price);
                        row.put("modificato", true);
                        newCartJson.put(Constants.PLANTS,
                                newCartJson.getInt(Constants.PLANTS)
                                        + itemCount - oldRowItem);
                        newCartJson.put(Constants.USED_HEIGHT,
                                newCartJson.getInt(Constants.USED_HEIGHT)
                                        + itemOverHeight - p.getAltezza());
                        newCartJson.put(Constants.AMOUNT,
                                newCartJson.getDouble(Constants.AMOUNT)
                                        + price - oldRowPrice);
                    }
                }
                newOrderJson.getJSONArray(Constants.CARTS).put(newCartJson);
            }
        }
        newOrderJson.put(Constants.ROWS_NUMBER,
                orderJson.getInt(Constants.ROWS_NUMBER));
        newOrderJson.put(Constants.DELIVERY_DATE,
                orderJson.getLong(Constants.DELIVERY_DATE));
        newOrderJson.put(Constants.CREATION_DATE,
                orderJson.getLong(Constants.CREATION_DATE));
        newOrderJson.put(Constants.ID, orderJson.getLong(Constants.ID));
        newOrderJson.put(Constants.AGENT_ID,
                orderJson.getLong(Constants.AGENT_ID));
        newOrderJson.put(Constants.CUSTOMER_ID,
                orderJson.getLong(Constants.CUSTOMER_ID));
        newOrderJson.put(Constants.CART_TYPE_ID,
                orderJson.getLong(Constants.CART_TYPE_ID));
        newOrderJson.put(Constants.CARRIER_ID,
                orderJson.getLong(Constants.CARRIER_ID));
        newOrderJson.put(Constants.AMOUNT,
                orderJson.getDouble(Constants.AMOUNT) + price);
        newOrderJson.put(Constants.LOADING_PLACE,
                orderJson.getString(Constants.LOADING_PLACE));
        newOrderJson.put(Constants.NOTE, orderJson.getString(Constants.NOTE));
        newOrderJson.put(Constants.CUSTOMER_NOTE,
                orderJson.getString(Constants.CUSTOMER_NOTE));
        newOrderJson.put(Constants.ROWS_NUMBER,
                orderJson.getInt(Constants.ROWS_NUMBER));
        newOrderJson.put(Constants.STATE, orderJson.getString(Constants.STATE));
        return newOrderJson;
    }
    
    /**
     * Deletes the row form the order.
     * 
     * @param orderJson
     *            current order
     * @param cartId
     *            cart id
     * @param rowId
     *            row id
     * @return the new order
     * @throws SystemException
     *             exception
     */
    private JSONObject deleteOrderRow(final JSONObject orderJson,
            final long cartId, final long rowId) throws SystemException {
        double price = 0.0;
        int plants = 0;
        JSONArray cartsJson = orderJson.getJSONArray(Constants.CARTS);
        JSONObject newOrderJson = JSONFactoryUtil.createJSONObject();

        for (int i = 0; i < cartsJson.length(); i++) {
            JSONObject cartJson = cartsJson.getJSONObject(i);
            if (!newOrderJson.has(Constants.CARTS)) {
                newOrderJson.put(Constants.CARTS,
                        JSONFactoryUtil.createJSONArray());
            }
            if (cartJson.getInt(Constants.ID) != cartId) {
                newOrderJson.getJSONArray(Constants.CARTS).put(cartJson);
            } else {
                JSONObject newCartJson = JSONFactoryUtil.createJSONObject();
                Piante p = null;
                for (int j = 0; j < cartJson.getJSONArray(Constants.ROWS).
                        length(); j++) {
                    JSONObject row = cartJson.getJSONArray(Constants.ROWS).
                            getJSONObject(j);
                    if (row.getInt(Constants.ID) != rowId) {
                        if (!newCartJson.has(Constants.ROWS)) {
                            newCartJson.put(Constants.ROWS,
                                    JSONFactoryUtil.createJSONArray());
                        }
                        newCartJson.getJSONArray(Constants.ROWS).put(row);
                    } else if (p == null) {
                        p = PianteLocalServiceUtil.fetchPiante(row.
                                getInt("idArticolo"));
                        price = row.getDouble(Constants.AMOUNT, 0);
                        plants = row.getInt(Constants.PLATFORM_ITEMS, 0);
                    }
                }
                if (newCartJson.length() > 0 && p != null) {
                    if (!cartJson.getBoolean(Constants.GENERIC)) {
                        newCartJson.put(
                                Constants.USED_HEIGHT,
                                cartJson.getInt(Constants.USED_HEIGHT)
                                        - p.getAltezza());
                    } else {
                        newCartJson.put(Constants.USED_HEIGHT, -1);
                    }
                    newCartJson.put(Constants.AMOUNT,
                            cartJson.getDouble(Constants.AMOUNT) - price);
                    newCartJson.put(Constants.PLANTS,
                            cartJson.getInt(Constants.PLANTS) - plants);
                    newCartJson.put(Constants.ID,
                            cartJson.getLong(Constants.ID));
                    newCartJson.put(Constants.ORDER_ID,
                            cartJson.getLong(Constants.ORDER_ID));
                    newCartJson.put(Constants.NUMBER,
                            cartJson.getInt(Constants.NUMBER));
                    newCartJson.put(Constants.GENERIC,
                            cartJson.getBoolean(Constants.GENERIC));
                    newCartJson.put(Constants.CLOSED,
                            cartJson.getBoolean(Constants.CLOSED));
                    newOrderJson.getJSONArray(Constants.CARTS).put(newCartJson);
                }
            }

        }
        newOrderJson.put(Constants.ROWS_NUMBER,
                orderJson.getInt(Constants.ROWS_NUMBER));
        newOrderJson.put(Constants.DELIVERY_DATE,
                orderJson.getLong(Constants.DELIVERY_DATE));
        newOrderJson.put(Constants.CREATION_DATE,
                orderJson.getLong(Constants.CREATION_DATE));
        newOrderJson.put(Constants.ID, orderJson.getLong(Constants.ID));
        newOrderJson.put(Constants.AGENT_ID,
                orderJson.getLong(Constants.AGENT_ID));
        newOrderJson.put(Constants.CUSTOMER_ID,
                orderJson.getLong(Constants.CUSTOMER_ID));
        newOrderJson.put(Constants.CART_TYPE_ID,
                orderJson.getLong(Constants.CART_TYPE_ID));
        newOrderJson.put(Constants.CARRIER_ID,
                orderJson.getLong(Constants.CARRIER_ID));
        newOrderJson.put(Constants.AMOUNT,
                orderJson.getDouble(Constants.AMOUNT) - price);
        newOrderJson.put(Constants.LOADING_PLACE,
                orderJson.getString(Constants.LOADING_PLACE));
        newOrderJson.put(Constants.NOTE, orderJson.getString(Constants.NOTE));
        newOrderJson.put(Constants.CUSTOMER_NOTE,
                orderJson.getString(Constants.CUSTOMER_NOTE));
        newOrderJson.put(Constants.ROWS_NUMBER,
                orderJson.getInt(Constants.ROWS_NUMBER));
        newOrderJson.put(Constants.STATE, orderJson.getString(Constants.STATE));
        return newOrderJson;
    }

    /**
     * Actually adds a cart of selected item to the order.
     * 
     * @param orderJson
     *            current order
     * @param item
     *            adding item
     * @param p
     *            item details
     * @param v
     *            variant selected
     * @param discount item discount
     * @throws SystemException
     *             exception
     * @throws JSONException
     *             exception
     */
    private void addCartToOrder(final JSONObject orderJson,
            final Articoli item, final Piante p, final DescrizioniVarianti v,
            final double discount) throws JSONException, SystemException {

//        int rows = this.height / (p.getAltezza() + p.getAggiuntaRipiano());
    	int rows = 0;
        int rowsNew = 0;
        int currHeight = 0;
        while (p.getAltezza() <= this.height - currHeight) {
        	if (currHeight + p.getAltezza() <= this.height) {
				if (p.getSormonto2Fila() > 0  &&
						currHeight + p.getAltezza() + p.getAggiuntaRipiano() < this.height) {
					if (p.getSormonto3Fila() > 0  &&
							currHeight + p.getAltezza()
							+ (p.getAggiuntaRipiano() * 2) <= this.height) {
						if (p.getSormonto4Fila() > 0  &&
								currHeight + p.getAltezza()
								+ (p.getAggiuntaRipiano() * 3) <= this.height) {
							currHeight += p.getAltezza()
									+ (p.getAggiuntaRipiano() * 3);
						} else {
							currHeight += p.getAltezza()
									+ (p.getAggiuntaRipiano() * 2);
						}
					} else {
						currHeight += p.getAltezza() + p.getAggiuntaRipiano();
					}
				} else {
					currHeight += p.getAltezza();
				}
			}
        	rows++;
        }
        currHeight = 0;
        while (p.getAltezza() <= this.height - currHeight) {
        	this.logger.debug("currHeight:  " + currHeight);
        	if (currHeight + p.getAltezza() <= this.height) {
				if (p.getSormonto2Fila() > 0  &&
						currHeight + p.getAltezza() + p.getAggiuntaRipiano() < this.height) {
					this.logger.debug("ENTRA IL SORMONTO DI 2 FILA");
					if (p.getSormonto3Fila() > 0  &&
							currHeight + p.getAltezza()
							+ (p.getAggiuntaRipiano() * 2) <= this.height) {
						this.logger.debug("ENTRA IL SORMONTO DI 3 FILA");
						if (p.getSormonto4Fila() > 0  &&
								currHeight + p.getAltezza()
								+ (p.getAggiuntaRipiano() * 3) <= this.height) {
							this.logger.debug("ENTRA IL SORMONTO DI 4 FILA");
							currHeight += p.getAltezza()
									+ (p.getAggiuntaRipiano() * 3);
							this.logger.debug("currHeight:  " + currHeight);
							this.addPlatformToOrder(orderJson, item, p, v,
									true, rowsNew == rows - 1, discount, true);
						} else {
							this.logger.debug("NON C'E' O NON ENTRA IL SORMONTO"
									+ " DI 4 FILA");
							currHeight += p.getAltezza()
									+ (p.getAggiuntaRipiano() * 2);
							this.logger.debug("currHeight:  " + currHeight);
							this.addPlatformToOrder(orderJson, item, p, v,
									true, rowsNew == rows - 1, discount, true);
						}
					} else {
						this.logger.debug("NON C'E' O NON ENTRA IL SORMONTO DI 3"
								+ " FILA");
						currHeight += p.getAltezza() + p.getAggiuntaRipiano();
						this.logger.debug("currHeight:  " + currHeight);
						this.addPlatformToOrder(orderJson, item, p, v,
								true, rowsNew == rows - 1, discount, true);
					}
				} else {
					this.logger.debug("NON C'E' O NON ENTRA IL SORMONTO DI 2"
							+ " FILA");
					currHeight += p.getAltezza();
					this.logger.debug("currHeight:  " + currHeight);
					this.addPlatformToOrder(orderJson, item, p, v, false,
							rowsNew == rows - 1, discount, true);
				}
			}
        	rowsNew++;
        	this.logger.debug("ROW NEW:  " + rowsNew);
        }
        
//        int i;
//        for (i = 1; i <= rowsNew; i++) {
//            this.addPlatformToOrder(orderJson, item, p, v,
//                    p.getSormonto2Fila() > 0, i == rowsNew, discount, true);
//        }
        orderJson.getJSONArray(Constants.CARTS).getJSONObject(
                orderJson.getJSONArray(Constants.CARTS).length() - 1).put(
                        Constants.USED_HEIGHT, currHeight);
        orderJson.getJSONArray(Constants.CARTS).getJSONObject(
                orderJson.getJSONArray(Constants.CARTS).length() - 1).put(
                        Constants.CLOSED, true);
    }

    /**
     * Actually adds the specified item's quantity items to the order.
     * 
     * @param orderJson
     *            current order
     * @param item
     *            adding item
     * @param p
     *            item details
     * @param v
     *            variant selected
     * @param quantity
     *            items quantity to add
     * @param itemOver
     *            true if item will be placed over other in the cart, false
     *            otherwise
     * @param discount item discount
     * @throws SystemException
     *             exception
     * @throws JSONException
     *             exception
     */
    private void addItemsToOrder(final JSONObject orderJson,
            final Articoli item, final Piante p, final DescrizioniVarianti v,
            final int quantity, final boolean itemOver, final double discount)
                    throws JSONException, SystemException {
        double tmpOrderPrice = 0.0;
        boolean added = false;
        JSONObject cartJson = null;
        int rows = orderJson.getInt(Constants.ROWS_NUMBER, 0);
        for (int i = 0; i < orderJson.getJSONArray(Constants.CARTS).length(); i++) {
            cartJson = orderJson.getJSONArray(Constants.CARTS).getJSONObject(i);
            
            if (!added) {
	            int usedHeight = cartJson.getInt(Constants.USED_HEIGHT);
	            boolean closed = cartJson.getBoolean(Constants.CLOSED);
	            if (!closed && usedHeight == -1) {
	                this.logger.debug(Ordini.CART + cartJson.getInt(Constants.ID)
	                        + ", used height: " + (this.height - usedHeight));
	                if (!cartJson.has(Constants.ROWS)) {
	                    orderJson.getJSONArray(Constants.CARTS).getJSONObject(i).
	                    put(Constants.ROWS, JSONFactoryUtil.createJSONArray());
	                }
	                this.addNewGenericRow(orderJson, item, p, v, cartJson, rows,
	                        quantity, itemOver, discount);
	                added = true;
	            } else {
	                this.logger.debug(Ordini.CART + cartJson.getLong(Constants.ID)
	                        + " full.");
	                added = false;
	            }
            }
            tmpOrderPrice += cartJson.getDouble(Constants.AMOUNT);
        }
        if (!added) {
            this.logger.debug("All generic carts are full, or there aren't.");
            int cartsLength = orderJson.getJSONArray(Constants.CARTS).length();
            if (cartsLength == 0) {
                cartJson = this.createNewCart(orderJson.getLong(
                        Ordini.ID_ORDER), cartsLength + 1, true);
            } else {
                int newCartId = orderJson
                        .getJSONArray(Constants.CARTS)
                        .getJSONObject(
                                orderJson.getJSONArray(Constants.CARTS).
                                        length() - 1).getInt(Constants.ID);
                cartJson = this.createNewCart(cartJson.getLong(Ordini.ID_ORDER),
                        newCartId + 1, true);
            }
            cartsLength++;
            this.logger.debug("New generic cart: " + cartJson);
            cartJson.put(Constants.ROWS, JSONFactoryUtil.createJSONArray());

            orderJson.put(Constants.ROWS_NUMBER, rows + 1);

            this.addNewGenericRow(orderJson, item, p, v, cartJson, rows,
                    quantity, itemOver, discount);
            orderJson.getJSONArray(Constants.CARTS).put(cartJson);
            tmpOrderPrice += cartJson.getDouble(Constants.AMOUNT);
        }

        orderJson.put(Constants.AMOUNT, tmpOrderPrice);
        
    }
    
    /**
     * Actually adds a platform of selected items to the order.
     * 
     * @param orderJson
     *            current order
     * @param item
     *            adding item
     * @param p
     *            item details
     * @param v
     *            variant selected
     * @param itemOver
     *            true if item will be placed over other in the cart, false
     *            otherwise
     * @param isLastInCart
     *            true if is the the last row in the cart, false otherwise 
     * @param discount item discount
     * @param isAddingIncart 
     * @throws SystemException
     *             exception
     * @throws JSONException
     *             exception
     */
    private void addPlatformToOrder(final JSONObject orderJson,
            final Articoli item, final Piante p, final DescrizioniVarianti v,
            final boolean itemOver, final boolean isLastInCart,
            final double discount, boolean isAddingIncart) throws SystemException, JSONException {
        double tmpOrderPrice = 0.0;
        boolean added = false;
        boolean dontAdd = false;
        JSONObject cartJson = null;
        int rows = orderJson.getInt(Constants.ROWS_NUMBER, 0);
        for (int i = 0; i < orderJson.getJSONArray(Constants.CARTS).length();
        		i++) {
            cartJson = orderJson.getJSONArray(Constants.CARTS).getJSONObject(i);
            if (!added) {
				int usedHeight = cartJson.getInt(Constants.USED_HEIGHT);
	            boolean closed = cartJson.getBoolean(Constants.CLOSED);
	            boolean generic = cartJson.getBoolean(Constants.GENERIC);
	            if (!generic && !closed
	                    && (this.height - usedHeight) >= p.getAltezza()) {
	                int plants = cartJson.getInt(Constants.PLANTS, 0);
	                if (isAddingIncart) {
	                    if (plants < p.getPianteCarrello()) {
	                        this.logger.debug(Ordini.CART + cartJson.getInt(Constants.ID)
	                                + ", used height: " + (this.height - usedHeight));
	                        if (!cartJson.has(Constants.ROWS)) {
	                            orderJson.getJSONArray(Constants.CARTS).getJSONObject(i).
	                            put(Constants.ROWS, JSONFactoryUtil.createJSONArray());
	                        }
	                        this.addNewRow(orderJson, item, p, v, cartJson, rows,
	                                usedHeight, itemOver, isLastInCart, discount);
	                        added = true;
	                    } else {
	                        dontAdd = true;
	                    }
	                } else {
	                    this.logger.debug(Ordini.CART + cartJson.getInt(Constants.ID)
	                            + ", used height: " + (this.height - usedHeight));
	                    if (!cartJson.has(Constants.ROWS)) {
	                        orderJson.getJSONArray(Constants.CARTS).getJSONObject(i).
	                        put(Constants.ROWS, JSONFactoryUtil.createJSONArray());
	                    }
	                    this.addNewRow(orderJson, item, p, v, cartJson, rows,
	                            usedHeight, itemOver, isLastInCart, discount);
	                    added = true;
	                }
	            } else {
	                this.logger.debug(Ordini.CART + cartJson.getLong(Constants.ID)
	                        + " full.");
	                added = false;
	            }
            }
            tmpOrderPrice += cartJson.getDouble(Constants.AMOUNT);
        }
        if (!added && !dontAdd) {
            this.logger.debug("All carts are full, creating new cart.");
            int cartsLength = orderJson.getJSONArray(Constants.CARTS).length();
            this.logger.debug(cartsLength);
            if (cartsLength == 0) {
                cartJson = this.createNewCart(orderJson.getLong(
                        Ordini.ID_ORDER), cartsLength + 1, false);
            } else {
                int newCartId = orderJson
                        .getJSONArray(Constants.CARTS)
                        .getJSONObject(
                                orderJson.getJSONArray(Constants.CARTS).
                                        length() - 1).getInt(Constants.ID);
                cartJson = this.createNewCart(cartJson.getLong(Ordini.ID_ORDER),
                        newCartId + 1, false);
            }
            cartsLength++;
            this.logger.debug("New cart: " + cartJson);
            cartJson.put(Constants.ROWS, JSONFactoryUtil.createJSONArray());

            orderJson.put(Constants.ROWS_NUMBER, rows + 1);

            this.addNewRow(orderJson, item, p, v, cartJson, rows, 0, itemOver,
                    isLastInCart, discount);
            orderJson.getJSONArray(Constants.CARTS).put(cartJson);
            tmpOrderPrice += cartJson.getDouble(Constants.AMOUNT);
        }

        orderJson.put(Constants.AMOUNT, tmpOrderPrice);

    }

    /**
     * Creates a new empty cart belonging current order.
     * 
     * @param orderId
     *            the current orderId
     * @param number
     *            cart number in the current order
     * @param generic
     *            true for a generic cart false otherwise
     * @return the new cart
     * @throws SystemException
     *             exception
     * @throws JSONException
     *             exception
     */
    private JSONObject createNewCart(final long orderId, final int number,
            final boolean generic) throws SystemException, JSONException {
        Carrello cart = CarrelloLocalServiceUtil.createCarrello(number);
        cart.setIdOrdine(orderId);
        cart.setProgressivo(number);
        cart.setImporto(0);
        cart.setGenerico(generic);
        return JSONFactoryUtil.createJSONObject(JSONFactoryUtil.
                looseSerialize(cart));
    }

    /**
     * Creates a new order.
     * 
     * @param customerCode
     *            customer code
     * @param orderDate
     *            order date
     * @param shipDate
     *            ship date
     * @param firstCartIsGeneric
     *            true if first cart is a genric cart false otherwise
     * @return the new order
     * @throws SystemException
     *             exception
     * @throws JSONException
     *             exception
     * @throws ParseException
     *             exception
     */
    private JSONObject createNewOrder(final String customerCode,
            final String orderDate, final String shipDate,
            final boolean firstCartIsGeneric) throws SystemException,
            JSONException, ParseException {

        Ordine order = OrdineLocalServiceUtil.createOrdine(-1);
        order.setDataInserimento(this.formatter.parse(orderDate));
        order.setIdCliente(customerCode);
        order.setImporto(0);
        order.setNote("");
        order.setDataConsegna(this.formatter.parse(shipDate));
        order.setIdTrasportatore("");
        order.setIdAgente(0);
        order.setIdTipoCarrello(0);
        order.setLuogoCarico("");
        order.setNoteCliente("");

        JSONObject orderJson = JSONFactoryUtil.createJSONObject(JSONFactoryUtil.
                looseSerialize(order));
        JSONArray cartsJson = JSONFactoryUtil.createJSONArray();
        JSONObject cartJson = this.createNewCart(
                orderJson.getLong(Constants.ID), 1, firstCartIsGeneric);
        if (firstCartIsGeneric) {
            cartJson.put(Constants.USED_HEIGHT, -1);
        }
        cartsJson.put(cartJson);

        orderJson.put(Constants.CARTS, cartsJson);

        return orderJson;
    }

    /**
     * Creates a new row in the current cart.
     * 
     * @param cartId
     *            current cart id
     * @param rowId
     *            row id
     * @param rowNumber
     *            row number in cart
     * @param item
     *            adding item price
     * @param p
     *            adding item details
     * @param v
     *            variant selected
     * @param itemOver
     *            true if item will be placed over other in the cart, false
     *            otherwise
     * @param discount item discount
     * @param isLastInCart true if is last row in cart false otherwise
     * @param 
     *            true if item will be placed over other in the cart, false
     *            otherwise
     * @return the new row
     * @throws SystemException
     *             exception
     * @throws JSONException
     *             exception
     */
    private JSONObject createNewRow(final long cartId, final int rowId,
            final int rowNumber, final Articoli item, final Piante p,
            final DescrizioniVarianti v, final boolean itemOver,
            final boolean isLastInCart, final double discount)
                    throws SystemException, JSONException {
        Riga row = RigaLocalServiceUtil.createRiga(rowId);
        row.setIdCarrello(cartId);
        row.setProgressivo(rowNumber);
        row.setIdArticolo(p.getId());
        double price = item.getPrezzo1() * (1 - discount / Constants.HUNDRED);
        double variantPrice = 0.0;
        String note = "";
        String variantCode = "";
        if (Validator.isNotNull(v)) {
            price += item.getPrezzo2() - item.getPrezzo1(); //item.getPrezzo2();
            variantPrice = item.getPrezzo2() - item.getPrezzo1();
            note = v.getDescrizione();
            variantCode = v.getCodiceVariante();
        }
        row.setPrezzo(price);

        row.setImportoVarianti(variantPrice);
        if (itemOver) {
            if (!isLastInCart) {
                row.setPianteRipiano(p.getPiantePianale() + p.getSormonto2Fila()
                        + p.getSormonto3Fila() + p.getSormonto4Fila());
            
            } else {
                row.setPianteRipiano(p.getPiantePianale());
            }
            row.setImporto(price * row.getPianteRipiano());
        } else {
            row.setPianteRipiano(p.getPiantePianale());
            row.setImporto(price * p.getPiantePianale());
        }
        row.setSormontate(itemOver);
        row.setStringa(note);
        row.setCodiceVariante(variantCode);
        row.setOpzioni("");
        row.setSconto(discount);
        row.setPrezzoFornitore(p.getPrezzoFornitore());
        JSONObject rowJson = JSONFactoryUtil.createJSONObject(JSONFactoryUtil.
                looseSerialize(row));
        String imageSrc = Constants.PICTURE_URL_PREFIX + "not-found.png";
        try {
            imageSrc = Utils.retrievePictureURL(p);
        } catch (PortalException e) {
            this.logger.warn(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        String html = "<a href='" + imageSrc + "' title='" + p.getNome()
                + "' target='_blank'><img src='" + imageSrc
                + "' class='center-h' /></a>";

        String shape = "";
        try {
            shape = CategorieMerceologicheLocalServiceUtil.
                    getCategorieMerceologiche(item.getCategoriaMerceologica()).
                    getDescrizione();
        } catch (PortalException e) {
            this.logger.warn(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        String name = p.getNome() + "\nVaso: " + p.getVaso() + " " + shape
                + "\nH. " + p.getAltezzaPianta();
        rowJson.put("prezzoUn", item.getPrezzo1());
        rowJson.put("prezzo3", item.getPrezzo3());
        rowJson.put("prezzoSc", item.getPrezzo1() * (1 - discount
                / Constants.HUNDRED));
        rowJson.put("img", html);
        rowJson.put("name", name);
        rowJson.put("itemCode", p.getCodice());
        rowJson.put("ean", p.getEan());
        rowJson.put("modificato", true);
        if (itemOver) {
            rowJson.put(Ordini.ItemOverCode.s2.name(), p.getSormonto2Fila());
            rowJson.put(Ordini.ItemOverCode.s3.name(), p.getSormonto3Fila());
            rowJson.put(Ordini.ItemOverCode.s4.name(), p.getSormonto4Fila());
        } else {
            rowJson.put(Ordini.ItemOverCode.s2.name(), 0);
            rowJson.put(Ordini.ItemOverCode.s3.name(), 0);
            rowJson.put(Ordini.ItemOverCode.s4.name(), 0);
        }
        return rowJson;
    }

    /**
     * Creates a new row in the current cart.
     * 
     * @param cartId
     *            current cart id
     * @param rowId
     *            row id
     * @param rowNumber
     *            row number in cart
     * @param item
     *            adding item price
     * @param p
     *            adding item details
     * @param v
     *            variant selected
     * @param quantity
     *            items quantity
     * @param itemOver
     *            true if item will be placed over other in the cart, false
     *            otherwise
     * @param discount item discount
     * @return the new row
     * @throws SystemException
     *             exception
     * @throws JSONException
     *             exception
     */
    private JSONObject createNewGenericRow(final long cartId, final int rowId,
            final int rowNumber, final Articoli item, final Piante p,
            final DescrizioniVarianti v, final int quantity,
            final boolean itemOver, final double discount) throws JSONException,
            SystemException {
        Riga row = RigaLocalServiceUtil.createRiga(rowId);
        row.setIdCarrello(cartId);
        row.setProgressivo(rowNumber);
        row.setIdArticolo(p.getId());
        double price = item.getPrezzo1() * (1 - discount / Constants.HUNDRED);
        double variantPrice = 0.0;
        String note = "";
        String variantCode = "";
        if (Validator.isNotNull(v)) {
            //item.getPrezzo2() * (1 - discount / Constants.HUNDRED);
            variantPrice = item.getPrezzo2() - item.getPrezzo1();
            price += variantPrice;
            note = v.getDescrizione();
            variantCode = v.getCodiceVariante();
        }
        row.setPrezzo(price);

        row.setImportoVarianti(variantPrice);
        row.setPianteRipiano(quantity);
        row.setImporto(price * quantity);
        row.setSormontate(itemOver);
        row.setStringa(note);
        row.setCodiceVariante(variantCode);
        row.setOpzioni("");
        row.setSconto(discount);
        row.setPrezzoFornitore(p.getPrezzoFornitore());
        JSONObject rowJson = JSONFactoryUtil.createJSONObject(JSONFactoryUtil.
                looseSerialize(row));
        String imageSrc = Constants.PICTURE_URL_PREFIX + "not-found.png";
        try {
            imageSrc = Utils.retrievePictureURL(p);
        } catch (PortalException e) {
            this.logger.warn(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        String html = "<a href='" + imageSrc + "' title='" + p.getNome()
                + "' target='_blank'><img src='" + imageSrc
                + "' class='center-h' /></a>";

        String shape = "";
        try {
            shape = CategorieMerceologicheLocalServiceUtil.
                    getCategorieMerceologiche(item.getCategoriaMerceologica()).
                    getDescrizione();
        } catch (PortalException e) {
            this.logger.warn(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        String name = p.getNome() + "\nVaso: " + p.getVaso() + " " + shape
                + "\nH. " + p.getAltezzaPianta();
        rowJson.put("img", html);
        rowJson.put("name", name);
        rowJson.put("itemCode", p.getCodice());
        rowJson.put("ean", p.getEan());
        rowJson.put("prezzoUn", item.getPrezzo1());
        rowJson.put("prezzo3", item.getPrezzo3());
        rowJson.put("prezzoSc", item.getPrezzo1() * (1 - discount
                / Constants.HUNDRED));
        rowJson.put("modificato", true);
        return rowJson;
    }
    
    /**
     * Returns the item with the code inputed.
     * 
     * @param resourceRequest
     *            {@link ResourceRequest}
     * @return {@link JSONArray};
     */
    private JSONArray getItems(final ResourceRequest resourceRequest) {

        String itemCode = ParamUtil.getString(resourceRequest, "itemCode");
        String name = ParamUtil.getString(resourceRequest, "itemName");
//        long idCategory = ParamUtil.getLong(resourceRequest, "category");
        String idSupplier = ParamUtil.getString(resourceRequest, "supplier");
        boolean active = ParamUtil.getBoolean(resourceRequest,
                "onlyActive", false);
        boolean inactive = ParamUtil.getBoolean(resourceRequest,
                "onlyInactive", false);
        boolean available = ParamUtil.getBoolean(resourceRequest,
                "onlyAvailable");

        JSONObject json = JSONFactoryUtil.createJSONObject();
        JSONArray results = JSONFactoryUtil.createJSONArray();
        json.put("response", results);

        try {
//            List<Piante> items = PianteLocalServiceUtil.findPlants(idCategory,
//                    active, idSupplier, name, itemCode);
            List<Piante> items = PianteLocalServiceUtil.findPlants(active,
            		inactive, idSupplier, name, itemCode, false);

            for (Piante p : items) {
                JSONObject object = JSONFactoryUtil
                        .createJSONObject(JSONFactoryUtil.looseSerialize(p));
                List<DescrizioniVarianti> variants =
                        DescrizioniVariantiLocalServiceUtil.findVariants(
                                p.getCodice());
                JSONArray variantsJson = JSONFactoryUtil.createJSONArray();
                object.put("varianti", variantsJson);
                for (DescrizioniVarianti v : variants) {
                    JSONObject vJson = JSONFactoryUtil.createJSONObject(
                            JSONFactoryUtil.looseSerialize(v));
                    variantsJson.put(vJson);
                }

                Articoli item = ArticoliLocalServiceUtil.fetchArticoli(p.
                        getCodice());
                JSONObject itemDetails = JSONFactoryUtil.createJSONObject();
                if (item != null) {
                    itemDetails = JSONFactoryUtil.createJSONObject(
                            JSONFactoryUtil.looseSerialize(item));
                }
                object.put("dettagliPianta", itemDetails);
                if (available) {
                    if (p.getDisponibilita() > 0) {
                        results.put(object);
                    }
                } else {
                    results.put(object);
                }
            }
        } catch (JSONException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        this.logger.debug("RESULTS: " + results.length());

        return results;
    }

    /**
     * Stores current order in the database.
     * 
     * @param resourceRequest
     *            {@link ResourceRequest}
     * @return {@link Response}
     */
    private Response saveOrder(final ResourceRequest resourceRequest) {

        Response response = null;
        JSONObject orderJson = null;
        String message = null;
        Code c = null;
        try {
            orderJson = JSONFactoryUtil.createJSONObject(ParamUtil.getString(
                    resourceRequest, Ordini.CURRENT_ORDER));
            Ordine order = this.getOrder(orderJson, resourceRequest);
            this.logger.debug(order.toString());
            List<Carrello> carts = new ArrayList<Carrello>();
            Map<Integer, List<Riga>> cartRows =
                    new HashMap<Integer, List<Riga>>();
            JSONArray cartsJson = orderJson.getJSONArray(Constants.CARTS);
            for (int i = 0; i < cartsJson.length(); i++) {
                JSONObject cartJson = cartsJson.getJSONObject(i);
                Carrello cart = this.getCart(cartJson);
                JSONArray rowsJson = cartJson.getJSONArray(Constants.ROWS);
                List<Riga> rows = new ArrayList<Riga>();
                for (int j = 0; j < rowsJson.length(); j++) {
                    JSONObject rowJson = rowsJson.getJSONObject(j);
                    Riga row = this.getRow(rowJson);
                    rows.add(row);
                }
                carts.add(cart);
                cartRows.put(i, rows);
            }
            Ordine savedOrder = this.storeOrder(order, carts, cartRows);
            c = Code.OK;
            message = JSONFactoryUtil.looseSerialize(savedOrder);
        } catch (JSONException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            c = Response.Code.GENERIC_ERROR;
            message = e.getMessage();
        } catch (PortalException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            c = Response.Code.GENERIC_ERROR;
            message = e.getMessage();
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            c = Response.Code.GENERIC_ERROR;
            message = e.getMessage();
        } catch (NumberFormatException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            c = Response.Code.GENERIC_ERROR;
            message = e.getMessage();
        } finally {
            response = new Response(c, message);
        }
        return response;
    }

    /**
     * Persists the order in the database and return the id.
     * 
     * @param order
     *            the order
     * @param carts
     *            list of carts belonging current order
     * @param cartRows
     *            maps containing carts and rows
     * 
     * @return the just added order
     * @throws SystemException
     *             exception
     * @throws PortalException 
     */
    private Ordine storeOrder(final Ordine order, final List<Carrello> carts,
            final Map<Integer, List<Riga>> cartRows) throws SystemException, PortalException {
        if (order.getId() > 0
                && order.getStato() != OrderState.SAVED.getValue()) {
        	order.setModificato(this.isModified(order));
            //order.setModificato(true);
            order.setDdtGenerati(false);
            List<TestataDocumento> documents = TestataDocumentoLocalServiceUtil.
                    findDocumentsByOrderId(order.getId());
            for (TestataDocumento document : documents) {
                TestataDocumentoPK documentPK = document.getPrimaryKey();
                List<RigoDocumento> rows = RigoDocumentoLocalServiceUtil.
                        findByHeaderItem(documentPK);
                for (RigoDocumento row : rows) {               
                    this.logger.info("DELETING ROW:\n" + row + "\n DUE TO AN"
                            + " ORDER UPDATE");
                    RigoDocumentoLocalServiceUtil.deleteRigoDocumento(row);
                }
                document.setInviato(false);
                TestataDocumentoLocalServiceUtil.updateTestataDocumento(
                		document);
            }
        }
        order.setStato(OrderState.SAVED.getValue());
        Ordine savedOrder = OrdineLocalServiceUtil.updateOrdine(order);
        for (int i = 0; i < carts.size(); i++) {
            Carrello cart = carts.get(i);
            cart.setIdOrdine(savedOrder.getId());
            CarrelloLocalServiceUtil.updateCarrello(cart);
            for (Riga row : cartRows.get(i)) {
                row.setIdCarrello(cart.getId());
                RigaLocalServiceUtil.updateRiga(row);
                Piante p = PianteLocalServiceUtil.fetchPiante(
                        row.getIdArticolo());
                if (p != null) {
                    p.setDisponibilita(p.getDisponibilita()
                            - row.getPianteRipiano());
                    PianteLocalServiceUtil.updatePiante(p);
                }
                this.logger.debug("ROW: " + row.toString());
            }
            this.logger.debug("CART: " + cart.toString());
        }
        this.logger.debug("ORDER: " + savedOrder.toString());

        return savedOrder;
    }

    private boolean isModified(Ordine order) throws PortalException,
    	SystemException {
		Ordine checking_order = OrdineLocalServiceUtil.getOrdine(
				order.getId());
		this.logger.debug(order.toString());
		this.logger.debug("");
		this.logger.debug(checking_order.toString());
		boolean edited = false;
		edited = edited || !order.getNote().equalsIgnoreCase(checking_order.getNote());
		edited = edited || !order.getNoteCliente().equalsIgnoreCase(
				checking_order.getNoteCliente());
		edited = edited || !order.getDataConsegna().equals(
				checking_order.getDataConsegna());
		edited = edited || !order.getIdTrasportatore().equals(
				checking_order.getIdTrasportatore());
		edited = edited || !order.getIdTrasportatore2().equals(
				checking_order.getIdTrasportatore2());
		edited = edited || order.getIdTipoCarrello() !=
				checking_order.getIdTipoCarrello();
		edited = edited || !order.getLuogoCarico().equalsIgnoreCase(
				checking_order.getLuogoCarico());
		edited = edited || !order.getCodiceIva().equals(checking_order.getCodiceIva());
		edited = edited || order.getStampaEtichette() !=
				checking_order.getStampaEtichette();
		return edited;
	}

	/**
     * Extracts the row information from the row json object.
     * 
     * @param rowJson
     *            row JSON object
     * @return the row
     * @throws SystemException
     *             exception
     * @throws PortalException
     *             exception
     */
    private Riga getRow(final JSONObject rowJson)
            throws SystemException, PortalException {
        Riga row = null;
        row = RigaLocalServiceUtil.createRiga(0);
        row.setIdCarrello(rowJson.getLong("idCarrello"));
        row.setProgressivo(rowJson.getInt(Constants.NUMBER));
        row.setIdArticolo(rowJson.getLong(Constants.ITEM_ID));
        row.setImporto(rowJson.getDouble(Constants.AMOUNT));
        row.setPianteRipiano(rowJson.getInt(Constants.PLATFORM_ITEMS));
        row.setSormontate(rowJson.getBoolean("sormontate"));
        row.setStringa(rowJson.getString("stringa"));
        row.setOpzioni(rowJson.getString("opzioni"));
        row.setImportoVarianti(rowJson.getDouble(Constants.VARIANT_PRICE));
        row.setPrezzo(rowJson.getDouble("prezzo"));
        row.setCodiceVariante(rowJson.getString("codiceVariante"));
        row.setCodiceRegionale(rowJson.getString("codiceRegionale"));
        row.setPassaporto(rowJson.getString("passaporto"));
        row.setS2(rowJson.getInt(Ordini.ItemOverCode.s2.name()));
        row.setS3(rowJson.getInt(Ordini.ItemOverCode.s3.name()));
        row.setS4(rowJson.getInt(Ordini.ItemOverCode.s4.name()));
        row.setSconto(rowJson.getDouble("sconto"));
        row.setPrezzoFornitore(rowJson.getDouble("prezzoFornitore"));
        row.setPrezzoEtichetta(rowJson.getDouble("prezzoEtichetta"));
        row.setEan(rowJson.getString("ean"));
        row.setModificato(rowJson.getBoolean("modificato"));
        row.setRicevuto(rowJson.getBoolean("ricevuto"));
        return row;
    }

    /**
     * Extracts the cart information from cart json obect.
     * 
     * @param cartJson
     *            current order
     * @return the cart
     * @throws SystemException
     *             exception
     * @throws PortalException
     *             exception
     */
    private Carrello getCart(final JSONObject cartJson)
            throws SystemException, PortalException {

        Carrello cart = null;
//        if (newOrder) {
            cart = CarrelloLocalServiceUtil.createCarrello(0);
            cart.setIdOrdine(cartJson.getLong(Constants.ORDER_ID));
            cart.setProgressivo(cartJson.getInt(Constants.NUMBER));
            cart.setChiuso(cartJson.getBoolean(Constants.CLOSED)
                    || cartJson.getInt(Constants.USED_HEIGHT) == height);
//        } else {
//            cart = CarrelloLocalServiceUtil.getCarrello(cartJson.getLong(
//                    Constants.ID));
//        }
        cart.setGenerico(cartJson.getBoolean(Constants.GENERIC));
        cart.setImporto(cartJson.getDouble(Constants.AMOUNT));
        return cart;
    }

    /**
     * Returns the order from the current order.
     * 
     * @param orderJson
     *            {@link JSONObject}
     * @param resourceRequest
     *            {@link ResourceRequest}
     * @return the order
     * @throws SystemException
     *             exception
     * @throws PortalException
     *             exception
     */
    private Ordine getOrder(final JSONObject orderJson,
            final ResourceRequest resourceRequest) throws PortalException,
            SystemException {

        Ordine order = null;
        if (orderJson.getInt(Constants.ID) <= 0) {
            order = OrdineLocalServiceUtil.createOrdine(0);
        } else {
            order = OrdineLocalServiceUtil.getOrdine(orderJson.getLong("id"));
            List<Carrello> carts = CarrelloLocalServiceUtil.findCartsInOrder(
                    order.getId());
            for (Carrello cart : carts) {
                List<Riga> rows = RigaLocalServiceUtil.findRowsInCart(
                        cart.getId());
                for (Riga row : rows) {
                    RigaLocalServiceUtil.deleteRiga(row);
                    Piante p = PianteLocalServiceUtil.fetchPiante(
                            row.getIdArticolo());
                    if (p != null) {
                        p.setDisponibilita(p.getDisponibilita()
                                + row.getPianteRipiano());
                        PianteLocalServiceUtil.updatePiante(p);
                    }
                }
                CarrelloLocalServiceUtil.deleteCarrello(cart);
            }
        }
       	order.setStampaEtichette(orderJson.getBoolean(Constants.PRINT_LABELS));
        order.setImporto(orderJson.getDouble(Constants.AMOUNT));
        order.setDataConsegna(new Date(ParamUtil.getLong(resourceRequest,
                Constants.DELIVERY_DATE)));
//        order.setDataConsegna(new Date(orderJson.getLong(
//                Constants.DELIVERY_DATE)));
        order.setDataInserimento(new Date(ParamUtil.getLong(resourceRequest,
                Constants.ORDER_DATE)));
//        order.setDataInserimento(new Date(orderJson.getLong(
//                Constants.ORDER_DATE)));
        order.setIdAgente(orderJson.getLong(Constants.AGENT_ID));
        order.setIdCliente(ParamUtil.getString(resourceRequest,
                Ordini.CUSTOMER_CODE, ""));
        AnagraficheClientiFornitori customer = AnagraficheClientiFornitoriLocalServiceUtil.
        		fetchAnagraficheClientiFornitori(order.getIdCliente());
        if (customer != null) {
			order.setCliente(customer.getRagioneSociale1() + " "
					+ customer.getRagioneSociale2());
			ClientiFornitoriDatiAgg cData = ClientiFornitoriDatiAggLocalServiceUtil.
					fetchClientiFornitoriDatiAgg(new ClientiFornitoriDatiAggPK(
							customer.getCodiceAnagrafica(), false));
			if (cData.getTipoPagamento() != null 
					&& !cData.getTipoPagamento().isEmpty()) {
				PianoPagamenti pagam = PianoPagamentiLocalServiceUtil.
						getPianoPagamenti(cData.getTipoPagamento());
				order.setPagamento(pagam.getDescrizione());
			}
		}
        order.setIdTipoCarrello(ParamUtil.getLong(resourceRequest,
                "cartTypeId", 0));
        order.setIdTrasportatore(ParamUtil.getString(resourceRequest,
                "carrierCode", ""));
        if (order.getIdTrasportatore() != null && !order.getIdTrasportatore().isEmpty()) {
        	Vettori carrier = VettoriLocalServiceUtil.fetchVettori(order.getIdTrasportatore());
        	if (carrier != null) {
				order.setVettore(carrier.getRagioneSociale() + " " + carrier.getRagioneSociale2());
			}
		}
        String luogoCarico = ParamUtil.getString(resourceRequest,
                "loadPlace", "");
        order.setLuogoCarico(luogoCarico);
        String note = ParamUtil.getString(resourceRequest, Constants.NOTE);
        order.setNote(note);
        order.setNoteCliente(ParamUtil.getString(resourceRequest,
                Constants.CUSTOMER_NOTE, ""));
        
        if (order.getNumero() == 0) {
            order.setAnno(Utils.getYear());
            order.setCentro(ParamUtil.getString(resourceRequest,
                    Constants.BILLING_CENTER));
            order.setNumero(OrdineLocalServiceUtil.getNumber(Utils.getYear(),
                    ParamUtil.getString(resourceRequest,
                            Constants.BILLING_CENTER)));
        }
        
        order.setCodiceIva(ParamUtil.getString(resourceRequest,
                Constants.VAT_CODE));
        
        order.setDataOrdineGT(new Date(ParamUtil.getLong(resourceRequest,
                Constants.GT_ORDER_DATE)));
        order.setNumOrdineGT(ParamUtil.getString(resourceRequest, 
        		Constants.GT_ORDER_NUM));
        
        return order;
    }

    /**
     * Sends the suppliers order confirmations.
     * @param rreq
     *            {@link ResourceRequest}
     * @return {@link Response}
     */
    private Response sendSuppliersConfirmation(final ResourceRequest rreq) {
        String message = null;
        Code c = null;
        
        long orderId = ParamUtil.getLong(rreq, Ordini.ORDER_ID, 0);
        if (orderId > 0) {
            try {
                Ordine order = OrdineLocalServiceUtil.getOrdine(orderId);
                if (!order.getIdTrasportatore().isEmpty() 
                        && order.getIdTipoCarrello() > 0 
                        && order.getDataConsegna().compareTo(Utils.today().
                                getTime()) >= 0) {
                    List<Carrello> carts = CarrelloLocalServiceUtil.
                            findCartsInOrder(orderId);
                    Set<String> suppliers = new HashSet<String>();
                    List<Confirmation> confs = new ArrayList<Confirmation>();
                    User user = UserLocalServiceUtil.getUser(Long.parseLong(
                            rreq.getRemoteUser()));
                    boolean added = false;
                    for (Carrello cart : carts) {
                        List<Riga> rows = RigaLocalServiceUtil.findRowsInCart(
                                cart.getId());
                        for (Riga row : rows) {
                        	this.logger.debug("ROW: " + row.toString());
                        	if (order.getModificato() || row.getModificato()) {
                        		Piante p = PianteLocalServiceUtil.getPiante(
                                        row.getIdArticolo());
                                Articoli item = ArticoliLocalServiceUtil.
                                        getArticoli(p.getCodice());
                                this.logger.debug("ROW: " + row.getId()
                        				+ " edited, sending notification to: "
                        				+ p.getIdFornitore());
                                if (item.getLibLng1() > 0) {
                                    ClientiFornitoriDatiAgg supplierData =
                                            ClientiFornitoriDatiAggLocalServiceUtil.
                                            fetchClientiFornitoriDatiAgg(
                                                    new ClientiFornitoriDatiAggPK(
                                                            p.getIdFornitore(),
                                                            true));
                                    if (supplierData != null) {
                                        row.setCodiceRegionale(supplierData.
                                                getCodiceRegionale());
                                    }
                                } 
                                if (suppliers.add(p.getIdFornitore())) {
                                    AnagraficheClientiFornitori supplier =
                                            AnagraficheClientiFornitoriLocalServiceUtil.
                                            getAnagraficheClientiFornitori(
                                                    p.getIdFornitore());
                                        confs.add(new SupplierConfirmation(
                                                user.getScreenName(), order,
                                                Ordini.SUPPLIER_CONFIRM,
                                                user.getScreenName(),
                                                user.getUserId(), supplier));
                                }
                                row.setModificato(false);
                                RigaLocalServiceUtil.updateRiga(row);
							}
                           
                        }
                    }
                    
                    AnagraficheClientiFornitori carrier =
                            AnagraficheClientiFornitoriLocalServiceUtil.
                                fetchAnagraficheClientiFornitori(
                                        order.getIdTrasportatore());
                    if (carrier != null) {
                        Rubrica carrierContact = RubricaLocalServiceUtil.
                                fetchRubrica(new RubricaPK(carrier.
                                        getCodiceAnagrafica(), 1));
                        if (carrierContact != null
                                && !carrierContact.getEMailTo().isEmpty()) {
                            confs.add(new CarrierConfirmation(user.
                                    getEmailAddress(), order,
                                    "carrier-confirm", user.getScreenName(),
                                    user.getUserId(), carrier, carrierContact.
                                            getEMailTo()));
                        }
                    }
                    
                    confs.add(new CustomerConfirmation(
                            user.getEmailAddress(), order, "customer-confirm",
                            user.getScreenName(), user.getUserId(),
                            AnagraficheClientiFornitoriLocalServiceUtil.
                                fetchAnagraficheClientiFornitori(
                                        order.getIdCliente()),
                                        user.getEmailAddress()));
                    List<Confirmation> sentConfs =
                    		new ArrayList<Confirmation>();
                    for (int i = 0; i < confs.size(); i++) {
                        confs.get(i).createConfirmation(
                                this.ds.getConnection());
                        if (confs.get(i) instanceof CustomerConfirmation) {
                            String dateStr = new SimpleDateFormat("yyyyMMdd").
                                    format(confs.get(i).getOrder().
                                            getDataConsegna());
                            File pdf = new File(DOCUMENTS_FOLDER
                                    + confs.get(i).getOrder().getIdCliente()
                                    + '_' + confs.get(i).getOrder().getNumero()
                                    + confs.get(i).getOrder().getCentro() + '_'
                                    + dateStr + ".pdf");
                            FileUtil.copyFile(
                                    new File(confs.get(i).getReport()), pdf);
                        }
                        
                        if (Validator.isEmailAddress(confs.get(i).
                        		getEmailTo())) {
                        	confs.get(i).sendConfirmation();
                        	sentConfs.add(confs.get(i));
                        } else {
                        	logger.debug(confs.get(i).getRecipient().
                        			getRagioneSociale1() + " has not email.");
                        }
                    }
                    
                    order.setStato(OrderState.INACTIVE.getValue());
                    order.setModificato(false);
                    OrdineLocalServiceUtil.updateOrdine(order);
                    c = Response.Code.OK;
                    message = JSONFactoryUtil.looseSerialize(sentConfs);
                } else {
                    c = Response.Code.SENDING_MAIL_ERROR;
                    message = "sending-error";
                }
            } catch (NumberFormatException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (PortalException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (SystemException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (ConfirmationException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (NamingException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (SQLException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (JRException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (AddressException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (IOException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            }
        } else {
            c = Response.Code.GENERIC_ERROR;
            message = Ordini.NO_ORDER_SPECIFIED;
        }
        
        return new Response(c, message);
    }
    
    /**
     * Sends the customer order confirmation.
     * 
     * @param resourceRequest
     *            {@link ResourceRequest}
     * @return {@link Response}
     
    private Response sendOrderConfirm(final ResourceRequest resourceRequest) {
        long orderId = ParamUtil.getLong(resourceRequest, Ordini.ORDER_ID, 0);

        String message = null;
        Code c = null;
        if (orderId > 0) {
            try {
                User user = UserLocalServiceUtil.getUser(Long.
                        parseLong(resourceRequest.getRemoteUser()));
                String customerEmail = ParamUtil.getString(resourceRequest,
                        "customerEmail");
                String customerCode = ParamUtil.getString(resourceRequest,
                        "customerCode");
                AnagraficheClientiFornitori customer =
                        AnagraficheClientiFornitoriLocalServiceUtil.
                        getAnagraficheClientiFornitori(customerCode);
                Ordine order = OrdineLocalServiceUtil.getOrdine(orderId);
                List<Confirmation> confirmations =
                        new ArrayList<Confirmation>();
                if (Validator.isNotNull(customerEmail)
                        && !customerEmail.isEmpty()) {
                    confirmations.add(new CustomerConfirmation(
                            user.getEmailAddress(), order,
                            Ordini.CUSTOMER_CONFIRM, user.getScreenName(),
                            user.getUserId(), customer, customerEmail));
                } else {
                    confirmations.add(new CustomerConfirmation(user
                            .getEmailAddress(), order, Ordini.CUSTOMER_CONFIRM,
                            user.getScreenName(), user .getUserId(), customer));
                }

                for (int i = 0; i < confirmations.size(); i++) {
                    confirmations.get(i).createConfirmation(
                            this.ds.getConnection());
                    confirmations.get(i).sendConfirmation();
                    if (i == 0) {
                        message = confirmations.get(0).getReport();
                    }
                }
                c = Response.Code.OK;
            } catch (SQLException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (NamingException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (NumberFormatException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (PortalException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (SystemException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (JRException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (AddressException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (ConfirmationException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            }
        } else {
            c = Response.Code.GENERIC_ERROR;
            message = Ordini.NO_ORDER_SPECIFIED;
        }

        return new Response(c, message);
    }
    */

    /**
     * Prints a cart label and returns the preview.
     * 
     * @param resourceRequest {@link ResourceRequest}
     * @return return {@link Response}
     */
    private Response printCartLabel(final ResourceRequest resourceRequest) {
        long orderId = ParamUtil.getLong(resourceRequest, Ordini.ORDER_ID, 0);

        String message = null;
        Code c = null;
        if (orderId > 0) {
            try {
                Report report = new Report(this.ds.getConnection());
                User user = UserLocalServiceUtil.getUser(Long.
                        parseLong(resourceRequest.getRemoteUser()));
                String filePath = report.print(user.getUserId(), "", orderId,
                        "cart-label-details", "", false);
                report.closeConnection();

                c = Response.Code.OK;
                message = filePath;
            } catch (SQLException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (NumberFormatException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (PortalException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (SystemException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            } catch (JRException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                c = Response.Code.GENERIC_ERROR;
                message = e.getMessage();
            }

        } else {
            c = Response.Code.GENERIC_ERROR;
            message = Ordini.NO_ORDER_SPECIFIED;
        }

        return new Response(c, message);
    }

    @Override
    public final void doView(final RenderRequest renderRequest,
            final RenderResponse renderResponse) throws IOException,
            PortletException {
        PortletPreferences preferences = renderRequest.getPreferences();
        int tmp = GetterUtil.getInteger(preferences.getValue(
                Constants.CART_DEFAULT_HEIGHT, "260"));
        double addLabelPrint = GetterUtil.getInteger(preferences.getValue(
                Constants.ADD_PRINT_LABEL, "0.1"));
        if (tmp != this.height) {
            this.height = tmp;
        }
        if (addLabelPrint != this.labelPrintCost) {
            this.labelPrintCost = addLabelPrint;
        }
        super.doView(renderRequest, renderResponse);

    }

    @Override
    public final void init(final PortletConfig config) throws PortletException {
        Context ctx;
        try {
            ctx = new InitialContext();
            this.ds = (DataSource) ctx.lookup("jdbc/OrdiniPool");
        } catch (NamingException e) {
            this.logger.fatal(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        this.height = GetterUtil.getInteger(config.
                getInitParameter(Constants.CART_DEFAULT_HEIGHT));
        this.labelPrintCost = GetterUtil.getDouble(config.
                getInitParameter(Constants.ADD_PRINT_LABEL));
        super.init(config);
    }

    /**
     * Delete a saved Order.
     * 
     * @param areq
     *            {@link ActionRequest}
     * @param ares
     *            {@link ActionResponse}
     * @throws SystemException
     *             exception
     */
    public final void deleteOrder(final ActionRequest areq,
            final ActionResponse ares) throws SystemException {
        
        if (Validator.isNotNull(ParamUtil.getLong(areq, "orderId"))) {
            long orderId = ParamUtil.getLong(areq, "orderId");
            this.logger.debug("DELETING ORDER: " + orderId + "...");
            Ordine deletedOrder = null;
            List<Carrello> deletedCarts = new ArrayList<Carrello>();
            List<Riga> deletedRows = new ArrayList<Riga>();
            boolean error = false;
            try {
                Ordine order = OrdineLocalServiceUtil.getOrdine(orderId);
                List<TestataDocumento> list = TestataDocumentoLocalServiceUtil.
                        findDocumentsByOrderId(order.getId());
                this.logger.debug("N° " + list.size() + " DOCUMENT WILL BE"
                        + " DELETED");
                for (TestataDocumento document : list) {
                    this.logger.debug("DELETING DOCUMENT: " + document);
                    List<RigoDocumento> rows = RigoDocumentoLocalServiceUtil.
                            findByHeaderItem(document.getPrimaryKey());
                    for (RigoDocumento row : rows) {
                        this.logger.debug("DELETING ROWS: " + document);
                        RigoDocumentoLocalServiceUtil.deleteRigoDocumento(row);
                    }
                    TestataDocumentoLocalServiceUtil.deleteTestataDocumento(
                            document);
                }
                this.logger.debug("DELETING ORDER: " + order);
                List<Carrello> carts = CarrelloLocalServiceUtil.
                        findCartsInOrder(orderId);
                for (Carrello cart : carts) {
                    this.logger.debug("DELETING CART: " + cart);
                    List<Riga> rows = RigaLocalServiceUtil.
                            findRowsInCart(cart.getId());
                    for (Riga row : rows) {
                        this.logger.debug("DELETING ROW: " + row);
                        Piante p = PianteLocalServiceUtil.
                                getPiante(row.getIdArticolo());
                        p.setDisponibilita(
                                p.getDisponibilita() + row.getPianteRipiano());
                        PianteLocalServiceUtil.updatePiante(p);
                        deletedRows.add(RigaLocalServiceUtil.deleteRiga(
                                row.getId()));
                    }
                    deletedCarts.add(CarrelloLocalServiceUtil.deleteCarrello(
                            cart.getId()));
                }
                deletedOrder = OrdineLocalServiceUtil.deleteOrdine(
                        order.getId());
            } catch (PortalException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
                SessionErrors.add(areq, "delete-order-error");
                error  = true;
            } finally {
                if (error) {
                    for (Carrello cart : deletedCarts) {
                        CarrelloLocalServiceUtil.updateCarrello(cart);
                    }
                    for (Riga row : deletedRows) {
                        RigaLocalServiceUtil.updateRiga(row);
                    }
                    if (deletedOrder != null) {
                        OrdineLocalServiceUtil.updateOrdine(deletedOrder);
                    }
                } else {
                    SessionMessages.add(areq, "delete-order-success");
                }
            }
        } else {
            this.logger.warn("ORDER ID IS NULL!!!");
        }
        
    }
    
    /**
     * Exports an confirmed Order.
     * 
     * @param areq
     *            {@link ActionRequest}
     * @param ares
     *            {@link ActionResponse}
     * @throws SystemException
     *             exception
     */
    public final void exportOrder(final ActionRequest areq,
            final ActionResponse ares) throws SystemException {
        if (Validator.isNotNull(ParamUtil.getLong(areq, "orderId"))) {
            long orderId = ParamUtil.getLong(areq, "orderId");
            this.logger.info("MANUAL EXPORTING ORDER: " + orderId + "...");
            try {
                Ordine o = OrdineLocalServiceUtil.getOrdine(orderId);
                List<Ordine> orders = new ArrayList<Ordine>();
                orders.add(o);
                File exportFolder = new File(PortletProps.get("export-folder"));
                OrderExporterService service =
                        OrderExporterService.getInstance();
                Ordine[] exportedOrders = null;
                if (Validator.isNotNull(areq.getRemoteUser())
                        && Validator.isNumber(areq.getRemoteUser())) {
                    exportedOrders = service.export(this.ds.getConnection(),
                            exportFolder, orders, Long.parseLong(
                                    areq.getRemoteUser()));
                    Utils.updateCheckFile(exportFolder, exportedOrders,
                            "MANUAL EXPORTED ORDERS");
                }
            } catch (PortalException e) {
                SessionErrors.add(areq, "error-retrieving-order-x");
                ares.setRenderParameter(Ordini.EXCEPTION_ARGS,
                        String.valueOf(orderId));
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();                        
                }
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Unlocks an inactive Order.
     * 
     * @param areq
     *            {@link ActionRequest}
     * @param ares
     *            {@link ActionResponse}
     * @throws SystemException
     *             exception
     */
    public final void unlockOrder(final ActionRequest areq,
            final ActionResponse ares) throws SystemException {
        
        if (Validator.isNotNull(ParamUtil.getLong(areq, "orderId"))) {
            final long orderId = ParamUtil.getLong(areq, "orderId");
            this.logger.debug("UNLOCKING ORDER: " + orderId + "...");
            try {
                Ordine o = OrdineLocalServiceUtil.getOrdine(orderId);
                boolean passport = false;
                List<Carrello> carts = CarrelloLocalServiceUtil.
                        findCartsInOrder(o.getId());
                for (Carrello cart : carts) {
                    List<Riga> rows = RigaLocalServiceUtil.findRowsInCart(
                            cart.getId());
                    for (Riga row : rows) {
                        Piante p = PianteLocalServiceUtil.
                                getPiante(row.getIdArticolo());
                        passport = (p.getPassaporto() 
                                && row.getPassaporto().isEmpty()) || passport;
                    }
                }
                if (passport) {
                    o.setStato(OrderState.PENDING.getValue());
                } else {
                    o.setStato(OrderState.CONFIRMED.getValue());
                }
                OrdineLocalServiceUtil.updateOrdine(o);
                
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        GiveDocumentExporterService service =
                                GiveDocumentExporterService.getInstance();
                        try {
                            File exportFolder = new File(PortletProps.get(
                                    "export-folder"));
                            TestataDocumentoPK[] documents =
                                    service.createTodayGiveDocuments();
                            Utils.updateCheckFile(exportFolder, documents,
                                    "GIVE DOCUMENTS CREATED AFTER ORDER"
                                    + " UNLOCK");
                        } catch (AddressException e) {
                            logger.error(e.getMessage());
                            if (logger.isDebugEnabled()) {
                                e.printStackTrace();
                            }
                        } catch (NamingException e) {
                            logger.error(e.getMessage());
                            if (logger.isDebugEnabled()) {
                                e.printStackTrace();
                            }
                        } catch (SQLException e) {
                            logger.error(e.getMessage());
                            if (logger.isDebugEnabled()) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            logger.error(e.getMessage());
                            if (logger.isDebugEnabled()) {
                                e.printStackTrace();
                            }
                        } catch (JRException e) {
                            logger.error(e.getMessage());
                            if (logger.isDebugEnabled()) {
                                e.printStackTrace();
                            }
                        } catch (PortalException e) {
                            logger.error(e.getMessage());
                            if (logger.isDebugEnabled()) {
                                e.printStackTrace();
                            }
                        } catch (SystemException e) {
                            logger.error(e.getMessage());
                            if (logger.isDebugEnabled()) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                t.run();
                
            } catch (PortalException e) {
                SessionErrors.add(areq, "error-retrieving-order-x");
                ares.setRenderParameter(Ordini.EXCEPTION_ARGS,
                        String.valueOf(orderId));
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();                        
                }
            }
        } else {
            this.logger.warn("ORDER ID IS NULL!!!");
        }
        
    }
    
    @Override
    public final void serveResource(final ResourceRequest resourceRequest,
            final ResourceResponse resourceResponse) throws IOException,
            PortletException {
        String resourceID = resourceRequest.getResourceID();
        PrintWriter out;
        Response response;
        switch (ResourceId.valueOf(resourceID)) {
            case autoComplete:
                this.logger.debug("Looking for plants...");
                JSONArray results = this.getItems(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(results.toString());
                out.flush();
                out.close();
                break;
            case addCart:
                this.logger.debug("Adding cart");
                response = this.addCart(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case addPlatform:
                this.logger.debug("Adding platform");
                response = this.addPlatform(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case addQuantity:
                this.logger.debug("Adding quantity");
                response = this.addQuantity(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case saveOrder:
                this.logger.debug("Saving order");
                response = this.saveOrder(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case removePlatform:
                this.logger.debug("Remove platform");
                response = this.removePlatform(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case lockCart:
                this.logger.debug("Locking or unlocking cart ...");
                response = this.lockUnlockCart(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case sendConfirm:
                this.logger.debug("Sending partners order confirmation");
                response = this.sendSuppliersConfirmation(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case printCartLabel:
                this.logger.debug("Printing cart label.");
                response = this.printCartLabel(resourceRequest);
                if (response.getCode() == Code.OK.ordinal()) {
                    File f = new File(response.getMessage());
                    ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.
                            getAttribute(WebKeys.THEME_DISPLAY);
                    User company = themeDisplay.getUser();
                    long groupId = themeDisplay.getLayout().getGroupId();
                    long repoId = themeDisplay.getScopeGroupId();
                    DLFolder companyFolder;
                    try {
                        companyFolder = Utils.getAziendaFolder(groupId,
                                company);
                        DLFolder itemSheetFolder = Utils.getPicturesFolder(
                                groupId, companyFolder);
                        FileEntry fileEntry = DLAppServiceUtil.addFileEntry(
                                repoId, itemSheetFolder.getFolderId(),
                                f.getName(), MimeTypesUtil.getContentType(f),
                                f.getName(), "", "", f, new ServiceContext());
                        this.logger.debug("Added: " + fileEntry.getTitle()
                                + " to: /" + companyFolder.getName());
                        response.setMessage(DLUtil.getPreviewURL(fileEntry,
                                fileEntry.getFileVersion(), themeDisplay, ""));
                    } catch (PortalException e) {
                        this.logger.error(e.getMessage());
                        if (this.logger.isDebugEnabled()) {
                            e.printStackTrace();                        
                        }
                    } catch (SystemException e) {
                        this.logger.error(e.getMessage());
                        if (this.logger.isDebugEnabled()) {
                            e.printStackTrace();                        
                        }
                    }
                }
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case deleteCart:
                this.logger.debug("Deleting cart ...");
                response = this.deleteCart(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case updatePlatItems:
                this.logger.debug("Updating item over ...");
                response = this.updateItemOver(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case findCustomer:
                this.logger.debug("Loking for customer...");
                response = this.searchCustomer(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case loadItem:
                this.logger.debug("Loading item price3 ...");
                response = this.loadItem(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            default:
                this.logger.warn("Unknown operation: " + resourceID);
                break;
        }
    }

    private Response loadItem(ResourceRequest resourceRequest) {
        String itemCode = ParamUtil.getString(resourceRequest, "itemCode"); 
        Response response = null;
        try {
        	Articoli item = ArticoliLocalServiceUtil.getArticoli(itemCode);
			response = new Response(Code.OK, Code.OK.name(),
					JSONFactoryUtil.looseSerialize(item));
        } catch (JSONException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            response = new Response(Response.Code.GENERIC_ERROR,
                    e.getMessage());
        } catch (PortalException e) {
        	this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            response = new Response(Response.Code.GENERIC_ERROR,
                    e.getMessage());
		} catch (SystemException e) {
			this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            response = new Response(Response.Code.GENERIC_ERROR,
                    e.getMessage());;
		}
        
        return response;
	}

	/**
     * Lock/Unlock cart from current order.
     * 
     * @param resourceRequest
     *            {@link ResourceRequest}
     * @return the new order
     */
    private Response lockUnlockCart(final ResourceRequest resourceRequest) {
        ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest
                .getAttribute(WebKeys.THEME_DISPLAY);
        
        Response response = null;
        JSONObject orderJson = null;
        try {
            orderJson = JSONFactoryUtil.createJSONObject(ParamUtil.getString(
                    resourceRequest, Ordini.CURRENT_ORDER));
            long cartId = ParamUtil.getLong(resourceRequest, Constants.CART_ID);
            orderJson = lockUnlockCart(orderJson, cartId);
            response = new Response(Code.OK, LanguageUtil.format(
                    getPortletConfig(), themeDisplay.getLocale(),
                    "lock-unlock-cart-x", cartId), orderJson.toString());
            
        } catch (JSONException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            response = new Response(Response.Code.GENERIC_ERROR,
                    e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Returns JSON object containing the customer information.
     * 
     * @param resourceRequest
     *            the request
     * @return {@link Response} object containing the customer
     */
    private Response searchCustomer(final ResourceRequest resourceRequest) {
        JSONObject customerJson = JSONFactoryUtil.createJSONObject();
        Code c = null;
        String msg = "";
        try {
            String customerCode = ParamUtil.getString(resourceRequest,
                    "customerCode", "");
            if (!customerCode.isEmpty()) {
                AnagraficheClientiFornitori customer =
                        AnagraficheClientiFornitoriLocalServiceUtil.
                            getAnagraficheClientiFornitori(customerCode);
                customerJson.put("customer", JSONFactoryUtil.looseSerialize(
                        customer));
                List<Rubrica> contacts = RubricaLocalServiceUtil.findContacts(
                        customerCode);
                if (contacts.size() > 0) {
                    customerJson.put("contact", JSONFactoryUtil.looseSerialize(
                            contacts.get(0).getEMailTo()));
                } else {
                    customerJson.put("contact", "");
                }
                c = Code.OK;
                msg = c.name();
            } else {
                c = Code.GENERIC_ERROR;
                msg = "empty-customer-code";
            }
        } catch (PortalException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            c = Code.GENERIC_ERROR;
            msg = e.getMessage();
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            c = Code.GENERIC_ERROR;
            msg = e.getMessage();
        }
        
        return new Response(c, msg, customerJson.toString());
    }
    
    /**
     * Deletes cart from current order.
     * 
     * @param resourceRequest
     *            {@link ResourceRequest}
     * @return the new order
     */
    private Response deleteCart(final ResourceRequest resourceRequest) {
        ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest
                .getAttribute(WebKeys.THEME_DISPLAY);
        
        Response response = null;
        JSONObject orderJson = null;
        try {
            
            orderJson = JSONFactoryUtil.createJSONObject(ParamUtil.getString(
                    resourceRequest, Ordini.CURRENT_ORDER));

            long cartId = ParamUtil.getLong(resourceRequest, Constants.CART_ID);
            if (orderJson.has(Constants.CARTS)) {
                this.logger.debug("Deleting cart: " + cartId);
                orderJson = deleteCart(orderJson, cartId);
                this.logger.debug("Deleted : " + orderJson);
            }
            if (orderJson.length() == 0) {
                this.logger.warn("Empty order!!!");
            }

            response = new Response(Code.OK, LanguageUtil.format(
                    getPortletConfig(), themeDisplay.getLocale(),
                    "delete-cart-x", cartId), orderJson.toString());

        } catch (JSONException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            response = new Response(Response.Code.GENERIC_ERROR,
                    e.getMessage());
        }
        
        return response;
    }

    /**
     * Actually lock or unlock cart from the current order.
     * 
     * @param orderJson
     *            current order
     * @param cartId
     *            cart id to remove
     * @return the new order
     */
    private JSONObject lockUnlockCart(final JSONObject orderJson,
            final long cartId) {
        JSONArray cartsJson = orderJson.getJSONArray(Constants.CARTS);
        JSONObject newOrderJson = JSONFactoryUtil.createJSONObject();

        for (int i = 0; i < cartsJson.length(); i++) {
            JSONObject cartJson = cartsJson.getJSONObject(i);
            if (!newOrderJson.has(Constants.CARTS)) {
                newOrderJson.put(Constants.CARTS,
                        JSONFactoryUtil.createJSONArray());
            }
            if (cartJson.getInt(Constants.ID) == cartId) {
                cartJson.put(Constants.CLOSED,
                        !cartJson.getBoolean(Constants.CLOSED));
            }
            newOrderJson.getJSONArray(Constants.CARTS).put(cartJson);

        }
        newOrderJson.put(Constants.ROWS_NUMBER,
                orderJson.getInt(Constants.ROWS_NUMBER));
        newOrderJson.put(Constants.DELIVERY_DATE,
                orderJson.getLong(Constants.DELIVERY_DATE));
        newOrderJson.put(Constants.CREATION_DATE,
                orderJson.getLong(Constants.CREATION_DATE));
        newOrderJson.put(Constants.ID, orderJson.getLong(Constants.ID));
        newOrderJson.put(Constants.AGENT_ID,
                orderJson.getLong(Constants.AGENT_ID));
        newOrderJson.put(Constants.CUSTOMER_ID,
                orderJson.getLong(Constants.CUSTOMER_ID));
        newOrderJson.put(Constants.CART_TYPE_ID,
                orderJson.getLong(Constants.CART_TYPE_ID));
        newOrderJson.put(Constants.CARRIER_ID,
                orderJson.getLong(Constants.CARRIER_ID));
        newOrderJson.put(Constants.AMOUNT,
                orderJson.getDouble(Constants.AMOUNT));
        newOrderJson.put(Constants.LOADING_PLACE,
                orderJson.getString(Constants.LOADING_PLACE));
        newOrderJson.put(Constants.NOTE, orderJson.getString(Constants.NOTE));
        newOrderJson.put(Constants.CUSTOMER_NOTE,
                orderJson.getString(Constants.CUSTOMER_NOTE));
        newOrderJson.put(Constants.ROWS_NUMBER,
                orderJson.getInt(Constants.ROWS_NUMBER));
        newOrderJson.put(Constants.STATE, orderJson.getString(Constants.STATE));
        return newOrderJson;
    }
    
    /**
     * Actually removes cart from the current order.
     * 
     * @param orderJson
     *            current order
     * @param cartId
     *            cart id to remove
     * @return the new order
     */
    private JSONObject deleteCart(final JSONObject orderJson,
            final long cartId) {
        double price = 0.0;
        JSONArray cartsJson = orderJson.getJSONArray(Constants.CARTS);
        JSONObject newOrderJson = JSONFactoryUtil.createJSONObject();

        for (int i = 0; i < cartsJson.length(); i++) {
            JSONObject cartJson = cartsJson.getJSONObject(i);
            if (!newOrderJson.has(Constants.CARTS)) {
                newOrderJson.put(Constants.CARTS,
                        JSONFactoryUtil.createJSONArray());
            }
            if (cartJson.getInt(Constants.ID) != cartId) {
                newOrderJson.getJSONArray(Constants.CARTS).put(cartJson);
            } else {
                price = cartJson.getInt(Constants.AMOUNT);
            }

        }
        newOrderJson.put(Constants.ROWS_NUMBER,
                orderJson.getInt(Constants.ROWS_NUMBER));
        newOrderJson.put(Constants.DELIVERY_DATE,
                orderJson.getLong(Constants.DELIVERY_DATE));
        newOrderJson.put(Constants.CREATION_DATE,
                orderJson.getLong(Constants.CREATION_DATE));
        newOrderJson.put(Constants.ID, orderJson.getLong(Constants.ID));
        newOrderJson.put(Constants.AGENT_ID,
                orderJson.getLong(Constants.AGENT_ID));
        newOrderJson.put(Constants.CUSTOMER_ID,
                orderJson.getLong(Constants.CUSTOMER_ID));
        newOrderJson.put(Constants.CART_TYPE_ID,
                orderJson.getLong(Constants.CART_TYPE_ID));
        newOrderJson.put(Constants.CARRIER_ID,
                orderJson.getLong(Constants.CARRIER_ID));
        newOrderJson.put(Constants.AMOUNT,
                orderJson.getDouble(Constants.AMOUNT) - price);
        newOrderJson.put(Constants.LOADING_PLACE,
                orderJson.getString(Constants.LOADING_PLACE));
        newOrderJson.put(Constants.NOTE, orderJson.getString(Constants.NOTE));
        newOrderJson.put(Constants.CUSTOMER_NOTE,
                orderJson.getString(Constants.CUSTOMER_NOTE));
        newOrderJson.put(Constants.ROWS_NUMBER,
                orderJson.getInt(Constants.ROWS_NUMBER));
        newOrderJson.put(Constants.STATE, orderJson.getString(Constants.STATE));
        return newOrderJson;
    }
}
