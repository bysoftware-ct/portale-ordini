package it.bysoftware.ct.scheduler.service;

import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Company;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.util.ContentUtil;
import com.liferay.util.portlet.PortletProps;

import it.bysoftware.ct.model.AnagraficheClientiFornitori;
import it.bysoftware.ct.model.Pagamento;
import it.bysoftware.ct.model.Rubrica;
import it.bysoftware.ct.model.SpettanzeSoci;
import it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.PagamentoLocalServiceUtil;
import it.bysoftware.ct.service.RubricaLocalServiceUtil;
import it.bysoftware.ct.service.SpettanzeSociLocalServiceUtil;
import it.bysoftware.ct.service.persistence.PagamentoPK;
import it.bysoftware.ct.utils.PaymentState;
import it.bysoftware.ct.utils.Report;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;

/**
 * @author Aliseo-G
 *
 */
public final class PaymentBoardService {
    
	/**
     * Email from.
     */
    private static final String FROM = "mail-from";
    
    /**
     * Export folder path.
     */
    private static final String EXPORT_FOLDER = PortletProps.get(
            "export-folder");
    
    /**
     * Export folder path.
     */
    private static final String DOCUMETS_FOLDER = EXPORT_FOLDER + File.separator
            + "SCHEDE_MENSILI" + File.separator;

    /**
     * Exported order file name.
     */
    private static final String VIRTUAL_HOST = PortletProps.get("virtual-host");
    
    /**
     * Email list.
     */
    private static final String CC_LIST = "cc-list";
    
    /**
     * PaymentBoardService unique instance.
     */
    private static PaymentBoardService instance = null;

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(PaymentBoardService.class);

    /**
     * 
     */
    private PaymentBoardService() {
    }

    /**
     * Returns the PaymentBoardservice instance.
     * 
     * @return the PaymentBoardservice instance
     */
    public static PaymentBoardService getInstance() {
        if (instance == null) {
            instance = new PaymentBoardService();
        }
        return instance;
    }

    /**
     * Sends payment board to the partner.
     * 
     * @param payment
     *            payment selected
     */
    public void sendPaymentBoard(final Pagamento payment) {
        try {
            double prevBalance = 0;
            Pagamento lastPayment = null;
            if (payment.getId() > 1) {
                lastPayment = PagamentoLocalServiceUtil
                        .fetchPagamento(new PagamentoPK(payment.getId() - 1,
                                payment.getAnno(),
                                payment.getCodiceSoggetto()));
            } else {
                lastPayment = PagamentoLocalServiceUtil
                        .fetchPagamento(new PagamentoPK(Calendar.DECEMBER + 1,
                                payment.getAnno() - 1, payment
                                        .getCodiceSoggetto()));
            }
            if (lastPayment != null
                    && lastPayment.getStato() == PaymentState.PAYED.getVal()) {
                prevBalance = lastPayment.getSaldo();
            }

            this.sendEmail(payment.getCodiceSoggetto(), payment.getCredito(),
                    prevBalance);
            payment.setStato(PaymentState.SENT.getVal());
            List<SpettanzeSoci> list = SpettanzeSociLocalServiceUtil
                    .findByPartnerPaymentYearPaymentId(payment.getCodiceSoggetto(),
                            payment.getAnno(), payment.getId());
            for (SpettanzeSoci entry : list) {
                entry.setStato(PaymentState.SENT.getVal());
                SpettanzeSociLocalServiceUtil.updateSpettanzeSoci(entry);
            }

            PagamentoLocalServiceUtil.updatePagamento(payment);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (PortalException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (NamingException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (AddressException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (JRException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Generates and send the payment board report by email.
     *
     * @param partnerCode
     *            partner code
     * @param credit
     *            credit
     * @param prevBalance
     *            previous balance
     * @throws NamingException
     *             exception
     * @throws SQLException
     *             exception
     * @throws PortalException
     *             exception
     * @throws SystemException
     *             exception
     * @throws AddressException
     *             exception
     * @throws JRException
     *             exception
     * @throws IOException exception
     */
    private void sendEmail(final String partnerCode, final double credit,
            final double prevBalance) throws NamingException, SQLException,
            PortalException, SystemException, AddressException, JRException,
            IOException {
        Context ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("jdbc/OrdiniPool");
        Connection c = ds.getConnection();
        Report r = new Report(c);
        Company company = CompanyLocalServiceUtil.getCompanyByVirtualHost(
                VIRTUAL_HOST);
        File f = r.printPaymentBoard(partnerCode, credit, prevBalance,
                company.getCompanyId(), "payment-board");
        
        String dateStr = new SimpleDateFormat("yyyyMMdd").format(new Date());
        File pdf = new File(DOCUMETS_FOLDER 
                + partnerCode + '_' + "SCHEDA_MENSILE" + '_' + dateStr
                + ".pdf");
        FileUtil.copyFile(f, pdf);
        
        AnagraficheClientiFornitori partner =
                AnagraficheClientiFornitoriLocalServiceUtil.
                    getAnagraficheClientiFornitori(partnerCode);
        List<Rubrica> contacts = RubricaLocalServiceUtil.
                findContacts(partnerCode); 
        MailMessage mail = new MailMessage();
        String from = PortletProps.get(FROM);
        if (Validator.isEmailAddress(from)) {
        	mail.setFrom(new InternetAddress(from));
		} else {
			mail.setFrom(new InternetAddress(company.getEmailAddress()));
		}
        InternetAddress[] addresses = new InternetAddress[contacts.size()];
        for (int i = 0; i < contacts.size(); i++) {
            InternetAddress address = new InternetAddress(contacts.get(i).
                    getEMailTo());
            addresses[i] = address;
        }
        mail.setTo(addresses);
        if (PortletProps.get(CC_LIST) != null
                && !PortletProps.get(CC_LIST).isEmpty()) {
            String[] emails = PortletProps.get(CC_LIST).split(",");
            addresses = new InternetAddress[emails.length];
            for (int i = 0; i < emails.length; i++) {
                InternetAddress address = new InternetAddress(emails[i]);
                addresses[i] = address;
            }
            mail.setCC(addresses);
        }
        
        String body = ContentUtil.get("/content/IT/payment-board.tmpl", true);
        body = StringUtil.replace(
                body,
                new String[] { "[$SUPPLIER$]"},
                new String[] { partner.getRagioneSociale1() + " "
                        + partner.getRagioneSociale2() });
        mail.setSubject("Scheda Pagamenti: " + partner.getRagioneSociale1());
        mail.setBody(body);
        mail.addFileAttachment(f);
        MailServiceUtil.sendEmail(mail);
        c.close();
    }
}
