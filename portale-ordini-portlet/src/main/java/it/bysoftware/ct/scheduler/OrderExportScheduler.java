package it.bysoftware.ct.scheduler;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.util.portlet.PortletProps;

import it.bysoftware.ct.model.Ordine;
import it.bysoftware.ct.scheduler.service.OrderExporterService;
import it.bysoftware.ct.service.OrdineLocalServiceUtil;
import it.bysoftware.ct.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * This class implements the scheduler logic to export orders.
 * 
 * @author Mario Torrisi torrisi.mario@gmail.com
 *
 */
public class OrderExportScheduler implements MessageListener {

    /**
     * Export folder path.
     */
    private static final String EXPORT_FOLDER = PortletProps.get(
            "export-folder");
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(OrderExportScheduler.class);

    @Override
    public final void receive(final Message message)
            throws MessageListenerException {
        this.logger.info("Order export scheduler executing...");
        File exportFolder = new File(EXPORT_FOLDER);
        Ordine[] exportedOrders;
        try {
            if (!exportFolder.exists()) {
                throw new IOException("Export folder doesn't exist.");
            }

            List<Ordine> orders = OrdineLocalServiceUtil.
                    getTodayConfirmedOrders();
            Context ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("jdbc/OrdiniPool");
            Connection c = ds.getConnection();
            OrderExporterService service = OrderExporterService.getInstance();
            exportedOrders = service.export(c, exportFolder, orders);
            
            
            Utils.updateCheckFile(exportFolder, exportedOrders, "ORDERS");
        } catch (IOException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (PortalException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (NamingException e) {
            this.logger.fatal(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            this.logger.fatal(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
    }
}
