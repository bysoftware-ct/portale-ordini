package it.bysoftware.ct.scheduler;

import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.util.ContentUtil;
import com.liferay.util.portlet.PortletProps;

import it.bysoftware.ct.model.AnagraficheClientiFornitori;
import it.bysoftware.ct.model.Rubrica;
import it.bysoftware.ct.model.Stati;
import it.bysoftware.ct.model.TestataDocumento;
import it.bysoftware.ct.service.RubricaLocalServiceUtil;
import it.bysoftware.ct.service.StatiLocalServiceUtil;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/**
 * @author Aliseo-G
 *
 */
public class MailSender {

	/**
     * Email from.
     */
    private static final String FROM = "mail-from";
    
    /**
     * Date formatter.
     */
    private static final SimpleDateFormat DATEFORMATTER = new SimpleDateFormat(
            "dd/MM/yyy");
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(MailSender.class);

    /**
     * Arrays of attachments.
     */
    private File[] attachments;

    /**
     * Recipient.
     */
    private AnagraficheClientiFornitori recipient;

    /**
     * Sender.
     */
    private String sender;

    /**
     * Document.
     */
    private TestataDocumento document;

    /**
     * CC email addresses.
     */
    private InternetAddress[] ccList;

    /**
     * Returns the attachments.
     * 
     * @return the attachments
     */
    public final File[] getAttachments() {
        return this.attachments;
    }

    /**
     * Sets the array of attachments.
     * 
     * @param list
     *            the attachments to set
     */
    public final void setAttachments(final File[] list) {
        this.attachments = list;
    }

    /**
     * Sets the mail recipient.
     * 
     * @param r
     *            the recipient to set
     */
    public final void setRecipient(final AnagraficheClientiFornitori r) {
        this.recipient = r;
    }

    /**
     * Sets the sender's email address.
     * 
     * @param string
     *            the sender to set
     */
    public final void setSender(final String string) {
        this.sender = string;
    }

    /**
     * Returns the document.
     * 
     * @return the document
     */
    public final TestataDocumento getDocument() {
        return this.document;
    }

    /**
     * Sets the document.
     * 
     * @param d
     *            the document to set
     */
    public final void setDocument(final TestataDocumento d) {
        this.document = d;
    }

    /**
     * Sets the array of {@link InternetAddress} for email cc.
     * 
     * @param list
     *            the list on cc's email.
     */
    public final void setCCList(final InternetAddress[] list) {
  this.ccList = list;
    }
    
    /**
     * Sends the confirmation.
     * 
     * @param type
     *            document type
     * @throws AddressException
     *             exception
     * @throws SystemException
     *             exception
     * @throws PortalException
     *             exception
     */
    public final void send(final String type) throws AddressException,
    PortalException, SystemException {
        this.send(type, false);
    }
    
    /**
     * Sends the confirmation.
     * 
     * @param type
     *            document type
     * @param sent
     *         true if document was already sent, false otherwise
     * @throws AddressException
     *             exception
     * @throws SystemException
     *             exception
     * @throws PortalException
     *             exception
     */
    public final void send(final String type, final boolean sent)
            throws AddressException, PortalException, SystemException {
        Stati s = StatiLocalServiceUtil
                .getStati(this.recipient.getSiglaStato());

        String body = "";
        if ("IT".equals(s.getCodiceISO())) {
            if (sent) {
                body = ContentUtil.get("/content/IT/" + type + "-edited.tmpl",
                        true);
            } else {
                body = ContentUtil.get("/content/IT/" + type + ".tmpl", true);
            }
        } else {
            if (sent) {
                body = ContentUtil.get("/content/EN/" + type + "-edited.tmpl",
                        true);
            } else {
                body = ContentUtil.get("/content/EN/" + type + ".tmpl", true);
            }
        }
        body = StringUtil.replace(
                body,
                new String[] { "[$SUPPLIER$]", "[$NUMBER$]", "[$DATE$]" },
                new String[] {
                        this.recipient.getRagioneSociale1(),
                        this.document.getProtocollo() + " / "
                                + this.document.getCodiceCentro(),
                        DATEFORMATTER.format(this.document
                                .getDataRegistrazione()) });

        MailMessage mailMessage = new MailMessage();
        mailMessage.setBody(body);

        mailMessage.setSubject(this.document.getTipoDocumento() + " N°: "
                + this.document.getProtocollo() + " / "
                + this.document.getCodiceCentro());
        String from = PortletProps.get(FROM);
        if (Validator.isEmailAddress(from)) {
        	mailMessage.setFrom(new InternetAddress(from));
		} else {
			mailMessage.setFrom(new InternetAddress(this.sender));
		}
        List<Rubrica> contacts = RubricaLocalServiceUtil
                .findContacts(this.document.getCodiceFornitore());
        List<InternetAddress> addresses = new ArrayList<InternetAddress>();
        for (Rubrica contact : contacts) {
            addresses.add(new InternetAddress(contact.getEMailTo()));
        }
        mailMessage.setTo(
                addresses.toArray(new InternetAddress[contacts.size()]));
        if (this.ccList != null) {
            mailMessage.setCC(this.ccList);
        }
        for (int i = 0; i < attachments.length; i++) {
            if (i == 0) {
                mailMessage.addFileAttachment(this.attachments[i],
                        "documenti.txt");
            } else {                
                mailMessage.addFileAttachment(this.attachments[i]);
            }
        }
        MailServiceUtil.sendEmail(mailMessage);
        for (int i = 0; i < mailMessage.getTo().length; i++) {
            this.logger.debug("SENT EMAIL TO: " + mailMessage.getTo()[i]);
        }
        
    }
}
