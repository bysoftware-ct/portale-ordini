package it.bysoftware.ct.scheduler;

import java.io.IOException;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

/**
 * Portlet implementation class OrderScheduler.
 */
public class OrderScheduler extends GenericPortlet {

    /**
     * View template.
     */
    private String viewTemplate;

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private static Log logger = LogFactoryUtil.getLog(OrderScheduler.class);
    
    /**
     * Init method.
     */
    public final void init() {
        viewTemplate = getInitParameter("view-template");
    }

    /**
     * Do view method.
     * 
     * @param renderRequest
     *            {@link RenderRequest}
     * @param renderResponse
     *            {@link RenderResponse}
     * 
     * @throws IOException
     *             exception
     * @throws PortletException
     *             exception
     */
    public final void doView(final RenderRequest renderRequest,
            final RenderResponse renderResponse) throws IOException,
            PortletException {

        include(viewTemplate, renderRequest, renderResponse);
    }

    /**
     * Include method.
     * 
     * @param path
     *            view file path
     * @param renderRequest
     *            {@link RenderRequest}
     * @param renderResponse
     *            {@link RenderResponse}
     * @throws IOException
     *             exception
     * @throws PortletException
     *             exception
     */
    protected final void include(final String path,
            final RenderRequest renderRequest,
            final RenderResponse renderResponse) throws IOException,
            PortletException {

        PortletRequestDispatcher portletRequestDispatcher = getPortletContext()
                .getRequestDispatcher(path);

        if (portletRequestDispatcher == null) {
            logger.error(path + " is not a valid include");
        } else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }

}
