package it.bysoftware.ct.scheduler;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.util.portlet.PortletProps;

import it.bysoftware.ct.scheduler.service.GiveDocumentExporterService;
import it.bysoftware.ct.service.persistence.TestataDocumentoPK;
import it.bysoftware.ct.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.mail.internet.AddressException;
import javax.naming.NamingException;

import net.sf.jasperreports.engine.JRException;

/**
 * @author Mario Torrisi
 *
 */
public class GiveDocumentExporterScheduler implements MessageListener {

    /**
     * Date formatter.
     */
    private static final SimpleDateFormat SDF = new SimpleDateFormat(
            "dd/MM/yyyy");
    
    /**
     * Export folder path.
     */     
    private static final String EXPORT_FOLDER = PortletProps.get(
            "export-folder");

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(
            GiveDocumentExporterScheduler.class);
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.liferay.portal.kernel.messaging.MessageListener#receive(com.liferay
     * .portal.kernel.messaging.Message)
     */
    @Override
    public final void receive(final Message message)
            throws MessageListenerException {
        this.logger.info("#### GIVE DOCUMENT EXPORTER EXECUTED AT: "
                + SDF.format(new Date()) + "####");

//        Calendar today = Utils.today();
        GiveDocumentExporterService exporterService =
                GiveDocumentExporterService.getInstance();
        try {
        TestataDocumentoPK[] documents = exporterService.
                createTodayGiveDocuments();
        File exportFolder = new File(EXPORT_FOLDER);
        
        Utils.updateCheckFile(exportFolder, documents,
                "GIVE DOCUMENTS CREATED");
        } catch (IOException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (PortalException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (AddressException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (NamingException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (JRException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
    }
 
}
