package it.bysoftware.ct.scheduler.service;

import it.bysoftware.ct.confirmation.AgentConfirmation;
import it.bysoftware.ct.confirmation.Confirmation;
import it.bysoftware.ct.confirmation.CustomerConfirmation;
import it.bysoftware.ct.confirmation.GTCustomerConfirmation;
import it.bysoftware.ct.model.AnagraficheClientiFornitori;
import it.bysoftware.ct.model.Carrello;
import it.bysoftware.ct.model.ClientiFornitoriDatiAgg;
import it.bysoftware.ct.model.Ordine;
import it.bysoftware.ct.model.Riga;
import it.bysoftware.ct.model.Rubrica;
import it.bysoftware.ct.orderexport.OrderExporter;
import it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.CarrelloLocalServiceUtil;
import it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil;
import it.bysoftware.ct.service.OrdineLocalServiceUtil;
import it.bysoftware.ct.service.RigaLocalServiceUtil;
import it.bysoftware.ct.service.RubricaLocalServiceUtil;
import it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK;
import it.bysoftware.ct.service.persistence.RubricaPK;
import it.bysoftware.ct.utils.OrderState;
import it.bysoftware.ct.utils.Utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.AddressException;
import javax.naming.NamingException;

import net.sf.jasperreports.engine.JRException;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

/**
 * @author Aliseo-G
 *
 */
public final class OrderExporterService {

	private static final String GT = PortletProps.get("gt");

	/**
	 * Exported order file name.
	 */
	private static final String EXPORTED_ORDERS = PortletProps
			.get("exported-orders");

	/**
	 * Exported order file name.
	 */
	private static final String VIRTUAL_HOST = PortletProps.get("virtual-host");

	/**
	 * ForeignInvoiceExporterService unique instance.
	 */
	private static OrderExporterService instance = new OrderExporterService();

	/**
	 * Logger object. Based on Liferay logger
	 * {@link com.liferay.portal.kernel.log.Log} commons logging.
	 */
	private Log logger = LogFactoryUtil.getLog(OrderExporterService.class);

	/**
	 * Order exporter service.
	 */
	private OrderExporterService() {
	}

	/**
	 * Returns the OrderExporterService instance.
	 * 
	 * @return the OrderExporterService instance
	 */
	public static OrderExporterService getInstance() {
		return instance;
	}

	/**
	 * Exports give invoice file for foreign orders.
	 * 
	 * @param c
	 *            database connection
	 * @param exportFolder
	 *            path to export folder
	 * @param orders
	 *            orders is going to be exported
	 * @return return an array of exported documents
	 * @throws SQLException
	 *             exception
	 * @throws IOException
	 *             exception
	 * @throws SystemException
	 *             exception
	 * @throws PortalException
	 *             exception
	 */
	public Ordine[] export(final Connection c, final File exportFolder,
			final List<Ordine> orders) throws SQLException, IOException,
			PortalException, SystemException {
		long userId = -1;
		Company company = CompanyLocalServiceUtil
				.getCompanyByVirtualHost(VIRTUAL_HOST);
		List<User> users = UserLocalServiceUtil.getCompanyUsers(company
				.getCompanyId(), 0, UserLocalServiceUtil
				.getCompanyUsersCount(company.getCompanyId()));
		for (User user : users) {
			List<Role> roles = user.getRoles();
			for (Role role : roles) {
				if (role.getName().equals("Azienda")) {
					userId = user.getUserId();
				}
			}
		}

		return this.export(c, exportFolder, orders, userId);
	}

	/**
	 * Exports give invoice file for foreign orders.
	 * 
	 * @param c
	 *            database connection
	 * @param exportFolder
	 *            path to export folder
	 * @param orders
	 *            orders is going to be exported
	 * @param userId
	 *            company user id
	 * @return return an array of exported documents
	 * @throws SQLException
	 *             exception
	 * @throws IOException
	 *             exception
	 */
	public Ordine[] export(final Connection c, final File exportFolder,
			final List<Ordine> orders, final long userId) throws SQLException,
			IOException {
		List<Ordine> expOrders = new ArrayList<Ordine>();
		File exportedOrder = new File(exportFolder + File.separator
				+ EXPORTED_ORDERS);
		FileWriter fw;
		BufferedWriter bw;
		PrintWriter out;
		List<Confirmation> confirmations = new ArrayList<Confirmation>();
		try {
			for (Ordine order : orders) {
				if (order.getDdtGenerati()) {
					expOrders.add(order);
					List<Carrello> carts = CarrelloLocalServiceUtil
							.findCartsInOrder(order.getId());
					Map<String, Riga> rowsWrapper = new HashMap<String, Riga>();
					for (Carrello cart : carts) {
						List<Riga> rows = RigaLocalServiceUtil
								.findRowsInCart(cart.getId());
						for (Riga r : rows) {
							String key = r.getIdArticolo() + "#"
									+ r.getCodiceVariante() + "#"
									+ r.getPrezzo();
							if (rowsWrapper.containsKey(key)) {
								rowsWrapper.get(key).setPianteRipiano(
										rowsWrapper.get(key).getPianteRipiano()
												+ r.getPianteRipiano());
								double amount = Utils
										.roundDouble(
												r.getPianteRipiano()
														* r.getPrezzo(), 2);
								rowsWrapper.get(key).setImporto(
										rowsWrapper.get(key).getImporto()
												+ amount);
							} else {
								rowsWrapper.put(key, r);
							}
						}
					}
					List<Riga> rows = new ArrayList<Riga>();
					for (String key : rowsWrapper.keySet()) {
						rows.add(rowsWrapper.get(key));
					}
					fw = new FileWriter(exportedOrder.getAbsoluteFile(), true);
					bw = new BufferedWriter(fw);
					out = new PrintWriter(bw);

					OrderExporter exporter = new OrderExporter(order, rows);

					out.print(exporter.exportDocument());
					out.close();
					User user = UserLocalServiceUtil.getUser(userId);
					AnagraficheClientiFornitori customer = AnagraficheClientiFornitoriLocalServiceUtil
							.getAnagraficheClientiFornitori(order
									.getIdCliente());
					Rubrica contact = RubricaLocalServiceUtil
							.getRubrica(new RubricaPK(customer
									.getCodiceAnagrafica(), 1));
					ClientiFornitoriDatiAgg cDetails = ClientiFornitoriDatiAggLocalServiceUtil
							.getClientiFornitoriDatiAgg(new ClientiFornitoriDatiAggPK(
									customer.getCodiceAnagrafica(), false));
					Confirmation confirmation;
					if (cDetails.getCategoriaEconomica().equals(GT)) {
						this.logger.info("Exporting order for a GT customer");
						confirmation = new GTCustomerConfirmation(
								user.getEmailAddress(), order,
								"customer-confirm", user.getScreenName(),
								userId, customer, contact.getEMailTo());
					} else {
						confirmation = new CustomerConfirmation(
								user.getEmailAddress(), order,
								"customer-confirm", user.getScreenName(),
								userId, customer, contact.getEMailTo());
					}
					confirmations.add(confirmation);
					order.setStato(OrderState.SCHEDULED.getValue());
					OrdineLocalServiceUtil.updateOrdine(order);
					if (!cDetails.getCodiceAgente().isEmpty()) {
						try {
							AnagraficheClientiFornitori agent = AnagraficheClientiFornitoriLocalServiceUtil
									.getAnagraficheClientiFornitori(cDetails
											.getCodiceAgente());
							Rubrica cAgent = RubricaLocalServiceUtil
									.fetchRubrica(new RubricaPK(agent
											.getCodiceAnagrafica(), 1));
							if (cAgent != null
									&& !cAgent.getEMailTo().isEmpty()) {
								confirmations.add(new AgentConfirmation(user
										.getEmailAddress(), order,
										"agent-confirm", user.getScreenName(),
										userId, agent, cAgent.getEMailTo(),
										customer.getRagioneSociale1()));
							}
						} catch (PortalException e) {
							this.logger.error(e.getMessage());
							if (this.logger.isDebugEnabled()) {
								e.printStackTrace();
							}
						} catch (SystemException e) {
							this.logger.error(e.getMessage());
							if (this.logger.isDebugEnabled()) {
								e.printStackTrace();
							}
						}
					}
				} else {
					this.logger.info("ORDER: " + order.getId() + " CURRENTLY"
							+ " NOT EXPORTED, GIVE DOCUMENT NOT READY YET.");
				}
			}
			for (int i = 0; i < confirmations.size(); i++) {
				confirmations.get(i).createConfirmation(c);
				confirmations.get(i).sendConfirmation();
			}
			c.close();
		} catch (PortalException e) {
			this.logger.error(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
		} catch (SystemException e) {
			this.logger.error(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
		} catch (NamingException e) {
			this.logger.error(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
		} catch (JRException e) {
			this.logger.error(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
		} catch (AddressException e) {
			this.logger.error(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
		}
		return expOrders.toArray(new Ordine[orders.size()]);
	}
}
