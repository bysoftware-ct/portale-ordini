package it.bysoftware.ct.scheduler.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.model.Company;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

import it.bysoftware.ct.model.ContatoreSocio;
import it.bysoftware.ct.model.Ordine;
import it.bysoftware.ct.model.RigoDocumento;
import it.bysoftware.ct.model.TestataDocumento;
import it.bysoftware.ct.orderexport.DocumentExporter;
import it.bysoftware.ct.orderexport.GiveInvoiceExporter;
import it.bysoftware.ct.orderexport.SellInvoiceExporter;
import it.bysoftware.ct.scheduler.MailSender;
import it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.ContatoreSocioLocalServiceUtil;
import it.bysoftware.ct.service.OrdineLocalServiceUtil;
import it.bysoftware.ct.service.RigoDocumentoLocalServiceUtil;
import it.bysoftware.ct.service.TestataDocumentoLocalServiceUtil;
import it.bysoftware.ct.service.persistence.ContatoreSocioPK;
import it.bysoftware.ct.service.persistence.RigoDocumentoPK;
import it.bysoftware.ct.service.persistence.TestataDocumentoPK;
import it.bysoftware.ct.utils.OrderState;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.mail.internet.AddressException;

import net.sf.jasperreports.engine.JRException;

/**
 * @author Aliseo-G
 *
 */
public final class ForeignInvoiceExporterService {
    
    /**
     * Default activity.
    
    private static final String DEFAULT_EMAIL = PortletProps.get(
            "company-email");
     */
    
    /**
     * Export folder path.
     */
    private static final String EXPORT_FOLDER = PortletProps.get(
            "export-folder");
    
    /**
     * Export folder path.
     */
    private static final String DOCUMETS_FOLDER = EXPORT_FOLDER
            + File.separator + "FATTURE_CONFERIMENTO" + File.separator;
    
    /**
     * Exported order file name.
     */
    private static final String EXPORTED_ORDERS = PortletProps.get(
            "exported-orders");
    
    /**
     * Default activity.
     */
    private static final String GIVE_DOCUMENT = PortletProps.get(
            "give-document");
    /**
     * Centre code for Italian document.
     */
    private static final String IT_CENTER_CODE = PortletProps.get(
            "it-center-code");
    
    /**
     * Sell invoice type for the partner (partner --> company).
     */
    private static final String SELL_INVOICE = PortletProps.get("sell-invoice");
    
    /**
     * Exported order file name.
     */
    private static final String VIRTUAL_HOST = PortletProps.get(
            "virtual-host");

    /** 
     * Date formatter.
     */
    private static final SimpleDateFormat SDF = new SimpleDateFormat(
            "dd/MM/yyyy");
    
    /**
     * ForeignInvoiceExporterService unique instance.
     */
    private static ForeignInvoiceExporterService instance = null;
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(
            ForeignInvoiceExporterService.class);
    
    /**
     * 
     */
    private ForeignInvoiceExporterService() {
    }

    /**
     * Returns the ForeignInvoiceExporterService instance.
     * 
     * @return the ForeignInvoiceExporterService instance
     */
    public static ForeignInvoiceExporterService getInstance() {
        if (instance == null) {
            instance = new ForeignInvoiceExporterService();
        }
        return instance;
    }

    /**
     * Converts and stores foreign order's transport document to invoice.
     * 
     * @param day
     *            today date
     * @param orderId
     *            the order for which the documents should be created, 0 to
     *            create invoice for all orders.
     */
    public void convertDocument2Invoice(final Calendar day,
            final long orderId) {
        List<TestataDocumento> documents = new ArrayList<TestataDocumento>();
        if (orderId > 0) {
            documents = TestataDocumentoLocalServiceUtil
                    .findDocumentsByOrderId(orderId);
        } else {
            documents = TestataDocumentoLocalServiceUtil.
                    findTodayDocuments(day.getTime(), GIVE_DOCUMENT);
        }
        List<TestataDocumento> list = new ArrayList<TestataDocumento>();
		for (TestataDocumento document : documents) {
			if (!document.getStato()) {

				Ordine order = null;
				try {
					order = OrdineLocalServiceUtil.fetchOrdine(document
							.getLibLng1());
				} catch (SystemException e) {
					this.logger.error(e.getMessage());
					if (this.logger.isDebugEnabled()) {
						e.printStackTrace();
					}
				}
				if (order != null
						&& !order.getCentro().equals(IT_CENTER_CODE)
						&& order.getDataConsegna().compareTo(day.getTime()) == 0
						&& order.getStato() == OrderState.SCHEDULED.getValue())
				{
					list.add(document);
				}
			}
		}
        try {
            for (TestataDocumento doc : list) {
                TestataDocumento invoice = this.createInvoiceHeader(doc,
                        day.getTime());
                List<RigoDocumento> rows = RigoDocumentoLocalServiceUtil.
                        findByHeaderItem(doc.getPrimaryKey());
                int index = 1;
                RigoDocumentoLocalServiceUtil.updateRigoDocumento(
                        this.createInvoiceRow(invoice, index, doc));
                index++;
                for (RigoDocumento row : rows) {
                    RigoDocumentoLocalServiceUtil.updateRigoDocumento(
                            this.createInvoiceRow(invoice, index, row));
                    index++;
                }
                doc.setStato(true);
                TestataDocumentoLocalServiceUtil.updateTestataDocumento(doc);
                TestataDocumentoLocalServiceUtil.updateTestataDocumento(
                        invoice);
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Creates a descriptive row with the give document reference.
     * 
     * @param invoiceHeader
     *            the invoice header
     * @param rowIndex
     *            the row index
     * @param doc
     *            the give document
     * @return the new row
     */
    private RigoDocumento createInvoiceRow(final TestataDocumento invoiceHeader,
            final int rowIndex, final TestataDocumento doc) {
        RigoDocumento invoiceRow = RigoDocumentoLocalServiceUtil.
                createRigoDocumento(new RigoDocumentoPK(
                        invoiceHeader.getAnno(),
                        invoiceHeader.getCodiceAttivita(),
                        invoiceHeader.getCodiceCentro(),
                        invoiceHeader.getCodiceDeposito(),
                        invoiceHeader.getProtocollo(),
                        invoiceHeader.getCodiceFornitore(), rowIndex,
                        invoiceHeader.getTipoDocumento()));
        String s = "Rif. DDT N: " + doc.getProtocollo() + "/"
                        + doc.getCodiceCentro() + " del: "
                        + SDF.format(doc.getDataRegistrazione());
        
        invoiceRow.setDescrizione(s);        
        invoiceRow.setTipoRigo(2);
        
        return invoiceRow;
    }

    /**
     * Creates the new invoice row.
     * 
     * @param invoiceHeader
     *            the invoice header reference
     * @param rowIndex
     *            the row index
     * @param row
     *            the invoice row
     * @return the new row
     */
    private RigoDocumento createInvoiceRow(
            final TestataDocumento invoiceHeader, final int rowIndex,
            final RigoDocumento row) {
        RigoDocumento invoiceRow = RigoDocumentoLocalServiceUtil.
                createRigoDocumento(new RigoDocumentoPK(invoiceHeader
                        .getAnno(), invoiceHeader.getCodiceAttivita(),
                        invoiceHeader.getCodiceCentro(),
                        invoiceHeader.getCodiceDeposito(),
                        invoiceHeader.getProtocollo(),
                        invoiceHeader.getCodiceFornitore(), rowIndex,
                        invoiceHeader.getTipoDocumento()));
        invoiceRow.setCodiceArticolo(row.getCodiceArticolo());
        invoiceRow.setCodiceIVA(row.getCodiceIVA());
        invoiceRow.setCodiceVariante(row.getCodiceVariante());
        invoiceRow.setDescrizione(row.getDescrizione());
        invoiceRow.setImportoNetto(row.getImportoNetto());
        invoiceRow.setLibLng1(row.getLibLng1());
        invoiceRow.setLibStr1(row.getLibStr1());
        invoiceRow.setLibStr2(row.getLibStr2());
        invoiceRow.setPrezzo(row.getPrezzo());
        invoiceRow.setQuantita(row.getQuantita());
        invoiceRow.setSconto1(row.getSconto1());
        invoiceRow.setSconto2(row.getSconto2());
        invoiceRow.setSconto3(row.getSconto3());
        invoiceRow.setTipoRigo(row.getTipoRigo());
        
        return invoiceRow;
    }

    /**
     * Creates the invoice header.
     * 
     * @param doc
     *            first document from where get the invoice information.
     * @param regDate
     *            registration date
     * @return the invoice header
     */
    private TestataDocumento createInvoiceHeader(final TestataDocumento doc,
            final Date regDate) {
        int num = 0;
        ContatoreSocio counter = null;
        try {
            counter = ContatoreSocioLocalServiceUtil.
                    fetchContatoreSocio(new ContatoreSocioPK(doc.getAnno(),
                            doc.getCodiceFornitore(), SELL_INVOICE));
            if (counter == null) {
                num = TestataDocumentoLocalServiceUtil
                        .getNumber(doc.getAnno(), doc.getCodiceAttivita(),
                                doc.getCodiceCentro(), doc.getCodiceDeposito(),
                                doc.getCodiceFornitore(), SELL_INVOICE);
            } else {
                num = counter.getNumero();
                ContatoreSocioLocalServiceUtil.deleteContatoreSocio(counter);
            } 
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

        TestataDocumento invoiceHeader = TestataDocumentoLocalServiceUtil
                .createTestataDocumento(new TestataDocumentoPK(doc.getAnno(),
                        doc.getCodiceAttivita(), doc.getCodiceCentro(), doc
                                .getCodiceDeposito(), num, doc
                                .getCodiceFornitore(), SELL_INVOICE));
        invoiceHeader
                .setCodiceAspettoEsteriore(doc.getCodiceAspettoEsteriore());
        invoiceHeader.setCodicePorto(doc.getCodicePorto());
        invoiceHeader.setCodiceVettore1(doc.getCodiceVettore1());
        invoiceHeader.setCodiceVettore2(doc.getCodiceVettore2());
        invoiceHeader.setDataRegistrazione(regDate);
        invoiceHeader.setLibLng1(doc.getLibLng1());
        invoiceHeader.setLibStr1(doc.getLibStr1());
        this.logger.debug("DOC1: " + doc.getLibStr1());
        this.logger.debug("INVOICE1: " + invoiceHeader.getLibStr1());
        invoiceHeader.setLibStr2(doc.getLibStr2());
        this.logger.debug("INVOICE2 " + invoiceHeader.getLibStr2());
        invoiceHeader.setCodiceConsorzio(doc.getCodiceConsorzio());
        return invoiceHeader;
    }

    /**
     * Exports give invoice file for foreign orders.
     * 
     * @param c
     *            database connection
     * @param exportFolder
     *            path to export folder
     * @param today
     *            today date
     * @return return an array of exported documents
     * @throws SQLException
     *             exception
     * @throws IOException
     *             exception
     */
    public TestataDocumentoPK[] export(final Connection c,
            final File exportFolder, final Calendar today) throws SQLException,
            IOException {
        TestataDocumentoPK[] exportedInvoices = new TestataDocumentoPK[0];
        List<TestataDocumento> invoices = TestataDocumentoLocalServiceUtil.
                findTodayDocuments(today.getTime(), SELL_INVOICE);
        if (invoices.size() > 0) {
            exportedInvoices = new TestataDocumentoPK[invoices.size()];
        }
        
        
        File exportedOrder = new File(exportFolder + File.separator
                + EXPORTED_ORDERS);
        FileWriter fw;
        BufferedWriter bw;
        PrintWriter out;
        int i = 0;
        for (TestataDocumento invoice : invoices) {
            exportedInvoices[i] = invoice.getPrimaryKey();
            List<RigoDocumento> rows = RigoDocumentoLocalServiceUtil.
                    findByHeaderItem(invoice.getPrimaryKey());
            try {
                fw = new FileWriter(exportedOrder.getAbsoluteFile(),
                        true);
                bw = new BufferedWriter(fw);
                out = new PrintWriter(bw);
                DocumentExporter exporter =
                        new GiveInvoiceExporter(invoice, rows, c);
                out.print(exporter.exportDocument());
                out.close();
                
                exporter = new SellInvoiceExporter(invoice, rows, c);
                
                File doc = FileUtil.createTempFile("txt");
                fw = new FileWriter(doc.getAbsoluteFile());
                bw = new BufferedWriter(fw);
                out = new PrintWriter(bw);
                out.print(exporter.exportDocument());
                out.close();
                bw.close();
                Company company = CompanyLocalServiceUtil.
                        getCompanyByVirtualHost(VIRTUAL_HOST);
                exporter.createReport(company.getCompanyId(),
                        "partner_sell-invoice");
                MailSender sender = new MailSender();
                sender.setAttachments(
                        new File[] {doc, exporter.getReport() });
                sender.setSender(company.getEmailAddress());
                sender.setRecipient(AnagraficheClientiFornitoriLocalServiceUtil.
                        getAnagraficheClientiFornitori(exporter.
                                getDocument().getCodiceFornitore()));
                sender.setDocument(exporter.getDocument());
                sender.send("partner_sell-invoice");
                String dateStr = new SimpleDateFormat("yyyyMMdd").format(
                        invoice.getDataRegistrazione());
                File pdf = new File(DOCUMETS_FOLDER 
                        + invoice.getCodiceFornitore() + '_'
                        + invoice.getTipoDocumento() + '_'
                        + invoice.getProtocollo() + invoice.getCodiceCentro() 
                        + '_' + dateStr + ".pdf");
                FileUtil.copyFile(exporter.getReport(), pdf);
                invoice.setStato(true);
                
                TestataDocumentoLocalServiceUtil.updateTestataDocumento(
                        invoice);
                i++;
            } catch (PortalException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
            } catch (SystemException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
            } catch (AddressException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
            } catch (JRException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
            }
        }
        return exportedInvoices;
    }

}
