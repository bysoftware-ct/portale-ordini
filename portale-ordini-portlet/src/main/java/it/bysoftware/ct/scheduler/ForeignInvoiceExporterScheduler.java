package it.bysoftware.ct.scheduler;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.util.portlet.PortletProps;

import it.bysoftware.ct.scheduler.service.ForeignInvoiceExporterService;
import it.bysoftware.ct.service.persistence.TestataDocumentoPK;
import it.bysoftware.ct.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * @author Mario Torrisi
 *
 */
public class ForeignInvoiceExporterScheduler implements MessageListener {

    /**
     * Date formatter.
     */
    private static final SimpleDateFormat SDF = new SimpleDateFormat(
            "dd/MM/yyyy");
    
    /**
     * Export folder path.
     */
    private static final String EXPORT_FOLDER = PortletProps.get(
            "export-folder");
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(
            ForeignInvoiceExporterScheduler.class);
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.liferay.portal.kernel.messaging.MessageListener#receive(com.liferay
     * .portal.kernel.messaging.Message)
     */
    @Override
    public final void receive(final Message message)
            throws MessageListenerException {
        this.logger.info("#### FOREIGN INVOICE EXPORTER EXECUTED AT: "
                + SDF.format(new Date()) + "####");

        Calendar today = Utils.today();
        ForeignInvoiceExporterService exporterService =
                ForeignInvoiceExporterService.getInstance();
        exporterService.convertDocument2Invoice(today, 0);
        Context ctx;
        try {
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("jdbc/OrdiniPool");
            Connection c = ds.getConnection();
            File exportFolder = new File(EXPORT_FOLDER);
            TestataDocumentoPK[] exportedInvoices = exporterService.export(c,
                    exportFolder, today);
            c.close();
            Utils.updateCheckFile(exportFolder, exportedInvoices,
                    "FOREIGN INVOICES");
        } catch (NamingException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }

    }
 
}
