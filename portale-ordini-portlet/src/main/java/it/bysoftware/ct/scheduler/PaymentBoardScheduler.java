package it.bysoftware.ct.scheduler;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.util.portlet.PortletProps;

import it.bysoftware.ct.model.Pagamento;
import it.bysoftware.ct.scheduler.service.PaymentBoardService;
import it.bysoftware.ct.service.PagamentoLocalServiceUtil;
import it.bysoftware.ct.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Aliseo-G
 *
 */
public class PaymentBoardScheduler implements MessageListener {

    /**
     * Date formatter.
     */
    private static final SimpleDateFormat SDF = new SimpleDateFormat(
            "dd/MM/yyyy");
    
    /**
     * Export folder path.
     */
    private static final String EXPORT_FOLDER = PortletProps.get(
            "export-folder");
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(PaymentBoardScheduler.class);

    @Override
    public final void receive(final Message message)
            throws MessageListenerException {
        this.logger.info("#### PAYMENT BOARD SENDER EXECUTED AT: "
                + SDF.format(new Date()) + "####");
        
        try {
            
            List<Pagamento> payments = PagamentoLocalServiceUtil.
                    findByYearAndState(Utils.getYear(), 0);
            
            PaymentBoardService service = PaymentBoardService.getInstance();
            Object[] obj = new Object[payments.size()];
            int i = 0;
            for (Pagamento payment : payments) {
                service.sendPaymentBoard(payment);
                obj[i] = payment.getPrimaryKeyObj();
            }
            File exportFolder = new File(EXPORT_FOLDER);
            Utils.updateCheckFile(exportFolder, obj, "PAYMENTS");
        } catch (IOException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
        }
        
    }

}
