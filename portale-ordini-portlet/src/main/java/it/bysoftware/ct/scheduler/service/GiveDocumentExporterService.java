package it.bysoftware.ct.scheduler.service;

import it.bysoftware.ct.model.AnagraficheClientiFornitori;
import it.bysoftware.ct.model.Articoli;
import it.bysoftware.ct.model.Carrello;
import it.bysoftware.ct.model.ClientiFornitoriDatiAgg;
import it.bysoftware.ct.model.CodiceConsorzio;
import it.bysoftware.ct.model.Ordine;
import it.bysoftware.ct.model.Piante;
import it.bysoftware.ct.model.Riga;
import it.bysoftware.ct.model.RigoDocumento;
import it.bysoftware.ct.model.TestataDocumento;
import it.bysoftware.ct.orderexport.TransportDocumentExporter;
import it.bysoftware.ct.scheduler.MailSender;
import it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.ArticoliLocalServiceUtil;
import it.bysoftware.ct.service.CarrelloLocalServiceUtil;
import it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil;
import it.bysoftware.ct.service.CodiceConsorzioLocalServiceUtil;
import it.bysoftware.ct.service.OrdineLocalServiceUtil;
import it.bysoftware.ct.service.PianteLocalServiceUtil;
import it.bysoftware.ct.service.RigaLocalServiceUtil;
import it.bysoftware.ct.service.RigoDocumentoLocalServiceUtil;
import it.bysoftware.ct.service.TestataDocumentoLocalServiceUtil;
import it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK;
import it.bysoftware.ct.service.persistence.RigoDocumentoPK;
import it.bysoftware.ct.service.persistence.TestataDocumentoPK;
import it.bysoftware.ct.utils.OrderState;
import it.bysoftware.ct.utils.Utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.internet.AddressException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.model.Company;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

/**
 * @author Aliseo-G
 *
 */
public final class GiveDocumentExporterService {
    
    /**
     * Default activity.
     
    private static final String DEFAULT_EMAIL = PortletProps.get(
            "company-email");
    */
    /**
    * Default activity.
    */
   private static final String DEFAULT_ACTIVITY_CODE = PortletProps.get(
           "activity-code");

   /**
    * Default activity.
    */
   private static final String DEFAULT_CENTER_CODE = PortletProps.get(
           "center-code");
   
   /**
    * Default activity.
    */
   private static final String IT_CENTER_CODE = PortletProps.get(
           "it-center-code");
   
   /**
    * Default activity.
    */
   private static final String IT_PORT_CODE = PortletProps.get(
           "it-port-code");
   
   /**
    * Default activity.
    */
   private static final String IT_TRANS_CODE = PortletProps.get(
           "it-trans-code");
   
   /**
    * Default activity.
    */
   private static final String FOREIGN_PORT_CODE = PortletProps.get(
           "foreign-port-code");
   
   /**
    * Default activity.
    */
   private static final String FOREIGN_TRANS_CODE = PortletProps.get(
           "foreign-trans-code");
   
   /**
    * Default activity.
    */
   private static final String DEFAULT_ASPECT = PortletProps.get(
           "default_aspect");
   
   /**
    * Default activity.
    */
   private static final String DEFAULT_TRANS_CAUSAL = PortletProps.get(
           "default_trans_causal");
   
   
   /**
    * Default activity.
    */
   private static final String GIVE_DOCUMENT = PortletProps.get(
           "give-document");
   
   /**
    * Default storage code.
    */
   private static final String DEFAULT_STORAGE_CODE = PortletProps.get(
           "storage-code");
   /**
    * Exported order file name.
    */
   private static final String VIRTUAL_HOST = PortletProps.get(
           "virtual-host");

   /**
    * Export folder path.
    */
   private static final String EXPORT_FOLDER = PortletProps.get(
           "export-folder");
   
   /**
    * Export folder path.
    */
   private static final String DOCUMENTS_FOLDER = EXPORT_FOLDER
           + File.separator + "DDT" + File.separator;

    /**
     * {@link GiveDocumentExporterService} unique instance.
     */
    private static GiveDocumentExporterService instance = null;
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(
            GiveDocumentExporterService.class);

    /**
     * Default constructor.
     */
    private GiveDocumentExporterService() {
    }
    
    /**
     * Returns the {@link GiveDocumentExporterService}  instance.
     * 
     * @return the {@link GiveDocumentExporterService}  instance
     */
    public static GiveDocumentExporterService getInstance() {
        if (instance == null) {
            instance = new GiveDocumentExporterService();
        }
        return instance;
    }

	public TestataDocumentoPK[] createTodayGiveDocuments()
			throws PortalException, SystemException, AddressException,
			NamingException, SQLException, IOException, JRException {
		List<TestataDocumentoPK> res = new ArrayList<TestataDocumentoPK>();
		List<Ordine> orders = OrdineLocalServiceUtil.getTodayReadyOrders();
		this.logger.info("Ordini: " + orders.size());
		for (Ordine ordine : orders) {
			this.logger.debug("Ordine: " + ordine.getId());
			List<Carrello> carts = CarrelloLocalServiceUtil
					.findCartsInOrder(ordine.getId());
			this.logger.debug("Ordine: " + carts.size());
			Set<String> partners = new HashSet<String>();
			for (Carrello cart : carts) {
				this.logger.debug("Ordine: " + cart.getId());
				List<Riga> rows = RigaLocalServiceUtil.findRowsInCart(cart
						.getId());
				this.logger.debug("Righe: " + rows.size());
				for (Riga row : rows) {
					this.logger.debug("Riga: " + row.getId());
					Piante p = PianteLocalServiceUtil.getPiante(row
							.getIdArticolo());
					partners.add(p.getIdFornitore());
				}
			}
			this.logger.debug("Fornitori: " + partners.size());
			for (String p : partners) {
				this.logger.debug("Fornitore: " + p);
				TestataDocumentoPK[] docs = this.createTodayGiveDocuments(
						ordine.getId(), p);
				for (TestataDocumentoPK testataDocumentoPK : docs) {
					res.add(testataDocumentoPK);
				}
			}
			if (ordine.getStato() == OrderState.CONFIRMED.getValue()) {
				this.logger.debug("Ordine: " + ordine.getId() + " CONFERMATO");
				ordine.setDdtGenerati(true);
				OrdineLocalServiceUtil.updateOrdine(ordine);
			} else {
				this.logger.debug("Ordine: " + ordine.getId() + " PENDING");
			}
		}

		return res.toArray(new TestataDocumentoPK[res.size()]);

	}

    /**
     * Creates, exports and sends the give document for the ready part of order.
     * 
     * @param orderId
     *            the order id to be converted into give document
     * @param code
     *            partner cod for which documents should be exported, null to
     *            export documents for all partners
     * @return the document created
     * @throws PortalException
     *             exception
     * @throws SystemException
     *             exception
     * @throws JRException
     *             Exception
     * @throws IOException
     *             Exception
     * @throws SQLException
     *             Exception
     * @throws NamingException
     *             Exception
     * @throws AddressException
     *             Exception
     */
    public TestataDocumentoPK[] createTodayGiveDocuments(final long orderId,
            final String code) throws PortalException, SystemException,
            AddressException, NamingException, SQLException, IOException,
            JRException {
        Ordine order = OrdineLocalServiceUtil.getOrdine(orderId);
        List<TestataDocumentoPK> headerPK = new ArrayList<TestataDocumentoPK>();
//        for (Ordine order : orders) {
        	Map<String, Map<String, Riga>> partnersRows =
        			this.getPartnerRows(order);
            for (String partnerCode : partnersRows.keySet()) {
//                ClientiFornitoriDatiAgg data =
//                        ClientiFornitoriDatiAggLocalServiceUtil.
//                            fetchClientiFornitoriDatiAgg(
//                                    new ClientiFornitoriDatiAggPK(partnerCode,
//                                            true));
//                if (data != null && data.getAssociato()) {
                    TestataDocumento document =
                            TestataDocumentoLocalServiceUtil.
                                findDocumentsByOrderIdPartner(order.getId(),
                                        partnerCode);
                    if (document != null) {
                        List<RigoDocumento> rows =
                                RigoDocumentoLocalServiceUtil.findByHeaderItem(
                                        document.getPrimaryKey());
                        for (RigoDocumento row : rows) {
                            RigoDocumentoLocalServiceUtil.deleteRigoDocumento(
                                    row);
                        }
                    }
                    
                    headerPK.add(this.createNewDocument(partnerCode, order,
                            partnersRows.get(partnerCode), document).
                            getPrimaryKey());
//                }
            }
//        }
        TestataDocumentoPK[] docs = headerPK.toArray(
                new TestataDocumentoPK[headerPK.size()]);
        this.export(docs, orderId, code);
        return docs;
    }
    /**
     * Exports the documents.
     * @param documents the documents
     * @throws NamingException Exception
     * @throws SQLException Exception
     * @throws PortalException Exception
     * @throws SystemException Exception
     * @throws IOException Exception
     * @throws JRException Exception
     * @throws AddressException Exception
     */
    private void export(final TestataDocumentoPK[] documents,
            final long orderId, final String code) throws NamingException,
            SQLException, PortalException, SystemException, IOException,
            JRException, AddressException {
        Context ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("jdbc/OrdiniPool");
        Connection c = ds.getConnection();
        List<TransportDocumentExporter> listTDocExp =
                new ArrayList<TransportDocumentExporter>(); 
        for (int i = 0; i < documents.length; i++) {
            TestataDocumento header = TestataDocumentoLocalServiceUtil.
                    getTestataDocumento(documents[i]);
            List<RigoDocumento> rows = RigoDocumentoLocalServiceUtil.
                    findByHeaderItem(header.getPrimaryKey());
            if (header.getLibLng1() == orderId) {
                if (code != null) {
                    if (header.getCodiceFornitore().equals(code)) {
                        listTDocExp.add(new TransportDocumentExporter(header,
                                rows, c));
                    }
                } else {
                    listTDocExp.add(new TransportDocumentExporter(header, rows,
                            c));
                }
            }
        }
        for (TransportDocumentExporter docExporter : listTDocExp) {
            MailSender sender = new MailSender();
            File doc = FileUtil.createTempFile("txt");
            FileWriter fw = new FileWriter(doc.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw);
            out.print(docExporter.exportDocument());
            out.close();
            bw.close();
            Company company = CompanyLocalServiceUtil.
                    getCompanyByVirtualHost(VIRTUAL_HOST);
            docExporter.createReport(company.getCompanyId());
            
            String dateStr = new SimpleDateFormat("yyyyMMdd").format(
                    docExporter.getDocument().getDataRegistrazione());
            File pdf = new File(DOCUMENTS_FOLDER 
                    + docExporter.getDocument().getCodiceFornitore() + '_'
                    + docExporter.getDocument().getTipoDocumento() + '_'
                    + docExporter.getDocument().getProtocollo()
                    + docExporter.getDocument().getCodiceCentro() + '_'
                    + dateStr + ".pdf");
            FileUtil.copyFile(docExporter.getReport(), pdf);
            
            ClientiFornitoriDatiAgg data =
            		ClientiFornitoriDatiAggLocalServiceUtil.fetchClientiFornitoriDatiAgg(
            				new ClientiFornitoriDatiAggPK(
            						docExporter.getDocument().getCodiceFornitore(),
            						true));
            if (!data.getAssociato()) {
            	docExporter.getDocument().setStato(true);
            } else if (!docExporter.getDocument().getInviato()){
            	sender.setAttachments(
                        new File[] {doc, docExporter.getReport() });
                sender.setSender(company.getEmailAddress());
                sender.setRecipient(AnagraficheClientiFornitoriLocalServiceUtil.
                        getAnagraficheClientiFornitori(docExporter.
                                getDocument().getCodiceFornitore()));
                sender.setDocument(docExporter.getDocument());
                sender.send("transport-document", docExporter.getDocument().
                        getInviato());
            }
            docExporter.getDocument().setInviato(true);
            TestataDocumentoLocalServiceUtil.updateTestataDocumento(
                    docExporter.getDocument());
        }
        
        c.close();
    }
    /**
     * Returns the partner codes involved in the specified order for which give
     * document can be created.
     * 
     * @param order
     *            the order
     * @return 
     * @throws PortalException
     *             exception
     * @throws SystemException
     *             exception
     */
    private Map<String, Map<String, Riga>> getPartnerRows(final Ordine order)
    		throws PortalException,
        SystemException {
    	Map<String, Map<String, Riga>> partnersRows =
    			new HashMap<String, Map<String, Riga>>();
        List<Carrello> carts = CarrelloLocalServiceUtil.findCartsInOrder(
                order.getId());
        Set<String> ok = new HashSet<String>();
        Set<String> ko = new HashSet<String>();
        for (Carrello cart : carts) {
            List<Riga> rows = RigaLocalServiceUtil.findRowsInCart(cart.getId());
            for (Riga row : rows) {
                Piante p = PianteLocalServiceUtil.getPiante(
                        row.getIdArticolo());
//                boolean added = ok.add(p.getIdFornitore());
                if (p.getPassaporto()) {
                    if (!row.getPassaporto().isEmpty()) {
                        ok.add(p.getIdFornitore());
                        this.addRow(p, row, partnersRows);
                    } else {
                        ko.add(p.getIdFornitore());
                        ok.remove(p.getIdFornitore());
                    }
                } else if (!ko.contains(p.getIdFornitore())) {
                    this.addRow(p, row, partnersRows);
                }
            }
        }
        for (String string : ko) {
			partnersRows.remove(string);
		}
        return partnersRows;
    }
    
    /**
     * Adds the rows.
     * 
     * @param p
     *            plant
     * @param row
     *            row
     * @param partnersRows 
     */
    private void addRow(final Piante p, final Riga row,
    		Map<String, Map<String, Riga>> partnersRows) {
        Map<String, Riga> tmp = null;
        if (partnersRows.containsKey(p.getIdFornitore())) {
            tmp = partnersRows.get(p.getIdFornitore());
            String k = p.getCodice() + "#" + row.getCodiceVariante() + "#"
                    + row.getPrezzoFornitore();
            if (tmp.containsKey(k)) {
                Riga r = tmp.get(k);
                r.setPianteRipiano(r.getPianteRipiano()
                        + row.getPianteRipiano());
                r.setImporto(r.getImporto() + row.getImporto());
            } else {
                tmp.put(k, row);
            }
        } else {
            tmp = new HashMap<String, Riga>();
            tmp.put(p.getCodice() + "#" + row.getCodiceVariante() + "#"
                    + row.getPrezzoFornitore(), row);
            partnersRows.put(p.getIdFornitore(), tmp);
        }

    }

    /**
     * Creates the document row.
     * 
     * @param headerPK
     *            the header primary key
     * @param p
     *            the item
     * @param r
     *            the order row
     * @param rowType
     *            document row type
     * @param o order
     * @return the created row
     * @throws SystemException
     *             exception
     * @throws PortalException
     *             exception
     */
    private RigoDocumento createOrUpdateDocumentRow(
            final TestataDocumentoPK headerPK, final Piante p, final Riga r,
            final int rowType, Ordine o) throws SystemException, PortalException {
        RigoDocumentoPK rowPK = new RigoDocumentoPK(headerPK.getAnno(),
                headerPK.getCodiceAttivita(), headerPK.getCodiceCentro(),
                headerPK.getCodiceDeposito(), headerPK.getProtocollo(),
                headerPK.getCodiceFornitore(),
                RigoDocumentoLocalServiceUtil.getNumber(headerPK),
                headerPK.getTipoDocumento());

        RigoDocumento row = RigoDocumentoLocalServiceUtil.
                createRigoDocumento(rowPK);
        row.setTipoRigo(rowType);
        if (rowType == 0) {
            row.setCodiceArticolo(p.getCodice());
            row.setCodiceVariante(r.getCodiceVariante());
//            row.setDescrizione(p.getNome());
            row.setQuantita(r.getPianteRipiano());
            row.setPrezzo(r.getPrezzoFornitore() + r.getImportoVarianti());
            Articoli item = ArticoliLocalServiceUtil.
                    getArticoli(p.getCodice());
            if (!r.getCodiceRegionale().isEmpty()) {
                row.setLibStr1(item.getLibStr1());
            }
            row.setImportoNetto(row.getImportoNetto()
                    + Utils.roundDouble(r.getPianteRipiano()
                                    * (r.getPrezzoFornitore() + r.
                                            getImportoVarianti()), 2));
            if (!o.getCodiceIva().isEmpty()) {
                row.setCodiceIVA(o.getCodiceIva());
            } else {
                row.setCodiceIVA(item.getCodiceIVA());
            }
        } else {
            row.setDescrizione(r.getStringa());
        }
        
        if (!r.getCodiceRegionale().isEmpty()) {
        	row.setLibStr1(r.getPassaporto());
            row.setLibStr2(r.getCodiceRegionale());
        }
//        if (!r.getCodiceRegionale().isEmpty()
//                && Validator.isNumber(r.getPassaporto())) {
//            row.setLibStr2(r.getCodiceRegionale());
//            row.setLibLng1(Integer.parseInt(r.getPassaporto()));
//        }
        
        return RigoDocumentoLocalServiceUtil.updateRigoDocumento(row);
    }

    /**
     * Creates a new document based on the specified order.
     * 
     * @param o
     *            current order
     * @param rows
     *            the rows
     * @param partner
     *            partner code
     * @param doc
     *            {@link TestataDocumento} the document header
     * @return the new document
     */
    private TestataDocumento createNewDocument(final String partner,
            final Ordine o, final Map<String, Riga> rows,
            final TestataDocumento doc) {
        TestataDocumento header = null;
        List<RigoDocumento> createdRows = new ArrayList<RigoDocumento>();
        try {
            TestataDocumentoPK headerPK;
            if (doc == null) {
                int num = TestataDocumentoLocalServiceUtil.getNumber(
                        Utils.getYear(), DEFAULT_ACTIVITY_CODE, DEFAULT_CENTER_CODE,
                        DEFAULT_STORAGE_CODE, String.valueOf(partner),
                        GIVE_DOCUMENT);
                headerPK = new TestataDocumentoPK(
                        Utils.getYear(), DEFAULT_ACTIVITY_CODE, DEFAULT_CENTER_CODE,
                        DEFAULT_STORAGE_CODE, num, String.valueOf(partner),
                        GIVE_DOCUMENT);
                header = TestataDocumentoLocalServiceUtil.createTestataDocumento(
                        headerPK);
            } else {
                headerPK = new TestataDocumentoPK(Utils.getYear(),
                        DEFAULT_ACTIVITY_CODE, DEFAULT_CENTER_CODE,
                        DEFAULT_STORAGE_CODE, doc.getProtocollo(),
                        String.valueOf(partner), GIVE_DOCUMENT);
                header = TestataDocumentoLocalServiceUtil.getTestataDocumento(
                        headerPK);
            }
            
            header.setTipoDocumento(GIVE_DOCUMENT);
            header.setDataRegistrazione(o.getDataConsegna());
            header.setCodiceFornitore(String.valueOf(partner));
            CodiceConsorzio companyCode = CodiceConsorzioLocalServiceUtil.
                    fetchCodiceConsorzio(String.valueOf(partner));
            if (companyCode != null) {
                header.setCodiceConsorzio(companyCode.getCodiceConsorzio());
            }

            if (o.getCentro().equals(IT_CENTER_CODE)) {
                header.setLibStr1(o.getLuogoCarico());
                header.setCodicePorto(IT_PORT_CODE);
                header.setCodiceCuraTrasporto(IT_TRANS_CODE);
            } else {
                AnagraficheClientiFornitori cust =
                        AnagraficheClientiFornitoriLocalServiceUtil.
                        getAnagraficheClientiFornitori(o.getIdCliente());
                header.setLibStr1(cust.getRagioneSociale1() + " "
                        + cust.getRagioneSociale2());
                header.setLibStr2(Utils.getFullAddress(cust));
                header.setCodicePorto(FOREIGN_PORT_CODE);
                header.setCodiceCuraTrasporto(FOREIGN_TRANS_CODE);
                header.setCodiceVettore1(o.getIdTrasportatore());
                header.setCodiceVettore2(o.getIdTrasportatore2());
            }
            header.setLibLng1(o.getId());
            header.setCodiceAspettoEsteriore(DEFAULT_ASPECT);
            header.setCodiceCausaleTrasporto(DEFAULT_TRANS_CAUSAL);
            for (String k : rows.keySet()) {
                Riga row = rows.get(k);
                Piante p = PianteLocalServiceUtil.getPiante(
                        row.getIdArticolo());
                createdRows.add(this.createOrUpdateDocumentRow(headerPK, p,
                        row, 0, o));
                if (!row.getCodiceVariante().isEmpty()) {
                    createdRows.add(this.createOrUpdateDocumentRow(headerPK,
                            p, row, 2, o));
                }
            }
            
            header = TestataDocumentoLocalServiceUtil.updateTestataDocumento(
                header);
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            if (createdRows.size() > 0) {
                this.removeCreatedRows(createdRows);
            }
        } catch (PortalException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            if (createdRows.size() > 0) {
                this.removeCreatedRows(createdRows);
            }
        } 
        return header;
    }

    /**
     * Deletes the created rows.
     * 
     * @param createdRows
     *            the list of created rows to remove
     */
    private void removeCreatedRows(final List<RigoDocumento> createdRows) {
        for (RigoDocumento docRow : createdRows) {
            this.logger.info("Deleting: " + docRow);
            try {
                RigoDocumentoLocalServiceUtil.deleteRigoDocumento(docRow);
            } catch (SystemException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
            }
        }

    }

}
