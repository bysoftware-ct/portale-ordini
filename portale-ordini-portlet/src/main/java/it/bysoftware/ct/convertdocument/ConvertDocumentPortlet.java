package it.bysoftware.ct.convertdocument;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.liferay.util.portlet.PortletProps;
import com.opencsv.CSVReader;

import it.bysoftware.ct.model.Articoli;
import it.bysoftware.ct.model.ArticoloSocio;
import it.bysoftware.ct.model.ClientiFornitoriDatiAgg;
import it.bysoftware.ct.model.DescrizioniVarianti;
import it.bysoftware.ct.model.Ordine;
import it.bysoftware.ct.model.RigoDocumento;
import it.bysoftware.ct.model.TestataDocumento;
import it.bysoftware.ct.service.ArticoliLocalServiceUtil;
import it.bysoftware.ct.service.ArticoloSocioLocalServiceUtil;
import it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil;
import it.bysoftware.ct.service.DescrizioniVariantiLocalServiceUtil;
import it.bysoftware.ct.service.OrdineLocalServiceUtil;
import it.bysoftware.ct.service.RigoDocumentoLocalServiceUtil;
import it.bysoftware.ct.service.TestataDocumentoLocalServiceUtil;
import it.bysoftware.ct.service.persistence.ArticoloSocioPK;
import it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK;
import it.bysoftware.ct.service.persistence.DescrizioniVariantiPK;
import it.bysoftware.ct.service.persistence.RigoDocumentoPK;
import it.bysoftware.ct.service.persistence.TestataDocumentoPK;
import it.bysoftware.ct.utils.Constants;
import it.bysoftware.ct.utils.ResourceId;
import it.bysoftware.ct.utils.Response;
import it.bysoftware.ct.utils.Utils;
import it.bysoftware.ct.utils.Response.Code;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

/**
 * Portlet implementation class ConvertDocumentPortlet.
 */
public class ConvertDocumentPortlet extends MVCPortlet {

    /**
     * WorkRigaDocumento constant.
     */
    private static final String WORK_RIGA_DOCUMENTO = "WorkRigaDocumento";
    /**
     * WorkTestataDocumento constant.
     */
    private static final String WORK_TESTATA_DOCUMENTO = "WorkTestataDocumento";
    /**
     * Date format pattern constant.
     */
    private static final SimpleDateFormat SDF = new SimpleDateFormat(
            "dd/MM/yyyy");
    /**
     * File upload constant.
     */
    private static final DecimalFormat NF = new DecimalFormat("#0,00");
    /**
     * File upload constant.
     */
    private static final String FILEUPLOAD = "fileupload";
    
    /**
     * Default activity.
     */
    private static final String DEFAULT_ACTIVITY_CODE = PortletProps.get(
            "activity-code");

    /**
     * Default activity.
     */
    private static final String DEFAULT_CENTER_CODE = PortletProps.get(
            "center-code");

    /**
     * Default storage code.
     */
    private static final String DEFAULT_STORAGE_CODE = PortletProps.get(
            "storage-code");

    /**
     * Default storage code.
     */
    private static final String DEFAULT_SELL_INVOICE = PortletProps.get(
            "sell-invoice");
    

    /**
     * Invoice header columns.
     * @author Aliseo-G
     *
     */
    private enum HeaderColumn {
        /**
         * Tipdoc column.
         */
        Tipdoc,
        /**
         * Codcen column.
         */
        Codcen,
        /**
         * Datreg column.
         */
        Datreg,
        /**
         * Codsog column.
         */
        Codsog,
        /**
         * Codspe column.
         */
        Codspe,
        /**
         * Codpor column.
         */
        Codpor,
        /**
         * Codase column.
         */
        Codase,
        /**
         * Codve1 column.
         */
        Codve1,
        /**
         * Codve2 column.
         */
        Codve2,
        /**
         * Protoc column.
         */
        Protoc,
        /**
         * ProtocInt column.
         */
        ProtocInt,
        /**
         * Datdoc column.
         */
        Datdoc,
        /**
         * Datann column.
         */
        Datann,
        /**
         * Nudaor column.
         */
        Nudaor;
    }
    
    /**
     * Invoice header columns.
     * @author Aliseo-G
     *
     */
    private enum RowColumn {
        /**
         * Tiprig column.
         */
        Tiprig,
        /**
         * Tiprig column.
         */
        Codart,
        /**
         * Tiprig column.
         */
        Codvar,
        /**
         * Tiprig column.
         */
        Descri,
        /**
         * Tiprig column.
         */
        Quanet,
        /**
         * Tiprig column.
         */
        Qm2net,
        /**
         * Tiprig column.
         */
        Prezzo,
        /**
         * Tiprig column.
         */
        Scont1,
        /**
         * Tiprig column.
         */
        Scont2,
        /**
         * Tiprig column.
         */
        Scont3,
        /**
         * Tiprig column.
         */
        Libstr1,
        /**
         * Tiprig column.
         */
        Libstr2,
        /**
         * Tiprig column.
         */
        Libstr3,
        /**
         * Tiprig column.
         */
        Libdbl1,
        /**
         * Tiprig column.
         */
        Libdbl2,
        /**
         * Tiprig column.
         */
        Libdbl3,
        /**
         * Tiprig column.
         */
        Liblng1,
        /**
         * Tiprig column.
         */
        Liblng2,
        /**
         * Tiprig column.
         */
        Liblng3,
        /**
         * Tiprig column.
         */
        Libdat1,
        /**
         * Tiprig column.
         */
        Libdat2,
        /**
         * Tiprig column.
         */
        Libdat3,
        /**
         * Tiprig column.
         */
        Codlot,
        /**
         * Tiprig column.
         */
        Numbol,
        /**
         * Tiprig column.
         */
        Numrigbol,
        /**
         * Tiprig column.
         */
        Unimis,
        /**
         * Tiprig column.
         */
        Posizione,
        /**
         * Tiprig column.
         */
        Impnet,
        /**
         * Tiprig column.
         */
        Codiva
    }

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(ConvertDocumentPortlet.class);

    
    @Override
    public final void serveResource(final ResourceRequest resourceRequest,
            final ResourceResponse resourceResponse) throws IOException,
            PortletException {
        String resourceID = resourceRequest.getResourceID();
        PrintWriter out;
        Response response;
        switch (ResourceId.valueOf(resourceID)) {
            case saveInvoice:
                this.logger.debug("Saving invoice");
                response = this.saveAssociation(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            default:
                break;
        }
    }
    
    /**
     * Activates or Deactivates a user based on his current state.
     * 
     * @param areq
     *            {@link ActionRequest}
     * @param ares
     *            {@link ActionResponse}
     */
    public final void uploadInvoice(final ActionRequest areq,
            final ActionResponse ares) {
        this.logger.info("Uploading Invoice file...");

        UploadPortletRequest uploadRequest = PortalUtil
                .getUploadPortletRequest(areq);
        String backUrl = ParamUtil.getString(uploadRequest, "backUrl");
        String partnerCode = ParamUtil.getString(uploadRequest, "partnerCode");
        File tmpFolder = new File(System.getProperty("java.io.tmpdir"));
        if (!uploadRequest.getFileName(ConvertDocumentPortlet.FILEUPLOAD)
                .isEmpty()) {
            if (tmpFolder.getUsableSpace() < Constants.ONE_GB) {
                this.logger.warn("No enough space in: "
                        + tmpFolder.getAbsolutePath());
                SessionErrors.add(areq, "disk-space");
            } else if (uploadRequest.getSize(ConvertDocumentPortlet.FILEUPLOAD)
                    == 0) {
                this.logger.warn("Uploaded an empty file.");
                SessionErrors.add(areq, "empty-file");
            } else {
                File csvFile = uploadRequest.getFile(FILEUPLOAD);
                try {
                    @SuppressWarnings("resource")
                    CSVReader reader = new CSVReader(new FileReader(csvFile),
                            ';', '\"');
                    List<String[]> data = reader.readAll();
                    areq.setAttribute("invoices", this.parseData(data));
                    SessionMessages.add(areq, "request_processed", "");
                } catch (IOException e) {
                    this.logger.error(e.getMessage());
                    if (this.logger.isDebugEnabled()) {
                        e.printStackTrace();
                    }
                    SessionErrors.add(areq, e.getMessage());
                } catch (ParseException e) {
                    this.logger.error(e.getMessage());
                    if (this.logger.isDebugEnabled()) {
                        e.printStackTrace();
                    }
                    SessionErrors.add(areq, e.getMessage());
                } catch (Exception e) {
                    this.logger.error(e.getMessage());
                    if (this.logger.isDebugEnabled()) {
                        e.printStackTrace();
                    }
                    SessionErrors.add(areq, "error-index-x");
                    ares.setRenderParameter("exceptionArgs", e.getMessage());
                }
            }
        }
        ares.setRenderParameter("backUrl", backUrl);
        ares.setRenderParameter("partnerCode", partnerCode);
        ares.setRenderParameter("jspPage", "/html/convertdocument/view.jsp");
    }

    /**
     * Parses the uploaded data into an invoice.
     * 
     * @param data
     *            the uploaded CSV data
     * @return the parsed data
     * @throws Exception
     *             exception
     */
    private List<Invoice> parseData(List<String[]> data) throws Exception {
        Invoice invoice = null;
        List<Invoice> invoices = new ArrayList<Invoice>();
        boolean isHeader = false;
        int index = 1;
        try {
            for (String[] strings : data) {
                this.logger.debug("TEST: " + strings.length);
                this.logger.debug(StringUtil.merge(Arrays.asList(strings),
                        ";"));
                if (isHeader) {
                    this.logger.debug("1");
                    invoice = new Invoice();
                    invoice.setTipdoc(strings[HeaderColumn.Tipdoc.ordinal()]);
                    invoice.setCodcen(strings[HeaderColumn.Codcen.ordinal()]);
                    try {
                    invoice.setDatreg(ConvertDocumentPortlet.SDF.parse(
                            strings[HeaderColumn.Datreg.ordinal()]));
                    } catch (ParseException ex) {
                        this.logger.debug("Unparsable value");
                    }
                    invoice.setCodsog(strings[HeaderColumn.Codsog.ordinal()]);
                    invoice.setCodspe(strings[HeaderColumn.Codspe.ordinal()]);
                    invoice.setCodpor(strings[HeaderColumn.Codpor.ordinal()]);
                    invoice.setCodase(strings[HeaderColumn.Codase.ordinal()]);
                    invoice.setCodve1(strings[HeaderColumn.Codve1.ordinal()]);
                    invoice.setCodve2(strings[HeaderColumn.Codve2.ordinal()]);
                    try {
                        invoice.setProtoc(Integer.parseInt(
                                strings[HeaderColumn.Protoc.ordinal()]));
                    } catch (NumberFormatException e) {
                        this.logger.debug("Unparsable value");
                    }
                    try {
                        invoice.setProtocInt(Integer.parseInt(
                                strings[HeaderColumn.ProtocInt.ordinal()]));
                    } catch (NumberFormatException e) {
                        this.logger.debug("Unparsable value");
                    }
                    try {
                    invoice.setDatdoc(ConvertDocumentPortlet.SDF.parse(
                            strings[HeaderColumn.Datdoc.ordinal()]));
                    } catch (ParseException e) {
                        this.logger.debug("Unparsable value");
                    }
                    try {
                        invoice.setDatann(ConvertDocumentPortlet.SDF.parse(
                                strings[HeaderColumn.Datann.ordinal()]));
                    } catch (ParseException e) {
                        this.logger.debug("Unparsable value");
                    }
                    invoice.setNudaor(strings[HeaderColumn.Nudaor.ordinal()]);
                    isHeader = false;
                } else if (ConvertDocumentPortlet.WORK_TESTATA_DOCUMENTO.
                        equals(strings[0].trim())) {
                    this.logger.debug("2");
                    if (invoice != null) {
                        invoices.add(invoice);
                    }
                    isHeader = true;
                    continue;
                } else if (ConvertDocumentPortlet.WORK_RIGA_DOCUMENTO
                        .equals(strings[0].trim())) {
                    this.logger.debug("3");
                    continue;
                } else {
                    this.logger.debug("4");
                    Row r = new Row();
                    try {
                        r.setTiprig(Integer.parseInt(strings[RowColumn.Tiprig.
                                ordinal()]));
                    } catch (NumberFormatException ex) {
                        throw ex;
                    }
                    r.setCodart(strings[RowColumn.Codart.ordinal()]);
                    r.setCodvar(strings[RowColumn.Codvar.ordinal()]);
                    r.setDescri(strings[RowColumn.Descri.ordinal()]);
                    try {
                        r.setQuanet(ConvertDocumentPortlet.NF.parse(
                                strings[RowColumn.Quanet.ordinal()])
                                    .doubleValue());
                    } catch (ParseException e) {
                        this.logger.debug("Unparsable value");
                    }
                    try {
                        r.setQm2net(ConvertDocumentPortlet.NF.parse(
                                strings[RowColumn.Qm2net.ordinal()]).
                                    doubleValue());
                    } catch (ParseException e) {
                        this.logger.debug("Unparsable value");
                    }
                    try {
                        r.setPrezzo(ConvertDocumentPortlet.NF.parse(
                                strings[RowColumn.Prezzo.ordinal()]).
                                    doubleValue());
                    } catch (ParseException e) {
                        this.logger.debug("Unparsable value");
                    }
                    try {
                        r.setScont1(ConvertDocumentPortlet.NF.parse(
                                strings[RowColumn.Scont1.ordinal()]).
                                    doubleValue());
                    } catch (ParseException e) {
                        this.logger.debug("Unparsable value");
                    }
                    try {
                        r.setScont2(ConvertDocumentPortlet.NF.parse(
                                strings[RowColumn.Scont2.ordinal()]).
                                    doubleValue());
                    } catch (ParseException e) {
                        this.logger.debug("Unparsable value");
                    }
                    try {
                        r.setScont3(ConvertDocumentPortlet.NF.parse(
                                strings[RowColumn.Scont3.ordinal()]).
                                    doubleValue());
                    } catch (ParseException e) {
                        this.logger.debug("Unparsable value");
                    }
                    r.setLibstr1(strings[RowColumn.Libstr1.ordinal()]);
                    r.setLibstr2(strings[RowColumn.Libstr2.ordinal()]);
                    r.setLibstr3(strings[RowColumn.Libstr3.ordinal()]);
                    try {
                        r.setLibdbl1(ConvertDocumentPortlet.NF.parse(
                                strings[RowColumn.Libdbl1.ordinal()]).
                                    doubleValue());
                    } catch (ParseException e) {
                        this.logger.debug("Unparsable value");
                    }
                    try {
                        r.setLibdbl2(ConvertDocumentPortlet.NF.parse(
                                strings[RowColumn.Libdbl2.ordinal()]).
                                    doubleValue());
                    } catch (ParseException e) {
                        this.logger.debug("Unparsable value");
                    }
                    try {
                        r.setLibdbl3(ConvertDocumentPortlet.NF.parse(
                                strings[RowColumn.Libdbl3.ordinal()]).
                                    doubleValue());
                    } catch (ParseException e) {
                        this.logger.debug("Unparsable value");
                    }
                    try {
                        r.setLiblng1(Long.parseLong(strings[RowColumn.Liblng1.
                                ordinal()]));
                    } catch (NumberFormatException e) {
                        this.logger.debug("Unparsable value");
                    }
                    try {
                        r.setLiblng2(Long.parseLong(strings[RowColumn.Liblng2.
                                ordinal()]));
                    } catch (NumberFormatException e) {
                        this.logger.debug("Unparsable value");
                    }
                    try {
                        r.setLiblng3(Long.parseLong(strings[RowColumn.Liblng3.
                                ordinal()]));
                    } catch (NumberFormatException e) {
                        this.logger.debug("Unparsable value");
                    }
                    
                    try {
                        r.setLibdat1(ConvertDocumentPortlet.SDF.parse(
                                strings[RowColumn.Libdat1.ordinal()]));
                    } catch (ParseException e) {
                        this.logger.debug("Unparsable value");
                    }
                    try {
                        r.setLibdat2(ConvertDocumentPortlet.SDF.parse(
                                strings[RowColumn.Libdat2.ordinal()]));
                    } catch (ParseException e) {
                        this.logger.debug("Unparsable value");
                    }
                    try {
                        r.setLibdat3(ConvertDocumentPortlet.SDF.parse(
                                strings[RowColumn.Libdat3.ordinal()]));
                    } catch (ParseException e) {
                        this.logger.debug("Unparsable value");
                    }
                    r.setCodlot(strings[RowColumn.Codlot.ordinal()]);
                    try {
                        r.setNumbol(Integer.parseInt(strings[RowColumn.Numbol.
                                ordinal()]));
                    } catch (NumberFormatException e) {
                        this.logger.debug("Unparsable value");
                    }
                    try {
                        r.setNumrigbol(Integer.parseInt(
                                strings[RowColumn.Numrigbol.ordinal()]));
                    } catch (NumberFormatException e) {
                        this.logger.debug("Unparsable value");
                    }
                    r.setUnimis(strings[RowColumn.Unimis.ordinal()]);
                    try {
                        r.setPosizione(Integer.parseInt(
                                strings[RowColumn.Posizione.ordinal()]));
                    } catch (NumberFormatException e) {
                        this.logger.debug("Unparsable value");
                    }
                    try {
                        r.setImpnet(ConvertDocumentPortlet.NF.parse(
                                strings[RowColumn.Impnet.ordinal()]).
                                    doubleValue());
                    } catch (ParseException e) {
                        this.logger.debug("Unparsable value");
                    }
                    r.setCodiva(strings[RowColumn.Codiva.ordinal()]);
                    invoice.getRows().add(r);
                }
                index++;
            }
        } catch (Exception ex) {
            throw new Exception(String.valueOf(index), ex);
        }
        invoices.add(invoice);
        return invoices;
    }
    
    /**
     * Saves invoice sent by the partner.
     * 
     * @param resourceRequest {@link ResourceRequest}
     * @return {@link Response}
     */
    private Response saveAssociation(final ResourceRequest resourceRequest) {
        Response response = null;
        String partner = ParamUtil.getString(resourceRequest, "partner",
                null); 
        Invoice invoice = JSONFactoryUtil.looseDeserialize(
                ParamUtil.getString(resourceRequest, "invoice", null),
                Invoice.class);
        if (Validator.isNotNull(invoice) && Validator.isNotNull(partner)) {
            this.logger.debug(invoice);
            if (invoice.getNudaor() != null && !invoice.getNudaor().isEmpty()
                    && invoice.getNudaor().contains("/")
                    && invoice.getNudaor().split(" ").length > 0) {
                String[] tmp = invoice.getNudaor().split(" ");
                try {
                    int num = Integer.parseInt(tmp[0].split("/")[0]);
                    Ordine order = OrdineLocalServiceUtil.getByYearNumberCenter(
                            num, tmp[0].split("/")[1]);
                    if (order != null) {
                        TestataDocumento doc = TestataDocumentoLocalServiceUtil.
                                findDocumentsByOrderIdPartner(order.getId(),
                                        partner);
                        if (doc != null) {
                            doc.setStato(true);
                            try {
                                Map<TestataDocumento, List<RigoDocumento>> cInv
                                    = this.convertInvoice(invoice, partner,
                                            order);
                                for (TestataDocumento h : cInv.keySet()) {
                                    for (RigoDocumento r : cInv.get(h)) {
                                        RigoDocumentoLocalServiceUtil.
                                            updateRigoDocumento(r);
                                    }
                                    TestataDocumentoLocalServiceUtil.
                                        updateTestataDocumento(h);
                                }
                                TestataDocumentoLocalServiceUtil.
                                    updateTestataDocumento(doc);
                                response = new Response(Code.OK, "OK");
                            } catch (SystemException e) {
                                this.logger.error(e.getMessage());
                                if (this.logger.isDebugEnabled()) {
                                    e.printStackTrace();
                                }
                                response = new Response(Code.GENERIC_ERROR,
                                        "ERRORE: " + e.getMessage());
                            } catch (PortalException e) {
                                this.logger.error(e.getMessage());
                                if (this.logger.isDebugEnabled()) {
                                    e.printStackTrace();
                                }
                                if (e.getMessage().contains("ArticoloSocio")) {
                                    response = new Response(Code.GENERIC_ERROR,
                                            "Associazione Articolo"
                                            + " Socio-Consorzio non trovata.");
                                } else {
                                    response = new Response(Code.GENERIC_ERROR,
                                            "ERRORE: " + e.getMessage());
                                }
                            }
                        } else {
                            response = new Response(Code.GENERIC_ERROR,
                                    "Documento non trovato, riprovare più"
                                    + " tardi. Se il problema persiste,"
                                    + " contattare il gestore del serviizio.");
                        }
                    } else {
                        response = new Response(Code.GENERIC_ERROR, "Ordine non"
                                + " trovato, verificare il riferimento inserito"
                                + " prima di riprovare.");
                    }
                } catch (NumberFormatException ex) {
                    response = new Response(Code.GENERIC_ERROR, "Riferimento"
                            + " ordine assente o non valido.");
                }
            } else {
                response = new Response(Code.GENERIC_ERROR, "Riferimento"
                        + " ordine assente o non valido");
            }
        } else {
            response = new Response(Code.GENERIC_ERROR, "ERRORE 1");
        }
        return response;
    }

    /**
     * Converts the partner invoice in the company invoice.
     * 
     * @param invoice
     *            partner invoice
     * @param partner
     *            partner code
     * @param doc 
     *            original Order
     * @return an {@link Map} containing the converted invoice.
     * @throws SystemException 
     * @throws PortalException 
     */
    private Map<TestataDocumento, List<RigoDocumento>>
        convertInvoice(final Invoice invoice, final String partner,
                final Ordine doc) throws SystemException, PortalException {
        ClientiFornitoriDatiAgg p = ClientiFornitoriDatiAggLocalServiceUtil.
                getClientiFornitoriDatiAgg(new ClientiFornitoriDatiAggPK(
                        partner, true));
        TestataDocumento header = null;
        header = TestataDocumentoLocalServiceUtil
                .fetchTestataDocumento(new TestataDocumentoPK(Utils.getYear(),
                        DEFAULT_ACTIVITY_CODE, DEFAULT_CENTER_CODE,
                        DEFAULT_STORAGE_CODE, invoice.getProtoc(), partner,
                        DEFAULT_SELL_INVOICE));
        if (header == null) {
            header = TestataDocumentoLocalServiceUtil
                    .createTestataDocumento(new TestataDocumentoPK(Utils
                            .getYear(), DEFAULT_ACTIVITY_CODE,
                            DEFAULT_CENTER_CODE, DEFAULT_STORAGE_CODE,
                            invoice.getProtoc(), partner,
                            DEFAULT_SELL_INVOICE));
        }
        header.setDataRegistrazione(invoice.getDatreg());
        header.setLibLng1(doc.getId());
        List<RigoDocumento> rows = RigoDocumentoLocalServiceUtil.
                findByHeaderItem(header.getPrimaryKey());
        for (RigoDocumento r : rows) {
            RigoDocumentoLocalServiceUtil.deleteRigoDocumento(r);
        }
        rows = new ArrayList<RigoDocumento>();
        int pos = 1;
        RigoDocumento invoiceRow = RigoDocumentoLocalServiceUtil.
                createRigoDocumento(new RigoDocumentoPK(header.getAnno(),
                        header.getCodiceAttivita(), header.getCodiceCentro(),
                        header.getCodiceDeposito(), header.getProtocollo(),
                        header.getCodiceFornitore(), pos,
                        header.getTipoDocumento()));
        invoiceRow.setTipoRigo(2);
        invoiceRow.setDescrizione("Rif. Ordine N: " + doc.getNumero() + "/"
                + doc.getCentro());
        rows.add(invoiceRow);
        for (int i = 0; i < invoice.getRows().size(); i++) {
            Row r = invoice.getRows().get(i);
            invoiceRow = RigoDocumentoLocalServiceUtil.
                    createRigoDocumento(new RigoDocumentoPK(header.getAnno(),
                            header.getCodiceAttivita(),
                            header.getCodiceCentro(),
                            header.getCodiceDeposito(), header.getProtocollo(),
                            header.getCodiceFornitore(), ++pos,
                            header.getTipoDocumento()));
            invoiceRow.setTipoRigo(r.getTiprig());
            ArticoloSocio pItem = null;
            Articoli item = null;
            if (invoiceRow.getTipoRigo() == 0) {
                pItem = ArticoloSocioLocalServiceUtil.getArticoloSocio(
                        new ArticoloSocioPK(partner, r.getCodart(),
                                r.getCodvar()));
                invoiceRow.setCodiceArticolo(pItem.getCodiceArticolo());
                invoiceRow.setCodiceVariante(pItem.getCodiceVariante());
                item = ArticoliLocalServiceUtil.getArticoli(
                        pItem.getCodiceArticolo());
                invoiceRow.setLibStr1(item.getLibStr1());
            }
            invoiceRow.setQuantita(r.getQuanet());
            invoiceRow.setQuantitaSecondaria(r.getQm2net());
            invoiceRow.setPrezzo(r.getPrezzo());
            invoiceRow.setSconto1(r.getScont1());
            invoiceRow.setSconto2(r.getScont2());
            invoiceRow.setSconto3(r.getScont3());
            invoiceRow.setLibStr2(p.getCodiceRegionale());
            invoiceRow.setLibStr3(r.getLibstr3());
            invoiceRow.setLibDbl1(r.getLibdbl1());
            invoiceRow.setLibDbl2(r.getLibdbl2());
            invoiceRow.setLibDbl3(r.getLibdbl3());
            invoiceRow.setLibLng1(invoice.getProtoc());
            invoiceRow.setLibLng2(r.getLiblng2());
            invoiceRow.setLibLng3(r.getLiblng3());
            invoiceRow.setLibDat1(r.getLibdat1());
            invoiceRow.setLibDat2(r.getLibdat2());
            invoiceRow.setLibDat3(r.getLibdat3());
            invoiceRow.setImportoNetto(r.getImpnet());
            invoiceRow.setCodiceIVA(r.getCodiva());
            rows.add(invoiceRow);
            if (invoiceRow.getTipoRigo() == 0 && !invoiceRow.
                    getCodiceVariante().isEmpty()) {
                invoiceRow = RigoDocumentoLocalServiceUtil.
                        createRigoDocumento(new RigoDocumentoPK(
                                header.getAnno(),
                                header.getCodiceAttivita(),
                                header.getCodiceCentro(),
                                header.getCodiceDeposito(),
                                header.getProtocollo(),
                                header.getCodiceFornitore(), ++pos,
                                header.getTipoDocumento()));
                DescrizioniVarianti v = DescrizioniVariantiLocalServiceUtil.
                        getDescrizioniVarianti(new DescrizioniVariantiPK(
                                pItem.getCodiceArticolo(),
                                pItem.getCodiceVariante()));
                invoiceRow.setTipoRigo(2);
                invoiceRow.setDescrizione(v.getDescrizione());
                rows.add(invoiceRow);
            }
        }
        Map<TestataDocumento, List<RigoDocumento>> result =
                new HashMap<TestataDocumento, List<RigoDocumento>>();
        result.put(header, rows);
        return result;
    }
}
