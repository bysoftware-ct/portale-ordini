/**
 * 
 */
package it.bysoftware.ct.convertdocument;

import java.util.Date;

/**
 * @author Aliseo-G
 *
 */
public class Row {
    private String codart, codvar, descri, libstr1, libstr2, libstr3,
            codlot, unimis, codiva;
    private double quanet, qm2net, prezzo, scont1, scont2, scont3, libdbl1,
            libdbl2, libdbl3, impnet;
    private long liblng1, liblng2, liblng3;
    private Date libdat1, libdat2, libdat3;
    private int tiprig, numbol, numrigbol, posizione;
    /**
     * @return the tiprig
     */
    public int getTiprig() {
        return tiprig;
    }
    /**
     * @param tiprig the tiprig to set
     */
    public void setTiprig(int tiprig) {
        this.tiprig = tiprig;
    }
    /**
     * @return the codart
     */
    public String getCodart() {
        return codart;
    }
    /**
     * @param codart the codart to set
     */
    public void setCodart(String codart) {
        this.codart = codart;
    }
    /**
     * @return the codvar
     */
    public String getCodvar() {
        return codvar;
    }
    /**
     * @param codvar the codvar to set
     */
    public void setCodvar(String codvar) {
        this.codvar = codvar;
    }
    /**
     * @return the descri
     */
    public String getDescri() {
        return descri;
    }
    /**
     * @param descri the descri to set
     */
    public void setDescri(String descri) {
        this.descri = descri;
    }
    /**
     * @return the libstr1
     */
    public String getLibstr1() {
        return libstr1;
    }
    /**
     * @param libstr1 the libstr1 to set
     */
    public void setLibstr1(String libstr1) {
        this.libstr1 = libstr1;
    }
    /**
     * @return the libstr2
     */
    public String getLibstr2() {
        return libstr2;
    }
    /**
     * @param libstr2 the libstr2 to set
     */
    public void setLibstr2(String libstr2) {
        this.libstr2 = libstr2;
    }
    /**
     * @return the libstr3
     */
    public String getLibstr3() {
        return libstr3;
    }
    /**
     * @param libstr3 the libstr3 to set
     */
    public void setLibstr3(String libstr3) {
        this.libstr3 = libstr3;
    }
    /**
     * @return the codlot
     */
    public String getCodlot() {
        return codlot;
    }
    /**
     * @param codlot the codlot to set
     */
    public void setCodlot(String codlot) {
        this.codlot = codlot;
    }
    /**
     * @return the unimis
     */
    public String getUnimis() {
        return unimis;
    }
    /**
     * @param unimis the unimis to set
     */
    public void setUnimis(String unimis) {
        this.unimis = unimis;
    }
    /**
     * @return the codiva
     */
    public String getCodiva() {
        return codiva;
    }
    /**
     * @param codiva the codiva to set
     */
    public void setCodiva(String codiva) {
        this.codiva = codiva;
    }
    /**
     * @return the quanet
     */
    public double getQuanet() {
        return quanet;
    }
    /**
     * @param quanet the quanet to set
     */
    public void setQuanet(double quanet) {
        this.quanet = quanet;
    }
    /**
     * @return the qm2net
     */
    public double getQm2net() {
        return qm2net;
    }
    /**
     * @param qm2net the qm2net to set
     */
    public void setQm2net(double qm2net) {
        this.qm2net = qm2net;
    }
    /**
     * @return the prezzo
     */
    public double getPrezzo() {
        return prezzo;
    }
    /**
     * @param prezzo the prezzo to set
     */
    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }
    /**
     * @return the scont1
     */
    public double getScont1() {
        return scont1;
    }
    /**
     * @param scont1 the scont1 to set
     */
    public void setScont1(double scont1) {
        this.scont1 = scont1;
    }
    /**
     * @return the scont2
     */
    public double getScont2() {
        return scont2;
    }
    /**
     * @param scont2 the scont2 to set
     */
    public void setScont2(double scont2) {
        this.scont2 = scont2;
    }
    /**
     * @return the scont3
     */
    public double getScont3() {
        return scont3;
    }
    /**
     * @param scont3 the scont3 to set
     */
    public void setScont3(double scont3) {
        this.scont3 = scont3;
    }
    /**
     * @return the libdbl1
     */
    public double getLibdbl1() {
        return libdbl1;
    }
    /**
     * @param libdbl1 the libdbl1 to set
     */
    public void setLibdbl1(double libdbl1) {
        this.libdbl1 = libdbl1;
    }
    /**
     * @return the libdbl2
     */
    public double getLibdbl2() {
        return libdbl2;
    }
    /**
     * @param libdbl2 the libdbl2 to set
     */
    public void setLibdbl2(double libdbl2) {
        this.libdbl2 = libdbl2;
    }
    /**
     * @return the libdbl3
     */
    public double getLibdbl3() {
        return libdbl3;
    }
    /**
     * @param libdbl3 the libdbl3 to set
     */
    public void setLibdbl3(double libdbl3) {
        this.libdbl3 = libdbl3;
    }
    /**
     * @return the impnet
     */
    public double getImpnet() {
        return impnet;
    }
    /**
     * @param impnet the impnet to set
     */
    public void setImpnet(double impnet) {
        this.impnet = impnet;
    }
    /**
     * @return the liblng1
     */
    public long getLiblng1() {
        return liblng1;
    }
    /**
     * @param liblng1 the liblng1 to set
     */
    public void setLiblng1(long liblng1) {
        this.liblng1 = liblng1;
    }
    /**
     * @return the liblng2
     */
    public long getLiblng2() {
        return liblng2;
    }
    /**
     * @param liblng2 the liblng2 to set
     */
    public void setLiblng2(long liblng2) {
        this.liblng2 = liblng2;
    }
    /**
     * @return the liblng3
     */
    public long getLiblng3() {
        return liblng3;
    }
    /**
     * @param liblng3 the liblng3 to set
     */
    public void setLiblng3(long liblng3) {
        this.liblng3 = liblng3;
    }
    /**
     * @return the libdat1
     */
    public Date getLibdat1() {
        return libdat1;
    }
    /**
     * @param libdat1 the libdat1 to set
     */
    public void setLibdat1(Date libdat1) {
        this.libdat1 = libdat1;
    }
    /**
     * @return the libdat2
     */
    public Date getLibdat2() {
        return libdat2;
    }
    /**
     * @param libdat2 the libdat2 to set
     */
    public void setLibdat2(Date libdat2) {
        this.libdat2 = libdat2;
    }
    /**
     * @return the libdat3
     */
    public Date getLibdat3() {
        return libdat3;
    }
    /**
     * @param libdat3 the libdat3 to set
     */
    public void setLibdat3(Date libdat3) {
        this.libdat3 = libdat3;
    }
    /**
     * @return the numbol
     */
    public int getNumbol() {
        return numbol;
    }
    /**
     * @param numbol the numbol to set
     */
    public void setNumbol(int numbol) {
        this.numbol = numbol;
    }
    /**
     * @return the numrigbol
     */
    public int getNumrigbol() {
        return numrigbol;
    }
    /**
     * @param numrigbol the numrigbol to set
     */
    public void setNumrigbol(int numrigbol) {
        this.numrigbol = numrigbol;
    }
    /**
     * @return the posizione
     */
    public int getPosizione() {
        return posizione;
    }
    /**
     * @param posizione the posizione to set
     */
    public void setPosizione(int posizione) {
        this.posizione = posizione;
    }
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Row [codart=" + codart + ", codvar=" + codvar + ", descri="
                + descri + ", libstr1=" + libstr1 + ", libstr2=" + libstr2
                + ", libstr3=" + libstr3 + ", codlot=" + codlot + ", unimis="
                + unimis + ", codiva=" + codiva + ", quanet=" + quanet
                + ", qm2net=" + qm2net + ", prezzo=" + prezzo + ", scont1="
                + scont1 + ", scont2=" + scont2 + ", scont3=" + scont3
                + ", libdbl1=" + libdbl1 + ", libdbl2=" + libdbl2
                + ", libdbl3=" + libdbl3 + ", impnet=" + impnet + ", liblng1="
                + liblng1 + ", liblng2=" + liblng2 + ", liblng3=" + liblng3
                + ", libdat1=" + libdat1 + ", libdat2=" + libdat2
                + ", libdat3=" + libdat3 + ", tiprig=" + tiprig + ", numbol="
                + numbol + ", numrigbol=" + numrigbol + ", posizione="
                + posizione + "]";
    }
    
    
}
