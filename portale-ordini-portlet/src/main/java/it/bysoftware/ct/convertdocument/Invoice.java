/**
 * 
 */
package it.bysoftware.ct.convertdocument;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Aliseo-G
 *
 */
public class Invoice {
    /**
     * String attributes.
     */
    private String tipdoc, codcen, codsog, codspe, codpor, codase, codve1,
            codve2, nudaor;
    /**
     * Date attributes.
     */
    private Date datreg, datdoc, datann;
    /**
     * Integer attribute.
     */
    private int protoc, protocInt;
    /**
     * Invoice rows.
     */
    private List<Row> rows = new ArrayList<Row>();
    /**
     * @return the tipdoc
     */
    public String getTipdoc() {
        return this.tipdoc;
    }

    /**
     * @param t
     *            the tipdoc to set
     */
    public void setTipdoc(final String t) {
        this.tipdoc = t;
    }

    /**
     * @return the codcen
     */
    public String getCodcen() {
        return codcen;
    }

    /**
     * @param c
     *            the codcen to set
     */
    public void setCodcen(final String c) {
        this.codcen = c;
    }

    /**
     * @return the codsog
     */
    public String getCodsog() {
        return codsog;
    }

    /**
     * @param c
     *            the codsog to set
     */
    public void setCodsog(final String c) {
        this.codsog = c;
    }

    /**
     * @return the codspe
     */
    public String getCodspe() {
        return codspe;
    }

    /**
     * @param c
     *            the codspe to set
     */
    public void setCodspe(final String c) {
        this.codspe = c;
    }

    /**
     * @return the codpor
     */
    public String getCodpor() {
        return codpor;
    }

    /**
     * @param cp
     *            the codpor to set
     */
    public void setCodpor(final String cp) {
        this.codpor = cp;
    }

    /**
     * @return the codase
     */
    public String getCodase() {
        return codase;
    }

    /**
     * @param cae
     *            the codase to set
     */
    public void setCodase(final String cae) {
        this.codase = cae;
    }

    /**
     * @return the codve1
     */
    public String getCodve1() {
        return codve1;
    }

    /**
     * @param cv1
     *            the codve1 to set
     */
    public void setCodve1(final String cv1) {
        this.codve1 = cv1;
    }

    /**
     * @return the codve2
     */
    public String getCodve2() {
        return codve2;
    }

    /**
     * @param cv2
     *            the codve2 to set
     */
    public void setCodve2(final String cv2) {
        this.codve2 = cv2;
    }

    /**
     * @return the nudaor
     */
    public String getNudaor() {
        return nudaor;
    }

    /**
     * @param nor
     *            the nudaor to set
     */
    public void setNudaor(final String nor) {
        this.nudaor = nor;
    }

    /**
     * @return the datreg
     */
    public Date getDatreg() {
        return datreg;
    }

    /**
     * @param dreg
     *            the datreg to set
     */
    public void setDatreg(final Date dreg) {
        this.datreg = dreg;
    }

    /**
     * @return the datdoc
     */
    public Date getDatdoc() {
        return datdoc;
    }

    /**
     * @param ddoc
     *            the datdoc to set
     */
    public void setDatdoc(final Date ddoc) {
        this.datdoc = ddoc;
    }

    /**
     * @return the datann
     */
    public Date getDatann() {
        return datann;
    }

    /**
     * @param dann
     *            the datann to set
     */
    public void setDatann(final Date dann) {
        this.datann = dann;
    }

    /**
     * @return the protoc
     */
    public int getProtoc() {
        return protoc;
    }

    /**
     * @param p
     *            the protoc to set
     */
    public void setProtoc(final int p) {
        this.protoc = p;
    }

    /**
     * @return the protocInt
     */
    public int getProtocInt() {
        return this.protocInt;
    }

    /**
     * @param pInt
     *            the protocInt to set
     */
    public void setProtocInt(final int pInt) {
        this.protocInt = pInt;
    }

    /**
     * @return the rows
     */
    public List<Row> getRows() {
        return rows;
    }

    /**
     * @param r the rows to set
     */
    public void setRows(final List<Row> r) {
        this.rows = r;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Invoice [tipdoc=" + tipdoc + ", codcen=" + codcen + ", codsog="
                + codsog + ", codspe=" + codspe + ", codpor=" + codpor
                + ", codase=" + codase + ", codve1=" + codve1 + ", codve2="
                + codve2 + ", nudaor=" + nudaor + ", datreg=" + datreg
                + ", datdoc=" + datdoc + ", datann=" + datann + ", protoc="
                + protoc + ", protocInt=" + protocInt + ", rows=" + rows + "]";
    }
}
