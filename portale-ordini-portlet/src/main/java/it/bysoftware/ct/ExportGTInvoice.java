package it.bysoftware.ct;

import it.bysoftware.ct.model.AnagraficheClientiFornitori;
import it.bysoftware.ct.model.RigheFattureVedita;
import it.bysoftware.ct.model.Rubrica;
import it.bysoftware.ct.model.Stati;
import it.bysoftware.ct.model.TestataFattureClienti;
import it.bysoftware.ct.orderexport.GTInvoiceExporter;
import it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.RigheFattureVeditaLocalServiceUtil;
import it.bysoftware.ct.service.RubricaLocalServiceUtil;
import it.bysoftware.ct.service.StatiLocalServiceUtil;
import it.bysoftware.ct.service.TestataFattureClientiLocalServiceUtil;
import it.bysoftware.ct.service.persistence.TestataFattureClientiPK;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.ContentUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.liferay.util.portlet.PortletProps;

/**
 * Portlet implementation class ExportGTInvoice
 */
public class ExportGTInvoice extends MVCPortlet {

	private static final String EXPORT_FOLDER = PortletProps
			.get("export-folder");
	private static final String GT_FOLDER_INVOICE = PortletProps
			.get("gt-folder-invoice");
	private static final String GT_FOLDER = PortletProps.get("gt");
	private static final String[] CC_LIST = PortletProps.get("cc-list").split(
			",");
	/**
	 * Email from.
	 */
	private static final String FROM = "mail-from";
	/**
	 * Logger object. Based on Liferay logger
	 * {@link com.liferay.portal.kernel.log.Log} commons logging.
	 */
	private Log logger = LogFactoryUtil.getLog(ExportGTInvoice.class);
	private static final DateFormat SDF = new SimpleDateFormat("yyyyMMdd");

	public final void export(final ActionRequest areq, final ActionResponse ares) {
		boolean send = ParamUtil.getBoolean(areq, "send", false);

		List<TestataFattureClienti> invoicesToExp = new ArrayList<TestataFattureClienti>();
		try {
			JSONArray selectedInvoicesJSON = JSONFactoryUtil
					.createJSONArray(ParamUtil.getString(areq, "invoicesPKs",
							"[]"));
			for (int i = 0; i < selectedInvoicesJSON.length(); i++) {
				JSONObject invoiceJSON = selectedInvoicesJSON.getJSONObject(i);
				TestataFattureClientiPK invoicePK = JSONFactoryUtil
						.looseDeserialize(invoiceJSON.toString(),
								TestataFattureClientiPK.class);
				try {
					invoicesToExp.add(TestataFattureClientiLocalServiceUtil
							.fetchTestataFattureClienti(invoicePK));
				} catch (SystemException e) {
					this.logger.error(e.getMessage());
					if (this.logger.isDebugEnabled()) {
						e.printStackTrace();
					}
				}
			}
			List<TestataFattureClienti> expInvoices;
			expInvoices = this.exportInvoices(invoicesToExp);
			areq.setAttribute("expInvoices", expInvoices);
			if (send) {
				List<TestataFattureClienti> sentInvoice = new ArrayList<TestataFattureClienti>();
				for (TestataFattureClienti invoice : expInvoices) {
					try {
						sentInvoice.add(this.sendEmail(invoice));
					} catch (AddressException e) {
						this.logger.error(e.getMessage());
						if (this.logger.isDebugEnabled()) {
							e.printStackTrace();
						}
					}
				}
				areq.setAttribute("sentInvoices", sentInvoice);
			}

		} catch (JSONException e) {
			this.logger.error(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
			SessionErrors.add(areq, "parse-error");
		} catch (SystemException e) {
			this.logger.error(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
			SessionErrors.add(areq, e.getMessage());
		} catch (PortalException e) {
			this.logger.error(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
			SessionErrors.add(areq, e.getMessage());
		} catch (IOException e) {
			this.logger.error(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
			SessionErrors.add(areq, e.getMessage());
		}
		SessionMessages.add(areq, "request_processed", "");
		PortalUtil.copyRequestParameters(areq, ares);
		ares.setRenderParameter("jspPage", "/html/export-gt-invoice/view.jsp");
	}

	private TestataFattureClienti sendEmail(TestataFattureClienti invoice)
			throws AddressException, SystemException {

		this.logger.debug("Sending invoice: " + invoice.toString());
		AnagraficheClientiFornitori recipient = AnagraficheClientiFornitoriLocalServiceUtil
				.fetchAnagraficheClientiFornitori(invoice.getCodiceCliente());
		Stati s = StatiLocalServiceUtil.fetchStati(recipient.getSiglaStato());

		String body = "";
		if ("IT".equals(s.getCodiceISO())) {
			body = ContentUtil.get("/content/IT/gtCustomer-invoice.tmpl", true);
		} else {
			body = ContentUtil.get("/content/EN/gtCustomer-invoice.tmpl", true);
		}
		body = StringUtil.replace(
				body,
				new String[] { "[$SUPPLIER$]", "[$NUMBER$]", "[$DATE$]" },
				new String[] {
						recipient.getRagioneSociale1() + " "
								+ recipient.getRagioneSociale2(),
						invoice.getNumeroDocumento() + "/"
								+ invoice.getCodiceCentro(),
						SDF.format(invoice.getDataDocumento()) });

		MailMessage mailMessage = new MailMessage();
		mailMessage.setBody(body);

		mailMessage.setSubject(invoice.getTipoDocumento() + " N°: "
				+ invoice.getNumeroDocumento() + " / "
				+ invoice.getCodiceCentro());
		mailMessage.setFrom(new InternetAddress(PortletProps.get(FROM)));
		this.logger.info("Sending invoice: " + mailMessage.getSubject()
				+ " to: " + mailMessage.getFrom());
		List<Rubrica> contacts = RubricaLocalServiceUtil.findContacts(invoice
				.getCodiceCliente());
		List<InternetAddress> addresses = new ArrayList<InternetAddress>();
		for (Rubrica contact : contacts) {
			addresses.add(new InternetAddress(contact.getEMailTo()));
		}
		mailMessage
				.setTo(addresses.toArray(new InternetAddress[contacts.size()]));
		if (CC_LIST != null && CC_LIST.length != 0) {
			InternetAddress[] ccAddresses = new InternetAddress[CC_LIST.length];
			for (int i = 0; i < CC_LIST.length; i++) {
				InternetAddress address = new InternetAddress(CC_LIST[i]);
				ccAddresses[i] = address;
			}
			mailMessage.setCC(ccAddresses);
		}
		mailMessage.addFileAttachment(getInvoiceFile(invoice));

		MailServiceUtil.sendEmail(mailMessage);
		return invoice;

	}

	private File getInvoiceFile(TestataFattureClienti invoice) {
		File exportFolder = new File(EXPORT_FOLDER);
		return new File(exportFolder + File.separator + GT_FOLDER
				+ File.separator + GT_FOLDER_INVOICE + File.separator
				+ getFileName(invoice) + ".csv");
	}

	private List<TestataFattureClienti> exportInvoices(
			List<TestataFattureClienti> invoicesToExp) throws SystemException,
			PortalException, IOException {
		List<TestataFattureClienti> r = new ArrayList<TestataFattureClienti>();
		GTInvoiceExporter exporter;
		File exportFolder = new File(EXPORT_FOLDER);

		for (TestataFattureClienti invoice : invoicesToExp) {
			List<RigheFattureVedita> rows = RigheFattureVeditaLocalServiceUtil
					.getRigheFatturaByTestata(invoice);
			exporter = new GTInvoiceExporter(invoice, rows);

			File exportedInvoice = new File(exportFolder + File.separator
					+ GT_FOLDER + File.separator + GT_FOLDER_INVOICE
					+ File.separator + getFileName(invoice) + ".csv");
			String invoiceCSV = exporter.exportDocument();
			this.writeToFile(exportedInvoice, invoiceCSV);
			r.add(invoice);
		}
		return r;
	}

	private final void writeToFile(File exportedInvoice, String csvData)
			throws IOException {
		FileWriter fw;
		BufferedWriter bw;
		PrintWriter out;
		fw = new FileWriter(exportedInvoice.getAbsoluteFile(), false);
		bw = new BufferedWriter(fw);
		out = new PrintWriter(bw);
		out.print(csvData);
		out.close();
	}

	private String getFileName(TestataFattureClienti invoice) {
		String fileName = invoice.getCodiceCliente() + "_"
				+ invoice.getNumeroDocumento() + "_"
				+ SDF.format(invoice.getDataDocumento());
		return fileName;
	}
}
