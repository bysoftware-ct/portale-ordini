package it.bysoftware.ct;

import it.bysoftware.ct.model.Carrello;
import it.bysoftware.ct.service.CarrelloLocalServiceUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class OrderProcessing
 */
public class OrderProcessing extends MVCPortlet {
 
	/**
	 * Logger object. Based on Liferay logger
	 * {@link com.liferay.portal.kernel.log.Log} commons logging.
	 */
	private static Log logger = LogFactoryUtil.getLog(OrderProcessing.class);

	public final void lavoraCarrello(final ActionRequest areq,
			final ActionResponse ares) {
		
		final long cartId = ParamUtil.getLong(areq, "cartId", -1);
		logger.info("Set lavorato to cart: " + cartId);
		
		if (cartId > 0) {
			try {
				Carrello cart = CarrelloLocalServiceUtil.getCarrello(cartId);
				cart.setLavorato(!cart.getLavorato());
				CarrelloLocalServiceUtil.updateCarrello(cart);
			} catch (PortalException e) {
				logger.error(e.getMessage());
	            if (logger.isDebugEnabled()) {
	                e.printStackTrace();
	            }
			} catch (SystemException e) {
				logger.error(e.getMessage());
	            if (logger.isDebugEnabled()) {
	                e.printStackTrace();
	            }
			}
		}
		PortalUtil.copyRequestParameters(areq, ares);
		ares.setRenderParameter("jspPage", "/html/orderprocessing/order.jsp");
	}
}
	