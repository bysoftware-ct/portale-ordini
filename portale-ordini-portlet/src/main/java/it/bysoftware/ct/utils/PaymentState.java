package it.bysoftware.ct.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents payment state.
 * 
 * @author Mario Torrisi torrisi.mario@gmail.com
 *
 */
public enum PaymentState {
    /**
     * Generated payment.
     */
    GENERATED(1),

    /**
     * Sent payment board to all recipients.
     */
    SENT(2),

    /**
     * Payed.
     */
    PAYED(3),
    
    /**
     * Rewarded.
     */
    REWARDED(4);

    /**
     * Payment value.
     */
    private final int val;

    /**
     * Maps the payments with its own value.
     */
    private static Map<Integer, PaymentState> map =
            new HashMap<Integer, PaymentState>();

    /**
     * Creates a payment using its state.
     * 
     * @param v
     *            real state of the entry
     */
    PaymentState(final int v) {
        this.val = v;
    }

    static {
        for (PaymentState paymentState : PaymentState.values()) {
            map.put(paymentState.val, paymentState);
        }
    }

    /**
     * Returns the corresponding {@link PaymentState}.
     * 
     * @param state
     *            state value
     * @return the payment state
     */
    public static PaymentState valueOf(final int state) {
        return (PaymentState) map.get(state);
    }

    /**
     * Returns the payment state.
     * 
     * @return the payment state
     */
    public int getVal() {
        return val;
    }
}
