package it.bysoftware.ct.utils;

import it.bysoftware.ct.model.AnagraficheClientiFornitori;

import com.liferay.portal.kernel.util.OrderByComparator;

/**
 * Customer comparator.
 *
 * @author Mario Torrisi torrisi.mario@gmail.com
 */
public class SubjectComparator extends OrderByComparator {

    /**
     * Auto generated Serial ID.
     */
    private static final long serialVersionUID = 1648568202568749188L;
    
    /**
     * Ascendi or descending order type. 
     */
    private boolean ascending;
    
    /**
     * Ascending Order constant.
     */
    public static final String ORDER_BY_ASC =
            "AnagraficheClientiFornitori.ragioneSociale1 ASC";

    /**
     * Descending Order constant.
     */
    public static final String ORDER_BY_DESC =
            "AnagraficheClientiFornitori.ragioneSociale1 DESC";

    /**
     * Order by fields.
     */
    public static final String[] ORDER_BY_FIELDS = { "ragioneSociale1" };

    /**
     * Default constructor.
     */
    public SubjectComparator() {
        this(true);
    }

    /**
     * Other constructor.
     * 
     * @param order
     *            true for asceninding order false otherwise
     */
    public SubjectComparator(final boolean order) {
        this.ascending = order;
    }

    @Override
    public final int compare(final Object obj1, final Object obj2) {
        AnagraficheClientiFornitori c1 = (AnagraficheClientiFornitori) obj1;
        AnagraficheClientiFornitori c2 = (AnagraficheClientiFornitori) obj2;

        String c1Name = c1.getRagioneSociale1();
        String c2Name = c2.getRagioneSociale1();

        int value = c1Name.compareTo(c2Name);

        if (this.ascending) {
            return value;
        } else {
            return -value;
        }
    }

    @Override
    public final String getOrderBy() {
        if (this.ascending) {
            return ORDER_BY_ASC;
        } else {
            return ORDER_BY_DESC;
        }
    }

    @Override
    public final String[] getOrderByFields() {
        return ORDER_BY_FIELDS;
    }

    @Override
    public final boolean isAscending() {
        return this.ascending;
    }

}
