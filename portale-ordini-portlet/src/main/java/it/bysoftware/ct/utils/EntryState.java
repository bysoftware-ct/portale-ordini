package it.bysoftware.ct.utils;

/**
 * Represents the state of the entry.
 * 
 * @author Mario Torrisi torrisi.mario@gmail.com
 *
 */
public enum EntryState {
    /**
     * Open entry.
     */
    OPENED(1),
    /**
     * Exposed entry.
     */
    EXPOSED(2),
    /**
     * Expired entry.
     */
    EXPIRED(3),
    /**
     * Unsolved entry.
     */
    UNSOLVED(4),
    /**
     * Protested entry.
     */
    CONTENTIONS(5),
    /**
     * Closed entry.
     */
    CLOSED(6),
    /**
     * Replaced entry.
     */
    REPLACED(7);

    /**
     * Entry value.
     */
    private final int val;

    /**
     * Creates a entry using its state.
     * 
     * @param v
     *            real state of the entry
     */
    EntryState(final int v) {
        this.val = v;
    }

    /**
     * Returns the entry state.
     * 
     * @return the entry state
     */
    public int getVal() {
        return val;
    }
}
