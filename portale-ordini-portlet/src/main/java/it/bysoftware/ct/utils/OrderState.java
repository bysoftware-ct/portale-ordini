package it.bysoftware.ct.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Enum to manage order state.
 */
public enum OrderState {
    /**
     * Confirmed state.
     */
    CONFIRMED(0),
    
    /**
     * PENDING state.
     */
    PENDING(1),
    
    /**
     * Saved state.
     */
    SAVED(2),
    
    /**
     * Inactive state.
     */
    INACTIVE(5),
    
    /**
     * Scheduled state.
     */
    SCHEDULED(7),
    
    /**
     * Processed state.
     */
    PROCESSED(-1); 
    
    /**
     * Maps the {@link OrderState} to its value.
     */
    private static final Map<Integer, OrderState> ORDERSTATE_MAP =
            new HashMap<Integer, OrderState>();

    static {
        for (OrderState state : OrderState.values()) {
            ORDERSTATE_MAP.put(state.getValue(), state);
        }
    }

    /**
     * Returns the the corresponding {@link OrderState}.
     * 
     * @param i
     *            value
     * @return the {@link OrderState}
     */
    public static OrderState fromInt(final int i) {
        OrderState state = ORDERSTATE_MAP.get(Integer.valueOf(i));
        if (state == null) {
            return null;
        }
        return state;
    }
    
    /**
     * Order state.
     */
    private int state;

    /**
     * Creates an Order state with desired value.
     * 
     * @param s
     *            the order state.
     */
    private OrderState(final int s) {
        this.state = s;
    }

    /**
     * @return the value
     */
    public int getValue() {
        return state;
    }

}
