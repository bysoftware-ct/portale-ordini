package it.bysoftware.ct.utils;

import it.bysoftware.ct.model.ArticoloSocio;

import com.liferay.portal.kernel.util.OrderByComparator;

/**
 * Customer comparator.
 *
 * @author Mario Torrisi torrisi.mario@gmail.com
 */
public class ItemComparator extends OrderByComparator {

    /**
     * Auto generated Serial ID.
     */
    private static final long serialVersionUID = 3990870738448074293L;

    /**
     * Ascending or descending order type. 
     */
    private boolean ascending;
    
    /**
     * Ascending Order constant.
     */
    public static final String ORDER_BY_ASC =
            "ArticoloSocio.codiceArticolo ASC";

    /**
     * Descending Order constant.
     */
    public static final String ORDER_BY_DESC =
            "ArticoloSocio.codiceArticolo DESC";

    /**
     * Order by fields.
     */
    public static final String[] ORDER_BY_FIELDS = { "codiceArticolo" };

    /**
     * Default constructor.
     */
    public ItemComparator() {
        this(true);
    }

    /**
     * Other constructor.
     * 
     * @param order
     *            true for ascending order false otherwise
     */
    public ItemComparator(final boolean order) {
        this.ascending = order;
    }

    @Override
    public final int compare(final Object obj1, final Object obj2) {
        ArticoloSocio c1 = (ArticoloSocio) obj1;
        ArticoloSocio c2 = (ArticoloSocio) obj2;

        String c1Code = c1.getCodiceArticolo();
        String c2Code = c2.getCodiceArticolo();

        int value = c1Code.compareTo(c2Code);

        if (this.ascending) {
            return value;
        } else {
            return -value;
        }
    }

    @Override
    public final String getOrderBy() {
        if (this.ascending) {
            return ORDER_BY_ASC;
        } else {
            return ORDER_BY_DESC;
        }
    }

    @Override
    public final String[] getOrderByFields() {
        return ORDER_BY_FIELDS;
    }

    @Override
    public final boolean isAscending() {
        return this.ascending;
    }

}
