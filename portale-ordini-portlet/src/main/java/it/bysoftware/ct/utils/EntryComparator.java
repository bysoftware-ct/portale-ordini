package it.bysoftware.ct.utils;

import com.liferay.portal.kernel.util.OrderByComparator;

import java.util.Date;

import it.bysoftware.ct.model.ScadenzePartite;

/**
 * Entry comparator.
 *
 * @author Mario Torrisi torrisi.mario@gmail.com
 */
public class EntryComparator extends OrderByComparator {

    /**
     * Auto generated Serial ID.
     */
    private static final long serialVersionUID = 2208359617544311341L;

    /**
     * Ascending or descending order type. 
     */
    private boolean ascending;
    
    /**
     * Ascending Customer constant.
     */
    public static final String ORDER_BY_ASC =
            "ScadenzePartite.dataChiusura ASC";

    /**
     * Descending Order constant.
     */
    public static final String ORDER_BY_DESC =
            "ScadenzePartite.dataChiusura DESC";

    /**
     * Order by fields.
     */
    public static final String[] ORDER_BY_FIELDS = { "dataChiusura" };

    /**
     * Default constructor.
     */
    public EntryComparator() {
        this(true);
    }

    /**
     * Other constructor.
     * 
     * @param order
     *            true for ascending order false otherwise
     */
    public EntryComparator(final boolean order) {
        this.ascending = order;
    }

    @Override
    public final int compare(final Object obj1, final Object obj2) {
        ScadenzePartite o1 = (ScadenzePartite) obj1;
        ScadenzePartite o2 = (ScadenzePartite) obj2;

        Date o1Date = o1.getDataChiusura();
        Date o2Date = o2.getDataChiusura();

        int value = 0;
        if (o1Date.before(o2Date)) {
            value = 1;
        } else {
            value = -1;
        }

        if (this.ascending) {
            return value;
        } else {
            return -value;
        }
    }

    @Override
    public final String getOrderBy() {
        if (this.ascending) {
            return ORDER_BY_ASC;
        } else {
            return ORDER_BY_DESC;
        }
    }

    @Override
    public final String[] getOrderByFields() {
        return ORDER_BY_FIELDS;
    }

    @Override
    public final boolean isAscending() {
        return this.ascending;
    }

}
