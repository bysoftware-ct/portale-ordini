package it.bysoftware.ct.utils;

/**
 * Class that handles Response.
 * 
 * @author Mario Torrisi
 *
 */
public class Response {

    /**
     * Response Code.
     * 
     * @author Mario Torrisi
     */
    public enum Code {
        /**
         * Action completes correctly.
         */
        OK,
        /**
         * Duplicate key error.
         */
        GET_PRIMARY_KEY_ERROR,
        /**
         * Generic insert error.
         */
        INSERT_ERROR,
        /**
         * Parsing JSON error.
         */
        PARSING_JSON_ERROR,
        /**
         * Error occurred sending email.
         */
        SENDING_MAIL_ERROR,
        /**
         * Date format error.
         */
        DATE_FORMAT_ERROR,
        /**
         * Generic error.
         */
        GENERIC_ERROR,
        /**
         * Order acquired.
         */
        ACQUIRED,
        /**
         * Print order error.
         */
        PRINT_ERROR,
        /**
         * Save note error.
         */
        SAVE_NOTE_ERROR, 
        /**
         * Exceed height error.
         */
        EXCEED_HEIGHT_ERROR,
        /**
         * operation not permitted.
         */
        NOT_PERMITTED
    }

    /**
     * Response data.
     */
    private String data;
    
    /**
     * Response message.
     */
    private String message;

    /**
     * Response code attribute.
     */
    private int code;

    /**
     * Creates an empty response.
     */
    public Response() {
    }

    /**
     * Creates a response with given identifier and {@link Code}.
     * 
     * @param c
     *            response Code
     */
    public Response(final Code c) {
        this.code = c.ordinal();
    }

    /**
     * Creates a response with given identifier and {@link Code}.
     * 
     * @param c
     *            response Code
     * @param msg
     *            message of the new response
     */
    public Response(final Code c, final String msg) {
        this.code = c.ordinal();
        this.message = msg;
    }
    
    /**
     * Creates a response with given identifier and {@link Code}.
     * 
     * @param c
     *            response Code
     * @param msg
     *            message of the new response
     * @param obj
     *            data
     */
    public Response(final Code c, final String msg, final String obj) {
        this.code = c.ordinal();
        this.message = msg;
        this.data = obj;
    }

    /**
     * Returns response message.
     * 
     * @return response message
     */
    public final String getMessage() {
        return message;
    }

    /**
     * Sets response identifier.
     * 
     * @param msg
     *            response identifier
     */
    public final void setMessage(final String msg) {
        this.message = msg;
    }

    /**
     * Returns response {@link Code}.
     * 
     * @return response Code
     */
    public final int getCode() {
        return code;
    }

    /**
     * Returns response data.
     * 
     * @return the data
     */
    public final String getData() {
        return this.data;
    }

    /**
     * Sets response data.
     * 
     * @param obj
     *            the data to set
     */
    public final void setData(final String obj) {
        this.data = obj;
    }

    @Override
    public final String toString() {
        return "Response [message=" + this.message + ", code=" + this.code
                + "]";
    }
}

