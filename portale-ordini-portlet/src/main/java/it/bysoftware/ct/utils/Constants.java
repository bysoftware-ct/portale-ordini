/***********************************************************************
 * Copyright (c) 2016:
 * BySoftware Soc. coop. a.r.l., Belpasso (CT) - Italy
 *
 * See http://www.bysoftware.ct.it for details on the copyright holder.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***********************************************************************/

package it.bysoftware.ct.utils;

/**
 * This class contains all portlets constants. 
 *
 * @author Mario Torrisi torrisi.mario@gmail.com
 */
public final class Constants {
    
	/**
     * Add for label print constant.
     */
    public static final String ADD_PRINT_LABEL = "labelPrice";
	
	/**
     * Agent id constant.
     */
    public static final String AGENT_ID = "idAgente";
    /**
     * Amount constant.
     */
    public static final String AMOUNT = "importo";
    /**
     * Billing center constant. 
     */
    public static final String BILLING_CENTER = "billingCenter";
    
    /**
     * Carrier ID constant.
     */
    public static final String CARRIER_ID = "idTrasportatore";
    
    /**
     * Cart height constant.
     */
    public static final String CART_DEFAULT_HEIGHT = "cartHeight";
    
    /**
     * Cart id constant.
     */
    public static final String CART_ID = "cartId";
    
    /**
     * Cart type id constant.
     */
    public static final String CART_TYPE_ID = "idTipoCarrello";
    
    /**
     * Carts constant.
     */
    public static final String CARTS = "carts";
    
    /**
     * Closed state constant.
     */
    public static final String CLOSED = "chiuso";

    /**
     * Generic constant.
     */
    public static final String GENERIC = "generico";
    
    /**
     * Generic configuration command.
     */
    public static final String CMD = "cmd";
    
    /**
     * Role name for Azienda.
     */
    public static final String COMPANY = "Azienda";
    
    /**
     * Order creation date constant.
     */
    public static final String CREATION_DATE = "dataInserimento";
    
    /**
     * Credit board recipients list.
     */
    public static final String CREDIT_BOARD_CC_LIST = "emailList";
    
    /**
     * Customer id constant.
     */
    public static final String CUSTOMER_ID = "idCliente";
    
    /**
     * Customer note constant.
     */
    public static final String CUSTOMER_NOTE = "customerNote";

    /**
     * Delivery date constant.
     */
    public static final String DELIVERY_DATE = "dataConsegna";
    
    /**
     * Documents folder name.
     */
    public static final String DOCUMENTS_FOLDER_NAME = "documents";
    
    /**
     * Role name for Guest.
     */
    public static final String GUEST = "Guest";
    
    /**
     * Hundred constant.
     */
    public static final int HUNDRED = 100;
    
    /**
     * ID constant.
     */
    public static final String ID = "id";
    
    /**
     * Variant ID constant.
     */
    public static final String ID_VARIANT = "idVariant";
    
    /**
     * Item ID constants.
     */
    public static final String ITEM_ID = "idArticolo";
    
    /**
     * Loading place constant.
     */
    public static final String LOADING_PLACE = "luogoCarico";
    
    /**
     * Note constant.
     */
    public static final String NOTE = "note";
    
    /**
     * Cart number constant.
     */
    public static final String NUMBER = "progressivo";
    
    /**
     * 1GB constant.
     */
    public static final int ONE_GB = 1073741824;
    
    /**
     * Order constant.
     */
    public static final String ORDER = "order";
    
    /**
     * Order date constants.
     */
    public static final String ORDER_DATE = "dataInserimento";
    
    /**
     * Order ID constant.
     */
    public static final String ORDER_ID = "idOrdine";
    
    /**
     * Role name for Socio.
     */
    public static final String PARTNER = "Socio";
    
    /**
     * Item pictures folder name.
     */
    public static final String PICTURE_FOLDER_NAME = "pictures";
    
    /**
     * Picture URL prefix.
     */
    public static final String PICTURE_URL_PREFIX =
            "/portale-ordini-portlet/icons/";
    
    /**
     * Closed plants constant.
     */
    public static final String PLANTS = "plants";
    
    /**
     * Platform items constant.
     */
    public static final String PLATFORM_ITEMS = "pianteRipiano";
    
    /**
     * Print label key constant.
     */
    public static final String PRINT_LABELS = "stampaEtichette";

    /**
     * Platform items constant.
     */
    public static final String REGISTRY_CODE = "CodiceAnagrafica";

    /**
     * Rows constant.
     */
    public static final String ROWS = "rows";

    /**
     * Rows number constant.
     */
    public static final String ROWS_NUMBER = "rowsNumber";
    
    /**
     * Role name for Socio Speciale.
     */
    public static final String SPECIAL_PARTNER = "SocioS";

    /**
     * Role name for Socio con Contabilità.
     */
    public static final String SPECIAL_PARTNER_ACCOUNTING = "SocioCont";
    
    /**
     * Ordeer state constant.
     */
    public static final String STATE = "stato";

    /**
     * Update constant.
     */
    public static final String UPDATE = "update";
    
    /**
     * Cart used height constant.
     */
    public static final String USED_HEIGHT = "usedHeight";
    
    /**
     * User home folder.
     */
    public static final String USER_HOME = System.getProperty("user.home");
    
    /**
     * Variant price constant.
     */
    public static final String VARIANT_PRICE = "importoVarianti";

    /**
     * Variant price constant.
     */
    public static final String VAT_CODE = "vatSelected";
    
    /**
     * Year string constant.
     */
    public static final String YEAR = "year";
    
    /**
     * Ten constant.
     */
    public static final double TEN = 10;
    
    /**
     * Ten constant.
     */
    public static final int TWELVE = 12;
    
    /**
     * Ten constant.
     */
    public static final int THIRTEEN = 13;
    
    public static final int FOURTEEN = 14;
    
    /**
     * Year string constant.
     */
    public static final String GT_ORDER_DATE = "GTorderDate";
    
    /**
     * Year string constant.
     */
    public static final String GT_ORDER_NUM = "GTorderNum";
    
    /**
     * Avoid the class be istantiable.
     */
    private Constants() { }
}
