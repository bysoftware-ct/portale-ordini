package it.bysoftware.ct.utils;

import com.liferay.portal.kernel.util.OrderByComparator;

import java.util.Date;

import it.bysoftware.ct.model.MovimentoVuoto;

/**
 * Entry comparator.
 *
 * @author Mario Torrisi torrisi.mario@gmail.com
 */
public class MovementComparator extends OrderByComparator {

    /**
     * Auto generated Serial ID.
     */
    private static final long serialVersionUID = 1923792243151133797L;

    /**
     * Ascending or descending order type. 
     */
    private boolean ascending;
    
    /**
     * Ascending Customer constant.
     */
    public static final String ORDER_BY_ASC =
            "MovimentoVuoto.dataMovimento ASC";

    /**
     * Descending Order constant.
     */
    public static final String ORDER_BY_DESC =
            "MovimentoVuoto.dataMovimento DESC";

    /**
     * Order by fields.
     */
    public static final String[] ORDER_BY_FIELDS = { "dataMovimento" };

    /**
     * Default constructor.
     */
    public MovementComparator() {
        this(true);
    }

    /**
     * Other constructor.
     * 
     * @param order
     *            true for ascending order false otherwise
     */
    public MovementComparator(final boolean order) {
        this.ascending = order;
    }

    @Override
    public final int compare(final Object obj1, final Object obj2) {
        MovimentoVuoto o1 = (MovimentoVuoto) obj1;
        MovimentoVuoto o2 = (MovimentoVuoto) obj2;

        Date o1Date = o1.getDataMovimento();
        Date o2Date = o2.getDataMovimento();

        int value = 0;
        if (o1Date.before(o2Date)) {
            value = 1;
        } else {
            value = -1;
        }

        if (this.ascending) {
            return value;
        } else {
            return -value;
        }
    }

    @Override
    public final String getOrderBy() {
        if (this.ascending) {
            return ORDER_BY_ASC;
        } else {
            return ORDER_BY_DESC;
        }
    }

    @Override
    public final String[] getOrderByFields() {
        return ORDER_BY_FIELDS;
    }

    @Override
    public final boolean isAscending() {
        return this.ascending;
    }

}
