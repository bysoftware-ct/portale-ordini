/***********************************************************************
 * Copyright (c) 2016:
 * BySoftware Soc. coop. a.r.l., Belpasso (CT) - Italy
 *
 * See http://www.bysoftware.ct.it for details on the copyright holder.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***********************************************************************/

package it.bysoftware.ct.utils;

import it.bysoftware.ct.comparators.OrderAmountComparator;
import it.bysoftware.ct.comparators.OrderCarrierComparator;
import it.bysoftware.ct.comparators.OrderCustomerComparator;
import it.bysoftware.ct.comparators.OrderDateComparator;
import it.bysoftware.ct.comparators.OrderNumberComparator;
import it.bysoftware.ct.comparators.OrderPaymentComparator;
import it.bysoftware.ct.comparators.OrderStateComparator;
import it.bysoftware.ct.comparators.OrderShipDateComparator;
import it.bysoftware.ct.comparators.PlantsByCodeComparator;
import it.bysoftware.ct.comparators.PlantsByNameComparator;
import it.bysoftware.ct.model.AnagraficheClientiFornitori;
import it.bysoftware.ct.model.Piante;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.NoSuchResourcePermissionException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ResourceAction;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.model.ResourcePermission;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.ResourceActionLocalServiceUtil;
import com.liferay.portal.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.model.DLFolderConstants;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;
import com.opencsv.CSVWriter;

/**
 * Provide some Utilities to deal with Document Library.
 * 
 * @author Mario Torrisi torrisi.mario@gmail.com
 *
 */
public final class Utils {

    /**
     * Check file name.
     */
    private static final String CHECK_FILE = "check-file";

    /**
     * Default constructor to avoid class instatiation.
     */
    private Utils() {
    }

    /**
     * Returns current year.
     * 
     * @return current year
     */
    public static int getYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    /**
     * Returns Azienda {@link DLFolder} folder.
     * 
     * @param groupId
     *            azienda groupId
     * @param azienda
     *            {@link User} user
     * @return the {@link DLFolder}
     * @throws PortalException
     *             if {@link PortalException}
     * @throws SystemException
     *             if {@link SystemException}
     */
    public static DLFolder getAziendaFolder(final long groupId,
            final User azienda) throws PortalException, SystemException {
        return DLFolderLocalServiceUtil.getFolder(groupId,
                DLFolderConstants.DEFAULT_PARENT_FOLDER_ID,
                azienda.getScreenName());
    }
    
    /**
     * Returns pictures {@link DLFolder} folder.
     * 
     * @param groupId
     *            agent grouId
     * @param aziendaFolder
     *            azienda {@link DLFolder} folder
     * @return the {@link DLFolder}
     * @throws PortalException
     *             if {@link PortalException}
     * @throws SystemException
     *             if {@link SystemException}
     */
    public static DLFolder getPicturesFolder(final long groupId,
            final DLFolder aziendaFolder) throws PortalException,
            SystemException {
        return DLFolderLocalServiceUtil.getFolder(groupId,
                aziendaFolder.getFolderId(), Constants.PICTURE_FOLDER_NAME);
    }
    
    /**
     * Sets {@link ActionKeys.VIEW} permission to the the file.
     * 
     * @param fileEntry
     *            the file
     * @throws SystemException system exception
     * @throws PortalException portal exception
     */
    public static void setFilePermissions(final FileEntry fileEntry)
            throws PortalException, SystemException {
        ResourcePermission resourcePermission = null;
        final Role siteMemberRole = RoleLocalServiceUtil.getRole(
                fileEntry.getCompanyId(), Constants.GUEST);
        ResourceAction resourceAction = ResourceActionLocalServiceUtil.
                getResourceAction(DLFileEntry.class.getName(),
                        ActionKeys.VIEW);
        try {
            resourcePermission = ResourcePermissionLocalServiceUtil.
                    getResourcePermission(fileEntry.getCompanyId(),
                            DLFileEntry.class.getName(),
                            ResourceConstants.SCOPE_INDIVIDUAL,
                            String.valueOf(fileEntry.getPrimaryKey()),
                            siteMemberRole.getRoleId());

            if (Validator.isNotNull(resourcePermission)) {

                resourcePermission.setActionIds(resourceAction.
                        getBitwiseValue());
                ResourcePermissionLocalServiceUtil.
                        updateResourcePermission(resourcePermission);
            }
        } catch (NoSuchResourcePermissionException e) {

            resourcePermission = ResourcePermissionLocalServiceUtil.
                    createResourcePermission(CounterLocalServiceUtil.
                            increment());
            resourcePermission.setCompanyId(fileEntry.getCompanyId());
            resourcePermission.setName(DLFileEntry.class.getName());
            resourcePermission.setScope(ResourceConstants.SCOPE_INDIVIDUAL);
            resourcePermission.setPrimKey(String.valueOf(fileEntry.
                    getPrimaryKey()));
            resourcePermission.setRoleId(siteMemberRole.getRoleId());
            resourcePermission.setActionIds(resourceAction.getBitwiseValue());
            ResourcePermissionLocalServiceUtil.
                    addResourcePermission(resourcePermission);
        }
    }

    /**
     * Retrieves item picture URL. 
     * @param p item
     * @return the picture id
     * @throws SystemException exception
     * @throws PortalException exception
     */
    public static String retrievePictureURL(final Piante p)
            throws PortalException, SystemException {
        long imgId = -1;
        FileEntry image = null;

        try {
            imgId = Long.parseLong(p.getFoto1());
        } catch (NumberFormatException ex) {
            return Constants.PICTURE_URL_PREFIX + "not-found.png";
        }
        
        image = DLAppLocalServiceUtil.getFileEntry(imgId);
        return Constants.DOCUMENTS_FOLDER_NAME + File.separator
                + image.getRepositoryId() + File.separator
                + image.getFolderId() + File.separator + image.getTitle();
    }
    
    /**
     * Returns the customer full address.
     * 
     * @param c {@link AnagraficheClientiFornitori} customer
     * @return full address
     */
    public static String getFullAddress(final AnagraficheClientiFornitori c) {
        return c.getIndirizzo() + "\n" + c.getCAP() + " - " + c.getComune()
               + " (" + c.getSiglaProvincia() + ") - " + c.getSiglaStato(); 
    }
    
    /**
     * Rounds a double to the specified number of digits.
     * 
     * @param number
     *            number to round
     * @param decimalDigits
     *            number of decimal digits
     * @return rounded number
     */
    public static double roundDouble(final double number,
            final double decimalDigits) {
        return (double) Math.round(number
                * Math.pow(Constants.TEN, decimalDigits))
                / Math.pow(Constants.TEN, decimalDigits);
    }
    
    /**
     * Updates the check file.
     * 
     * @param exportFolder
     *            export folder
     * @param documents
     *            the document exported
     * @param msg
     *            an info message to be printed
     * @throws IOException
     *             exception
     */
    public static void updateCheckFile(final File exportFolder,
            final Object[] documents, final String msg) throws IOException {
        File checkFile = new File(exportFolder + File.separator + CHECK_FILE);
        
        Calendar lastEdit = Calendar.getInstance();
        lastEdit.setTime(new Date(checkFile.lastModified()));
        Calendar today = Calendar.getInstance();
        FileWriter fw;
        if (lastEdit.get(Calendar.YEAR) == today.get(Calendar.YEAR)
                && lastEdit.get(Calendar.DAY_OF_YEAR) < today.
                        get(Calendar.DAY_OF_YEAR)) {
            File destination = new File(exportFolder + File.separator
                    + CHECK_FILE + "_" + today.getTimeInMillis());
            FileUtil.move(checkFile, destination);
            fw = new FileWriter(checkFile.getAbsoluteFile());
        } else {
            fw = new FileWriter(checkFile.getAbsoluteFile(), true);
        }
        BufferedWriter bw = new BufferedWriter(fw);
        PrintWriter out = new PrintWriter(bw);
        String s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(
                new Date()) + " EXPORTED " + msg + ":\n\t["
                + StringUtil.merge(documents, ",\n\t") + "]";
        out.println(s);
        out.close();
        bw.close();
    }
    
    /**
     * Returns today date at midnight.
     * 
     * @return today date at midnight
     */
    public static Calendar today() {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        return today;
    }
    
    /**
     * Returns the external IP address.
     * 
     * @return the external IP address.
     */
    public static String getExternalIp() {

        String result = "";
        try {
            URL whatismyip = new URL("http://checkip.amazonaws.com");
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    whatismyip.openStream()));

            result = in.readLine();
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
         return result;
    }
    
    /**
     * Convert an excel file to csv format with ';' separator.
     * 
     * @param sheet
     *            the sheet is going to be converted
     * @return the csv file
     * @throws IOException
     *             exception
     */
    public static File echoAsCSV(final Sheet sheet) throws IOException {
        File f = FileUtil.createTempFile("csv");
        CSVWriter writer = new CSVWriter(new FileWriter(f), ';');
        List<String[]> data = new ArrayList<String[]>();
        Row row = null;
        int columns = 0;
        for (int i = 0; i <= sheet.getLastRowNum(); i++) {
            row = sheet.getRow(i);
            if (i == 0) {
                columns = row.getLastCellNum();
            }
            String[] cells = null; //new String[row.getLastCellNum()];
            if (columns == row.getLastCellNum()) {
                cells = new String[row.getLastCellNum()];
            } else {
                cells = new String[row.getLastCellNum()
                                   + (columns - row.getLastCellNum())];
            }
            for (int j = 0; j < row.getLastCellNum()
                    + (columns - row.getLastCellNum()); j++) {
                if (j < row.getLastCellNum()) {
                    cells[j] = row.getCell(j).toString();
                } else {
                    cells[j] = "";
                }
            }
            
            data.add(cells);
        }
        writer.writeAll(data);
        writer.close();
        return f;
    }
    
    public static File compressImage(File imageFile, File compressedImageFile) throws IOException {

		InputStream is = new FileInputStream(imageFile);
		OutputStream os = new FileOutputStream(compressedImageFile);

		float quality = 0.5f;

		// create a BufferedImage as the result of decoding the supplied InputStream
		BufferedImage image = ImageIO.read(is);

		// get all image writers for JPG format
		Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName("jpg");

		if (!writers.hasNext()) {
			os.close();
			throw new IllegalStateException("No writers found");
		}

		ImageWriter writer = (ImageWriter) writers.next();
		ImageOutputStream ios = ImageIO.createImageOutputStream(os);
		writer.setOutput(ios);

		ImageWriteParam param = writer.getDefaultWriteParam();

		// compress to a given quality
		param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		param.setCompressionQuality(quality);

		// appends a complete image stream containing a single image and
	    //associated stream and image metadata and thumbnails to the output
		writer.write(null, new IIOImage(image, null, null), param);

		// close all streams
		is.close();
		os.close();
		ios.close();
		writer.dispose();
		return compressedImageFile;
    }
    
	public static OrderByComparator getOrderByComparator(String orderByCol,
			String orderByType) {

		boolean orderByAsc = false;

		if (orderByType.equals("asc")) {
			orderByAsc = true;
		}

		OrderByComparator orderByComparator = null;

		if (orderByCol.equalsIgnoreCase("ship-date")) {
			orderByComparator = new OrderShipDateComparator(orderByAsc);
		} else if (orderByCol.equalsIgnoreCase("order-date")) {
			orderByComparator = new OrderDateComparator(orderByAsc);
		} else if (orderByCol.equalsIgnoreCase("number")) {
			orderByComparator = new OrderNumberComparator(orderByAsc);
		} else if (orderByCol.equalsIgnoreCase("amount")) {
			orderByComparator = new OrderAmountComparator(orderByAsc);
		} else if (orderByCol.equalsIgnoreCase("company-name")) {
			orderByComparator = new OrderCustomerComparator(orderByAsc);
		} else if (orderByCol.equalsIgnoreCase("status")) {
			orderByComparator = new OrderStateComparator(orderByAsc);
		} else if (orderByCol.equalsIgnoreCase("carrier")) {
			orderByComparator = new OrderCarrierComparator(orderByAsc);
		} else if (orderByCol.equalsIgnoreCase("payment")) {
			orderByComparator = new OrderPaymentComparator(orderByAsc);
		}

		return orderByComparator;
	}
	
	public static OrderByComparator getPianteByComparator(String orderByCol,
			String orderByType) {

		boolean orderByAsc = false;

		if (orderByType.equals("asc")) {
			orderByAsc = true;
		}

		OrderByComparator orderByComparator = null;

		if (orderByCol.equalsIgnoreCase("code")) {
			orderByComparator = new PlantsByCodeComparator(orderByAsc);
		} else if (orderByCol.equalsIgnoreCase("name")) {
			orderByComparator = new PlantsByNameComparator(orderByAsc);
		} 

		return orderByComparator;
	}
	
	public static String extractFileNameWithoutExtension(String filePath) {
        // Parse the file path
        Path path = Paths.get(filePath);

        // Get the file name without extension
        String fileName = path.getFileName().toString();
        int dotIndex = fileName.lastIndexOf('.');
        if (dotIndex != -1) {
            return fileName.substring(0, dotIndex);
        } else {
            return fileName;
        }
    }
	
	public static boolean isLastChar(char targetChar, String inputString) {
        if (inputString != null && !inputString.isEmpty()) {
            char lastChar = inputString.charAt(inputString.length() - 1);
            return lastChar == targetChar;
        }
        return false; 
    }
	
	public static String escapeCsv(String value, String separator) {
		if (value != null && value.contains(separator)) {
			return "\"" + value + "\"";
		}
		return value;
	}
}
