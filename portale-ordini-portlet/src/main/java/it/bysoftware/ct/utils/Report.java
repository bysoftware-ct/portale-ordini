package it.bysoftware.ct.utils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.model.Image;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ImageLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;

import it.bysoftware.ct.model.CodiceConsorzio;
import it.bysoftware.ct.model.TestataDocumento;
import it.bysoftware.ct.service.CodiceConsorzioLocalServiceUtil;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 * Utility class to manage reports printing.
 * 
 * @author Mario Torrisi torrisi.mario@gmail.com
 */
public class Report {

    /**
     * Compiled report extension.
     */
    private static final String JASPER = ".jasper";

    /**
     * Report file extension.
     */
    private static final String JRXML = ".jrxml";

    /**
     * Compiled report folder name.
     */
    private static final String JASPER_REPORT_FOLDER = Constants.USER_HOME
            + File.separator + "ITS";

    /**
     * Sub-reports folder name.
     */
    private static final String SUBREPORTS_FOLDER = "subreports";

    /**
     * Summary report name suffix constant.
     */
    private static final String SUMMARY = "-summary";
    
    /**
     * Subreport name suffix constant.
     */
    private static final String SUBREPORT = "-subreport";
    
    /**
     * Subreport name suffix constant.
     */
    private static final String DETAILS = "-details";
    
    /**
     * Subreport name suffix constant.
     */
    private static final String MASTER = "-master";
    
    /**
     * Subreport name suffix constant.
     */
    private static final String NOTE = "-note";
    
    /**
     * VAT summary report name constant.
     */
    private static final String VAT_SUMMARY = "castelletto_iva";
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(Report.class);
    
    /**
     * Connection taken from data source.
     */
    private Connection conn;
    
    /**
     * Create an object to print report files in PDF format.
     * 
     * @param c
     *            database connection
     */
    public Report(final Connection c) {
        this.conn = c;
    }

    /**
     * Creates a report file with the order confirmation.
     * 
     * @param userId
     *            company user Id
     * @param logo
     *            company logo to be printed on the confirmation
     * @param orderId
     *            the order id is going to be printed out
     * @param reportName
     *            report name
     * @param suppId
     *            supplier code if report is addressed to a supplier, 0
     *            otherwise
     * @param summary
     *            true to print summary false otherwise
     * @return the order confirmation file
     * @throws JRException
     *             exception
     */
    public final String print(final long userId, final String logo,
            final long orderId, final String reportName, final String suppId,
            final boolean summary) throws JRException {
        Map<String, Object> parametersMap = new HashMap<String, Object>();
        if (!logo.isEmpty()) {
            parametersMap.put("logo", logo + ".jpg");
        }
        parametersMap.put("orderId", orderId);
        if (!suppId.isEmpty()) {
            parametersMap.put("supplier_id", suppId);
        }

        parametersMap.put(JRParameter.REPORT_LOCALE, new Locale("it", "IT"));
        
        JasperDesign jasperDesign = JRXmlLoader.load(JASPER_REPORT_FOLDER
                + File.separator + userId + File.separator + reportName
                + Report.JRXML);
        
        JasperCompileManager.compileReportToFile(jasperDesign,
                JASPER_REPORT_FOLDER + File.separator + userId + File.separator
                        + reportName + Report.JASPER);

        if (summary) {
            jasperDesign = JRXmlLoader.load(JASPER_REPORT_FOLDER
                    + File.separator + userId + File.separator
                    + Report.SUBREPORTS_FOLDER + File.separator + reportName
                    + Report.SUMMARY + Report.JRXML);
            
            JasperCompileManager.compileReportToFile(jasperDesign,
                    JASPER_REPORT_FOLDER + File.separator + userId
                            + File.separator + Report.SUBREPORTS_FOLDER
                            + File.separator + reportName + Report.SUMMARY
                            + Report.JASPER);
        }

        JasperPrint jp = JasperFillManager.fillReport(JASPER_REPORT_FOLDER
                + File.separator + userId + File.separator + reportName
                + Report.JASPER, parametersMap, conn);

        File file = FileUtil.createTempFile("pdf");
        this.logger.debug(file.getPath());
        JasperExportManager.exportReportToPdfFile(jp, file.getPath());

        return file.getPath();
    }
    
    public String print(long userId, String logo,
			Map<String, Object> parametersMap, String reportName,
			String subReportName) throws JRException {
        
        JasperDesign jasperDesign = JRXmlLoader.load(JASPER_REPORT_FOLDER
                + File.separator + userId + File.separator + reportName
                + Report.JRXML);
        
        JasperCompileManager.compileReportToFile(jasperDesign,
                JASPER_REPORT_FOLDER + File.separator + userId + File.separator
                        + reportName + Report.JASPER);
        jasperDesign = JRXmlLoader.load(JASPER_REPORT_FOLDER
                + File.separator + userId + File.separator
                + Report.SUBREPORTS_FOLDER + File.separator + subReportName
                + Report.JRXML);

        JasperCompileManager.compileReportToFile(jasperDesign,
                JASPER_REPORT_FOLDER + File.separator + userId
                        + File.separator + Report.SUBREPORTS_FOLDER
                        + File.separator + subReportName + Report.JASPER);

        JasperPrint jp = JasperFillManager.fillReport(JASPER_REPORT_FOLDER
                + File.separator + userId + File.separator + reportName
                + Report.JASPER, parametersMap, conn);

        File file = FileUtil.createTempFile("pdf");
        this.logger.debug(file.getPath());
        JasperExportManager.exportReportToPdfFile(jp, file.getPath());

        return file.getPath();
	}

	public String print(long userId, String logo,
			Map<String, Object> parametersMap, String reportName) throws JRException {
        
        JasperDesign jasperDesign = JRXmlLoader.load(JASPER_REPORT_FOLDER
                + File.separator + userId + File.separator + reportName
                + Report.JRXML);
        
        JasperCompileManager.compileReportToFile(jasperDesign,
                JASPER_REPORT_FOLDER + File.separator + userId + File.separator
                        + reportName + Report.JASPER);
        jasperDesign = JRXmlLoader.load(JASPER_REPORT_FOLDER
                + File.separator + userId + File.separator
                + Report.SUBREPORTS_FOLDER + File.separator + reportName
                + Report.SUMMARY + Report.JRXML);
        
        JasperCompileManager.compileReportToFile(jasperDesign,
                JASPER_REPORT_FOLDER + File.separator + userId
                        + File.separator + Report.SUBREPORTS_FOLDER
                        + File.separator + reportName + Report.SUMMARY
                        + Report.JASPER);

        JasperPrint jp = JasperFillManager.fillReport(JASPER_REPORT_FOLDER
                + File.separator + userId + File.separator + reportName
                + Report.JASPER, parametersMap, conn);

        File file = FileUtil.createTempFile("pdf");
        this.logger.debug(file.getPath());
        JasperExportManager.exportReportToPdfFile(jp, file.getPath());

        return file.getPath();
	}

	public String print(long userId, Map<String, Object> parametersMap,
			String reportName) throws JRException {
        
		for (String k : parametersMap.keySet()) {
			this.logger.debug("KEY: " + k + ", VALUE: " + parametersMap.get(k));
		}
		
        JasperDesign jasperDesign = JRXmlLoader.load(JASPER_REPORT_FOLDER
                + File.separator + userId + File.separator + reportName
                + Report.JRXML);
        
        JasperCompileManager.compileReportToFile(jasperDesign,
                JASPER_REPORT_FOLDER + File.separator + userId + File.separator
                        + reportName + Report.JASPER);
//        jasperDesign = JRXmlLoader.load(JASPER_REPORT_FOLDER
//                + File.separator + userId + File.separator
//                + Report.SUBREPORTS_FOLDER + File.separator + reportName
//                + Report.SUMMARY + Report.JRXML);
//        
//        JasperCompileManager.compileReportToFile(jasperDesign,
//                JASPER_REPORT_FOLDER + File.separator + userId
//                        + File.separator + Report.SUBREPORTS_FOLDER
//                        + File.separator + reportName + Report.SUMMARY
//                        + Report.JASPER);

        JasperPrint jp = JasperFillManager.fillReport(JASPER_REPORT_FOLDER
                + File.separator + userId + File.separator + reportName
                + Report.JASPER, parametersMap, conn);

        File file = FileUtil.createTempFile("pdf");
        this.logger.debug(file.getPath());
        JasperExportManager.exportReportToPdfFile(jp, file.getPath());

        return file.getPath();
	}
	
    /**
     * Creates a report file with the returnable movements.
     * 
     * @param userId
     *            company user Id
     * @param logo
     *            company logo to be printed on the confirmation
     * @param reportName
     *            report name
     * @param suppId
     *            supplier code if report is addressed to a supplier, 0
     *            otherwise
     * @return the order confirmation file
     * @throws JRException
     *             exception
     */
    public final String print(final long userId, final String logo,
            final String reportName, final String suppId, final String num,
            final boolean empty, final Date date) throws JRException {
        Map<String, Object> parametersMap = new HashMap<String, Object>();
        if (!logo.isEmpty()) {
            parametersMap.put("logo", logo + ".jpg");
        }
        parametersMap.put("codice_sogetto", suppId);
        
        if (reportName.contains("returnable")) {
            parametersMap.put("azienda", logo.toUpperCase());
        }
        
        parametersMap.put("uuid", num);
        parametersMap.put("empty", empty);
        parametersMap.put("data_stampa", date);
        parametersMap.put(JRParameter.REPORT_LOCALE, new Locale("it", "IT"));
        
        JasperDesign jasperDesign = JRXmlLoader.load(JASPER_REPORT_FOLDER
                + File.separator + userId + File.separator + reportName
                + Report.JRXML);
        
        JasperCompileManager.compileReportToFile(jasperDesign,
                JASPER_REPORT_FOLDER + File.separator + userId + File.separator
                        + reportName + Report.JASPER);

        if (reportName.contains("returnable")) {
            jasperDesign = JRXmlLoader.load(JASPER_REPORT_FOLDER
                    + File.separator + userId + File.separator + reportName
                    + Report.MASTER + Report.JRXML);
            
            JasperCompileManager.compileReportToFile(jasperDesign,
                    JASPER_REPORT_FOLDER + File.separator + userId
                            + File.separator +  reportName + Report.MASTER
                            + Report.JASPER);
            
            jasperDesign = JRXmlLoader.load(JASPER_REPORT_FOLDER
                    + File.separator + userId + File.separator + reportName
                    + Report.DETAILS + Report.JRXML);
            JasperCompileManager.compileReportToFile(jasperDesign,
                    JASPER_REPORT_FOLDER + File.separator + userId
                            + File.separator +  reportName + Report.DETAILS
                            + Report.JASPER);
            jasperDesign = JRXmlLoader.load(JASPER_REPORT_FOLDER
                    + File.separator + userId + File.separator + reportName
                    + Report.NOTE + Report.JRXML);
            JasperCompileManager.compileReportToFile(jasperDesign,
                    JASPER_REPORT_FOLDER + File.separator + userId
                            + File.separator +  reportName + Report.NOTE
                            + Report.JASPER);
        }
        
        jasperDesign = JRXmlLoader.load(JASPER_REPORT_FOLDER
                + File.separator + userId + File.separator
                + Report.SUBREPORTS_FOLDER + File.separator + reportName
                + Report.SUBREPORT + Report.JRXML);
        
        JasperCompileManager.compileReportToFile(jasperDesign,
                JASPER_REPORT_FOLDER + File.separator + userId
                        + File.separator + Report.SUBREPORTS_FOLDER
                        + File.separator + reportName + Report.SUBREPORT
                        + Report.JASPER);

        JasperPrint jp = JasperFillManager.fillReport(JASPER_REPORT_FOLDER
                + File.separator + userId + File.separator + reportName
                + Report.JASPER, parametersMap, conn);

        File file = FileUtil.createTempFile("pdf");
        this.logger.debug(file.getPath());
        JasperExportManager.exportReportToPdfFile(jp, file.getPath());

        return file.getPath();
    }

    /**
     * Creates the transport document report.
     * 
     * @param document
     *            the document
     * @param companyId
     *            the company ID
     * @param reportName
     *            the reportName
     * @param invoice
     *            true if is invoice false otherwise
     * @return the report path file
     * @throws SystemException
     *             exception
     * @throws JRException
     *             exception
     * @throws PortalException
     *             exception
     * @throws IOException exception
     */
    public final File printTranspDoc(final TestataDocumento document,
            final long companyId, final String reportName,
            final boolean invoice) throws SystemException, JRException,
            PortalException, IOException {

        Map<String, Object> parametersMap = new HashMap<String, Object>();
        parametersMap.put("RfiAnno", document.getAnno());
        parametersMap.put("RfiCodatt", document.getCodiceAttivita());
        parametersMap.put("RfiCodcen", document.getCodiceCentro());
        parametersMap.put("RfiCoddep", document.getCodiceDeposito());
        parametersMap.put("RfiProtoc", document.getProtocollo());
        parametersMap.put("RfiTipdoc", document.getTipoDocumento());
        parametersMap.put("RfiCodfor", document.getCodiceFornitore());
        parametersMap.put(JRParameter.REPORT_LOCALE, new Locale("it", "IT"));
        
        String logo = "";
        CodiceConsorzio code = CodiceConsorzioLocalServiceUtil.
                fetchCodiceConsorzio(document.getCodiceFornitore());
        
        if (code != null) {
            User partner = UserLocalServiceUtil.fetchUser(code.getIdLiferay());
            if (partner != null) {
                //logo = partner.getScreenName();
                long portraitId = partner.getPortraitId();
                Image image = ImageLocalServiceUtil.getImage(portraitId);
                if (image != null) {
                    File f = FileUtil.createTempFile(image.getTextObj());
                    logo = f.getPath();
                }
            }
        }
        parametersMap.put("logo", logo);
        
        User company = null;
        Role role = RoleLocalServiceUtil.getRole(companyId, "Azienda");
        long[] ids = UserLocalServiceUtil.getRoleUserIds(role.getRoleId());
        for (int i = 0; i < ids.length; i++) {
            User u = UserLocalServiceUtil.getUserById(ids[i]);
            if (u.getCompanyId() == companyId) {
                company = u;
                break;
            }
        }
        
        JasperDesign jasperDesign = JRXmlLoader.load(JASPER_REPORT_FOLDER
                + File.separator + company.getUserId() + File.separator
                + reportName + Report.JRXML);
        
        JasperCompileManager.compileReportToFile(jasperDesign,
                JASPER_REPORT_FOLDER + File.separator + company.getUserId() 
                + File.separator + reportName + Report.JASPER);
        
        if (invoice) {
            jasperDesign = JRXmlLoader.load(JASPER_REPORT_FOLDER
                    + File.separator + company.getUserId() + File.separator
                    + Report.SUBREPORTS_FOLDER + File.separator
                    + Report.VAT_SUMMARY + Report.JRXML);
            
            JasperCompileManager.compileReportToFile(jasperDesign,
                    JASPER_REPORT_FOLDER + File.separator + company.getUserId()
                            + File.separator + Report.SUBREPORTS_FOLDER
                            + File.separator + Report.VAT_SUMMARY
                            + Report.JASPER);
        }
        
        JasperPrint jp = JasperFillManager.fillReport(JASPER_REPORT_FOLDER
                + File.separator + company.getUserId() + File.separator
                + reportName + Report.JASPER, parametersMap, conn);

        File file = FileUtil.createTempFile("pdf");
        this.logger.debug(file.getPath());
        JasperExportManager.exportReportToPdfFile(jp, file.getPath());
        return file;
    }
    

    /**
     * Prints payment board report.
     * 
     * @param partnerCode
     *            partner code
     * @param credit
     *            credit
     * @param prevBalance
     *            previous balance
     * @param companyId
     *            company Id
     * @param reportName
     *            report name
     * @return return the report file
     * @throws PortalException
     *             exception
     * @throws SystemException
     *             exception
     * @throws JRException
     *             exception
     */
    public final File printPaymentBoard(final String partnerCode,
            final double credit, final double prevBalance, final long companyId,
            final String reportName) throws PortalException, SystemException,
            JRException {

        Map<String, Object> parametersMap = new HashMap<String, Object>();
        parametersMap.put("partnerCode", partnerCode);
        parametersMap.put("credit", credit);
        parametersMap.put("prevBalance", prevBalance);
        parametersMap.put(JRParameter.REPORT_LOCALE, new Locale("it", "IT"));
        
        User company = null;
        Role role = RoleLocalServiceUtil.getRole(companyId, "Azienda");
        long[] ids = UserLocalServiceUtil.getRoleUserIds(role.getRoleId());
        for (int i = 0; i < ids.length; i++) {
            User u = UserLocalServiceUtil.getUserById(ids[i]);
            if (u.getCompanyId() == companyId) {
                company = u;
                break;
            }
        }
        
        JasperDesign jasperDesign = JRXmlLoader.load(JASPER_REPORT_FOLDER
                + File.separator + company.getUserId() + File.separator
                + reportName + Report.JRXML);
        
        JasperCompileManager.compileReportToFile(jasperDesign,
                JASPER_REPORT_FOLDER + File.separator + company.getUserId() 
                + File.separator + reportName + Report.JASPER);
        
        JasperPrint jp = JasperFillManager.fillReport(JASPER_REPORT_FOLDER
                + File.separator + company.getUserId() + File.separator
                + reportName + Report.JASPER, parametersMap, conn);

        File file = FileUtil.createTempFile("pdf");
        this.logger.debug(file.getPath());
        JasperExportManager.exportReportToPdfFile(jp, file.getPath());
        return file;
    }

    /**
     * Closes the current connection to the database.
     * 
     * @throws SQLException
     *             exception
     */
    public final void closeConnection() throws SQLException {
        this.conn.close();
    }

	public String print(long userId, String screenName, String reportName,
			String codiceAnagrafica, Date from, Date to, String string) throws JRException {
		Map<String, Object> parametersMap = new HashMap<String, Object>();
        parametersMap.put("partner_code", codiceAnagrafica);
        parametersMap.put("from", from);
        parametersMap.put("to", to);
        parametersMap.put("partner", string);
        parametersMap.put(JRParameter.REPORT_LOCALE, new Locale("it", "IT"));
        
        JasperDesign jasperDesign = JRXmlLoader.load(JASPER_REPORT_FOLDER
                + File.separator + userId + File.separator
                + reportName + Report.JRXML);
        
        JasperCompileManager.compileReportToFile(jasperDesign,
                JASPER_REPORT_FOLDER + File.separator + userId 
                + File.separator + reportName + Report.JASPER);
        
        JasperPrint jp = JasperFillManager.fillReport(JASPER_REPORT_FOLDER
                + File.separator + userId + File.separator
                + reportName + Report.JASPER, parametersMap, conn);

        File file = FileUtil.createTempFile("pdf");
        this.logger.info(file.getPath());
        JasperExportManager.exportReportToPdfFile(jp, file.getPath());
        return file.getPath();
	}
}
