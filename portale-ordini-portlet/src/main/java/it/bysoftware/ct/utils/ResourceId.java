package it.bysoftware.ct.utils;

/**
 * Enum to manage resource id.
 */
public enum ResourceId {
    
    /**
     * Add cart ID.
     */
    addCart,
    
    /**
     * Add platform ID.
     */
    addPlatform,
    
    /**
     * Add quantity ID.
     */
    addQuantity,
    
    /**
     * Lock cart.
     */
    lockCart, 
    
    /**
     * Remove platform cart.
     */
    removePlatform,
    
    /**
     * Auto complete ID.
     */
    autoComplete, 
    
    /**
     * Save order ID.
     */
    saveOrder,
    
    /**
     * Save order ID.
     */
    saveMovements,
    
    /**
     * Save association ID.
     */
    saveAssociation,
    
    /**
     * Save invoice ID.
     */
    saveInvoice,
    
    /**
     * Select entries ID.
     */
    selectEntries,
    
    /**
     * Select entries ID.
     */
    selectInvoices,
    
    /**
     * Send confirm ID.
     */
    sendConfirm,
    
    /**
     * Send confirm ID.
     */
    sendMovements,
    
    /**
     * Print cart label ID.
     */
    printCartLabel,
    
    /**
     * Print cart label ID.
     */
    printMovements,
    
    /**
     * Print cart label ID.
     */
    printListItem,
    
    /**
     * Delete cart ID.
     */
    deleteCart, 
    
    /**
     * Delete picture.
     */
    deletePicture,
    
    /**
     * Add item over.
     */
    updatePlatItems,
    
    /**
     * Find association id.
     */
    findAssociation,
    
    /**
     * Edit association id.
     */
    editAssociation,
    
    /**
     * Find customer id.
     */
    findCustomer, 
    
    /**
     * Download movements id.
     */
    downloadMovements,
    
    /**
     * Print credits id.
     */
    printCredits, 
    
    /**
     * Print partner daily report
     */
    printDailyReport,
    
    /**
     * Print orders summary report
     */
    printSummary,
    
    /**
     * Load item constant.
     */
    loadItem 
}
