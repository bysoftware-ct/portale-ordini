package it.bysoftware.ct.utils;

import it.bysoftware.ct.model.TestataFattureClienti;

import java.util.Date;

import com.liferay.portal.kernel.util.OrderByComparator;

public class InvoiceComparator extends OrderByComparator {

	/**
	 * Auto generated Serial ID.
	 */
	private static final long serialVersionUID = -2520427154329829112L;

	/**
	 * Ascending or descending order type.
	 */
	private boolean ascending;

	/**
	 * Ascending Customer constant.
	 */
	public static final String ORDER_BY_ASC =
			"TestataFattureClienti.dataDocumento ASC,"
			+ " TestataFattureClienti.numeroDocumento ASC";

	/**
	 * Descending Order constant.
	 */
	public static final String ORDER_BY_DESC =
			"TestataFattureClienti.dataDocumento DESC,"
			+ " TestataFattureClienti.numeroDocumento DESC";;

	/**
	 * Order by fields.
	 */
	public static final String[] ORDER_BY_FIELDS = { "dataDocumento",
		"numeroDocmento" };

	/**
	 * Default constructor.
	 * 
	 */
	public InvoiceComparator() {
		this(true);
	}

	/**
	 * Other constructor.
	 * 
	 * @param order
	 *            true for ascending order false otherwise
	 */
	public InvoiceComparator(boolean ascending) {
		super();
		this.ascending = ascending;
	}

	@Override
	public int compare(Object obj1, Object obj2) {
		int value;
		
		TestataFattureClienti t1 = (TestataFattureClienti) obj1;
		TestataFattureClienti t2 = (TestataFattureClienti) obj2;
        if (ascending) {
            value = compareByDataDocumento(t1, t2);
        } else {
            value = compareByDataDocumento(t2, t1);
        }

        if (value == 0) {
            if (ascending) {
                value = compareByNumeroDocumento(t1, t2);
            } else {
                value = compareByNumeroDocumento(t2, t1);
            }
        }

        return value;
	}

	private final int compareByNumeroDocumento(TestataFattureClienti obj1,
			TestataFattureClienti obj2) {
		return obj1.getDataDocumento().compareTo(obj2.getDataDocumento());
	}

	private final int compareByDataDocumento(TestataFattureClienti obj1,
			TestataFattureClienti obj2) {
		return Integer.compare(obj1.getNumeroDocumento(),
				obj2.getNumeroDocumento());
	}

	@Override
	public String getOrderBy() {
		return this.ascending ? ORDER_BY_ASC : ORDER_BY_DESC;
	}

	@Override
	public String[] getOrderByConditionFields() {
		return ORDER_BY_FIELDS;
	}

	@Override
	public boolean isAscending() {
		return this.ascending;
	}
	
	

}
