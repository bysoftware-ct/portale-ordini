package it.bysoftware.ct.utils;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.OrderByComparator;

import it.bysoftware.ct.model.Ordine;

/**
 * Order comparator.
 *
 * @author Mario Torrisi torrisi.mario@gmail.com
 */
public class OrderComparator extends OrderByComparator {

    /**
     * Auto generated Serial ID.
     */
    private static final long serialVersionUID = -8058917336307629088L;
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(OrderComparator.class);

    /**
     * Ascending or descending order type. 
     */
    private boolean ascending;
    
    /**
     * Ascending Customer constant.
     */
    public static final String ORDER_BY_ASC =
            "Ordini.dataConsegna ASC";

    /**
     * Descending Order constant.
     */
    public static final String ORDER_BY_DESC =
            "Ordini.dataConsegna DESC";

    /**
     * Order by fields.
     */
    public static final String[] ORDER_BY_FIELDS = { "dataConsegna" };

    /**
     * Default constructor.
     */
    public OrderComparator() {
        this(true);
    }

    /**
     * Other constructor.
     * 
     * @param order
     *            true for ascending order false otherwise
     */
    public OrderComparator(final boolean order) {
        this.ascending = order;
    }

    @Override
    public final int compare(final Object obj1, final Object obj2) {
        Ordine o1 = (Ordine) obj1;
        Ordine o2 = (Ordine) obj2;

        long o1Date = o1.getDataConsegna().getTime();
        long o2Date = o2.getDataConsegna().getTime();
        
        int value = 0;
        if (o1Date == o2Date) {
            if (o1.getStato() >= o2.getStato()) {
                value = 1;
            } else {
                value = -1;
            }
        }

        if (this.ascending) {
            return value;
        } else {
            return -value;
        }
    }

    @Override
    public final String getOrderBy() {
        if (this.ascending) {
            return ORDER_BY_ASC;
        } else {
            return ORDER_BY_DESC;
        }
    }

    @Override
    public final String[] getOrderByFields() {
        return ORDER_BY_FIELDS;
    }

    @Override
    public final boolean isAscending() {
        return this.ascending;
    }

}
