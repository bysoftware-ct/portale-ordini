package it.bysoftware.ct;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.portlet.PortletProps;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;

/**
 * Partner Payments configuration portlet implementation.
 *
 * @author Mario Torrisi torrisi.mario@gmail.com
 */
public class PartnersPaymentPortletConfig extends DefaultConfigurationAction {

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil
            .getLog(PartnersPaymentPortletConfig.class);

    @Override
    public final void processAction(final PortletConfig portletConfig,
            final ActionRequest actionRequest,
            final ActionResponse actionResponse) throws Exception {

        String value = ParamUtil.getString(actionRequest,
                "preferences--emailList--");

        OutputStream output = null;
        Properties p = new Properties();

        if (!PortletProps.contains("cc-list")
                || !PortletProps.get("cc-list").equals(value)) {
            try {
                FileInputStream in = new FileInputStream(
                        PartnersPaymentPortlet.getPropertiesFile());
                p.load(in);
                in.close();
                output = new FileOutputStream(
                        PartnersPaymentPortlet.getPropertiesFile());
                p.setProperty("cc-list", value);
                p.store(output, null);
            } catch (IOException e) {
                this.logger.error(e.getMessage());
                if (this.logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
            } finally {
                if (output != null) {
                    try {
                        output.close();
                    } catch (IOException e) {
                        this.logger.error(e.getMessage());
                        if (this.logger.isDebugEnabled()) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        PortletProps.set("cc-list", value);
        super.processAction(portletConfig, actionRequest, actionResponse);
    }

}
