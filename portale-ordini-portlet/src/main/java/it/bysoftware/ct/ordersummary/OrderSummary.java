package it.bysoftware.ct.ordersummary;

import it.bysoftware.ct.utils.Constants;
import it.bysoftware.ct.utils.Report;
import it.bysoftware.ct.utils.ResourceId;
import it.bysoftware.ct.utils.Response;
import it.bysoftware.ct.utils.Response.Code;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Base64;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class OrderSummary
 */
public class OrderSummary extends MVCPortlet {
 
	/**
	 * Logger object. Based on Liferay logger
	 * {@link com.liferay.portal.kernel.log.Log} commons logging.
	 */
	private Log logger = LogFactoryUtil.getLog(OrderSummary.class);
	
	/**
	 * Data source connection.
	 */
	private DataSource ds;

	@Override
	public final void init(final PortletConfig config) throws PortletException {
		Context ctx;
		try {
			ctx = new InitialContext();
			this.ds = (DataSource) ctx.lookup("jdbc/OrdiniPool");
		} catch (NamingException e) {
			this.logger.fatal(e.getMessage());
			if (this.logger.isDebugEnabled()) {
				e.printStackTrace();
			}
		}
		super.init(config);
	}
	
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		String resourceID = resourceRequest.getResourceID();
		PrintWriter out;
		Response response;
		switch (ResourceId.valueOf(resourceID)) {
		case printSummary:
			this.logger.debug("Printing summary report");
			response = this.printSummaryReport(resourceRequest);
			if (response.getCode() == Code.OK.ordinal()) {
//				File f = new File(response.getMessage());
				
				byte[] inFileBytes = Files.readAllBytes(Paths.get(
						response.getMessage())); 
			    String encoded = Base64.encode(inFileBytes);
			    response.setMessage(encoded);
			}
			out = resourceResponse.getWriter();
			out.print(JSONFactoryUtil.looseSerialize(response));
			out.flush();
			out.close();
			break;
		default:
			break;
		}
	}
	
	private Response printSummaryReport(ResourceRequest resourceRequest) {
		int orderId = ParamUtil.getInteger(resourceRequest, "orderId", 0);
		this.logger.debug(orderId);
		long fromDateLng = ParamUtil.getLong(resourceRequest, "fromDate", 0);
		this.logger.debug(fromDateLng);
		long toDateLng = ParamUtil.getLong(resourceRequest, "toDate", 0);
		this.logger.debug(toDateLng);
		boolean showSaved = ParamUtil.getBoolean(resourceRequest, "showSaved",
				false);
		this.logger.debug(showSaved);
		String partnerCode = ParamUtil.getString(resourceRequest,
				"partnerCode", "");
		this.logger.debug(partnerCode);
		String message = null;
		Code c = null;
		if (fromDateLng > 0 && toDateLng > 0) {
			Map<String, Object> parametersMap = new HashMap<String, Object>();
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(fromDateLng);
			parametersMap.put("dateFrom", calendar.getTime());
			calendar.setTimeInMillis(toDateLng);
			parametersMap.put("dateTo", calendar.getTime());
			parametersMap.put("order_id", orderId);
			parametersMap.put("supplier_id", partnerCode);
			parametersMap.put("bozza", showSaved);
	        
	        parametersMap.put(JRParameter.REPORT_LOCALE, new Locale("it", "IT"));
	        
			try {
				Report report = new Report(this.ds.getConnection());
				User user = UserLocalServiceUtil.getUser(Long
						.parseLong(resourceRequest.getRemoteUser()));
				long userId = -1; 
				if (RoleLocalServiceUtil.hasUserRole(user.getUserId(),
                        RoleLocalServiceUtil.getRole(user.getCompanyId(),
                                Constants.COMPANY).getRoleId())){
					userId = user.getUserId();
					parametersMap.put("company", true);
				} else {
					parametersMap.put("company", false);
					List<User> list = UserLocalServiceUtil.getCompanyUsers(
							user.getCompanyId(), QueryUtil.ALL_POS,
							QueryUtil.ALL_POS);
					long roleId = RoleLocalServiceUtil.getRole(user.getCompanyId(),
                            Constants.COMPANY).getRoleId();
					for (User u : list) {
						if (RoleLocalServiceUtil.hasUserRole(u.getUserId(),
								roleId)) {
							userId = u.getUserId();
							break;
						}
					}
				}
				if (userId <= 0) {
					this.logger.error(message);
					c = Response.Code.GENERIC_ERROR;
					message = "User with '" + Constants.COMPANY + "' role not "
							+ "found!";
				} else {
					String filePath = report.print(userId, "", parametersMap,
							"confirm-summary", "confirm-summary-subreport");
					report.closeConnection();

					c = Response.Code.OK;
					message = filePath;
				}
			} catch (SQLException e) {
				this.logger.error(e.getMessage());
				e.printStackTrace();
				c = Response.Code.GENERIC_ERROR;
				message = e.getMessage();
			} catch (NumberFormatException e) {
				this.logger.error(e.getMessage());
				e.printStackTrace();
				c = Response.Code.GENERIC_ERROR;
				message = e.getMessage();
			} catch (PortalException e) {
				this.logger.error(e.getMessage());
				e.printStackTrace();
				c = Response.Code.GENERIC_ERROR;
				message = e.getMessage();
			} catch (SystemException e) {
				this.logger.error(e.getMessage());
				e.printStackTrace();
				c = Response.Code.GENERIC_ERROR;
				message = e.getMessage();
			} catch (JRException e) {
				this.logger.error(e.getMessage());
				e.printStackTrace();
				c = Response.Code.GENERIC_ERROR;
				message = e.getMessage();
			}
		} else {
			c = Response.Code.GENERIC_ERROR;
			message = "Data o socio non validi";
		}

		return new Response(c, message);
	}
}
