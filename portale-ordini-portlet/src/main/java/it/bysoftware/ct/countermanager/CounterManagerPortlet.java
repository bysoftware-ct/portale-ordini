package it.bysoftware.ct.countermanager;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import it.bysoftware.ct.model.ContatoreSocio;
import it.bysoftware.ct.service.ContatoreSocioLocalServiceUtil;
import it.bysoftware.ct.service.persistence.ContatoreSocioPK;
import it.bysoftware.ct.utils.Utils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.liferay.util.portlet.PortletProps;

/**
 * Portlet implementation class CounterManagerPortlet.
 */
public class CounterManagerPortlet extends MVCPortlet {

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(CounterManagerPortlet.class);

    /**
     * Stores the passport number for the current order.
     * 
     * @param areq
     *            the request {@link ActionRequest}
     * @param ares
     *            the response {@link ActionResponse}
     */
    public final void saveDocumentNumber(final ActionRequest areq,
            final ActionResponse ares) {
        this.logger.info("Saving document number...");
        boolean anyCounter = ParamUtil.getBoolean(areq, "anyCounter");
        int newNumber = ParamUtil.getInteger(areq, "newNumber");
        final String partnerCode = ParamUtil.getString(areq, "partnerCode", "");
        try {
            ContatoreSocio counter = null;
            if (anyCounter) {
                counter = ContatoreSocioLocalServiceUtil.getContatoreSocio(
                        new ContatoreSocioPK(Utils.getYear(), partnerCode,
                                PortletProps.get("sell-invoice")));
            } else {
                counter = ContatoreSocioLocalServiceUtil.createContatoreSocio(
                        new ContatoreSocioPK(Utils.getYear(), partnerCode,
                                PortletProps.get("sell-invoice")));
            }
            
            counter.setNumero(newNumber);
            ContatoreSocioLocalServiceUtil.updateContatoreSocio(counter);
        } catch (PortalException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            SessionErrors.add(areq, e.getMessage());
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            if (this.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            SessionErrors.add(areq, e.getMessage());
        }

        PortalUtil.copyRequestParameters(areq, ares);
        ares.setRenderParameter("jspPage",
                "/html/countermanager/edit-counter.jsp");
    }

}
