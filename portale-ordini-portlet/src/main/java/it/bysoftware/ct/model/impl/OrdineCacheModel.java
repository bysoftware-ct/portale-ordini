package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.Ordine;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Ordine in entity cache.
 *
 * @author Mario Torrisi
 * @see Ordine
 * @generated
 */
public class OrdineCacheModel implements CacheModel<Ordine>, Externalizable {
    public long id;
    public long dataInserimento;
    public String idCliente;
    public int stato;
    public double importo;
    public String note;
    public long dataConsegna;
    public String idTrasportatore;
    public String idTrasportatore2;
    public long idAgente;
    public long idTipoCarrello;
    public String luogoCarico;
    public String noteCliente;
    public int anno;
    public int numero;
    public String centro;
    public boolean modificato;
    public String codiceIva;
    public String cliente;
    public String vettore;
    public String pagamento;
    public boolean ddtGenerati;
    public boolean stampaEtichette;
    public String numOrdineGT;
    public long dataOrdineGT;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(51);

        sb.append("{id=");
        sb.append(id);
        sb.append(", dataInserimento=");
        sb.append(dataInserimento);
        sb.append(", idCliente=");
        sb.append(idCliente);
        sb.append(", stato=");
        sb.append(stato);
        sb.append(", importo=");
        sb.append(importo);
        sb.append(", note=");
        sb.append(note);
        sb.append(", dataConsegna=");
        sb.append(dataConsegna);
        sb.append(", idTrasportatore=");
        sb.append(idTrasportatore);
        sb.append(", idTrasportatore2=");
        sb.append(idTrasportatore2);
        sb.append(", idAgente=");
        sb.append(idAgente);
        sb.append(", idTipoCarrello=");
        sb.append(idTipoCarrello);
        sb.append(", luogoCarico=");
        sb.append(luogoCarico);
        sb.append(", noteCliente=");
        sb.append(noteCliente);
        sb.append(", anno=");
        sb.append(anno);
        sb.append(", numero=");
        sb.append(numero);
        sb.append(", centro=");
        sb.append(centro);
        sb.append(", modificato=");
        sb.append(modificato);
        sb.append(", codiceIva=");
        sb.append(codiceIva);
        sb.append(", cliente=");
        sb.append(cliente);
        sb.append(", vettore=");
        sb.append(vettore);
        sb.append(", pagamento=");
        sb.append(pagamento);
        sb.append(", ddtGenerati=");
        sb.append(ddtGenerati);
        sb.append(", stampaEtichette=");
        sb.append(stampaEtichette);
        sb.append(", numOrdineGT=");
        sb.append(numOrdineGT);
        sb.append(", dataOrdineGT=");
        sb.append(dataOrdineGT);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Ordine toEntityModel() {
        OrdineImpl ordineImpl = new OrdineImpl();

        ordineImpl.setId(id);

        if (dataInserimento == Long.MIN_VALUE) {
            ordineImpl.setDataInserimento(null);
        } else {
            ordineImpl.setDataInserimento(new Date(dataInserimento));
        }

        if (idCliente == null) {
            ordineImpl.setIdCliente(StringPool.BLANK);
        } else {
            ordineImpl.setIdCliente(idCliente);
        }

        ordineImpl.setStato(stato);
        ordineImpl.setImporto(importo);

        if (note == null) {
            ordineImpl.setNote(StringPool.BLANK);
        } else {
            ordineImpl.setNote(note);
        }

        if (dataConsegna == Long.MIN_VALUE) {
            ordineImpl.setDataConsegna(null);
        } else {
            ordineImpl.setDataConsegna(new Date(dataConsegna));
        }

        if (idTrasportatore == null) {
            ordineImpl.setIdTrasportatore(StringPool.BLANK);
        } else {
            ordineImpl.setIdTrasportatore(idTrasportatore);
        }

        if (idTrasportatore2 == null) {
            ordineImpl.setIdTrasportatore2(StringPool.BLANK);
        } else {
            ordineImpl.setIdTrasportatore2(idTrasportatore2);
        }

        ordineImpl.setIdAgente(idAgente);
        ordineImpl.setIdTipoCarrello(idTipoCarrello);

        if (luogoCarico == null) {
            ordineImpl.setLuogoCarico(StringPool.BLANK);
        } else {
            ordineImpl.setLuogoCarico(luogoCarico);
        }

        if (noteCliente == null) {
            ordineImpl.setNoteCliente(StringPool.BLANK);
        } else {
            ordineImpl.setNoteCliente(noteCliente);
        }

        ordineImpl.setAnno(anno);
        ordineImpl.setNumero(numero);

        if (centro == null) {
            ordineImpl.setCentro(StringPool.BLANK);
        } else {
            ordineImpl.setCentro(centro);
        }

        ordineImpl.setModificato(modificato);

        if (codiceIva == null) {
            ordineImpl.setCodiceIva(StringPool.BLANK);
        } else {
            ordineImpl.setCodiceIva(codiceIva);
        }

        if (cliente == null) {
            ordineImpl.setCliente(StringPool.BLANK);
        } else {
            ordineImpl.setCliente(cliente);
        }

        if (vettore == null) {
            ordineImpl.setVettore(StringPool.BLANK);
        } else {
            ordineImpl.setVettore(vettore);
        }

        if (pagamento == null) {
            ordineImpl.setPagamento(StringPool.BLANK);
        } else {
            ordineImpl.setPagamento(pagamento);
        }

        ordineImpl.setDdtGenerati(ddtGenerati);
        ordineImpl.setStampaEtichette(stampaEtichette);

        if (numOrdineGT == null) {
            ordineImpl.setNumOrdineGT(StringPool.BLANK);
        } else {
            ordineImpl.setNumOrdineGT(numOrdineGT);
        }

        if (dataOrdineGT == Long.MIN_VALUE) {
            ordineImpl.setDataOrdineGT(null);
        } else {
            ordineImpl.setDataOrdineGT(new Date(dataOrdineGT));
        }

        ordineImpl.resetOriginalValues();

        return ordineImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        dataInserimento = objectInput.readLong();
        idCliente = objectInput.readUTF();
        stato = objectInput.readInt();
        importo = objectInput.readDouble();
        note = objectInput.readUTF();
        dataConsegna = objectInput.readLong();
        idTrasportatore = objectInput.readUTF();
        idTrasportatore2 = objectInput.readUTF();
        idAgente = objectInput.readLong();
        idTipoCarrello = objectInput.readLong();
        luogoCarico = objectInput.readUTF();
        noteCliente = objectInput.readUTF();
        anno = objectInput.readInt();
        numero = objectInput.readInt();
        centro = objectInput.readUTF();
        modificato = objectInput.readBoolean();
        codiceIva = objectInput.readUTF();
        cliente = objectInput.readUTF();
        vettore = objectInput.readUTF();
        pagamento = objectInput.readUTF();
        ddtGenerati = objectInput.readBoolean();
        stampaEtichette = objectInput.readBoolean();
        numOrdineGT = objectInput.readUTF();
        dataOrdineGT = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(dataInserimento);

        if (idCliente == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(idCliente);
        }

        objectOutput.writeInt(stato);
        objectOutput.writeDouble(importo);

        if (note == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(note);
        }

        objectOutput.writeLong(dataConsegna);

        if (idTrasportatore == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(idTrasportatore);
        }

        if (idTrasportatore2 == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(idTrasportatore2);
        }

        objectOutput.writeLong(idAgente);
        objectOutput.writeLong(idTipoCarrello);

        if (luogoCarico == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(luogoCarico);
        }

        if (noteCliente == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(noteCliente);
        }

        objectOutput.writeInt(anno);
        objectOutput.writeInt(numero);

        if (centro == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(centro);
        }

        objectOutput.writeBoolean(modificato);

        if (codiceIva == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceIva);
        }

        if (cliente == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(cliente);
        }

        if (vettore == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(vettore);
        }

        if (pagamento == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(pagamento);
        }

        objectOutput.writeBoolean(ddtGenerati);
        objectOutput.writeBoolean(stampaEtichette);

        if (numOrdineGT == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(numOrdineGT);
        }

        objectOutput.writeLong(dataOrdineGT);
    }
}
