package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.Rubrica;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Rubrica in entity cache.
 *
 * @author Mario Torrisi
 * @see Rubrica
 * @generated
 */
public class RubricaCacheModel implements CacheModel<Rubrica>, Externalizable {
    public String codiceSoggetto;
    public int numeroVoce;
    public String descrizione;
    public String telefono;
    public String cellulare;
    public String fax;
    public String eMailTo;
    public String eMailCc;
    public String eMailCcn;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(19);

        sb.append("{codiceSoggetto=");
        sb.append(codiceSoggetto);
        sb.append(", numeroVoce=");
        sb.append(numeroVoce);
        sb.append(", descrizione=");
        sb.append(descrizione);
        sb.append(", telefono=");
        sb.append(telefono);
        sb.append(", cellulare=");
        sb.append(cellulare);
        sb.append(", fax=");
        sb.append(fax);
        sb.append(", eMailTo=");
        sb.append(eMailTo);
        sb.append(", eMailCc=");
        sb.append(eMailCc);
        sb.append(", eMailCcn=");
        sb.append(eMailCcn);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Rubrica toEntityModel() {
        RubricaImpl rubricaImpl = new RubricaImpl();

        if (codiceSoggetto == null) {
            rubricaImpl.setCodiceSoggetto(StringPool.BLANK);
        } else {
            rubricaImpl.setCodiceSoggetto(codiceSoggetto);
        }

        rubricaImpl.setNumeroVoce(numeroVoce);

        if (descrizione == null) {
            rubricaImpl.setDescrizione(StringPool.BLANK);
        } else {
            rubricaImpl.setDescrizione(descrizione);
        }

        if (telefono == null) {
            rubricaImpl.setTelefono(StringPool.BLANK);
        } else {
            rubricaImpl.setTelefono(telefono);
        }

        if (cellulare == null) {
            rubricaImpl.setCellulare(StringPool.BLANK);
        } else {
            rubricaImpl.setCellulare(cellulare);
        }

        if (fax == null) {
            rubricaImpl.setFax(StringPool.BLANK);
        } else {
            rubricaImpl.setFax(fax);
        }

        if (eMailTo == null) {
            rubricaImpl.setEMailTo(StringPool.BLANK);
        } else {
            rubricaImpl.setEMailTo(eMailTo);
        }

        if (eMailCc == null) {
            rubricaImpl.setEMailCc(StringPool.BLANK);
        } else {
            rubricaImpl.setEMailCc(eMailCc);
        }

        if (eMailCcn == null) {
            rubricaImpl.setEMailCcn(StringPool.BLANK);
        } else {
            rubricaImpl.setEMailCcn(eMailCcn);
        }

        rubricaImpl.resetOriginalValues();

        return rubricaImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        codiceSoggetto = objectInput.readUTF();
        numeroVoce = objectInput.readInt();
        descrizione = objectInput.readUTF();
        telefono = objectInput.readUTF();
        cellulare = objectInput.readUTF();
        fax = objectInput.readUTF();
        eMailTo = objectInput.readUTF();
        eMailCc = objectInput.readUTF();
        eMailCcn = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        if (codiceSoggetto == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceSoggetto);
        }

        objectOutput.writeInt(numeroVoce);

        if (descrizione == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(descrizione);
        }

        if (telefono == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(telefono);
        }

        if (cellulare == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(cellulare);
        }

        if (fax == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(fax);
        }

        if (eMailTo == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(eMailTo);
        }

        if (eMailCc == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(eMailCc);
        }

        if (eMailCcn == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(eMailCcn);
        }
    }
}
