package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.CategorieMerceologiche;
import it.bysoftware.ct.service.CategorieMerceologicheLocalServiceUtil;

/**
 * The extended model base implementation for the CategorieMerceologiche service. Represents a row in the &quot;CategorieMerceologiche&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This class exists only as a container for the default extended model level methods generated by ServiceBuilder. Helper methods and all application logic should be put in {@link CategorieMerceologicheImpl}.
 * </p>
 *
 * @author Mario Torrisi
 * @see CategorieMerceologicheImpl
 * @see it.bysoftware.ct.model.CategorieMerceologiche
 * @generated
 */
public abstract class CategorieMerceologicheBaseImpl
    extends CategorieMerceologicheModelImpl implements CategorieMerceologiche {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. All methods that expect a categorie merceologiche model instance should use the {@link CategorieMerceologiche} interface instead.
     */
    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            CategorieMerceologicheLocalServiceUtil.addCategorieMerceologiche(this);
        } else {
            CategorieMerceologicheLocalServiceUtil.updateCategorieMerceologiche(this);
        }
    }
}
