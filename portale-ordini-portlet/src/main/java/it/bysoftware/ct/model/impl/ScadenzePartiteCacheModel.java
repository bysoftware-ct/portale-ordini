package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.ScadenzePartite;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ScadenzePartite in entity cache.
 *
 * @author Mario Torrisi
 * @see ScadenzePartite
 * @generated
 */
public class ScadenzePartiteCacheModel implements CacheModel<ScadenzePartite>,
    Externalizable {
    public boolean tipoSoggetto;
    public String codiceSoggetto;
    public int esercizioRegistrazione;
    public int numeroPartita;
    public int numeroScadenza;
    public long dataScadenza;
    public int codiceTipoPagam;
    public int stato;
    public String codiceBanca;
    public String codiceContoCorrente;
    public String codiceAgente;
    public String tipoCausale;
    public int esercizioDocumento;
    public int protocolloDocumento;
    public String codiceAttivita;
    public String codiceCentro;
    public long dataRegistrazione;
    public long dataOperazione;
    public long dataAggiornamento;
    public long dataChiusura;
    public String codiceCausale;
    public long dataDocumento;
    public int numeroDocumento;
    public String codiceTipoPagamOrig;
    public String codiceTipoPagamScad;
    public String causaleEstrattoContoOrig;
    public String codiceDivisaOrig;
    public double importoTotaleInt;
    public double importoPagatoInt;
    public double importoApertoInt;
    public double importoEspostoInt;
    public double importoScadutoInt;
    public double importoInsolutoInt;
    public double importoInContenziosoInt;
    public double importoTotale;
    public double importoPagato;
    public double importoAperto;
    public double importoEsposto;
    public double importoScaduto;
    public double importoInsoluto;
    public double importoInContenzioso;
    public int numeroLettereSollecito;
    public String codiceDivisaInterna;
    public int codiceEsercizioScadenzaConversione;
    public int numeroPartitaScadenzaCoversione;
    public int numeroScadenzaCoversione;
    public int tipoCoversione;
    public int codiceEsercizioScritturaConversione;
    public int numeroRegistrazioneScritturaConversione;
    public long dataRegistrazioneScritturaConversione;
    public String codiceDivisaIntScadenzaConversione;
    public String codiceDivisaScadenzaConversione;
    public double cambioScadenzaConversione;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(107);

        sb.append("{tipoSoggetto=");
        sb.append(tipoSoggetto);
        sb.append(", codiceSoggetto=");
        sb.append(codiceSoggetto);
        sb.append(", esercizioRegistrazione=");
        sb.append(esercizioRegistrazione);
        sb.append(", numeroPartita=");
        sb.append(numeroPartita);
        sb.append(", numeroScadenza=");
        sb.append(numeroScadenza);
        sb.append(", dataScadenza=");
        sb.append(dataScadenza);
        sb.append(", codiceTipoPagam=");
        sb.append(codiceTipoPagam);
        sb.append(", stato=");
        sb.append(stato);
        sb.append(", codiceBanca=");
        sb.append(codiceBanca);
        sb.append(", codiceContoCorrente=");
        sb.append(codiceContoCorrente);
        sb.append(", codiceAgente=");
        sb.append(codiceAgente);
        sb.append(", tipoCausale=");
        sb.append(tipoCausale);
        sb.append(", esercizioDocumento=");
        sb.append(esercizioDocumento);
        sb.append(", protocolloDocumento=");
        sb.append(protocolloDocumento);
        sb.append(", codiceAttivita=");
        sb.append(codiceAttivita);
        sb.append(", codiceCentro=");
        sb.append(codiceCentro);
        sb.append(", dataRegistrazione=");
        sb.append(dataRegistrazione);
        sb.append(", dataOperazione=");
        sb.append(dataOperazione);
        sb.append(", dataAggiornamento=");
        sb.append(dataAggiornamento);
        sb.append(", dataChiusura=");
        sb.append(dataChiusura);
        sb.append(", codiceCausale=");
        sb.append(codiceCausale);
        sb.append(", dataDocumento=");
        sb.append(dataDocumento);
        sb.append(", numeroDocumento=");
        sb.append(numeroDocumento);
        sb.append(", codiceTipoPagamOrig=");
        sb.append(codiceTipoPagamOrig);
        sb.append(", codiceTipoPagamScad=");
        sb.append(codiceTipoPagamScad);
        sb.append(", causaleEstrattoContoOrig=");
        sb.append(causaleEstrattoContoOrig);
        sb.append(", codiceDivisaOrig=");
        sb.append(codiceDivisaOrig);
        sb.append(", importoTotaleInt=");
        sb.append(importoTotaleInt);
        sb.append(", importoPagatoInt=");
        sb.append(importoPagatoInt);
        sb.append(", importoApertoInt=");
        sb.append(importoApertoInt);
        sb.append(", importoEspostoInt=");
        sb.append(importoEspostoInt);
        sb.append(", importoScadutoInt=");
        sb.append(importoScadutoInt);
        sb.append(", importoInsolutoInt=");
        sb.append(importoInsolutoInt);
        sb.append(", importoInContenziosoInt=");
        sb.append(importoInContenziosoInt);
        sb.append(", importoTotale=");
        sb.append(importoTotale);
        sb.append(", importoPagato=");
        sb.append(importoPagato);
        sb.append(", importoAperto=");
        sb.append(importoAperto);
        sb.append(", importoEsposto=");
        sb.append(importoEsposto);
        sb.append(", importoScaduto=");
        sb.append(importoScaduto);
        sb.append(", importoInsoluto=");
        sb.append(importoInsoluto);
        sb.append(", importoInContenzioso=");
        sb.append(importoInContenzioso);
        sb.append(", numeroLettereSollecito=");
        sb.append(numeroLettereSollecito);
        sb.append(", codiceDivisaInterna=");
        sb.append(codiceDivisaInterna);
        sb.append(", codiceEsercizioScadenzaConversione=");
        sb.append(codiceEsercizioScadenzaConversione);
        sb.append(", numeroPartitaScadenzaCoversione=");
        sb.append(numeroPartitaScadenzaCoversione);
        sb.append(", numeroScadenzaCoversione=");
        sb.append(numeroScadenzaCoversione);
        sb.append(", tipoCoversione=");
        sb.append(tipoCoversione);
        sb.append(", codiceEsercizioScritturaConversione=");
        sb.append(codiceEsercizioScritturaConversione);
        sb.append(", numeroRegistrazioneScritturaConversione=");
        sb.append(numeroRegistrazioneScritturaConversione);
        sb.append(", dataRegistrazioneScritturaConversione=");
        sb.append(dataRegistrazioneScritturaConversione);
        sb.append(", codiceDivisaIntScadenzaConversione=");
        sb.append(codiceDivisaIntScadenzaConversione);
        sb.append(", codiceDivisaScadenzaConversione=");
        sb.append(codiceDivisaScadenzaConversione);
        sb.append(", cambioScadenzaConversione=");
        sb.append(cambioScadenzaConversione);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public ScadenzePartite toEntityModel() {
        ScadenzePartiteImpl scadenzePartiteImpl = new ScadenzePartiteImpl();

        scadenzePartiteImpl.setTipoSoggetto(tipoSoggetto);

        if (codiceSoggetto == null) {
            scadenzePartiteImpl.setCodiceSoggetto(StringPool.BLANK);
        } else {
            scadenzePartiteImpl.setCodiceSoggetto(codiceSoggetto);
        }

        scadenzePartiteImpl.setEsercizioRegistrazione(esercizioRegistrazione);
        scadenzePartiteImpl.setNumeroPartita(numeroPartita);
        scadenzePartiteImpl.setNumeroScadenza(numeroScadenza);

        if (dataScadenza == Long.MIN_VALUE) {
            scadenzePartiteImpl.setDataScadenza(null);
        } else {
            scadenzePartiteImpl.setDataScadenza(new Date(dataScadenza));
        }

        scadenzePartiteImpl.setCodiceTipoPagam(codiceTipoPagam);
        scadenzePartiteImpl.setStato(stato);

        if (codiceBanca == null) {
            scadenzePartiteImpl.setCodiceBanca(StringPool.BLANK);
        } else {
            scadenzePartiteImpl.setCodiceBanca(codiceBanca);
        }

        if (codiceContoCorrente == null) {
            scadenzePartiteImpl.setCodiceContoCorrente(StringPool.BLANK);
        } else {
            scadenzePartiteImpl.setCodiceContoCorrente(codiceContoCorrente);
        }

        if (codiceAgente == null) {
            scadenzePartiteImpl.setCodiceAgente(StringPool.BLANK);
        } else {
            scadenzePartiteImpl.setCodiceAgente(codiceAgente);
        }

        if (tipoCausale == null) {
            scadenzePartiteImpl.setTipoCausale(StringPool.BLANK);
        } else {
            scadenzePartiteImpl.setTipoCausale(tipoCausale);
        }

        scadenzePartiteImpl.setEsercizioDocumento(esercizioDocumento);
        scadenzePartiteImpl.setProtocolloDocumento(protocolloDocumento);

        if (codiceAttivita == null) {
            scadenzePartiteImpl.setCodiceAttivita(StringPool.BLANK);
        } else {
            scadenzePartiteImpl.setCodiceAttivita(codiceAttivita);
        }

        if (codiceCentro == null) {
            scadenzePartiteImpl.setCodiceCentro(StringPool.BLANK);
        } else {
            scadenzePartiteImpl.setCodiceCentro(codiceCentro);
        }

        if (dataRegistrazione == Long.MIN_VALUE) {
            scadenzePartiteImpl.setDataRegistrazione(null);
        } else {
            scadenzePartiteImpl.setDataRegistrazione(new Date(dataRegistrazione));
        }

        if (dataOperazione == Long.MIN_VALUE) {
            scadenzePartiteImpl.setDataOperazione(null);
        } else {
            scadenzePartiteImpl.setDataOperazione(new Date(dataOperazione));
        }

        if (dataAggiornamento == Long.MIN_VALUE) {
            scadenzePartiteImpl.setDataAggiornamento(null);
        } else {
            scadenzePartiteImpl.setDataAggiornamento(new Date(dataAggiornamento));
        }

        if (dataChiusura == Long.MIN_VALUE) {
            scadenzePartiteImpl.setDataChiusura(null);
        } else {
            scadenzePartiteImpl.setDataChiusura(new Date(dataChiusura));
        }

        if (codiceCausale == null) {
            scadenzePartiteImpl.setCodiceCausale(StringPool.BLANK);
        } else {
            scadenzePartiteImpl.setCodiceCausale(codiceCausale);
        }

        if (dataDocumento == Long.MIN_VALUE) {
            scadenzePartiteImpl.setDataDocumento(null);
        } else {
            scadenzePartiteImpl.setDataDocumento(new Date(dataDocumento));
        }

        scadenzePartiteImpl.setNumeroDocumento(numeroDocumento);

        if (codiceTipoPagamOrig == null) {
            scadenzePartiteImpl.setCodiceTipoPagamOrig(StringPool.BLANK);
        } else {
            scadenzePartiteImpl.setCodiceTipoPagamOrig(codiceTipoPagamOrig);
        }

        if (codiceTipoPagamScad == null) {
            scadenzePartiteImpl.setCodiceTipoPagamScad(StringPool.BLANK);
        } else {
            scadenzePartiteImpl.setCodiceTipoPagamScad(codiceTipoPagamScad);
        }

        if (causaleEstrattoContoOrig == null) {
            scadenzePartiteImpl.setCausaleEstrattoContoOrig(StringPool.BLANK);
        } else {
            scadenzePartiteImpl.setCausaleEstrattoContoOrig(causaleEstrattoContoOrig);
        }

        if (codiceDivisaOrig == null) {
            scadenzePartiteImpl.setCodiceDivisaOrig(StringPool.BLANK);
        } else {
            scadenzePartiteImpl.setCodiceDivisaOrig(codiceDivisaOrig);
        }

        scadenzePartiteImpl.setImportoTotaleInt(importoTotaleInt);
        scadenzePartiteImpl.setImportoPagatoInt(importoPagatoInt);
        scadenzePartiteImpl.setImportoApertoInt(importoApertoInt);
        scadenzePartiteImpl.setImportoEspostoInt(importoEspostoInt);
        scadenzePartiteImpl.setImportoScadutoInt(importoScadutoInt);
        scadenzePartiteImpl.setImportoInsolutoInt(importoInsolutoInt);
        scadenzePartiteImpl.setImportoInContenziosoInt(importoInContenziosoInt);
        scadenzePartiteImpl.setImportoTotale(importoTotale);
        scadenzePartiteImpl.setImportoPagato(importoPagato);
        scadenzePartiteImpl.setImportoAperto(importoAperto);
        scadenzePartiteImpl.setImportoEsposto(importoEsposto);
        scadenzePartiteImpl.setImportoScaduto(importoScaduto);
        scadenzePartiteImpl.setImportoInsoluto(importoInsoluto);
        scadenzePartiteImpl.setImportoInContenzioso(importoInContenzioso);
        scadenzePartiteImpl.setNumeroLettereSollecito(numeroLettereSollecito);

        if (codiceDivisaInterna == null) {
            scadenzePartiteImpl.setCodiceDivisaInterna(StringPool.BLANK);
        } else {
            scadenzePartiteImpl.setCodiceDivisaInterna(codiceDivisaInterna);
        }

        scadenzePartiteImpl.setCodiceEsercizioScadenzaConversione(codiceEsercizioScadenzaConversione);
        scadenzePartiteImpl.setNumeroPartitaScadenzaCoversione(numeroPartitaScadenzaCoversione);
        scadenzePartiteImpl.setNumeroScadenzaCoversione(numeroScadenzaCoversione);
        scadenzePartiteImpl.setTipoCoversione(tipoCoversione);
        scadenzePartiteImpl.setCodiceEsercizioScritturaConversione(codiceEsercizioScritturaConversione);
        scadenzePartiteImpl.setNumeroRegistrazioneScritturaConversione(numeroRegistrazioneScritturaConversione);

        if (dataRegistrazioneScritturaConversione == Long.MIN_VALUE) {
            scadenzePartiteImpl.setDataRegistrazioneScritturaConversione(null);
        } else {
            scadenzePartiteImpl.setDataRegistrazioneScritturaConversione(new Date(
                    dataRegistrazioneScritturaConversione));
        }

        if (codiceDivisaIntScadenzaConversione == null) {
            scadenzePartiteImpl.setCodiceDivisaIntScadenzaConversione(StringPool.BLANK);
        } else {
            scadenzePartiteImpl.setCodiceDivisaIntScadenzaConversione(codiceDivisaIntScadenzaConversione);
        }

        if (codiceDivisaScadenzaConversione == null) {
            scadenzePartiteImpl.setCodiceDivisaScadenzaConversione(StringPool.BLANK);
        } else {
            scadenzePartiteImpl.setCodiceDivisaScadenzaConversione(codiceDivisaScadenzaConversione);
        }

        scadenzePartiteImpl.setCambioScadenzaConversione(cambioScadenzaConversione);

        scadenzePartiteImpl.resetOriginalValues();

        return scadenzePartiteImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        tipoSoggetto = objectInput.readBoolean();
        codiceSoggetto = objectInput.readUTF();
        esercizioRegistrazione = objectInput.readInt();
        numeroPartita = objectInput.readInt();
        numeroScadenza = objectInput.readInt();
        dataScadenza = objectInput.readLong();
        codiceTipoPagam = objectInput.readInt();
        stato = objectInput.readInt();
        codiceBanca = objectInput.readUTF();
        codiceContoCorrente = objectInput.readUTF();
        codiceAgente = objectInput.readUTF();
        tipoCausale = objectInput.readUTF();
        esercizioDocumento = objectInput.readInt();
        protocolloDocumento = objectInput.readInt();
        codiceAttivita = objectInput.readUTF();
        codiceCentro = objectInput.readUTF();
        dataRegistrazione = objectInput.readLong();
        dataOperazione = objectInput.readLong();
        dataAggiornamento = objectInput.readLong();
        dataChiusura = objectInput.readLong();
        codiceCausale = objectInput.readUTF();
        dataDocumento = objectInput.readLong();
        numeroDocumento = objectInput.readInt();
        codiceTipoPagamOrig = objectInput.readUTF();
        codiceTipoPagamScad = objectInput.readUTF();
        causaleEstrattoContoOrig = objectInput.readUTF();
        codiceDivisaOrig = objectInput.readUTF();
        importoTotaleInt = objectInput.readDouble();
        importoPagatoInt = objectInput.readDouble();
        importoApertoInt = objectInput.readDouble();
        importoEspostoInt = objectInput.readDouble();
        importoScadutoInt = objectInput.readDouble();
        importoInsolutoInt = objectInput.readDouble();
        importoInContenziosoInt = objectInput.readDouble();
        importoTotale = objectInput.readDouble();
        importoPagato = objectInput.readDouble();
        importoAperto = objectInput.readDouble();
        importoEsposto = objectInput.readDouble();
        importoScaduto = objectInput.readDouble();
        importoInsoluto = objectInput.readDouble();
        importoInContenzioso = objectInput.readDouble();
        numeroLettereSollecito = objectInput.readInt();
        codiceDivisaInterna = objectInput.readUTF();
        codiceEsercizioScadenzaConversione = objectInput.readInt();
        numeroPartitaScadenzaCoversione = objectInput.readInt();
        numeroScadenzaCoversione = objectInput.readInt();
        tipoCoversione = objectInput.readInt();
        codiceEsercizioScritturaConversione = objectInput.readInt();
        numeroRegistrazioneScritturaConversione = objectInput.readInt();
        dataRegistrazioneScritturaConversione = objectInput.readLong();
        codiceDivisaIntScadenzaConversione = objectInput.readUTF();
        codiceDivisaScadenzaConversione = objectInput.readUTF();
        cambioScadenzaConversione = objectInput.readDouble();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeBoolean(tipoSoggetto);

        if (codiceSoggetto == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceSoggetto);
        }

        objectOutput.writeInt(esercizioRegistrazione);
        objectOutput.writeInt(numeroPartita);
        objectOutput.writeInt(numeroScadenza);
        objectOutput.writeLong(dataScadenza);
        objectOutput.writeInt(codiceTipoPagam);
        objectOutput.writeInt(stato);

        if (codiceBanca == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceBanca);
        }

        if (codiceContoCorrente == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceContoCorrente);
        }

        if (codiceAgente == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceAgente);
        }

        if (tipoCausale == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(tipoCausale);
        }

        objectOutput.writeInt(esercizioDocumento);
        objectOutput.writeInt(protocolloDocumento);

        if (codiceAttivita == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceAttivita);
        }

        if (codiceCentro == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceCentro);
        }

        objectOutput.writeLong(dataRegistrazione);
        objectOutput.writeLong(dataOperazione);
        objectOutput.writeLong(dataAggiornamento);
        objectOutput.writeLong(dataChiusura);

        if (codiceCausale == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceCausale);
        }

        objectOutput.writeLong(dataDocumento);
        objectOutput.writeInt(numeroDocumento);

        if (codiceTipoPagamOrig == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceTipoPagamOrig);
        }

        if (codiceTipoPagamScad == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceTipoPagamScad);
        }

        if (causaleEstrattoContoOrig == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(causaleEstrattoContoOrig);
        }

        if (codiceDivisaOrig == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceDivisaOrig);
        }

        objectOutput.writeDouble(importoTotaleInt);
        objectOutput.writeDouble(importoPagatoInt);
        objectOutput.writeDouble(importoApertoInt);
        objectOutput.writeDouble(importoEspostoInt);
        objectOutput.writeDouble(importoScadutoInt);
        objectOutput.writeDouble(importoInsolutoInt);
        objectOutput.writeDouble(importoInContenziosoInt);
        objectOutput.writeDouble(importoTotale);
        objectOutput.writeDouble(importoPagato);
        objectOutput.writeDouble(importoAperto);
        objectOutput.writeDouble(importoEsposto);
        objectOutput.writeDouble(importoScaduto);
        objectOutput.writeDouble(importoInsoluto);
        objectOutput.writeDouble(importoInContenzioso);
        objectOutput.writeInt(numeroLettereSollecito);

        if (codiceDivisaInterna == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceDivisaInterna);
        }

        objectOutput.writeInt(codiceEsercizioScadenzaConversione);
        objectOutput.writeInt(numeroPartitaScadenzaCoversione);
        objectOutput.writeInt(numeroScadenzaCoversione);
        objectOutput.writeInt(tipoCoversione);
        objectOutput.writeInt(codiceEsercizioScritturaConversione);
        objectOutput.writeInt(numeroRegistrazioneScritturaConversione);
        objectOutput.writeLong(dataRegistrazioneScritturaConversione);

        if (codiceDivisaIntScadenzaConversione == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceDivisaIntScadenzaConversione);
        }

        if (codiceDivisaScadenzaConversione == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceDivisaScadenzaConversione);
        }

        objectOutput.writeDouble(cambioScadenzaConversione);
    }
}
