package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.DescrizioniVarianti;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing DescrizioniVarianti in entity cache.
 *
 * @author Mario Torrisi
 * @see DescrizioniVarianti
 * @generated
 */
public class DescrizioniVariantiCacheModel implements CacheModel<DescrizioniVarianti>,
    Externalizable {
    public String codiceArticolo;
    public String codiceVariante;
    public String descrizione;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{codiceArticolo=");
        sb.append(codiceArticolo);
        sb.append(", codiceVariante=");
        sb.append(codiceVariante);
        sb.append(", descrizione=");
        sb.append(descrizione);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public DescrizioniVarianti toEntityModel() {
        DescrizioniVariantiImpl descrizioniVariantiImpl = new DescrizioniVariantiImpl();

        if (codiceArticolo == null) {
            descrizioniVariantiImpl.setCodiceArticolo(StringPool.BLANK);
        } else {
            descrizioniVariantiImpl.setCodiceArticolo(codiceArticolo);
        }

        if (codiceVariante == null) {
            descrizioniVariantiImpl.setCodiceVariante(StringPool.BLANK);
        } else {
            descrizioniVariantiImpl.setCodiceVariante(codiceVariante);
        }

        if (descrizione == null) {
            descrizioniVariantiImpl.setDescrizione(StringPool.BLANK);
        } else {
            descrizioniVariantiImpl.setDescrizione(descrizione);
        }

        descrizioniVariantiImpl.resetOriginalValues();

        return descrizioniVariantiImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        codiceArticolo = objectInput.readUTF();
        codiceVariante = objectInput.readUTF();
        descrizione = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        if (codiceArticolo == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceArticolo);
        }

        if (codiceVariante == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceVariante);
        }

        if (descrizione == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(descrizione);
        }
    }
}
