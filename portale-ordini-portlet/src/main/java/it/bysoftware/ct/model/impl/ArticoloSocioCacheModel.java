package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.ArticoloSocio;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing ArticoloSocio in entity cache.
 *
 * @author Mario Torrisi
 * @see ArticoloSocio
 * @generated
 */
public class ArticoloSocioCacheModel implements CacheModel<ArticoloSocio>,
    Externalizable {
    public String associato;
    public String codiceArticoloSocio;
    public String codiceVarianteSocio;
    public String codiceArticolo;
    public String codiceVariante;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{associato=");
        sb.append(associato);
        sb.append(", codiceArticoloSocio=");
        sb.append(codiceArticoloSocio);
        sb.append(", codiceVarianteSocio=");
        sb.append(codiceVarianteSocio);
        sb.append(", codiceArticolo=");
        sb.append(codiceArticolo);
        sb.append(", codiceVariante=");
        sb.append(codiceVariante);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public ArticoloSocio toEntityModel() {
        ArticoloSocioImpl articoloSocioImpl = new ArticoloSocioImpl();

        if (associato == null) {
            articoloSocioImpl.setAssociato(StringPool.BLANK);
        } else {
            articoloSocioImpl.setAssociato(associato);
        }

        if (codiceArticoloSocio == null) {
            articoloSocioImpl.setCodiceArticoloSocio(StringPool.BLANK);
        } else {
            articoloSocioImpl.setCodiceArticoloSocio(codiceArticoloSocio);
        }

        if (codiceVarianteSocio == null) {
            articoloSocioImpl.setCodiceVarianteSocio(StringPool.BLANK);
        } else {
            articoloSocioImpl.setCodiceVarianteSocio(codiceVarianteSocio);
        }

        if (codiceArticolo == null) {
            articoloSocioImpl.setCodiceArticolo(StringPool.BLANK);
        } else {
            articoloSocioImpl.setCodiceArticolo(codiceArticolo);
        }

        if (codiceVariante == null) {
            articoloSocioImpl.setCodiceVariante(StringPool.BLANK);
        } else {
            articoloSocioImpl.setCodiceVariante(codiceVariante);
        }

        articoloSocioImpl.resetOriginalValues();

        return articoloSocioImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        associato = objectInput.readUTF();
        codiceArticoloSocio = objectInput.readUTF();
        codiceVarianteSocio = objectInput.readUTF();
        codiceArticolo = objectInput.readUTF();
        codiceVariante = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        if (associato == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(associato);
        }

        if (codiceArticoloSocio == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceArticoloSocio);
        }

        if (codiceVarianteSocio == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceVarianteSocio);
        }

        if (codiceArticolo == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceArticolo);
        }

        if (codiceVariante == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceVariante);
        }
    }
}
