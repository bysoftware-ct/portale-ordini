package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.CodiceConsorzio;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing CodiceConsorzio in entity cache.
 *
 * @author Mario Torrisi
 * @see CodiceConsorzio
 * @generated
 */
public class CodiceConsorzioCacheModel implements CacheModel<CodiceConsorzio>,
    Externalizable {
    public String codiceSocio;
    public long idLiferay;
    public String codiceConsorzio;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{codiceSocio=");
        sb.append(codiceSocio);
        sb.append(", idLiferay=");
        sb.append(idLiferay);
        sb.append(", codiceConsorzio=");
        sb.append(codiceConsorzio);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public CodiceConsorzio toEntityModel() {
        CodiceConsorzioImpl codiceConsorzioImpl = new CodiceConsorzioImpl();

        if (codiceSocio == null) {
            codiceConsorzioImpl.setCodiceSocio(StringPool.BLANK);
        } else {
            codiceConsorzioImpl.setCodiceSocio(codiceSocio);
        }

        codiceConsorzioImpl.setIdLiferay(idLiferay);

        if (codiceConsorzio == null) {
            codiceConsorzioImpl.setCodiceConsorzio(StringPool.BLANK);
        } else {
            codiceConsorzioImpl.setCodiceConsorzio(codiceConsorzio);
        }

        codiceConsorzioImpl.resetOriginalValues();

        return codiceConsorzioImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        codiceSocio = objectInput.readUTF();
        idLiferay = objectInput.readLong();
        codiceConsorzio = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        if (codiceSocio == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceSocio);
        }

        objectOutput.writeLong(idLiferay);

        if (codiceConsorzio == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceConsorzio);
        }
    }
}
