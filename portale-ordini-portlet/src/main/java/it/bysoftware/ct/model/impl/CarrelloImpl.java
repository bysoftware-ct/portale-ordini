package it.bysoftware.ct.model.impl;

/**
 * The extended model implementation for the Carrello service. Represents a row in the &quot;_carrelli&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.bysoftware.ct.model.Carrello} interface.
 * </p>
 *
 * @author Mario Torrisi
 */
public class CarrelloImpl extends CarrelloBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a carrello model instance should use the {@link it.bysoftware.ct.model.Carrello} interface instead.
     */
    public CarrelloImpl() {
    }
}
