package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.Varianti;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Varianti in entity cache.
 *
 * @author Mario Torrisi
 * @see Varianti
 * @generated
 */
public class VariantiCacheModel implements CacheModel<Varianti>, Externalizable {
    public String codiceVariante;
    public String tipoVariante;
    public String descrizione;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{codiceVariante=");
        sb.append(codiceVariante);
        sb.append(", tipoVariante=");
        sb.append(tipoVariante);
        sb.append(", descrizione=");
        sb.append(descrizione);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Varianti toEntityModel() {
        VariantiImpl variantiImpl = new VariantiImpl();

        if (codiceVariante == null) {
            variantiImpl.setCodiceVariante(StringPool.BLANK);
        } else {
            variantiImpl.setCodiceVariante(codiceVariante);
        }

        if (tipoVariante == null) {
            variantiImpl.setTipoVariante(StringPool.BLANK);
        } else {
            variantiImpl.setTipoVariante(tipoVariante);
        }

        if (descrizione == null) {
            variantiImpl.setDescrizione(StringPool.BLANK);
        } else {
            variantiImpl.setDescrizione(descrizione);
        }

        variantiImpl.resetOriginalValues();

        return variantiImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        codiceVariante = objectInput.readUTF();
        tipoVariante = objectInput.readUTF();
        descrizione = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        if (codiceVariante == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceVariante);
        }

        if (tipoVariante == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(tipoVariante);
        }

        if (descrizione == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(descrizione);
        }
    }
}
