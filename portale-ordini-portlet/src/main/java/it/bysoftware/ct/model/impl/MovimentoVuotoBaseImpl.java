package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.MovimentoVuoto;
import it.bysoftware.ct.service.MovimentoVuotoLocalServiceUtil;

/**
 * The extended model base implementation for the MovimentoVuoto service. Represents a row in the &quot;_movimenti_vuoti&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This class exists only as a container for the default extended model level methods generated by ServiceBuilder. Helper methods and all application logic should be put in {@link MovimentoVuotoImpl}.
 * </p>
 *
 * @author Mario Torrisi
 * @see MovimentoVuotoImpl
 * @see it.bysoftware.ct.model.MovimentoVuoto
 * @generated
 */
public abstract class MovimentoVuotoBaseImpl extends MovimentoVuotoModelImpl
    implements MovimentoVuoto {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. All methods that expect a movimento vuoto model instance should use the {@link MovimentoVuoto} interface instead.
     */
    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            MovimentoVuotoLocalServiceUtil.addMovimentoVuoto(this);
        } else {
            MovimentoVuotoLocalServiceUtil.updateMovimentoVuoto(this);
        }
    }
}
