package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.Centro;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Centro in entity cache.
 *
 * @author Mario Torrisi
 * @see Centro
 * @generated
 */
public class CentroCacheModel implements CacheModel<Centro>, Externalizable {
    public String attivita;
    public String centro;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(5);

        sb.append("{attivita=");
        sb.append(attivita);
        sb.append(", centro=");
        sb.append(centro);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Centro toEntityModel() {
        CentroImpl centroImpl = new CentroImpl();

        if (attivita == null) {
            centroImpl.setAttivita(StringPool.BLANK);
        } else {
            centroImpl.setAttivita(attivita);
        }

        if (centro == null) {
            centroImpl.setCentro(StringPool.BLANK);
        } else {
            centroImpl.setCentro(centro);
        }

        centroImpl.resetOriginalValues();

        return centroImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        attivita = objectInput.readUTF();
        centro = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        if (attivita == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(attivita);
        }

        if (centro == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(centro);
        }
    }
}
