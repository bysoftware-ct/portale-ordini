package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.ClientiFornitoriDatiAgg;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing ClientiFornitoriDatiAgg in entity cache.
 *
 * @author Mario Torrisi
 * @see ClientiFornitoriDatiAgg
 * @generated
 */
public class ClientiFornitoriDatiAggCacheModel implements CacheModel<ClientiFornitoriDatiAgg>,
    Externalizable {
    public String codiceAnagrafica;
    public boolean fornitore;
    public String codiceAgente;
    public String codiceEsenzione;
    public String categoriaEconomica;
    public String codiceRegionale;
    public String codicePuntoVenditaGT;
    public String tipoPagamento;
    public boolean associato;
    public double sconto;
    public String codiceIBAN;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(23);

        sb.append("{codiceAnagrafica=");
        sb.append(codiceAnagrafica);
        sb.append(", fornitore=");
        sb.append(fornitore);
        sb.append(", codiceAgente=");
        sb.append(codiceAgente);
        sb.append(", codiceEsenzione=");
        sb.append(codiceEsenzione);
        sb.append(", categoriaEconomica=");
        sb.append(categoriaEconomica);
        sb.append(", codiceRegionale=");
        sb.append(codiceRegionale);
        sb.append(", codicePuntoVenditaGT=");
        sb.append(codicePuntoVenditaGT);
        sb.append(", tipoPagamento=");
        sb.append(tipoPagamento);
        sb.append(", associato=");
        sb.append(associato);
        sb.append(", sconto=");
        sb.append(sconto);
        sb.append(", codiceIBAN=");
        sb.append(codiceIBAN);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public ClientiFornitoriDatiAgg toEntityModel() {
        ClientiFornitoriDatiAggImpl clientiFornitoriDatiAggImpl = new ClientiFornitoriDatiAggImpl();

        if (codiceAnagrafica == null) {
            clientiFornitoriDatiAggImpl.setCodiceAnagrafica(StringPool.BLANK);
        } else {
            clientiFornitoriDatiAggImpl.setCodiceAnagrafica(codiceAnagrafica);
        }

        clientiFornitoriDatiAggImpl.setFornitore(fornitore);

        if (codiceAgente == null) {
            clientiFornitoriDatiAggImpl.setCodiceAgente(StringPool.BLANK);
        } else {
            clientiFornitoriDatiAggImpl.setCodiceAgente(codiceAgente);
        }

        if (codiceEsenzione == null) {
            clientiFornitoriDatiAggImpl.setCodiceEsenzione(StringPool.BLANK);
        } else {
            clientiFornitoriDatiAggImpl.setCodiceEsenzione(codiceEsenzione);
        }

        if (categoriaEconomica == null) {
            clientiFornitoriDatiAggImpl.setCategoriaEconomica(StringPool.BLANK);
        } else {
            clientiFornitoriDatiAggImpl.setCategoriaEconomica(categoriaEconomica);
        }

        if (codiceRegionale == null) {
            clientiFornitoriDatiAggImpl.setCodiceRegionale(StringPool.BLANK);
        } else {
            clientiFornitoriDatiAggImpl.setCodiceRegionale(codiceRegionale);
        }

        if (codicePuntoVenditaGT == null) {
            clientiFornitoriDatiAggImpl.setCodicePuntoVenditaGT(StringPool.BLANK);
        } else {
            clientiFornitoriDatiAggImpl.setCodicePuntoVenditaGT(codicePuntoVenditaGT);
        }

        if (tipoPagamento == null) {
            clientiFornitoriDatiAggImpl.setTipoPagamento(StringPool.BLANK);
        } else {
            clientiFornitoriDatiAggImpl.setTipoPagamento(tipoPagamento);
        }

        clientiFornitoriDatiAggImpl.setAssociato(associato);
        clientiFornitoriDatiAggImpl.setSconto(sconto);

        if (codiceIBAN == null) {
            clientiFornitoriDatiAggImpl.setCodiceIBAN(StringPool.BLANK);
        } else {
            clientiFornitoriDatiAggImpl.setCodiceIBAN(codiceIBAN);
        }

        clientiFornitoriDatiAggImpl.resetOriginalValues();

        return clientiFornitoriDatiAggImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        codiceAnagrafica = objectInput.readUTF();
        fornitore = objectInput.readBoolean();
        codiceAgente = objectInput.readUTF();
        codiceEsenzione = objectInput.readUTF();
        categoriaEconomica = objectInput.readUTF();
        codiceRegionale = objectInput.readUTF();
        codicePuntoVenditaGT = objectInput.readUTF();
        tipoPagamento = objectInput.readUTF();
        associato = objectInput.readBoolean();
        sconto = objectInput.readDouble();
        codiceIBAN = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        if (codiceAnagrafica == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceAnagrafica);
        }

        objectOutput.writeBoolean(fornitore);

        if (codiceAgente == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceAgente);
        }

        if (codiceEsenzione == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceEsenzione);
        }

        if (categoriaEconomica == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(categoriaEconomica);
        }

        if (codiceRegionale == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceRegionale);
        }

        if (codicePuntoVenditaGT == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codicePuntoVenditaGT);
        }

        if (tipoPagamento == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(tipoPagamento);
        }

        objectOutput.writeBoolean(associato);
        objectOutput.writeDouble(sconto);

        if (codiceIBAN == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceIBAN);
        }
    }
}
