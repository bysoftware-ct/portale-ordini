package it.bysoftware.ct.model.impl;

import it.bysoftware.ct.model.MovimentoVuoto;
import it.bysoftware.ct.service.MovimentoVuotoLocalServiceUtil;

import java.util.List;

/**
 * The extended model implementation for the Vuoto service. Represents a row in
 * the &quot;_vuoti&quot; database table, with each column mapped to a property
 * of this class.
 *
 * <p>Helper methods and all application logic should be put in this class.
 * Whenever methods are added, rerun ServiceBuilder to copy their definitions
 * into the {@link it.bysoftware.ct.model.Vuoto} interface.
 * </p>
 *
 * @author Mario Torrisi
 */
public class VuotoImpl extends VuotoBaseImpl {
    
    /**
     * Serial ID.
     */
    private static final long serialVersionUID = -634357527963726825L;

    public VuotoImpl() {
    }

    public List<MovimentoVuoto> getVuoti() {
        return MovimentoVuotoLocalServiceUtil.findMovementiByCodiceVuoto(
                super.getCodiceVuoto());
    }
}
