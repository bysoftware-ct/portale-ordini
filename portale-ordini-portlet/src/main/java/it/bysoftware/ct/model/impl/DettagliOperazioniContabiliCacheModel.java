package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.DettagliOperazioniContabili;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing DettagliOperazioniContabili in entity cache.
 *
 * @author Mario Torrisi
 * @see DettagliOperazioniContabili
 * @generated
 */
public class DettagliOperazioniContabiliCacheModel implements CacheModel<DettagliOperazioniContabili>,
    Externalizable {
    public String codiceOperazione;
    public int progressivoLegame;
    public int tipoLegame;
    public String causaleContabile;
    public String sottoconto;
    public String naturaConto;
    public int tipoImporto;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(15);

        sb.append("{codiceOperazione=");
        sb.append(codiceOperazione);
        sb.append(", progressivoLegame=");
        sb.append(progressivoLegame);
        sb.append(", tipoLegame=");
        sb.append(tipoLegame);
        sb.append(", causaleContabile=");
        sb.append(causaleContabile);
        sb.append(", sottoconto=");
        sb.append(sottoconto);
        sb.append(", naturaConto=");
        sb.append(naturaConto);
        sb.append(", tipoImporto=");
        sb.append(tipoImporto);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public DettagliOperazioniContabili toEntityModel() {
        DettagliOperazioniContabiliImpl dettagliOperazioniContabiliImpl = new DettagliOperazioniContabiliImpl();

        if (codiceOperazione == null) {
            dettagliOperazioniContabiliImpl.setCodiceOperazione(StringPool.BLANK);
        } else {
            dettagliOperazioniContabiliImpl.setCodiceOperazione(codiceOperazione);
        }

        dettagliOperazioniContabiliImpl.setProgressivoLegame(progressivoLegame);
        dettagliOperazioniContabiliImpl.setTipoLegame(tipoLegame);

        if (causaleContabile == null) {
            dettagliOperazioniContabiliImpl.setCausaleContabile(StringPool.BLANK);
        } else {
            dettagliOperazioniContabiliImpl.setCausaleContabile(causaleContabile);
        }

        if (sottoconto == null) {
            dettagliOperazioniContabiliImpl.setSottoconto(StringPool.BLANK);
        } else {
            dettagliOperazioniContabiliImpl.setSottoconto(sottoconto);
        }

        if (naturaConto == null) {
            dettagliOperazioniContabiliImpl.setNaturaConto(StringPool.BLANK);
        } else {
            dettagliOperazioniContabiliImpl.setNaturaConto(naturaConto);
        }

        dettagliOperazioniContabiliImpl.setTipoImporto(tipoImporto);

        dettagliOperazioniContabiliImpl.resetOriginalValues();

        return dettagliOperazioniContabiliImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        codiceOperazione = objectInput.readUTF();
        progressivoLegame = objectInput.readInt();
        tipoLegame = objectInput.readInt();
        causaleContabile = objectInput.readUTF();
        sottoconto = objectInput.readUTF();
        naturaConto = objectInput.readUTF();
        tipoImporto = objectInput.readInt();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        if (codiceOperazione == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceOperazione);
        }

        objectOutput.writeInt(progressivoLegame);
        objectOutput.writeInt(tipoLegame);

        if (causaleContabile == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(causaleContabile);
        }

        if (sottoconto == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(sottoconto);
        }

        if (naturaConto == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(naturaConto);
        }

        objectOutput.writeInt(tipoImporto);
    }
}
