package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.Pagamento;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Pagamento in entity cache.
 *
 * @author Mario Torrisi
 * @see Pagamento
 * @generated
 */
public class PagamentoCacheModel implements CacheModel<Pagamento>,
    Externalizable {
    public int id;
    public int anno;
    public String codiceSoggetto;
    public long dataCreazione;
    public long dataPagamento;
    public double credito;
    public double importo;
    public double saldo;
    public int stato;
    public String nota;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(21);

        sb.append("{id=");
        sb.append(id);
        sb.append(", anno=");
        sb.append(anno);
        sb.append(", codiceSoggetto=");
        sb.append(codiceSoggetto);
        sb.append(", dataCreazione=");
        sb.append(dataCreazione);
        sb.append(", dataPagamento=");
        sb.append(dataPagamento);
        sb.append(", credito=");
        sb.append(credito);
        sb.append(", importo=");
        sb.append(importo);
        sb.append(", saldo=");
        sb.append(saldo);
        sb.append(", stato=");
        sb.append(stato);
        sb.append(", nota=");
        sb.append(nota);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Pagamento toEntityModel() {
        PagamentoImpl pagamentoImpl = new PagamentoImpl();

        pagamentoImpl.setId(id);
        pagamentoImpl.setAnno(anno);

        if (codiceSoggetto == null) {
            pagamentoImpl.setCodiceSoggetto(StringPool.BLANK);
        } else {
            pagamentoImpl.setCodiceSoggetto(codiceSoggetto);
        }

        if (dataCreazione == Long.MIN_VALUE) {
            pagamentoImpl.setDataCreazione(null);
        } else {
            pagamentoImpl.setDataCreazione(new Date(dataCreazione));
        }

        if (dataPagamento == Long.MIN_VALUE) {
            pagamentoImpl.setDataPagamento(null);
        } else {
            pagamentoImpl.setDataPagamento(new Date(dataPagamento));
        }

        pagamentoImpl.setCredito(credito);
        pagamentoImpl.setImporto(importo);
        pagamentoImpl.setSaldo(saldo);
        pagamentoImpl.setStato(stato);

        if (nota == null) {
            pagamentoImpl.setNota(StringPool.BLANK);
        } else {
            pagamentoImpl.setNota(nota);
        }

        pagamentoImpl.resetOriginalValues();

        return pagamentoImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readInt();
        anno = objectInput.readInt();
        codiceSoggetto = objectInput.readUTF();
        dataCreazione = objectInput.readLong();
        dataPagamento = objectInput.readLong();
        credito = objectInput.readDouble();
        importo = objectInput.readDouble();
        saldo = objectInput.readDouble();
        stato = objectInput.readInt();
        nota = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeInt(id);
        objectOutput.writeInt(anno);

        if (codiceSoggetto == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceSoggetto);
        }

        objectOutput.writeLong(dataCreazione);
        objectOutput.writeLong(dataPagamento);
        objectOutput.writeDouble(credito);
        objectOutput.writeDouble(importo);
        objectOutput.writeDouble(saldo);
        objectOutput.writeInt(stato);

        if (nota == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(nota);
        }
    }
}
