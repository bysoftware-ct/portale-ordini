package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.Riga;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Riga in entity cache.
 *
 * @author Mario Torrisi
 * @see Riga
 * @generated
 */
public class RigaCacheModel implements CacheModel<Riga>, Externalizable {
    public long id;
    public long idCarrello;
    public long idArticolo;
    public long progressivo;
    public double prezzo;
    public double importo;
    public int pianteRipiano;
    public boolean sormontate;
    public String stringa;
    public String opzioni;
    public double importoVarianti;
    public String codiceVariante;
    public String codiceRegionale;
    public String passaporto;
    public int s2;
    public int s3;
    public int s4;
    public double sconto;
    public double prezzoFornitore;
    public double prezzoEtichetta;
    public String ean;
    public boolean modificato;
    public boolean ricevuto;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(47);

        sb.append("{id=");
        sb.append(id);
        sb.append(", idCarrello=");
        sb.append(idCarrello);
        sb.append(", idArticolo=");
        sb.append(idArticolo);
        sb.append(", progressivo=");
        sb.append(progressivo);
        sb.append(", prezzo=");
        sb.append(prezzo);
        sb.append(", importo=");
        sb.append(importo);
        sb.append(", pianteRipiano=");
        sb.append(pianteRipiano);
        sb.append(", sormontate=");
        sb.append(sormontate);
        sb.append(", stringa=");
        sb.append(stringa);
        sb.append(", opzioni=");
        sb.append(opzioni);
        sb.append(", importoVarianti=");
        sb.append(importoVarianti);
        sb.append(", codiceVariante=");
        sb.append(codiceVariante);
        sb.append(", codiceRegionale=");
        sb.append(codiceRegionale);
        sb.append(", passaporto=");
        sb.append(passaporto);
        sb.append(", s2=");
        sb.append(s2);
        sb.append(", s3=");
        sb.append(s3);
        sb.append(", s4=");
        sb.append(s4);
        sb.append(", sconto=");
        sb.append(sconto);
        sb.append(", prezzoFornitore=");
        sb.append(prezzoFornitore);
        sb.append(", prezzoEtichetta=");
        sb.append(prezzoEtichetta);
        sb.append(", ean=");
        sb.append(ean);
        sb.append(", modificato=");
        sb.append(modificato);
        sb.append(", ricevuto=");
        sb.append(ricevuto);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Riga toEntityModel() {
        RigaImpl rigaImpl = new RigaImpl();

        rigaImpl.setId(id);
        rigaImpl.setIdCarrello(idCarrello);
        rigaImpl.setIdArticolo(idArticolo);
        rigaImpl.setProgressivo(progressivo);
        rigaImpl.setPrezzo(prezzo);
        rigaImpl.setImporto(importo);
        rigaImpl.setPianteRipiano(pianteRipiano);
        rigaImpl.setSormontate(sormontate);

        if (stringa == null) {
            rigaImpl.setStringa(StringPool.BLANK);
        } else {
            rigaImpl.setStringa(stringa);
        }

        if (opzioni == null) {
            rigaImpl.setOpzioni(StringPool.BLANK);
        } else {
            rigaImpl.setOpzioni(opzioni);
        }

        rigaImpl.setImportoVarianti(importoVarianti);

        if (codiceVariante == null) {
            rigaImpl.setCodiceVariante(StringPool.BLANK);
        } else {
            rigaImpl.setCodiceVariante(codiceVariante);
        }

        if (codiceRegionale == null) {
            rigaImpl.setCodiceRegionale(StringPool.BLANK);
        } else {
            rigaImpl.setCodiceRegionale(codiceRegionale);
        }

        if (passaporto == null) {
            rigaImpl.setPassaporto(StringPool.BLANK);
        } else {
            rigaImpl.setPassaporto(passaporto);
        }

        rigaImpl.setS2(s2);
        rigaImpl.setS3(s3);
        rigaImpl.setS4(s4);
        rigaImpl.setSconto(sconto);
        rigaImpl.setPrezzoFornitore(prezzoFornitore);
        rigaImpl.setPrezzoEtichetta(prezzoEtichetta);

        if (ean == null) {
            rigaImpl.setEan(StringPool.BLANK);
        } else {
            rigaImpl.setEan(ean);
        }

        rigaImpl.setModificato(modificato);
        rigaImpl.setRicevuto(ricevuto);

        rigaImpl.resetOriginalValues();

        return rigaImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        idCarrello = objectInput.readLong();
        idArticolo = objectInput.readLong();
        progressivo = objectInput.readLong();
        prezzo = objectInput.readDouble();
        importo = objectInput.readDouble();
        pianteRipiano = objectInput.readInt();
        sormontate = objectInput.readBoolean();
        stringa = objectInput.readUTF();
        opzioni = objectInput.readUTF();
        importoVarianti = objectInput.readDouble();
        codiceVariante = objectInput.readUTF();
        codiceRegionale = objectInput.readUTF();
        passaporto = objectInput.readUTF();
        s2 = objectInput.readInt();
        s3 = objectInput.readInt();
        s4 = objectInput.readInt();
        sconto = objectInput.readDouble();
        prezzoFornitore = objectInput.readDouble();
        prezzoEtichetta = objectInput.readDouble();
        ean = objectInput.readUTF();
        modificato = objectInput.readBoolean();
        ricevuto = objectInput.readBoolean();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(idCarrello);
        objectOutput.writeLong(idArticolo);
        objectOutput.writeLong(progressivo);
        objectOutput.writeDouble(prezzo);
        objectOutput.writeDouble(importo);
        objectOutput.writeInt(pianteRipiano);
        objectOutput.writeBoolean(sormontate);

        if (stringa == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(stringa);
        }

        if (opzioni == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(opzioni);
        }

        objectOutput.writeDouble(importoVarianti);

        if (codiceVariante == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceVariante);
        }

        if (codiceRegionale == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceRegionale);
        }

        if (passaporto == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(passaporto);
        }

        objectOutput.writeInt(s2);
        objectOutput.writeInt(s3);
        objectOutput.writeInt(s4);
        objectOutput.writeDouble(sconto);
        objectOutput.writeDouble(prezzoFornitore);
        objectOutput.writeDouble(prezzoEtichetta);

        if (ean == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(ean);
        }

        objectOutput.writeBoolean(modificato);
        objectOutput.writeBoolean(ricevuto);
    }
}
