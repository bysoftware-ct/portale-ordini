package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.CategorieMerceologiche;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing CategorieMerceologiche in entity cache.
 *
 * @author Mario Torrisi
 * @see CategorieMerceologiche
 * @generated
 */
public class CategorieMerceologicheCacheModel implements CacheModel<CategorieMerceologiche>,
    Externalizable {
    public String codiceCategoria;
    public String descrizione;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(5);

        sb.append("{codiceCategoria=");
        sb.append(codiceCategoria);
        sb.append(", descrizione=");
        sb.append(descrizione);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public CategorieMerceologiche toEntityModel() {
        CategorieMerceologicheImpl categorieMerceologicheImpl = new CategorieMerceologicheImpl();

        if (codiceCategoria == null) {
            categorieMerceologicheImpl.setCodiceCategoria(StringPool.BLANK);
        } else {
            categorieMerceologicheImpl.setCodiceCategoria(codiceCategoria);
        }

        if (descrizione == null) {
            categorieMerceologicheImpl.setDescrizione(StringPool.BLANK);
        } else {
            categorieMerceologicheImpl.setDescrizione(descrizione);
        }

        categorieMerceologicheImpl.resetOriginalValues();

        return categorieMerceologicheImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        codiceCategoria = objectInput.readUTF();
        descrizione = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        if (codiceCategoria == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceCategoria);
        }

        if (descrizione == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(descrizione);
        }
    }
}
