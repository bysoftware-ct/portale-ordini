package it.bysoftware.ct.model.impl;

import it.bysoftware.ct.model.Piante;
import it.bysoftware.ct.service.PianteLocalServiceUtil;

/**
 * The extended model implementation for the Articoli service. Represents a row
 * in the &quot;Articoli&quot; database table, with each column mapped to a
 * property of this class.
 *
 * <p>Helper methods and all application logic should be put in this class.
 * Whenever methods are added, rerun ServiceBuilder to copy their definitions
 * into the {@link it.bysoftware.ct.model.Articoli} interface.
 * </p>
 *
 * @author Mario Torrisi
 */
public class ArticoliImpl extends ArticoliBaseImpl {
    /**
     * 
     */
    private static final long serialVersionUID = -7073056345575640003L;

    public ArticoliImpl() {
    }

    public Piante getPianta(String code) {
        return PianteLocalServiceUtil.findPlants(code);
    }
}
