package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.Carrello;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Carrello in entity cache.
 *
 * @author Mario Torrisi
 * @see Carrello
 * @generated
 */
public class CarrelloCacheModel implements CacheModel<Carrello>, Externalizable {
    public long id;
    public long idOrdine;
    public long progressivo;
    public double importo;
    public boolean chiuso;
    public boolean generico;
    public boolean lavorato;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(15);

        sb.append("{id=");
        sb.append(id);
        sb.append(", idOrdine=");
        sb.append(idOrdine);
        sb.append(", progressivo=");
        sb.append(progressivo);
        sb.append(", importo=");
        sb.append(importo);
        sb.append(", chiuso=");
        sb.append(chiuso);
        sb.append(", generico=");
        sb.append(generico);
        sb.append(", lavorato=");
        sb.append(lavorato);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Carrello toEntityModel() {
        CarrelloImpl carrelloImpl = new CarrelloImpl();

        carrelloImpl.setId(id);
        carrelloImpl.setIdOrdine(idOrdine);
        carrelloImpl.setProgressivo(progressivo);
        carrelloImpl.setImporto(importo);
        carrelloImpl.setChiuso(chiuso);
        carrelloImpl.setGenerico(generico);
        carrelloImpl.setLavorato(lavorato);

        carrelloImpl.resetOriginalValues();

        return carrelloImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        idOrdine = objectInput.readLong();
        progressivo = objectInput.readLong();
        importo = objectInput.readDouble();
        chiuso = objectInput.readBoolean();
        generico = objectInput.readBoolean();
        lavorato = objectInput.readBoolean();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(idOrdine);
        objectOutput.writeLong(progressivo);
        objectOutput.writeDouble(importo);
        objectOutput.writeBoolean(chiuso);
        objectOutput.writeBoolean(generico);
        objectOutput.writeBoolean(lavorato);
    }
}
