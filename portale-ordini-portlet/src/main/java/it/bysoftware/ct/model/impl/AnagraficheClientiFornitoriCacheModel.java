package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.AnagraficheClientiFornitori;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing AnagraficheClientiFornitori in entity cache.
 *
 * @author Mario Torrisi
 * @see AnagraficheClientiFornitori
 * @generated
 */
public class AnagraficheClientiFornitoriCacheModel implements CacheModel<AnagraficheClientiFornitori>,
    Externalizable {
    public boolean attivoEC;
    public String CAP;
    public String codiceAnagrafica;
    public String codiceFiscale;
    public String codiceMnemonico;
    public String comune;
    public String indirizzo;
    public String note;
    public String partitaIVA;
    public String ragioneSociale1;
    public String ragioneSociale2;
    public String siglaProvincia;
    public String siglaStato;
    public boolean personaFisica;
    public String tipoSoggetto;
    public int tipoSollecito;
    public int codiceAzienda;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(35);

        sb.append("{attivoEC=");
        sb.append(attivoEC);
        sb.append(", CAP=");
        sb.append(CAP);
        sb.append(", codiceAnagrafica=");
        sb.append(codiceAnagrafica);
        sb.append(", codiceFiscale=");
        sb.append(codiceFiscale);
        sb.append(", codiceMnemonico=");
        sb.append(codiceMnemonico);
        sb.append(", comune=");
        sb.append(comune);
        sb.append(", indirizzo=");
        sb.append(indirizzo);
        sb.append(", note=");
        sb.append(note);
        sb.append(", partitaIVA=");
        sb.append(partitaIVA);
        sb.append(", ragioneSociale1=");
        sb.append(ragioneSociale1);
        sb.append(", ragioneSociale2=");
        sb.append(ragioneSociale2);
        sb.append(", siglaProvincia=");
        sb.append(siglaProvincia);
        sb.append(", siglaStato=");
        sb.append(siglaStato);
        sb.append(", personaFisica=");
        sb.append(personaFisica);
        sb.append(", tipoSoggetto=");
        sb.append(tipoSoggetto);
        sb.append(", tipoSollecito=");
        sb.append(tipoSollecito);
        sb.append(", codiceAzienda=");
        sb.append(codiceAzienda);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public AnagraficheClientiFornitori toEntityModel() {
        AnagraficheClientiFornitoriImpl anagraficheClientiFornitoriImpl = new AnagraficheClientiFornitoriImpl();

        anagraficheClientiFornitoriImpl.setAttivoEC(attivoEC);

        if (CAP == null) {
            anagraficheClientiFornitoriImpl.setCAP(StringPool.BLANK);
        } else {
            anagraficheClientiFornitoriImpl.setCAP(CAP);
        }

        if (codiceAnagrafica == null) {
            anagraficheClientiFornitoriImpl.setCodiceAnagrafica(StringPool.BLANK);
        } else {
            anagraficheClientiFornitoriImpl.setCodiceAnagrafica(codiceAnagrafica);
        }

        if (codiceFiscale == null) {
            anagraficheClientiFornitoriImpl.setCodiceFiscale(StringPool.BLANK);
        } else {
            anagraficheClientiFornitoriImpl.setCodiceFiscale(codiceFiscale);
        }

        if (codiceMnemonico == null) {
            anagraficheClientiFornitoriImpl.setCodiceMnemonico(StringPool.BLANK);
        } else {
            anagraficheClientiFornitoriImpl.setCodiceMnemonico(codiceMnemonico);
        }

        if (comune == null) {
            anagraficheClientiFornitoriImpl.setComune(StringPool.BLANK);
        } else {
            anagraficheClientiFornitoriImpl.setComune(comune);
        }

        if (indirizzo == null) {
            anagraficheClientiFornitoriImpl.setIndirizzo(StringPool.BLANK);
        } else {
            anagraficheClientiFornitoriImpl.setIndirizzo(indirizzo);
        }

        if (note == null) {
            anagraficheClientiFornitoriImpl.setNote(StringPool.BLANK);
        } else {
            anagraficheClientiFornitoriImpl.setNote(note);
        }

        if (partitaIVA == null) {
            anagraficheClientiFornitoriImpl.setPartitaIVA(StringPool.BLANK);
        } else {
            anagraficheClientiFornitoriImpl.setPartitaIVA(partitaIVA);
        }

        if (ragioneSociale1 == null) {
            anagraficheClientiFornitoriImpl.setRagioneSociale1(StringPool.BLANK);
        } else {
            anagraficheClientiFornitoriImpl.setRagioneSociale1(ragioneSociale1);
        }

        if (ragioneSociale2 == null) {
            anagraficheClientiFornitoriImpl.setRagioneSociale2(StringPool.BLANK);
        } else {
            anagraficheClientiFornitoriImpl.setRagioneSociale2(ragioneSociale2);
        }

        if (siglaProvincia == null) {
            anagraficheClientiFornitoriImpl.setSiglaProvincia(StringPool.BLANK);
        } else {
            anagraficheClientiFornitoriImpl.setSiglaProvincia(siglaProvincia);
        }

        if (siglaStato == null) {
            anagraficheClientiFornitoriImpl.setSiglaStato(StringPool.BLANK);
        } else {
            anagraficheClientiFornitoriImpl.setSiglaStato(siglaStato);
        }

        anagraficheClientiFornitoriImpl.setPersonaFisica(personaFisica);

        if (tipoSoggetto == null) {
            anagraficheClientiFornitoriImpl.setTipoSoggetto(StringPool.BLANK);
        } else {
            anagraficheClientiFornitoriImpl.setTipoSoggetto(tipoSoggetto);
        }

        anagraficheClientiFornitoriImpl.setTipoSollecito(tipoSollecito);
        anagraficheClientiFornitoriImpl.setCodiceAzienda(codiceAzienda);

        anagraficheClientiFornitoriImpl.resetOriginalValues();

        return anagraficheClientiFornitoriImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        attivoEC = objectInput.readBoolean();
        CAP = objectInput.readUTF();
        codiceAnagrafica = objectInput.readUTF();
        codiceFiscale = objectInput.readUTF();
        codiceMnemonico = objectInput.readUTF();
        comune = objectInput.readUTF();
        indirizzo = objectInput.readUTF();
        note = objectInput.readUTF();
        partitaIVA = objectInput.readUTF();
        ragioneSociale1 = objectInput.readUTF();
        ragioneSociale2 = objectInput.readUTF();
        siglaProvincia = objectInput.readUTF();
        siglaStato = objectInput.readUTF();
        personaFisica = objectInput.readBoolean();
        tipoSoggetto = objectInput.readUTF();
        tipoSollecito = objectInput.readInt();
        codiceAzienda = objectInput.readInt();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeBoolean(attivoEC);

        if (CAP == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(CAP);
        }

        if (codiceAnagrafica == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceAnagrafica);
        }

        if (codiceFiscale == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceFiscale);
        }

        if (codiceMnemonico == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceMnemonico);
        }

        if (comune == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(comune);
        }

        if (indirizzo == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(indirizzo);
        }

        if (note == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(note);
        }

        if (partitaIVA == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(partitaIVA);
        }

        if (ragioneSociale1 == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(ragioneSociale1);
        }

        if (ragioneSociale2 == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(ragioneSociale2);
        }

        if (siglaProvincia == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(siglaProvincia);
        }

        if (siglaStato == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(siglaStato);
        }

        objectOutput.writeBoolean(personaFisica);

        if (tipoSoggetto == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(tipoSoggetto);
        }

        objectOutput.writeInt(tipoSollecito);
        objectOutput.writeInt(codiceAzienda);
    }
}
