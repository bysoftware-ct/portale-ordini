package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.Articoli;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Articoli in entity cache.
 *
 * @author Mario Torrisi
 * @see Articoli
 * @generated
 */
public class ArticoliCacheModel implements CacheModel<Articoli>, Externalizable {
    public String codiceArticolo;
    public String categoriaMerceologica;
    public String categoriaInventario;
    public String descrizione;
    public String descrizioneDocumento;
    public String descrizioneFiscale;
    public String unitaMisura;
    public double tara;
    public String codiceIVA;
    public double prezzo1;
    public double prezzo2;
    public double prezzo3;
    public long libLng1;
    public String libStr1;
    public boolean obsoleto;
    public String tipoArticolo;
    public String codiceProvvigione;
    public double pesoLordo;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(37);

        sb.append("{codiceArticolo=");
        sb.append(codiceArticolo);
        sb.append(", categoriaMerceologica=");
        sb.append(categoriaMerceologica);
        sb.append(", categoriaInventario=");
        sb.append(categoriaInventario);
        sb.append(", descrizione=");
        sb.append(descrizione);
        sb.append(", descrizioneDocumento=");
        sb.append(descrizioneDocumento);
        sb.append(", descrizioneFiscale=");
        sb.append(descrizioneFiscale);
        sb.append(", unitaMisura=");
        sb.append(unitaMisura);
        sb.append(", tara=");
        sb.append(tara);
        sb.append(", codiceIVA=");
        sb.append(codiceIVA);
        sb.append(", prezzo1=");
        sb.append(prezzo1);
        sb.append(", prezzo2=");
        sb.append(prezzo2);
        sb.append(", prezzo3=");
        sb.append(prezzo3);
        sb.append(", libLng1=");
        sb.append(libLng1);
        sb.append(", libStr1=");
        sb.append(libStr1);
        sb.append(", obsoleto=");
        sb.append(obsoleto);
        sb.append(", tipoArticolo=");
        sb.append(tipoArticolo);
        sb.append(", codiceProvvigione=");
        sb.append(codiceProvvigione);
        sb.append(", pesoLordo=");
        sb.append(pesoLordo);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Articoli toEntityModel() {
        ArticoliImpl articoliImpl = new ArticoliImpl();

        if (codiceArticolo == null) {
            articoliImpl.setCodiceArticolo(StringPool.BLANK);
        } else {
            articoliImpl.setCodiceArticolo(codiceArticolo);
        }

        if (categoriaMerceologica == null) {
            articoliImpl.setCategoriaMerceologica(StringPool.BLANK);
        } else {
            articoliImpl.setCategoriaMerceologica(categoriaMerceologica);
        }

        if (categoriaInventario == null) {
            articoliImpl.setCategoriaInventario(StringPool.BLANK);
        } else {
            articoliImpl.setCategoriaInventario(categoriaInventario);
        }

        if (descrizione == null) {
            articoliImpl.setDescrizione(StringPool.BLANK);
        } else {
            articoliImpl.setDescrizione(descrizione);
        }

        if (descrizioneDocumento == null) {
            articoliImpl.setDescrizioneDocumento(StringPool.BLANK);
        } else {
            articoliImpl.setDescrizioneDocumento(descrizioneDocumento);
        }

        if (descrizioneFiscale == null) {
            articoliImpl.setDescrizioneFiscale(StringPool.BLANK);
        } else {
            articoliImpl.setDescrizioneFiscale(descrizioneFiscale);
        }

        if (unitaMisura == null) {
            articoliImpl.setUnitaMisura(StringPool.BLANK);
        } else {
            articoliImpl.setUnitaMisura(unitaMisura);
        }

        articoliImpl.setTara(tara);

        if (codiceIVA == null) {
            articoliImpl.setCodiceIVA(StringPool.BLANK);
        } else {
            articoliImpl.setCodiceIVA(codiceIVA);
        }

        articoliImpl.setPrezzo1(prezzo1);
        articoliImpl.setPrezzo2(prezzo2);
        articoliImpl.setPrezzo3(prezzo3);
        articoliImpl.setLibLng1(libLng1);

        if (libStr1 == null) {
            articoliImpl.setLibStr1(StringPool.BLANK);
        } else {
            articoliImpl.setLibStr1(libStr1);
        }

        articoliImpl.setObsoleto(obsoleto);

        if (tipoArticolo == null) {
            articoliImpl.setTipoArticolo(StringPool.BLANK);
        } else {
            articoliImpl.setTipoArticolo(tipoArticolo);
        }

        if (codiceProvvigione == null) {
            articoliImpl.setCodiceProvvigione(StringPool.BLANK);
        } else {
            articoliImpl.setCodiceProvvigione(codiceProvvigione);
        }

        articoliImpl.setPesoLordo(pesoLordo);

        articoliImpl.resetOriginalValues();

        return articoliImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        codiceArticolo = objectInput.readUTF();
        categoriaMerceologica = objectInput.readUTF();
        categoriaInventario = objectInput.readUTF();
        descrizione = objectInput.readUTF();
        descrizioneDocumento = objectInput.readUTF();
        descrizioneFiscale = objectInput.readUTF();
        unitaMisura = objectInput.readUTF();
        tara = objectInput.readDouble();
        codiceIVA = objectInput.readUTF();
        prezzo1 = objectInput.readDouble();
        prezzo2 = objectInput.readDouble();
        prezzo3 = objectInput.readDouble();
        libLng1 = objectInput.readLong();
        libStr1 = objectInput.readUTF();
        obsoleto = objectInput.readBoolean();
        tipoArticolo = objectInput.readUTF();
        codiceProvvigione = objectInput.readUTF();
        pesoLordo = objectInput.readDouble();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        if (codiceArticolo == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceArticolo);
        }

        if (categoriaMerceologica == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(categoriaMerceologica);
        }

        if (categoriaInventario == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(categoriaInventario);
        }

        if (descrizione == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(descrizione);
        }

        if (descrizioneDocumento == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(descrizioneDocumento);
        }

        if (descrizioneFiscale == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(descrizioneFiscale);
        }

        if (unitaMisura == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(unitaMisura);
        }

        objectOutput.writeDouble(tara);

        if (codiceIVA == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceIVA);
        }

        objectOutput.writeDouble(prezzo1);
        objectOutput.writeDouble(prezzo2);
        objectOutput.writeDouble(prezzo3);
        objectOutput.writeLong(libLng1);

        if (libStr1 == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(libStr1);
        }

        objectOutput.writeBoolean(obsoleto);

        if (tipoArticolo == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(tipoArticolo);
        }

        if (codiceProvvigione == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceProvvigione);
        }

        objectOutput.writeDouble(pesoLordo);
    }
}
