package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.model.CausaliEstrattoConto;
import it.bysoftware.ct.model.CausaliEstrattoContoModel;
import it.bysoftware.ct.model.CausaliEstrattoContoSoap;

import java.io.Serializable;

import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The base model implementation for the CausaliEstrattoConto service. Represents a row in the &quot;CausaliEstrattoConto&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link it.bysoftware.ct.model.CausaliEstrattoContoModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link CausaliEstrattoContoImpl}.
 * </p>
 *
 * @author Mario Torrisi
 * @see CausaliEstrattoContoImpl
 * @see it.bysoftware.ct.model.CausaliEstrattoConto
 * @see it.bysoftware.ct.model.CausaliEstrattoContoModel
 * @generated
 */
@JSON(strict = true)
public class CausaliEstrattoContoModelImpl extends BaseModelImpl<CausaliEstrattoConto>
    implements CausaliEstrattoContoModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. All methods that expect a causali estratto conto model instance should use the {@link it.bysoftware.ct.model.CausaliEstrattoConto} interface instead.
     */
    public static final String TABLE_NAME = "CausaliEstrattoConto";
    public static final Object[][] TABLE_COLUMNS = {
            { "Ru3Codcau", Types.VARCHAR },
            { "Ru3Descri", Types.VARCHAR }
        };
    public static final String TABLE_SQL_CREATE = "create table CausaliEstrattoConto (Ru3Codcau VARCHAR(75) not null primary key,Ru3Descri VARCHAR(75) null)";
    public static final String TABLE_SQL_DROP = "drop table CausaliEstrattoConto";
    public static final String ORDER_BY_JPQL = " ORDER BY causaliEstrattoConto.codiceCausaleEC ASC";
    public static final String ORDER_BY_SQL = " ORDER BY CausaliEstrattoConto.Ru3Codcau ASC";
    public static final String DATA_SOURCE = "futuroDS";
    public static final String SESSION_FACTORY = "futuroSessionFactory";
    public static final String TX_MANAGER = "futuroTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.entity.cache.enabled.it.bysoftware.ct.model.CausaliEstrattoConto"),
            true);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.finder.cache.enabled.it.bysoftware.ct.model.CausaliEstrattoConto"),
            true);
    public static final boolean COLUMN_BITMASK_ENABLED = false;
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
                "lock.expiration.time.it.bysoftware.ct.model.CausaliEstrattoConto"));
    private static ClassLoader _classLoader = CausaliEstrattoConto.class.getClassLoader();
    private static Class<?>[] _escapedModelInterfaces = new Class[] {
            CausaliEstrattoConto.class
        };
    private String _codiceCausaleEC;
    private String _descrizioneCausaleEC;
    private CausaliEstrattoConto _escapedModel;

    public CausaliEstrattoContoModelImpl() {
    }

    /**
     * Converts the soap model instance into a normal model instance.
     *
     * @param soapModel the soap model instance to convert
     * @return the normal model instance
     */
    public static CausaliEstrattoConto toModel(
        CausaliEstrattoContoSoap soapModel) {
        if (soapModel == null) {
            return null;
        }

        CausaliEstrattoConto model = new CausaliEstrattoContoImpl();

        model.setCodiceCausaleEC(soapModel.getCodiceCausaleEC());
        model.setDescrizioneCausaleEC(soapModel.getDescrizioneCausaleEC());

        return model;
    }

    /**
     * Converts the soap model instances into normal model instances.
     *
     * @param soapModels the soap model instances to convert
     * @return the normal model instances
     */
    public static List<CausaliEstrattoConto> toModels(
        CausaliEstrattoContoSoap[] soapModels) {
        if (soapModels == null) {
            return null;
        }

        List<CausaliEstrattoConto> models = new ArrayList<CausaliEstrattoConto>(soapModels.length);

        for (CausaliEstrattoContoSoap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    @Override
    public String getPrimaryKey() {
        return _codiceCausaleEC;
    }

    @Override
    public void setPrimaryKey(String primaryKey) {
        setCodiceCausaleEC(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _codiceCausaleEC;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((String) primaryKeyObj);
    }

    @Override
    public Class<?> getModelClass() {
        return CausaliEstrattoConto.class;
    }

    @Override
    public String getModelClassName() {
        return CausaliEstrattoConto.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("codiceCausaleEC", getCodiceCausaleEC());
        attributes.put("descrizioneCausaleEC", getDescrizioneCausaleEC());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        String codiceCausaleEC = (String) attributes.get("codiceCausaleEC");

        if (codiceCausaleEC != null) {
            setCodiceCausaleEC(codiceCausaleEC);
        }

        String descrizioneCausaleEC = (String) attributes.get(
                "descrizioneCausaleEC");

        if (descrizioneCausaleEC != null) {
            setDescrizioneCausaleEC(descrizioneCausaleEC);
        }
    }

    @JSON
    @Override
    public String getCodiceCausaleEC() {
        if (_codiceCausaleEC == null) {
            return StringPool.BLANK;
        } else {
            return _codiceCausaleEC;
        }
    }

    @Override
    public void setCodiceCausaleEC(String codiceCausaleEC) {
        _codiceCausaleEC = codiceCausaleEC;
    }

    @JSON
    @Override
    public String getDescrizioneCausaleEC() {
        if (_descrizioneCausaleEC == null) {
            return StringPool.BLANK;
        } else {
            return _descrizioneCausaleEC;
        }
    }

    @Override
    public void setDescrizioneCausaleEC(String descrizioneCausaleEC) {
        _descrizioneCausaleEC = descrizioneCausaleEC;
    }

    @Override
    public CausaliEstrattoConto toEscapedModel() {
        if (_escapedModel == null) {
            _escapedModel = (CausaliEstrattoConto) ProxyUtil.newProxyInstance(_classLoader,
                    _escapedModelInterfaces, new AutoEscapeBeanHandler(this));
        }

        return _escapedModel;
    }

    @Override
    public Object clone() {
        CausaliEstrattoContoImpl causaliEstrattoContoImpl = new CausaliEstrattoContoImpl();

        causaliEstrattoContoImpl.setCodiceCausaleEC(getCodiceCausaleEC());
        causaliEstrattoContoImpl.setDescrizioneCausaleEC(getDescrizioneCausaleEC());

        causaliEstrattoContoImpl.resetOriginalValues();

        return causaliEstrattoContoImpl;
    }

    @Override
    public int compareTo(CausaliEstrattoConto causaliEstrattoConto) {
        String primaryKey = causaliEstrattoConto.getPrimaryKey();

        return getPrimaryKey().compareTo(primaryKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CausaliEstrattoConto)) {
            return false;
        }

        CausaliEstrattoConto causaliEstrattoConto = (CausaliEstrattoConto) obj;

        String primaryKey = causaliEstrattoConto.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public void resetOriginalValues() {
    }

    @Override
    public CacheModel<CausaliEstrattoConto> toCacheModel() {
        CausaliEstrattoContoCacheModel causaliEstrattoContoCacheModel = new CausaliEstrattoContoCacheModel();

        causaliEstrattoContoCacheModel.codiceCausaleEC = getCodiceCausaleEC();

        String codiceCausaleEC = causaliEstrattoContoCacheModel.codiceCausaleEC;

        if ((codiceCausaleEC != null) && (codiceCausaleEC.length() == 0)) {
            causaliEstrattoContoCacheModel.codiceCausaleEC = null;
        }

        causaliEstrattoContoCacheModel.descrizioneCausaleEC = getDescrizioneCausaleEC();

        String descrizioneCausaleEC = causaliEstrattoContoCacheModel.descrizioneCausaleEC;

        if ((descrizioneCausaleEC != null) &&
                (descrizioneCausaleEC.length() == 0)) {
            causaliEstrattoContoCacheModel.descrizioneCausaleEC = null;
        }

        return causaliEstrattoContoCacheModel;
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(5);

        sb.append("{codiceCausaleEC=");
        sb.append(getCodiceCausaleEC());
        sb.append(", descrizioneCausaleEC=");
        sb.append(getDescrizioneCausaleEC());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(10);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.CausaliEstrattoConto");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>codiceCausaleEC</column-name><column-value><![CDATA[");
        sb.append(getCodiceCausaleEC());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>descrizioneCausaleEC</column-name><column-value><![CDATA[");
        sb.append(getDescrizioneCausaleEC());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
