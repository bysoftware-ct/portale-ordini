package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.model.AnagraficheClientiFornitori;
import it.bysoftware.ct.model.AnagraficheClientiFornitoriModel;
import it.bysoftware.ct.model.AnagraficheClientiFornitoriSoap;

import java.io.Serializable;

import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The base model implementation for the AnagraficheClientiFornitori service. Represents a row in the &quot;AnagraficheClientiFornitori&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link it.bysoftware.ct.model.AnagraficheClientiFornitoriModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link AnagraficheClientiFornitoriImpl}.
 * </p>
 *
 * @author Mario Torrisi
 * @see AnagraficheClientiFornitoriImpl
 * @see it.bysoftware.ct.model.AnagraficheClientiFornitori
 * @see it.bysoftware.ct.model.AnagraficheClientiFornitoriModel
 * @generated
 */
@JSON(strict = true)
public class AnagraficheClientiFornitoriModelImpl extends BaseModelImpl<AnagraficheClientiFornitori>
    implements AnagraficheClientiFornitoriModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. All methods that expect a anagrafiche clienti fornitori model instance should use the {@link it.bysoftware.ct.model.AnagraficheClientiFornitori} interface instead.
     */
    public static final String TABLE_NAME = "AnagraficheClientiFornitori";
    public static final Object[][] TABLE_COLUMNS = {
            { "RacAttivoEC", Types.BOOLEAN },
            { "RacCap", Types.VARCHAR },
            { "RacCodana", Types.VARCHAR },
            { "RacCodfis", Types.VARCHAR },
            { "RacCodmne", Types.VARCHAR },
            { "RacComune", Types.VARCHAR },
            { "RacIndiri", Types.VARCHAR },
            { "RacNote", Types.VARCHAR },
            { "RacPariva", Types.VARCHAR },
            { "RacRagso1", Types.VARCHAR },
            { "RacRagso2", Types.VARCHAR },
            { "RacSigpro", Types.VARCHAR },
            { "RacSigsta", Types.VARCHAR },
            { "RacTepefi", Types.BOOLEAN },
            { "RacTipoSoggetto", Types.VARCHAR },
            { "RacTipsol", Types.INTEGER },
            { "RacCodiaz", Types.INTEGER }
        };
    public static final String TABLE_SQL_CREATE = "create table AnagraficheClientiFornitori (RacAttivoEC BOOLEAN,RacCap VARCHAR(75) null,RacCodana VARCHAR(75) not null primary key,RacCodfis VARCHAR(75) null,RacCodmne VARCHAR(75) null,RacComune VARCHAR(75) null,RacIndiri VARCHAR(75) null,RacNote VARCHAR(75) null,RacPariva VARCHAR(75) null,RacRagso1 VARCHAR(75) null,RacRagso2 VARCHAR(75) null,RacSigpro VARCHAR(75) null,RacSigsta VARCHAR(75) null,RacTepefi BOOLEAN,RacTipoSoggetto VARCHAR(75) null,RacTipsol INTEGER,RacCodiaz INTEGER)";
    public static final String TABLE_SQL_DROP = "drop table AnagraficheClientiFornitori";
    public static final String ORDER_BY_JPQL = " ORDER BY anagraficheClientiFornitori.codiceAnagrafica ASC";
    public static final String ORDER_BY_SQL = " ORDER BY AnagraficheClientiFornitori.RacCodana ASC";
    public static final String DATA_SOURCE = "futuroDS";
    public static final String SESSION_FACTORY = "futuroSessionFactory";
    public static final String TX_MANAGER = "futuroTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.entity.cache.enabled.it.bysoftware.ct.model.AnagraficheClientiFornitori"),
            false);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.finder.cache.enabled.it.bysoftware.ct.model.AnagraficheClientiFornitori"),
            false);
    public static final boolean COLUMN_BITMASK_ENABLED = false;
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
                "lock.expiration.time.it.bysoftware.ct.model.AnagraficheClientiFornitori"));
    private static ClassLoader _classLoader = AnagraficheClientiFornitori.class.getClassLoader();
    private static Class<?>[] _escapedModelInterfaces = new Class[] {
            AnagraficheClientiFornitori.class
        };
    private boolean _attivoEC;
    private String _CAP;
    private String _codiceAnagrafica;
    private String _codiceFiscale;
    private String _codiceMnemonico;
    private String _comune;
    private String _indirizzo;
    private String _note;
    private String _partitaIVA;
    private String _ragioneSociale1;
    private String _ragioneSociale2;
    private String _siglaProvincia;
    private String _siglaStato;
    private boolean _personaFisica;
    private String _tipoSoggetto;
    private int _tipoSollecito;
    private int _codiceAzienda;
    private AnagraficheClientiFornitori _escapedModel;

    public AnagraficheClientiFornitoriModelImpl() {
    }

    /**
     * Converts the soap model instance into a normal model instance.
     *
     * @param soapModel the soap model instance to convert
     * @return the normal model instance
     */
    public static AnagraficheClientiFornitori toModel(
        AnagraficheClientiFornitoriSoap soapModel) {
        if (soapModel == null) {
            return null;
        }

        AnagraficheClientiFornitori model = new AnagraficheClientiFornitoriImpl();

        model.setAttivoEC(soapModel.getAttivoEC());
        model.setCAP(soapModel.getCAP());
        model.setCodiceAnagrafica(soapModel.getCodiceAnagrafica());
        model.setCodiceFiscale(soapModel.getCodiceFiscale());
        model.setCodiceMnemonico(soapModel.getCodiceMnemonico());
        model.setComune(soapModel.getComune());
        model.setIndirizzo(soapModel.getIndirizzo());
        model.setNote(soapModel.getNote());
        model.setPartitaIVA(soapModel.getPartitaIVA());
        model.setRagioneSociale1(soapModel.getRagioneSociale1());
        model.setRagioneSociale2(soapModel.getRagioneSociale2());
        model.setSiglaProvincia(soapModel.getSiglaProvincia());
        model.setSiglaStato(soapModel.getSiglaStato());
        model.setPersonaFisica(soapModel.getPersonaFisica());
        model.setTipoSoggetto(soapModel.getTipoSoggetto());
        model.setTipoSollecito(soapModel.getTipoSollecito());
        model.setCodiceAzienda(soapModel.getCodiceAzienda());

        return model;
    }

    /**
     * Converts the soap model instances into normal model instances.
     *
     * @param soapModels the soap model instances to convert
     * @return the normal model instances
     */
    public static List<AnagraficheClientiFornitori> toModels(
        AnagraficheClientiFornitoriSoap[] soapModels) {
        if (soapModels == null) {
            return null;
        }

        List<AnagraficheClientiFornitori> models = new ArrayList<AnagraficheClientiFornitori>(soapModels.length);

        for (AnagraficheClientiFornitoriSoap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    @Override
    public String getPrimaryKey() {
        return _codiceAnagrafica;
    }

    @Override
    public void setPrimaryKey(String primaryKey) {
        setCodiceAnagrafica(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _codiceAnagrafica;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey((String) primaryKeyObj);
    }

    @Override
    public Class<?> getModelClass() {
        return AnagraficheClientiFornitori.class;
    }

    @Override
    public String getModelClassName() {
        return AnagraficheClientiFornitori.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("attivoEC", getAttivoEC());
        attributes.put("CAP", getCAP());
        attributes.put("codiceAnagrafica", getCodiceAnagrafica());
        attributes.put("codiceFiscale", getCodiceFiscale());
        attributes.put("codiceMnemonico", getCodiceMnemonico());
        attributes.put("comune", getComune());
        attributes.put("indirizzo", getIndirizzo());
        attributes.put("note", getNote());
        attributes.put("partitaIVA", getPartitaIVA());
        attributes.put("ragioneSociale1", getRagioneSociale1());
        attributes.put("ragioneSociale2", getRagioneSociale2());
        attributes.put("siglaProvincia", getSiglaProvincia());
        attributes.put("siglaStato", getSiglaStato());
        attributes.put("personaFisica", getPersonaFisica());
        attributes.put("tipoSoggetto", getTipoSoggetto());
        attributes.put("tipoSollecito", getTipoSollecito());
        attributes.put("codiceAzienda", getCodiceAzienda());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Boolean attivoEC = (Boolean) attributes.get("attivoEC");

        if (attivoEC != null) {
            setAttivoEC(attivoEC);
        }

        String CAP = (String) attributes.get("CAP");

        if (CAP != null) {
            setCAP(CAP);
        }

        String codiceAnagrafica = (String) attributes.get("codiceAnagrafica");

        if (codiceAnagrafica != null) {
            setCodiceAnagrafica(codiceAnagrafica);
        }

        String codiceFiscale = (String) attributes.get("codiceFiscale");

        if (codiceFiscale != null) {
            setCodiceFiscale(codiceFiscale);
        }

        String codiceMnemonico = (String) attributes.get("codiceMnemonico");

        if (codiceMnemonico != null) {
            setCodiceMnemonico(codiceMnemonico);
        }

        String comune = (String) attributes.get("comune");

        if (comune != null) {
            setComune(comune);
        }

        String indirizzo = (String) attributes.get("indirizzo");

        if (indirizzo != null) {
            setIndirizzo(indirizzo);
        }

        String note = (String) attributes.get("note");

        if (note != null) {
            setNote(note);
        }

        String partitaIVA = (String) attributes.get("partitaIVA");

        if (partitaIVA != null) {
            setPartitaIVA(partitaIVA);
        }

        String ragioneSociale1 = (String) attributes.get("ragioneSociale1");

        if (ragioneSociale1 != null) {
            setRagioneSociale1(ragioneSociale1);
        }

        String ragioneSociale2 = (String) attributes.get("ragioneSociale2");

        if (ragioneSociale2 != null) {
            setRagioneSociale2(ragioneSociale2);
        }

        String siglaProvincia = (String) attributes.get("siglaProvincia");

        if (siglaProvincia != null) {
            setSiglaProvincia(siglaProvincia);
        }

        String siglaStato = (String) attributes.get("siglaStato");

        if (siglaStato != null) {
            setSiglaStato(siglaStato);
        }

        Boolean personaFisica = (Boolean) attributes.get("personaFisica");

        if (personaFisica != null) {
            setPersonaFisica(personaFisica);
        }

        String tipoSoggetto = (String) attributes.get("tipoSoggetto");

        if (tipoSoggetto != null) {
            setTipoSoggetto(tipoSoggetto);
        }

        Integer tipoSollecito = (Integer) attributes.get("tipoSollecito");

        if (tipoSollecito != null) {
            setTipoSollecito(tipoSollecito);
        }

        Integer codiceAzienda = (Integer) attributes.get("codiceAzienda");

        if (codiceAzienda != null) {
            setCodiceAzienda(codiceAzienda);
        }
    }

    @JSON
    @Override
    public boolean getAttivoEC() {
        return _attivoEC;
    }

    @Override
    public boolean isAttivoEC() {
        return _attivoEC;
    }

    @Override
    public void setAttivoEC(boolean attivoEC) {
        _attivoEC = attivoEC;
    }

    @JSON
    @Override
    public String getCAP() {
        if (_CAP == null) {
            return StringPool.BLANK;
        } else {
            return _CAP;
        }
    }

    @Override
    public void setCAP(String CAP) {
        _CAP = CAP;
    }

    @JSON
    @Override
    public String getCodiceAnagrafica() {
        if (_codiceAnagrafica == null) {
            return StringPool.BLANK;
        } else {
            return _codiceAnagrafica;
        }
    }

    @Override
    public void setCodiceAnagrafica(String codiceAnagrafica) {
        _codiceAnagrafica = codiceAnagrafica;
    }

    @JSON
    @Override
    public String getCodiceFiscale() {
        if (_codiceFiscale == null) {
            return StringPool.BLANK;
        } else {
            return _codiceFiscale;
        }
    }

    @Override
    public void setCodiceFiscale(String codiceFiscale) {
        _codiceFiscale = codiceFiscale;
    }

    @JSON
    @Override
    public String getCodiceMnemonico() {
        if (_codiceMnemonico == null) {
            return StringPool.BLANK;
        } else {
            return _codiceMnemonico;
        }
    }

    @Override
    public void setCodiceMnemonico(String codiceMnemonico) {
        _codiceMnemonico = codiceMnemonico;
    }

    @JSON
    @Override
    public String getComune() {
        if (_comune == null) {
            return StringPool.BLANK;
        } else {
            return _comune;
        }
    }

    @Override
    public void setComune(String comune) {
        _comune = comune;
    }

    @JSON
    @Override
    public String getIndirizzo() {
        if (_indirizzo == null) {
            return StringPool.BLANK;
        } else {
            return _indirizzo;
        }
    }

    @Override
    public void setIndirizzo(String indirizzo) {
        _indirizzo = indirizzo;
    }

    @JSON
    @Override
    public String getNote() {
        if (_note == null) {
            return StringPool.BLANK;
        } else {
            return _note;
        }
    }

    @Override
    public void setNote(String note) {
        _note = note;
    }

    @JSON
    @Override
    public String getPartitaIVA() {
        if (_partitaIVA == null) {
            return StringPool.BLANK;
        } else {
            return _partitaIVA;
        }
    }

    @Override
    public void setPartitaIVA(String partitaIVA) {
        _partitaIVA = partitaIVA;
    }

    @JSON
    @Override
    public String getRagioneSociale1() {
        if (_ragioneSociale1 == null) {
            return StringPool.BLANK;
        } else {
            return _ragioneSociale1;
        }
    }

    @Override
    public void setRagioneSociale1(String ragioneSociale1) {
        _ragioneSociale1 = ragioneSociale1;
    }

    @JSON
    @Override
    public String getRagioneSociale2() {
        if (_ragioneSociale2 == null) {
            return StringPool.BLANK;
        } else {
            return _ragioneSociale2;
        }
    }

    @Override
    public void setRagioneSociale2(String ragioneSociale2) {
        _ragioneSociale2 = ragioneSociale2;
    }

    @JSON
    @Override
    public String getSiglaProvincia() {
        if (_siglaProvincia == null) {
            return StringPool.BLANK;
        } else {
            return _siglaProvincia;
        }
    }

    @Override
    public void setSiglaProvincia(String siglaProvincia) {
        _siglaProvincia = siglaProvincia;
    }

    @JSON
    @Override
    public String getSiglaStato() {
        if (_siglaStato == null) {
            return StringPool.BLANK;
        } else {
            return _siglaStato;
        }
    }

    @Override
    public void setSiglaStato(String siglaStato) {
        _siglaStato = siglaStato;
    }

    @JSON
    @Override
    public boolean getPersonaFisica() {
        return _personaFisica;
    }

    @Override
    public boolean isPersonaFisica() {
        return _personaFisica;
    }

    @Override
    public void setPersonaFisica(boolean personaFisica) {
        _personaFisica = personaFisica;
    }

    @JSON
    @Override
    public String getTipoSoggetto() {
        if (_tipoSoggetto == null) {
            return StringPool.BLANK;
        } else {
            return _tipoSoggetto;
        }
    }

    @Override
    public void setTipoSoggetto(String tipoSoggetto) {
        _tipoSoggetto = tipoSoggetto;
    }

    @JSON
    @Override
    public int getTipoSollecito() {
        return _tipoSollecito;
    }

    @Override
    public void setTipoSollecito(int tipoSollecito) {
        _tipoSollecito = tipoSollecito;
    }

    @JSON
    @Override
    public int getCodiceAzienda() {
        return _codiceAzienda;
    }

    @Override
    public void setCodiceAzienda(int codiceAzienda) {
        _codiceAzienda = codiceAzienda;
    }

    @Override
    public AnagraficheClientiFornitori toEscapedModel() {
        if (_escapedModel == null) {
            _escapedModel = (AnagraficheClientiFornitori) ProxyUtil.newProxyInstance(_classLoader,
                    _escapedModelInterfaces, new AutoEscapeBeanHandler(this));
        }

        return _escapedModel;
    }

    @Override
    public Object clone() {
        AnagraficheClientiFornitoriImpl anagraficheClientiFornitoriImpl = new AnagraficheClientiFornitoriImpl();

        anagraficheClientiFornitoriImpl.setAttivoEC(getAttivoEC());
        anagraficheClientiFornitoriImpl.setCAP(getCAP());
        anagraficheClientiFornitoriImpl.setCodiceAnagrafica(getCodiceAnagrafica());
        anagraficheClientiFornitoriImpl.setCodiceFiscale(getCodiceFiscale());
        anagraficheClientiFornitoriImpl.setCodiceMnemonico(getCodiceMnemonico());
        anagraficheClientiFornitoriImpl.setComune(getComune());
        anagraficheClientiFornitoriImpl.setIndirizzo(getIndirizzo());
        anagraficheClientiFornitoriImpl.setNote(getNote());
        anagraficheClientiFornitoriImpl.setPartitaIVA(getPartitaIVA());
        anagraficheClientiFornitoriImpl.setRagioneSociale1(getRagioneSociale1());
        anagraficheClientiFornitoriImpl.setRagioneSociale2(getRagioneSociale2());
        anagraficheClientiFornitoriImpl.setSiglaProvincia(getSiglaProvincia());
        anagraficheClientiFornitoriImpl.setSiglaStato(getSiglaStato());
        anagraficheClientiFornitoriImpl.setPersonaFisica(getPersonaFisica());
        anagraficheClientiFornitoriImpl.setTipoSoggetto(getTipoSoggetto());
        anagraficheClientiFornitoriImpl.setTipoSollecito(getTipoSollecito());
        anagraficheClientiFornitoriImpl.setCodiceAzienda(getCodiceAzienda());

        anagraficheClientiFornitoriImpl.resetOriginalValues();

        return anagraficheClientiFornitoriImpl;
    }

    @Override
    public int compareTo(
        AnagraficheClientiFornitori anagraficheClientiFornitori) {
        int value = 0;

        value = getCodiceAnagrafica()
                    .compareTo(anagraficheClientiFornitori.getCodiceAnagrafica());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof AnagraficheClientiFornitori)) {
            return false;
        }

        AnagraficheClientiFornitori anagraficheClientiFornitori = (AnagraficheClientiFornitori) obj;

        String primaryKey = anagraficheClientiFornitori.getPrimaryKey();

        if (getPrimaryKey().equals(primaryKey)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    @Override
    public void resetOriginalValues() {
    }

    @Override
    public CacheModel<AnagraficheClientiFornitori> toCacheModel() {
        AnagraficheClientiFornitoriCacheModel anagraficheClientiFornitoriCacheModel =
            new AnagraficheClientiFornitoriCacheModel();

        anagraficheClientiFornitoriCacheModel.attivoEC = getAttivoEC();

        anagraficheClientiFornitoriCacheModel.CAP = getCAP();

        String CAP = anagraficheClientiFornitoriCacheModel.CAP;

        if ((CAP != null) && (CAP.length() == 0)) {
            anagraficheClientiFornitoriCacheModel.CAP = null;
        }

        anagraficheClientiFornitoriCacheModel.codiceAnagrafica = getCodiceAnagrafica();

        String codiceAnagrafica = anagraficheClientiFornitoriCacheModel.codiceAnagrafica;

        if ((codiceAnagrafica != null) && (codiceAnagrafica.length() == 0)) {
            anagraficheClientiFornitoriCacheModel.codiceAnagrafica = null;
        }

        anagraficheClientiFornitoriCacheModel.codiceFiscale = getCodiceFiscale();

        String codiceFiscale = anagraficheClientiFornitoriCacheModel.codiceFiscale;

        if ((codiceFiscale != null) && (codiceFiscale.length() == 0)) {
            anagraficheClientiFornitoriCacheModel.codiceFiscale = null;
        }

        anagraficheClientiFornitoriCacheModel.codiceMnemonico = getCodiceMnemonico();

        String codiceMnemonico = anagraficheClientiFornitoriCacheModel.codiceMnemonico;

        if ((codiceMnemonico != null) && (codiceMnemonico.length() == 0)) {
            anagraficheClientiFornitoriCacheModel.codiceMnemonico = null;
        }

        anagraficheClientiFornitoriCacheModel.comune = getComune();

        String comune = anagraficheClientiFornitoriCacheModel.comune;

        if ((comune != null) && (comune.length() == 0)) {
            anagraficheClientiFornitoriCacheModel.comune = null;
        }

        anagraficheClientiFornitoriCacheModel.indirizzo = getIndirizzo();

        String indirizzo = anagraficheClientiFornitoriCacheModel.indirizzo;

        if ((indirizzo != null) && (indirizzo.length() == 0)) {
            anagraficheClientiFornitoriCacheModel.indirizzo = null;
        }

        anagraficheClientiFornitoriCacheModel.note = getNote();

        String note = anagraficheClientiFornitoriCacheModel.note;

        if ((note != null) && (note.length() == 0)) {
            anagraficheClientiFornitoriCacheModel.note = null;
        }

        anagraficheClientiFornitoriCacheModel.partitaIVA = getPartitaIVA();

        String partitaIVA = anagraficheClientiFornitoriCacheModel.partitaIVA;

        if ((partitaIVA != null) && (partitaIVA.length() == 0)) {
            anagraficheClientiFornitoriCacheModel.partitaIVA = null;
        }

        anagraficheClientiFornitoriCacheModel.ragioneSociale1 = getRagioneSociale1();

        String ragioneSociale1 = anagraficheClientiFornitoriCacheModel.ragioneSociale1;

        if ((ragioneSociale1 != null) && (ragioneSociale1.length() == 0)) {
            anagraficheClientiFornitoriCacheModel.ragioneSociale1 = null;
        }

        anagraficheClientiFornitoriCacheModel.ragioneSociale2 = getRagioneSociale2();

        String ragioneSociale2 = anagraficheClientiFornitoriCacheModel.ragioneSociale2;

        if ((ragioneSociale2 != null) && (ragioneSociale2.length() == 0)) {
            anagraficheClientiFornitoriCacheModel.ragioneSociale2 = null;
        }

        anagraficheClientiFornitoriCacheModel.siglaProvincia = getSiglaProvincia();

        String siglaProvincia = anagraficheClientiFornitoriCacheModel.siglaProvincia;

        if ((siglaProvincia != null) && (siglaProvincia.length() == 0)) {
            anagraficheClientiFornitoriCacheModel.siglaProvincia = null;
        }

        anagraficheClientiFornitoriCacheModel.siglaStato = getSiglaStato();

        String siglaStato = anagraficheClientiFornitoriCacheModel.siglaStato;

        if ((siglaStato != null) && (siglaStato.length() == 0)) {
            anagraficheClientiFornitoriCacheModel.siglaStato = null;
        }

        anagraficheClientiFornitoriCacheModel.personaFisica = getPersonaFisica();

        anagraficheClientiFornitoriCacheModel.tipoSoggetto = getTipoSoggetto();

        String tipoSoggetto = anagraficheClientiFornitoriCacheModel.tipoSoggetto;

        if ((tipoSoggetto != null) && (tipoSoggetto.length() == 0)) {
            anagraficheClientiFornitoriCacheModel.tipoSoggetto = null;
        }

        anagraficheClientiFornitoriCacheModel.tipoSollecito = getTipoSollecito();

        anagraficheClientiFornitoriCacheModel.codiceAzienda = getCodiceAzienda();

        return anagraficheClientiFornitoriCacheModel;
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(35);

        sb.append("{attivoEC=");
        sb.append(getAttivoEC());
        sb.append(", CAP=");
        sb.append(getCAP());
        sb.append(", codiceAnagrafica=");
        sb.append(getCodiceAnagrafica());
        sb.append(", codiceFiscale=");
        sb.append(getCodiceFiscale());
        sb.append(", codiceMnemonico=");
        sb.append(getCodiceMnemonico());
        sb.append(", comune=");
        sb.append(getComune());
        sb.append(", indirizzo=");
        sb.append(getIndirizzo());
        sb.append(", note=");
        sb.append(getNote());
        sb.append(", partitaIVA=");
        sb.append(getPartitaIVA());
        sb.append(", ragioneSociale1=");
        sb.append(getRagioneSociale1());
        sb.append(", ragioneSociale2=");
        sb.append(getRagioneSociale2());
        sb.append(", siglaProvincia=");
        sb.append(getSiglaProvincia());
        sb.append(", siglaStato=");
        sb.append(getSiglaStato());
        sb.append(", personaFisica=");
        sb.append(getPersonaFisica());
        sb.append(", tipoSoggetto=");
        sb.append(getTipoSoggetto());
        sb.append(", tipoSollecito=");
        sb.append(getTipoSollecito());
        sb.append(", codiceAzienda=");
        sb.append(getCodiceAzienda());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(55);

        sb.append("<model><model-name>");
        sb.append("it.bysoftware.ct.model.AnagraficheClientiFornitori");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>attivoEC</column-name><column-value><![CDATA[");
        sb.append(getAttivoEC());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>CAP</column-name><column-value><![CDATA[");
        sb.append(getCAP());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceAnagrafica</column-name><column-value><![CDATA[");
        sb.append(getCodiceAnagrafica());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceFiscale</column-name><column-value><![CDATA[");
        sb.append(getCodiceFiscale());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceMnemonico</column-name><column-value><![CDATA[");
        sb.append(getCodiceMnemonico());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>comune</column-name><column-value><![CDATA[");
        sb.append(getComune());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>indirizzo</column-name><column-value><![CDATA[");
        sb.append(getIndirizzo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>note</column-name><column-value><![CDATA[");
        sb.append(getNote());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>partitaIVA</column-name><column-value><![CDATA[");
        sb.append(getPartitaIVA());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>ragioneSociale1</column-name><column-value><![CDATA[");
        sb.append(getRagioneSociale1());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>ragioneSociale2</column-name><column-value><![CDATA[");
        sb.append(getRagioneSociale2());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>siglaProvincia</column-name><column-value><![CDATA[");
        sb.append(getSiglaProvincia());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>siglaStato</column-name><column-value><![CDATA[");
        sb.append(getSiglaStato());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>personaFisica</column-name><column-value><![CDATA[");
        sb.append(getPersonaFisica());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tipoSoggetto</column-name><column-value><![CDATA[");
        sb.append(getTipoSoggetto());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tipoSollecito</column-name><column-value><![CDATA[");
        sb.append(getTipoSollecito());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceAzienda</column-name><column-value><![CDATA[");
        sb.append(getCodiceAzienda());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
