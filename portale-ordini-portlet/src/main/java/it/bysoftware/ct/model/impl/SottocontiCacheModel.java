package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.Sottoconti;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Sottoconti in entity cache.
 *
 * @author Mario Torrisi
 * @see Sottoconti
 * @generated
 */
public class SottocontiCacheModel implements CacheModel<Sottoconti>,
    Externalizable {
    public String codice;
    public String descrizione;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(5);

        sb.append("{codice=");
        sb.append(codice);
        sb.append(", descrizione=");
        sb.append(descrizione);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Sottoconti toEntityModel() {
        SottocontiImpl sottocontiImpl = new SottocontiImpl();

        if (codice == null) {
            sottocontiImpl.setCodice(StringPool.BLANK);
        } else {
            sottocontiImpl.setCodice(codice);
        }

        if (descrizione == null) {
            sottocontiImpl.setDescrizione(StringPool.BLANK);
        } else {
            sottocontiImpl.setDescrizione(descrizione);
        }

        sottocontiImpl.resetOriginalValues();

        return sottocontiImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        codice = objectInput.readUTF();
        descrizione = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        if (codice == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codice);
        }

        if (descrizione == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(descrizione);
        }
    }
}
