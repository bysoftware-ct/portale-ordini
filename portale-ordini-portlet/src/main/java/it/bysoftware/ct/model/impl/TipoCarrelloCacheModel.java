package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.TipoCarrello;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing TipoCarrello in entity cache.
 *
 * @author Mario Torrisi
 * @see TipoCarrello
 * @generated
 */
public class TipoCarrelloCacheModel implements CacheModel<TipoCarrello>,
    Externalizable {
    public long id;
    public String tipologia;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(5);

        sb.append("{id=");
        sb.append(id);
        sb.append(", tipologia=");
        sb.append(tipologia);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public TipoCarrello toEntityModel() {
        TipoCarrelloImpl tipoCarrelloImpl = new TipoCarrelloImpl();

        tipoCarrelloImpl.setId(id);

        if (tipologia == null) {
            tipoCarrelloImpl.setTipologia(StringPool.BLANK);
        } else {
            tipoCarrelloImpl.setTipologia(tipologia);
        }

        tipoCarrelloImpl.resetOriginalValues();

        return tipoCarrelloImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        tipologia = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);

        if (tipologia == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(tipologia);
        }
    }
}
