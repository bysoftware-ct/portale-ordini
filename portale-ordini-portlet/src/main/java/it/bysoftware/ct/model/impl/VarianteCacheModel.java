package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.Variante;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Variante in entity cache.
 *
 * @author Mario Torrisi
 * @see Variante
 * @generated
 */
public class VarianteCacheModel implements CacheModel<Variante>, Externalizable {
    public long id;
    public long idOpzione;
    public String variante;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{id=");
        sb.append(id);
        sb.append(", idOpzione=");
        sb.append(idOpzione);
        sb.append(", variante=");
        sb.append(variante);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Variante toEntityModel() {
        VarianteImpl varianteImpl = new VarianteImpl();

        varianteImpl.setId(id);
        varianteImpl.setIdOpzione(idOpzione);

        if (variante == null) {
            varianteImpl.setVariante(StringPool.BLANK);
        } else {
            varianteImpl.setVariante(variante);
        }

        varianteImpl.resetOriginalValues();

        return varianteImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        idOpzione = objectInput.readLong();
        variante = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(idOpzione);

        if (variante == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(variante);
        }
    }
}
