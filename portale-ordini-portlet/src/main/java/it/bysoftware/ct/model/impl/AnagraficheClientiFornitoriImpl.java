package it.bysoftware.ct.model.impl;

import java.util.Date;
import java.util.List;

import it.bysoftware.ct.model.MovimentoVuoto;
import it.bysoftware.ct.model.Rubrica;
import it.bysoftware.ct.service.MovimentoVuotoLocalServiceUtil;
import it.bysoftware.ct.service.RubricaLocalServiceUtil;
import it.bysoftware.ct.utils.MovementComparator;

/**
 * The extended model implementation for the AnagraficheClientiFornitori
 * service. Represents a row in the &quot;AnagraficheClientiFornitori&quot;
 * database table, with each column mapped to a property of this class.
 *
 * <p>Helper methods and all application logic should be put in this class.
 * Whenever methods are added, rerun ServiceBuilder to copy their definitions
 * into the {@link it.bysoftware.ct.model.AnagraficheClientiFornitori}
 * interface.
 * </p>
 *
 * @author Mario Torrisi
 */
public class AnagraficheClientiFornitoriImpl extends
        AnagraficheClientiFornitoriBaseImpl {
    /**
     * Auto-generated serial ID.
     */
    private static final long serialVersionUID = -7932290964668286442L;

    public AnagraficheClientiFornitoriImpl() {
    }
    
    public String getPreferredEmail(String code) {
        List<Rubrica> entries = RubricaLocalServiceUtil.findContacts(code);
        
        if (entries.size() > 0) {
            return entries.get(0).getEMailTo();
        } else {
            return "";
        }
    }
    
    public List<MovimentoVuoto> getMovimentiVuoti() {
        return MovimentoVuotoLocalServiceUtil.findMovementiByCodiceSoggetto(
                super.getCodiceAnagrafica());
    }
    
    public List<MovimentoVuoto> getMovimentiVuoti(Date from, Date to,
            String code, int start, int end) {
        return MovimentoVuotoLocalServiceUtil.findMovementiByCodiceSoggetto(
                super.getCodiceAnagrafica(), from, to, code, start, end, new MovementComparator());
    }
}
