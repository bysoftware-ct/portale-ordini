package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.SpettanzeSoci;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing SpettanzeSoci in entity cache.
 *
 * @author Mario Torrisi
 * @see SpettanzeSoci
 * @generated
 */
public class SpettanzeSociCacheModel implements CacheModel<SpettanzeSoci>,
    Externalizable {
    public long id;
    public int anno;
    public String codiceAttivita;
    public String codiceCentro;
    public int numeroProtocollo;
    public String codiceFornitore;
    public double importo;
    public long dataCalcolo;
    public int stato;
    public int idPagamento;
    public int annoPagamento;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(23);

        sb.append("{id=");
        sb.append(id);
        sb.append(", anno=");
        sb.append(anno);
        sb.append(", codiceAttivita=");
        sb.append(codiceAttivita);
        sb.append(", codiceCentro=");
        sb.append(codiceCentro);
        sb.append(", numeroProtocollo=");
        sb.append(numeroProtocollo);
        sb.append(", codiceFornitore=");
        sb.append(codiceFornitore);
        sb.append(", importo=");
        sb.append(importo);
        sb.append(", dataCalcolo=");
        sb.append(dataCalcolo);
        sb.append(", stato=");
        sb.append(stato);
        sb.append(", idPagamento=");
        sb.append(idPagamento);
        sb.append(", annoPagamento=");
        sb.append(annoPagamento);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public SpettanzeSoci toEntityModel() {
        SpettanzeSociImpl spettanzeSociImpl = new SpettanzeSociImpl();

        spettanzeSociImpl.setId(id);
        spettanzeSociImpl.setAnno(anno);

        if (codiceAttivita == null) {
            spettanzeSociImpl.setCodiceAttivita(StringPool.BLANK);
        } else {
            spettanzeSociImpl.setCodiceAttivita(codiceAttivita);
        }

        if (codiceCentro == null) {
            spettanzeSociImpl.setCodiceCentro(StringPool.BLANK);
        } else {
            spettanzeSociImpl.setCodiceCentro(codiceCentro);
        }

        spettanzeSociImpl.setNumeroProtocollo(numeroProtocollo);

        if (codiceFornitore == null) {
            spettanzeSociImpl.setCodiceFornitore(StringPool.BLANK);
        } else {
            spettanzeSociImpl.setCodiceFornitore(codiceFornitore);
        }

        spettanzeSociImpl.setImporto(importo);

        if (dataCalcolo == Long.MIN_VALUE) {
            spettanzeSociImpl.setDataCalcolo(null);
        } else {
            spettanzeSociImpl.setDataCalcolo(new Date(dataCalcolo));
        }

        spettanzeSociImpl.setStato(stato);
        spettanzeSociImpl.setIdPagamento(idPagamento);
        spettanzeSociImpl.setAnnoPagamento(annoPagamento);

        spettanzeSociImpl.resetOriginalValues();

        return spettanzeSociImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        anno = objectInput.readInt();
        codiceAttivita = objectInput.readUTF();
        codiceCentro = objectInput.readUTF();
        numeroProtocollo = objectInput.readInt();
        codiceFornitore = objectInput.readUTF();
        importo = objectInput.readDouble();
        dataCalcolo = objectInput.readLong();
        stato = objectInput.readInt();
        idPagamento = objectInput.readInt();
        annoPagamento = objectInput.readInt();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeInt(anno);

        if (codiceAttivita == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceAttivita);
        }

        if (codiceCentro == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceCentro);
        }

        objectOutput.writeInt(numeroProtocollo);

        if (codiceFornitore == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceFornitore);
        }

        objectOutput.writeDouble(importo);
        objectOutput.writeLong(dataCalcolo);
        objectOutput.writeInt(stato);
        objectOutput.writeInt(idPagamento);
        objectOutput.writeInt(annoPagamento);
    }
}
