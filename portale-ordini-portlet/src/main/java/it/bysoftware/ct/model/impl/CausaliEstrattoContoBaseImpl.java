package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.CausaliEstrattoConto;
import it.bysoftware.ct.service.CausaliEstrattoContoLocalServiceUtil;

/**
 * The extended model base implementation for the CausaliEstrattoConto service. Represents a row in the &quot;CausaliEstrattoConto&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This class exists only as a container for the default extended model level methods generated by ServiceBuilder. Helper methods and all application logic should be put in {@link CausaliEstrattoContoImpl}.
 * </p>
 *
 * @author Mario Torrisi
 * @see CausaliEstrattoContoImpl
 * @see it.bysoftware.ct.model.CausaliEstrattoConto
 * @generated
 */
public abstract class CausaliEstrattoContoBaseImpl
    extends CausaliEstrattoContoModelImpl implements CausaliEstrattoConto {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. All methods that expect a causali estratto conto model instance should use the {@link CausaliEstrattoConto} interface instead.
     */
    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            CausaliEstrattoContoLocalServiceUtil.addCausaliEstrattoConto(this);
        } else {
            CausaliEstrattoContoLocalServiceUtil.updateCausaliEstrattoConto(this);
        }
    }
}
