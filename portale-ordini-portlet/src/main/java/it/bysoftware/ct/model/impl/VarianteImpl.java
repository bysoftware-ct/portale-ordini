package it.bysoftware.ct.model.impl;

/**
 * The extended model implementation for the Variante service. Represents a row in the &quot;varianti&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.bysoftware.ct.model.Variante} interface.
 * </p>
 *
 * @author Mario Torrisi
 */
public class VarianteImpl extends VarianteBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a variante model instance should use the {@link it.bysoftware.ct.model.Variante} interface instead.
     */
    public VarianteImpl() {
    }
}
