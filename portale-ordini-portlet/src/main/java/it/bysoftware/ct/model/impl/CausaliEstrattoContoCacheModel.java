package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.CausaliEstrattoConto;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing CausaliEstrattoConto in entity cache.
 *
 * @author Mario Torrisi
 * @see CausaliEstrattoConto
 * @generated
 */
public class CausaliEstrattoContoCacheModel implements CacheModel<CausaliEstrattoConto>,
    Externalizable {
    public String codiceCausaleEC;
    public String descrizioneCausaleEC;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(5);

        sb.append("{codiceCausaleEC=");
        sb.append(codiceCausaleEC);
        sb.append(", descrizioneCausaleEC=");
        sb.append(descrizioneCausaleEC);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public CausaliEstrattoConto toEntityModel() {
        CausaliEstrattoContoImpl causaliEstrattoContoImpl = new CausaliEstrattoContoImpl();

        if (codiceCausaleEC == null) {
            causaliEstrattoContoImpl.setCodiceCausaleEC(StringPool.BLANK);
        } else {
            causaliEstrattoContoImpl.setCodiceCausaleEC(codiceCausaleEC);
        }

        if (descrizioneCausaleEC == null) {
            causaliEstrattoContoImpl.setDescrizioneCausaleEC(StringPool.BLANK);
        } else {
            causaliEstrattoContoImpl.setDescrizioneCausaleEC(descrizioneCausaleEC);
        }

        causaliEstrattoContoImpl.resetOriginalValues();

        return causaliEstrattoContoImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        codiceCausaleEC = objectInput.readUTF();
        descrizioneCausaleEC = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        if (codiceCausaleEC == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceCausaleEC);
        }

        if (descrizioneCausaleEC == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(descrizioneCausaleEC);
        }
    }
}
