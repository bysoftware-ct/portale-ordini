package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.VariantiPianta;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing VariantiPianta in entity cache.
 *
 * @author Mario Torrisi
 * @see VariantiPianta
 * @generated
 */
public class VariantiPiantaCacheModel implements CacheModel<VariantiPianta>,
    Externalizable {
    public long id;
    public long idVariante;
    public long idPianta;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{id=");
        sb.append(id);
        sb.append(", idVariante=");
        sb.append(idVariante);
        sb.append(", idPianta=");
        sb.append(idPianta);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public VariantiPianta toEntityModel() {
        VariantiPiantaImpl variantiPiantaImpl = new VariantiPiantaImpl();

        variantiPiantaImpl.setId(id);
        variantiPiantaImpl.setIdVariante(idVariante);
        variantiPiantaImpl.setIdPianta(idPianta);

        variantiPiantaImpl.resetOriginalValues();

        return variantiPiantaImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        idVariante = objectInput.readLong();
        idPianta = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(idVariante);
        objectOutput.writeLong(idPianta);
    }
}
