package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.Categorie;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Categorie in entity cache.
 *
 * @author Mario Torrisi
 * @see Categorie
 * @generated
 */
public class CategorieCacheModel implements CacheModel<Categorie>,
    Externalizable {
    public long id;
    public String nomeIta;
    public String nomeEng;
    public String nomeTed;
    public String nomeFra;
    public String nomeSpa;
    public boolean predefinita;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(15);

        sb.append("{id=");
        sb.append(id);
        sb.append(", nomeIta=");
        sb.append(nomeIta);
        sb.append(", nomeEng=");
        sb.append(nomeEng);
        sb.append(", nomeTed=");
        sb.append(nomeTed);
        sb.append(", nomeFra=");
        sb.append(nomeFra);
        sb.append(", nomeSpa=");
        sb.append(nomeSpa);
        sb.append(", predefinita=");
        sb.append(predefinita);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Categorie toEntityModel() {
        CategorieImpl categorieImpl = new CategorieImpl();

        categorieImpl.setId(id);

        if (nomeIta == null) {
            categorieImpl.setNomeIta(StringPool.BLANK);
        } else {
            categorieImpl.setNomeIta(nomeIta);
        }

        if (nomeEng == null) {
            categorieImpl.setNomeEng(StringPool.BLANK);
        } else {
            categorieImpl.setNomeEng(nomeEng);
        }

        if (nomeTed == null) {
            categorieImpl.setNomeTed(StringPool.BLANK);
        } else {
            categorieImpl.setNomeTed(nomeTed);
        }

        if (nomeFra == null) {
            categorieImpl.setNomeFra(StringPool.BLANK);
        } else {
            categorieImpl.setNomeFra(nomeFra);
        }

        if (nomeSpa == null) {
            categorieImpl.setNomeSpa(StringPool.BLANK);
        } else {
            categorieImpl.setNomeSpa(nomeSpa);
        }

        categorieImpl.setPredefinita(predefinita);

        categorieImpl.resetOriginalValues();

        return categorieImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        nomeIta = objectInput.readUTF();
        nomeEng = objectInput.readUTF();
        nomeTed = objectInput.readUTF();
        nomeFra = objectInput.readUTF();
        nomeSpa = objectInput.readUTF();
        predefinita = objectInput.readBoolean();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);

        if (nomeIta == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(nomeIta);
        }

        if (nomeEng == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(nomeEng);
        }

        if (nomeTed == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(nomeTed);
        }

        if (nomeFra == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(nomeFra);
        }

        if (nomeSpa == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(nomeSpa);
        }

        objectOutput.writeBoolean(predefinita);
    }
}
