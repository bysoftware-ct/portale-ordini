package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.TipoCarrello;
import it.bysoftware.ct.service.TipoCarrelloLocalServiceUtil;

/**
 * The extended model base implementation for the TipoCarrello service. Represents a row in the &quot;_tipi_carrello&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This class exists only as a container for the default extended model level methods generated by ServiceBuilder. Helper methods and all application logic should be put in {@link TipoCarrelloImpl}.
 * </p>
 *
 * @author Mario Torrisi
 * @see TipoCarrelloImpl
 * @see it.bysoftware.ct.model.TipoCarrello
 * @generated
 */
public abstract class TipoCarrelloBaseImpl extends TipoCarrelloModelImpl
    implements TipoCarrello {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. All methods that expect a tipo carrello model instance should use the {@link TipoCarrello} interface instead.
     */
    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            TipoCarrelloLocalServiceUtil.addTipoCarrello(this);
        } else {
            TipoCarrelloLocalServiceUtil.updateTipoCarrello(this);
        }
    }
}
