package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.RigoDocumento;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing RigoDocumento in entity cache.
 *
 * @author Mario Torrisi
 * @see RigoDocumento
 * @generated
 */
public class RigoDocumentoCacheModel implements CacheModel<RigoDocumento>,
    Externalizable {
    public int anno;
    public String codiceAttivita;
    public String codiceCentro;
    public String codiceDeposito;
    public int protocollo;
    public String codiceFornitore;
    public int rigo;
    public String tipoDocumento;
    public int tipoRigo;
    public String codiceArticolo;
    public String codiceVariante;
    public String descrizione;
    public double quantita;
    public double quantitaSecondaria;
    public double prezzo;
    public double sconto1;
    public double sconto2;
    public double sconto3;
    public String libStr1;
    public String libStr2;
    public String libStr3;
    public double libDbl1;
    public double libDbl2;
    public double libDbl3;
    public long libLng1;
    public long libLng2;
    public long libLng3;
    public long libDat1;
    public long libDat2;
    public long libDat3;
    public double importoNetto;
    public String codiceIVA;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(65);

        sb.append("{anno=");
        sb.append(anno);
        sb.append(", codiceAttivita=");
        sb.append(codiceAttivita);
        sb.append(", codiceCentro=");
        sb.append(codiceCentro);
        sb.append(", codiceDeposito=");
        sb.append(codiceDeposito);
        sb.append(", protocollo=");
        sb.append(protocollo);
        sb.append(", codiceFornitore=");
        sb.append(codiceFornitore);
        sb.append(", rigo=");
        sb.append(rigo);
        sb.append(", tipoDocumento=");
        sb.append(tipoDocumento);
        sb.append(", tipoRigo=");
        sb.append(tipoRigo);
        sb.append(", codiceArticolo=");
        sb.append(codiceArticolo);
        sb.append(", codiceVariante=");
        sb.append(codiceVariante);
        sb.append(", descrizione=");
        sb.append(descrizione);
        sb.append(", quantita=");
        sb.append(quantita);
        sb.append(", quantitaSecondaria=");
        sb.append(quantitaSecondaria);
        sb.append(", prezzo=");
        sb.append(prezzo);
        sb.append(", sconto1=");
        sb.append(sconto1);
        sb.append(", sconto2=");
        sb.append(sconto2);
        sb.append(", sconto3=");
        sb.append(sconto3);
        sb.append(", libStr1=");
        sb.append(libStr1);
        sb.append(", libStr2=");
        sb.append(libStr2);
        sb.append(", libStr3=");
        sb.append(libStr3);
        sb.append(", libDbl1=");
        sb.append(libDbl1);
        sb.append(", libDbl2=");
        sb.append(libDbl2);
        sb.append(", libDbl3=");
        sb.append(libDbl3);
        sb.append(", libLng1=");
        sb.append(libLng1);
        sb.append(", libLng2=");
        sb.append(libLng2);
        sb.append(", libLng3=");
        sb.append(libLng3);
        sb.append(", libDat1=");
        sb.append(libDat1);
        sb.append(", libDat2=");
        sb.append(libDat2);
        sb.append(", libDat3=");
        sb.append(libDat3);
        sb.append(", importoNetto=");
        sb.append(importoNetto);
        sb.append(", codiceIVA=");
        sb.append(codiceIVA);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public RigoDocumento toEntityModel() {
        RigoDocumentoImpl rigoDocumentoImpl = new RigoDocumentoImpl();

        rigoDocumentoImpl.setAnno(anno);

        if (codiceAttivita == null) {
            rigoDocumentoImpl.setCodiceAttivita(StringPool.BLANK);
        } else {
            rigoDocumentoImpl.setCodiceAttivita(codiceAttivita);
        }

        if (codiceCentro == null) {
            rigoDocumentoImpl.setCodiceCentro(StringPool.BLANK);
        } else {
            rigoDocumentoImpl.setCodiceCentro(codiceCentro);
        }

        if (codiceDeposito == null) {
            rigoDocumentoImpl.setCodiceDeposito(StringPool.BLANK);
        } else {
            rigoDocumentoImpl.setCodiceDeposito(codiceDeposito);
        }

        rigoDocumentoImpl.setProtocollo(protocollo);

        if (codiceFornitore == null) {
            rigoDocumentoImpl.setCodiceFornitore(StringPool.BLANK);
        } else {
            rigoDocumentoImpl.setCodiceFornitore(codiceFornitore);
        }

        rigoDocumentoImpl.setRigo(rigo);

        if (tipoDocumento == null) {
            rigoDocumentoImpl.setTipoDocumento(StringPool.BLANK);
        } else {
            rigoDocumentoImpl.setTipoDocumento(tipoDocumento);
        }

        rigoDocumentoImpl.setTipoRigo(tipoRigo);

        if (codiceArticolo == null) {
            rigoDocumentoImpl.setCodiceArticolo(StringPool.BLANK);
        } else {
            rigoDocumentoImpl.setCodiceArticolo(codiceArticolo);
        }

        if (codiceVariante == null) {
            rigoDocumentoImpl.setCodiceVariante(StringPool.BLANK);
        } else {
            rigoDocumentoImpl.setCodiceVariante(codiceVariante);
        }

        if (descrizione == null) {
            rigoDocumentoImpl.setDescrizione(StringPool.BLANK);
        } else {
            rigoDocumentoImpl.setDescrizione(descrizione);
        }

        rigoDocumentoImpl.setQuantita(quantita);
        rigoDocumentoImpl.setQuantitaSecondaria(quantitaSecondaria);
        rigoDocumentoImpl.setPrezzo(prezzo);
        rigoDocumentoImpl.setSconto1(sconto1);
        rigoDocumentoImpl.setSconto2(sconto2);
        rigoDocumentoImpl.setSconto3(sconto3);

        if (libStr1 == null) {
            rigoDocumentoImpl.setLibStr1(StringPool.BLANK);
        } else {
            rigoDocumentoImpl.setLibStr1(libStr1);
        }

        if (libStr2 == null) {
            rigoDocumentoImpl.setLibStr2(StringPool.BLANK);
        } else {
            rigoDocumentoImpl.setLibStr2(libStr2);
        }

        if (libStr3 == null) {
            rigoDocumentoImpl.setLibStr3(StringPool.BLANK);
        } else {
            rigoDocumentoImpl.setLibStr3(libStr3);
        }

        rigoDocumentoImpl.setLibDbl1(libDbl1);
        rigoDocumentoImpl.setLibDbl2(libDbl2);
        rigoDocumentoImpl.setLibDbl3(libDbl3);
        rigoDocumentoImpl.setLibLng1(libLng1);
        rigoDocumentoImpl.setLibLng2(libLng2);
        rigoDocumentoImpl.setLibLng3(libLng3);

        if (libDat1 == Long.MIN_VALUE) {
            rigoDocumentoImpl.setLibDat1(null);
        } else {
            rigoDocumentoImpl.setLibDat1(new Date(libDat1));
        }

        if (libDat2 == Long.MIN_VALUE) {
            rigoDocumentoImpl.setLibDat2(null);
        } else {
            rigoDocumentoImpl.setLibDat2(new Date(libDat2));
        }

        if (libDat3 == Long.MIN_VALUE) {
            rigoDocumentoImpl.setLibDat3(null);
        } else {
            rigoDocumentoImpl.setLibDat3(new Date(libDat3));
        }

        rigoDocumentoImpl.setImportoNetto(importoNetto);

        if (codiceIVA == null) {
            rigoDocumentoImpl.setCodiceIVA(StringPool.BLANK);
        } else {
            rigoDocumentoImpl.setCodiceIVA(codiceIVA);
        }

        rigoDocumentoImpl.resetOriginalValues();

        return rigoDocumentoImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        anno = objectInput.readInt();
        codiceAttivita = objectInput.readUTF();
        codiceCentro = objectInput.readUTF();
        codiceDeposito = objectInput.readUTF();
        protocollo = objectInput.readInt();
        codiceFornitore = objectInput.readUTF();
        rigo = objectInput.readInt();
        tipoDocumento = objectInput.readUTF();
        tipoRigo = objectInput.readInt();
        codiceArticolo = objectInput.readUTF();
        codiceVariante = objectInput.readUTF();
        descrizione = objectInput.readUTF();
        quantita = objectInput.readDouble();
        quantitaSecondaria = objectInput.readDouble();
        prezzo = objectInput.readDouble();
        sconto1 = objectInput.readDouble();
        sconto2 = objectInput.readDouble();
        sconto3 = objectInput.readDouble();
        libStr1 = objectInput.readUTF();
        libStr2 = objectInput.readUTF();
        libStr3 = objectInput.readUTF();
        libDbl1 = objectInput.readDouble();
        libDbl2 = objectInput.readDouble();
        libDbl3 = objectInput.readDouble();
        libLng1 = objectInput.readLong();
        libLng2 = objectInput.readLong();
        libLng3 = objectInput.readLong();
        libDat1 = objectInput.readLong();
        libDat2 = objectInput.readLong();
        libDat3 = objectInput.readLong();
        importoNetto = objectInput.readDouble();
        codiceIVA = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeInt(anno);

        if (codiceAttivita == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceAttivita);
        }

        if (codiceCentro == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceCentro);
        }

        if (codiceDeposito == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceDeposito);
        }

        objectOutput.writeInt(protocollo);

        if (codiceFornitore == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceFornitore);
        }

        objectOutput.writeInt(rigo);

        if (tipoDocumento == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(tipoDocumento);
        }

        objectOutput.writeInt(tipoRigo);

        if (codiceArticolo == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceArticolo);
        }

        if (codiceVariante == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceVariante);
        }

        if (descrizione == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(descrizione);
        }

        objectOutput.writeDouble(quantita);
        objectOutput.writeDouble(quantitaSecondaria);
        objectOutput.writeDouble(prezzo);
        objectOutput.writeDouble(sconto1);
        objectOutput.writeDouble(sconto2);
        objectOutput.writeDouble(sconto3);

        if (libStr1 == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(libStr1);
        }

        if (libStr2 == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(libStr2);
        }

        if (libStr3 == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(libStr3);
        }

        objectOutput.writeDouble(libDbl1);
        objectOutput.writeDouble(libDbl2);
        objectOutput.writeDouble(libDbl3);
        objectOutput.writeLong(libLng1);
        objectOutput.writeLong(libLng2);
        objectOutput.writeLong(libLng3);
        objectOutput.writeLong(libDat1);
        objectOutput.writeLong(libDat2);
        objectOutput.writeLong(libDat3);
        objectOutput.writeDouble(importoNetto);

        if (codiceIVA == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceIVA);
        }
    }
}
