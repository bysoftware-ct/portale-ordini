package it.bysoftware.ct.model.impl;

/**
 * The extended model implementation for the CausaliContabili service. Represents a row in the &quot;CausaliContabili&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.bysoftware.ct.model.CausaliContabili} interface.
 * </p>
 *
 * @author Mario Torrisi
 */
public class CausaliContabiliImpl extends CausaliContabiliBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a causali contabili model instance should use the {@link it.bysoftware.ct.model.CausaliContabili} interface instead.
     */
    public CausaliContabiliImpl() {
    }
}
