package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.ContatoreSocio;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing ContatoreSocio in entity cache.
 *
 * @author Mario Torrisi
 * @see ContatoreSocio
 * @generated
 */
public class ContatoreSocioCacheModel implements CacheModel<ContatoreSocio>,
    Externalizable {
    public int anno;
    public String codiceSocio;
    public String tipoDocumento;
    public int numero;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(9);

        sb.append("{anno=");
        sb.append(anno);
        sb.append(", codiceSocio=");
        sb.append(codiceSocio);
        sb.append(", tipoDocumento=");
        sb.append(tipoDocumento);
        sb.append(", numero=");
        sb.append(numero);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public ContatoreSocio toEntityModel() {
        ContatoreSocioImpl contatoreSocioImpl = new ContatoreSocioImpl();

        contatoreSocioImpl.setAnno(anno);

        if (codiceSocio == null) {
            contatoreSocioImpl.setCodiceSocio(StringPool.BLANK);
        } else {
            contatoreSocioImpl.setCodiceSocio(codiceSocio);
        }

        if (tipoDocumento == null) {
            contatoreSocioImpl.setTipoDocumento(StringPool.BLANK);
        } else {
            contatoreSocioImpl.setTipoDocumento(tipoDocumento);
        }

        contatoreSocioImpl.setNumero(numero);

        contatoreSocioImpl.resetOriginalValues();

        return contatoreSocioImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        anno = objectInput.readInt();
        codiceSocio = objectInput.readUTF();
        tipoDocumento = objectInput.readUTF();
        numero = objectInput.readInt();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeInt(anno);

        if (codiceSocio == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceSocio);
        }

        if (tipoDocumento == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(tipoDocumento);
        }

        objectOutput.writeInt(numero);
    }
}
