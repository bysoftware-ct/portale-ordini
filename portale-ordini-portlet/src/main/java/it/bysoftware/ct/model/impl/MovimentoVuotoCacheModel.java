package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.MovimentoVuoto;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing MovimentoVuoto in entity cache.
 *
 * @author Mario Torrisi
 * @see MovimentoVuoto
 * @generated
 */
public class MovimentoVuotoCacheModel implements CacheModel<MovimentoVuoto>,
    Externalizable {
    public long idMovimento;
    public String codiceVuoto;
    public long dataMovimento;
    public String codiceSoggetto;
    public double quantita;
    public int tipoMovimento;
    public String note;
    public int uuid;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(17);

        sb.append("{idMovimento=");
        sb.append(idMovimento);
        sb.append(", codiceVuoto=");
        sb.append(codiceVuoto);
        sb.append(", dataMovimento=");
        sb.append(dataMovimento);
        sb.append(", codiceSoggetto=");
        sb.append(codiceSoggetto);
        sb.append(", quantita=");
        sb.append(quantita);
        sb.append(", tipoMovimento=");
        sb.append(tipoMovimento);
        sb.append(", note=");
        sb.append(note);
        sb.append(", uuid=");
        sb.append(uuid);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public MovimentoVuoto toEntityModel() {
        MovimentoVuotoImpl movimentoVuotoImpl = new MovimentoVuotoImpl();

        movimentoVuotoImpl.setIdMovimento(idMovimento);

        if (codiceVuoto == null) {
            movimentoVuotoImpl.setCodiceVuoto(StringPool.BLANK);
        } else {
            movimentoVuotoImpl.setCodiceVuoto(codiceVuoto);
        }

        if (dataMovimento == Long.MIN_VALUE) {
            movimentoVuotoImpl.setDataMovimento(null);
        } else {
            movimentoVuotoImpl.setDataMovimento(new Date(dataMovimento));
        }

        if (codiceSoggetto == null) {
            movimentoVuotoImpl.setCodiceSoggetto(StringPool.BLANK);
        } else {
            movimentoVuotoImpl.setCodiceSoggetto(codiceSoggetto);
        }

        movimentoVuotoImpl.setQuantita(quantita);
        movimentoVuotoImpl.setTipoMovimento(tipoMovimento);

        if (note == null) {
            movimentoVuotoImpl.setNote(StringPool.BLANK);
        } else {
            movimentoVuotoImpl.setNote(note);
        }

        movimentoVuotoImpl.setUuid(uuid);

        movimentoVuotoImpl.resetOriginalValues();

        return movimentoVuotoImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        idMovimento = objectInput.readLong();
        codiceVuoto = objectInput.readUTF();
        dataMovimento = objectInput.readLong();
        codiceSoggetto = objectInput.readUTF();
        quantita = objectInput.readDouble();
        tipoMovimento = objectInput.readInt();
        note = objectInput.readUTF();
        uuid = objectInput.readInt();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(idMovimento);

        if (codiceVuoto == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceVuoto);
        }

        objectOutput.writeLong(dataMovimento);

        if (codiceSoggetto == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceSoggetto);
        }

        objectOutput.writeDouble(quantita);
        objectOutput.writeInt(tipoMovimento);

        if (note == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(note);
        }

        objectOutput.writeInt(uuid);
    }
}
