package it.bysoftware.ct.model.impl;

/**
 * The extended model implementation for the VariantiPianta service. Represents a row in the &quot;varianti_pianta&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.bysoftware.ct.model.VariantiPianta} interface.
 * </p>
 *
 * @author Mario Torrisi
 */
public class VariantiPiantaImpl extends VariantiPiantaBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a varianti pianta model instance should use the {@link it.bysoftware.ct.model.VariantiPianta} interface instead.
     */
    public VariantiPiantaImpl() {
    }
}
