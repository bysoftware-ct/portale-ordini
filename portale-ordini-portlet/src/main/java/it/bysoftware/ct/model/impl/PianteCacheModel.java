package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.Piante;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Piante in entity cache.
 *
 * @author Mario Torrisi
 * @see Piante
 * @generated
 */
public class PianteCacheModel implements CacheModel<Piante>, Externalizable {
    public long id;
    public String nome;
    public String vaso;
    public double prezzo;
    public int altezza;
    public int piantePianale;
    public int pianteCarrello;
    public boolean attiva;
    public String foto1;
    public String foto2;
    public String foto3;
    public int altezzaPianta;
    public long idCategoria;
    public String idFornitore;
    public double prezzoFornitore;
    public int disponibilita;
    public String codice;
    public String forma;
    public int altezzaChioma;
    public String tempoConsegna;
    public double prezzoB;
    public double percScontoMistoAziendale;
    public double prezzoACarrello;
    public double prezzoBCarrello;
    public long idForma;
    public double costoRipiano;
    public long ultimoAggiornamentoFoto;
    public long scadenzaFoto;
    public int giorniScadenzaFoto;
    public int sormonto2Fila;
    public int sormonto3Fila;
    public int sormonto4Fila;
    public int aggiuntaRipiano;
    public String ean;
    public boolean passaporto;
    public boolean bloccoDisponibilita;
    public String pathFile;
    public boolean obsoleto;
    public double prezzoEtichetta;
    public String passaportoDefault;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(81);

        sb.append("{id=");
        sb.append(id);
        sb.append(", nome=");
        sb.append(nome);
        sb.append(", vaso=");
        sb.append(vaso);
        sb.append(", prezzo=");
        sb.append(prezzo);
        sb.append(", altezza=");
        sb.append(altezza);
        sb.append(", piantePianale=");
        sb.append(piantePianale);
        sb.append(", pianteCarrello=");
        sb.append(pianteCarrello);
        sb.append(", attiva=");
        sb.append(attiva);
        sb.append(", foto1=");
        sb.append(foto1);
        sb.append(", foto2=");
        sb.append(foto2);
        sb.append(", foto3=");
        sb.append(foto3);
        sb.append(", altezzaPianta=");
        sb.append(altezzaPianta);
        sb.append(", idCategoria=");
        sb.append(idCategoria);
        sb.append(", idFornitore=");
        sb.append(idFornitore);
        sb.append(", prezzoFornitore=");
        sb.append(prezzoFornitore);
        sb.append(", disponibilita=");
        sb.append(disponibilita);
        sb.append(", codice=");
        sb.append(codice);
        sb.append(", forma=");
        sb.append(forma);
        sb.append(", altezzaChioma=");
        sb.append(altezzaChioma);
        sb.append(", tempoConsegna=");
        sb.append(tempoConsegna);
        sb.append(", prezzoB=");
        sb.append(prezzoB);
        sb.append(", percScontoMistoAziendale=");
        sb.append(percScontoMistoAziendale);
        sb.append(", prezzoACarrello=");
        sb.append(prezzoACarrello);
        sb.append(", prezzoBCarrello=");
        sb.append(prezzoBCarrello);
        sb.append(", idForma=");
        sb.append(idForma);
        sb.append(", costoRipiano=");
        sb.append(costoRipiano);
        sb.append(", ultimoAggiornamentoFoto=");
        sb.append(ultimoAggiornamentoFoto);
        sb.append(", scadenzaFoto=");
        sb.append(scadenzaFoto);
        sb.append(", giorniScadenzaFoto=");
        sb.append(giorniScadenzaFoto);
        sb.append(", sormonto2Fila=");
        sb.append(sormonto2Fila);
        sb.append(", sormonto3Fila=");
        sb.append(sormonto3Fila);
        sb.append(", sormonto4Fila=");
        sb.append(sormonto4Fila);
        sb.append(", aggiuntaRipiano=");
        sb.append(aggiuntaRipiano);
        sb.append(", ean=");
        sb.append(ean);
        sb.append(", passaporto=");
        sb.append(passaporto);
        sb.append(", bloccoDisponibilita=");
        sb.append(bloccoDisponibilita);
        sb.append(", pathFile=");
        sb.append(pathFile);
        sb.append(", obsoleto=");
        sb.append(obsoleto);
        sb.append(", prezzoEtichetta=");
        sb.append(prezzoEtichetta);
        sb.append(", passaportoDefault=");
        sb.append(passaportoDefault);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Piante toEntityModel() {
        PianteImpl pianteImpl = new PianteImpl();

        pianteImpl.setId(id);

        if (nome == null) {
            pianteImpl.setNome(StringPool.BLANK);
        } else {
            pianteImpl.setNome(nome);
        }

        if (vaso == null) {
            pianteImpl.setVaso(StringPool.BLANK);
        } else {
            pianteImpl.setVaso(vaso);
        }

        pianteImpl.setPrezzo(prezzo);
        pianteImpl.setAltezza(altezza);
        pianteImpl.setPiantePianale(piantePianale);
        pianteImpl.setPianteCarrello(pianteCarrello);
        pianteImpl.setAttiva(attiva);

        if (foto1 == null) {
            pianteImpl.setFoto1(StringPool.BLANK);
        } else {
            pianteImpl.setFoto1(foto1);
        }

        if (foto2 == null) {
            pianteImpl.setFoto2(StringPool.BLANK);
        } else {
            pianteImpl.setFoto2(foto2);
        }

        if (foto3 == null) {
            pianteImpl.setFoto3(StringPool.BLANK);
        } else {
            pianteImpl.setFoto3(foto3);
        }

        pianteImpl.setAltezzaPianta(altezzaPianta);
        pianteImpl.setIdCategoria(idCategoria);

        if (idFornitore == null) {
            pianteImpl.setIdFornitore(StringPool.BLANK);
        } else {
            pianteImpl.setIdFornitore(idFornitore);
        }

        pianteImpl.setPrezzoFornitore(prezzoFornitore);
        pianteImpl.setDisponibilita(disponibilita);

        if (codice == null) {
            pianteImpl.setCodice(StringPool.BLANK);
        } else {
            pianteImpl.setCodice(codice);
        }

        if (forma == null) {
            pianteImpl.setForma(StringPool.BLANK);
        } else {
            pianteImpl.setForma(forma);
        }

        pianteImpl.setAltezzaChioma(altezzaChioma);

        if (tempoConsegna == null) {
            pianteImpl.setTempoConsegna(StringPool.BLANK);
        } else {
            pianteImpl.setTempoConsegna(tempoConsegna);
        }

        pianteImpl.setPrezzoB(prezzoB);
        pianteImpl.setPercScontoMistoAziendale(percScontoMistoAziendale);
        pianteImpl.setPrezzoACarrello(prezzoACarrello);
        pianteImpl.setPrezzoBCarrello(prezzoBCarrello);
        pianteImpl.setIdForma(idForma);
        pianteImpl.setCostoRipiano(costoRipiano);

        if (ultimoAggiornamentoFoto == Long.MIN_VALUE) {
            pianteImpl.setUltimoAggiornamentoFoto(null);
        } else {
            pianteImpl.setUltimoAggiornamentoFoto(new Date(
                    ultimoAggiornamentoFoto));
        }

        if (scadenzaFoto == Long.MIN_VALUE) {
            pianteImpl.setScadenzaFoto(null);
        } else {
            pianteImpl.setScadenzaFoto(new Date(scadenzaFoto));
        }

        pianteImpl.setGiorniScadenzaFoto(giorniScadenzaFoto);
        pianteImpl.setSormonto2Fila(sormonto2Fila);
        pianteImpl.setSormonto3Fila(sormonto3Fila);
        pianteImpl.setSormonto4Fila(sormonto4Fila);
        pianteImpl.setAggiuntaRipiano(aggiuntaRipiano);

        if (ean == null) {
            pianteImpl.setEan(StringPool.BLANK);
        } else {
            pianteImpl.setEan(ean);
        }

        pianteImpl.setPassaporto(passaporto);
        pianteImpl.setBloccoDisponibilita(bloccoDisponibilita);

        if (pathFile == null) {
            pianteImpl.setPathFile(StringPool.BLANK);
        } else {
            pianteImpl.setPathFile(pathFile);
        }

        pianteImpl.setObsoleto(obsoleto);
        pianteImpl.setPrezzoEtichetta(prezzoEtichetta);

        if (passaportoDefault == null) {
            pianteImpl.setPassaportoDefault(StringPool.BLANK);
        } else {
            pianteImpl.setPassaportoDefault(passaportoDefault);
        }

        pianteImpl.resetOriginalValues();

        return pianteImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        nome = objectInput.readUTF();
        vaso = objectInput.readUTF();
        prezzo = objectInput.readDouble();
        altezza = objectInput.readInt();
        piantePianale = objectInput.readInt();
        pianteCarrello = objectInput.readInt();
        attiva = objectInput.readBoolean();
        foto1 = objectInput.readUTF();
        foto2 = objectInput.readUTF();
        foto3 = objectInput.readUTF();
        altezzaPianta = objectInput.readInt();
        idCategoria = objectInput.readLong();
        idFornitore = objectInput.readUTF();
        prezzoFornitore = objectInput.readDouble();
        disponibilita = objectInput.readInt();
        codice = objectInput.readUTF();
        forma = objectInput.readUTF();
        altezzaChioma = objectInput.readInt();
        tempoConsegna = objectInput.readUTF();
        prezzoB = objectInput.readDouble();
        percScontoMistoAziendale = objectInput.readDouble();
        prezzoACarrello = objectInput.readDouble();
        prezzoBCarrello = objectInput.readDouble();
        idForma = objectInput.readLong();
        costoRipiano = objectInput.readDouble();
        ultimoAggiornamentoFoto = objectInput.readLong();
        scadenzaFoto = objectInput.readLong();
        giorniScadenzaFoto = objectInput.readInt();
        sormonto2Fila = objectInput.readInt();
        sormonto3Fila = objectInput.readInt();
        sormonto4Fila = objectInput.readInt();
        aggiuntaRipiano = objectInput.readInt();
        ean = objectInput.readUTF();
        passaporto = objectInput.readBoolean();
        bloccoDisponibilita = objectInput.readBoolean();
        pathFile = objectInput.readUTF();
        obsoleto = objectInput.readBoolean();
        prezzoEtichetta = objectInput.readDouble();
        passaportoDefault = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);

        if (nome == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(nome);
        }

        if (vaso == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(vaso);
        }

        objectOutput.writeDouble(prezzo);
        objectOutput.writeInt(altezza);
        objectOutput.writeInt(piantePianale);
        objectOutput.writeInt(pianteCarrello);
        objectOutput.writeBoolean(attiva);

        if (foto1 == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(foto1);
        }

        if (foto2 == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(foto2);
        }

        if (foto3 == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(foto3);
        }

        objectOutput.writeInt(altezzaPianta);
        objectOutput.writeLong(idCategoria);

        if (idFornitore == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(idFornitore);
        }

        objectOutput.writeDouble(prezzoFornitore);
        objectOutput.writeInt(disponibilita);

        if (codice == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codice);
        }

        if (forma == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(forma);
        }

        objectOutput.writeInt(altezzaChioma);

        if (tempoConsegna == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(tempoConsegna);
        }

        objectOutput.writeDouble(prezzoB);
        objectOutput.writeDouble(percScontoMistoAziendale);
        objectOutput.writeDouble(prezzoACarrello);
        objectOutput.writeDouble(prezzoBCarrello);
        objectOutput.writeLong(idForma);
        objectOutput.writeDouble(costoRipiano);
        objectOutput.writeLong(ultimoAggiornamentoFoto);
        objectOutput.writeLong(scadenzaFoto);
        objectOutput.writeInt(giorniScadenzaFoto);
        objectOutput.writeInt(sormonto2Fila);
        objectOutput.writeInt(sormonto3Fila);
        objectOutput.writeInt(sormonto4Fila);
        objectOutput.writeInt(aggiuntaRipiano);

        if (ean == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(ean);
        }

        objectOutput.writeBoolean(passaporto);
        objectOutput.writeBoolean(bloccoDisponibilita);

        if (pathFile == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(pathFile);
        }

        objectOutput.writeBoolean(obsoleto);
        objectOutput.writeDouble(prezzoEtichetta);

        if (passaportoDefault == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(passaportoDefault);
        }
    }
}
