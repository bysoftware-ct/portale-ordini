package it.bysoftware.ct.model.impl;

import java.util.List;

import it.bysoftware.ct.model.Vuoto;
import it.bysoftware.ct.service.VuotoLocalServiceUtil;

/**
 * The extended model implementation for the MovimentoVuoto service. Represents
 * a row in the &quot;_movimenti_vuoti&quot; database table, with each column
 * mapped to a property of this class.
 *
 * <p>Helper methods and all application logic should be put in this class.
 * Whenever methods are added, rerun ServiceBuilder to copy their definitions
 * into the {@link it.bysoftware.ct.model.MovimentoVuoto} interface.
 * </p>
 *
 * @author Mario Torrisi
 */
public class MovimentoVuotoImpl extends MovimentoVuotoBaseImpl {

    /**
     * Serial ID.
     */
    private static final long serialVersionUID = -5370233844610292364L;

    public MovimentoVuotoImpl() {
    }

}
