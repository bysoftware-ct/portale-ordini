package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.Vuoto;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Vuoto in entity cache.
 *
 * @author Mario Torrisi
 * @see Vuoto
 * @generated
 */
public class VuotoCacheModel implements CacheModel<Vuoto>, Externalizable {
    public String codiceVuoto;
    public String nome;
    public String descrizione;
    public String unitaMisura;
    public String note;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{codiceVuoto=");
        sb.append(codiceVuoto);
        sb.append(", nome=");
        sb.append(nome);
        sb.append(", descrizione=");
        sb.append(descrizione);
        sb.append(", unitaMisura=");
        sb.append(unitaMisura);
        sb.append(", note=");
        sb.append(note);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Vuoto toEntityModel() {
        VuotoImpl vuotoImpl = new VuotoImpl();

        if (codiceVuoto == null) {
            vuotoImpl.setCodiceVuoto(StringPool.BLANK);
        } else {
            vuotoImpl.setCodiceVuoto(codiceVuoto);
        }

        if (nome == null) {
            vuotoImpl.setNome(StringPool.BLANK);
        } else {
            vuotoImpl.setNome(nome);
        }

        if (descrizione == null) {
            vuotoImpl.setDescrizione(StringPool.BLANK);
        } else {
            vuotoImpl.setDescrizione(descrizione);
        }

        if (unitaMisura == null) {
            vuotoImpl.setUnitaMisura(StringPool.BLANK);
        } else {
            vuotoImpl.setUnitaMisura(unitaMisura);
        }

        if (note == null) {
            vuotoImpl.setNote(StringPool.BLANK);
        } else {
            vuotoImpl.setNote(note);
        }

        vuotoImpl.resetOriginalValues();

        return vuotoImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        codiceVuoto = objectInput.readUTF();
        nome = objectInput.readUTF();
        descrizione = objectInput.readUTF();
        unitaMisura = objectInput.readUTF();
        note = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        if (codiceVuoto == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceVuoto);
        }

        if (nome == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(nome);
        }

        if (descrizione == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(descrizione);
        }

        if (unitaMisura == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(unitaMisura);
        }

        if (note == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(note);
        }
    }
}
