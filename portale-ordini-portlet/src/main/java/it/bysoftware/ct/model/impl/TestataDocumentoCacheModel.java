package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.TestataDocumento;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing TestataDocumento in entity cache.
 *
 * @author Mario Torrisi
 * @see TestataDocumento
 * @generated
 */
public class TestataDocumentoCacheModel implements CacheModel<TestataDocumento>,
    Externalizable {
    public int anno;
    public String codiceAttivita;
    public String codiceCentro;
    public String codiceDeposito;
    public int protocollo;
    public String codiceFornitore;
    public String tipoDocumento;
    public long dataRegistrazione;
    public String codiceSpedizione;
    public String codiceCausaleTrasporto;
    public String codiceCuraTrasporto;
    public String codicePorto;
    public String codiceAspettoEsteriore;
    public String codiceVettore1;
    public String codiceVettore2;
    public String libStr1;
    public String libStr2;
    public String libStr3;
    public double libDbl1;
    public double libDbl2;
    public double libDbl3;
    public long libLng1;
    public long libLng2;
    public long libLng3;
    public long libDat1;
    public long libDat2;
    public long libDat3;
    public boolean stato;
    public boolean inviato;
    public String codiceConsorzio;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(61);

        sb.append("{anno=");
        sb.append(anno);
        sb.append(", codiceAttivita=");
        sb.append(codiceAttivita);
        sb.append(", codiceCentro=");
        sb.append(codiceCentro);
        sb.append(", codiceDeposito=");
        sb.append(codiceDeposito);
        sb.append(", protocollo=");
        sb.append(protocollo);
        sb.append(", codiceFornitore=");
        sb.append(codiceFornitore);
        sb.append(", tipoDocumento=");
        sb.append(tipoDocumento);
        sb.append(", dataRegistrazione=");
        sb.append(dataRegistrazione);
        sb.append(", codiceSpedizione=");
        sb.append(codiceSpedizione);
        sb.append(", codiceCausaleTrasporto=");
        sb.append(codiceCausaleTrasporto);
        sb.append(", codiceCuraTrasporto=");
        sb.append(codiceCuraTrasporto);
        sb.append(", codicePorto=");
        sb.append(codicePorto);
        sb.append(", codiceAspettoEsteriore=");
        sb.append(codiceAspettoEsteriore);
        sb.append(", codiceVettore1=");
        sb.append(codiceVettore1);
        sb.append(", codiceVettore2=");
        sb.append(codiceVettore2);
        sb.append(", libStr1=");
        sb.append(libStr1);
        sb.append(", libStr2=");
        sb.append(libStr2);
        sb.append(", libStr3=");
        sb.append(libStr3);
        sb.append(", libDbl1=");
        sb.append(libDbl1);
        sb.append(", libDbl2=");
        sb.append(libDbl2);
        sb.append(", libDbl3=");
        sb.append(libDbl3);
        sb.append(", libLng1=");
        sb.append(libLng1);
        sb.append(", libLng2=");
        sb.append(libLng2);
        sb.append(", libLng3=");
        sb.append(libLng3);
        sb.append(", libDat1=");
        sb.append(libDat1);
        sb.append(", libDat2=");
        sb.append(libDat2);
        sb.append(", libDat3=");
        sb.append(libDat3);
        sb.append(", stato=");
        sb.append(stato);
        sb.append(", inviato=");
        sb.append(inviato);
        sb.append(", codiceConsorzio=");
        sb.append(codiceConsorzio);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public TestataDocumento toEntityModel() {
        TestataDocumentoImpl testataDocumentoImpl = new TestataDocumentoImpl();

        testataDocumentoImpl.setAnno(anno);

        if (codiceAttivita == null) {
            testataDocumentoImpl.setCodiceAttivita(StringPool.BLANK);
        } else {
            testataDocumentoImpl.setCodiceAttivita(codiceAttivita);
        }

        if (codiceCentro == null) {
            testataDocumentoImpl.setCodiceCentro(StringPool.BLANK);
        } else {
            testataDocumentoImpl.setCodiceCentro(codiceCentro);
        }

        if (codiceDeposito == null) {
            testataDocumentoImpl.setCodiceDeposito(StringPool.BLANK);
        } else {
            testataDocumentoImpl.setCodiceDeposito(codiceDeposito);
        }

        testataDocumentoImpl.setProtocollo(protocollo);

        if (codiceFornitore == null) {
            testataDocumentoImpl.setCodiceFornitore(StringPool.BLANK);
        } else {
            testataDocumentoImpl.setCodiceFornitore(codiceFornitore);
        }

        if (tipoDocumento == null) {
            testataDocumentoImpl.setTipoDocumento(StringPool.BLANK);
        } else {
            testataDocumentoImpl.setTipoDocumento(tipoDocumento);
        }

        if (dataRegistrazione == Long.MIN_VALUE) {
            testataDocumentoImpl.setDataRegistrazione(null);
        } else {
            testataDocumentoImpl.setDataRegistrazione(new Date(
                    dataRegistrazione));
        }

        if (codiceSpedizione == null) {
            testataDocumentoImpl.setCodiceSpedizione(StringPool.BLANK);
        } else {
            testataDocumentoImpl.setCodiceSpedizione(codiceSpedizione);
        }

        if (codiceCausaleTrasporto == null) {
            testataDocumentoImpl.setCodiceCausaleTrasporto(StringPool.BLANK);
        } else {
            testataDocumentoImpl.setCodiceCausaleTrasporto(codiceCausaleTrasporto);
        }

        if (codiceCuraTrasporto == null) {
            testataDocumentoImpl.setCodiceCuraTrasporto(StringPool.BLANK);
        } else {
            testataDocumentoImpl.setCodiceCuraTrasporto(codiceCuraTrasporto);
        }

        if (codicePorto == null) {
            testataDocumentoImpl.setCodicePorto(StringPool.BLANK);
        } else {
            testataDocumentoImpl.setCodicePorto(codicePorto);
        }

        if (codiceAspettoEsteriore == null) {
            testataDocumentoImpl.setCodiceAspettoEsteriore(StringPool.BLANK);
        } else {
            testataDocumentoImpl.setCodiceAspettoEsteriore(codiceAspettoEsteriore);
        }

        if (codiceVettore1 == null) {
            testataDocumentoImpl.setCodiceVettore1(StringPool.BLANK);
        } else {
            testataDocumentoImpl.setCodiceVettore1(codiceVettore1);
        }

        if (codiceVettore2 == null) {
            testataDocumentoImpl.setCodiceVettore2(StringPool.BLANK);
        } else {
            testataDocumentoImpl.setCodiceVettore2(codiceVettore2);
        }

        if (libStr1 == null) {
            testataDocumentoImpl.setLibStr1(StringPool.BLANK);
        } else {
            testataDocumentoImpl.setLibStr1(libStr1);
        }

        if (libStr2 == null) {
            testataDocumentoImpl.setLibStr2(StringPool.BLANK);
        } else {
            testataDocumentoImpl.setLibStr2(libStr2);
        }

        if (libStr3 == null) {
            testataDocumentoImpl.setLibStr3(StringPool.BLANK);
        } else {
            testataDocumentoImpl.setLibStr3(libStr3);
        }

        testataDocumentoImpl.setLibDbl1(libDbl1);
        testataDocumentoImpl.setLibDbl2(libDbl2);
        testataDocumentoImpl.setLibDbl3(libDbl3);
        testataDocumentoImpl.setLibLng1(libLng1);
        testataDocumentoImpl.setLibLng2(libLng2);
        testataDocumentoImpl.setLibLng3(libLng3);

        if (libDat1 == Long.MIN_VALUE) {
            testataDocumentoImpl.setLibDat1(null);
        } else {
            testataDocumentoImpl.setLibDat1(new Date(libDat1));
        }

        if (libDat2 == Long.MIN_VALUE) {
            testataDocumentoImpl.setLibDat2(null);
        } else {
            testataDocumentoImpl.setLibDat2(new Date(libDat2));
        }

        if (libDat3 == Long.MIN_VALUE) {
            testataDocumentoImpl.setLibDat3(null);
        } else {
            testataDocumentoImpl.setLibDat3(new Date(libDat3));
        }

        testataDocumentoImpl.setStato(stato);
        testataDocumentoImpl.setInviato(inviato);

        if (codiceConsorzio == null) {
            testataDocumentoImpl.setCodiceConsorzio(StringPool.BLANK);
        } else {
            testataDocumentoImpl.setCodiceConsorzio(codiceConsorzio);
        }

        testataDocumentoImpl.resetOriginalValues();

        return testataDocumentoImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        anno = objectInput.readInt();
        codiceAttivita = objectInput.readUTF();
        codiceCentro = objectInput.readUTF();
        codiceDeposito = objectInput.readUTF();
        protocollo = objectInput.readInt();
        codiceFornitore = objectInput.readUTF();
        tipoDocumento = objectInput.readUTF();
        dataRegistrazione = objectInput.readLong();
        codiceSpedizione = objectInput.readUTF();
        codiceCausaleTrasporto = objectInput.readUTF();
        codiceCuraTrasporto = objectInput.readUTF();
        codicePorto = objectInput.readUTF();
        codiceAspettoEsteriore = objectInput.readUTF();
        codiceVettore1 = objectInput.readUTF();
        codiceVettore2 = objectInput.readUTF();
        libStr1 = objectInput.readUTF();
        libStr2 = objectInput.readUTF();
        libStr3 = objectInput.readUTF();
        libDbl1 = objectInput.readDouble();
        libDbl2 = objectInput.readDouble();
        libDbl3 = objectInput.readDouble();
        libLng1 = objectInput.readLong();
        libLng2 = objectInput.readLong();
        libLng3 = objectInput.readLong();
        libDat1 = objectInput.readLong();
        libDat2 = objectInput.readLong();
        libDat3 = objectInput.readLong();
        stato = objectInput.readBoolean();
        inviato = objectInput.readBoolean();
        codiceConsorzio = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeInt(anno);

        if (codiceAttivita == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceAttivita);
        }

        if (codiceCentro == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceCentro);
        }

        if (codiceDeposito == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceDeposito);
        }

        objectOutput.writeInt(protocollo);

        if (codiceFornitore == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceFornitore);
        }

        if (tipoDocumento == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(tipoDocumento);
        }

        objectOutput.writeLong(dataRegistrazione);

        if (codiceSpedizione == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceSpedizione);
        }

        if (codiceCausaleTrasporto == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceCausaleTrasporto);
        }

        if (codiceCuraTrasporto == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceCuraTrasporto);
        }

        if (codicePorto == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codicePorto);
        }

        if (codiceAspettoEsteriore == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceAspettoEsteriore);
        }

        if (codiceVettore1 == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceVettore1);
        }

        if (codiceVettore2 == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceVettore2);
        }

        if (libStr1 == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(libStr1);
        }

        if (libStr2 == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(libStr2);
        }

        if (libStr3 == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(libStr3);
        }

        objectOutput.writeDouble(libDbl1);
        objectOutput.writeDouble(libDbl2);
        objectOutput.writeDouble(libDbl3);
        objectOutput.writeLong(libLng1);
        objectOutput.writeLong(libLng2);
        objectOutput.writeLong(libLng3);
        objectOutput.writeLong(libDat1);
        objectOutput.writeLong(libDat2);
        objectOutput.writeLong(libDat3);
        objectOutput.writeBoolean(stato);
        objectOutput.writeBoolean(inviato);

        if (codiceConsorzio == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceConsorzio);
        }
    }
}
