package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.SchedaPagamento;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing SchedaPagamento in entity cache.
 *
 * @author Mario Torrisi
 * @see SchedaPagamento
 * @generated
 */
public class SchedaPagamentoCacheModel implements CacheModel<SchedaPagamento>,
    Externalizable {
    public int esercizioRegistrazione;
    public String codiceSoggetto;
    public int numeroPartita;
    public int numeroScadenza;
    public boolean tipoSoggetto;
    public int esercizioRegistrazioneComp;
    public String codiceSoggettoComp;
    public int numeroPartitaComp;
    public int numeroScadenzaComp;
    public boolean tipoSoggettoComp;
    public double differenza;
    public int idPagamento;
    public int annoPagamento;
    public int stato;
    public String nota;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(31);

        sb.append("{esercizioRegistrazione=");
        sb.append(esercizioRegistrazione);
        sb.append(", codiceSoggetto=");
        sb.append(codiceSoggetto);
        sb.append(", numeroPartita=");
        sb.append(numeroPartita);
        sb.append(", numeroScadenza=");
        sb.append(numeroScadenza);
        sb.append(", tipoSoggetto=");
        sb.append(tipoSoggetto);
        sb.append(", esercizioRegistrazioneComp=");
        sb.append(esercizioRegistrazioneComp);
        sb.append(", codiceSoggettoComp=");
        sb.append(codiceSoggettoComp);
        sb.append(", numeroPartitaComp=");
        sb.append(numeroPartitaComp);
        sb.append(", numeroScadenzaComp=");
        sb.append(numeroScadenzaComp);
        sb.append(", tipoSoggettoComp=");
        sb.append(tipoSoggettoComp);
        sb.append(", differenza=");
        sb.append(differenza);
        sb.append(", idPagamento=");
        sb.append(idPagamento);
        sb.append(", annoPagamento=");
        sb.append(annoPagamento);
        sb.append(", stato=");
        sb.append(stato);
        sb.append(", nota=");
        sb.append(nota);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public SchedaPagamento toEntityModel() {
        SchedaPagamentoImpl schedaPagamentoImpl = new SchedaPagamentoImpl();

        schedaPagamentoImpl.setEsercizioRegistrazione(esercizioRegistrazione);

        if (codiceSoggetto == null) {
            schedaPagamentoImpl.setCodiceSoggetto(StringPool.BLANK);
        } else {
            schedaPagamentoImpl.setCodiceSoggetto(codiceSoggetto);
        }

        schedaPagamentoImpl.setNumeroPartita(numeroPartita);
        schedaPagamentoImpl.setNumeroScadenza(numeroScadenza);
        schedaPagamentoImpl.setTipoSoggetto(tipoSoggetto);
        schedaPagamentoImpl.setEsercizioRegistrazioneComp(esercizioRegistrazioneComp);

        if (codiceSoggettoComp == null) {
            schedaPagamentoImpl.setCodiceSoggettoComp(StringPool.BLANK);
        } else {
            schedaPagamentoImpl.setCodiceSoggettoComp(codiceSoggettoComp);
        }

        schedaPagamentoImpl.setNumeroPartitaComp(numeroPartitaComp);
        schedaPagamentoImpl.setNumeroScadenzaComp(numeroScadenzaComp);
        schedaPagamentoImpl.setTipoSoggettoComp(tipoSoggettoComp);
        schedaPagamentoImpl.setDifferenza(differenza);
        schedaPagamentoImpl.setIdPagamento(idPagamento);
        schedaPagamentoImpl.setAnnoPagamento(annoPagamento);
        schedaPagamentoImpl.setStato(stato);

        if (nota == null) {
            schedaPagamentoImpl.setNota(StringPool.BLANK);
        } else {
            schedaPagamentoImpl.setNota(nota);
        }

        schedaPagamentoImpl.resetOriginalValues();

        return schedaPagamentoImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        esercizioRegistrazione = objectInput.readInt();
        codiceSoggetto = objectInput.readUTF();
        numeroPartita = objectInput.readInt();
        numeroScadenza = objectInput.readInt();
        tipoSoggetto = objectInput.readBoolean();
        esercizioRegistrazioneComp = objectInput.readInt();
        codiceSoggettoComp = objectInput.readUTF();
        numeroPartitaComp = objectInput.readInt();
        numeroScadenzaComp = objectInput.readInt();
        tipoSoggettoComp = objectInput.readBoolean();
        differenza = objectInput.readDouble();
        idPagamento = objectInput.readInt();
        annoPagamento = objectInput.readInt();
        stato = objectInput.readInt();
        nota = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeInt(esercizioRegistrazione);

        if (codiceSoggetto == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceSoggetto);
        }

        objectOutput.writeInt(numeroPartita);
        objectOutput.writeInt(numeroScadenza);
        objectOutput.writeBoolean(tipoSoggetto);
        objectOutput.writeInt(esercizioRegistrazioneComp);

        if (codiceSoggettoComp == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceSoggettoComp);
        }

        objectOutput.writeInt(numeroPartitaComp);
        objectOutput.writeInt(numeroScadenzaComp);
        objectOutput.writeBoolean(tipoSoggettoComp);
        objectOutput.writeDouble(differenza);
        objectOutput.writeInt(idPagamento);
        objectOutput.writeInt(annoPagamento);
        objectOutput.writeInt(stato);

        if (nota == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(nota);
        }
    }
}
