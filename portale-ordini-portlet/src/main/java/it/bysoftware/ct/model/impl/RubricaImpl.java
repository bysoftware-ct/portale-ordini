package it.bysoftware.ct.model.impl;

/**
 * The extended model implementation for the Rubrica service. Represents a row
 * in the &quot;Rubrica&quot; database table, with each column mapped to a
 * property of this class.
 *
 * <p>Helper methods and all application logic should be put in this class.
 * Whenever methods are added, rerun ServiceBuilder to copy their definitions
 * into the {@link it.bysoftware.ct.model.Rubrica} interface.
 * </p>
 *
 * @author Mario Torrisi
 */
public class RubricaImpl extends RubricaBaseImpl {

    /**
     * Auto-generated serial ID.
     */
    private static final long serialVersionUID = 4428190621384024341L;

    public RubricaImpl() {
    }
}
