package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.CausaliContabili;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing CausaliContabili in entity cache.
 *
 * @author Mario Torrisi
 * @see CausaliContabili
 * @generated
 */
public class CausaliContabiliCacheModel implements CacheModel<CausaliContabili>,
    Externalizable {
    public String codiceCausaleCont;
    public String codiceCausaleEC;
    public String descrizioneCausaleCont;
    public int testDareAvere;
    public int tipoSoggettoMovimentato;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{codiceCausaleCont=");
        sb.append(codiceCausaleCont);
        sb.append(", codiceCausaleEC=");
        sb.append(codiceCausaleEC);
        sb.append(", descrizioneCausaleCont=");
        sb.append(descrizioneCausaleCont);
        sb.append(", testDareAvere=");
        sb.append(testDareAvere);
        sb.append(", tipoSoggettoMovimentato=");
        sb.append(tipoSoggettoMovimentato);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public CausaliContabili toEntityModel() {
        CausaliContabiliImpl causaliContabiliImpl = new CausaliContabiliImpl();

        if (codiceCausaleCont == null) {
            causaliContabiliImpl.setCodiceCausaleCont(StringPool.BLANK);
        } else {
            causaliContabiliImpl.setCodiceCausaleCont(codiceCausaleCont);
        }

        if (codiceCausaleEC == null) {
            causaliContabiliImpl.setCodiceCausaleEC(StringPool.BLANK);
        } else {
            causaliContabiliImpl.setCodiceCausaleEC(codiceCausaleEC);
        }

        if (descrizioneCausaleCont == null) {
            causaliContabiliImpl.setDescrizioneCausaleCont(StringPool.BLANK);
        } else {
            causaliContabiliImpl.setDescrizioneCausaleCont(descrizioneCausaleCont);
        }

        causaliContabiliImpl.setTestDareAvere(testDareAvere);
        causaliContabiliImpl.setTipoSoggettoMovimentato(tipoSoggettoMovimentato);

        causaliContabiliImpl.resetOriginalValues();

        return causaliContabiliImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        codiceCausaleCont = objectInput.readUTF();
        codiceCausaleEC = objectInput.readUTF();
        descrizioneCausaleCont = objectInput.readUTF();
        testDareAvere = objectInput.readInt();
        tipoSoggettoMovimentato = objectInput.readInt();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        if (codiceCausaleCont == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceCausaleCont);
        }

        if (codiceCausaleEC == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceCausaleEC);
        }

        if (descrizioneCausaleCont == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(descrizioneCausaleCont);
        }

        objectOutput.writeInt(testDareAvere);
        objectOutput.writeInt(tipoSoggettoMovimentato);
    }
}
