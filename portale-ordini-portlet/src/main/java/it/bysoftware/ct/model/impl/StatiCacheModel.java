package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.Stati;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Stati in entity cache.
 *
 * @author Mario Torrisi
 * @see Stati
 * @generated
 */
public class StatiCacheModel implements CacheModel<Stati>, Externalizable {
    public boolean black;
    public String codiceStato;
    public String codiceISO;
    public String codicePaese;
    public String stato;
    public String codiceAzienda;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(13);

        sb.append("{black=");
        sb.append(black);
        sb.append(", codiceStato=");
        sb.append(codiceStato);
        sb.append(", codiceISO=");
        sb.append(codiceISO);
        sb.append(", codicePaese=");
        sb.append(codicePaese);
        sb.append(", stato=");
        sb.append(stato);
        sb.append(", codiceAzienda=");
        sb.append(codiceAzienda);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Stati toEntityModel() {
        StatiImpl statiImpl = new StatiImpl();

        statiImpl.setBlack(black);

        if (codiceStato == null) {
            statiImpl.setCodiceStato(StringPool.BLANK);
        } else {
            statiImpl.setCodiceStato(codiceStato);
        }

        if (codiceISO == null) {
            statiImpl.setCodiceISO(StringPool.BLANK);
        } else {
            statiImpl.setCodiceISO(codiceISO);
        }

        if (codicePaese == null) {
            statiImpl.setCodicePaese(StringPool.BLANK);
        } else {
            statiImpl.setCodicePaese(codicePaese);
        }

        if (stato == null) {
            statiImpl.setStato(StringPool.BLANK);
        } else {
            statiImpl.setStato(stato);
        }

        if (codiceAzienda == null) {
            statiImpl.setCodiceAzienda(StringPool.BLANK);
        } else {
            statiImpl.setCodiceAzienda(codiceAzienda);
        }

        statiImpl.resetOriginalValues();

        return statiImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        black = objectInput.readBoolean();
        codiceStato = objectInput.readUTF();
        codiceISO = objectInput.readUTF();
        codicePaese = objectInput.readUTF();
        stato = objectInput.readUTF();
        codiceAzienda = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeBoolean(black);

        if (codiceStato == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceStato);
        }

        if (codiceISO == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceISO);
        }

        if (codicePaese == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codicePaese);
        }

        if (stato == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(stato);
        }

        if (codiceAzienda == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(codiceAzienda);
        }
    }
}
