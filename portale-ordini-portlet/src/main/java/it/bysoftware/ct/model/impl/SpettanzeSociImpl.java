package it.bysoftware.ct.model.impl;

/**
 * The extended model implementation for the SpettanzeSoci service. Represents a row in the &quot;_spettanze_soci&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.bysoftware.ct.model.SpettanzeSoci} interface.
 * </p>
 *
 * @author Mario Torrisi
 */
public class SpettanzeSociImpl extends SpettanzeSociBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a spettanze soci model instance should use the {@link it.bysoftware.ct.model.SpettanzeSoci} interface instead.
     */
    public SpettanzeSociImpl() {
    }
}
