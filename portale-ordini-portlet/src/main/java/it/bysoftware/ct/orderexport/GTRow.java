package it.bysoftware.ct.orderexport;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import it.bysoftware.ct.utils.Utils;

public class GTRow {
	private static final DecimalFormat NF = (DecimalFormat) NumberFormat.getInstance(Locale.ITALIAN);
	private static final String CSV_SEPARATOR = ";";
	private int LPosizione;
	private String LEAN;
	private String LNrArticoloFornitore;
	private String LNrArticoloInterno;
	private String LDescrizione;
	private double LQuantita;
	private String LUnitaDiMisura;
	private String LQuantitaUdM;
	private double LCostoLordo;
	private double LIVAPercent;
	private double LSconto1;
	private double LSconto2;
	private double LSconto3;
	private double LSconto4;
	private double LSconto5;
	private double LScontoPromo1;
	private double LScontoPromo2;
	private double LScontoPromo3;
	private double LScontoPromo4;
	private double LScontoPromo5;
	private double LImportoRiga;
	private double LPrezzoEtichette;
	private int LQuantitaEtichette;
	private String LIntraCodTariffa;
	private double LIntraPesoLordo;
	private double LIntraPesoNetto;
	private double LIntraNumeroColli;
	private String LPasspNrRUOP;
	private String LPasspLotto;
	private int LQuantitaPerKit;
	private String LEanPadre;
	private String LCodiceFamiglia;

	public GTRow(String lEAN, String lNrArticoloFornitore,
			String lNrArticoloInterno, String lDescrizione, double lQuantita,
			String lUnitaDiMisura, String lQuantitaUdM, double lCostoLordo,
			double lIVAPercent, double lSconto1, double lSconto2,
			double lSconto3, double lSconto4, double lSconto5,
			double lScontoPromo1, double lScontoPromo2, double lScontoPromo3,
			double lScontoPromo4, double lScontoPromo5, double lImportoRiga,
			double lPrezzoEtichette, int lQuantitaEtichette,
			String lIntraCodTariffa, double lIntraPesoLordo,
			double lIntraPesoNetto, double lIntraNumeroColli,
			String lPasspNrRUOP, String lPasspLotto, int lQuantitaPerKit,
			String lEanPadre, String lCodiceFamiglia) {
		super();
		LEAN = lEAN;
		LNrArticoloFornitore = lNrArticoloFornitore;
		LNrArticoloInterno = lNrArticoloInterno;
		LDescrizione = lDescrizione;
		LQuantita = lQuantita;
		LUnitaDiMisura = lUnitaDiMisura;
		LQuantitaUdM = lQuantitaUdM;
		LCostoLordo = lCostoLordo;
		LIVAPercent = lIVAPercent;
		LSconto1 = lSconto1;
		LSconto2 = lSconto2;
		LSconto3 = lSconto3;
		LSconto4 = lSconto4;
		LSconto5 = lSconto5;
		LScontoPromo1 = lScontoPromo1;
		LScontoPromo2 = lScontoPromo2;
		LScontoPromo3 = lScontoPromo3;
		LScontoPromo4 = lScontoPromo4;
		LScontoPromo5 = lScontoPromo5;
		LImportoRiga = lImportoRiga;
		LPrezzoEtichette = lPrezzoEtichette;
		LQuantitaEtichette = lQuantitaEtichette;
		LIntraCodTariffa = lIntraCodTariffa;
		LIntraPesoLordo = lIntraPesoLordo;
		LIntraPesoNetto = lIntraPesoNetto;
		LIntraNumeroColli = lIntraNumeroColli;
		LPasspNrRUOP = lPasspNrRUOP;
		LPasspLotto = lPasspLotto;
		LQuantitaPerKit = lQuantitaPerKit;
		LEanPadre = lEanPadre;
		LCodiceFamiglia = lCodiceFamiglia;
	}

	public int getLPosizione() {
		return LPosizione;
	}

	public void setLPosizione(int lPosizione) {
		LPosizione = lPosizione;
	}

	public String getLEAN() {
		return LEAN;
	}

	public void setLEAN(String lEAN) {
		LEAN = lEAN;
	}

	public String getLNrArticoloFornitore() {
		return LNrArticoloFornitore;
	}

	public void setLNrArticoloFornitore(String lNrArticoloFornitore) {
		LNrArticoloFornitore = lNrArticoloFornitore;
	}

	public String getLNrArticoloInterno() {
		return LNrArticoloInterno;
	}

	public void setLNrArticoloInterno(String lNrArticoloInterno) {
		LNrArticoloInterno = lNrArticoloInterno;
	}

	public String getLDescrizione() {
		return LDescrizione;
	}

	public void setLDescrizione(String lDescrizione) {
		LDescrizione = lDescrizione;
	}

	public double getLQuantita() {
		return LQuantita;
	}

	public void setLQuantita(double lQuantita) {
		LQuantita = lQuantita;
	}

	public String getLUnitaDiMisura() {
		return LUnitaDiMisura;
	}

	public void setLUnitaDiMisura(String lUnitaDiMisura) {
		LUnitaDiMisura = lUnitaDiMisura;
	}

	public String getLQuantitaUdM() {
		return LQuantitaUdM;
	}

	public void setLQuantitaUdM(String lQuantitaUdM) {
		LQuantitaUdM = lQuantitaUdM;
	}

	public double getLCostoLordo() {
		return LCostoLordo;
	}

	public void setLCostoLordo(double lCostoLordo) {
		LCostoLordo = lCostoLordo;
	}

	public double getLIVAPercent() {
		return LIVAPercent;
	}

	public void setLIVAPercent(double lIVAPercent) {
		LIVAPercent = lIVAPercent;
	}

	public double getLSconto1() {
		return LSconto1;
	}

	public void setLSconto1(double lSconto1) {
		LSconto1 = lSconto1;
	}

	public double getLSconto2() {
		return LSconto2;
	}

	public void setLSconto2(double lSconto2) {
		LSconto2 = lSconto2;
	}

	public double getLSconto3() {
		return LSconto3;
	}

	public void setLSconto3(double lSconto3) {
		LSconto3 = lSconto3;
	}

	public double getLSconto4() {
		return LSconto4;
	}

	public void setLSconto4(double lSconto4) {
		LSconto4 = lSconto4;
	}

	public double getLSconto5() {
		return LSconto5;
	}

	public void setLSconto5(double lSconto5) {
		LSconto5 = lSconto5;
	}

	public double getLScontoPromo1() {
		return LScontoPromo1;
	}

	public void setLScontoPromo1(double lScontoPromo1) {
		LScontoPromo1 = lScontoPromo1;
	}

	public double getLScontoPromo2() {
		return LScontoPromo2;
	}

	public void setLScontoPromo2(double lScontoPromo2) {
		LScontoPromo2 = lScontoPromo2;
	}

	public double getLScontoPromo3() {
		return LScontoPromo3;
	}

	public void setLScontoPromo3(double lScontoPromo3) {
		LScontoPromo3 = lScontoPromo3;
	}

	public double getLScontoPromo4() {
		return LScontoPromo4;
	}

	public void setLScontoPromo4(double lScontoPromo4) {
		LScontoPromo4 = lScontoPromo4;
	}

	public double getLScontoPromo5() {
		return LScontoPromo5;
	}

	public void setLScontoPromo5(double lScontoPromo5) {
		LScontoPromo5 = lScontoPromo5;
	}

	public double getLImportoRiga() {
		return LImportoRiga;
	}

	public void setLImportoRiga(double lImportoRiga) {
		LImportoRiga = lImportoRiga;
	}

	public double getLPrezzoEtichette() {
		return LPrezzoEtichette;
	}

	public void setLPrezzoEtichette(double lPrezzoEtichette) {
		LPrezzoEtichette = lPrezzoEtichette;
	}

	public int getLQuantitaEtichette() {
		return LQuantitaEtichette;
	}

	public void setLQuantitaEtichette(int lQuantitaEtichette) {
		LQuantitaEtichette = lQuantitaEtichette;
	}

	public String getLIntraCodTariffa() {
		return LIntraCodTariffa;
	}

	public void setLIntraCodTariffa(String lIntraCodTariffa) {
		LIntraCodTariffa = lIntraCodTariffa;
	}

	public double getLIntraPesoLordo() {
		return LIntraPesoLordo;
	}

	public void setLIntraPesoLordo(double lIntraPesoLordo) {
		LIntraPesoLordo = lIntraPesoLordo;
	}

	public double getLIntraPesoNetto() {
		return LIntraPesoNetto;
	}

	public void setLIntraPesoNetto(double lIntraPesoNetto) {
		LIntraPesoNetto = lIntraPesoNetto;
	}

	public double getLIntraNumeroColli() {
		return LIntraNumeroColli;
	}

	public void setLIntraNumeroColli(double lIntraNumeroColli) {
		LIntraNumeroColli = lIntraNumeroColli;
	}

	public String getLPasspNrRUOP() {
		return LPasspNrRUOP;
	}

	public void setLPasspNrRUOP(String lPasspNrRUOP) {
		LPasspNrRUOP = lPasspNrRUOP;
	}

	public String getLPasspLotto() {
		return LPasspLotto;
	}

	public void setLPasspLotto(String lPasspLotto) {
		LPasspLotto = lPasspLotto;
	}

	public int getLQuantitaPerKit() {
		return LQuantitaPerKit;
	}

	public void setLQuantitaPerKit(int lQuantitaPerKit) {
		LQuantitaPerKit = lQuantitaPerKit;
	}

	public String getLEanPadre() {
		return LEanPadre;
	}

	public void setLEanPadre(String lEanPadre) {
		LEanPadre = lEanPadre;
	}

	public String getLCodiceFamiglia() {
		return LCodiceFamiglia;
	}

	public void setLCodiceFamiglia(String lCodiceFamiglia) {
		LCodiceFamiglia = lCodiceFamiglia;
	}

	public String toCSV() {
		StringBuilder csvBuilder = new StringBuilder();

		csvBuilder.append(LPosizione).append(CSV_SEPARATOR)
				.append(Utils.escapeCsv(LEAN, CSV_SEPARATOR))
				.append(CSV_SEPARATOR)
				.append(Utils.escapeCsv(LNrArticoloFornitore, CSV_SEPARATOR))
				.append(CSV_SEPARATOR)
				.append(Utils.escapeCsv(LNrArticoloInterno, CSV_SEPARATOR))
				.append(CSV_SEPARATOR)
				.append(Utils.escapeCsv(LDescrizione, CSV_SEPARATOR))
				.append(CSV_SEPARATOR).append(NF.format(LQuantita)).append(CSV_SEPARATOR)
				.append(Utils.escapeCsv(LUnitaDiMisura, CSV_SEPARATOR))
				.append(CSV_SEPARATOR)
				.append(Utils.escapeCsv(LQuantitaUdM, CSV_SEPARATOR))
				.append(CSV_SEPARATOR).append(NF.format(LCostoLordo))
				.append(CSV_SEPARATOR).append(NF.format(LIVAPercent))
				.append(CSV_SEPARATOR).append(NF.format(LSconto1)).append(CSV_SEPARATOR)
				.append(NF.format(LSconto2)).append(CSV_SEPARATOR).append(NF.format(LSconto3))
				.append(CSV_SEPARATOR).append(NF.format(LSconto4)).append(CSV_SEPARATOR)
				.append(NF.format(LSconto5)).append(CSV_SEPARATOR).append(NF.format(LScontoPromo1))
				.append(CSV_SEPARATOR).append(NF.format(LScontoPromo2))
				.append(CSV_SEPARATOR).append(NF.format(LScontoPromo3))
				.append(CSV_SEPARATOR).append(NF.format(LScontoPromo4))
				.append(CSV_SEPARATOR).append(NF.format(LScontoPromo5))
				.append(CSV_SEPARATOR).append(NF.format(LImportoRiga))
				.append(CSV_SEPARATOR).append(NF.format(LPrezzoEtichette))
				.append(CSV_SEPARATOR).append(LQuantitaEtichette)
				.append(CSV_SEPARATOR)
				.append(Utils.escapeCsv(LIntraCodTariffa, CSV_SEPARATOR))	
				.append(CSV_SEPARATOR).append(NF.format(LIntraPesoLordo))
				.append(CSV_SEPARATOR).append(NF.format(LIntraPesoNetto))
				.append(CSV_SEPARATOR).append(NF.format(LIntraNumeroColli))
				.append(CSV_SEPARATOR)
				.append(Utils.escapeCsv(LPasspNrRUOP, CSV_SEPARATOR))
				.append(CSV_SEPARATOR)
				.append(Utils.escapeCsv(LPasspLotto, CSV_SEPARATOR))
				.append(CSV_SEPARATOR).append(LQuantitaPerKit)
				.append(CSV_SEPARATOR)
				.append(Utils.escapeCsv(LEanPadre, CSV_SEPARATOR))
				.append(CSV_SEPARATOR)
				.append(Utils.escapeCsv(LCodiceFamiglia, CSV_SEPARATOR));

		return csvBuilder.toString();
	}

}
