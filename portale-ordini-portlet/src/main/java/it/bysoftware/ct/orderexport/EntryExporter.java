package it.bysoftware.ct.orderexport;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.util.portlet.PortletProps;

import it.bysoftware.ct.model.CausaliContabili;
import it.bysoftware.ct.model.DettagliOperazioniContabili;
import it.bysoftware.ct.model.ScadenzePartite;
import it.bysoftware.ct.service.CausaliContabiliLocalServiceUtil;
import it.bysoftware.ct.service.DettagliOperazioniContabiliLocalServiceUtil;
import it.bysoftware.ct.utils.Utils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * This class manage the Entry Exporter behaviour.
 * 
 * @author Mario Torrisi
 *
 */
public class EntryExporter extends CSVExporter {

    /**
     * Default currency code.
     */
    protected static final String CURRENCY_CODE = PortletProps
            .get("def-currency-code");

    /**
     * Default currency code.
     */
    protected static final String[] ACCOUNTING_OPERATION = PortletProps
            .get("def-accounting-operation").split(",");

    /**
     * Default currency code.
     */
    protected static final String SUBACCOUNT_CODE = PortletProps
            .get("suppliers-subaccount-code");
    
    /**
     * Decimal format.
     */
    private static final DecimalFormat DF = new DecimalFormat("#.##");
    
    /**
     * Entry header columns.
     */
    private static final String[] ENTRY_HEADER_COLS = {
            "WORKTESTATAREGISTRAZIONE", "Annoes", "Cambio", "Codatt", "Codcen",
            "Coddiv", "Codope", "Datope", "Datreg", "Dtcamb", "Tescam",
            "Protoc1", "" };
    /**
     * Accounting entries header columns.
     */
    private static final String[] ACCONTING_HEADER_COLS = {
            "WORKRIGHECONTABILI", "NRiga", "Avere", "Codaut", "Codcae",
            "Codcau", "Codcon", "Codpag", "Codsog", "Darave", "Dare", "Datdoc",
            "Dtcapg", "TesAut", "Tipimp", "Tipsog", "Descri", "" };

    /**
     * Entry header columns.
     */
    private static final String[] MOV_SCADEN_HEADER_COLS = { "WORKMOVSCADENZE",
            "NRigaMovimento", "Tipsog", "Codsog", "Annpar", "Numpar", "Numsca",
            "Impmov", "Codage", "Imppro", "" };

    /**
     * Entry is going to be closed.
     */
    private ScadenzePartite entry;
    
    /**
     * Rewarded entry is going to be closed.
     */
    private ScadenzePartite rewardedEntry;

    /**
     * Entry is going to be closed.
     */
    private DettagliOperazioniContabili[] accountingOpsDetails;

    /**
     * Sub account used to pay supplier.
     */
    private String subAccount;
    
    /**
     * Entry note.
     */
    private String note;

    /**
     * Creates an Entry exporter object.
     */
    public EntryExporter() {
        super();
    }

    /**
     * Creates an Entry exporter object.
     * 
     * @param e
     *            the entry
     * @param re 
     *            the rewarded entry
     * @param s
     *            the sub account used
     * @param n
     *            entry note
     */
    public EntryExporter(final ScadenzePartite e, final ScadenzePartite re,
            final String s, final String n) {
        super();
        this.entry = e;
        this.rewardedEntry = re;
        this.subAccount = s;
        this.note = n;
    }

    /*
     * (non-Javadoc)
     * 
     * @see it.bysoftware.ct.orderexport.CSVExporter#exportDocument()
     */
    @Override
    public final String exportDocument() throws PortalException,
            SystemException {
        StringBundler sb = new StringBundler();

        if (this.entry != null) {
            sb.append(this.exportHeader(ENTRY_HEADER_COLS));
            sb.append(Utils.getYear());
            sb.append(CSV_SEPARATOR);
            sb.append(1);
            sb.append(CSV_SEPARATOR);
            sb.append(this.entry.getCodiceAttivita());
            sb.append(CSV_SEPARATOR);
            sb.append(this.entry.getCodiceCentro());
            sb.append(CSV_SEPARATOR);
            sb.append(CURRENCY_CODE);
            sb.append(CSV_SEPARATOR);
            sb.append(ACCOUNTING_OPERATION[0]);
            sb.append(CSV_SEPARATOR);
            sb.append(new SimpleDateFormat(DocumentExporter.DD_MM_YYYY)
                    .format(new Date()));
            sb.append(CSV_SEPARATOR);
            sb.append(new SimpleDateFormat(DocumentExporter.DD_MM_YYYY)
                    .format(new Date()));
            sb.append(CSV_SEPARATOR);
            sb.append(new SimpleDateFormat(DocumentExporter.DD_MM_YYYY)
                    .format(new Date()));
            sb.append(CSV_SEPARATOR);
            sb.append(2);
            sb.append(CSV_SEPARATOR);
            sb.append(this.entry.getNumeroDocumento());
            sb.append(CSV_SEPARATOR);
            sb.append(CharPool.NEW_LINE);
            sb.append(this.exportHeader(ACCONTING_HEADER_COLS));
            List<DettagliOperazioniContabili> aod =
                    DettagliOperazioniContabiliLocalServiceUtil.
                    findByAccountingOperation(ACCOUNTING_OPERATION[0]);
            this.accountingOpsDetails =
                    new DettagliOperazioniContabili[aod.size()];
            this.accountingOpsDetails = aod.toArray(this.accountingOpsDetails);
            for (int i = 0; i < this.accountingOpsDetails.length; i++) {
                DettagliOperazioniContabili operationDetails =
                        this.accountingOpsDetails[i];
                CausaliContabili causal = CausaliContabiliLocalServiceUtil.
                        getCausaliContabili(operationDetails.
                                getCausaleContabile());
                
                sb.append(i + 1);
                sb.append(CSV_SEPARATOR);
                String partner = "";
                double give = 0.0;
                double have = 0.0;
                String currentSub = "";
                if (causal.getTestDareAvere() == 1) {
                    currentSub = SUBACCOUNT_CODE;
                    partner = entry.getCodiceSoggetto();
                    if (this.rewardedEntry != null) {
                        give = (this.entry.getImportoTotale()
                                - this.entry.getImportoPagato()) 
                                - (this.rewardedEntry.getImportoTotale()
                                        - this.rewardedEntry.getImportoPagato()
                                        );
                    } else {
                        give = this.entry.getImportoTotale()
                                - this.entry.getImportoPagato();
                    }
                } else {
                    if (this.rewardedEntry != null) {
                        have = (this.entry.getImportoTotale()
                                - this.entry.getImportoPagato()) 
                                - (this.rewardedEntry.getImportoTotale()
                                        - this.rewardedEntry.getImportoPagato()
                                        );
                    } else {
                        have = this.entry.getImportoTotale()
                                - this.entry.getImportoPagato();
                    }
                    if (this.subAccount.isEmpty()) {
                        currentSub = operationDetails.getSottoconto();
                    } else {
                        currentSub = this.subAccount;
                    }
                }
                sb.append(DF.format(have));
                sb.append(CSV_SEPARATOR);
                sb.append(0);
                sb.append(CSV_SEPARATOR);
                sb.append(causal.getCodiceCausaleEC());
                sb.append(CSV_SEPARATOR);
                sb.append(causal.getCodiceCausaleCont());
                sb.append(CSV_SEPARATOR);
                sb.append(currentSub);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(partner);
                sb.append(CSV_SEPARATOR);
                sb.append(causal.getTestDareAvere());
                sb.append(CSV_SEPARATOR);
                sb.append(DF.format(give));
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(1);
                sb.append(CSV_SEPARATOR);
                sb.append(1);
                sb.append(CSV_SEPARATOR);
                sb.append(causal.getTipoSoggettoMovimentato());
                sb.append(CSV_SEPARATOR);
                sb.append(this.note);
                sb.append(CSV_SEPARATOR);
                sb.append(CharPool.NEW_LINE);
            }
            sb.append(this.exportHeader(MOV_SCADEN_HEADER_COLS));
            sb.append(1);
            sb.append(CSV_SEPARATOR);
            if (this.entry.getTipoSoggetto()) {
                sb.append(1);
            } else {
                sb.append(0);
            }
            sb.append(CSV_SEPARATOR);
            sb.append(this.entry.getCodiceSoggetto());
            sb.append(CSV_SEPARATOR);
            sb.append(this.entry.getEsercizioRegistrazione());
            sb.append(CSV_SEPARATOR);
            sb.append(this.entry.getNumeroPartita());
            sb.append(CSV_SEPARATOR);
            sb.append(this.entry.getNumeroScadenza());
            sb.append(CSV_SEPARATOR);
            if (this.rewardedEntry != null) {
                sb.append(DF.format((this.entry.getImportoTotale()
                        - this.entry.getImportoPagato()) 
                        - (this.rewardedEntry.getImportoTotale()
                                - this.rewardedEntry.getImportoPagato()
                                )));
            } else {
                sb.append(DF.format((this.entry.getImportoTotale()
                        - this.entry.getImportoPagato())));
            }
            sb.append(CSV_SEPARATOR);
            sb.append(CSV_SEPARATOR);
            sb.append(0);
            sb.append(CSV_SEPARATOR);
            sb.append(CharPool.NEW_LINE);
        }
        return sb.toString();
    }

    /**
     * Exports the rewarder entry if not null.
     * 
     * @return the rewarded entry
     * @throws SystemException
     *             exception
     * @throws PortalException
     *             exception
     */
    public final String exportComp() throws PortalException, SystemException {
        StringBundler sb = new StringBundler();

        if (this.rewardedEntry != null) {
            sb.append(this.exportHeader(ENTRY_HEADER_COLS));
            sb.append(Utils.getYear());
            sb.append(CSV_SEPARATOR);
            sb.append(1);
            sb.append(CSV_SEPARATOR);
            sb.append(this.entry.getCodiceAttivita());
            sb.append(CSV_SEPARATOR);
            sb.append(this.entry.getCodiceCentro());
            sb.append(CSV_SEPARATOR);
            sb.append(CURRENCY_CODE);
            sb.append(CSV_SEPARATOR);
            sb.append(ACCOUNTING_OPERATION[1]);
            sb.append(CSV_SEPARATOR);
            sb.append(new SimpleDateFormat(DocumentExporter.DD_MM_YYYY)
                    .format(new Date()));
            sb.append(CSV_SEPARATOR);
            sb.append(new SimpleDateFormat(DocumentExporter.DD_MM_YYYY)
                    .format(new Date()));
            sb.append(CSV_SEPARATOR);
            sb.append(new SimpleDateFormat(DocumentExporter.DD_MM_YYYY)
                    .format(new Date()));
            sb.append(CSV_SEPARATOR);
            sb.append(2);
            sb.append(CSV_SEPARATOR);
            sb.append(this.entry.getNumeroDocumento());
            sb.append(CSV_SEPARATOR);
            sb.append(CharPool.NEW_LINE);
            sb.append(this.exportHeader(ACCONTING_HEADER_COLS));
            List<DettagliOperazioniContabili> aod =
                    DettagliOperazioniContabiliLocalServiceUtil.
                    findByAccountingOperation(ACCOUNTING_OPERATION[1]);
            this.accountingOpsDetails =
                    new DettagliOperazioniContabili[aod.size()];
            this.accountingOpsDetails = aod.toArray(this.accountingOpsDetails);
            double rewardedAmount = this.rewardedEntry.getImportoTotale()
                    - this.rewardedEntry.getImportoPagato();
            double entryAmount = this.entry.getImportoTotale()
                    - this.entry.getImportoPagato();
            for (int i = 0; i < this.accountingOpsDetails.length; i++) {
                DettagliOperazioniContabili operationDetails =
                        this.accountingOpsDetails[i];
                CausaliContabili causal = CausaliContabiliLocalServiceUtil.
                        getCausaliContabili(operationDetails.
                                getCausaleContabile());
                sb.append(i + 1);
                sb.append(CSV_SEPARATOR);
                double give = 0.0;
                double have = 0.0;
                if (causal.getTestDareAvere() == 1) {
                    if (rewardedAmount > entryAmount) {
                        give = entryAmount;
                    } else {
                        give = rewardedAmount;
                    }
                } else {
                    if (rewardedAmount > entryAmount) {
                        have = entryAmount;
                    } else {
                        have = rewardedAmount;
                    }
                }
                sb.append(have);
                sb.append(CSV_SEPARATOR);
                sb.append(0);
                sb.append(CSV_SEPARATOR);
                sb.append(causal.getCodiceCausaleEC());
                sb.append(CSV_SEPARATOR);
                sb.append(causal.getCodiceCausaleCont());
                sb.append(CSV_SEPARATOR);
                sb.append(operationDetails.getSottoconto());
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(this.rewardedEntry.getCodiceSoggetto());
                sb.append(CSV_SEPARATOR);
                sb.append(causal.getTestDareAvere());
                sb.append(CSV_SEPARATOR);
                sb.append(give);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(1);
                sb.append(CSV_SEPARATOR);
                sb.append(1);
                sb.append(CSV_SEPARATOR);
                sb.append(causal.getTipoSoggettoMovimentato());
                sb.append(CSV_SEPARATOR);
                sb.append(this.note);
                sb.append(CSV_SEPARATOR);
                sb.append(CharPool.NEW_LINE);
            }
            sb.append(this.exportHeader(MOV_SCADEN_HEADER_COLS));
            for (int j = 0; j < 2; j++) {
                ScadenzePartite s = null;
                if (j == 0) {
                    s = this.entry;
                } else {
                    s = this.rewardedEntry;
                }
                
                sb.append(j + 1);
                sb.append(CSV_SEPARATOR);
                if (s.getTipoSoggetto()) {
                    sb.append(1);
                } else {
                    sb.append(0);
                }
                sb.append(CSV_SEPARATOR);
                sb.append(s.getCodiceSoggetto());
                sb.append(CSV_SEPARATOR);
                sb.append(s.getEsercizioRegistrazione());
                sb.append(CSV_SEPARATOR);
                sb.append(s.getNumeroPartita());
                sb.append(CSV_SEPARATOR);
                sb.append(s.getNumeroScadenza());
                sb.append(CSV_SEPARATOR);
                if (rewardedAmount > entryAmount) {
                    sb.append(entryAmount);
                } else {
                    sb.append(rewardedAmount);
                }
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(0);
                sb.append(CSV_SEPARATOR);
                sb.append(CharPool.NEW_LINE);
            }
        }
        return sb.toString();
    }
}
