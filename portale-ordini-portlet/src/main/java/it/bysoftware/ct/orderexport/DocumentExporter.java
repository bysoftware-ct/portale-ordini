package it.bysoftware.ct.orderexport;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.util.portlet.PortletProps;

import it.bysoftware.ct.model.RigoDocumento;
import it.bysoftware.ct.model.TestataDocumento;
import it.bysoftware.ct.utils.Report;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.sf.jasperreports.engine.JRException;

/**
 * @author Aliseo-G
 *
 */
public abstract class DocumentExporter extends CSVExporter {

    /**
     * Default storage code.
     */
    protected static final String GIVE_INVOICE =
            PortletProps.get("give-invoice");
    
    /**
     * Default storage code.
     */
    protected static final String SELL_INVOICE =
            PortletProps.get("sell-invoice");
    
    /**
     * Order to be exported.
     */
    private TestataDocumento document;

    /**
     * List of row.
     */
    private List<RigoDocumento> rows;

    /**
     * Database connection used to generate report.
     */
    private Connection connection;
    
    /**
     * Report file.
     */
    private File report;

    /**
     * Create an {@link DocumentExporter}.
     */
    DocumentExporter() {
        this(null, new ArrayList<RigoDocumento>(), null);
    }

    /**
     * Creates an {@link DocumentExporter} instance with the document
     * and rows have to be exported.
     * 
     * @param header
     *            the documents is going to be exported.
     * @param list
     *            document rows.
     * @param conn
     *            database connection;
     */
    public DocumentExporter(final TestataDocumento header,
            final List<RigoDocumento> list, final Connection conn) {
        super();
        this.connection = conn;
        this.document = header;
        this.setRows(list);
    }

    /**
     * Return the document.
     * 
     * @return the document
     */
    public final TestataDocumento getDocument() {
        return this.document;
    }

    /**
     * Sets the document.
     * 
     * @param header
     *            the document
     */
    public final void setOrder(final TestataDocumento header) {
        this.document = header;
    }

    /**
     * Returns row list.
     * 
     * @return the rows
     */
    public final List<RigoDocumento> getRows() {
        return this.rows;
    }

    /**
     * Sets row list.
     * 
     * @param list
     *            the rows to set
     */
    public final void setRows(final List<RigoDocumento> list) {
        this.rows = list;
    }

    /**
     * Returns the report file.
     * 
     * @return the report file
     */
    public final File getReport() {
        return this.report;
    }

    /**
     * Sets report path file.
     * 
     * @param file
     *            the report to set
     */
    public final void setReport(final File file) {
        this.report = file;
    }
    
    @Override
    public abstract String exportDocument() throws PortalException,
            SystemException;

    /**
     * Creates the report file for the current transport document.
     * 
     * @param companyId
     *            the company ID
     * @param type document type
     * 
     * @throws SQLException
     *             exception
     * @throws SystemException
     *             exception
     * @throws PortalException
     *             exception
     * @throws JRException
     *             exception
     * @throws IOException exception
     */
    public final void createReport(final long companyId, final String type)
            throws SQLException, PortalException, SystemException, JRException,
            IOException {
        Report r = new Report(this.connection);
        this.setReport(r.printTranspDoc(this.document, companyId, type, true));
    }
}
