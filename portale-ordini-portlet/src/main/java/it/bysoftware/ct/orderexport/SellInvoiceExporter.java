package it.bysoftware.ct.orderexport;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.StringBundler;

import it.bysoftware.ct.model.ArticoloSocio;
import it.bysoftware.ct.model.RigoDocumento;
import it.bysoftware.ct.model.TestataDocumento;
import it.bysoftware.ct.service.ArticoloSocioLocalServiceUtil;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * This class manage the sell invoice behaviour.
 * 
 * @author Mario Torrisi
 *
 */
public class SellInvoiceExporter extends DocumentExporter {

    /**
     * Body columns.
     */
    private static final String[] INVOICE_BODY_COLS = {
        "WorkRigaDocumento", "Tiprig", "Codart", "Codvar", "Descri", "Quanet",
        "Qm2net", "Prezzo", "Scont1", "Scont2", "Scont3", "Libstr1", "Libstr2",
        "Libstr3", "Libdbl1", "Libdbl2", "Libdbl3", "Liblng1", "Liblng2",
        "Liblng3", "Libdat1", "Libdat2", "Libdat3", "Impnet", "Codiva",
        "Numbol", "Numrigbol"
    };
    
    /**
     * Creates an {@link SellInvoiceExporter} instance with the document
     * and rows have to be exported.
     * 
     * @param invoice
     *            the documents is going to be exported.
     * @param rows
     *            document rows.
     * @param c
     *            database connection;
     */
    public SellInvoiceExporter(final TestataDocumento invoice,
            final List<RigoDocumento> rows, final Connection c) {
         super(invoice, rows, c);
    }

    /*
     * (non-Javadoc)
     * 
     * @see it.bysoftware.ct.orderexport.DocumentExporter#exportDocument()
     */
    @Override
    public final String exportDocument() throws PortalException,
            SystemException {
        StringBundler sb = new StringBundler();
        sb.append(this.exportHeader(HEADER_COLS));
        
        if (this.getDocument() != null) {
            sb.append(SELL_INVOICE);
            sb.append(CSV_SEPARATOR);
            sb.append(this.getDocument().getCodiceCentro());
            sb.append(CSV_SEPARATOR);
            sb.append(new SimpleDateFormat(
                    TransportDocumentExporter.DD_MM_YYYY).format(
                            this.getDocument().getDataRegistrazione()));
            sb.append(CSV_SEPARATOR);
            sb.append(this.getDocument().getCodiceConsorzio());
            sb.append(CSV_SEPARATOR);
            sb.append(CSV_SEPARATOR);
            sb.append(CSV_SEPARATOR);
            sb.append(CSV_SEPARATOR);
            sb.append(CSV_SEPARATOR);
            sb.append(CSV_SEPARATOR);
            sb.append(this.getDocument().getProtocollo());
            sb.append(CSV_SEPARATOR);
            sb.append(this.getDocument().getProtocollo());
            sb.append(CSV_SEPARATOR);
            sb.append(new SimpleDateFormat(
                    TransportDocumentExporter.DD_MM_YYYY).format(
                            this.getDocument().getDataRegistrazione()));
            sb.append(CSV_SEPARATOR);
            sb.append(new SimpleDateFormat(
                    TransportDocumentExporter.DD_MM_YYYY).format(
                            this.getDocument().getDataRegistrazione()));
            sb.append(CSV_SEPARATOR);
            sb.append(this.getDocument().getProtocollo());
            sb.append(CSV_SEPARATOR);
            sb.append(this.getDocument().getLibLng1());
            sb.append(CharPool.NEW_LINE);
            sb.append(this.exportHeader(INVOICE_BODY_COLS));
            for (RigoDocumento row : this.getRows()) {
                sb.append(row.getTipoRigo());
                sb.append(CSV_SEPARATOR);
                
                if (row.getTipoRigo() == 0) {
                    ArticoloSocio partnerItem = ArticoloSocioLocalServiceUtil.
                            findPartnerItems(row.getCodiceFornitore(),
                                    row.getCodiceArticolo(),
                                    row.getCodiceVariante());
                    if (partnerItem != null) {
                        sb.append(partnerItem.getCodiceArticoloSocio());
                        sb.append(CSV_SEPARATOR);
                        sb.append(partnerItem.getCodiceVarianteSocio());
                        sb.append(CSV_SEPARATOR);
                    } else {
                        sb.append(row.getCodiceArticolo());
                        sb.append(CSV_SEPARATOR);
                        sb.append(row.getCodiceVariante());
                        sb.append(CSV_SEPARATOR);
                    }
                } else {
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(row.getDescrizione());
                }
                sb.append(CSV_SEPARATOR);
                sb.append(row.getQuantita());
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(row.getPrezzo());
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                if (row.getTipoRigo() == 0) {
                    sb.append(row.getLibStr1());
                }
                sb.append(CSV_SEPARATOR);
                if (row.getTipoRigo() == 0) {
                    sb.append(row.getLibStr2());
                }
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                if (row.getTipoRigo() == 0) {
                    sb.append(row.getLibLng1());
                }
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(row.getImportoNetto());
                sb.append(CSV_SEPARATOR);
                sb.append(row.getCodiceIVA());
                sb.append(CSV_SEPARATOR);
                sb.append(this.getDocument().getProtocollo());
                sb.append(CSV_SEPARATOR);
                sb.append(row.getRigo());
                sb.append(CharPool.NEW_LINE);
            }
        }
        return sb.toString();
    }

}
