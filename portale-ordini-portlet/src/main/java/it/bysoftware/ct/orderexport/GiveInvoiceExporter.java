package it.bysoftware.ct.orderexport;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.StringBundler;

import it.bysoftware.ct.model.RigoDocumento;
import it.bysoftware.ct.model.TestataDocumento;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * This class manage the give invoice behaviour.
 * 
 * @author Mario Torrisi
 *
 */
public final class GiveInvoiceExporter extends DocumentExporter {
    
    /**
     * Create an {@link GiveInvoiceExporter}.
     */
    public GiveInvoiceExporter() {
        super();
    }

    /**
     * Creates an {@link GiveInvoiceExporter} instance with the document
     * and rows have to be exported.
     * 
     * @param header
     *            the documents is going to be exported.
     * @param list
     *            document rows.
     * @param conn
     *            database connection;
     */
    public GiveInvoiceExporter(final TestataDocumento header,
            final List<RigoDocumento> list, final Connection conn) {
        super(header, list, conn);
    }

    /* (non-Javadoc)
     * @see it.bysoftware.ct.orderexport.DocumentExporter#exportDocument()
     */
    @Override
    public String exportDocument() throws PortalException, SystemException {
        StringBundler sb = new StringBundler();
        sb.append(this.exportHeader(HEADER_COLS));
        
        if (this.getDocument() != null) {
            sb.append(GIVE_INVOICE);
            sb.append(CSV_SEPARATOR);
            sb.append(this.getDocument().getCodiceCentro());
            sb.append(CSV_SEPARATOR);
            sb.append(new SimpleDateFormat(
                    TransportDocumentExporter.DD_MM_YYYY).format(
                            this.getDocument().getDataRegistrazione()));
            sb.append(CSV_SEPARATOR);
            sb.append(this.getDocument().getCodiceFornitore());
            sb.append(CSV_SEPARATOR);
            sb.append(CSV_SEPARATOR);
            sb.append(CSV_SEPARATOR);
            sb.append(CSV_SEPARATOR);
            sb.append(CSV_SEPARATOR);
            sb.append(CSV_SEPARATOR);
            sb.append(0);
            sb.append(CSV_SEPARATOR);
            sb.append(0);
            sb.append(CSV_SEPARATOR);
            sb.append(new SimpleDateFormat(
                    TransportDocumentExporter.DD_MM_YYYY).format(
                            this.getDocument().getDataRegistrazione()));
            sb.append(CSV_SEPARATOR);
            sb.append(new SimpleDateFormat(
                    TransportDocumentExporter.DD_MM_YYYY).format(
                            this.getDocument().getDataRegistrazione()));
            sb.append(CSV_SEPARATOR);
            sb.append(this.getDocument().getProtocollo());
            sb.append(CSV_SEPARATOR);
            sb.append(this.getDocument().getLibLng1());
            sb.append(CharPool.NEW_LINE);
            sb.append(this.exportHeader(BODY_COLS));
            for (RigoDocumento row : this.getRows()) {
                sb.append(row.getTipoRigo());
                sb.append(CSV_SEPARATOR);
                if (row.getTipoRigo() == 0) {
                    sb.append(row.getCodiceArticolo());
                    sb.append(CSV_SEPARATOR);
                    sb.append(row.getCodiceVariante());
                    sb.append(CSV_SEPARATOR);
                } else {
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(row.getDescrizione());
                }
                sb.append(CSV_SEPARATOR);
                sb.append(row.getQuantita());
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(row.getPrezzo());
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                if (row.getTipoRigo() == 0) {
                    sb.append(row.getLibStr1());
                }
                sb.append(CSV_SEPARATOR);
                if (row.getTipoRigo() == 0) {
                    sb.append(row.getLibStr2());
                }
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                if (row.getTipoRigo() == 0) {
                    sb.append(row.getLibLng1());
                }
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(row.getImportoNetto());
                sb.append(CSV_SEPARATOR);
                sb.append(row.getCodiceIVA());
                sb.append(CharPool.NEW_LINE);
            }
        }
        return sb.toString();
    }

}
