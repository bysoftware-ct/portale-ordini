package it.bysoftware.ct.orderexport;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.StringBundler;

/**
 * CSVExport interface.
 * 
 * @author Mario Torrisi torrisi.mario@gmail.com
 *
 */
public abstract class CSVExporter {

    /**
     * Date format constant.
     */
    protected static final String DD_MM_YYYY = "dd/MM/yyyy";
    
    /**
     * Separator char.
     */
    protected static final String CSV_SEPARATOR = ";";
    
    /**
     * Header columns.
     */
    protected static final String[] HEADER_COLS = {
        "WorkTestataDocumento", "Tipdoc", "Codcen", "Datreg", "Codsog",
        "Codspe", "Codpor", "Codase", "Codve1", "Codve2", "Protoc", "ProtocInt",
        "Datdoc", "Datann", "Numdoc", "Liblng1",
    };
    
    /**
     * Body columns.
     */
    protected static final String[] BODY_COLS = {
        "WorkRigaDocumento", "Tiprig", "Codart", "Codvar", "Descri", "Quanet",
        "Qm2net", "Prezzo", "Scont1", "Scont2", "Scont3", "Libstr1", "Libstr2",
        "Libstr3", "Libdbl1", "Libdbl2", "Libdbl3", "Liblng1", "Liblng2",
        "Liblng3", "Libdat1", "Libdat2", "Libdat3", "Impnet", "Codiva",
    };
    
    /**
     * Exports header in CSV format.
     * 
     * @return the data exported in CSV format
     * @throws SystemException exception
     * @throws PortalException exception
     */
    public abstract String exportDocument() throws PortalException,
            SystemException;


    /**
     * Returns header columns CSV formatted.
     * 
     * @param cols
     *            columns to be exported
     * @return columns name exported
     */
    protected final String exportHeader(final String[] cols) {
        StringBundler sb = new StringBundler();
        for (String columnName : cols) {
            sb.append(columnName);
            sb.append(CSV_SEPARATOR);
        }
        sb.setIndex(sb.index() - 1);
        sb.append(CharPool.NEW_LINE);
        return sb.toString();
    }
    
    /**
     * Returns header columns CSV formatted.
     * 
     * @param cols
     *            columns to be exported
     * @return columns name exported
     */
    protected final String exportHeader(final String[] cols, String separator) {
        StringBundler sb = new StringBundler();
        for (String columnName : cols) {
            sb.append(columnName);
            sb.append(separator);
        }
        sb.setIndex(sb.index() - 1);
        sb.append(CharPool.NEW_LINE);
        return sb.toString();
    }
}
