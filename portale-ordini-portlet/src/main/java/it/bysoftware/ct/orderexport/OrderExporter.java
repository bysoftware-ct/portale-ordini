package it.bysoftware.ct.orderexport;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.util.portlet.PortletProps;

import it.bysoftware.ct.model.Articoli;
import it.bysoftware.ct.model.Ordine;
import it.bysoftware.ct.model.Piante;
import it.bysoftware.ct.model.Riga;
import it.bysoftware.ct.service.ArticoliLocalServiceUtil;
import it.bysoftware.ct.service.PianteLocalServiceUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to manage Order export.
 * 
 * @author Mario Torrisi torrisi.mario@gmail.com
 *
 */
public final class OrderExporter extends CSVExporter {

    /**
     * Default activity.
     */
    private static final String IT_PORT_CODE = PortletProps.get(
            "it-port-code");
    
    /**
     * Default activity.
     */
    private static final String FOREIGN_PORT_CODE = PortletProps.get(
            "foreign-port-code");
    
    /**
     * Default activity.
     */
    private static final String DEFAULT_ASPECT = PortletProps.get(
            "default_aspect");
    
    /**
     * Body columns.
     */
    private static final String[] BODY_COLS = {
        "WorkRigaDocumento", "Tiprig", "Codart", "Codvar", "Descri", "Quanet",
        "Qm2net", "Prezzo", "Scont1", "Scont2", "Scont3", "Libstr1", "Libstr2",
        "Libstr3", "Libdbl1", "Libdbl2", "Libdbl3", "Liblng1", "Liblng2",
        "Liblng3", "Libdat1", "Libdat2", "Libdat3", "Impnet", "Codiva",
    };

    /**
     * Order to be exported.
     */
    private Ordine order;

    /**
     * List of row.
     */
    private List<Riga> rows;

    /**
     * Create an {@link OrderExporter}.
     */
    public OrderExporter() {
        this(null, new ArrayList<Riga>());
    }

    /**
     * Creates an {@link OrderExporter} instance with the order and rows have to
     * be exported.
     * 
     * @param o
     *            the orders is going to be exported.
     * @param list
     *            order rows.
     */
    public OrderExporter(final Ordine o, final List<Riga> list) {
        super();
        this.order = o;
        this.setRows(list);
    }

    /**
     * Return the order.
     * 
     * @return the order
     */
    public Ordine getOrder() {
        return order;
    }

    /**
     * Sets the order.
     * 
     * @param o
     *            the order
     */
    public void setOrder(final Ordine o) {
        this.order = o;
    }

    /**
     * Returns row list.
     * 
     * @return the rows
     */
    public List<Riga> getRows() {
        return rows;
    }

    /**
     * Sets row list.
     * 
     * @param list
     *            the rows to set
     */
    public void setRows(final List<Riga> list) {
        this.rows = list;
    }

    /*
     * (non-Javadoc)
     * 
     * @see it.bysoftware.ct.orderexport.Exporter#exportHeader()
     */
    @Override
    public String exportDocument() throws PortalException, SystemException {
        StringBundler sb = new StringBundler();
        sb.append(this.exportHeader(HEADER_COLS));
        
        if (this.order != null) {
            sb.append("OAC");
            sb.append(CSV_SEPARATOR);
            sb.append(this.order.getCentro());
            sb.append(CSV_SEPARATOR);
            sb.append(new SimpleDateFormat(OrderExporter.DD_MM_YYYY).
                    format(this.order.getDataInserimento()));
            sb.append(CSV_SEPARATOR);
            sb.append(this.order.getIdCliente());
            sb.append(CSV_SEPARATOR);
            sb.append(CSV_SEPARATOR);
            sb.append(CSV_SEPARATOR);
            sb.append(OrderExporter.DEFAULT_ASPECT);
            sb.append(CSV_SEPARATOR);
            sb.append(this.order.getIdTrasportatore());
            sb.append(CSV_SEPARATOR);
            sb.append(this.order.getIdTrasportatore2());
            sb.append(CSV_SEPARATOR);
            sb.append(this.order.getNumero());
            sb.append(CSV_SEPARATOR);
            sb.append(this.order.getNumero());
            sb.append(CSV_SEPARATOR);
            sb.append(new SimpleDateFormat(OrderExporter.DD_MM_YYYY).
                    format(this.order.getDataInserimento()));
            sb.append(CSV_SEPARATOR);
            sb.append(new SimpleDateFormat(OrderExporter.DD_MM_YYYY).
                    format(this.order.getDataInserimento()));
            sb.append(CSV_SEPARATOR);
            sb.append(this.order.getNumero());
            sb.append(CSV_SEPARATOR);
            sb.append(this.order.getId());
            sb.append(CharPool.NEW_LINE);
            sb.append(this.exportHeader(BODY_COLS));
            for (Riga row : this.rows) {
                sb.append(0);
                sb.append(CSV_SEPARATOR);
                Piante p = PianteLocalServiceUtil.getPiante(
                        row.getIdArticolo());
                Articoli item = ArticoliLocalServiceUtil.getArticoli(
                        p.getCodice());

                sb.append(p.getCodice());
                sb.append(CSV_SEPARATOR);
                sb.append(row.getCodiceVariante());
                sb.append(CSV_SEPARATOR);
                sb.append(item.getDescrizioneDocumento());
                sb.append(CSV_SEPARATOR);
                sb.append(row.getPianteRipiano());
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(row.getPrezzo());
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                //sb.append(item.getLibStr1());
                sb.append(row.getPassaporto());
                sb.append(CSV_SEPARATOR);
                sb.append(row.getCodiceRegionale());
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                //sb.append(row.getPassaporto());
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(row.getImporto());
                sb.append(CSV_SEPARATOR);
                sb.append(this.order.getCodiceIva());
                sb.append(CharPool.NEW_LINE);
                if (row.getPassaporto() != null
                		&& !row.getPassaporto().trim().isEmpty()) {
                	sb.append(2);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append("Cod. RUOP:" + row.getCodiceRegionale()
                    		+ " - Cod. tracciabilita': " + row.getPassaporto());
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(CharPool.NEW_LINE);
				}
            }
        }
        return sb.toString();
    }

}
