package it.bysoftware.ct.orderexport;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.util.portlet.PortletProps;

import it.bysoftware.ct.model.ArticoloSocio;
import it.bysoftware.ct.model.RigoDocumento;
import it.bysoftware.ct.model.TestataDocumento;
import it.bysoftware.ct.service.ArticoloSocioLocalServiceUtil;
import it.bysoftware.ct.utils.Report;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import net.sf.jasperreports.engine.JRException;

/**
 * Class to manage Document export.
 * 
 * @author Mario Torrisi torrisi.mario@gmail.com
 *
 */
public final class TransportDocumentExporter extends CSVExporter {

    /**
     * Give document type constant.
     */
    private static final String DDM = PortletProps.get("give-document");

    /**
     * Order to be exported.
     */
    private TestataDocumento document;

    /**
     * List of row.
     */
    private List<RigoDocumento> rows;

    /**
     * Database connection used to generate report.
     */
    private Connection connection;
    
    /**
     * Report file.
     */
    private File report;

    /**
     * Create an {@link TransportDocumentExporter}.
     */
    public TransportDocumentExporter() {
        this(null, new ArrayList<RigoDocumento>(), null);
    }

    /**
     * Creates an {@link TransportDocumentExporter} instance with the document
     * and rows have to be exported.
     * 
     * @param header
     *            the documents is going to be exported.
     * @param list
     *            document rows.
     * @param conn
     *            database connection;
     */
    public TransportDocumentExporter(final TestataDocumento header,
            final List<RigoDocumento> list, final Connection conn) {
        super();
        this.connection = conn;
        this.document = header;
        this.setRows(list);
    }

    /**
     * Return the document.
     * 
     * @return the document
     */
    public TestataDocumento getDocument() {
        return this.document;
    }

    /**
     * Sets the document.
     * 
     * @param header
     *            the document
     */
    public void setOrder(final TestataDocumento header) {
        this.document = header;
    }

    /**
     * Returns row list.
     * 
     * @return the rows
     */
    public List<RigoDocumento> getRows() {
        return this.rows;
    }

    /**
     * Sets row list.
     * 
     * @param list
     *            the rows to set
     */
    public void setRows(final List<RigoDocumento> list) {
        this.rows = list;
    }

    /**
     * Returns the report file.
     * 
     * @return the report file
     */
    public File getReport() {
        return this.report;
    }

    /**
     * Sets report path file.
     * 
     * @param file
     *            the report to set
     */
    public void setReport(final File file) {
        this.report = file;
    }

    @Override
    public String exportDocument() throws PortalException, SystemException {
        StringBundler sb = new StringBundler();
        sb.append(this.exportHeader(HEADER_COLS));
        
        if (this.document != null) {
            sb.append(TransportDocumentExporter.DDM);
            sb.append(CSV_SEPARATOR);
            sb.append(this.document.getCodiceCentro());
            sb.append(CSV_SEPARATOR);
            sb.append(new SimpleDateFormat(
                    TransportDocumentExporter.DD_MM_YYYY).format(
                            this.document.getDataRegistrazione()));
            sb.append(CSV_SEPARATOR);
            sb.append(this.document.getCodiceConsorzio());
            sb.append(CSV_SEPARATOR);
            sb.append(CSV_SEPARATOR);
            sb.append(CSV_SEPARATOR);
            sb.append(CSV_SEPARATOR);
            sb.append(CSV_SEPARATOR);
            sb.append(CSV_SEPARATOR);
            sb.append(this.document.getProtocollo());
            sb.append(CSV_SEPARATOR);
            sb.append(this.document.getProtocollo());
            sb.append(CSV_SEPARATOR);
            sb.append(new SimpleDateFormat(
                    TransportDocumentExporter.DD_MM_YYYY).format(
                            this.document.getDataRegistrazione()));
            sb.append(CSV_SEPARATOR);
            sb.append(new SimpleDateFormat(
                    TransportDocumentExporter.DD_MM_YYYY).format(
                            this.document.getDataRegistrazione()));
            sb.append(CSV_SEPARATOR);
            sb.append(this.document.getProtocollo());
            sb.append(CSV_SEPARATOR);
            sb.append(this.document.getLibLng1());
            sb.append(CharPool.NEW_LINE);
            sb.append(this.exportHeader(BODY_COLS));
            for (RigoDocumento row : this.rows) {
                sb.append(row.getTipoRigo());
                sb.append(CSV_SEPARATOR);
                
                if (row.getTipoRigo() == 0) {
                    ArticoloSocio partnerItem = ArticoloSocioLocalServiceUtil.
                            findPartnerItems(row.getCodiceFornitore(),
                                    row.getCodiceArticolo(),
                                    row.getCodiceVariante());
                    if (partnerItem != null) {
                        sb.append(partnerItem.getCodiceArticoloSocio());
                        sb.append(CSV_SEPARATOR);
                        sb.append(partnerItem.getCodiceVarianteSocio());
                        sb.append(CSV_SEPARATOR);
                    } else {
                        sb.append(row.getCodiceArticolo());
                        sb.append(CSV_SEPARATOR);
                        sb.append(row.getCodiceVariante());
                        sb.append(CSV_SEPARATOR);
                    }
                } else {
                    sb.append(CSV_SEPARATOR);
                    sb.append(CSV_SEPARATOR);
                    sb.append(row.getDescrizione());
                }
                sb.append(CSV_SEPARATOR);
                sb.append(row.getQuantita());
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(row.getPrezzo());
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                if (row.getTipoRigo() == 0) {
                    sb.append(row.getLibStr1());
                }
                sb.append(CSV_SEPARATOR);
                if (row.getTipoRigo() == 0) {
                    sb.append(row.getLibStr2());
                }
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                if (row.getTipoRigo() == 0) {
                    sb.append(row.getLibLng1());
                }
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(CSV_SEPARATOR);
                sb.append(row.getImportoNetto());
                sb.append(CSV_SEPARATOR);
                sb.append(row.getCodiceIVA());
                sb.append(CharPool.NEW_LINE);
            }
        }
        return sb.toString();
    }
    
    /**
     * Creates the report file for the current transport document.
     * 
     * @param companyId
     *            the company ID
     * 
     * @throws SQLException
     *             exception
     * @throws SystemException
     *             exception
     * @throws PortalException
     *             exception
     * @throws JRException
     *             exception
     * @throws IOException eception
     */
    public void createReport(final long companyId) throws SQLException,
            PortalException, SystemException, JRException, IOException {
        Report r = new Report(this.connection);
        this.setReport(r.printTranspDoc(this.document, companyId,
                        "partner_trans-document", false));
    }

}
