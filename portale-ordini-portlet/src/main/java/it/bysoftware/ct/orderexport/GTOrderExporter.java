package it.bysoftware.ct.orderexport;

import it.bysoftware.ct.model.AnagraficheClientiFornitori;
import it.bysoftware.ct.model.Articoli;
import it.bysoftware.ct.model.Carrello;
import it.bysoftware.ct.model.ClientiFornitoriDatiAgg;
import it.bysoftware.ct.model.Ordine;
import it.bysoftware.ct.model.Piante;
import it.bysoftware.ct.model.Riga;
import it.bysoftware.ct.model.Stati;
import it.bysoftware.ct.model.Vettori;
import it.bysoftware.ct.model.VociIva;
import it.bysoftware.ct.service.ArticoliLocalServiceUtil;
import it.bysoftware.ct.service.CarrelloLocalServiceUtil;
import it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil;
import it.bysoftware.ct.service.PianteLocalServiceUtil;
import it.bysoftware.ct.service.RigaLocalServiceUtil;
import it.bysoftware.ct.service.StatiLocalServiceUtil;
import it.bysoftware.ct.service.VettoriLocalServiceUtil;
import it.bysoftware.ct.service.VociIvaLocalServiceUtil;
import it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK;
import it.bysoftware.ct.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.util.portlet.PortletProps;

public class GTOrderExporter extends CSVExporter {

	private static final String[] HEADERS = { "RecordType",
			"H-Partita IVA Fornitore", "H-Dest. Codice Punto Vendita",
			"RecordID", "H-Nr. Documento", "H-Data Documento",
			"H-Nazione Fornitore", "H-CF Fornitore", "H-Partita IVA Cliente",
			"H-NazioneCliente", "H-CF Cliente", "H-Dest. Indirizzo",
			"H-Dest. Città", "H-Dest. Data Cons. Prevista", "H-Nr. Ordine GT",
			"H-Data Ordine GT", "H-Nr. Ordine Fornitore",
			"H-Metodo di Pagamento", "H-Condizioni di Pagamento",
			"H-IBAN Pagamento", "H-Nr. Fornitore Origine",
			"H-Percentuale Commissione", "H-Costo Commissioni",
			"H-Costo Trasporto", "H-Costo Etichettatura", "H-Costo Imballi",
			"H-Nr. Carrelli CC", "H-Nr. Carrello c/Placca",
			"H-Nr. Carrelli senza Placca", "H-Nr. Pianali",
			"H-Nr. Prolunghe Corte", "H-Nr. Prolunghe Lunghe", "H-Nr. Colli",
			"H-Nr. Pallet EPAL", "H-Nr. Pallet a Perdere",
			"H-Nome Corriere Incaricato", "H-Stato", "L-Posizione", "L-EAN",
			"L-Nr. Articolo Fornitore", "L-Nr. Articolo Interno",
			"L-Descrizione", "L-Quantità", "L-Unità di Misura",
			"L-Quantità UdM", "L-Costo Lordo", "L-IVA %", "L-Sconto 1%",
			"L-Sconto 2%", "L-Sconto 3%", "L-Sconto 4%", "L-Sconto 5%",
			"L-Sconto Promo 1%", "L-Sconto Promo 2%", "L-Sconto Promo 3%",
			"L-Sconto Promo 4%", "L-Sconto Promo 5%", "L-Importo Riga",
			"L-Prezzo Etichette", "L-Quantità Etichette",
			"L-Intra Cod. Tariffa", "L-Intra Peso Lordo", "L-Intra Peso Netto",
			"L-Intra Numero Colli", "L-Passp. Nr. RUOP", "L-Passp. Lotto",
			"L-Quantità per Kit", "L-Ean Padre", "L-Codice Famiglia" };

	protected static final String CSV_SEPARATOR = ";";

	private static final String GT_RECORD_TYPE = PortletProps
			.get("gt-confirm-code");

	private static final String VAT_NUMBER = PortletProps
			.get("company-vat-number");

	private static final String COUNTRY_CODE = PortletProps
			.get("company-country-code");

	private Ordine order;

	private AnagraficheClientiFornitori customer;

	private ClientiFornitoriDatiAgg customerDetails;

	public Ordine getOrder() {
		return order;
	}

	public void setOrder(Ordine order) {
		this.order = order;
	}

	public AnagraficheClientiFornitori getCustomer() {
		return customer;
	}

	public void setCustomer(AnagraficheClientiFornitori customer) {
		this.customer = customer;
	}

	public ClientiFornitoriDatiAgg getCustomerDetails() {
		return customerDetails;
	}

	public void setCustomerDetails(ClientiFornitoriDatiAgg customerDetails) {
		this.customerDetails = customerDetails;
	}

	public GTOrderExporter(Ordine order, AnagraficheClientiFornitori customer)
			throws PortalException, SystemException {
		this.order = order;
		this.customer = customer;
		this.customerDetails = ClientiFornitoriDatiAggLocalServiceUtil
				.getClientiFornitoriDatiAgg(new ClientiFornitoriDatiAggPK(
						this.customer.getCodiceAnagrafica(), false));

	}

	@Override
	public String exportDocument() throws PortalException, SystemException {
		StringBundler sb = new StringBundler();
		sb.append(this.exportHeader(HEADERS, CSV_SEPARATOR));

		List<Carrello> carts = CarrelloLocalServiceUtil
				.findCartsInOrder(this.order.getId());
		Map<String, GTRow> rows = new HashMap<String, GTRow>();
		for (Carrello c : carts) {
			List<Riga> r = RigaLocalServiceUtil.findRowsInCart(c.getId());
			for (Riga riga : r) {
				Piante p = PianteLocalServiceUtil.getPiante(riga
						.getIdArticolo());
				Articoli a = ArticoliLocalServiceUtil
						.getArticoli(p.getCodice());
				String k = p.getCodice() + "#" + riga.getCodiceVariante();
				VociIva v = VociIvaLocalServiceUtil
						.getVociIva(a.getCodiceIVA());
				if (rows.keySet().contains(k)) {
					GTRow row = rows.get(k);
					row.setLQuantita(row.getLQuantita()
							+ riga.getPianteRipiano());
					row.setLImportoRiga(row.getLImportoRiga()
							+ riga.getImporto());
				} else {
					double grossPrice = (Utils.isLastChar('#', k)) ? a
							.getPrezzo1() : a.getPrezzo2();
					GTRow gtRow = new GTRow(riga.getEan(),
							a.getCodiceArticolo(), "", a.getDescrizione()
									+ " vaso: " + p.getVaso(),
							riga.getPianteRipiano(), "", "1", grossPrice,
							v.getAliquota(), riga.getSconto(), 0, 0, 0, 0, 0,
							0, 0, 0, 0, riga.getImporto(),
							riga.getPrezzoEtichetta(), 0, "", 0, 0, 0,
							riga.getCodiceRegionale(), riga.getPassaporto(), 0,
							"", "");
					rows.put(k, gtRow);
				}
			}
		}
		String orderHeader = this.exportDocHeader(carts.size());
		int pos = 1;
		for (String k : rows.keySet()) {
			sb.append(orderHeader);
			rows.get(k).setLPosizione(pos);
			sb.append(rows.get(k).toCSV());
			sb.append(CharPool.NEW_LINE);
			pos++;
		}

		return sb.toString();
	}

	private String exportDocHeader(int carts) throws PortalException,
			SystemException {
		Stati s = StatiLocalServiceUtil.getStati(customer.getSiglaStato());
		StringBundler sb = new StringBundler();
		sb.append(GT_RECORD_TYPE);
		sb.append(CSV_SEPARATOR);
		sb.append(VAT_NUMBER);
		sb.append(CSV_SEPARATOR);
		sb.append(Utils.escapeCsv(
				this.customerDetails.getCodicePuntoVenditaGT(), CSV_SEPARATOR));
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(this.order.getNumero() + "/" + this.order.getCentro());
		sb.append(CSV_SEPARATOR);
		sb.append(new SimpleDateFormat(OrderExporter.DD_MM_YYYY)
				.format(this.order.getDataInserimento()));
		sb.append(CSV_SEPARATOR);
		sb.append(COUNTRY_CODE);
		sb.append(CSV_SEPARATOR);
		sb.append(VAT_NUMBER);
		sb.append(CSV_SEPARATOR);
		sb.append(this.customer.getPartitaIVA());
		sb.append(CSV_SEPARATOR);
		sb.append(s.getCodiceISO());
		sb.append(CSV_SEPARATOR);
		sb.append(this.customer.getCodiceFiscale());
		sb.append(CSV_SEPARATOR);
		sb.append(Utils.escapeCsv(this.customer.getIndirizzo(), CSV_SEPARATOR));
		sb.append(CSV_SEPARATOR);
		sb.append(Utils.escapeCsv(this.customer.getComune(), CSV_SEPARATOR));
		sb.append(CSV_SEPARATOR);
		sb.append(new SimpleDateFormat(OrderExporter.DD_MM_YYYY)
				.format(this.order.getDataConsegna()));
		sb.append(CSV_SEPARATOR);
		sb.append(this.order.getNumOrdineGT());
		sb.append(CSV_SEPARATOR);
		sb.append(new SimpleDateFormat(OrderExporter.DD_MM_YYYY)
				.format(this.order.getDataOrdineGT()));
		sb.append(CSV_SEPARATOR);
		sb.append(this.order.getNumero() + "/" + this.order.getCentro());
		sb.append(CSV_SEPARATOR);
		sb.append(2);
		sb.append(CSV_SEPARATOR);
		sb.append(4);
		sb.append(CSV_SEPARATOR);
		sb.append(customerDetails.getCodiceIBAN());
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(carts);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		Vettori v = VettoriLocalServiceUtil.getVettori(this.order
				.getIdTrasportatore());
		sb.append(Utils.escapeCsv(
				v.getRagioneSociale() + " " + v.getRagioneSociale2(),
				CSV_SEPARATOR));
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		return sb.toString();
	}

}
