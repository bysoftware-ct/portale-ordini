package it.bysoftware.ct.orderexport;

import it.bysoftware.ct.model.AnagraficheClientiFornitori;
import it.bysoftware.ct.model.Articoli;
import it.bysoftware.ct.model.ClientiFornitoriDatiAgg;
import it.bysoftware.ct.model.Ordine;
import it.bysoftware.ct.model.Piante;
import it.bysoftware.ct.model.RigheFattureVedita;
import it.bysoftware.ct.model.Stati;
import it.bysoftware.ct.model.TestataFattureClienti;
import it.bysoftware.ct.model.Vettori;
import it.bysoftware.ct.model.VociIva;
import it.bysoftware.ct.service.AnagraficheClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.ArticoliLocalServiceUtil;
import it.bysoftware.ct.service.CarrelloLocalServiceUtil;
import it.bysoftware.ct.service.ClientiFornitoriDatiAggLocalServiceUtil;
import it.bysoftware.ct.service.OrdineLocalServiceUtil;
import it.bysoftware.ct.service.PianteLocalServiceUtil;
import it.bysoftware.ct.service.StatiLocalServiceUtil;
import it.bysoftware.ct.service.VettoriLocalServiceUtil;
import it.bysoftware.ct.service.VociIvaLocalServiceUtil;
import it.bysoftware.ct.service.persistence.ClientiFornitoriDatiAggPK;
import it.bysoftware.ct.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.util.portlet.PortletProps;

public class GTInvoiceExporter extends CSVExporter {

	private static final String LABEL_CODE = PortletProps.get("label_code");
	private static final String RUOP = PortletProps.get("ruop");
	private static final String TRACEABILITY = PortletProps.get("traceability");
	private static final String GT_RECORD_TYPE = PortletProps
			.get("gt-invoice-code");
	private static final String VAT_NUMBER = PortletProps
			.get("company-vat-number");

	private static final String COUNTRY_CODE = PortletProps
			.get("company-country-code");
	protected static final String CSV_SEPARATOR = ";";

	private static final String[] HEADERS = { "RecordType",
			"H-Partita IVA Fornitore", "H-Dest. Codice Punto Vendita",
			"RecordID", "H-Nr. Documento", "H-Data Documento",
			"H-Nazione Fornitore", "H-CF Fornitore", "H-Partita IVA Cliente",
			"H-NazioneCliente", "H-CF Cliente", "H-Dest. Indirizzo",
			"H-Dest. Città", "H-Dest. Data Cons. Prevista", "H-Nr. Ordine GT",
			"H-Data Ordine GT", "H-Nr. Ordine Fornitore",
			"H-Metodo di Pagamento", "H-Condizioni di Pagamento",
			"H-IBAN Pagamento", "H-Nr. Fornitore Origine",
			"H-Percentuale Commissione", "H-Costo Commissioni",
			"H-Costo Trasporto", "H-Costo Etichettatura", "H-Costo Imballi",
			"H-Nr. Carrelli CC", "H-Nr. Carrello c/Placca",
			"H-Nr. Carrelli senza Placca", "H-Nr. Pianali",
			"H-Nr. Prolunghe Corte", "H-Nr. Prolunghe Lunghe", "H-Nr. Colli",
			"H-Nr. Pallet EPAL", "H-Nr. Pallet a Perdere",
			"H-Nome Corriere Incaricato", "H-Stato", "L-Posizione", "L-EAN",
			"L-Nr. Articolo Fornitore", "L-Nr. Articolo Interno",
			"L-Descrizione", "L-Quantità", "L-Unità di Misura",
			"L-Quantità UdM", "L-Costo Lordo", "L-IVA %", "L-Sconto 1%",
			"L-Sconto 2%", "L-Sconto 3%", "L-Sconto 4%", "L-Sconto 5%",
			"L-Sconto Promo 1%", "L-Sconto Promo 2%", "L-Sconto Promo 3%",
			"L-Sconto Promo 4%", "L-Sconto Promo 5%", "L-Importo Riga",
			"L-Prezzo Etichette", "L-Quantità Etichette",
			"L-Intra Cod. Tariffa", "L-Intra Peso Lordo", "L-Intra Peso Netto",
			"L-Intra Numero Colli", "L-Passp. Nr. RUOP", "L-Passp. Lotto",
			"L-Quantità per Kit", "L-Ean Padre", "L-Codice Famiglia" };

	private TestataFattureClienti invoice;

	private List<RigheFattureVedita> rows;

	private AnagraficheClientiFornitori customer;
	private ClientiFornitoriDatiAgg customerDetails;
	private Ordine order;

	public TestataFattureClienti getInvoice() {
		return invoice;
	}

	public void setInvoice(TestataFattureClienti invoice) {
		this.invoice = invoice;
	}

	public List<RigheFattureVedita> getRows() {
		return rows;
	}

	public void setRows(List<RigheFattureVedita> rows) {
		this.rows = rows;
	}

	public AnagraficheClientiFornitori getCustomer() {
		return customer;
	}

	public void setCustomer(AnagraficheClientiFornitori customer) {
		this.customer = customer;
	}

	public ClientiFornitoriDatiAgg getCustomerDetails() {
		return customerDetails;
	}

	public void setCustomerDetails(ClientiFornitoriDatiAgg customerDetails) {
		this.customerDetails = customerDetails;
	}

	public Ordine getOrder() {
		return order;
	}

	public void setOrder(Ordine order) {
		this.order = order;
	}

	public GTInvoiceExporter() {
		super();
	}

	public GTInvoiceExporter(TestataFattureClienti invoice,
			List<RigheFattureVedita> rows) throws SystemException {
		super();
		this.invoice = invoice;
		this.rows = rows;
		this.customer = AnagraficheClientiFornitoriLocalServiceUtil
				.fetchAnagraficheClientiFornitori(invoice.getCodiceCliente());
		this.customerDetails = ClientiFornitoriDatiAggLocalServiceUtil
				.fetchClientiFornitoriDatiAgg(new ClientiFornitoriDatiAggPK(
						invoice.getCodiceCliente(), false));
		this.order = OrdineLocalServiceUtil.fetchOrdine(this.invoice
				.getLibLng1());
	}

	public GTInvoiceExporter(TestataFattureClienti invoice,
			List<RigheFattureVedita> rows, AnagraficheClientiFornitori customer) {
		super();
		this.invoice = invoice;
		this.rows = rows;
		this.customer = customer;
	}

	public GTInvoiceExporter(TestataFattureClienti invoice,
			List<RigheFattureVedita> rows,
			AnagraficheClientiFornitori customer,
			ClientiFornitoriDatiAgg customerDetails) {
		super();
		this.invoice = invoice;
		this.rows = rows;
		this.customer = customer;
		this.customerDetails = customerDetails;
	}

	@Override
	public final String exportDocument() throws PortalException,
			SystemException {
		StringBundler sb = new StringBundler();
		sb.append(this.exportHeader(HEADERS, CSV_SEPARATOR));
		String invoiceHeader = this.exportInvoiceHeader();
		GTRow gtRow;
		VociIva vat;
		Articoli label = ArticoliLocalServiceUtil.fetchArticoli(LABEL_CODE);
		double labelPrice = label != null ? label.getPrezzo1() : 0.0;
		int pos = 1;
		for (int i = 0; i < this.rows.size(); i++) {
			RigheFattureVedita invoiceRow = this.rows.get(i);
			if (!invoiceRow.getCodiceArticolo().equals(LABEL_CODE)) {
				Piante p = PianteLocalServiceUtil.findPlants(invoiceRow
						.getCodiceArticolo());
				RigheFattureVedita phytoInfoRow = null;
				String[] phytoInfo = new String[] { "", "" };
				if (p != null && i++ < this.rows.size()) {
					phytoInfoRow = this.rows.get(i);
					phytoInfo = this
							.extractPhytoInfo(phytoInfoRow.getDescrizione());
				}
				vat = VociIvaLocalServiceUtil.fetchVociIva(invoiceRow
						.getCodiceIVAFatturazione());
				
				gtRow = new GTRow(p.getEan(), invoiceRow.getCodiceArticolo(), "",
						invoiceRow.getDescrizione(), invoiceRow.getQuantita(), "",
						"1", invoiceRow.getPrezzo(),
						vat != null ? vat.getAliquota() : 0.0,
						invoiceRow.getSconto1(), invoiceRow.getSconto2(),
						invoiceRow.getSconto3(), 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
						invoiceRow.getImportoNetto(), labelPrice,
						(int) invoiceRow.getQuantita(), "", 0.0, 0.0, 0,
						phytoInfo[0], phytoInfo[1], 0, "", "");
				sb.append(invoiceHeader);
				gtRow.setLPosizione(pos);
				sb.append(gtRow.toCSV());
				sb.append(CharPool.NEW_LINE);
				pos++;
			}
		}

		return sb.toString();
	}

	private String[] extractPhytoInfo(String string) {

		String ruopPattern = GTInvoiceExporter.RUOP
				+ "\\s*([^\\-]+(?:-[^\\s]+)*)";
		String tracciabilitaPattern = GTInvoiceExporter.TRACEABILITY
				+ "\\s*(.+)";

		// Create Pattern objects
		Pattern ruopPatternObj = Pattern.compile(ruopPattern);
		Pattern tracciabilitaPatternObj = Pattern.compile(tracciabilitaPattern);

		// Create Matcher objects
		Matcher ruopMatcher = ruopPatternObj.matcher(string);
		Matcher tracciabilitaMatcher = tracciabilitaPatternObj.matcher(string);

		// Find the matches and extract the values
		if (ruopMatcher.find() && tracciabilitaMatcher.find()) {
			return new String[] { ruopMatcher.group(1).trim(),
					tracciabilitaMatcher.group(1).trim() };
		} else {
			return new String[] { "", "" };
		}
	}

	private final String exportInvoiceHeader() throws SystemException,
			PortalException {
		Stati s = StatiLocalServiceUtil.getStati(customer.getSiglaStato());

		int cartsNum;
		if (this.order == null) {
			cartsNum = 0;
		} else {
			cartsNum = CarrelloLocalServiceUtil.findCartsInOrder(
					this.order.getId()).size();
		}

		StringBundler sb = new StringBundler();
		sb.append(GT_RECORD_TYPE);
		sb.append(CSV_SEPARATOR);
		sb.append(Utils.escapeCsv(VAT_NUMBER, CSV_SEPARATOR));
		sb.append(CSV_SEPARATOR);
		sb.append(Utils.escapeCsv(
				this.customerDetails.getCodicePuntoVenditaGT(), CSV_SEPARATOR));
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(this.invoice.getNumeroDocumento() + "/"
				+ this.invoice.getCodiceCentro());
		sb.append(CSV_SEPARATOR);
		sb.append(new SimpleDateFormat(OrderExporter.DD_MM_YYYY)
				.format(this.invoice.getDataDocumento()));
		sb.append(CSV_SEPARATOR);
		sb.append(COUNTRY_CODE);
		sb.append(CSV_SEPARATOR);
		sb.append(VAT_NUMBER);
		sb.append(CSV_SEPARATOR);
		sb.append(Utils.escapeCsv(this.customer.getPartitaIVA(), CSV_SEPARATOR));
		sb.append(CSV_SEPARATOR);
		sb.append(s.getCodiceISO());
		sb.append(CSV_SEPARATOR);
		sb.append(Utils.escapeCsv(this.customer.getCodiceFiscale(),
				CSV_SEPARATOR));
		sb.append(CSV_SEPARATOR);
		sb.append(Utils.escapeCsv(this.customer.getIndirizzo(), CSV_SEPARATOR));
		sb.append(CSV_SEPARATOR);
		sb.append(Utils.escapeCsv(this.customer.getComune(), CSV_SEPARATOR));
		sb.append(CSV_SEPARATOR);
		sb.append(new SimpleDateFormat(OrderExporter.DD_MM_YYYY).format(order
				.getDataConsegna()));
		sb.append(CSV_SEPARATOR);
		sb.append(Utils.escapeCsv(this.order.getNumOrdineGT(), CSV_SEPARATOR));
		sb.append(CSV_SEPARATOR);
		sb.append(new SimpleDateFormat(OrderExporter.DD_MM_YYYY)
				.format(this.order.getDataOrdineGT()));
		sb.append(CSV_SEPARATOR);
		sb.append(this.order.getNumero() + "/" + this.order.getCentro());
		sb.append(CSV_SEPARATOR);
		sb.append(2);
		sb.append(CSV_SEPARATOR);
		sb.append(4);
		sb.append(CSV_SEPARATOR);
		sb.append(customerDetails.getCodiceIBAN());
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(cartsNum);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		Vettori v = VettoriLocalServiceUtil.getVettori(order
				.getIdTrasportatore());
		sb.append(Utils.escapeCsv(
				v.getRagioneSociale() + " " + v.getRagioneSociale2(),
				CSV_SEPARATOR));
		sb.append(CSV_SEPARATOR);
		sb.append(CSV_SEPARATOR);
		return sb.toString();

	}
}
