SELECT     dbo._vuoti.nome, dbo._vuoti.codice, dbo._movimenti_vuoti.data_movimento, dbo._movimenti_vuoti.codice_sogetto, dbo._movimenti_vuoti.quantita, dbo._movimenti_vuoti.tipo_movimento, 
                      dbo._movimenti_vuoti.note
FROM         dbo._vuoti LEFT OUTER JOIN
                      dbo._movimenti_vuoti ON dbo._vuoti.codice = dbo._movimenti_vuoti.codice_vuoto