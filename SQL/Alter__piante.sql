ALTER TABLE _piante ALTER COLUMN scheda_pubblica VARCHAR(255)
ALTER TABLE _piante ADD obsoleto TINYINT NOT NULL DEFAULT (0)
-- 26/03/2018 Change id_fornitore from INT to VARCHAR(5)
ALTER TABLE _piante DROP CONSTRAINT DF___piante__id_forn__583DC9EE
ALTER TABLE _piante ALTER COLUMN id_fornitore VARCHAR(5) NULL
ALTER TABLE _piante ADD DEFAULT '' FOR id_fornitore
-- 25/10/2021 Add columns for new features
ALTER TABLE _piante ADD prezzo_etichetta DECIMAL(10,2)
ALTER TABLE _piante ADD passaporto_default NVARCHAR(50)