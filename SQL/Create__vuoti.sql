CREATE TABLE [dbo].[_vuoti](
    [codice] [nvarchar](10) NOT NULL,
    [nome] [nvarchar](50) NOT NULL,
    [descrizione] [nvarchar](255) NOT NULL,
    [unita_misura] [nvarchar](3) NOT NULL,
    [note] [text] NOT NULL,
    [foto] [int] NULL,
 CONSTRAINT [PK_Vuoti] PRIMARY KEY CLUSTERED 
(
    [codice] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]