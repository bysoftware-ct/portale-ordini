SELECT     dbo._TestateDocumenti.RfiAnno, dbo._TestateDocumenti.RfiCodatt, dbo._TestateDocumenti.RfiCodcen, dbo._TestateDocumenti.RfiCoddep, dbo._TestateDocumenti.RfiProtoc, 
                      dbo._TestateDocumenti.RfiTipdoc, dbo._TestateDocumenti.RfiCodiaz, dbo._TestateDocumenti.RfiDatreg, dbo._TestateDocumenti.RfiCodpag, dbo._TestateDocumenti.RfiCodpor, 
                      dbo._TestateDocumenti.RfiCodase, dbo._TestateDocumenti.RfiCodfor, dbo.AnagraficheClientiFornitori.RacCap, dbo.AnagraficheClientiFornitori.RacComune, dbo.AnagraficheClientiFornitori.RacIndiri, 
                      dbo.AnagraficheClientiFornitori.RacPariva, dbo.AnagraficheClientiFornitori.RacRagso1, dbo.AnagraficheClientiFornitori.RacRagso2, dbo.AnagraficheClientiFornitori.RacSigpro, 
                      dbo.AnagraficheClientiFornitori.RacSigsta, dbo.Articoli.RanCodart, ISNULL(dbo.Articoli.RanDesdoc, dbo._RigheDocumenti.RbfDescri) AS RbfDescri, dbo._RigheDocumenti.RbfLibstr1, 
                      dbo._RigheDocumenti.RbfLibstr2, dbo._RigheDocumenti.RbfLiblng1, dbo._RigheDocumenti.RbfQuanti, dbo._RigheDocumenti.RbfPrezzo, dbo._RigheDocumenti.RbfScont1, 
                      dbo._RigheDocumenti.RbfScont2, dbo._RigheDocumenti.RbfScont3, dbo._RigheDocumenti.RbfImpnet, dbo._RigheDocumenti.RbfTiprig, dbo.VociIva.RiaCodiva, dbo.VociIva.RiaDescri, 
                      dbo.VociIva.RiaAliquo, dbo.Porto.RdeDescri AS porto, dbo.AspettoEsterioreBeni.RdeDescri AS aspetto, dbo.CuraTrasporto.RdeDescri AS cura, dbo.Spedizioni.RdeDescri AS spedizione, 
                      dbo.Vettori.RavAlbo, dbo.Vettori.RavCap, dbo.Vettori.RavCodfis, dbo.Vettori.RavIndiri, dbo.Vettori.RavPariva, dbo.Vettori.RavRagso1, dbo.Vettori.RavSigpro, dbo.Vettori.RavSigsta, 
                      dbo.Pagamenti.RpaDescri, dbo.View_PO_TestataConferma.numero, dbo.View_PO_TestataConferma.centro, dbo._TestateDocumenti.RfiLibstr1, dbo._TestateDocumenti.RfiLibstr2, 
                      dbo.AnagraficheClientiFornitori.RacCodfis, dbo.ClientiFornitoriDatiAgg.RveLibStr1, dbo.ClientiFornitoriDatiAgg.RveLibStr2, dbo.View_PO_TestataConferma.luogo_carico, dbo.Vettori.RavComune, 
                      dbo.Vettori.RavRagso2, dbo.Articoli.RanPeso, dbo.Articoli.RanPesoLordo, dbo.CausaleTrasporto.RdeDescri AS causale_tasporto, dbo.VociIva.RiaDesdoc
FROM         dbo.View_PO_TestataConferma INNER JOIN
                      dbo._RigheDocumenti INNER JOIN
                      dbo._TestateDocumenti ON dbo._RigheDocumenti.RbfAnno = dbo._TestateDocumenti.RfiAnno AND dbo._RigheDocumenti.RbfCodatt = dbo._TestateDocumenti.RfiCodatt AND 
                      dbo._RigheDocumenti.RbfCodcen = dbo._TestateDocumenti.RfiCodcen AND dbo._RigheDocumenti.RbfCoddep = dbo._TestateDocumenti.RfiCoddep AND 
                      dbo._RigheDocumenti.RbfCodfor = dbo._TestateDocumenti.RfiCodfor AND dbo._RigheDocumenti.RbfProtoc = dbo._TestateDocumenti.RfiProtoc AND 
                      dbo._RigheDocumenti.RbfTipdoc = dbo._TestateDocumenti.RfiTipdoc INNER JOIN
                      dbo.AnagraficheClientiFornitori ON dbo._TestateDocumenti.RfiCodfor = dbo.AnagraficheClientiFornitori.RacCodana ON 
                      dbo.View_PO_TestataConferma.id = dbo._TestateDocumenti.RfiLiblng1 INNER JOIN
                      dbo.ClientiFornitoriDatiAgg ON dbo.AnagraficheClientiFornitori.RacCodana = dbo.ClientiFornitoriDatiAgg.RveCodclf LEFT OUTER JOIN
                      dbo.Articoli ON dbo._RigheDocumenti.RbfCodart = dbo.Articoli.RanCodart LEFT OUTER JOIN
                      dbo.VociIva ON dbo.VociIva.RiaCodiva = dbo._RigheDocumenti.RbfCodiva LEFT OUTER JOIN
                      dbo.Porto ON dbo._TestateDocumenti.RfiCodpor = dbo.Porto.RdeCoddes LEFT OUTER JOIN
                      dbo.AspettoEsterioreBeni ON dbo._TestateDocumenti.RfiCodase = dbo.AspettoEsterioreBeni.RdeCoddes LEFT OUTER JOIN
                      dbo.CuraTrasporto ON dbo._TestateDocumenti.RfiCodtrd = dbo.CuraTrasporto.RdeCoddes LEFT OUTER JOIN
                      dbo.Spedizioni ON dbo._TestateDocumenti.RfiCodspe = dbo.Spedizioni.RdeCoddes LEFT OUTER JOIN
                      dbo.Vettori ON dbo._TestateDocumenti.RfiCodve1 = dbo.Vettori.RavCodana LEFT OUTER JOIN
                      dbo.Pagamenti ON dbo._TestateDocumenti.RfiCodpag = dbo.Pagamenti.RpaCodpag LEFT OUTER JOIN
                      dbo.CausaleTrasporto ON dbo._TestateDocumenti.RfiCodtrc = dbo.CausaleTrasporto.RdeCoddes
WHERE     (dbo.ClientiFornitoriDatiAgg.RveLibLng1 = 1) AND (dbo.ClientiFornitoriDatiAgg.RveTipo = 1)
