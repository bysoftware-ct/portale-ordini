CREATE TABLE [dbo].[_contatori_soci](
    [anno] [int] NOT NULL,
    [codice_socio] [nvarchar](5) NOT NULL,
    [tipo_documento] [nvarchar](3) NOT NULL,
    [numero] [int] NOT NULL,
 CONSTRAINT [PK_ContatoriSoci] PRIMARY KEY CLUSTERED 
(
    [anno] ASC,
    [codice_socio] ASC,
    [tipo_documento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
