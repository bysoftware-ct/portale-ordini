SELECT     dbo._pagamenti.codice_soggetto, dbo._pagamenti.stato, dbo.AnagraficheClientiFornitori.RacRagso1, dbo.AnagraficheClientiFornitori.RacRagso2, dbo.FattureFornitoriTestate.Rt2Codatt, 
                      dbo.FattureFornitoriTestate.Rt2Codcen, dbo.FattureFornitoriTestate.Rt2Numdoc, dbo.FattureFornitoriTestate.Rt2Datdoc, ISNULL(dbo.FattureFornitoriTestate.Rt2Imponi1, 0) AS Rt2Imponi1, 
                      ISNULL(dbo.FattureFornitoriTestate.Rt2Iva1, 0) AS Rt2Iva1, dbo.FattureClientiTestate.Rt1Anno, dbo.FattureClientiTestate.Rt1Codatt, dbo.FattureClientiTestate.Rt1Codcen, 
                      dbo.FattureClientiTestate.Rt1Numdoc, dbo.FattureClientiTestate.Rt1Datdoc, ISNULL(dbo.FattureClientiTestate.Rt1Imponi1, 0) AS Rt1Imponi1, ISNULL(dbo.FattureClientiTestate.Rt1Iva1, 0) AS Rt1Iva1, 
                      dbo._schede_pagamenti.differenza, dbo._schede_pagamenti.RjaTipsog, dbo.ScadenzePartite.RjaTotsca - dbo.ScadenzePartite.RjaImppag AS residuo_dare, 
                      ScadenzePartite_1.RjaTotsca - ScadenzePartite_1.RjaImppag AS residuo_avere
FROM         dbo._pagamenti INNER JOIN
                      dbo._schede_pagamenti ON dbo._pagamenti.anno = dbo._schede_pagamenti.anno_pagamento AND dbo._pagamenti.id = dbo._schede_pagamenti.id_pagamento INNER JOIN
                      dbo.ScadenzePartite ON dbo._schede_pagamenti.RjaAnno = dbo.ScadenzePartite.RjaAnnpar AND dbo._schede_pagamenti.RjaCodsog = dbo.ScadenzePartite.RjaCodsog AND 
                      dbo._schede_pagamenti.RjaNumpar = dbo.ScadenzePartite.RjaNumpar AND dbo._schede_pagamenti.RjaNumsca = dbo.ScadenzePartite.RjaNumsca AND 
                      dbo._schede_pagamenti.RjaTipsog = dbo.ScadenzePartite.RjaTipsog INNER JOIN
                      dbo.FattureFornitoriTestate ON dbo.ScadenzePartite.RjaAnnpar = dbo.FattureFornitoriTestate.Rt2Anno AND dbo.ScadenzePartite.RjaCodatt = dbo.FattureFornitoriTestate.Rt2Codatt AND 
                      dbo.ScadenzePartite.RjaCodcen = dbo.FattureFornitoriTestate.Rt2Codcen AND dbo.ScadenzePartite.RjaCodsog = dbo.FattureFornitoriTestate.Rt2Codfor AND 
                      dbo.ScadenzePartite.RjaDocpro = dbo.FattureFornitoriTestate.Rt2Nuprve INNER JOIN
                      dbo.AnagraficheClientiFornitori ON dbo._pagamenti.codice_soggetto = dbo.AnagraficheClientiFornitori.RacCodana AND 
                      dbo._schede_pagamenti.RjaCodsog = dbo.AnagraficheClientiFornitori.RacCodana LEFT OUTER JOIN
                      dbo._schede_pagamenti AS _schede_pagamenti_1 ON dbo._schede_pagamenti.RjaTipsog_comp = _schede_pagamenti_1.RjaTipsog AND 
                      dbo._schede_pagamenti.RjaAnno_comp = _schede_pagamenti_1.RjaAnno AND dbo._schede_pagamenti.RjaCodSog_comp = _schede_pagamenti_1.RjaCodsog AND 
                      dbo._schede_pagamenti.RjaNumpar_comp = _schede_pagamenti_1.RjaNumpar AND dbo._schede_pagamenti.RjaNumsca_comp = _schede_pagamenti_1.RjaNumsca LEFT OUTER JOIN
                      dbo.ScadenzePartite AS ScadenzePartite_1 ON _schede_pagamenti_1.RjaAnno = ScadenzePartite_1.RjaAnnpar AND _schede_pagamenti_1.RjaCodsog = ScadenzePartite_1.RjaCodsog AND 
                      _schede_pagamenti_1.RjaNumpar = ScadenzePartite_1.RjaNumpar AND _schede_pagamenti_1.RjaNumsca = ScadenzePartite_1.RjaNumsca AND 
                      _schede_pagamenti_1.RjaTipsog = ScadenzePartite_1.RjaTipsog LEFT OUTER JOIN
                      dbo.FattureClientiTestate ON ScadenzePartite_1.RjaAnnpar = dbo.FattureClientiTestate.Rt1Anno AND ScadenzePartite_1.RjaCodatt = dbo.FattureClientiTestate.Rt1Codatt AND 
                      ScadenzePartite_1.RjaCodcen = dbo.FattureClientiTestate.Rt1Codcen AND ScadenzePartite_1.RjaCodsog = dbo.FattureClientiTestate.Rt1Codcli AND 
                      ScadenzePartite_1.RjaDocpro = dbo.FattureClientiTestate.Rt1Numdoc