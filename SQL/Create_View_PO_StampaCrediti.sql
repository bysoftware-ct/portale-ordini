CREATE VIEW [dbo].[View_PO_StampaCrediti]
AS
SELECT     dbo._spettanze_soci.importo, ISNULL(dbo.FattureClientiTestate.Rt1Numdoc, - 1) AS Numdoc, ISNULL(dbo.FattureClientiTestate.Rt1Codcen, '') AS Codcen, 
                      ISNULL(dbo.AnagraficheClientiFornitori.RacRagso1, '') AS RagSo1, ISNULL(dbo.AnagraficheClientiFornitori.RacRagso2, '') AS Ragso2, dbo._spettanze_soci.data_calcolo, 
                      dbo._spettanze_soci.codice_fornitore
FROM         dbo._spettanze_soci LEFT OUTER JOIN
                      dbo.FattureClientiTestate ON dbo._spettanze_soci.Rt1Anno = dbo.FattureClientiTestate.Rt1Anno AND dbo._spettanze_soci.Rt1Codatt = dbo.FattureClientiTestate.Rt1Codatt AND 
                      dbo._spettanze_soci.Rt1Codcen = dbo.FattureClientiTestate.Rt1Codcen AND dbo._spettanze_soci.Rt1Nuprve = dbo.FattureClientiTestate.Rt1Nuprve LEFT OUTER JOIN
                      dbo.AnagraficheClientiFornitori ON dbo.FattureClientiTestate.Rt1Codcli = dbo.AnagraficheClientiFornitori.RacCodana
