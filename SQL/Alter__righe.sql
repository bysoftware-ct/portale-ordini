ALTER TABLE _righe ADD s2 SMALLINT NOT NULL DEFAULT (0),
    s3 SMALLINT NOT NULL DEFAULT (0),
    s4 SMALLINT NOT NULL DEFAULT (0),
    sconto FLOAT NOT NULL DEFAULT(0),
    prezzo_fornitore FLOAT NOT NULL DEFAULT (0);
-- 20/11/2021 gestione prezzo etichetta
ALTER TABLE _righe ADD prezzo_etichetta FLOAT NOT NULL DEFAULT (0);
-- 03/04/2022 codice a barre modificabile
ALTER TABLE _righe ADD ean VARCHAR(13) NOT NULL DEFAULT ('');
-- 09/08/2023 notifica solo fornitori coinvolti
ALTER TABLE _righe ADD modificato TINYINT NOT NULL DEFAULT (0);
-- 01/11/2024 merce ricevuta
ALTER TABLE _righe ADD ricevuto TINYINT NOT NULL DEFAULT (0);