SELECT     dbo.AnagraficheClientiFornitori.RacCodana, dbo.AnagraficheClientiFornitori.RacRagso1, dbo._movimenti_vuoti.data_movimento, dbo._movimenti_vuoti.tipo_movimento, 
                      dbo._movimenti_vuoti.quantita, dbo._movimenti_vuoti.note, dbo._vuoti.nome, dbo._vuoti.unita_misura
FROM         dbo._vuoti INNER JOIN
                      dbo._movimenti_vuoti ON dbo._vuoti.codice = dbo._movimenti_vuoti.codice_vuoto INNER JOIN
                      dbo.AnagraficheClientiFornitori ON dbo._movimenti_vuoti.codice_sogetto = dbo.AnagraficheClientiFornitori.RacCodana
