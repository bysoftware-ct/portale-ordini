SELECT     TOP (100) PERCENT dbo._RigheDocumenti.RbfAnno, dbo._RigheDocumenti.RbfCodatt, dbo._RigheDocumenti.RbfCodcen, dbo._RigheDocumenti.RbfCoddep, dbo._RigheDocumenti.RbfProtoc, 
                      dbo._RigheDocumenti.RbfCodfor, dbo._RigheDocumenti.RbfTipdoc, dbo.VociIva.RiaCodiva, dbo.VociIva.RiaAliquo, dbo.VociIva.RiaDescri, dbo._RigheDocumenti.RbfImpnet
FROM         dbo._RigheDocumenti INNER JOIN
                      dbo.VociIva ON dbo._RigheDocumenti.RbfCodiva = dbo.VociIva.RiaCodiva