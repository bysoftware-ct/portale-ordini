SELECT        dbo._ordini.id_cliente, dbo.AnagraficheClientiFornitori.RacRagso1, dbo.AnagraficheClientiFornitori.RacRagso2, dbo.Vettori.RavRagso1, dbo._tipi_carrello.tipologia, dbo._ordini.luogo_carico, dbo._ordini.note_cliente, 
                         dbo._ordini.note, dbo._ordini.data_ins, dbo._ordini.data_consegna_merce, dbo._ordini.id, dbo._ordini.importo, dbo.ClientiFornitoriDatiAgg.RveEsenzi, dbo.VociIva.RiaCodiva, dbo.VociIva.RiaDescri, dbo.VociIva.RiaAliquo, 
                         dbo.AnagraficheClientiFornitori.RacPariva, dbo.AnagraficheClientiFornitori.RacIndiri, dbo.AnagraficheClientiFornitori.RacComune, dbo.AnagraficheClientiFornitori.RacCap, dbo.AnagraficheClientiFornitori.RacSigsta, 
                         dbo.AnagraficheClientiFornitori.RacSigpro, dbo.Vettori.RavCap, dbo.Vettori.RavComune, dbo.Vettori.RavIndiri, dbo.Vettori.RavPariva, dbo.Vettori.RavSigpro, dbo.Vettori.RavSigsta, dbo.Rubrica.CruCellulare, 
                         dbo.Rubrica.CruEMailA, dbo.Rubrica.CruFax, dbo.Rubrica.CruTelefono, dbo.AnagraficheClientiFornitori.RacCodfis, dbo.StatiEsteri.RdeCodiso, dbo._ordini.centro, dbo._ordini.numero, dbo.Vettori.RavNote, 
                         dbo._ordini.stampa_etichette, dbo._ordini.stato, dbo.ClientiFornitoriDatiAgg.RveCateco
FROM            dbo._ordini INNER JOIN
                         dbo.AnagraficheClientiFornitori ON dbo._ordini.id_cliente = dbo.AnagraficheClientiFornitori.RacCodana INNER JOIN
                         dbo.ClientiFornitoriDatiAgg ON dbo.AnagraficheClientiFornitori.RacCodana = dbo.ClientiFornitoriDatiAgg.RveCodclf INNER JOIN
                         dbo.StatiEsteri ON dbo.AnagraficheClientiFornitori.RacSigsta = dbo.StatiEsteri.RdeCoddes LEFT OUTER JOIN
                         dbo.Rubrica ON dbo.ClientiFornitoriDatiAgg.RveCodclf = dbo.Rubrica.CruCodsog LEFT OUTER JOIN
                         dbo.Vettori ON dbo._ordini.id_trasportatore = dbo.Vettori.RavCodana LEFT OUTER JOIN
                         dbo._tipi_carrello ON dbo._ordini.id_tipo_carrello = dbo._tipi_carrello.id LEFT OUTER JOIN
                         dbo.VociIva ON dbo.ClientiFornitoriDatiAgg.RveEsenzi = dbo.VociIva.RiaCodiva
WHERE        (dbo.ClientiFornitoriDatiAgg.RveTipo = 0) AND (dbo.Rubrica.CruNumpro = 1)