SELECT        dbo._carrelli.id AS idCarrello, dbo._carrelli.id_ordine, dbo._carrelli.progr, dbo._carrelli.importo AS importoCarrello, dbo._righe.progr AS riga, dbo._righe.id_articolo, dbo._righe.importo AS importoRiga, dbo._righe.piante_ripiano, 
                         dbo._righe.sormontate, dbo._righe.stringa, dbo._righe.opzioni, dbo._righe.importo_varianti, dbo._righe.prezzo, dbo._righe.codice_variante, dbo._piante.cod, dbo._piante.nome, dbo._piante.vaso, dbo._piante.altezza_pianta, 
                         dbo._piante.foto1, dbo.CategorieMerceologiche.RdtDescri, dbo.VociIva.RiaDescri AS descr_aliquota_rigo, dbo.VociIva.RiaAliquo AS aliquota_rigo, dbo._piante.id_fornitore, dbo._piante.ean, dbo._piante.path_file, 
                         dbo.AnagraficheClientiFornitori.RacRagso1 AS f_RacRagso1, dbo.AnagraficheClientiFornitori.RacCodfis AS f_RacCodfis, dbo.AnagraficheClientiFornitori.RacPariva AS f_RacPariva, 
                         dbo.AnagraficheClientiFornitori.RacRagso2 AS f_RacRagso2, dbo.AnagraficheClientiFornitori.RacIndiri AS f_RacIndiri, dbo.AnagraficheClientiFornitori.RacCap AS f_RacCap, 
                         dbo.AnagraficheClientiFornitori.RacComune AS f_RacComune, dbo.AnagraficheClientiFornitori.RacSigpro AS f_RacSigpro, dbo.StatiEsteri.RdeCodiso AS f_RdeCodiso, dbo.Rubrica.CruCellulare AS f_CruCellulare, 
                         dbo.Rubrica.CruEMailA AS f_CruEMailA, dbo.Rubrica.CruFax AS f_CruFax, dbo.Rubrica.CruTelefono AS f_CruTelefono, dbo.ClientiFornitoriDatiAgg.RveLibLng1, dbo._righe.sconto, dbo._righe.prezzo_fornitore, 
                         dbo._righe.prezzo_etichetta, dbo._righe.ean AS righe_ean
FROM            dbo._carrelli INNER JOIN
                         dbo._righe ON dbo._carrelli.id = dbo._righe.id_carrello INNER JOIN
                         dbo._piante ON dbo._righe.id_articolo = dbo._piante.id INNER JOIN
                         dbo.Articoli ON dbo._piante.cod = dbo.Articoli.RanCodart LEFT OUTER JOIN
                         dbo.CategorieMerceologiche ON dbo.Articoli.RanCatmer = dbo.CategorieMerceologiche.RdtCodtot INNER JOIN
                         dbo.VociIva ON dbo.Articoli.RanCodiva = dbo.VociIva.RiaCodiva LEFT OUTER JOIN
                         dbo.AnagraficheClientiFornitori ON dbo._piante.id_fornitore = dbo.AnagraficheClientiFornitori.RacCodana LEFT OUTER JOIN
                         dbo.ClientiFornitoriDatiAgg ON dbo.ClientiFornitoriDatiAgg.RveCodclf = dbo.AnagraficheClientiFornitori.RacCodana AND dbo.ClientiFornitoriDatiAgg.RveTipo = 1 LEFT OUTER JOIN
                         dbo.StatiEsteri ON dbo.AnagraficheClientiFornitori.RacSigsta = dbo.StatiEsteri.RdeCoddes LEFT OUTER JOIN
                         dbo.Rubrica ON dbo.AnagraficheClientiFornitori.RacCodana = dbo.Rubrica.CruCodsog AND dbo.Rubrica.CruNumpro = 1