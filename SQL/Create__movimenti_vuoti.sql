CREATE TABLE [dbo].[_movimenti_vuoti](
    [id] [bigint] IDENTITY(1,1) NOT NULL,
    [codice_vuoto] [varchar](10) NOT NULL,
    [data_movimento] [datetime] NOT NULL,
    [codice_sogetto] [varchar](5) NOT NULL,
    [quantita] [float] NOT NULL,
    [tipo_movimento] [int] NOT NULL,
    [note] [varchar](255) NULL,
 CONSTRAINT [PK__movimenti_vuoti] PRIMARY KEY CLUSTERED 
(
    [id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
