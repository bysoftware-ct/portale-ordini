CREATE TABLE [dbo].[_RigheDocumenti](
	[RbfAnno] [smallint] NOT NULL,
	[RbfAnnord] [smallint] NULL,
	[RbfCodart] [varchar](15) NULL,
	[RbfCodatt] [varchar](4) NOT NULL,
	[RbfCodcam] [varchar](3) NULL,
	[RbfCodcen] [varchar](3) NOT NULL,
	[RbfCodcon] [varchar](9) NULL,
	[RbfCoddem] [varchar](3) NULL,
	[RbfCoddep] [varchar](3) NOT NULL,
	[RbfCodiva] [varchar](3) NULL,
	[RbfCodtes] [varchar](6) NULL,
	[RbfCodvar] [varchar](60) NULL,
	[RbfDescri] [varchar](3000) NULL,
	[RbfImplor] [float] NULL,
	[RbfImpnet] [float] NULL,
	[RbfImport] [float] NULL,
	[RbfImppro] [float] NULL,
	[RbfLibdat1] [datetime] NULL,
	[RbfLibdat2] [datetime] NULL,
	[RbfLibdat3] [datetime] NULL,
	[RbfLibdbl1] [float] NULL,
	[RbfLibdbl2] [float] NULL,
	[RbfLibdbl3] [float] NULL,
	[RbfLiblng1] [int] NULL,
	[RbfLiblng2] [int] NULL,
	[RbfLiblng3] [int] NULL,
	[RbfLibstr1] [varchar](255) NULL,
	[RbfLibstr2] [varchar](255) NULL,
	[RbfLibstr3] [varchar](255) NULL,
	[RbfMaxlivEsp] [smallint] NULL,
	[RbfNudepr] [smallint] NULL,
	[RbfNudequ] [smallint] NULL,
	[RbfNumord] [int] NULL,
	[RbfNumrig] [int] NOT NULL,
	[RbfNupnma] [int] NULL,
	[RbfNurima] [int] NULL,
	[RbfNurior] [int] NULL,
	[RbfPeimpr] [float] NULL,
	[RbfPrezzo] [float] NULL,
	[RbfProtoc] [int] NOT NULL,
	[RbfQuant1] [float] NULL,
	[RbfQuant2] [float] NULL,
	[RbfQuant3] [float] NULL,
	[RbfQuanti] [float] NULL,
	[RbfQumis2] [float] NULL,
	[RbfRivoor] [varchar](40) NULL,
	[RbfScont1] [float] NULL,
	[RbfScont2] [float] NULL,
	[RbfScont3] [float] NULL,
	[RbfSegfat] [smallint] NULL,
	[RbfStadib] [smallint] NULL,
	[RbfStamov] [smallint] NULL,
	[RbfTeacsa] [varchar](1) NULL,
	[RbfTipcomEsp] [smallint] NULL,
	[RbfTipord] [smallint] NULL,
	[RbfTippro] [smallint] NULL,
	[RbfTiprig] [smallint] NULL,
	[RbfUnimi2] [varchar](4) NULL,
	[RbfUnimis] [varchar](4) NULL,
	[RbfCodiaz] [varchar](50) NOT NULL,
 CONSTRAINT [PK__RigheDocumenti] PRIMARY KEY NONCLUSTERED 
(
	[RbfAnno] ASC,
	[RbfCodatt] ASC,
	[RbfCodcen] ASC,
	[RbfCoddep] ASC,
	[RbfProtoc] ASC,
	[RbfNumrig] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]


