ALTER TABLE _ordini ADD cliente NVARCHAR(100) NOT NULL DEFAULT('')
ALTER TABLE _ordini ADD vettore NVARCHAR(100) NOT NULL DEFAULT('')
ALTER TABLE _ordini ADD pagamento NVARCHAR(50) NOT NULL DEFAULT('')
ALTER TABLE _ordini ADD ddt_generati TINYINT NOT NULL DEFAULT (0)
-- 20/11/2021 per stampa etichette e codici a barre
ALTER TABLE _ordini ADD stampa_etichette TINYINT NOT NULL DEFAULT (0)
-- 09/12/2023 per sviluppo Garden Team
ALTER TABLE _ordini ADD n_ordine_gt NVARCHAR(35), data_ordine_gt DATE 