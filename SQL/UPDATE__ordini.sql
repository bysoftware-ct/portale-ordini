UPDATE
    _ordini
SET
    _ordini.cliente   = AnagraficheClientiFornitori.RacRagso1 + ' ' + AnagraficheClientiFornitori.RacRagso2,
    _ordini.vettore   = Vettori.RavRagso1 + ' ' + Vettori.RavRagso2,
    _ordini.pagamento = Pagamenti.RpaDescri
FROM
    _ordini
    INNER JOIN AnagraficheClientiFornitori
        ON _ordini.id_cliente = AnagraficheClientiFornitori.RacCodana
    INNER JOIN Vettori
        ON _ordini.id_trasportatore = Vettori.RavCodana
    INNER JOIN ClientiFornitoriDatiAgg
        ON _ordini.id_cliente = ClientiFornitoriDatiAgg.RveCodclf
    INNER JOIN Pagamenti
        ON ClientiFornitoriDatiAgg.RvePagame = Pagamenti.RpaCodpag
WHERE
    _ordini.cliente = '' AND _ordini.vettore = '' AND _ordini.pagamento = ''